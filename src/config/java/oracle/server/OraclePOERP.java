/*
    Copyright (c) 1996-2000 Ariba, Inc.
    All rights reserved. Patents pending.

    $Id: //ariba/current/code/sample/oracle-10.7/java/oracle/server/OraclePOERP.java#6

    Responsible: jspier
*/

/*
    ARIBA CUSTOM JAVA CLASS

    This file is intended as an example of how to customize functionality
    within the Ariba ORMS. If you wish to make modifications, either subclass
    or make a modified copy in the extension (config) area, and update the
    configuration parameters (either in parameters.table or an *Ext.aml file)
    accordingly. Consult the Application Customization Guide for more details.

    This software is the confidential and proprietary information of
    Ariba, Inc.  Use it only in accordance with the terms
    of the license agreement with Ariba.

    ARIBA MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
    SOFTWARE, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
    IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
    PURPOSE, OR NON-INFRINGEMENT. ARIBA SHALL NOT BE LIABLE FOR ANY DAMAGES
    SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
    THIS SOFTWARE OR ITS DERIVATIVES.
*/
/* 03/02/2004: Globally change Log.util.debug to Log.customer.debug (Rob Giesen) */

package config.java.oracle.server;

import ariba.common.core.Core;
// Ariba 8.1: Changed package for Currency and Money classes.
import ariba.basic.core.Currency;
import ariba.basic.core.Money;
import ariba.common.core.Supplier;
import ariba.common.core.SupplierLocation;
//jackie - need to ask Ariba consultant or in house ariba experts. using purchasing Log instead of procure log
import ariba.purchasing.core.Log;
//import ariba.procure.core.Log;
import ariba.purchasing.core.POLineItem;
import ariba.purchasing.core.PurchaseOrder;
import ariba.purchasing.core.ReqLineItem;
// Ariba 8.1: Changed packages for the OrderMethod classes.
import ariba.purchasing.ordering.AribaERPOrderMethod;
import ariba.purchasing.core.ordering.OrderMethod;
import ariba.purchasing.core.ordering.OrderMethodException;

/**
    Begin class OraclePOERP

    This class defines an ordering method to be used with the Oracle
    ERP.  This ordering method handles the breaking of requisitions
    into Purchase Orders by subclassing the AribaERPOrderMethod.

    Once reqs have been broken up into the appropriate POs, they are
    sent to the OraclePOERPFormatter and AribaPOERPSender.  These
    classes are responsible for formatting the Orders in the ways
    needed by Oracle Financials and for sending the POs to the ERP
    system, respectively.
*/
public class OraclePOERP extends AribaERPOrderMethod
{
    public static final String ClassName =
        "ariba.oracle.server.OraclePOERP";

    /**Buysense 06/27 Added this sender here. Not sure whether this is the right place */
    protected String OrderSenderClassName    =
        "config.java.oracle.server.BuysensePOERPSender";

    protected String OrderingMethod = "OraclePOERP";


    /**
        canProcessLineItem signals if this ordering method can handle the
        line item.

        @param item the item in question

        @return OrderMethod.canProcessLineItem or
        OrderMethod.cannotProcessLineItem
    */
    public int canProcessLineItem (ReqLineItem item)
      throws OrderMethodException
    {
        // We are going to compare the item's price to
        // 25000 units of the default system currency.
        //These are out of the box business rules.  Buysense baseline
        // will not enforce these rules/
        //Currency currency = Core.getService().getDefaultCurrency(item.getPartition());
        //Money money = new Money(25000, currency);
        //if (item.getAmount().compareTo(money) <= 0) {

            Supplier sup = item.getSupplier();
            SupplierLocation supLoc = item.getSupplierLocation();

            if (sup != null && supLoc != null)
            {
                if (sup.getCreator() == null && supLoc.getCreator() == null)
                {
                    int i =  OrderMethod.canProcessLineItem;
					Log.customer.debug("This the return value from the OrderMethod.canProcessLine Item: %s",i);
                    return OrderMethod.canProcessLineItem;
                }
            }
        //}

            // Some above property failed.  Reject the line item.
        Log.ordering.debug("Item %s Rejected by %s", item, ClassName);
        return OrderMethod.cannotProcessLineItem;
    }


    /**
        addRecipients adds the recipients defined by the
        class variables OrderSenderClassName and
        OrderFormatterClassName.

        Subclasses of AribaOrderMethod should override this method if
        they do not desire the default implementation.
    */
    protected void addRecipients (PurchaseOrder order)
      throws OrderMethodException
    {
        if (useReqOrPOIntegration(order.getPartition())) {
            Log.customer.debug("Buysense: Hit the addRecipients for %s",order);
           /* String csnId = (String)order.getSupplierLocation().getDottedFieldValue("AribaNetworkId");
            Log.customer.debug("Buysense:This is the CSN %s",csnId);
            if (csnId == null || csnId.trim().length() == 0) {
                addRecipient(order, "DirectOrder");
                return;
            }
            Log.customer.debug("Buysense: setting the ordering Method: %s",OrderingMethod); */
            addRecipient(order, OrderingMethod);
        }
        else {
            Log.fixme.warning(1583, ClassName);
        }
    }

    /**
        Provides a default implementation which returns true if the
        parent's method returns true and the two line items have the
        same currency.
        <p>
        This has the effect of putting all the line items with the
        same currency in the same order.
        <p>
        If subclasses want a different behavior, they should override
        this method.

        @param reqLineItem  reqLineItem to be put in a purchase order
        @param poLineItem   poLineItem from a purchase order which is used for
                            comparison with the ReqLineItem
        @return boolean showing whether the two line items belong to the
                        same purchase order
    */
    public boolean canAggregateLineItems (ReqLineItem reqLineItem,
                                          POLineItem  poLineItem)
      throws OrderMethodException
    {
        if (super.canAggregateLineItems(reqLineItem, poLineItem) == false) {
            return false;
        }

        Currency firstCur = reqLineItem.getDescription().getPrice().getCurrency();
        Currency nextCur = poLineItem.getDescription().getPrice().getCurrency();

        if ((firstCur == null) || (nextCur == null)) {
            return false;
        }

        if (firstCur.equals(nextCur)) {
            return true;
        }

        return false;
    }
}
