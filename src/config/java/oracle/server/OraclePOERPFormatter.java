/*
    Copyright (c) 1996-2000 Ariba, Inc.
    All rights reserved. Patents pending.

    $Id: //ariba/current/code/ariba/oracle/server/OraclePOERPFormatter.java#10 $

    Responsible: lbajaj
*/

/*
    ARIBA CUSTOM JAVA CLASS

    This file is intended as an example of how to customize functionality
    within the Ariba ORMS. If you wish to make modifications, either subclass
    or make a modified copy in the extension (config) area, and update the
    configuration parameters (either in parameters.table or an *Ext.aml file)
    accordingly. Consult the Application Customization Guide for more details.

    This software is the confidential and proprietary information of
    Ariba, Inc.  Use it only in accordance with the terms
    of the license agreement with Ariba.

    ARIBA MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
    SOFTWARE, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
    IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
    PURPOSE, OR NON-INFRINGEMENT. ARIBA SHALL NOT BE LIABLE FOR ANY DAMAGES
    SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
    THIS SOFTWARE OR ITS DERIVATIVES.
*/
/* 03/02/2004: Globally change Log.util.debug to Log.customer.debug (Rob Giesen) */
//rgiesen March 15, 2004 - Dev SPL # 19 - upgraded this file for 8.1, to compile

package config.java.oracle.server;

import ariba.base.core.Base;
import ariba.base.core.BaseVector;
import ariba.base.core.DynamicClusterRoot;
//import ariba.common.core.Splitable;
import ariba.common.core.SplitAccounting;
import ariba.util.core.FastStringBuffer;
//import ariba.procure.core.Log;
import ariba.purchasing.core.POLineItem;
import ariba.procure.core.ProcureLineItem;
import ariba.procure.core.ProductDescription;
import ariba.purchasing.core.PurchaseOrder;
// Ariba 8.1: Changed packages for the OrderMethod classes.
//import ariba.procure.server.ERPOrderOnServer;
import ariba.purchasing.core.ordering.OrderFormatter;
import java.io.PrintWriter;
import ariba.util.log.Log;
import ariba.oracle.server.OracleFormatter;


/**
    Begin class OraclePOERPFormatter

    This class defines an OrderFormatter for the Oracle ERP.
    It is an Ariba sample implementation of the formatting of an
    order as needed before it can be pushed to Oracle.
*/
public class OraclePOERPFormatter implements OrderFormatter
{
    private static String ExtDescription = "ExtDescription";
    private static String BillToLocation = "BillToLocation";
    private static String ShipToLocation = "ShipToLocation";

    private static final String FrozenOrder   = "FrozenOrder";
    private static final String ERPSplitValue = "ERPSplitValue";
    private static final String ReqNumber     = "ReqNumber";
    private static final String PONumber      = "PONumber";
    private static final String POLineNumber  = "POLineNumber";
    private static final String DeliverId     = "DeliverId";
    private static final String DestinationId = "DestinationId";
    private static final String OperatingUnit = "OperatingUnit";
    private static final String OracleRate    = "OracleRate";
    private static final String OracleRateDate = "OracleRateDate";
    private static final String QtyRcvTolerance = "QtyRcvTolerance";
    private static final String InventoryOrganization = "InventoryOrganization";

    private static final String OverReceivingPercentage =
        "Application.Procure.OverReceivingPercentage";

    /**
        No construction is needed
    */
    public OraclePOERPFormatter ()
    {
    }

    /*
        format formats an order as needed for Oracle 10.5 <br>

        @param order The purchase order to format
        @param writer Not used
        @return Not used
    */
    public String format (PurchaseOrder order, PrintWriter writer)
    {
            // Set the orderID from uniquename. At this point, we are sure that
            // we are generating the order for the line items, and the order id can be
            // set from the unique name. In the case of Requisition, a pull adapter and
            // ERPOrderPostHook() method will overwrite the OrderId, that it gets from
            // the ERP system.
            // Note that we don't want to RE-set the order id if this is
            // a change or cancel order.
        if ((order.getOrderedState()          != PurchaseOrder.Canceling) &&
            (order.getOrderedState()          != PurchaseOrder.Canceled) &&
            (order.getLineItemsChangedState() != 0)) {
            order.setOrderID(order.getUniqueName(), true);
        }

        String overRecvPercentage =
            Base.getService().getParameter(order.getPartition(), OverReceivingPercentage);
        boolean addBillToLocationFlag = false;

        BaseVector lineItemVector = order.getLineItems();
        int numLineItems = lineItemVector.size();
        for (int j = 0; j < numLineItems; j++) {
            //Ariba 8.1 Replaced elementAt with get() ProcureLineItem li =
            //    (ProcureLineItem)lineItemVector.elementAt(j);
            ProcureLineItem li =
                (ProcureLineItem)lineItemVector.get(j);
            ProductDescription pd = li.getDescription();
            FastStringBuffer fsb = new FastStringBuffer();
            String supplierPartNumber = pd.getSupplierPartNumber();
            if ((supplierPartNumber != null) &&
                (supplierPartNumber.length() != 0)) {
                fsb.append(supplierPartNumber + "|");
            }
            String manPartNumber = pd.getManPartNumber();
            if ((manPartNumber != null) &&
                (manPartNumber.length() != 0)) {
                fsb.append(manPartNumber + "|");
            }
            fsb.append(pd.getDescription());
            li.setFieldValue(OraclePOERPFormatter.ExtDescription, fsb.toString());
            li.setFieldValue(QtyRcvTolerance, overRecvPercentage);

            if (addBillToLocationFlag == false) {

                addBillToLocationFlag = true;

                order.setFieldValue(BillToLocation,
                                    li.getBillingAddress());
                order.setFieldValue(ShipToLocation,
                                    li.getShipTo());
            }
        }

            // Set the frozen flag, based on whether or not
            // this is a CC:DO.
        if (order.isCarbonCopyDO()) {
            order.setFieldValue(FrozenOrder, "Y");
        }
        else {
            order.setFieldValue(FrozenOrder, "N");
        }

        OracleFormatter.moneyFormatForOrders(order);
        formatSplitAccountings(order);
        order.save();

            // Dummy return value
        return "";
    }

    /**
        This method does the formatting for split accounting
        objects so that the proper values are sent to the
        interface tables for Oracle Financials.
    */
    protected void formatSplitAccountings (PurchaseOrder order)
    {
            // Get the purchase order number
        String poNum = order.getUniqueName();
        BaseVector lineItemVector = order.getLineItems();
        int numLineItems = lineItemVector.size();
        DynamicClusterRoot invOrg =
            (DynamicClusterRoot)order.getFieldValue(InventoryOrganization);
        DynamicClusterRoot opUnit =
            (DynamicClusterRoot)invOrg.getFieldValue(OperatingUnit);

        for (int j = 0; j < numLineItems; j++) {
            //Ariba 8.1 POLineItem li = (POLineItem)lineItemVector.elementAt(j);
            POLineItem li = (POLineItem)lineItemVector.get(j);

                // Get the requisition number and the ship to number
            String reqNum = li.getRequisition().getUniqueName();
            String shipTo = li.getShipTo().getUniqueName();

                // Go through each of the split accountings and
                // assign the proper values for the extrinsics
            BaseVector splitAccountingVector =
                li.getAccountings().getSplitAccountings();
            int numSplitAccountings = splitAccountingVector.size();

            for (int i = 0; i < numSplitAccountings; i++) {
                //Ariba 8.1 SplitAccounting sa = (SplitAccounting)splitAccountingVector.elementAt(i);
                SplitAccounting sa = (SplitAccounting)splitAccountingVector.get(i);

                    // set various other fields
                sa.setFieldValue(ERPSplitValue,  sa.getQuantity().toString());
                sa.setFieldValue(ReqNumber,      reqNum);
                sa.setFieldValue(PONumber,       poNum);
                sa.setFieldValue(POLineNumber,
                                 Integer.toString(li.getNumberInCollection()));
                sa.setFieldValue(DeliverId,      shipTo);
                sa.setFieldValue(DestinationId,  invOrg.getUniqueName());
                sa.setFieldValue(OperatingUnit,  opUnit.getUniqueName());
                sa.setFieldValue(OracleRate,     order.getFieldValue(OracleRate));
                sa.setFieldValue(OracleRateDate, order.getFieldValue(OracleRateDate));
            }
        }
    }
}
