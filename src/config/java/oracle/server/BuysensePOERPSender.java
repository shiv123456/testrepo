/*


    Responsible: imohideen
*/



package config.java.oracle.server;

// Ariba 8.1: Permission class changed from ariba.common.core package to ariba.user.core package.
import ariba.user.core.Permission;
import ariba.common.core.SupplierLocation;
// Ariba 8.1:  Util class has been deprecated.
//import ariba.util.core.Util;

//rgiesen March 15, 2004 - Dev SPL # 19 commented out unused and deprecated classes
//import ariba.integration.core.IntegrationEventDataInterface;
//import ariba.integration.eventprocessor.IntegrationDirectory;
//import ariba.integration.eventprocessor.IntegrationDirectoryException;
//import ariba.integration.server.IntegrationSendEvent;
import ariba.procure.core.Log;
import ariba.purchasing.core.OrderRecipient;
import ariba.purchasing.core.PurchaseOrder;
//import ariba.procure.server.Notifications;
// Ariba 8.1: Changed packages for the OrderMethod classes.
import ariba.purchasing.ordering.AribaOrderMethod;
import ariba.purchasing.core.ordering.OrderSender;
//import ariba.server.objectserver.core.Event;
import java.io.IOException;
import java.io.InputStream;
//rgiesen March 15, 2004 - Dev SPL # 19 Added util.core
import ariba.util.core.*;

import config.java.ams.custom.AMSPOPrep;
/* 03/02/2004: Globally change Log.util.debug to Log.customer.debug (Rob Giesen) */

/**
    Implements a generic OrderSender.  This may be used, for instance,
    when approved orders should be sent to a legacy or ERP system.
*/
public class BuysensePOERPSender extends OrderSender
{
    public static final String ClassName =
        "ariba.procure.server.ordering.BuysensePOERPSender";


    /**
        No construction needed.
    */
    public BuysensePOERPSender ()
    {
    }

    /**
        The Permission to notify about order cancels
    */
    public static final String PermissionPurchaseOrderCancelEmail =
        "PurchaseOrderCancelEmail";

    /**
        The Permission to notify about order changes
    */
    public static final String PermissionPurchaseOrderChangeEmail =
        "PurchaseOrderChangeEmail";

    /**
        Sends the order to the proper ERP adapter.
        Note that the send method uses parameters defined
        in the Parameters.table file, such as
        UseReqOrPOIntegration and UseChangeOrderIntegration,
        and gets the adapter to use from the
        Application.Procure.PurchaseOrderAdapter parameter.

        @param recipient This order's OrderRecipient
        @param po The order to send.
        @param formattedOrder Not used.
        @param fileExtension Not used.
    */
    public void send (OrderRecipient recipient,
                      PurchaseOrder  po,
                      InputStream    formattedOrder,
                      String         fileExtension)
      throws IOException
    {
        String tranType ;

        int orderState = po.getOrderedState();

        Log.customer.debug( "This is the state of the Order in the sender: %s",new Integer (orderState));

        Log.customer.debug("This is useReqFlag: %s",new Boolean(AribaOrderMethod.useReqOrPOIntegration(po.getPartition())));
         // The UseReqOrPOIntegration parameter should be set to true!
        if (AribaOrderMethod.useReqOrPOIntegration(po.getPartition()) == false) {
            Log.fixme.warning(860, ClassName);
        }

            // Find the adapter to use
        String adapter = AribaOrderMethod.purchaseOrderEvent(po.getPartition());
		//rgiesen March 15, 2004 - Dev SPL # 19 use StringUtil
		//if (Util.nullOrEmptyString(adapter)) {
        if (StringUtil.nullOrEmptyString(adapter)) {
            Log.fixme.warning(886, ClassName);
            return;
        }

        /*  eProcure Implementation ST SPL # 559 - The following logic was commented out because
            it is no longer required now that ERPOrders are going to have ChangeOrder functionality
            enabled.
            // If the UseChangeOrder system parameter is not true...
        if (!AribaOrderMethod.useChangeOrderIntegration(po.getPartition())) {

                // If this order is being canceled, we send an email
            if (po.getOrderedState() ==  PurchaseOrder.Canceling) {


                Log.ordering.debug(ClassName +
                               ":  Attempt to cancel order %s without " +
                               "UseChangeOrderIntegration system parameter set " +
                               "to true.  This order will not be sent to " +
                               "adapter %s.  An email is being sent to " +
                               "PurchasingAgents.", po, adapter);

                Permission pa =
                    Permission.getPermission(PermissionPurchaseOrderCancelEmail);
                Notifications.sendMail(
                    pa.users(po.getPartition()),
                    po,
                    Notifications.PurchaseOrderCancel,
                    "has been canceled",
                    "has been canceled",
                    "Content/procure/notifications/ChangeOrderCancel.htm");
                return;
            }

                // If this order is being change, we send an email to notify
                // the purchasing agents of the change.
            else if (po.getLineItemsChangedState() != 0) {
                Log.ordering.debug(ClassName +
                               ":  Attempt to change order %s without " +
                               "UseChangeOrderIntegration system parameter set " +
                               "to true.  This order will not be sent to " +
                               "adapter %s.  An email is being sent to " +
                               "PurchasingAgents.", po, adapter);

                Permission pa =
                    Permission.getPermission(PermissionPurchaseOrderChangeEmail);
                Notifications.sendMail(
                    pa.users(po.getPartition()),
                    po,
                    Notifications.PurchaseOrderChange,
                    "has been changed",
                    "has been changed",
                    "Content/procure/notifications/po-change.htm");
                return;
            }

        }
        */

       // We will set up the tranType here. AMSPOPrep know how to handle the tran type.
       if (po.getOrderedState() ==  PurchaseOrder.Canceling) tranType ="cancel";
       else tranType = "new";
       Log.customer.debug("Buysense in POERPSender calling AMSPOPrep");
       int result = AMSPOPrep.send(po, tranType,recipient);
    }

    public String category ()
    {
        return SupplierLocation.ERP;
    }
}
