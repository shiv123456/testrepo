/*
    Copyright (c) 1996-2000 Ariba, Inc.
    All rights reserved. Patents pending.

    $Id: //ariba/release/6.1/product/code/sample/oracle-10.7/java/oracle/server/OraclePOERPCC.java#6

    Responsible: jspier
*/

/*
    ARIBA CUSTOM JAVA CLASS

    This file is intended as an example of how to customize functionality
    within the Ariba ORMS. If you wish to make modifications, either subclass
    or make a modified copy in the extension (config) area, and update the
    configuration parameters (either in parameters.table or an *Ext.aml file)
    accordingly. Consult the Application Customization Guide for more details.

    This software is the confidential and proprietary information of
    Ariba, Inc.  Use it only in accordance with the terms
    of the license agreement with Ariba.

    ARIBA MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
    SOFTWARE, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
    IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
    PURPOSE, OR NON-INFRINGEMENT. ARIBA SHALL NOT BE LIABLE FOR ANY DAMAGES
    SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
    THIS SOFTWARE OR ITS DERIVATIVES.
*/
/* 03/02/2004: Globally change Log.util.debug to Log.customer.debug (Rob Giesen) */

package config.java.oracle.server;

// Ariba 8.1: Changed package for Currency and Money classes.
import ariba.basic.core.Currency;
import ariba.basic.core.Money;
import ariba.util.log.Log;
import ariba.purchasing.core.PurchaseOrder;
import ariba.purchasing.core.ReqLineItem;
// Ariba 8.1: Changed packages for the OrderMethod classes.
import ariba.purchasing.ordering.AribaERPOrderMethod;
import ariba.purchasing.core.ordering.OrderMethod;
import ariba.purchasing.core.ordering.OrderMethodException;


/**
    Begin class OraclePOERPCC

    This class defines an ordering method to be used with the Oracle
    ERP.  This ordering method handles the breaking of requisitions
    into Purchase Orders by subclassing the AribaERPOrderMethod.

    Once reqs have been broken up into the appropriate POs, they are
    sent to the OraclePOERPFormatter and AribaPOERPSender.  These
    classes are responsible for formatting the Orders in the ways
    needed by Oracle Financials and for sending the POs to the ERP
    system, respectively.
*/
public class OraclePOERPCC extends OraclePOERP
{
    public static final String ClassName =
        "ariba.oracle.server.OraclePOERPCC";

    protected String OrderSenderClassName    =
        "config.java.oracle.server.BuysensePOERPSender";
    protected String OrderFormatterClassName =
        "config.java.oracle.server.OraclePOERPFormatter";

    /**
        canProcessLineItem signals if this ordering method can handle the
        given line item.  This order method will reject the line item if:
          The supplier has no corresponding "direct" (Ariba Network) supplier
          The supplier does not exist in the current partition

        This method overrides the superclass implementation,
        which implements a "default" set of business rules.

        It is likely that this will not be the desired set of rules for
        any given installation of Ariba.  It is therefore probable that
        you will need to override this function (in a subclass) or write
        a custom order method.

        @param item the item in question
        @return OrderMethod.canProcessLineItem or OrderMethod.cannotProcessLineItem
    */
    public int canProcessLineItem (ReqLineItem item)
      throws OrderMethodException
    {
            // Check whether our parent class can handle this item
            // Among other things, this makes sure that the supplier
            // and supplier location aren't null.
        if (super.canProcessLineItem(item) == OrderMethod.cannotProcessLineItem) {
            return OrderMethod.cannotProcessLineItem;
        }

        return AribaERPOrderMethod.canProcessLineItemAsERPCC(item);
    }

    /**
        Returns the category of this OrderMethod

        @return <code>PurchaseOrder.ERPCategory</code>
    */
    public String category ()
    {
        return PurchaseOrder.ERPCCCategory;
    }
}



