// Decompiled by Decafe PRO - Java Decompiler

// Ariba 8.1: Moved this class to the config.java.oracle.server package with all the other custom order methods.
package config.java.oracle.server;

// Ariba 8.1: Added import for the ariba.procure.ordering package now that this class has moved to config.java.oracle.server package
import ariba.purchasing.core.Requisition;
import ariba.purchasing.core.PurchaseOrder;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.POLineItem;
import ariba.purchasing.ordering.AribaOrderMethod;
import ariba.purchasing.core.ordering.OrderMethodException;
// Ariba 8.1: Added package for Currency and Money classes.
import ariba.basic.core.Currency;
import ariba.base.core.*;
import ariba.common.core.*;
// Ariba 8.1: Added import for the new ariba.user.core.User class.
import ariba.user.core.User;
import ariba.procure.core.*;
import java.util.List;
//jackie - need to ask Ariba consultant or in house ariba experts. using purchasing Log instead of procure log
import ariba.purchasing.core.Log;
//import ariba.procure.core.Log;
//import ariba.base.core.BasicFieldDefaulter;
import config.java.ams.custom.BuyintConstants;
/*** JN 8.2.2 upgrade - Adding PCard specific ordering method per support case SR#1-564606151 ***/
import ariba.pcard.ordering.AribaPCardOrderMethod;

/***************************
Rob Giesen
December 2000
Extended the AribaBuysensePCard class to allow for the user to select whether they want to use
a PCard or not, and if so, which PCard they want to use (if they have multi-PCards assigned
to them.

JPerry and RLee, June 8, 2001
Take out if statement to allow users to order non-catalog items with Pcard.


Marc Berlove, December 5, 2001
Took out the section of code that would split the order when the accounting or line information was different.
Code was also added to insure that orders with different currencies are split, which matches the other
Buysense ordering methods. eVA Prod SPL # 104
rlee 2/12/2004 Prod SPL#281 - Expense/Purchase Pcards not generating PcardOrders.
     Mod to allow PCard type 3 as well as type 2

rlee 3/31/2005:  Dev SPL 543 - INT PCard change order before ERP Response

****************************/
/* 03/02/2004: Globally change Log.util.debug to Log.ordering.debug (Rob Giesen) */

/*** JN 8.2.2 upgrade - Adding PCard specific ordering method per support case SR#1-564606151 ***/
//public class AribaBuysensePCard extends AribaOrderMethod implements BuyintConstants
public class AribaBuysensePCard extends AribaPCardOrderMethod implements BuyintConstants
{

    public AribaBuysensePCard()
    {
                /*** JN 8.2.2 upgrade - Adding PCard specific ordering method per support case SR#1-564606151 ***/
		/// Calling the parent.
		super();
    }

    public int canProcessLineItem(ReqLineItem lineItem)
        throws OrderMethodException
    {
        if(!supplierLocationTakesPCard(lineItem))
        {
            Log.ordering.debug("Rejected by AribaBuysensePCard because SupplierLocation of %s does not take PCard.", lineItem);
            return 0;
        }
        User user = getRequester(lineItem);
        List pcards = ariba.common.core.User.getPartitionedUser(user,Base.getSession().getPartition()).getPCardsVector();

        if(pcards.isEmpty())
        {
            Log.ordering.debug("Rejected by AribaBuysensePCard because %s does not have PCard.", user);
            return 0;
        }

        //Added by Rob Giesen
        //Get the requisition, and get the newly added field "UsePCardBool" that the user specified in the requisition header
        Requisition currentReq = (Requisition) lineItem.getLineItemCollection();
        Boolean bolUserPCard = (Boolean)currentReq.getFieldValue("UsePCardBool");


	if (bolUserPCard.booleanValue()==false)
	{
            Log.ordering.debug("Rejected by AribaBuysensePCard because %s specified that they did not want to use a PCard.",user);
            return 0;
        }

        BaseId pcardId = pickPCard(lineItem);

        if(pcardId == null)
        {
            Log.ordering.debug("Rejected by AribaBuysensePCard because there is no valid PCard.");
            return 0;
        }
        else
        {
			Log.ordering.debug("Accepted by AribaBuysensePCard.");
            return 1;
        }
    }


    public void processLineItem(ReqLineItem lineItem)
        throws OrderMethodException
    {
        Log.customer.debug("Calling precessLineItem lineItem = " + lineItem);
        User user = getRequester(lineItem);
        OrderInfo orderInfo = lineItem.getOrderInfo();
        if(orderInfo == null)
        {
            orderInfo = (OrderInfo)BaseObject.create("ariba.procure.core.OrderInfo", lineItem.getPartition());
            lineItem.setOrderInfo(orderInfo);
        }
        BaseId pcardId = pickPCard(lineItem);
        orderInfo.setPCardId(pcardId);
        super.processLineItem(lineItem);

        // CSPL-842 New pcard values not reflected on change orders 
        PurchaseOrder loNewOrder = (PurchaseOrder) lineItem.getOrder();
        if(loNewOrder != null && loNewOrder.instanceOf("ariba.pcard.core.PCardOrder"))
        {
            Requisition loNewReq = (Requisition)lineItem.getLineItemCollection();
            PCard loReqPCard = (PCard) loNewReq.getFieldValue("PCardToUse");
            loNewOrder.setFieldValue("PCard", loReqPCard);
            loNewOrder.save();
        }
    }

    /*
     * rlee Dev SPL 543
     * Before leaving PCard order process, set EncumbranceStatus and Encumbered fields to default value
     * in the case of changed order where Ariba clones this order from the previous version
     * which would set the EncumbranceStatus to a non-default values causing the AMSPushPCO not able to push this order.
     */
    public List endProcessingRequisition(Requisition req)
        throws OrderMethodException
    {
       	List lvVector = super.endProcessingRequisition(req);
	Log.ordering.debug("Calling endProcessingRequisition with lvVector = " + lvVector);

	for(int i = 0; i < lvVector.size(); i++)
	{
		PurchaseOrder loPO = (PurchaseOrder)lvVector.get(i);
		loPO.setFieldValue("EncumbranceStatus", STATUS_ERP_READY);
		loPO.setFieldValue("Encumbered", new Boolean("false"));
	}
	return lvVector;
    }

    public boolean canAggregateLineItems(ReqLineItem li1, POLineItem li2)
        throws OrderMethodException
    {
        if(!super.canAggregateLineItems(li1, li2))
	{
                    /*** JN 8.2.2 upgrade - Adding event==null per support case SR#1-564606111 ***/
		    Log.ordering.debug("AribaBuysensePCard can't aggregate");
            return false;
	}

    /* The following block of code was added to make the PCard ordering method behave the same as the ERP ordering
       Method. eVA Prod SPL # 104
    */
        Currency firstCur = li1.getDescription().getPrice().getCurrency();
		Currency nextCur = li2.getDescription().getPrice().getCurrency();

		if ((firstCur == null) || (nextCur == null)) {
		    return false;
		}

		if (firstCur.equals(nextCur)) {
		    return true;
		}

        return false;


    /* The following block of code was taken out so that the Pcard orders do not split if the accounting
       information is different between lines. eVA Prod SPL # 104

        OrderInfo info1 = li1.getOrderInfo();
        if(info1 == null)
        {
            Log.ordering.debug("AribaBuysensePCard can't aggregate %s: no order info.", li1);
            return false;
        }
        OrderInfo info2 = li2.getOrderInfo();
        if(info2 == null)
        {
            Log.ordering.debug("AribaBuysensePCard can't aggregate %s: no order info.", li2);
            return false;
        }

        if(!info1.equals(info2))
        {
            Log.ordering.debug("AribaBuysensePCard can't aggregate %s and %s.", li1, li2);
            return false;
        }
        if(!OrderMethodUtil.lineItemsHaveSameAccountings(li1, li2))
        {
            Log.ordering.debug("AribaBuysensePCard can't aggregate %s and %s due to different accountings.", li1, li2);
            return false;
        }
        else
        {
            return true;
        }
    */

    }

    public String category()
    {
        return "PCard";
    }

    protected User getRequester(ReqLineItem lineItem)
    {
        Requisition req = (Requisition)lineItem.getLineItemCollection();
        return req.getRequester();
    }

    protected boolean supplierLocationTakesPCard(ReqLineItem lineItem)
    {
        ariba.common.core.SupplierLocation location = lineItem.getSupplierLocation();
        if(location != null)
            return location.getPCardAcceptanceLevel() != 0;
        else
            return false;
    }

    protected BaseId pickPCard(ReqLineItem lineItem)
    {
		//Get the requisition
		Requisition currentReq = (Requisition) lineItem.getLineItemCollection();

		//Get the PCard specified by the user
		PCard usePCard = (PCard)currentReq.getFieldValue("PCardToUse");
		if (usePCard==null)
		{
			return null;
		}

		//Get the actual PCard, so that we can get the CardType
		//Only use cards of type 2 (procurement cards)
		if ((usePCard.getCardType())==2 || (usePCard.getCardType())==3)
		{
			if (usePCard.isValid())
				{
					//function is expecting a BaseId, so return it
					return (BaseId)(usePCard.getBaseId());
				}
		}

		//No usable PCard was found, return null
		return null;

    }
}
