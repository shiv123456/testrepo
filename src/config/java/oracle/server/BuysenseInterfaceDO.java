/*
    Responsible: labraham
*/

/*
    BUYSENSE CUSTOM JAVA CLASS TO HANDLE SPECIAL FUNCTIONALITY FOR INTERFACED ORDERS
*/

package config.java.oracle.server;

import ariba.base.core.*;
import ariba.procure.core.Log;
import ariba.purchasing.core.POLineItem;
import ariba.purchasing.core.PurchaseOrder;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
// Ariba 8.1: Changed packages for the OrderMethod classes.
//import ariba.purchasing.ordering.AribaERPOrderMethod;
//import ariba.procure.server.ordering.OrderMethod;
//import ariba.procure.server.ordering.OrderMethodException;
import ariba.purchasing.ordering.AribaERPOrderMethod;
import ariba.purchasing.core.ordering.OrderMethod;
import ariba.purchasing.core.ordering.OrderMethodException;

// Ariba 8.1: Added new ListUtil class.
import ariba.util.core.ListUtil;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import ariba.base.core.aql.*;
import ariba.basic.core.Currency;
import ariba.basic.core.Money;
import ariba.common.core.Core;


/**

    This class defines an ordering method to handle special functionality for processing
    imported interfaced orders.

*/
// Ariba 8.1: Changed package for the AribaDirectOrder Method class.
/* 03/02/2004: Globally change Log.util.debug to Log.customer.debug (Rob Giesen) */
public class BuysenseInterfaceDO extends ariba.purchasing.ordering.AribaDirectOrder
{
    private static final String PREFIX = "IRQ:";


    /**
        canProcessLineItem signals if this ordering method can handle the
        line item.

        @param item the item in question

        @return OrderMethod.canProcessLineItem or
        OrderMethod.cannotProcessLineItem
    */
    public int canProcessLineItem (ReqLineItem item)
      throws OrderMethodException
    {
       Log.customer.debug("inside BuysenseInterfaceDO");
       Requisition req = (Requisition)item.getLineItemCollection();
       
       boolean bReqSendEmallReqToQQ = false;

       bReqSendEmallReqToQQ = ((Boolean) req.getDottedFieldValue("ReqHeadCB5Value")).booleanValue();

       Log.customer.debug("BuysenseInterfaceDO::canProcessLineItem bReqSendEmallReqToQQ: "+ bReqSendEmallReqToQQ);
       
       // Checking only for ReqHeadCB5Value not ClientName.SendEmallReqToQQ, as it would have been checked in Submit/Approve hooks before invoking this Order Method.
       
       if(bReqSendEmallReqToQQ)
       {
           super.canProcessLineItem(item);
           Log.customer.debug("BuysenseInterfaceDO::canProcessLineItem marked "+ item+" for DO");
           return OrderMethod.canProcessLineItem;
       }
       
       Object transType = req.getFieldValue("TransactionSource");
       boolean lbBypassApprovers = (Boolean)req.getFieldValue("BypassApprovers");
       if (transType == null) {
           return OrderMethod.cannotProcessLineItem;
       }
       else if((!lbBypassApprovers)&& (transType.toString().equals("EXT")))
       {
    	   Log.customer.debug("BuysenseInterfaceDO::External Requisition found: BuysenseInterfaceDO will not process the line items");
    	   return OrderMethod.cannotProcessAllLineItems;
       }
       else {
           if ((transType.toString().equals("EXT")))
           {
               //START::Set effective user if it is null for external import orders
        	   if(Core.getService().getEffectiveUser() == null)
        	   {
        		   BaseSession loSession = Base.getSession();
        		   loSession.setEffectiveUser(req.getRequester().getBaseId());
                        Log.customer.debug("BuysenseInterfaceDO::EffectiveUsersetsuccessfull"+loSession.getEffectiveUser());
        	   }
        	   else
        	   {
        		   Log.customer.debug("BuysenseInterfaceDO::SRINI::Need to place this somewhere before SETCEME called");
        	   }
               //END::Set effective user if it is null for external import orders

	       super.canProcessLineItem(item);
               return OrderMethod.canProcessLineItem;
	   }
           else {
               return OrderMethod.cannotProcessLineItem;
           }
       }
    }


    /**
        Call OrderUtil.genOrderUniquename() which will increment order uniquenames if necessary

        @param foReq the Requisition in question

        @return List of Orders that were generated for this Requisition
    */
    public List endProcessingRequisition (Requisition foReq)
      throws OrderMethodException
    {
       Log.customer.debug("inside BuysenseInterfaceDO endProcessingRequisition");

       VersionedOrderUtil loVersionedOrderUtil = new VersionedOrderUtil(); 
       List lvOrders = super.endProcessingRequisition(foReq);
       if( lvOrders.size() > 0)
       { 
       String lsPOID2 = (String) ((PurchaseOrder) lvOrders.get(0)).getFieldValue("OrderID");
         Log.customer.debug("rlee InterfaceDO lsPOID2 = " + lsPOID2);
       }


       if ( ((String)foReq.getFieldValue("UniqueName")).startsWith(PREFIX) ) 
       {
           Log.customer.debug("PO is a result of an Interfaced Order");
           loVersionedOrderUtil.genOrderUniqueName(foReq, lvOrders); 
       }
       else 
       {
           Log.customer.debug("ERROR - Interfaced PO does not start with " + PREFIX);
       }

        /* Update the Direct Order (this is dummy; of no procurement value) that is created after sending the REq to QQ to fulfill below business needs
             A blank order for $0.00 will be created with no Order Number.
             After the ordering process has completed, the Ordered requisition cannot be changed, canceled, or received.?? (need to check)
               No additional action can be taken on this requisition after it has moved to Quick Quote. */
       boolean bReqSendEmallReqToQQ = false;
       
       Boolean loReqSendEmallReqToQQ = (Boolean) foReq.getDottedFieldValue("ReqHeadCB5Value");
       bReqSendEmallReqToQQ = (loReqSendEmallReqToQQ != null && loReqSendEmallReqToQQ.booleanValue());
       
       if(bReqSendEmallReqToQQ)
       {
           processNoValuePO(foReq, lvOrders);
       }
       
       return lvOrders;
    }


    private void processNoValuePO(Requisition foReq, List lvOrders)
    {
        String sReqUniqueName = foReq.getUniqueName();

        for (int i = 0; i < lvOrders.size(); i++)
        {
            PurchaseOrder loPO = (PurchaseOrder) lvOrders.get(i);
            Object lo = loPO.getDottedFieldValue("LineItems[0].Requisition.UniqueName");
            String sPOReqUN = lo != null ? (String) lo : "";

            if (sReqUniqueName.equalsIgnoreCase(sPOReqUN))
            {
                for (Iterator poliI = loPO.getLineItemsIterator(); poliI.hasNext();)
                {
                    POLineItem loPOLI = (POLineItem) poliI.next();
                    Log.customer.debug("BuysenseInterfaceDO::processNoValuePO Updating loPOLI: " + loPOLI);
                    loPOLI.setQuantity(new BigDecimal(0.0));
                    loPOLI.setAmount(new Money(0.0, Currency.getDefaultCurrency(foReq.getPartition())));
                    loPOLI.setOrderID("");
                    loPOLI.setReceivingType(4); // No receipt
                }

                loPO.setOrderID("", true);
                loPO.updateTotalCost();
                // calling CloseOrder will update the Composing Receipt and all other Status making it of no value in terms of procurement
                // For now passing null for Close comment. Need to add one?? (just to make sure that it is closed programmatically for E2Q)
                /*   int ClosedForChange = 2;
                     int ClosedForReceiving = 3;
                     int ClosedForInvoicing = 4;
                     int ClosedForAll = 5;
                     int ClosedForReceivingAndAdjust = 6;*/
                loPO.setClosed(5);
                loPO.closeOrder(null);
            }

            Log.customer.debug("BuysenseInterfaceDO::processNoValuePO Updated PO: " + loPO);
        }

    }


    public BuysenseInterfaceDO()
    {
    }

}
