/**
	Author: Mommasani Srinivasulu Reddy
	Date: 11/28/2008
	Comments: This class is created for CSPL-735. This is the revision of OLD BuysensePOERP and BuysensePOERPCC java classes. Core objective of this
	class is to combine both ORDER METHODS BuysensePOERP and BuysensePOERPCC into one without affecting the existing functionalities of ERP ORDER METHODS.
	This class will eliminates BuysensePOERP, BuysensePOERPCC, OraclePOERP and OraclePOERPCC and this only class will take care of all functionalities
	of other ERP classes
	Modifications: Main Modifications is in protected void addRecipients (PurchaseOrder order) methods. Based on the category it will return ERP or ERPCC

	Revision History:
	Author: Mommasani Srinivasulu Reddy
	Comments: Commented three lines before last line of canProcessLineItem method for CSPL-926. Please see line level comment by searching with
	keyword CSPL-926.
*/

package config.java.oracle.server;

import ariba.base.core.Base;
import ariba.basic.core.Currency;
import ariba.basic.core.Money;
import ariba.common.core.Supplier;
import ariba.common.core.SupplierLocation;
import ariba.util.log.Log;
import ariba.purchasing.core.PurchaseOrder;
import ariba.purchasing.core.POLineItem;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.purchasing.ordering.AribaERPOrderMethod;
import ariba.purchasing.core.ordering.OrderMethod;
import ariba.purchasing.core.ordering.OrderMethodException;
import ariba.base.core.BaseVector;
import java.math.BigDecimal;
import java.util.List;
import ariba.util.core.ListUtil;
import ariba.util.core.StringUtil;
import ariba.base.core.TransientData;


public class BuysensePOERPCC extends AribaERPOrderMethod
{
    public static final String ClassName =
        "config.java.oracle.server.BuysensePOERPCC";

    protected String OrderSenderClassName    =
        "config.java.oracle.server.BuysensePOERPSender";
    protected String OrderFormatterClassName =
        "config.java.oracle.server.OraclePOERPFormatter";

    //e2e
    private static final String  PROCUREMENT = "Procurement" ;
    //e2e

    private static final String PREFIX = "E2E:";

    protected String OrderingMethodERPCC = "OraclePOERP";

    protected String OrderingMethodERP = "BuysensePOERP";

   BigDecimal encumLimit;
   BaseVector refLIVector;
   public void beginProcessingRequisition (Requisition req)
      throws OrderMethodException
    {
    super.beginProcessingRequisition(req);
    }

	public int canProcessLineItem (ReqLineItem item)
      throws OrderMethodException
    {
     //e2e

     boolean lboolADVPro = false;
     boolean lboolEncumbrance = false;

     // Get the ERPName from requester's Buysense Org client
     //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
     //Partitioned User to obtain the extrinsic BuysenseOrg field value.
     ariba.user.core.User liRequesterUser = (ariba.user.core.User)item.getLineItemCollection().getDottedFieldValue("Requester");
     ariba.common.core.User liRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(liRequesterUser,Base.getSession().getPartition());
     lboolADVPro = ((Boolean)liRequesterPartitionUser.getDottedFieldValue("ClientName.ADVPro")).booleanValue();
     lboolEncumbrance = ((Boolean)liRequesterPartitionUser.getDottedFieldValue("ClientName.Encumbrance")).booleanValue();
     Log.customer.debug( "lboolADVPro: " + lboolADVPro + "; lboolEncumbrance: " + lboolEncumbrance) ;

     // If this is not a Req for a Procurement enabled client and no set up for encumbrance,
     // skip the rest of the processing
     if ( !(lboolADVPro || lboolEncumbrance))
     {
       Log.customer.debug( "Cannot process LineItem by %s as client is not a Procurement client", ClassName ) ;
       return OrderMethod.cannotProcessLineItem ;
     }

     // eProcure Dev SPL # 268 - VITA OrderMethod updates to ensure that existing DOs and EPs remain the same
     //                          Order Type after a change is made.
     //Srini - For CSPL-633
     //Globalised below variable to also access in else part
     Requisition loReq = (Requisition)item.getLineItemCollection().getDottedFieldValue("PreviousVersion");
     ReqLineItem loRI = null;
     PurchaseOrder loOrder = null;
     if ( item.getDottedFieldValue("OldValues") != null )
     {
       Log.customer.debug( "Processing a ChangeOrder") ;
       int liNum = ((Integer)item.getDottedFieldValue("NumberInCollection")).intValue();
       loRI = (ReqLineItem)loReq.getLineItem(liNum);
       loOrder = (PurchaseOrder)loRI.getDottedFieldValue("Order");
       if (loOrder instanceof ariba.purchasing.core.DirectOrder) {
           //Log.customer.debug( "Cannot process LineItem by %s as original Order is not an ERPOrder", OrderingMethod );
           return OrderMethod.cannotProcessLineItem ;
       }
     }
     //Srini - Code starts for CSPL-633
     else{
		 Log.customer.debug( "BuysensePOERPCC Code start for new else");
		 TransientData tData = Base.getSession().getTransientData();
		 String sReqNumber = (String) tData.get("CGIReqNumber");
		 Log.customer.debug( "BuysensePOERPCC::sReqNumber "+sReqNumber);
		 Log.customer.debug( "BuysensePOERPCC::new req UniqueName "+item.getLineItemCollection().getUniqueName());

		 if(loReq != null  && ((StringUtil.nullOrEmptyOrBlankString(sReqNumber)) || !sReqNumber.equalsIgnoreCase(item.getLineItemCollection().getUniqueName()))){
			 BaseVector lineItemVector = loReq.getLineItems();
	         for(int i=0;i<lineItemVector.size();i++){
	            loRI = (ReqLineItem)lineItemVector.get(i);
	            loOrder = (PurchaseOrder)loRI.getFieldValue("Order");
	            Log.customer.debug( "BuysensePOERPCC::Executed for line # "+i);
		        if (loOrder instanceof ariba.purchasing.core.DirectOrder) {
	            	tData.put("CGIReqNumber", item.getLineItemCollection().getUniqueName());
	            	Log.customer.debug( "BuysensePOERPCC:: cannotProcessLineItem for line # " +i+ "and for Order " +loOrder.getUniqueName());
	            	return OrderMethod.cannotProcessLineItem;
				}
       		 }
		 }else if(loReq != null && sReqNumber.equalsIgnoreCase(item.getLineItemCollection().getUniqueName())){
			 return OrderMethod.cannotProcessLineItem;
		 }
	  }//Srini - Code ends for CSPL-633

     //Srini: Commented below lines for CSPL-926
     //if (AribaERPOrderMethod.canProcessLineItemAsERPCC(item) == 1)
     // 	return 1;
     //else
	 return OrderMethod.canProcessLineItem;
    }


    /**
        Calls OrderUtil.genOrderUniqueName() to increment the order uniquename if necessary

        @param foReq the Requisition in question
        @return List of Orders that were generated for this Requisition
    */
    public List endProcessingRequisition (Requisition foReq)
      throws OrderMethodException
    {
       Log.customer.debug("Inside BuysensePOERPCC endProcessingRequisition");

       VersionedOrderUtil loVersionedOrderUtil = new VersionedOrderUtil();
       List lvOrders = super.endProcessingRequisition(foReq);

       if( lvOrders.size() > 0)
       {
       String lsPOID2 = (String) ((PurchaseOrder) lvOrders.get(0)).getFieldValue("OrderID");
         Log.customer.debug("rlee POERPCC lsPOID2 = " + lsPOID2);
       }

       if ( ((String)foReq.getFieldValue("UniqueName")).startsWith(PREFIX) )
       {
           Log.customer.debug("BuysensePOERPCC - PO is a result of a POB");
           loVersionedOrderUtil.genOrderUniqueName(foReq, lvOrders);
       }
       else
       {
           Log.customer.debug("ERROR - POB does not start with " + PREFIX);
       }

       return lvOrders;
    }

    /**
	        Provides a default implementation which returns true if the
	        parent's method returns true and the two line items have the
	        same currency.
	        <p>
	        This has the effect of putting all the line items with the
	        same currency in the same order.
	        <p>
	        If subclasses want a different behavior, they should override
	        this method.

	        @param reqLineItem  reqLineItem to be put in a purchase order
	        @param poLineItem   poLineItem from a purchase order which is used for
	                            comparison with the ReqLineItem
	        @return boolean showing whether the two line items belong to the
	                        same purchase order
	*/
	    public boolean canAggregateLineItems (ReqLineItem reqLineItem,
	                                          POLineItem  poLineItem)
	      throws OrderMethodException
	    {
	        if (super.canAggregateLineItems(reqLineItem, poLineItem) == false) {
	            return false;
	        }

	        Currency firstCur = reqLineItem.getDescription().getPrice().getCurrency();
	        Currency nextCur = poLineItem.getDescription().getPrice().getCurrency();

	        if ((firstCur == null) || (nextCur == null)) {
	            return false;
	        }

	        if (firstCur.equals(nextCur)) {
	            return true;
	        }

	        return false;
    }

    /**
        addRecipients adds the recipients defined by the
        class variables OrderSenderClassName and
        OrderFormatterClassName.

        Subclasses of AribaOrderMethod should override this method if
        they do not desire the default implementation.
    */

    /**
    	Srini: This is the only main method modified for CSPL-735 to combine both ORDER METHODS.
    	Based on category we will set OrderMethodCategory and it will take cares for ERP and ERPCC
    */
    protected void addRecipients (PurchaseOrder order)
      throws OrderMethodException
    {
		String sASNId = (String)order.getSupplierLocation().getDottedFieldValue("AribaNetworkId");
		String sPreferedOrdMethod = (String)order.getSupplierLocation().getPreferredOrderingMethod();
		if (useReqOrPOIntegration(order.getPartition())&& ((sASNId != null && sPreferedOrdMethod.equals("URL")) ||
				sPreferedOrdMethod.equals("AribaEmail") || sPreferedOrdMethod.equals("AribaFax"))) 
		{
			Log.customer.debug("BuysensePOERPCC executing ERPCC recepient");
			order.setOrderMethodCategory(PurchaseOrder.ERPCCCategory);
			addRecipient(order, OrderingMethodERPCC);
		}
		else if (useReqOrPOIntegration(order.getPartition()))
		{
			Log.customer.debug("BuysensePOERPCC executing ERP recepient");
			order.setOrderMethodCategory(PurchaseOrder.ERPCategory);
            addRecipient(order, OrderingMethodERP);
        }else
        {
        	Log.fixme.warning(1583, ClassName);
        }
	}

	public BuysensePOERPCC()
    {
    }
}



