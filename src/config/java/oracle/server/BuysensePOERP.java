/*


    Responsible: imohideen
*/

/*
    ARIBA CUSTOM JAVA CLASS


*/

package config.java.oracle.server;

// Ariba 8.1: Added package for the Base class.
import ariba.base.core.Base;
// Ariba 8.1: Changed package for Currency and Money classes.
import ariba.basic.core.Currency;
import ariba.basic.core.Money;
import ariba.procure.core.Log;
import ariba.purchasing.core.POLineItem;
import ariba.purchasing.core.PurchaseOrder;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
// Ariba 8.1: Changed packages for the OrderMethod classes.
import ariba.purchasing.ordering.AribaERPOrderMethod;
import ariba.purchasing.core.ordering.OrderMethod;
import ariba.purchasing.core.ordering.OrderMethodException;
import java.util.List;


/**


    This class defines an ordering method to be used with the Oracle
    ERP.  This ordering method handles the breaking of requisitions
    into Purchase Orders by subclassing the AribaERPOrderMethod.

    Once reqs have been broken up into the appropriate POs, they are
    sent to the OraclePOERPFormatter and AribaPOERPSender.  These
    classes are responsible for formatting the Orders in the ways
    needed by Oracle Financials and for sending the POs to the ERP
    system, respectively.
*/
/* 03/02/2004: Globally change Log.util.debug to Log.customer.debug (Rob Giesen) */
public class BuysensePOERP extends AribaERPOrderMethod
{
    public static final String ClassName =
        "ariba.oracle.server.BuysensePOERP";

    /**Buysense 06/27 Added this sender here. Not sure whether this is the right place */
    protected String OrderSenderClassName    =
        "config.java.oracle.server.BuysensePOERPSender";

    protected String OrderingMethod = "BuysensePOERP";

    //e2e
    private static final String  PROCUREMENT = "Procurement" ;
    //e2e

    private static final String PREFIX = "E2E:";

    /**
        canProcessLineItem signals if this ordering method can handle the
        line item.

        @param item the item in question

        @return OrderMethod.canProcessLineItem or
        OrderMethod.cannotProcessLineItem
    */
    // Ariba 8.1 Update - Go after ADVPro field to determine ADV procurement enablement and
    // comment out encumbrance logic which would be in AMSPOPrep.java.
    public int canProcessLineItem (ReqLineItem item)
      throws OrderMethodException
    {
     boolean encumbrance = false;
     boolean lboolADVPro = false;
     boolean lboolEncumbrance = false;

     //Ariba 8.1: Added logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
     //Partitioned User to obtain the extrinsic field values.
     ariba.user.core.User RequesterUser = (ariba.user.core.User)item.getLineItemCollection().getDottedFieldValue("Requester");
     ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());

     //Ariba 8.1: Modified logic to get the extrinsic field value from the new RequesterPartitionUser object.
     lboolADVPro = ((Boolean)RequesterPartitionUser.getDottedFieldValue("ClientName.ADVPro")).booleanValue();
     lboolEncumbrance = ((Boolean)RequesterPartitionUser.getDottedFieldValue("ClientName.Encumbrance")).booleanValue();
     Log.customer.debug( "lboolADVPro: " + lboolADVPro + "; lboolEncumbrance: " + lboolEncumbrance) ;

     // If this is not a Req for a Procurement enabled client and no set up for encumbrance,
     // skip the rest of the processing
     if ( !(lboolADVPro || lboolEncumbrance))
     {
       Log.customer.debug( "Cannot process LineItem by %s as client is not a Procurement client", OrderingMethod ) ;
       return OrderMethod.cannotProcessLineItem ;
     }

     // eProcure Dev SPL # 268 - VITA OrderMethod updates to ensure that existing DOs and EPs remain the same
     //                          Order Type after a change is made.
     if ( item.getDottedFieldValue("OldValues") != null )
     {
       Log.customer.debug( "Processing a ChangeOrder") ;
       int liNum = ((Integer)item.getDottedFieldValue("NumberInCollection")).intValue();
       Requisition loReq = (Requisition)item.getLineItemCollection().getDottedFieldValue("PreviousVersion");
       ReqLineItem loRI = (ReqLineItem)loReq.getLineItem(liNum);
       PurchaseOrder loOrder = (PurchaseOrder)loRI.getDottedFieldValue("Order");
       if (loOrder instanceof ariba.purchasing.core.DirectOrder) {
           Log.customer.debug( "Cannot process LineItem by %s as original Order is not an ERPOrder", OrderingMethod );
           return OrderMethod.cannotProcessLineItem ;
       }
     }
     //e2e

     //WA Dev SPL 156 - We send it to ERP only if the Encumbrance Flag is set to true
     //Ariba 8.1: Modified logic to get the extrinsic field value from the new RequesterPartitionUser object.
     // Ariba 8.1 Update - comment out encumbrance logic.
     /*
     encumbrance = ((Boolean)RequesterPartitionUser.
                        getDottedFieldValue("BuysenseOrg.ClientName.Encumbrance")).booleanValue();
     Log.customer.debug("Encumbrance flag in BuysensePOERP: %s",new Boolean(encumbrance));
     if (!encumbrance){
        return OrderMethod.cannotProcessLineItem ;
     }
     */

     return OrderMethod.canProcessLineItem;

     }



    /**
        addRecipients adds the recipients defined by the
        class variables OrderSenderClassName and
        OrderFormatterClassName.

        Subclasses of AribaOrderMethod should override this method if
        they do not desire the default implementation.
    */
    protected void addRecipients (PurchaseOrder order)
      throws OrderMethodException
    {
        if (useReqOrPOIntegration(order.getPartition())) {
            Log.customer.debug("Buysense: Hit the addRecipients in BuysensePOERP for %s",order);
            addRecipient(order, OrderingMethod);
        }
        else {
            Log.fixme.warning(1583, ClassName);
        }
    }

    /**
        Provides a default implementation which returns true if the
        parent's method returns true and the two line items have the
        same currency.
        <p>
        This has the effect of putting all the line items with the
        same currency in the same order.
        <p>
        If subclasses want a different behavior, they should override
        this method.

        @param reqLineItem  reqLineItem to be put in a purchase order
        @param poLineItem   poLineItem from a purchase order which is used for
                            comparison with the ReqLineItem
        @return boolean showing whether the two line items belong to the
                        same purchase order
    */
    public boolean canAggregateLineItems (ReqLineItem reqLineItem,
                                          POLineItem  poLineItem)
      throws OrderMethodException
    {
        if (super.canAggregateLineItems(reqLineItem, poLineItem) == false) {
            return false;
        }

        Currency firstCur = reqLineItem.getDescription().getPrice().getCurrency();
        Currency nextCur = poLineItem.getDescription().getPrice().getCurrency();

        if ((firstCur == null) || (nextCur == null)) {
            return false;
        }

        if (firstCur.equals(nextCur)) {
            return true;
        }

        return false;
    }


    public List endCancelingRequisition (Requisition req)
      throws OrderMethodException
    {
        Log.customer.debug("BS: endCanceling got called in BuysensePOERP");
       /* Permission pa =
            Permission.getPermission(PermissionRequisitionCancelEmail);
        String msg = ResourceService.getString(StringTable,
                                            CancelObjectEmailStringKey);
        Notifications.sendMail(
            pa.users(req.getPartition()),
            req,
            Notifications.RequisitionCancel,
            msg,
            msg,
            "Content/procure/notifications/canceled.htm"); */
        List testVector =  super.endCancelingRequisition(req);
        Log.customer.debug("Return List from endCancelling in BuysensePOERP: %s",testVector);
        return testVector;
    }

    /**
        Calls OrderUtil.genOrderUniqueName() to increment the order uniquename if necessary 

        @param foReq the Requisition in question
        @return List of Orders that were generated for this Requisition
    */
    public List endProcessingRequisition (Requisition foReq)
      throws OrderMethodException
    {
       Log.customer.debug("inside BuysensePOERP endProcessingRequisition");

       VersionedOrderUtil loVersionedOrderUtil = new VersionedOrderUtil();
       List lvOrders = super.endProcessingRequisition(foReq); 
         Log.customer.debug("rlee POERP lvOrders = "+ lvOrders);
       if( lvOrders.size() > 0)
       {
       String lsPOID2 = (String) ((PurchaseOrder) lvOrders.get(0)).getFieldValue("OrderID");
         Log.customer.debug("rlee POERP lsPOID2 = " + lsPOID2);
       }


       if ( ((String)foReq.getFieldValue("UniqueName")).startsWith(PREFIX) ) 
       {
           Log.customer.debug("BuysensePOERP - PO is a result of a POB"); 
           loVersionedOrderUtil.genOrderUniqueName(foReq, lvOrders); 
       }
       else 
       {
           Log.customer.debug("ERROR - POB does not start with " + PREFIX);
       }

       return lvOrders;
    }
}
