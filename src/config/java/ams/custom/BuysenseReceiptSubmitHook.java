package config.java.ams.custom;

import java.util.List;

import ariba.admin.core.Log;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableHook;
import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.util.core.Constants;
import ariba.util.core.Date;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;
import ariba.basic.core.Money;
import ariba.user.core.*;

public class BuysenseReceiptSubmitHook implements ApprovableHook
{
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));
    
    public List run(Approvable approvable)
    {
    	Log.customer.debug("Calling BuysenseReceiptSubmitHook.");
        BaseVector rclines = (BaseVector)approvable.getFieldValue("ReceiptItems");
        Date editDate = (Date)approvable.getFieldValue("BuysenseEditDate");
        Log.customer.debug("the value of BuysenseEditDate" +editDate);
        Date receivedDate = (Date)approvable.getFieldValue("Date");
        Log.customer.debug("the value of receivedDate" +receivedDate);
        Log.customer.debug("the receipts lines." + rclines);
        int rcLines = rclines.size();
        if(rclines!=null)
        for(int i=0;i<rcLines;i++)
        {
            Log.customer.debug("Inside for Loop...# : %s", new Integer(i));
            BaseObject receiptItem = (BaseObject)rclines.get(i);
			Boolean loMassEdit = ((Boolean) receiptItem.getFieldValue("MassEditForLine"));
			Log.customer.debug("the receipts lines." + loMassEdit);
            if(editDate !=null && !(editDate.equals(receivedDate)))
            {
            	Log.customer.debug("Inside if of date compariosion");
            	return ListUtil.list(Constants.getInteger(-1),"Receive date was updated for lines x, y, z " +
            			"but no items have been received. Please review and make necessary adjustments");
            	
            }
        }
        return NoErrorResult;
 	 }

}

