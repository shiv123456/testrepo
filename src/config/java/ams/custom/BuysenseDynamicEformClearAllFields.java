package config.java.ams.custom;

import java.util.List;
import ariba.approvable.core.Approvable;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseDynamicEformClearAllFields extends Action {
 
    public void fire(ValueSource valuesource, PropertyTable propertytable)
            throws ActionExecutionException {
        Approvable loAppr = null;
        Log.customer.debug("BuysenseDynamicEformClearAllFields called");
        if (valuesource != null && valuesource instanceof Approvable) {
            loAppr = (Approvable) valuesource;
            clearAllEformFields(loAppr);
}
}
    public void clearAllEformFields(Approvable appr)
    {
        Log.customer.debug("Inside clearAllEformFields :");
        String[] eFromFields = { "NIGPDescription","EformAcctNIGPDescription","EformAcctLargeText", "EformAcctLargeTextVal", "EformHeadPckList1", "EformHeadPckList","EformHeadLongTextField", "EformHeadDate1", "EformHeadDate2", "EformHeadDate3",
            "EformHeadPckList2", "EformHeadPckList3", "EformHeadPckList4",
            "EformHeadPckList5", "EformHeadPckList6", "EformHeadPckList7", "EformHeadPckList8","EformHeadLargeText1", "EformHeadLargeText3_1","EformHeadLargeText3_2","EformHeadLargeText3_3","EformHeadLargeText3_1Val","EformHeadLargeText3_2Val","EformHeadLargeText3_3Val", "EformIntegerText1", "EformIntegerText2", "EformIntegerText3", "EformIntegerText4",
            "EformHeadLargeText2", "EformHeadLargeText3", "EformHeadText1","EformHeadText","EformHeadText_1",
            "EformHeadText2", "EformHeadText3", "EformHeadText4",
            "EformHeadText5", "EformHeadCB","EformHeadCB1", "EformHeadCB2", "EformHeadCB3","EformAcctCB1", "EformAcctCB2", "EformAcctCB3", "EformAcctCB4", "EformAcctCB5", "EformAcctCB6", "EformAcctCB7","EformAcctCB8","EformAcctCB9",
            "EformHeadCB4", "EformHeadCB5","EformHeadCB6","EformHeadCB7","EformHeadLongTextField", "EformAcctLongTextField2", "EformAcctLongTextField3",
            "EformHeadLargeText4","EformHeadLargeText4Val","EformHeadGrp1RadioHeader","EformHeadGrp2RadioHeader","EformHeadGrp2RadioVal","EformHeadGrp_1RadioHeader","EformHeadGrp_2RadioHeader","EformHeadGrp_3RadioHeader","EformHeadGrp_4RadioHeader","EformHeadGrp_5RadioHeader","EformHeadGrp_6RadioHeader","EformHeadGrp_7RadioHeader",
            "EformHeadGrp1RadioVal","EformHeadGrp_1RadioHeaderVal","EformHeadGrp_2RadioHeaderVal","EformHeadGrp_3RadioHeaderVal","EformHeadGrp_4RadioHeaderVal","EformHeadGrp_5RadioHeaderVal","EformHeadGrp_6RadioHeaderVal","EformHeadGrp_7RadioHeaderVal","EformAcctLongTextField1", "EformHeadLongTextField1","EformAcctLargeText2Val","EformHeadLargeText1Val","EformHeadLargeText2Val","EformHeadLargeText3Val",
            "EformAcctLongTextField1", "EformMoney1", "EformMoney","EformTable1Field","EformTable1Field1","EformTable1Field1_1",
            "EformTable1Field2","EformTable1Field2_1","EformTable1Field2_2","EformTable1Field2_3", "EformTable1Field3", "EformTable1Field4",
            "EformTable1Field5","EformTable1Field5_1","EformTable1Field5_2", "EformTable1Field6", "EformTable1Field7","EformTable1Field7_1",
            "EformTable1Field8", "EformTable1Field9", "EformTable1Field10",
            "EformTable1Field11", "EformTable1Field12", "EformTable1Field13","EformTable1Field14","EformTable1Field14_1",
            "EformTable2Field", "EformTable2Field1","EformTable2Field1_1","EformTable2Field2","EformTable2Field2_1","EformTable2Field2_2","EformTable2Field2_3", "EformTable2Field3",
            "EformTable2Field4", "EformTable2Field5","EformTable2Field5_1","EformTable2Field5_2", "EformTable2Field6",
            "EformTable2Field7", "EformTable2Field7_1","EformTable2Field8", "EformTable2Field9",
            "EformTable2Field10", "EformTable2Field11", "EformTable2Field12",
            "EformTable2Field13", "EformTable2Field14","EformTable2Field14_1", "EformTable3Field", "EformTable3Field1","EformTable3Field1_1","EformTable3Field2", "EformTable3Field2_1","EformTable3Field2_2","EformTable3Field2_3",
            "EformTable3Field3", "EformTable3Field4", "EformTable3Field5","EformTable3Field5_1","EformTable3Field5_2",
            "EformTable3Field6", "EformTable3Field7","EformTable3Field7_1", "EformTable3Field8",
            "EformTable3Field9", "EformTable3Field10", "EformTable3Field11",
            "EformTable3Field12", "EformTable3Field13", "EformTable3Field14","EformTable3Field14_1", "EformTable4Field","EformTable4Field1","EformTable4Field1_1",
            "EformTable4Field2","EformTable4Field2_1","EformTable4Field2_2","EformTable4Field2_3", "EformTable4Field3", "EformTable4Field4",
            "EformTable4Field5","EformTable4Field5_1", "EformTable4Field6", "EformTable4Field7","EformTable4Field7_1",
            "EformTable4Field8", "EformTable4Field9", "EformTable4Field10",
            "EformTable4Field11", "EformTable4Field12", "EformTable4Field13","EformTable4Field14","EformTable4Field14_1",
            "EformTable5Field", "EformTable5Field1", "EformTable5Field1_1", "EformTable5Field2", "EformTable5Field2_1","EformTable5Field2_2","EformTable5Field2_3","EformTable5Field3",
            "EformTable5Field4", "EformTable5Field5", "EformTable5Field6",
            "EformTable5Field7", "EformTable5Field7_1","EformTable5Field8", "EformTable5Field9",
            "EformTable5Field10", "EformTable5Field11", "EformTable5Field12",
            "EformTable5Field13", "EformTable5Field14", "EformTable5Field14_1","EformTable6Field","EformTable6Field1","EformTable6Field1_1", "EformTable6Field2","EformTable6Field2_1","EformTable6Field2_2","EformTable6Field2_3",
            "EformTable6Field3", "EformTable6Field4", "EformTable6Field5","EformTable6Field5_1","EformTable6Field5_2",
            "EformTable6Field6", "EformTable6Field7", "EformTable6Field8",
            "EformTable6Field9", "EformTable6Field10", "EformTable6Field11",
            "EformTable6Field12", "EformTable6Field13", "EformTable6Field14","EformTable6Field14_1", "EformTable7Field","EformTable7Field1","EformTable7Field1_1",
            "EformTable7Field2","EformTable7Field2_1","EformTable7Field2_2","EformTable7Field2_3", "EformTable7Field3", "EformTable7Field4",
            "EformTable7Field5", "EformTable7Field5_1","EformTable7Field6", "EformTable7Field7","EformTable7Field7_1",
            "EformTable7Field8", "EformTable7Field9", "EformTable7Field10",
            "EformTable7Field11", "EformTable7Field12", "EformTable7Field13","EformTable7Field14","EformTable7Field14_1",
            "EformTable8Field", "EformTable8Field1","EformTable8Field1_1","EformTable8Field2", "EformTable8Field2_1","EformTable8Field2_2","EformTable8Field2_3","EformTable8Field3",
            "EformTable8Field4", "EformTable8Field5","EformTable8Field5_1","EformTable8Field5_2", "EformTable8Field6",
            "EformTable8Field7", "EformTable8Field7_1","EformTable8Field8", "EformTable8Field9",
            "EformTable8Field10", "EformTable8Field11", "EformTable8Field12",
            "EformTable8Field13", "EformTable8Field14","EformTable8Field14_1", "EformTable9Field","EformTable9Field1","EformTable9Field1_1", "EformTable9Field2","EformTable9Field2_1","EformTable9Field2_2","EformTable9Field2_3",
            "EformTable9Field3", "EformTable9Field4", "EformTable9Field5","EformTable9Field5_1","EformTable9Field5_2",
            "EformTable9Field6", "EformTable9Field7", "EformTable9Field7_1","EformTable9Field8",
            "EformTable9Field9", "EformTable9Field10", "EformTable9Field11",
            "EformTable9Field12", "EformTable9Field13", "EformTable9Field14","EformTable9Field14_1", "EformTable10Field","EformTable10Field1","EformTable10Field1_1",
            "EformTable10Field2","EformTable10Field2_1","EformTable10Field2_2","EformTable10Field2_3", "EformTable10Field3", "EformTable10Field4",
            "EformTable10Field5", "EformTable10Field5_1","EformTable10Field5_2","EformTable10Field6", "EformTable10Field7","EformTable10Field7_1",
            "EformTable10Field8", "EformTable10Field9", "EformTable10Field10",
            "EformTable10Field11", "EformTable10Field12","EformHeaderInstruction1","EformHeader","EformHeader3","EformHeaderNote1","EformHeader2","EformHeaderNote2","EformHeaderNote3",
            "EformTable10Field13", "EformTable10Field14","EformTable10Field14_1", "EformAcctPckList1", "EformAcctPckList2",
            "EformAcctPckList3", "EformAcctPckList4", "EformAcctPckList5",
            "EformAcctPckList6", "EformAcctPckList7", "EformAcctPckList8",
            "EformAcctPckList9", "EformAcctPckList10", "EformAcctText1",
            "EformAcctText2", "EformAcctText3", "EformAcctText4","EformAcctText5",
            "EformAcctLargeText1", "EformAcctLargeText1Val","EformAcctLargeText2",
            "EformAcctLargeText3","EformAcctLargeText3Val", "EformAcctLargeText4","EformAcctLargeText4Val",
            "EformAcctLargeText5","EformAcctLargeText5Val", "EformAcctDate1", "EformAcctMoney1","TotalCost"

            };

        for (int i=0; i<eFromFields.length;i++) 
        {
            Log.customer.debug("Setting null for: " +eFromFields[i]);
            appr.setFieldValue(eFromFields[i], null);
            List loNIGPCodes = (List) appr.getFieldValue("NIGPCommodityCodes");
            loNIGPCodes.clear();
            List loEformNIGPCodes = (List) appr.getFieldValue("EformAcctNIGPCommodityCodes");
            loEformNIGPCodes.clear();
        }
}
}