/*
 * @(#)BuyintDBRecExportData.java      2004/09/03
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information"). You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 * $Log:   R:\Software\development\archives\Ariba 8.x Customizations\app\buyer\Server\classes\extensions\config\java\ams\custom\BuyintDBRecExportData.java-arc  $
 *
 *    Rev 1.6   23 Mar 2006 11:28:36   rlee
 * Dev SPL 629 - INT - Add REQUISITION_NUMBER Column to EXPORT_TABLE.
 *
 *    Rev 1.5   13 Mar 2006 16:44:26   rlee
 * Dev SPL 629 - INT - Add REQUISITION_NUMBER Column to EXPORT_TABLE.
 *
 *    Rev 1.4   01 Jul 2005 19:08:34   dchamber
 * VEPI DEV SPL#576: Modified the class for re-structure
 *
 *    Rev 1.3   16 Jun 2005 17:14:48   dchamber
 * VEPI DEV SPL 576: Modified the save and methods to set the new 'ClientName' field in the Export_Data table.
 */

package config.java.ams.custom;

import java.math.*;
import java.sql.*;
import java.util.*;

import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.util.log.Log;
public class BuyintDBRecExportData extends BuyintDBRec
{
    private static final String EXPORT_DATA = "EXPORT_DATA";
    protected static final String COL_CLIENTNAME = "CLIENTNAME";
    protected static final String COL_REQ_NUMBER = "REQ_NUMBER";
    protected static final String COL_REQ_VERSION = "REQ_VERSION";
    protected static final String COL_PO_NUMBER = "PO_NUMBER";
    protected static final String COL_PO_VERSION = "PO_VERSION";
    protected static final String COL_TOTALCOST = "TOTALCOST";

    public BuyintDBRecExportData(BuyintDBIO foDBIO)
    {
        super(foDBIO, EXPORT_DATA);
    }

    public BuyintDBRecExportData(BuyintDBIO foDBIO, String foTable)
    {
        super(foDBIO, foTable);
    }

    public BuyintDBRecExportData(BuyintDBIO foDBIO, Connection foConn)
    {
        super(foDBIO, foConn, EXPORT_DATA);
    }

    protected static String getSeqSQL()
    {
        return "SELECT SEQ_EXPORT_DATA_TIN.nextval from DUAL";
    }

   /**
    * Retrieves multiple recs based on the passed where clause,
    * upto the max specified. If no max is specified, all qualifying
    * records are returned.
    *
    */
    public BuyintDBRecExportData[] getRecs(String fsWhereClause, int fiMax)
    {
        return getRecs(fsWhereClause, fiMax, "TIN");
    }

   /**
    * Retrieves multiple records based on where clause passed
    *
    */
    public BuyintDBRecExportData[] getRecs(String fsWhereClause, int fiMax, String fsOrderBy)
    {
        Statement loStmt = null;
        ResultSet loResult = null;
        String lsQuery = null;
        BuyintDBRecExportData[] loArray = null;

        try
        {
            this.checkConnection();
            lsQuery = getSelectString(msTableName) + fsWhereClause;

            if (fiMax > 0)
            {
               lsQuery += " and rownum <= " + String.valueOf(fiMax);
            }

            if (fsOrderBy != null)
            {
               lsQuery += " order by " + fsOrderBy;
            }

            debug("About to execute query: " + lsQuery);
            loStmt = moConn.createStatement();
            loResult = loStmt.executeQuery(lsQuery);
            loArray = getResultSetAsInfoArray(loResult);
            debug("Query execution complete ... returned array of length " + loArray.length);
        }
        catch(SQLException loEx1)
        {
            loEx1.printStackTrace();
            // no point throwing exception or calling ExceptionHandler at this time
        }
        finally
        {
           try
           {
               if (loStmt != null)
               {
                  loStmt.close();
               }
               if (loResult != null)
               {
                  loResult.close();
               }
           }
           catch (SQLException loEx2)
           {
              // nothing to do
           }
        }
        return loArray;
    }

    /**
     * Retrieves multiple records with direct SQL Query
     *
     */
     public BuyintDBRecExportData[] getRecs(String fsSQL)
     {
         Statement loStmt = null;
         ResultSet loResult = null;
         BuyintDBRecExportData[] loArray = null;

         try
         {
             this.checkConnection();

             debug("getRecs::about to execute query: " +fsSQL);
             loStmt = moConn.createStatement();
             loResult = loStmt.executeQuery(fsSQL);
             loArray = getResultSetAsInfoArray(loResult);
             debug("getRecs::query execution complete ... returned array of length " + loArray.length);
         }
         catch(SQLException loEx1)
         {
             loEx1.printStackTrace();
             // no point throwing exception or calling ExceptionHandler at this time
         }
         finally
         {
            try
            {
                if (loStmt != null)
                {
                   loStmt.close();
                }
                if (loResult != null)
                {
                   loResult.close();
                }
            }
            catch (SQLException loEx2)
            {
               // nothing to do
            }
         }
         return loArray;
     }
    
    /**
     * Returns the ResultSet as an Info Array. If ResultSet is empty, returns null
     *
     * @param rs ResultSet to be converted
     * @return BuyintDBRecExportData[]
     */
    public BuyintDBRecExportData[] getResultSetAsInfoArray(ResultSet results) throws SQLException {
        Vector vInf = getResultSetAsInfoVector(results);
        int elements = vInf.size();
        if (elements > 0) {
            BuyintDBRecExportData[] infoArray = new BuyintDBRecExportData[elements];
            for (int i=0; i < elements ; i++)
            {
                infoArray[i]= (BuyintDBRecExportData)vInf.elementAt(i);
            }
            return infoArray;
        }
        else
        {
            return new BuyintDBRecExportData[0];
        }
    }
   /**
    * Returns the ResultSet as a Vector of BuyintDBRec.
    *
    * @param rs ResultSet to be converted
    * @return Vector
    */
    public Vector getResultSetAsInfoVector (ResultSet results) throws SQLException {
        ResultSetMetaData rsMeta = results.getMetaData();
        Vector vInf = new Vector();
        BuyintDBRecExportData inf;

        int cols = rsMeta.getColumnCount();          // initialize nbr of columns
        String colsValue, colsName;
        while (results.next())
        {
            inf = new BuyintDBRecExportData(moDBIO, moConn);
            for (int j=1; j <= cols; j++)
            {   // database column identifier is 1-based
                colsName = rsMeta.getColumnLabel(j);
                colsValue = getColValueAsString(results, rsMeta, j);
                inf.setValue(colsName,colsValue);
            }
            vInf.addElement(inf);
        }
        return vInf;
    }

    protected String getSelectString(String fsTableName)
    {
        return "SELECT TIN, DOCUMENT_TYPE, DOCUMENT_ID, TRANSACTION_TYPE, STATUS, "
               + " TRANSACTION_DATA, ERROR_MESSAGE, RETRY_COUNT, CREATION_DATE, "
               + " PROCESS_DATE, CLIENTNAME, REQ_NUMBER, REQ_VERSION, PO_NUMBER, PO_VERSION, TOTALCOST "
               + " FROM " + fsTableName + " ";
    }

    protected String getSelectByKey(String fsTableName)
    {
        return "SELECT TIN, DOCUMENT_TYPE, DOCUMENT_ID, TRANSACTION_TYPE, STATUS, "
               + " TRANSACTION_DATA, ERROR_MESSAGE, RETRY_COUNT, CREATION_DATE, "
               + " PROCESS_DATE, CLIENTNAME, REQ_NUMBER, REQ_VERSION, PO_NUMBER, PO_VERSION, TOTALCOST "
               + " FROM " + fsTableName
               + " WHERE DOCUMENT_TYPE = ? AND DOCUMENT_ID = ? AND TRANSACTION_TYPE = ? "
               + " AND (STATUS = ? OR STATUS = ? OR STATUS = ? OR STATUS = ? OR STATUS = ?)"
               + " ORDER BY TIN";
    }

    protected String getSelectByStatus(String fsTableName)
    {
        return "SELECT TIN, DOCUMENT_TYPE, DOCUMENT_ID, TRANSACTION_TYPE, STATUS, "
               + " TRANSACTION_DATA, ERROR_MESSAGE, RETRY_COUNT, CREATION_DATE, "
               + " PROCESS_DATE, CLIENTNAME, REQ_NUMBER, REQ_VERSION, PO_NUMBER, PO_VERSION, TOTALCOST "
               + " FROM " + fsTableName
               + " WHERE (STATUS = ? OR STATUS = ? OR STATUS = ? OR STATUS = ? OR STATUS = ?)";
    }

    protected String getSelectByTin(String fsTableName, long flTIN)
    {
        return "SELECT TIN, DOCUMENT_TYPE, DOCUMENT_ID, TRANSACTION_TYPE, STATUS, "
               + " TRANSACTION_DATA, ERROR_MESSAGE, RETRY_COUNT, CREATION_DATE, "
               + " PROCESS_DATE, CLIENTNAME, REQ_NUMBER, REQ_VERSION, PO_NUMBER, PO_VERSION, TOTALCOST "
               + " FROM " + fsTableName
               + " WHERE TIN = " + String.valueOf(flTIN);
    }

    protected String getInsert(String fsTableName)
    {
        return "INSERT INTO " + fsTableName
               + "(TIN, DOCUMENT_TYPE, DOCUMENT_ID, TRANSACTION_TYPE, STATUS, "
               + " TRANSACTION_DATA, ERROR_MESSAGE, RETRY_COUNT, CREATION_DATE, "
               + " PROCESS_DATE, CLIENTNAME, REQ_NUMBER, REQ_VERSION, PO_NUMBER, PO_VERSION, TOTALCOST, BATCH_INT)"
               + " VALUES (?, ?, ?, ?, ?, EMPTY_CLOB(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    }

    protected String getUpdateByTin(String fsTableName)
    {
        return "UPDATE " + fsTableName + " SET "
               + " STATUS = ?, ERROR_MESSAGE = ?, RETRY_COUNT = ?, PROCESS_DATE = ?, CLIENTNAME = ?"
               + " WHERE TIN = ?";

    }

    /** Gets the ClientName of the current record .
    * @param ClientName
    */
    protected String getClientName()
    {
        return getValue(COL_CLIENTNAME);
    }

    /** Sets the ClientName of the current record .
    * @param fsClientName The current Client Name.
    */
    protected void setClientName(String fsClientName)
    {
        setValue(COL_CLIENTNAME, fsClientName);
    }

    /** Gets and Sets the ReqNumber of the current record .
    * @param ReqNumber
    */
    protected String getReqNumber()
    {
        return getValue(COL_REQ_NUMBER);
    }
    protected void setReqNumber(String fsReqNumber)
    {
        setValue(COL_REQ_NUMBER, fsReqNumber);
    }

    /** Gets and sets the PONumber of the current record .
    * @param PONumber
    */
    protected String getPONumber()
    {
        return getValue(COL_PO_NUMBER);
    }
    protected void setPONumber(String fsPONumber)
    {
        setValue(COL_PO_NUMBER, fsPONumber);
    }

    /** Gets and Sets the ReqVersion of the current record .
    * @param ReqVersion
    */
    protected String getReqVersion()
    {
        return getValue(COL_REQ_VERSION);
    }
    protected void setReqVersion(int fiReqVersion)
    {
        setValue(COL_REQ_VERSION, String.valueOf(fiReqVersion));
    }
    protected void setReqVersion(String fsReqVersion)
    {
        setValue(COL_REQ_VERSION, fsReqVersion);
    }

    /** Gets and Sets the POVersion of the current record .
    * @param POVersion
    */
    protected String getPOVersion()
    {
        return getValue(COL_PO_VERSION);
    }
    protected void setPOVersion(int fiPOVersion)
    {
        setValue(COL_PO_VERSION, String.valueOf(fiPOVersion));
    }
    protected void setPOVersion(String fsPOVersion)
    {
        setValue(COL_PO_VERSION, fsPOVersion);
    }

    /** Gets and Sets the TotalCost of the current record .
    * @param TotalCost
    */
    protected BigDecimal getTotalCost()
    {
        return getBigDecimalValue(COL_TOTALCOST, new BigDecimal(0));
    }
    protected void setTotalCost(java.math.BigDecimal fdTotalCost)
    {
        setValue(COL_TOTALCOST, fdTotalCost.toString());
    }
    protected String getBatchINT()
    {
        String lsBatchINT = "No";
        String sQuery = "SELECT BatchINT from ariba.core.BuysenseClient where ClientName = '"+getClientName()+"'";
        Log.customer.debug("sQuery: " +sQuery);
        try
        {
        AQLQuery aqlQuery = AQLQuery.parseQuery(sQuery);
        Partition partition = Base.getService().getPartition("pcsv");
        AQLOptions aqlOptions = new AQLOptions(partition);
        AQLResultCollection aqlResults = Base.getService().executeQuery(aqlQuery, aqlOptions);
        if (aqlResults.getErrors() == null)
        {
            while (aqlResults.next())
            {
                Boolean lbBatchINT = aqlResults.getBoolean(0);
                if(lbBatchINT.toString().trim().equals("true"))
                {
                    lsBatchINT = "Yes";
                }
                
            } 
        }
        return lsBatchINT;
        
    } catch(Exception e)
    {
        Log.customer.warning(8000,"%s - Exception in AQL execution -%s", e.getMessage());
        return lsBatchINT;
    }
    }

    /**
    * Sets the prepared statment with the columns to be inserted
    * This method overrides the super class
    */
    protected void setInsertColumns() throws BuyintException
    {
        try
        {
            super.setInsertColumns();
            moPrep.setString(++miCol, getClientName());
            moPrep.setString(++miCol, getReqNumber());
            moPrep.setString(++miCol, getReqVersion());
            moPrep.setString(++miCol, getPONumber());
            moPrep.setString(++miCol, getPOVersion());
            moPrep.setBigDecimal(++miCol, getTotalCost());
            moPrep.setString(++miCol, getBatchINT());            
        }
        catch(SQLException loEx1)
        {
            loEx1.printStackTrace();
            throw new BuyintException("Error during insert", BuyintException.EXCPT_DBIO_ERROR);
        }
    }

    /**
    * Sets the prepared statment with the columns to be updated
    * This method overrides the super class
    */
    protected void setUpdateColumns() throws BuyintException
    {
        try
        {
            super.setUpdateColumns();
            //Need to put clientname before the tin because the update statement
            //in getUpdateByTin() method requires TIN to be the last parameter

            moPrep.setString(miCol, getClientName());
            moPrep.setLong(++miCol, getTIN());
        }
        catch(SQLException loEx1)
        {
            loEx1.printStackTrace();
            throw new BuyintException("Error during insert", BuyintException.EXCPT_DBIO_ERROR);
        }
    }

}
