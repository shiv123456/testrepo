package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.BaseVector;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseResetVMIAssetTrackingFields extends Action
{
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
    	Log.customer.debug("***%s***Called fire BuysenseResetUserProfileFields:: valuesource obj %s", valuesource);
    	Approvable loAppr = null;
        String lsFormAction = null;
        BaseVector bvDepartment = null;
        
        if(valuesource!= null && valuesource instanceof Approvable)
        {
           loAppr = (Approvable)valuesource;
           lsFormAction = (String) loAppr.getFieldValue("FormAction");
           if(lsFormAction != null && lsFormAction.equalsIgnoreCase("Lost/Stolen (if stolen, scan and attach Police Report)"))
           {
        	   loAppr.setFieldValue("Sub1dept", null);
        	   loAppr.setFieldValue("Sub1building", null);
        	   loAppr.setFieldValue("Sub1room", null);
        	   loAppr.setFieldValue("Sub2Radio", null);
        	   loAppr.setFieldValue("Sub2HDD", null);
        	   loAppr.setFieldValue("Sub2memory", null);
        	   loAppr.setFieldValue("Sub2Auth", null);
        	   loAppr.setFieldValue("Sub3checkbox1", null);
        	   loAppr.setFieldValue("Sub3checkbox2", null);
        	   loAppr.setFieldValue("Sub3checkbox3", null);
        	   loAppr.setFieldValue("Sub3checkbox4", null);
        	   loAppr.setFieldValue("Sub3checkbox5", null);
           }
           if(lsFormAction != null && lsFormAction.equalsIgnoreCase("To Surplus Property Warehouse"))
           {
        	   loAppr.setFieldValue("Sub1dept", null);
        	   loAppr.setFieldValue("Sub1building", null);
        	   loAppr.setFieldValue("Sub1room", null);        	   
           } 
           if(lsFormAction != null && lsFormAction.equalsIgnoreCase("To Another VMI Department"))
           {
        	   loAppr.setFieldValue("Sub3checkbox1", null);
        	   loAppr.setFieldValue("Sub3checkbox2", null);
        	   loAppr.setFieldValue("Sub3checkbox3", null);
        	   loAppr.setFieldValue("Sub3checkbox4", null);
        	   loAppr.setFieldValue("Sub3checkbox5", null);
           } 
           if((loAppr.getDottedFieldValue("ItemITrelated1") != null && !((Boolean)loAppr.getFieldValue("ItemITrelated1")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated2") != null && !((Boolean)loAppr.getFieldValue("ItemITrelated2")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated3") != null && !((Boolean)loAppr.getFieldValue("ItemITrelated3")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated4") != null && !((Boolean)loAppr.getFieldValue("ItemITrelated4")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated5") != null && !((Boolean)loAppr.getFieldValue("ItemITrelated5")).booleanValue()))
           {
        	   loAppr.setFieldValue("Sub2Radio", null);
        	   loAppr.setFieldValue("Sub2HDD", null);
        	   loAppr.setFieldValue("Sub2memory", null);
        	   loAppr.setFieldValue("Sub2Auth", null);        	   
        }
    }
    }

}
