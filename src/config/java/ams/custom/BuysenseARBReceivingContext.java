package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.htmlui.approvableui.wizards.ARBApprovableContext;
import ariba.htmlui.procure.receiving.wizards.ARBReceivingContext;
import ariba.util.core.Date;
import ariba.util.log.Log;
import ariba.receiving.core.ReceiptCoreApprovable;
import ariba.receiving.core.ReceiptItem;
import ariba.receiving.core.ReceivableLineItemCollection;
import ariba.search.core.SavedSearch;
import ariba.search.core.SearchExpression;
import ariba.search.core.SearchTerm;
import ariba.ui.aribaweb.core.AWComponent;
import ariba.user.core.Role;
import ariba.user.core.User;
import ariba.user.core.UserPreferencesUtil;
import ariba.util.core.Assert;
import ariba.util.core.Constants;
import ariba.util.core.FastStringBuffer;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;
import ariba.util.log.Logger;
import java.math.BigDecimal;
import java.util.List;

public class BuysenseARBReceivingContext extends ARBReceivingContext {
	
       public Approvable getApprovableContext()
	    {
		  Log.customer.debug("Inside before: getApprovableContext");
		  Approvable m_approvableContext = (Approvable)getApprovableContext();
		  Log.customer.debug("Inside after: getApprovableContext");
	        return m_approvableContext;
	    }
	
public String getReviewScreenDetails() {
        Log.customer.debug("Inside getReviewScreenDetails");
        String sHint = "Shiva";
        Approvable oReceipt = (Approvable) getApprovableContext();
        if (((BaseObject) oReceipt).instanceOf("ariba.receiving.core.Receipt"))
        {
            Date oDate = (Date) oReceipt.getDottedFieldValue("BuysenseEditDate");
            if (oDate != null)
            {
                sHint = "Shiva";
            }
        }
        return sHint;
	}
   

}
