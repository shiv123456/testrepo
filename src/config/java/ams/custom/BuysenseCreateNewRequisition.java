package config.java.ams.custom;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import ariba.approvable.core.Approvable;
import ariba.approvable.core.Comment;
import ariba.approvable.core.LineItem;
import ariba.approvable.core.LineItemCollection;
import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.core.Partition;
import ariba.base.core.TransientData;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.FieldProperties;
import ariba.base.fields.ValueSource;
import ariba.basic.core.Currency;
import ariba.basic.core.Money;
import ariba.procure.core.LineItemProductDescription;
import ariba.procure.core.ProcureLineItemCollection;
import ariba.purchasing.core.Requisition;
import ariba.user.core.Approver;
import ariba.user.core.Role;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.util.formatter.BigDecimalFormatter;
import ariba.util.log.Log;
import config.java.ams.custom.BuysenseCopyFields;

public class BuysenseCreateNewRequisition extends Action
{
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        Log.customer.debug("***%s***Called fire BuysenseCreateNewRequisition:: valuesource obj %s " +valuesource);

        Approvable loAppr = null;
        Requisition loReq = null;
        if(valuesource!= null && valuesource instanceof Approvable)
        {
        	 loAppr = (Approvable)valuesource;
        	 loReq = createRequisition(loAppr);
        }
    }

    public Requisition createRequisition(Approvable loAppr)
    {

        Requisition loReq = null;
        BaseObject sourceObject = null;
        BaseObject sourceObject1 = null;
        String lsFieldValues = null;
        ariba.user.core.User loUser;
        BaseObject loBaseObj = null ;
        List llTableNumbers = ListUtil.list();
        LineItemProductDescription LiProdDesc= null;
        String lsLineDetails = "Line Item Details" + "\n";
        lsLineDetails = lsLineDetails + "--------------" + "\n" ;
  	    TransientData loTransientData =  Base.getSession().getTransientData();
  		loTransientData.put("EformToRequisition", "Yes");
  		Log.customer.debug("BuysenseCreateNewRequisition::fire::EformToRequisition %s", loTransientData.get("EformToRequisition"));

	    ariba.user.core.User RequesterUser = (ariba.user.core.User)loAppr.getDottedFieldValue("Requester");
	    Partition appPartition = loAppr.getPartition();
	    ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,appPartition);
	    sourceObject = (BaseObject)RequesterPartitionUser.getFieldValue("BuysenseOrg");

        lsFieldValues = (String)loAppr.getFieldValue("UniqueName");

        lsFieldValues  = lsFieldValues + "," +(String)loAppr.getFieldValue("FormIdentifier");

        //lsFieldValues  = lsFieldValues + "," +(String)loAppr.getFieldValue("Price");
        Log.customer.debug("***%s***BuysenseCreateNewRequisition:: The value of  lsFieldValues is " +lsFieldValues);

        loReq = (Requisition)BaseObject.create("ariba.purchasing.core.Requisition", loAppr.getPartition());
        Log.customer.debug("***%s***BuysenseCreateNewRequisition:: The new requisition object created is " +loReq);

        loReq.setName((String)loAppr.getFieldValue("Name"));
        Log.customer.debug("***%s***BuysenseCreateNewRequisition:: The new requisition Name is " +loReq.getName());

        Log.customer.debug("***%s***BuysenseCreateNewRequisition:: The approver for the approvable doc " + loAppr +" is" +loAppr.getDottedFieldValue("ApprovalRequests.Approver"));

        /*if((loAppr.getDottedFieldValue("ApprovalRequests.Approver")) != null)
        {
            Approver loAppr1 = (Approver)loAppr.getDottedFieldValue("ApprovalRequests.Approver");
            if(loAppr1 instanceof ariba.user.core.Role)
            {
            	Role role = (ariba.user.core.Role) loAppr1;
                List llUsers = (List)role.getAllUsers();
                if(llUsers != null)
                {
                   loUser = (ariba.user.core.User)llUsers.get(0);
                   loReq.setRequester(loUser);
                   loReq.setPreparer(loUser);
                }
            }
            if(loAppr1 instanceof ariba.user.core.User)
            {
            	loUser = (ariba.user.core.User)loAppr1;
                loReq.setRequester(loUser);
                loReq.setPreparer(loUser);
            }
        }
        else
        {
            loReq.setPreparer(loAppr.getPreparer());
            loReq.setRequester(loAppr.getRequester());

            Log.customer.debug("***%s***BuysenseCreateNewRequisition:: The preparer for the requisition " + loReq +" is" +loReq.getPreparer());
            Log.customer.debug("***%s***BuysenseCreateNewRequisition:: The requester for the requisition " + loReq +" is" +loReq.getRequester());
        }*/
        ariba.user.core.User currentEffectiveUser=(ariba.user.core.User)(Base.getSession().getEffectiveUser());
        loReq.setPreparer(currentEffectiveUser);
        loReq.setRequester(currentEffectiveUser);

        loReq.setFieldValue("ReqHeadText3Value",lsFieldValues);
        loReq.save();
        createHistoryRecords(loAppr,loReq);
        Log.customer.debug("Adding a line item");

        try
        {

        String lsClientID = (String)loAppr.getDottedFieldValue("EformChooser.ClientID");
		String lsProfileName = (String)loAppr.getDottedFieldValue("EformChooser.ProfileName");
		String lsQuery = "Select FieldName from ariba.core.BuysEformFieldTable where ClientID = '"+lsClientID+"' and ProfileName = '"+lsProfileName +"' and FieldName like 'EformTable%' and FieldName Not Like 'EformTableHeader%' and VisibleOnEform = true Order By FieldName";
        AQLQuery aqlQuery = AQLQuery.parseQuery(lsQuery);
        Partition partition = Base.getService().getPartition("pcsv");
        AQLOptions aqlOptions = new AQLOptions(partition);
        Log.customer.debug("aqlQuery: " + aqlQuery);
        ariba.util.javascript.Log.javascript.debug("aqlQuery: " + aqlQuery);
        AQLResultCollection aqlResults = Base.getService().executeQuery(aqlQuery, aqlOptions);

        if (aqlResults.getErrors() == null)
        {
          while (aqlResults.next())
          {
        	  String lsFieldName = aqlResults.getString(0);
        	  int liIndex1 = lsFieldName.indexOf("Field");
        	  String lsTableNum = lsFieldName.substring(10, liIndex1);

        	  if(llTableNumbers != null)
        	  {
        		  if(!(llTableNumbers.contains(lsTableNum)))
        		  {
        			  Log.customer.debug("New Line");
        			  llTableNumbers.add(lsTableNum);
        		  }
        		  else
        		  {
        			  Log.customer.debug("Line number already exists");
        		  }
        	  }
        	  else
        	  {
        		  Log.customer.debug("First Line");
        		  llTableNumbers.add(lsTableNum);
        	  }
          }
        }
        }
        catch (Exception e)
        {
            Log.customer.warning(8888,
                    "Exception in BuysenseCreateNewRequisition method" + e.getMessage());
            ariba.util.javascript.Log.javascript
                    .debug("Exception in BuysenseCreateNewRequisition method "
                            + e.getMessage());
        }
        if(llTableNumbers != null)
        {
        	for(int i=0;i<llTableNumbers.size();i++)
        	{
        		Object liLine = llTableNumbers.get(i);
    		    String lsField1 = "EformTable"+llTableNumbers.get(i)+"Field1";
        	String lsDescriptionField = "EformTable" +llTableNumbers.get(i)+"Field2";
        	String lsMoneyField = "EformTable"+llTableNumbers.get(i)+"Field5";
        	String lsQuantityField = "EformTable"+llTableNumbers.get(i)+"Field3";
	        	String lsField4 = "EformTable"+llTableNumbers.get(i)+"Field4";
	        	String lsField6 = "EformTable"+llTableNumbers.get(i)+"Field6";
	        	String lsField7 = "EformTable"+llTableNumbers.get(i)+"Field7";
	        	String lsField8 = "EformTable"+llTableNumbers.get(i)+"Field8";
	        	String lsField9 = "EformTable"+llTableNumbers.get(i)+"Field9";
	        	String lsField10 = "EformTable"+llTableNumbers.get(i)+"Field10";
	        	String lsField11 = "EformTable"+llTableNumbers.get(i)+"Field11";
	        	String lsField12 = "EformTable"+llTableNumbers.get(i)+"Field12";
	        	String lsField13 = "EformTable"+llTableNumbers.get(i)+"Field13";
                        String lsField14 = "EformTable"+llTableNumbers.get(i)+"Field14";
	        	Boolean lbField1Value = (Boolean)loAppr.getFieldValue(lsField1);
	        	String lsDescriptionValue = (String)loAppr.getFieldValue(lsDescriptionField);
	        	String lsField7Value = (String)loAppr.getFieldValue(lsField7);
	        	String lsField8Value = (String)loAppr.getFieldValue(lsField8);
	        	String lsField11Value = (String)loAppr.getFieldValue(lsField11);
	        	String lsField12Value = (String)loAppr.getFieldValue(lsField12);

	        	Log.customer.debug("These are the values of the fields 1:"+lbField1Value+" 2:"+lsDescriptionValue);


	        	if((lbField1Value == null||(lbField1Value != null && lbField1Value.booleanValue() == false)) && (StringUtil.nullOrEmptyOrBlankString(lsDescriptionValue)) && (loAppr.getFieldValue(lsQuantityField) == null)
	        	&& (loAppr.getDottedFieldValue(lsField4) == null) && (loAppr.getDottedFieldValue(lsMoneyField) == null) && (loAppr.getDottedFieldValue(lsField6) == null)
	        	&& (StringUtil.nullOrEmptyOrBlankString(lsField7Value)) && (StringUtil.nullOrEmptyOrBlankString(lsField8Value)) && (loAppr.getDottedFieldValue(lsField9) == null)
	        	&& (loAppr.getDottedFieldValue(lsField10) == null) && (StringUtil.nullOrEmptyOrBlankString(lsField11Value)) && (StringUtil.nullOrEmptyOrBlankString(lsField12Value))
	        	&& (loAppr.getFieldValue(lsField13) == null) && (loAppr.getDottedFieldValue(lsField14) == null))
	        	{

		            Log.customer.debug("This line is a blank line. No Requisition line item will be created for this line.");
	        	}
	        	else{
	        		lsLineDetails = getLineDetails(loAppr,liLine,lsLineDetails);
	        		Log.customer.debug("Atleast one field in this line has value");
            loBaseObj = (ariba.purchasing.core.ReqLineItem)BaseObject.create("ariba.purchasing.core.ReqLineItem" ,loAppr.getPartition());
            List loLineItmVector = (List)((LineItemCollection)loReq).getLineItems() ;
            loLineItmVector.add(loBaseObj);
            if(loLineItmVector != null)
            {
            	int liCounter = loLineItmVector.size() -1;
            	LineItem tempLineItem = (LineItem)loLineItmVector.get( liCounter ) ;
		                String sourceGroup = "ObjectDefaulting";
		                String targetGroup = "ObjectDefaulting";
		            	BaseObject targetObject = tempLineItem;
		            	BaseObject targetObject1 = (BaseObject)tempLineItem.getDottedFieldValue("Accountings.SplitAccountings[0]");
		            	String lsCopyFieldName = "ClientName";
		                List sourceFields = FieldProperties.getFieldsInGroup(sourceGroup,
		                		sourceObject);
                        List targetFields = FieldProperties.getFieldsInGroup(targetGroup,
                        		targetObject);
                        List targetFields2 = FieldProperties.getFieldsInGroup(targetGroup,
                        		targetObject1);
                        List fieldNames = ListUtil.equalElements(sourceFields,
                                targetFields);
                        List fieldNames1 = ListUtil.equalElements(sourceFields,
                                targetFields2);
                        copyField(sourceObject,targetObject,loReq,fieldNames);
                        copyField(sourceObject,targetObject1,loReq,fieldNames1);
            	LiProdDesc = (LineItemProductDescription)tempLineItem.getDottedFieldValue("Description");
                LiProdDesc.setFieldValue("Description", loAppr.getFieldValue(lsDescriptionField));
                if(loAppr.getFieldValue(lsQuantityField)!=null)
                {
                	BigDecimal lbdQuantity = new BigDecimal(loAppr.getFieldValue(lsQuantityField).toString());
                    tempLineItem.setFieldValue("Quantity", lbdQuantity);
                }
                
                Money loZeroMoney = new Money(0.0, Currency.getDefaultCurrency(appPartition));
                Object loPrice = loAppr.getFieldValue(lsMoneyField);
                LiProdDesc.setFieldValue("Price", loPrice == null?loZeroMoney:loPrice);
                tempLineItem.setFieldValue("ShipTo", loReq.getFieldValue("ShipTo"));
                tempLineItem.setFieldValue("DeliverTo", loReq.getFieldValue("DeliverTo"));
            }
        }
        }
        }
        String lsCommentString = getCommentString(loAppr,lsLineDetails);
        Comment loComment = new Comment(loAppr.getPartition());
        if (loAppr.getPreparer() == null)
        {
            Log.customer.debug("BuysenseAddConfirmationOrderComment: Preparer is null");
            loComment.setUser((ariba.user.core.User) Base.getSession().getEffectiveUser());
        }
        else
        {
            Log.customer.debug("BuysenseAddConfirmationOrderComment: Preparer is not null");
            loComment.setUser(loAppr.getPreparer()); // add preparer
        }
        String lsTitle = "eForm" +":" + (String)loAppr.getFieldValue("InitialUniqueName");
        Log.customer.debug("The comment title is :"+lsTitle);
        loComment.setTitle(lsTitle);
        loComment.setBody(lsCommentString);
        loComment.setType(Comment.TypeGeneral);
        loComment.setDate(new ariba.util.core.Date());
        loComment.setExternalComment(false);
        loReq.addComment(loComment);
        loReq.save();
        Base.getSession().sessionCommit();

        return loReq;
    }

	private void copyField(BaseObject sourceObject, BaseObject targetObject,
			Requisition loReq, List fieldNames) {
		BuysenseCopyFields loBcp = new BuysenseCopyFields();
		boolean defaulting = true;
        for(int i = 0; i < fieldNames.size(); i++)
        {
        	// Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            String fieldName = (String)fieldNames.get(i);
            Log.customer.debug("BuysenseCopyFields::copyFields::FieldName = %s FieldValue = %s",fieldName,targetObject.getDottedFieldValue(fieldName));
            //if(fieldName.equalsIgnoreCase("BillingAddress") && (source.getDottedFieldValue(fieldName) != null))
            if(fieldName.equalsIgnoreCase("BillingAddress"))
            {
                	Log.customer.debug("FieldName: %s",fieldName);
                	loBcp.setBillingAddress(sourceObject,targetObject,loReq,fieldName);
            }
            else
            if((!defaulting || !loBcp.nullField(sourceObject, fieldName)))
            {
                //Log.customer.debug("FieldName: %s",fieldName);
            	Log.customer.debug("BuysenseCopyFields::copyFields::Overriding");
                BaseObject.copyField(sourceObject,
                		targetObject, fieldName, !defaulting, false);
            }
        }

	}

	private String getLineDetails(Approvable loAppr, Object i, String lsLineDetails) {
		Object loLineDetails;
		Object loFieldValues;
	    String lsField1 = "EformTable"+i+"Field1";
    	String lsDescriptionField = "EformTable" +i+"Field2";
    	String lsQuantityField = "EformTable"+i+"Field3";
    	String lsField4 = "EformTable"+i+"Field4";
    	String lsMoneyField = "EformTable"+i+"Field5";
    	String lsField6 = "EformTable"+i+"Field6";
    	String lsField7 = "EformTable"+i+"Field7";
    	String lsField8 = "EformTable"+i+"Field8";
    	String lsField9 = "EformTable"+i+"Field9";
    	String lsField10 = "EformTable"+i+"Field10";
    	String lsField11 = "EformTable"+i+"Field11";
    	String lsField12 = "EformTable"+i+"Field12";
    	String lsField13 = "EformTable"+i+"Field13";
        String lsField14 = "EformTable"+i+"Field14";

    	String lsFieldLabel = new String();

    	lsLineDetails = lsLineDetails + "Line " +i+ "- ";
	    ariba.user.core.User RequesterUser = (ariba.user.core.User)loAppr.getDottedFieldValue("Requester");
	    Partition appPartition = loAppr.getPartition();
	    ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,appPartition);
	    String lsClientID = (String)RequesterPartitionUser.getDottedFieldValue("ClientName.ClientID");
	    BaseObject bo = (BaseObject)loAppr;
	    String appType = "Eform";
	    String lsProfileName = (String)loAppr.getDottedFieldValue("EformChooser.ProfileName");
		Boolean lbVisible = BuysenseEformUIConditions.isFieldVisible(lsClientID ,lsProfileName,lsField1,bo,appType);
		if(lbVisible)
		{
			lsFieldLabel = (String)getLabel("EformTableHeaderTxt1",loAppr);
			loFieldValues = loAppr.getDottedFieldValue(lsField1);
			if(!(StringUtil.nullOrEmptyOrBlankString(lsFieldLabel)))
			{
			if((loFieldValues != null && loFieldValues.toString().trim().equals("true")))
               {
               	  Log.customer.debug("converting boolean true to yes for field name :"+lsField1);
               	  loFieldValues="Yes";
               }
			else if((loFieldValues != null && loFieldValues.toString().trim().equals("false")))
               {
             	  Log.customer.debug("converting boolean true to yes for field name :"+lsField1);
             	  loFieldValues="No";
               }
			lsLineDetails = lsLineDetails + lsFieldLabel + ":" + loFieldValues;
		}
		}
		lbVisible = BuysenseEformUIConditions.isFieldVisible(lsClientID ,lsProfileName,lsDescriptionField,bo,appType);
		if(lbVisible)
		{
			lsFieldLabel = (String)getLabel("EformTableHeaderTxt2",loAppr);
			loFieldValues = loAppr.getFieldValue(lsDescriptionField);
			if(!(StringUtil.nullOrEmptyOrBlankString(lsFieldLabel)))
			{
			if(loFieldValues != null)
			{
				lsLineDetails = lsLineDetails + "|"+ lsFieldLabel + ":" + loFieldValues;
			}
		    }
		}
		lbVisible = BuysenseEformUIConditions.isFieldVisible(lsClientID ,lsProfileName,lsQuantityField,bo,appType);
		if(lbVisible)
		{
			lsFieldLabel = (String)getLabel("EformTableHeaderTxt3",loAppr);
			loFieldValues = loAppr.getFieldValue(lsQuantityField);
			if(!(StringUtil.nullOrEmptyOrBlankString(lsFieldLabel)))
			{
			if(loFieldValues != null)
			{
				lsLineDetails = lsLineDetails + "|"+ lsFieldLabel + ":" + loFieldValues;
			}
		}
		}
		lbVisible = BuysenseEformUIConditions.isFieldVisible(lsClientID ,lsProfileName,lsField4,bo,appType);
		if(lbVisible)
		{
			lsFieldLabel = (String)getLabel("EformTableHeaderTxt4",loAppr);
			String lsField4_1 = lsField4 + ".UniqueName";
			loFieldValues = loAppr.getFieldValue(lsField4);
			if(!(StringUtil.nullOrEmptyOrBlankString(lsFieldLabel)))
			{
			if(loFieldValues != null)
			{
				String lsFieldValue = (String)loAppr.getDottedFieldValue(lsField4_1);
				lsLineDetails = lsLineDetails + "|"+ lsFieldLabel + ":" + lsFieldValue;
			}
		}
		}
		lbVisible = BuysenseEformUIConditions.isFieldVisible(lsClientID ,lsProfileName,lsMoneyField,bo,appType);
		if(lbVisible)
		{
			lsFieldLabel = (String)getLabel("EformTableHeaderTxt5",loAppr);
			//loFieldValues = loAppr.getFieldValue(lsQuantityField);
			if(!(StringUtil.nullOrEmptyOrBlankString(lsFieldLabel)))
			{
			if(loAppr.getFieldValue(lsMoneyField) != null)
			{
                Money loProcMoney=(Money)loAppr.getDottedFieldValue(lsMoneyField);
                loProcMoney = new Money(BigDecimalFormatter.round(loProcMoney.getAmount(), 2), loProcMoney.getCurrency());
                loFieldValues= (loProcMoney.asString()).substring(0, (loProcMoney.asString().indexOf(".")+3)).concat(" ").concat(loProcMoney.getCurrency().getSuffix());
                lsLineDetails = lsLineDetails + "|" + lsFieldLabel + ":" + loFieldValues;
			}
		}
		}
		lbVisible = BuysenseEformUIConditions.isFieldVisible(lsClientID ,lsProfileName,lsField6,bo,appType);
		if(lbVisible)
		{
			lsFieldLabel = (String)getLabel("EformTableHeaderTxt6",loAppr);
			//loFieldValues = loAppr.getFieldValue(lsQuantityField);
			if(!(StringUtil.nullOrEmptyOrBlankString(lsFieldLabel)))
			{
			if(loAppr.getFieldValue(lsField6) != null)
			{
                Money loProcMoney=(Money)loAppr.getDottedFieldValue(lsField6);
                loProcMoney = new Money(BigDecimalFormatter.round(loProcMoney.getAmount(), 2), loProcMoney.getCurrency());
                loFieldValues= (loProcMoney.asString()).substring(0, (loProcMoney.asString().indexOf(".")+3)).concat(" ").concat(loProcMoney.getCurrency().getSuffix());
                lsLineDetails = lsLineDetails + "|" + lsFieldLabel + ":" + loFieldValues;
			}
		}
		}
		lbVisible = BuysenseEformUIConditions.isFieldVisible(lsClientID ,lsProfileName,lsField7,bo,appType);
		if(lbVisible)
		{
			lsFieldLabel = (String)getLabel("EformTableHeaderTxt7",loAppr);
			loFieldValues = loAppr.getFieldValue(lsField7);
			if(!(StringUtil.nullOrEmptyOrBlankString(lsFieldLabel)))
			{
			if(loFieldValues != null)
			{
				lsLineDetails = lsLineDetails + "|"+ lsFieldLabel + ":" + loFieldValues;
			}
		}
		}
		lbVisible = BuysenseEformUIConditions.isFieldVisible(lsClientID ,lsProfileName,lsField8,bo,appType);
		if(lbVisible)
		{
			lsFieldLabel = (String)getLabel("EformTableHeaderTxt8",loAppr);
			loFieldValues = loAppr.getFieldValue(lsField8);
			if(!(StringUtil.nullOrEmptyOrBlankString(lsFieldLabel)))
			{
			if(loFieldValues != null)
			{
				lsLineDetails = lsLineDetails + "|"+ lsFieldLabel + ":" + loFieldValues;
			}
		}
	   }
		lbVisible = BuysenseEformUIConditions.isFieldVisible(lsClientID ,lsProfileName,lsField9,bo,appType);
		if(lbVisible)
		{
			lsFieldLabel = (String)getLabel("EformTableHeaderTxt9",loAppr);
			String lsField9_1 = lsField9 + ".Name";
			loFieldValues = loAppr.getFieldValue(lsField9);
			if(!(StringUtil.nullOrEmptyOrBlankString(lsFieldLabel)))
			{
			if(loFieldValues != null)
			{
				String lsFieldValue = (String)loAppr.getDottedFieldValue(lsField9_1);
				lsLineDetails = lsLineDetails + "|"+ lsFieldLabel + ":" + lsFieldValue;
			}
		}
		}
		lbVisible = BuysenseEformUIConditions.isFieldVisible(lsClientID ,lsProfileName,lsField10,bo,appType);
		if(lbVisible)
		{
			lsFieldLabel = (String)getLabel("EformTableHeaderTxt10",loAppr);
			String lsField10_1 = lsField10 + ".Name";
			loFieldValues = loAppr.getFieldValue(lsField10);
			if(!(StringUtil.nullOrEmptyOrBlankString(lsFieldLabel)))
			{
			if(loFieldValues != null)
			{
				String lsFieldValue = (String)loAppr.getDottedFieldValue(lsField10_1);
				lsLineDetails = lsLineDetails + "|"+ lsFieldLabel + ":" + lsFieldValue;
			}
		}
		}
		lbVisible = BuysenseEformUIConditions.isFieldVisible(lsClientID ,lsProfileName,lsField11,bo,appType);
		if(lbVisible)
		{
			lsFieldLabel = (String)getLabel("EformTableHeaderTxt11",loAppr);
			loFieldValues = loAppr.getFieldValue(lsField11);
			if(!(StringUtil.nullOrEmptyOrBlankString(lsFieldLabel)))
			{
			if(loFieldValues != null)
			{
				lsLineDetails = lsLineDetails + "|"+ lsFieldLabel + ":" + loFieldValues;
			}
		}
		}
		lbVisible = BuysenseEformUIConditions.isFieldVisible(lsClientID ,lsProfileName,lsField12,bo,appType);
		if(lbVisible)
		{
			lsFieldLabel = (String)getLabel("EformTableHeaderTxt12",loAppr);
			loFieldValues = loAppr.getFieldValue(lsField12);
			if(!(StringUtil.nullOrEmptyOrBlankString(lsFieldLabel)))
			{
			if(loFieldValues != null)
			{
				lsLineDetails = lsLineDetails + "|"+ lsFieldLabel + ":" + loFieldValues;
			}
		}
		}
		lbVisible = BuysenseEformUIConditions.isFieldVisible(lsClientID ,lsProfileName,lsField13,bo,appType);
		if(lbVisible)
		{
			lsFieldLabel = (String)getLabel("EformTableHeaderTxt13",loAppr);
			loFieldValues = loAppr.getFieldValue(lsField13);
			if(!(StringUtil.nullOrEmptyOrBlankString(lsFieldLabel)))
			{
			if(loFieldValues != null)
			{
        	    ariba.util.core.Date loDateValue = (ariba.util.core.Date) loAppr.getFieldValue(lsField13);
        	    String lsFieldValue = loDateValue.toDateMonthYearString();
				lsLineDetails = lsLineDetails + "|"+ lsFieldLabel + ":" + lsFieldValue;
			}
		}
		}
        lbVisible = BuysenseEformUIConditions.isFieldVisible(lsClientID ,lsProfileName,lsField14,bo,appType);
        if(lbVisible)
        {
            lsFieldLabel = (String)getLabel("EformTableHeaderTxt14",loAppr);
            String lsField14_1 = lsField14 + ".NIGPDescription";
            loFieldValues = loAppr.getDottedFieldValue(lsField14);
            if(!(StringUtil.nullOrEmptyOrBlankString(lsFieldLabel)))
            {
            if(loFieldValues != null)
            {
                String lsFieldValue = (String)loAppr.getDottedFieldValue(lsField14_1);
                lsLineDetails = lsLineDetails + "|"+ lsFieldLabel + ":" + lsFieldValue;
            }
        }
        }
		lsLineDetails = lsLineDetails + "\n";
		return lsLineDetails;
	}

	private String getCommentString(Approvable loAppr, String lsLineDetails)
	{
		Log.customer.debug("Inside getCommentSTring method");
		String lsCommentString = new String();
		String lsUniqueName = (String)loAppr.getUniqueName();
        String[] Local_eformFields = { "Header","NIGPDescription","EformHeadPckList","EformHeadLongTextField","EformHeadText","EformHeadPckList1","EformHeadPckList2", "EformHeadDate1",
                "EformHeadPckList3", "EformIntegerText1", "EformHeadPckList4","EformHeadPckList5", "EformHeadDate2", "EformHeadDate3","EformIntegerText2","EformIntegerText3",
                "EformHeadPckList6", "EformHeadPckList7","EformHeadLargeText1","EformHeadLargeText", "EformHeadLargeText2", "EformHeadLargeText3","EformHeadLargeText3_1","EformHeadLargeText3_2","EformHeadLargeText3_3","EformHeadText1","EformHeadText_1","EformHeadText2",
                "EformHeadText3", "EformHeadPckList8","EformIntegerText4", "EformHeadText4","EformHeadText5","EformHeadCB","EformHeadCB1", "EformHeadCB2", "EformHeadCB3","EformHeadCB4",
                "EformHeadCB5","EformHeadCB6","EformHeadCB7","EformHeadLongTextField1","EformHeadLongTextField_1","EformHeadLargeText4","EformHeadGrp1RadioHeader","EformHeadGrp_1RadioHeader","EformHeadGrp_2RadioHeader","EformHeadGrp_3RadioHeader","EformHeadGrp_4RadioHeader","EformHeadGrp_5RadioHeader","EformHeadGrp_6RadioHeader","EformHeadGrp_7RadioHeader","EformAcctLongTextField1","EformMoney1","EformMoney","EformHeadGrp2RadioHeader",
                "Line", "Accounting","EformAcctNIGPDescription","EformAcctPckList1", "EformAcctPckList2","EformAcctPckList3", "EformAcctPckList4", "EformAcctPckList5","EformAcctPckList6", 
                "EformAcctPckList7", "EformAcctPckList8","EformAcctPckList9", "EformAcctPckList10", "EformAcctText1","EformAcctText2","EformAcctText3","EformAcctText4",
                "EformAcctLargeText1","EformAcctLargeText","EformAcctText5","EformAcctCB1","EformAcctLongTextField2","EformAcctCB2","EformAcctLongTextField3","EformAcctCB3","EformAcctCB4","EformAcctCB5",
                "EformAcctCB6","EformAcctCB7","EformAcctCB8","EformAcctCB9","EformAcctLargeText2","EformAcctLargeText3",
                "EformAcctLargeText4","EformAcctLargeText5","EformAcctDate1","EformAcctMoney1"};
		List eformFieldsVector = ListUtil.arrayToList(Local_eformFields);
		for (Iterator fieldTableEnum = eformFieldsVector.iterator(); fieldTableEnum
				.hasNext();)
		{
			String lsFieldName = (String)fieldTableEnum.next();
			Log.customer.debug("The FieldName is :"+lsFieldName);
			String lsFieldLabel;
			String lsFieldValue;
			if((!StringUtil.nullOrEmptyOrBlankString(lsFieldName)) && (lsFieldName.equalsIgnoreCase("Header")||lsFieldName.equalsIgnoreCase("Accounting")||lsFieldName.equalsIgnoreCase("Line")))
			{
				Log.customer.debug("Checking whether it is a header field or accounting field");
				if(lsFieldName.equalsIgnoreCase("Header"))
				{
					lsCommentString = lsCommentString + "[eform " + lsUniqueName +"]" + "\n\n";
					lsCommentString = lsCommentString + "\n" + "Header Data" + "\n";
					lsCommentString = lsCommentString + "--------------" + "\n";
					String lsTitle = (String)loAppr.getFieldValue("Name");
					String PreparerUser = (String)loAppr.getDottedFieldValue("Preparer.Name.PrimaryString");					
					lsCommentString = lsCommentString + "Title" + ":" + lsTitle + "\n";
                                        lsCommentString = lsCommentString + "Preparer" + ":" + PreparerUser + "\n";
				}
				else if(lsFieldName.equalsIgnoreCase("Line"))
				{
					lsCommentString = lsCommentString + "\n" + lsLineDetails + "\n";
				}
				else
				{
					lsCommentString = lsCommentString + "\n" + "Accounting/Other Data" + "\n";
					lsCommentString = lsCommentString + "--------------" + "\n";
				}
			}
			else if(!StringUtil.nullOrEmptyOrBlankString(lsFieldName))
			{
        	    ariba.user.core.User RequesterUser = (ariba.user.core.User)loAppr.getDottedFieldValue("Requester");
        	    Partition appPartition = loAppr.getPartition();
        	    ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,appPartition);
        	    String lsClientID = (String)RequesterPartitionUser.getDottedFieldValue("ClientName.ClientID");
        	    BaseObject bo = (BaseObject)loAppr;
        	    String appType = "Eform";
        	    String lsProfileName = (String)loAppr.getDottedFieldValue("EformChooser.ProfileName");
            	if(lsProfileName != null)
            	{
            		Boolean lbVisible = BuysenseEformUIConditions.isFieldVisible(lsClientID ,lsProfileName,lsFieldName,bo,appType);
            		if(lbVisible)
            		{
            			if(lsFieldName.contains("PckList"))
            			{
            				Log.customer.debug("The FieldName is :"+lsFieldName);
            				lsFieldLabel = (String)getLabel(lsFieldName,loAppr);
            				String lsFieldName1 = lsFieldName + ".Name";
             			    Log.customer.debug("The new field name is :"+lsFieldName1);
            			    Log.customer.debug("The FieldLabel is :"+lsFieldLabel);
                            lsFieldValue = (String)loAppr.getDottedFieldValue(lsFieldName1);
                            if(!StringUtil.nullOrEmptyOrBlankString(lsFieldValue))
                            {
                                Log.customer.debug("The FieldValue is :"+lsFieldValue);
                                lsCommentString = lsCommentString + lsFieldLabel + ":" +lsFieldValue + "\n";
                            }



            			}
            			else if(lsFieldName.contains("Money") )
            			{
            				if(loAppr.getDottedFieldValue(lsFieldName) != null)
            				{
            				    Log.customer.debug("The FieldName is :"+lsFieldName);
            				    lsFieldLabel = (String)getLabel(lsFieldName,loAppr);
                                Money loProcMoney=(Money)loAppr.getDottedFieldValue(lsFieldName);
                                loProcMoney = new Money(BigDecimalFormatter.round(loProcMoney.getAmount(), 2), loProcMoney.getCurrency());
                                lsFieldValue= (loProcMoney.asString()).substring(0, (loProcMoney.asString().indexOf(".")+3)).concat(" ").concat(loProcMoney.getCurrency().getSuffix());
                                Log.customer.debug("Value of Money field  : "+lsFieldValue);
                                lsCommentString = lsCommentString + lsFieldLabel + ":" +lsFieldValue + "\n";
            				}
            			}
            			else if(lsFieldName.contains("Date"))
            			{
            				if( loAppr.getDottedFieldValue(lsFieldName) != null)
            				{
            				    Log.customer.debug("The FieldName is :"+lsFieldName);
            				    lsFieldLabel = (String)getLabel(lsFieldName,loAppr);
                        	    ariba.util.core.Date loDateValue = (ariba.util.core.Date) loAppr.getFieldValue(lsFieldName);
                        	    lsFieldValue = loDateValue.toDateMonthYearString();
                                Log.customer.debug("Value of  field  : "+lsFieldValue);
                                lsCommentString = lsCommentString + lsFieldLabel + ":" +lsFieldValue + "\n";
            				}
            			}
            			else if(lsFieldName.contains("LargeText"))
            			{
        				    Log.customer.debug("The FieldName is :"+lsFieldName);
        				    lsFieldLabel = (String)getLabel(lsFieldName,loAppr);
        				    lsFieldName = lsFieldName + "Val";
                            lsFieldValue = (String)loAppr.getDottedFieldValue(lsFieldName);
                            if(!StringUtil.nullOrEmptyOrBlankString(lsFieldValue))
                            {
                                Log.customer.debug("The FieldValue is :"+lsFieldValue);
                                lsCommentString = lsCommentString + lsFieldLabel + ":" +lsFieldValue + "\n";
                            }
                            }
                        else if(lsFieldName.contains("LongText"))
                        {
                            Log.customer.debug("The FieldName is :"+lsFieldName);
                            lsFieldLabel = (String)getLabel(lsFieldName,loAppr);
                            lsFieldValue = (String)loAppr.getDottedFieldValue(lsFieldName);
                            if(!StringUtil.nullOrEmptyOrBlankString(lsFieldValue))
                            {
                                Log.customer.debug("The FieldValue is :"+lsFieldValue);
                                lsCommentString = lsCommentString + lsFieldLabel + ":" +lsFieldValue + "\n";
                            }
                        }
                        else if(lsFieldName.contains("IntegerText"))
                        {
                            Log.customer.debug("The FieldName is :"+lsFieldName);
                            lsFieldLabel = (String)getLabel(lsFieldName,loAppr);                         
                            Integer liFieldValue=(Integer)loAppr.getDottedFieldValue(lsFieldName);                            		
                        
                            if(liFieldValue!=null)
                            {
                            	int intFieldValue= ((Integer)liFieldValue).intValue();
                            	 Log.customer.debug("The FieldValue is :"+liFieldValue);
                                 lsCommentString = lsCommentString + lsFieldLabel + ":" +intFieldValue+ "\n";
                            
                            }
                        }
                       else if(lsFieldName.contains("AcctText"))
                        {
                            Log.customer.debug("The FieldName is :"+lsFieldName);
                            lsFieldLabel = (String)getLabel(lsFieldName,loAppr);
                            lsFieldValue = (String)loAppr.getDottedFieldValue(lsFieldName);
                            if(!StringUtil.nullOrEmptyOrBlankString(lsFieldValue))
                            {

                                Log.customer.debug("The FieldValue is :"+lsFieldValue);
                                lsCommentString = lsCommentString + lsFieldLabel + ":" +lsFieldValue + "\n";
                            }
                        }
            			else if(lsFieldName.contains("CB"))
            			{
        				    Log.customer.debug("The FieldName is :"+lsFieldName);
        				    lsFieldLabel = (String)getLabel(lsFieldName,loAppr);
                        	Object loFieldVal = loAppr.getFieldValue(lsFieldName);
                        	if(loFieldVal != null)
                        	{
                        	    if((lsFieldName.equals("EformHeadCB1")  && loFieldVal.toString().trim().equals("true"))||
                           		(lsFieldName.equals("EformHeadCB") && loFieldVal.toString().trim().equals("true"))||
                           		(lsFieldName.equals("EformHeadCB6") && loFieldVal.toString().trim().equals("true"))||
                           		(lsFieldName.equals("EformHeadCB7") && loFieldVal.toString().trim().equals("true"))||
                           		(lsFieldName.equals("EformHeadCB2") && loFieldVal.toString().trim().equals("true"))||
                           		(lsFieldName.equals("EformHeadCB3") && loFieldVal.toString().trim().equals("true"))||
                           		(lsFieldName.equals("EformHeadCB4") && loFieldVal.toString().trim().equals("true"))||
                           		(lsFieldName.equals("EformHeadCB5") && loFieldVal.toString().trim().equals("true"))||
                           		(lsFieldName.equals("EformAcctCB1") && loFieldVal.toString().trim().equals("true"))||
                           		(lsFieldName.equals("EformAcctCB2") && loFieldVal.toString().trim().equals("true"))||
                           		(lsFieldName.equals("EformAcctCB3") && loFieldVal.toString().trim().equals("true"))||
                           		(lsFieldName.equals("EformAcctCB4") && loFieldVal.toString().trim().equals("true"))||
                           		(lsFieldName.equals("EformAcctCB5") && loFieldVal.toString().trim().equals("true"))||
                           		(lsFieldName.equals("EformAcctCB6") && loFieldVal.toString().trim().equals("true"))||
                           		(lsFieldName.equals("EformAcctCB7") && loFieldVal.toString().trim().equals("true"))||
                           		(lsFieldName.equals("EformAcctCB8") && loFieldVal.toString().trim().equals("true"))||
                           		(lsFieldName.equals("EformAcctCB9") && loFieldVal.toString().trim().equals("true")))
                                {
                           	        Log.customer.debug("converting boolean true to yes for field name :"+lsFieldName);
                                    loFieldVal="Yes";
                                    lsFieldValue = (String)loFieldVal;
                                    lsCommentString = lsCommentString + lsFieldLabel + ":" +lsFieldValue + "\n";
                                }
                        	    else if((lsFieldName.equals("EformHeadCB1") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                               		(lsFieldName.equals("EformHeadCB2") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                               		(lsFieldName.equals("EformHeadCB") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                               		(lsFieldName.equals("EformHeadCB6") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                               		(lsFieldName.equals("EformHeadCB7") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                               		(lsFieldName.equals("EformHeadCB3") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                               		(lsFieldName.equals("EformHeadCB4") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                               		(lsFieldName.equals("EformHeadCB5") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                               		(lsFieldName.equals("EformAcctCB1") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                               		(lsFieldName.equals("EformAcctCB2") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                               		(lsFieldName.equals("EformAcctCB3") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                               		(lsFieldName.equals("EformAcctCB4") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                               		(lsFieldName.equals("EformAcctCB5") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                               		(lsFieldName.equals("EformAcctCB6") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                               		(lsFieldName.equals("EformAcctCB7") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                               		(lsFieldName.equals("EformAcctCB8") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                               		(lsFieldName.equals("EformAcctCB9") && loFieldVal != null && loFieldVal.toString().trim().equals("false")))
                        	        {
                       	                Log.customer.debug("converting boolean true to yes for field name :"+lsFieldName);
                                        loFieldVal="No";
                                        lsFieldValue = (String)loFieldVal;
                                        lsCommentString = lsCommentString + lsFieldLabel + ":" +lsFieldValue + "\n";

                        	        }
            			     }
            			}
                        else if(lsFieldName.contains("Radio"))
                        {
                            Log.customer.debug("The FieldName is :"+lsFieldName);
                            lsFieldLabel = (String)getLabel(lsFieldName,loAppr);
                            lsFieldName = lsFieldName.substring(0, lsFieldName.lastIndexOf("Head")) +""+"Val";
                            lsFieldValue = (String)loAppr.getDottedFieldValue(lsFieldName);
                            if(!StringUtil.nullOrEmptyOrBlankString(lsFieldValue))
                            {

                                Log.customer.debug("The FieldValue is :"+lsFieldValue);
                                lsCommentString = lsCommentString + lsFieldLabel + ":" +lsFieldValue + "\n";
                            }

                        }
            			else
            			{
                			lsFieldLabel = (String)getLabel(lsFieldName,loAppr);
                            if (loAppr.getDottedFieldValue(lsFieldName) != null)
                            {
                                lsFieldValue = (String) loAppr.getDottedFieldValue(lsFieldName);
                                if (!StringUtil.nullOrEmptyOrBlankString(lsFieldValue))
                                {


                                    lsCommentString = lsCommentString + lsFieldLabel + ":" + lsFieldValue + "\n";
                                }
                            }
            			}
            		}
            	}
			}
		}
		return lsCommentString;
	}

	private Object getLabel(String lsActualFieldName, Approvable approvable) {

		Log.customer.debug("Inside getLabel method ");
		FieldProperties lfpFieldProperty1 = approvable.getFieldProperties(lsActualFieldName);
		String lsLabel = lfpFieldProperty1.getLabel();
		Log.customer.debug("the label value is:"+lsLabel);
		return lsLabel;
	}

	public void createHistoryRecords(Approvable loAppr, Requisition loReq) {
		// TODO Auto-generated method stub
        BaseObject loEformObj = (BaseObject)loAppr;
        BaseObject loReqObj = (BaseObject)loReq;
        String lsEformUN = (String)loAppr.getFieldValue("UniqueName");
        String lsReqUN = (String)loReq.getFieldValue("UniqueName");
        String lsRecordType = "ReqCreated";
        String lsRecordTypeReq = "CreateReqRecord";
        //String lsRecordType = null;

        //String lsMessage = lsReqUN+": has been created from this eform";
        String lsMessage = Fmt.Sil(Base.getSession().getLocale(),
				"buysense.DynamicEform", "ReqCreatedTypeRecordDetails",lsReqUN);
        //String lsMessage1 = "This Requisition has been created from:"+lsEformUN;
        String lsMessage1 = Fmt.Sil(Base.getSession().getLocale(),
				"buysense.DynamicEform", "createRequisitionRecordDetails",lsEformUN);
        Partition loPartition = loAppr.getPartition();
        //User loAribaSystemUser = Core.getService().getAribaSystemUser(loPartition);
	    ariba.user.core.User currentEffectiveUser = (ariba.user.core.User) (Base.getSession().getEffectiveUser());

	    Partition appPartition = loAppr.getPartition();

	    ariba.common.core.User loAribaSystemUser = ariba.common.core.User.getPartitionedUser(currentEffectiveUser,appPartition);

        Log.customer.debug("Creating a history record for eform");
        BuysenseEformUtil.createHistory(loEformObj, lsEformUN,  lsRecordType, lsMessage, loAribaSystemUser);

        Log.customer.debug("Creating a history record for requisition");
        BuysenseEformUtil.createHistory(loReqObj, lsReqUN,  lsRecordTypeReq, lsMessage1, loAribaSystemUser);

	}

}
