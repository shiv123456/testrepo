package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.ClusterRoot;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;

public class SetExemptionRequestClient extends Action
{

    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        Approvable loAppr = null;
        String lsClientName = null;
        ClusterRoot loClient = null;
        if(valuesource!= null && valuesource instanceof Approvable)
        {
           loAppr = (Approvable)valuesource;
           ariba.user.core.User loRequesterUser = (ariba.user.core.User)loAppr.getRequester();
           if(loRequesterUser != null)
           {
               ariba.common.core.User loRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(loRequesterUser,loAppr.getPartition());
               loClient = (ClusterRoot)loRequesterPartitionUser.getFieldValue("ClientName");
               lsClientName = (String)loClient.getFieldValue("ClientName");
               if(loClient != null && lsClientName != null)
               {
  
            	   loAppr.setFieldValue("ClientName",loClient);
               }
           }
        }
    }

}
