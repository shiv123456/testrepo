/* 08/11/2003: Updates for Ariba 8.0 (Jeff Namadan) */
/* 08/28/2003: Updates for Ariba 8.0 (David Chamberlain)
   Had to back out previous 8.0 changes. We can't change
   ValueInfo to ValueInfo at this time.      */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.core.BaseObject;
//import ariba.htmlui.fieldsui.Log;
import ariba.util.log.Log;

//81->822 changed ConditionValueInfo to ValueInfo
public class CalcTotalButtonVisibility extends Condition
{
    
    /* Ariba 8.0: Replaced ValueInfo */
    private static final ValueInfo parameterInfo[];
    private static final String EqualToMsg1 = "EqualToMsg1";
    
    public boolean evaluate(Object value, PropertyTable params)
    {
        Log.customer.debug("CalcTotalButtonVisibility: This is the object we get in the evaluate: %s ", value);
        BaseObject bo = (BaseObject)params.getPropertyForKey("TargetValue");
        String status = (String)bo.getFieldValue("StatusString");
        if(status.equals("Approved"))
            return false;
        else 
            return true;
        
    }
    
    /* Ariba 8.0: Replaced ValueInfo */
    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }
    
    static
        {
        parameterInfo = (new ValueInfo[] 
                         {
                new ValueInfo("TargetValue",
                                       0),
                    new ValueInfo("Message", 0, Behavior.StringClass)
            }
        );
    }
}
