package config.java.ams.custom;

import java.io.ByteArrayOutputStream;
import java.util.List;
import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.util.DefaultPrettyPrinter;
import ariba.approvable.core.ApprovalRequest;
import ariba.approvable.core.Comment;
import ariba.base.core.BaseObject;
import ariba.util.core.ListUtil;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseDW2ExtractRecursiveElements
{
    private JsonGenerator jsonGeneratorRec;
    private String        msCN = "BuysenseDW2ExtractRecursiveElements";

    // Make sure special characters are extracted correctly using UTF-8 encoding - JB (CSPL-6888)
    public String getRecursiveJSON(BaseObject CurrentBaseObj, BuysenseDWHierarchyDetailsObj HierarchyDetailsObj, BuysenseDWFieldDetails[] fieldList) throws Exception
    {
        ByteArrayOutputStream bOutputJson = new ByteArrayOutputStream();
        try
        {
            if (CurrentBaseObj == null || HierarchyDetailsObj.getBuysenseAribaObjHierarchy().getAribaObjname() == null)
            {
                Log.customer.debug("*****%s*****  CurrentBaseObj or HierarchyDetails.getAribaObjname() is null", msCN);
                return "";
            }
            jsonGeneratorRec = (new JsonFactory()).createJsonGenerator(bOutputJson,JsonEncoding.UTF8);
            jsonGeneratorRec.setPrettyPrinter(new DefaultPrettyPrinter());

            if (StringUtil.equalsIgnoreCase(HierarchyDetailsObj.getBuysenseAribaObjHierarchy().getAribaObjname(), "ApprovalRequests"))
            {
                getApprovalRequests(CurrentBaseObj, HierarchyDetailsObj, fieldList);
                // just print for now. Move getFieldsForObjectLevel and getFieldValue to utility class
                Log.customer.debug("*****%s*****  CurrentBaseObj :" + CurrentBaseObj, msCN);
            }
            else if (StringUtil.equalsIgnoreCase(HierarchyDetailsObj.getBuysenseAribaObjHierarchy().getAribaObjname(), "Comments"))
            {
                getReqComments(CurrentBaseObj, HierarchyDetailsObj, fieldList);
                Log.customer.debug("*****%s***** REQ Comments CurrentBaseObj :" + CurrentBaseObj, msCN);
            }

            jsonGeneratorRec.flush();
            jsonGeneratorRec.close();

            Log.customer.debug("*****%s*****  ApprovalRequests JSON :" + new String( bOutputJson.toByteArray(),"UTF-8"), msCN);

        }
        finally
        {
            try
            {
                jsonGeneratorRec.flush();
                jsonGeneratorRec.close();
            }
            catch (Exception ex)
            {
            }
        }

        if (!StringUtil.nullOrEmptyOrBlankString(bOutputJson.toString()))
        {
            return new String( bOutputJson.toByteArray(),"UTF-8");     // Make sure special characters are extracted correctly using UTF-8 encoding - JB (CSPL-6888)
        }
        else
        {
            return "";
        }
    }

    private String getApprovalRequests(BaseObject currBaseObj, BuysenseDWHierarchyDetailsObj HierDetailsObj, BuysenseDWFieldDetails[] fieldList) throws Exception
    {
        java.util.List lAllApprovalRequests = ListUtil.list();
        BaseObject loApprovalRequest;

        List lvApprovalRequestsVector = (java.util.List) currBaseObj.getDottedFieldValue(HierDetailsObj.getBuysenseAribaObjHierarchy().getAribaObjname()); // get ApprovalRequests
        List lvecApprovalRequests = ListUtil.cloneList(lvApprovalRequestsVector);

        jsonGeneratorRec.writeStartObject();
        jsonGeneratorRec.writeObjectFieldStart(HierDetailsObj.getBuysenseAribaObjHierarchy().getDWOutputStr()); // starting current object
        jsonGeneratorRec.writeArrayFieldStart(HierDetailsObj.getBuysenseAribaObjHierarchy().getDWOutputStr() + "RECVECTOR"); // don't hardcode vector here

        // collect all the data approval request data in lvecApprovalRequests
        for (int i = 0; i < lvecApprovalRequests.size(); i++)
        {
            loApprovalRequest = (ApprovalRequest) lvecApprovalRequests.get(i);
            getDependencies(lvecApprovalRequests, loApprovalRequest); // collecting the requests in lvecApprovalRequests list
        }

        // render approval requests in JSON format
        for (int i = 0; i < lvecApprovalRequests.size(); i++)
        {
            jsonGeneratorRec.writeStartObject();
            loApprovalRequest = (BaseObject) lvecApprovalRequests.get(i);
            BuysenseDW2ExtractUtils.getFieldsForObjectLevel(jsonGeneratorRec, fieldList, loApprovalRequest, HierDetailsObj);
            jsonGeneratorRec.writeEndObject();

        }
        jsonGeneratorRec.writeEndArray();
        jsonGeneratorRec.writeEndObject(); // closing root object
        jsonGeneratorRec.writeEndObject(); // closing root object

        return "";
    }

    @SuppressWarnings( { "rawtypes", "unchecked" })
    public void getDependencies(List lvec, BaseObject appReq)
    {
        List vecNewDependencies = ListUtil.list();
        ApprovalRequest newDependency = new ApprovalRequest();
        vecNewDependencies = (List) appReq.getFieldValue("Dependencies");
        int size = vecNewDependencies.size();

        for (int i = 0; i < size; i++)
        {
            newDependency = (ApprovalRequest) vecNewDependencies.get(i);
            if (lvec.contains(newDependency))
            {
                continue;
            }
            else
            {
                lvec.add(newDependency);
                getDependencies(lvec, newDependency);
            }
        }
        return;
    }

    // Make sure special characters are extracted correctly using UTF-8 encoding - JB (CSPL-6888)
    @SuppressWarnings("unchecked")
    private void getReqComments(BaseObject currentBaseObj, BuysenseDWHierarchyDetailsObj hierarchyDetailsObj, BuysenseDWFieldDetails[] fieldList) throws Exception
    {
        Comment loReqCmt;

        List<Comment> lvReqCommntsVector = (java.util.List<Comment>) currentBaseObj.getDottedFieldValue(hierarchyDetailsObj.getBuysenseAribaObjHierarchy().getAribaObjname());
        List<Comment> lvAllReqComments = ListUtil.list();
        //((Requisition) currentBaseObj).getComments();
        jsonGeneratorRec.writeStartObject();
        jsonGeneratorRec.writeObjectFieldStart(hierarchyDetailsObj.getBuysenseAribaObjHierarchy().getDWOutputStr());
        jsonGeneratorRec.writeArrayFieldStart(hierarchyDetailsObj.getBuysenseAribaObjHierarchy().getDWOutputStr() + "RECVECTOR");

        // collect all the comments
        for (int i = 0; i < lvReqCommntsVector.size(); i++)
        {
            loReqCmt = lvReqCommntsVector.get(i);
            lvAllReqComments.add(loReqCmt);
            getComments(lvAllReqComments, loReqCmt); // collecting the requests in lvAllReqComments list
        }

        // render approval requests in JSON format
        for (int i = 0; i < lvAllReqComments.size(); i++)
        {
            jsonGeneratorRec.writeStartObject();
            loReqCmt = lvAllReqComments.get(i);
            BuysenseDW2ExtractUtils.getFieldsForObjectLevel(jsonGeneratorRec, fieldList, loReqCmt, hierarchyDetailsObj);
            jsonGeneratorRec.writeEndObject();

        }
        jsonGeneratorRec.writeEndArray();
        jsonGeneratorRec.writeEndObject();
        jsonGeneratorRec.writeEndObject();

    }

    // Make sure special characters are extracted correctly using UTF-8 encoding - JB (CSPL-6888)
    @SuppressWarnings("rawtypes")
    private void getComments(List<Comment> lvReqComments, Comment loReqComment)
    {
        List lvCmts = (List) ((Comment) loReqComment).getComments();
        for (int i = 0; i < lvCmts.size(); i++)
        {
            Comment loCmt = (Comment) lvCmts.get(i);

            if (lvReqComments.contains(loCmt))
            {
                continue;
            }
            else
            {
                lvReqComments.add(loCmt);
                getComments(lvReqComments, loCmt);
            }
        }
    }
}
