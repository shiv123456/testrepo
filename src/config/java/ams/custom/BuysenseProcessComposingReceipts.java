package config.java.ams.custom;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import ariba.approvable.core.ApprovalRequest;
import ariba.approvable.core.SimpleRecord;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.purchasing.core.POLineItem;
import ariba.purchasing.core.PurchaseOrder;
import ariba.receiving.core.Receipt;
import ariba.receiving.core.ReceiptItem;
import ariba.receiving.core.ReceivableLineItemCollection;
import ariba.user.core.Approver;
import ariba.user.core.Group;
import ariba.user.core.Role;
import ariba.user.core.User;
import ariba.util.core.Date;
import ariba.util.core.ListUtil;
import ariba.util.core.StringUtil;
import ariba.util.formatter.BigDecimalFormatter;
import ariba.util.log.Log;
import ariba.workforce.core.TimeSheet;

/**
 * CSPL #: 1287-Push order to Received when fully receiving with Timesheet
 * @author Pavan Aluri
 * Date  : 21-Oct-2009
 * @version 1.0
 * @see    BuysenseEmailNotifications
 * Explanation: This is called from BuysenseEmailNotifications when a timesheet is processed.
 *              This class checks for any composing receipt for the Order, which is completely received through timesheet and
 *              auto approves the same accepting zero items.
 *              
 *              At this point it is assumed that the only approver in the Receipt is Requester of Requisition.
 *              
 * CSPL        1300-Processing of composing Receipt with multiple approvers ...
 * @author     Pavan Aluri
 * Date        01-Dec-2009
 * @version    1.1
 * Explanation Here the code is added to auto approve the Receipt for all approvers.          
 */

public class BuysenseProcessComposingReceipts
{
	public static void processReceipts(TimeSheet timeSheet)
	{
		Log.customer.debug(" BuysenseProcessComposingReceipts.processReceipts()- start ");		
		String sIsOrderReceiving = "No";
		boolean bAnyComposingReceipt = false;
		boolean bAnyOrderLIStillToReceive = false;
		
		Receipt composingReceipt = null;
		ReceivableLineItemCollection RLIC = timeSheet.getOrder();
		if(RLIC instanceof PurchaseOrder)
		{
			PurchaseOrder po = (PurchaseOrder) RLIC;
			sIsOrderReceiving = po.getStatusString();
			Log.customer.debug(" BuysenseProcessComposingReceipts- sIsOrderReceiving "+sIsOrderReceiving);
			List lreceipts = ListUtil.list();
			lreceipts = (List)po.getReceipts();
			for(int i=0;i<lreceipts.size(); i++)
			{
				Receipt receipt;
				BaseId receiptId = (BaseId) lreceipts.get(i);
				receipt = (Receipt)Base.getSession().objectFromId(receiptId);
				Log.customer.debug(" BuysenseProcessComposingReceipts- receipt "+i+" is: "+receipt);				
				if(receipt != null && !(receipt instanceof TimeSheet))
				{
					Log.customer.debug(" BuysenseProcessComposingReceipts- receipt is not time sheet ");
					if(receipt.getStatusString().equalsIgnoreCase("Composing"))
					{
						Log.customer.debug(" BuysenseProcessComposingReceipts- Composing receipt is: "+receipt.getUniqueName());
						bAnyComposingReceipt = true;
						composingReceipt = receipt;
						for(Iterator poLIsIterator = po.getLineItemsIterator();poLIsIterator.hasNext();)
						{
							POLineItem poLI = (POLineItem) poLIsIterator.next();
							BigDecimal qty = poLI.getQuantity();
							BigDecimal noAccepted = poLI.getNumberAccepted();
							// (Quantity - NoAccepted)>0 means items yet to receive
							if(BigDecimalFormatter.compareBigDecimals(qty, noAccepted)==1)
							{
								bAnyOrderLIStillToReceive = true;
								Log.customer.debug(" BuysenseProcessComposingReceipts Order still to receive?: "+bAnyOrderLIStillToReceive);
							}
						}					
					}
				}
			}
		
			if(!StringUtil.nullOrEmptyOrBlankString(sIsOrderReceiving) && sIsOrderReceiving.equalsIgnoreCase("Receiving") && bAnyComposingReceipt && !bAnyOrderLIStillToReceive)
			{
				composingReceipt.setCloseOrder(true);
				for(Iterator receiptItemIterator = composingReceipt.getReceiptItemsIterator();receiptItemIterator.hasNext();)
				{
					ReceiptItem recpItem = (ReceiptItem)receiptItemIterator.next();
					recpItem.setDate(Date.getNow());
					Log.customer.debug(" BuysenseProcessComposingReceipts.processReceipts()- setting date to "+Date.getNow());					
				}
				
				autoApprove(composingReceipt);	
			}
		}
		
	}

	private static void autoApprove(Receipt composingReceipt)
	{
		
		addRecord(composingReceipt); //This is auto submit record
		List lApprovalRequests = composingReceipt.getApprovalRequests();
		for(int i=0;i<lApprovalRequests.size();i++)
		{
			List vDependencies = ListUtil.list();
			ApprovalRequest ar = (ApprovalRequest) lApprovalRequests.get(i);
			Log.customer.debug(" BuysenseProcessComposingReceipts.processReceipts()-ApprovalRequest "+i+" is: "+ar);
			vDependencies.add(ar);
			List seriesApprovalRequests = getAllDependencies(ar,vDependencies);
			Collections.reverse(seriesApprovalRequests);  
			Log.customer.debug(" BuysenseProcessComposingReceipts.processReceipts()-seriesApprovalRequests: "+seriesApprovalRequests.size());
			
			if( seriesApprovalRequests!=null && seriesApprovalRequests.size()>0 ) 
	          {
		  		 for( int j=0; j < seriesApprovalRequests.size(); j++)
				 {
		  			ApprovalRequest currentApprovalRequest = (ApprovalRequest) seriesApprovalRequests.get(j);
		  			Log.customer.debug(" currentApprovalRequest: "+currentApprovalRequest);
		  			if(!currentApprovalRequest.getApprovalRequired())
		  			{
		  				Log.customer.debug(" currentApprovalRequest is a watcher and doing nothing");
		  			}else
		  			{
		  				Log.customer.debug(" currentApprovalRequest is required to approve");
			  			if(currentApprovalRequest.isApproved())
			  			{
			  				Log.customer.debug(" currentApprovalRequest is already approved and doing nothing");
			  			}else
			  			{
			  				Log.customer.debug(" currentApprovalRequest is not yet approved and auto approving");	
		  					Log.customer.debug(" currentApprovalRequest is Active? : "+currentApprovalRequest.isActive());
		    				Log.customer.debug(" processing Recp "+composingReceipt.getUniqueName()+" and approving currentApprovalRequest- "+currentApprovalRequest.getApprover().getName().toString());		  					
		  					currentApprovalRequest.setState(8);
		  					approveWith(composingReceipt,currentApprovalRequest);
			  			}
		  			}
				 }
	          }		
		}				
		
	}
    public static List getAllDependencies( ApprovalRequest approvalRequest, List vDependencies )
	{
        List vDependencies1 = (List)approvalRequest.getDependencies();
        if( vDependencies1 != null )
		{
            ApprovalRequest dependency;
            for( int i=0; i < vDependencies1.size(); i++ )
			{
                dependency = (ApprovalRequest)vDependencies1.get( i );
				if(!(vDependencies.contains(dependency) == true))
				{
					vDependencies.add(dependency);
				}
				  getAllDependencies(dependency, vDependencies);		  
			}
        }
        return vDependencies;
    }     

	private static void approveWith(Receipt composingReceipt, ApprovalRequest ar)
	{
		User aribaSystemUser = User.getAribaSystemUser();	
		try
		{
			Log.customer.debug(" approving rececipt "+composingReceipt);
			Approver approver = ar.getApprover();
			if(approver instanceof User)
			{
				Log.customer.debug(" Approver is instance of User: "+ ar.getApprover());
				ar.setApprovedBy(ar.getApprover());
				User appUser = (User) ar.getApprover();
				composingReceipt.approve(ar.getApprover(), appUser);
				Log.customer.debug(" Approved with realUser: "+appUser);
			} else
			if(approver instanceof Group)
			{
				Log.customer.debug(" Approver is instance of Group: "+ ar.getApprover());			
				ar.setApprovedBy(aribaSystemUser);
				composingReceipt.approve(ar.getApprover(), aribaSystemUser);
				Log.customer.debug(" Approved with realUser: "+aribaSystemUser);			
			}
			else
			if(approver instanceof Role)
			{
				Log.customer.debug(" Approver is instance of Role: "+ ar.getApprover());			
				ar.setApprovedBy(aribaSystemUser);
				composingReceipt.approve(ar.getApprover(), aribaSystemUser);
				Log.customer.debug(" Approved with realUser: "+aribaSystemUser);						
			}else	
			{
				Log.customer.debug(" Approver is not an User or Group or Role: "+ ar.getApprover());
			}

		}catch (Exception e)
		{
			Log.customer.debug(" Caught exception: "+e);
		}
		
	}

	private static void addRecord(Receipt composingReceipt)
	{
		User aribaSystemUser = User.getAribaSystemUser();
		// This is a ForceSubmitRecord record
		SimpleRecord simpRecord3 = new SimpleRecord(composingReceipt,aribaSystemUser,null,"ForceSubmitRecord");
		Log.customer.debug(" simpRecord "+simpRecord3);
	}

}
