package config.java.ams.custom;

import ariba.base.core.ClusterRoot;
import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import ariba.purchasing.core.Requisition;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

/*
 This class gets called from a trigger in ReqExtrinsicFields.aml.
 jb: CSPL # 5525 - Create an Auto Comment for "Confirming Order: Do not Duplicate"
 jb: Added trigger to set BuysenseOrderConfirm Header fields from PreviousVersion of Requisition when creating a Change Order
 */

/**
 * @author Jackie Bhadange
 * @version 1 DEV
 * @reference CSPL-5525
 * @see Date 09-24-2013
 * @Explanation Added trigger to set BuysenseOrderConfirm Header fields from PreviousVersion of Requisition when creating a Change Order
 * 
 */
public class SetBuysenseOrderConfirmField extends Action
{

    public void fire(ValueSource object, PropertyTable params)
    {
        Log.customer.debug("SetBuysenseOrderConfirmField::passed object %s ", object);
        if(object != null && object instanceof Requisition)
        {
	        ClusterRoot loCurrentVersion = (ClusterRoot) object;
	        ClusterRoot loPreviousVersion = (ClusterRoot) loCurrentVersion.getDottedFieldValue("PreviousVersion");
	        
	        if (loPreviousVersion == null)
	        {
	        	Log.customer.debug("SetBuysenseOrderConfirmField::setting false to current version = %s", loCurrentVersion);
	        	loCurrentVersion.setDottedFieldValue("BuysenseOrderConfirm", new Boolean(false));
	            return;
	        }
	        else
	        {
	            // Copy Header BuysenseOrderConfirm field information from previous version of Requisition.
	            Log.customer.debug("SetBuysenseOrderConfirmField:: PreviousReq = %s", loPreviousVersion);
	            // Old value from previous req
	            Boolean bOldOrderConfirmVal = (Boolean) loPreviousVersion.getDottedFieldValue("BuysenseOrderConfirm");
	            Log.customer.debug("SetBuysenseOrderConfirmField::  previous value = %s", bOldOrderConfirmVal.toString());
	            loCurrentVersion.setDottedFieldValue("BuysenseOrderConfirm", bOldOrderConfirmVal);
	        }
        }
    }
}
