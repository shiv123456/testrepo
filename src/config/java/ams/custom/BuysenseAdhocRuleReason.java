package config.java.ams.custom;

import java.util.List;

import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovalRequest;

import ariba.util.core.ListUtil;
import ariba.util.log.Log;

    public class  BuysenseAdhocRuleReason {

    public static String getApprovables (Approvable approvable)
    {
        Log.customer.debug("The BuysenseAdhocRuleReason.java called");
        List<ApprovalRequest> vAllApprovalRequests = ListUtil.list();

        List approvalRequestsList = approvable.getApprovalRequests();
        Log.customer.debug("The BuysenseAdhocRuleReason.java::List of approval requests" +approvalRequestsList );
        
        String lsRuleReason = "eForm requires your approval";
        String lsRuleReason1 = "Buysense Exemption Request Must Approve";

        for (int i = 0; i < (approvalRequestsList.size()); i++)
        {

            ApprovalRequest ar = (ApprovalRequest) approvalRequestsList.get(i);
            Log.customer.debug("The BuysenseAdhocRuleReason.java::Inside for loop called" +ar );
            Log.customer.debug("The BuysenseAdhocRuleReason.java::Print the value of ar.getManuallyAdded() " +ar.getManuallyAdded());
            
            if (!ar.getManuallyAdded())
            {
            	Log.customer.debug("The BuysenseAdhocRuleReason.java::Inside first if" +ar.getManuallyAdded());
                vAllApprovalRequests.add(ar);
                Boolean lbreason = getAllDependencies(ar, vAllApprovalRequests);
            	Log.customer.debug("The Value returned from getAllDependencies method" +lbreason );
                if(lbreason)
            	return lsRuleReason;
                Log.customer.debug("The Value of lsRuleReason" +lsRuleReason );
            }
            
           }
		return lsRuleReason1;
        }
 

   public static Boolean getAllDependencies(ApprovalRequest approvalRequest, List<ApprovalRequest> vDependencies)
    {
	   Log.customer.debug("Inside getAllDependencies method");
	   
        List vDependencies1 = (List) approvalRequest.getDependencies();
        Log.customer.debug("Inside getAllDependencies method vDependencies1" +vDependencies1);
        Boolean reason = false;
        if (vDependencies1 != null)
        {
            ApprovalRequest dependency;
            for (int i = 0; i < vDependencies1.size(); i++)
            {
                dependency = (ApprovalRequest) vDependencies1.get(i);
                //dependency.getManuallyAdded();
                Log.customer.debug("Inside getAllDependencies method:inside for" +dependency.getManuallyAdded());
                if (dependency.getManuallyAdded())
                {
                   return true;
                }
            }
        }
		return false;

    }
   }

