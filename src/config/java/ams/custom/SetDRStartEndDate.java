package config.java.ams.custom;
import ariba.base.fields.Action;
import ariba.base.fields.ValueInfo;
import ariba.base.fields.ValueSource;
import ariba.util.core.Date;
import ariba.util.core.PropertyTable;
import ariba.workforce.core.LaborLineItemDetails;
import ariba.workforce.core.Log;
import ariba.workforce.core.TimeSheet;

/**
 *  Date 			: 08-Dec-2009
 *  CSPL           	: 1460
 *  ACP Requirement	: Created duplicate Start and End Date fields for DR extract
 *      			  and updated those fields with the Start and End Date.
 *      
 *  Date            : 14-Jun-2010
 *  CSPL            : 1914
 *  ACP Requirement : Created duplicate Project Start and Project End Date fields for DR extract
 *                    and updated those fields with the Project Start and Project End Date.
**/
public class SetDRStartEndDate extends Action
{	
    public void fire(ValueSource loObject, PropertyTable loPropertyTable)
    {    	
        String lsClassName = "SetDRStartEndDates"; 
        String START_DATE = "DRStartDate";
        String END_DATE = "DREndDate";
        String PROJECT_START_DATE = "DRProjectStartDate";
        String PROJECT_END_DATE = "DRProjectEndDate";
        String ConsultingLine_START_DATE = "DRConsultingLineStartDate";
        String ConsultingLine_END_DATE = "DRConsultingLineEndDate";

        //START::Added by SRINI for CSPL-3619
        String TimeSheet_END_DATE = "DREndPeriodDate";
        //END::Added by SRINI for CSPL-3619
        
        String lsTargetField = (String)(loPropertyTable.getPropertyForKey("DateString"));
        Log.customer.debug("lsTargetField is"+ lsTargetField);
        Date loSourceDate = null;
        Date loTargetDate = null;
        // Added null check as part of CSPL-2066
        if(loObject!=null && (loObject instanceof LaborLineItemDetails || loObject.toString().indexOf("ConsultingLineItemDetails")>=0))
        {            
            if (lsTargetField.equals(START_DATE)||lsTargetField.equals(ConsultingLine_START_DATE))
            {
                loSourceDate =(Date)loObject.getFieldValue("StartDate");
            }
            else if (lsTargetField.equals(END_DATE)||lsTargetField.equals(ConsultingLine_END_DATE))
            {
                loSourceDate =(Date)loObject.getFieldValue("EndDate");
            }
            else
            {
                Log.customer.debug(lsClassName + ": Neither StartDate nor EndDate!");
            }
        }
        // Added null check as part of CSPL-2066
        if (loObject!=null && loObject.toString().indexOf("ConsultingSharedGlobalItemProperties")>=0)
        { 
            Log.customer.debug("Inside Consulting Line Header");
            if (lsTargetField.equals(PROJECT_START_DATE))
            {
                loSourceDate =(Date)loObject.getFieldValue("ProjectStartDate");
            }
            else if (lsTargetField.equals(PROJECT_END_DATE))
            {
                loSourceDate =(Date)loObject.getFieldValue("ProjectEndDate");
            }
            else
            {
                Log.customer.debug(lsClassName + ": Neither Project StartDate nor Project EndDate!");
            }
        }
        //START::Added by SRINI for CSPL-3619
        if (loObject!= null && loObject instanceof TimeSheet)
        { 
            Log.customer.debug("Inside TimeSheet");
            if (lsTargetField.equals(TimeSheet_END_DATE))
            {
                loSourceDate =(Date)loObject.getFieldValue("EndPeriodDate");
            }
            else
            {
                Log.customer.debug(lsClassName + ": Not TS EndDate!");
            }
        }
        //END::Added by SRINI for CSPL-3619        
        try 
        {
            loTargetDate = new Date (Date.getYear((java.util.Date)loSourceDate), Date.getMonth(loSourceDate), Date.getDayOfMonth(loSourceDate));        	
            //START::Added by SRINI for CSPL-3619
            if (loObject!=null && loObject instanceof TimeSheet)
            {
                loTargetDate.setHours(loSourceDate.getHours() - 1);
            }
            else
            //END::Added by SRINI for CSPL-3619	
            {
            	loTargetDate.setHours(loSourceDate.getHours());
            }
            loTargetDate.setMinutes(loSourceDate.getMinutes());
            loTargetDate.setSeconds(loSourceDate.getSeconds());
            loObject.setFieldValue(lsTargetField, loTargetDate);
            Log.customer.debug(lsClassName + ": lsTargetField = " + lsTargetField);
            Log.customer.debug(lsClassName + ": loSourceDate = " + loSourceDate);
            Log.customer.debug(lsClassName + ": loTargetDate = " + loTargetDate);
        }
        catch (Exception e)
        {			
            e.printStackTrace();
        }
    }
    /**
        Return the list of valid value types.
   */
    protected ValueInfo getValueInfo ()
    {
        return valueInfo;
    }

    /**
        Return the parameter names.
    */
    protected ValueInfo[] getParameterInfo ()
    {
        return parameterInfo;
    }    
    public static final String ReleasePrefixParam = "DateString";
    private static ValueInfo valueInfo = new ValueInfo(0, "ariba.workforce.core.Workforce");
    private static final ValueInfo[] parameterInfo = (new ValueInfo[] {
            new ValueInfo("DateString", 0, StringClass)
        });    
}

