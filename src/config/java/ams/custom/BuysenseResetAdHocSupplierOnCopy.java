package config.java.ams.custom;


import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;
import java.util.Calendar;
import ariba.approvable.core.Record;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseVector;
import ariba.base.core.ClusterRoot;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.fields.Action;
import ariba.base.fields.Log;
import ariba.base.fields.ValueSource;
import ariba.common.core.Supplier;
import ariba.common.core.SupplierLocation;
import ariba.common.core.User;
import ariba.procure.core.SimpleProcureRecord;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.util.core.ListUtil;
import ariba.util.core.PropertyTable;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;

public class BuysenseResetAdHocSupplierOnCopy extends Action
{
    
    public void fire(ValueSource object, PropertyTable params)
      {
    	Log.customer.debug("Calling BuysenseResetAdHocSupplierOnCopy.java");
        try
        {
       
          Requisition loReq = null;
          if (object != null && object instanceof Requisition)
           {
             loReq= (Requisition) object;
             boolean lbCopiedReq = isCopiedReq(loReq);
             List llReqLineItems = loReq.getLineItems();
             for(int i =0;i<llReqLineItems.size();i++)
             {
            	 Log.customer.debug("Iterating the Line Items");
            	 ReqLineItem rli = (ReqLineItem)llReqLineItems.get(i);
                 boolean lbcontractValidation = contractValidation(rli, params);
            	 Object loSupplier = rli.getSupplier();
            	 Object loSupplierLoc = rli.getSupplierLocation();
            	 if(loSupplier != null && loSupplierLoc != null)
            	 {
            		 Log.customer.debug("The Supplier and Supplier Location is not Null");
            		 Supplier loSupp = (Supplier)loSupplier;
            		 SupplierLocation loSuppLoc = (SupplierLocation)loSupplierLoc;
            		 Object lsSuppCreator = loSupp.getCreator();
            		 Object lsSuppLocCreator = loSuppLoc.getCreator();
                     if(lsSuppCreator != null && lsSuppLocCreator != null)
                     {
                    	 Log.customer.debug("The Supplier and Supplier Location are adhoc so it must be cleared.");
                    	 rli.setFieldValue("Supplier",null);
                    	 rli.setFieldValue("SupplierLocation", null);
                     }            		 
                     else if(lsSuppCreator == null && lsSuppLocCreator != null)
                     {
                    	 Log.customer.debug("The Supplier Location are adhoc so it must be cleared.");
                    	 rli.setFieldValue("SupplierLocation", null);                 	 
                     }
            	 }
            	 if(lbCopiedReq && !lbcontractValidation)
            	 {
            		 //Log.customer.debug("BuysenseResetAdHocSupplierOnCopy:: Inside lbCopiedReq method" +lbcontractValidation  );
            	   clearContractFields(rli);
            	 }
             }
          }
        }
        catch(Exception e)
        {
            Log.customer.debug("BuysenseResetAdHocSupplierOnCopy:: exception"+e.getMessage());         
        }
      }
    public boolean isCopiedReq(Requisition reqObject)
    {
    	BaseVector lvREcords = (BaseVector) reqObject.getRecords();
    	for(int i = 0; i < lvREcords.size(); i++)
    	{
    		Record lsRecord = (Record) lvREcords.get(i);
    		if (lsRecord instanceof SimpleProcureRecord)
    		{
    			String lsType = (String)lsRecord.getFieldValue("Type");
    			String lsRecordType = (String)lsRecord.getFieldValue("RecordType");
    			if(lsType.equals("Copied") && lsRecordType.equals("CopyReqRecord"))
    			{
    				return true;
    			}
    		}
    	}
    	Log.customer.debug("BuysenseResetAdHocSupplierOnCopy:: returning false");
    	return false;
    }
    
    private static void clearContractFields(ReqLineItem rli)
    {	
        rli.setDottedFieldValueWithoutTriggering("ContractNumList", null);
        rli.setDottedFieldValueWithoutTriggering("Description.ContractNum", null);
        Log.customer.debug("BuysenseResetAdHocSupplierOnCopy::clearContractFields() cleared req line ContractNumList and ContractNum fiels");
    }
    private static boolean contractValidation(ReqLineItem reqLI, PropertyTable params)
    {
    	Log.customer.debug("BuysenseResetAdHocSupplierOnCopy::Inside contractValidation");
        String        errorStringTable            = "ariba.procure.core";
        String        expiredOnSubmitMsgKey       = "ExpiredContractOnSubmit";
        String        expiredOnApproveMsgKey      = "ExpiredContractOnApprove";
        String        selectAContractMsg          = "SelectAContract";
        String        sNotInListUniqueNameKey     = "NotInListUniqueName";
        String        sNotonContractUniqueNameKey = "NotonContractUniqueName";

        String sErrorMessage               = null;
        String        sNotinListUniqueName        = ResourceService.getString(errorStringTable,
                                                                  sNotInListUniqueNameKey);
        String        sNotonContractUniqueName    = ResourceService.getString(errorStringTable,
                                                                  sNotonContractUniqueNameKey);
        Supplier supplier = reqLI.getSupplier();
        SupplierLocation supplierLocation = reqLI.getSupplierLocation();
        Object objValue = reqLI.getDottedFieldValue("ContractNumList");
        BaseId objBaseID = (BaseId) reqLI.getDottedFieldValue("ContractNumList.BaseId");
        String sReqStatus = reqLI.getLineItemCollection().getStatusString();
        AQLResultCollection aqlResults = null;
        aqlResults = getResults(reqLI);
        List lNonContracts = ListUtil.list();
        //lNonContracts = Not In list, Not On Contract
        List lValidContracts = ListUtil.list();
        //lValidContracts = Actual contracts        
        if (aqlResults != null)
        {
            if (aqlResults.getErrors() != null)
            {
                Log.customer.debug(" Errors in results: " + aqlResults.getErrors());
                return true;
            }
            else
            {
                try
                {
                    while (aqlResults.next())
                    {
                        BaseId b = (BaseId) (BaseId) aqlResults.getObject(0);
                        ClusterRoot cObj = (ClusterRoot) b.get();
                        String sUniqueName = (String) cObj.getDottedFieldValue("UniqueName");
                        if (!StringUtil.nullOrEmptyOrBlankString(sUniqueName)
                                && (sUniqueName.equals(sNotinListUniqueName) || sUniqueName
                                        .equals(sNotonContractUniqueName)))
                        {
                            lNonContracts.add(b);
                        }
                        else
                        {
                            lValidContracts.add(b);
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.customer.debug(" Exception: " + e);
                }
            }
        }
        Log.customer.debug(" Inside contractValidation ::lValidContracts: " + lValidContracts.size());
        Log.customer.debug(" Inside contractValidation::lNonContracts: " + lNonContracts.size());

        if (supplier == null || supplierLocation == null)
        {
            return true;
        }
        else
        {
            if (objValue == null)
            {
                if (lValidContracts.isEmpty())
                {
                    //if selected supplier does not have Valid Contracts, return true
                    return true;
                }
                else
                {
                    //There are some valid contracts available to user, but user has selected none, throw error.
                    sErrorMessage = ResourceService.getString(errorStringTable, selectAContractMsg);
                    return false;
                }
            }
            else
            {
                // else if the selected object might have expired or is of different supplier, return false
                if (lNonContracts.contains(objBaseID))
                {
                    return true;
                }
                else if (lValidContracts.contains(objBaseID))
                {
                    return true;
                }
                else
                {
                    if (sReqStatus.equalsIgnoreCase("Composing"))
                    {
                        sErrorMessage = ResourceService.getString(errorStringTable, expiredOnSubmitMsgKey);
                        Log.customer.debug(" user selected value that is not a valid- returning false");
                        return false;
                    }
                    else if (sReqStatus.equalsIgnoreCase("Submitted"))
                    {
                        sErrorMessage = ResourceService.getString(errorStringTable, expiredOnApproveMsgKey);
                        Log.customer.debug(" user selected value that is not a valid- returning false");
                        return false;
                    }
                    return true;
                }
            }
        }
    }

    private static AQLResultCollection getResults(ReqLineItem reqLI)
    {
        if (reqLI != null)
        {
            Supplier loSupplier = reqLI.getSupplier();
            if (loSupplier != null)
            {
                String supplierId = loSupplier.getUniqueName();

                    String queryString = "SELECT s FROM ariba.core.BuysenseContracts s where s.SupplierID in ('" + supplierId
                            + "','ALL')";
                    Requisition req = (Requisition) reqLI.getLineItemCollection();
                    ariba.user.core.User requester = req.getRequester();
                    ariba.common.core.User partitionedUser = ariba.common.core.User.getPartitionedUser(requester, req
                            .getPartition());
                    String sClinetUniqueName = (String) partitionedUser.getDottedFieldValue("ClientName.ClientName");
                    sClinetUniqueName = sClinetUniqueName.trim().substring(0, 4);
                    if (!StringUtil.nullOrEmptyString(sClinetUniqueName))
                    {
                        queryString += " AND s.AuthorizedAgency  in ('" + sClinetUniqueName + "','ALL')";
                    }
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    df.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
                    String lsDateTime = df.format(Calendar.getInstance().getTime());
                    lsDateTime = lsDateTime +" 00:00:00";
                    queryString += " AND s.EffectiveBeginDate <= CurrentDate()";
                    queryString += " AND s.EffectiveEndDate >= CalendarDate('" + lsDateTime + "')";
                    Log.customer.debug( " query: " + queryString);
                    AQLQuery aqlQuery = AQLQuery.parseQuery(queryString);
                    AQLOptions aqlOptions = new AQLOptions(req.getPartition());
                    AQLResultCollection results = Base.getService().executeQuery(aqlQuery, aqlOptions);
                    return results;
                }

        }
        return null;
    }

}