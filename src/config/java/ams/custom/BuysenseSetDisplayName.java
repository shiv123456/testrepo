package config.java.ams.custom;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseVector;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.basic.core.Money;
import ariba.collaboration.core.CollaborationDocument;
import ariba.collaboration.core.CollaborationLineItem;
import ariba.collaboration.core.CollaborationLineItemDetails;
import ariba.collaboration.core.CollaborationRequest;
import ariba.collaboration.core.CollaborationThread;
import ariba.collaboration.core.CollaborationThreadDocument;
import ariba.collaboration.core.CounterProposal;
import ariba.collaboration.core.Proposal;
import ariba.procure.core.CategoryLineItemDetails;
import ariba.procure.core.MappableProperties;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.util.core.ListUtil;
import ariba.util.core.MapUtil;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
import ariba.workforce.core.ContractorCandidate;
import ariba.workforce.core.LaborCollaborationLineItemDetails;
import ariba.workforce.core.LaborLineItemDetails;

/**
 * CSPL #: 4147
 * @author Pavan Aluri
 * Date  : 03-Apr-2012
 * @version 2.0
 * Explanation: Modified fire() method to set custom labor line filed values for Collaboration and Proposal documents same as Req.
 */
/**
 * CSPL #: 4149
 * @author Partho Sarathi Patro
 * Date  : 10-Apr-2012
 * @version 2.0
 * Explanation: Added a new methos setDRProposalTotal set the new field DRProposalTotal wiht the ProposalTotal value fron the LaborLineItemDetails.
 */
/**
 * CSPL #: 4205
 * @author Pavan Aluri
 * Date  : 18-Apr-2012
 * @version 3.0
 * Explanation: Re-wrote the code to set the custom dr fields for all labor lines of CollaborationRequisition/Proposal/CounterProposal. 
 */
public class BuysenseSetDisplayName extends Action
{
   String msClassName = "BuysenseSetDisplayName";
   
   public void fire(ValueSource object, PropertyTable param) throws ActionExecutionException
   {
	   if(object instanceof Requisition)
	   {
		   Log.customer.debug("Class %s on method fire", msClassName);
		   Requisition loReq = (Requisition) object;
		   List loReqLIVector = loReq.getLineItems();
		   ReqLineItem loReqLine = null;
		   for (int i=0; i<loReqLIVector.size(); i++)
		   {
			   loReqLine = (ReqLineItem)loReqLIVector.get(i);
			   if(loReqLine.getCategoryLineItemDetails() != null && 
				  loReqLine.getCategoryLineItemDetails() instanceof LaborLineItemDetails &&
				  loReqLine.getCategoryLineItemDetails().getMappableProperties() != null)
			   {
				   Log.customer.debug("Class %s on method fire::Calling setDisplayName", msClassName);
				   setDisplayName(loReqLine.getCategoryLineItemDetails(), 
						          loReqLine.getCategoryLineItemDetails().getMappableProperties());
			   }
		   }
		   
		   List loAllCollabThreads = getAllCollaborationThreads(loReq);	   
		   for(int i=0; (!loAllCollabThreads.isEmpty() && i<loAllCollabThreads.size()); i++)
		   {
			   BaseId bi = (BaseId)loAllCollabThreads.get(i);
			   CollaborationThread loCollabThread = (CollaborationThread) bi.get();
			   Log.customer.debug("Class %s - thread- %s: %s", msClassName,i,loCollabThread);
			   CollaborationRequest loCollabReq = loCollabThread.getCollaborationRequest();
			   if (loCollabReq !=null)
			   {
				   Log.customer.debug("Class %s - processing req: %s", msClassName, loCollabReq.getUniqueName());
				   setCollabLineFields(loCollabReq.getLineItems());   
			   }
			   
			   List loAllDocuments = loCollabThread.getDocuments();
               for(int j=0; j<loAllDocuments.size();j++)
               {
            	   BaseId bid = (BaseId)loAllDocuments.get(j);
            	   ClusterRoot loCR = (ClusterRoot) bid.get();
            	   if(loCR instanceof Proposal && loCR != null)
            	   {
            		   Proposal loProposal = (Proposal) loCR;
    				   Log.customer.debug("Class %s - processing Proposal %s", msClassName,loProposal.getUniqueName());
    				   setCollabLineFields(loProposal.getLineItems());
    				   Log.customer.debug("Class %s - setting DRProposalTotal", msClassName);
    				   setDRProposalTotal(loProposal);  
            	   }else if(loCR instanceof CounterProposal && loCR != null)
            	   {
            		   CounterProposal loCounterProposal = (CounterProposal) loCR;
    				   Log.customer.debug("Class %s - processing Counter Proposal %s", msClassName,loCounterProposal.getUniqueName());
    				   setCollabLineFields(loCounterProposal.getLineItems());            		   
            	   }
               }
		   }
	   }
   }
   
   public void setDisplayName(CategoryLineItemDetails loCategoryLineItemDetails, MappableProperties loMappableProperties)
   {
	   String lsDisplayName = null;
	   String lsFieldValue = null;
	   String lsFieldName = null;
	   String lsDRAppend = "dr";
	   List lvMappedFields = loCategoryLineItemDetails.getCategoryItem().getMappedAttributes();
	   String lsCategoryDriver = (String) loCategoryLineItemDetails.getCategoryItem().getCategoryDriverName();
	   Log.customer.debug("Class %s on method setDisplayName::lsCategoryDriver %s", msClassName,lsCategoryDriver);
	   for (int j=0; j<lvMappedFields.size(); j++)
	   {
		   lsFieldName = ((String) lvMappedFields.get(j)).trim();
		   lsFieldValue = (String) loMappableProperties.getFieldValue(lsFieldName);
		   Log.customer.debug("Class %s on method setDisplayName::lsFieldName %s lsFieldValue %s", msClassName,lsFieldName,lsFieldValue);
		   if(lsCategoryDriver != null && lsFieldValue != null)
		   {
			   lsDisplayName = (String) getDisplayName((lsFieldName+lsCategoryDriver).trim(), lsFieldValue.trim());
			   if(lsDisplayName != null)
			   {
				   Log.customer.debug("Class %s on method setDisplayName::lsDisplayName %s", msClassName,lsDisplayName);
				   loCategoryLineItemDetails.setFieldValue((lsDRAppend+lsFieldName).trim(), lsDisplayName.trim());
			   }
		   }
	   }
   }
   
   public String getDisplayName(String lsCategoryDriver, String lsFieldValue)
   {
	   
	   Log.customer.debug("Class %s on method getDisplayName::lsCategoryDriver %s lsFieldValue %s", msClassName,lsCategoryDriver,lsFieldValue);
	   String lsDisplayString = null;
	   Partition loPart = Base.getService().getPartition("None");
	   Log.customer.debug("Class %s on method getDisplayName::loPart %s", msClassName,loPart);
	   
	   Map loParams = MapUtil.map();
	   loParams.clear();
	   loParams.put("CategoryDriver", lsCategoryDriver);
	   loParams.put("FieldValue", lsFieldValue);
       
       AQLOptions loOptions;
       AQLQuery loQuery;
       AQLResultCollection loResults;
       
       loOptions = new AQLOptions(loPart);
       loOptions.setPartition(loPart);
       loOptions.setActualParameters(loParams);
       
       String lsQuery = "SELECT mev.DisplayString.PrimaryString from ManagedEnumerationValue mev " +
                        "WHERE mev.Enumeration.Name = :CategoryDriver and mev.Value = :FieldValue";
       
       loQuery = AQLQuery.parseQuery(lsQuery);
       loResults = Base.getService().executeQuery(loQuery, loOptions);
       
       if (loResults.getFirstError() == null) 
       {
    	   while (loResults.next())
           {
    		   lsDisplayString = (String) loResults.getString(0);
           }
       }
       Log.customer.debug("Class %s on method getDisplayName::lsDisplayString %s", msClassName,lsDisplayString);
       return lsDisplayString;
   }
      
   
	private List getAllCollaborationThreads(Requisition r)
	{
		CollaborationRequest loCollbReq = null;
		List loCollabThreads = ListUtil.list();
		try {
			for (Iterator e = r.getCollaborationRequestsIterator(); e.hasNext();)
			{
				BaseId bi = (BaseId) e.next();				
				loCollbReq = (CollaborationRequest) bi.get();
				for (Iterator ce = loCollbReq.getCollaborationThreadsIterator();ce.hasNext();)
				{
					loCollabThreads.add(ce.next());
				}
			}
		} catch (Exception e) 
		{
			Log.customer.warning(8000, msClassName+ "- Exception in getAllCollaborationThreads method -"+ e.getMessage());
			return null;
		}
		return loCollabThreads;
	}   

	private void setCollabLineFields(BaseVector loCollabLines)
	   {
		   CollaborationLineItem loCollbLine = null;
		   for(int k=0; k<loCollabLines.size(); k++)
		   {
			   Log.customer.debug("Class %s processing Collab line - %s", msClassName,(k+1));
			   loCollbLine = (CollaborationLineItem) loCollabLines.get(k);
			   if(loCollbLine.getCategoryLineItemDetails() != null && 
					   loCollbLine.getCategoryLineItemDetails() instanceof LaborLineItemDetails &&
					   loCollbLine.getCategoryLineItemDetails().getMappableProperties() != null)
					   {
						   Log.customer.debug("Class %s on setting custom fields", msClassName);
						   setDisplayName(loCollbLine.getCategoryLineItemDetails(), 
								   loCollbLine.getCategoryLineItemDetails().getMappableProperties());
					   }
		   }	
	   }

	private void setDRProposalTotal(Proposal loProposal)
	   {
			  Log.customer.debug("The proposal is "+loProposal.getUniqueName());
			  List collaborationLineItem = loProposal.getLineItems();
			  for(int i=0 ; i < collaborationLineItem.size() ; i++)
			  {
				  CollaborationLineItem loCollaborationLineItemObject =  (CollaborationLineItem)collaborationLineItem.get(i);
				  CollaborationLineItemDetails loCollabLineDetails = loCollaborationLineItemObject.getCollaborationLineItemDetails();
				   if (loCollabLineDetails instanceof LaborCollaborationLineItemDetails)
				   {
					   LaborCollaborationLineItemDetails loLcliDetails = (LaborCollaborationLineItemDetails)loCollabLineDetails;
					   for (Iterator e = loLcliDetails.getContractorCandidatesIterator(); e.hasNext();)
					   {
						   ContractorCandidate loContractor = (ContractorCandidate) e.next();
						   if(loContractor != null && loContractor.getLaborLineItemDetails() != null)
							  {
								  LaborLineItemDetails lliDetails = loContractor.getLaborLineItemDetails();
								  Money proposalTotal = (Money)lliDetails.getProposalTotal();
								  Log.customer.debug("The ProposalTotal is "+proposalTotal);
								  if(proposalTotal != null)
								  {
									  Log.customer.debug("The proposal total is set as " +proposalTotal.getAmount());
									  lliDetails.setDottedFieldValue("DRProposalTotal", proposalTotal.getAmount());
								  }
								  else
								   {
									   Log.customer.debug("%s - for some reasons, got null for Proposal Total", msClassName);
								   }
							  }
						   else
						   {
							   Log.customer.debug("%s - ContractorCandidate or its LaborLineItem is null ", msClassName);
						   }

						}
					}
				   else
				   {
					   Log.customer.debug("%s - looks like this is not a labor line", msClassName);
				   }
			  }	
	   }
   
}
