package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.BaseObject;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseSetUserProfileClient extends Action
{

    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        Approvable loAppr = null;

        if (valuesource != null && valuesource instanceof Approvable)
        {
            loAppr = (Approvable) valuesource;
            Object oAgency = loAppr.getDottedFieldValue("Client");
            if (oAgency != null)
            {
                String lsClientName = (String) ((BaseObject) oAgency).getFieldValue("ClientName");
                if (lsClientName != null)
                {
                    setCustomTitle(lsClientName, loAppr);
                    loAppr.setFieldValue("Agency", lsClientName);
                    Log.customer.debug("The Agency is now" + loAppr.getFieldValue("Agency"));
                }
            }
        }
    }

    public void setCustomTitle(String clientName, Approvable appr)
    {
        String lsFirstName = (String) appr.getFieldValue("FirstName");
        String lsLastName = (String) appr.getFieldValue("LastName");
        String lsName = lsFirstName + " " + lsLastName;
        String lsTitle = "New User Request For ";
        if (lsName != null)
        {
            appr.setName(clientName + " - " + lsTitle + " " + lsName);
            Log.customer.debug("The form title is now" + appr.getFieldValue("Name"));
        }
    }
}
