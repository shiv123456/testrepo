/*
 * @(#)ValidatePOTransactionFee.java     1.0 06/26/2008
 *
 * Copyright 2008 by CGI
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of CGI ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with CGI
 */

package config.java.ams.custom;

import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.fields.*;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;

public class ValidatePOTransactionFee extends Condition
{
    private static final ValueInfo parameterInfo[];

    public boolean evaluate(Object value, PropertyTable params)
    {
        return evaluateAndExplain(value, params) == null;
    }

    public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
    {
        Log.customer.debug("Calling ValidatePOTransactionFee");
        BaseObject obj   = (BaseObject)params.getPropertyForKey("SourceObject");
	boolean lbIsAdhoc = false;
	String lsAdapterSource = null;
	AMSReqSubmitHook loSubmitHook = new AMSReqSubmitHook();
	String lsMessage = null;
	ReqLineItem loItem = null;

        if(obj.instanceOf("ariba.purchasing.core.Requisition"))
        {
            Requisition loReq = (Requisition) obj;
            try
            {
                BaseVector lvLineItems = (BaseVector)loReq.getLineItems();
                int liSize = lvLineItems.size();

                for(int i=0; i<liSize; i++)
                {
                    loItem = (ReqLineItem) lvLineItems.get(i);
                    lsAdapterSource = (String) loItem.getDottedFieldValue("SupplierLocation.AdapterSource");
                    if( StringUtil.nullOrEmptyOrBlankString(lsAdapterSource))
                    {
                         lbIsAdhoc = true;
                    }
                }
                lsMessage = loSubmitHook.CheckVendorPay(lbIsAdhoc, loReq, "");
                if(StringUtil.nullOrEmptyOrBlankString(lsMessage))
                {
                    return null;
                }
                else
                {
		    return new ConditionResult(lsMessage);
                }
            }
            catch (Exception e )
	    {
                 e.printStackTrace();
                 Log.customer.debug("Catch exception: vendorPay is not a boolean value.");
                 lsMessage = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","VendorPayNull");
                 return new ConditionResult(lsMessage);
	    }
        }
        return null;
    }
    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }
    static
    {
         parameterInfo = (new ValueInfo[]{ new ValueInfo("SourceObject", 0)});
    }
}
