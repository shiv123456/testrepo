package config.java.ams.custom;

import java.util.List;

import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueInfo;
import ariba.base.fields.ValueSource;
import ariba.common.core.Address;
import ariba.user.core.Group;
import ariba.user.core.Role;
import ariba.util.core.ListUtil;
import ariba.util.core.PropertyTable;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public abstract class BuysenseUMDMWSPostLoad extends Action
{

    private String                sClassName                 = "BuysenseUMDMWSPostLoad";
    protected ClusterRoot         cObj;

    protected final int           iError                     = 2;
    protected final int           iSuccess                   = 0;
    protected final int           iWarning                   = 1;
    protected String              sResourceFile              = "buysense.WebServices";
    protected String              sServiceRefKey             = " ";
    private String                sInernalServiceReply       = "SUCCESS";

    private static final String   SERVICEREFNAME_CSV         = "ServiceRefName";
    private static final String   ALLOWEDOPERATIONS_CSV      = "AllowedOperations";
    private static final String   CREATESERVICE_CSV          = "CreateService";
    private static final String   SERVICEVARIANT_CSV         = "Variant";
    private static final String   SERVICEPARTITION_CSV       = "Partition";
    private static final String   CREATESOAPREQELEMENTS_CSV  = "CreateSOAPReqElements";
    private static final String   CREATESOAPRPLYELEMENTS_CSV = "CreateSOAPRplElements";

    private static final String   CREATESOAPMSGFRAME_CSV     = "CreateSOAPMsgFrame";

    private static final String   REQUIREDFIELDS_CSV         = "RequiredFields";
    protected static final String REFERENCEFIELDS_CSV        = "ReferenceFields";

    abstract boolean checkIfUniqueAribaInstanceExists(ClusterRoot oCustomObj);

    // define all values retrieved from CSV as class variables
    private String sWebServiceName;
    private String sVariant;
    private String sPartition;
    private String sAllowedOperations;
    private String sSOAPReqElements;
    private String sSOAPMsgFrame;
    private String sSOAPRplElements;
    private String sReferenceFieldList;
    private String sRequiredFields;

    // --JB -- reference fields should be used for Create/Update operations and not for delete

    @SuppressWarnings("rawtypes")
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        log(sClassName + " fire called " + valuesource);
        if (valuesource == null || !(valuesource instanceof ClusterRoot))
        {
            // error message and step out
            return;
        }

        cObj = (ClusterRoot) valuesource;
        try
        {
            // Example for Address ServiceRefName is AddressService
            sServiceRefKey = propertytable.stringPropertyForKey(SERVICEREFNAME_CSV);

            // clear status fields before doing anything
            // If custom object UniqueName(BuysenseWSAddress.UniqueName) is same then same object is used by ariba
            clearStatus(cObj);

            // clean up all the variables retrieved from CSV file
            cleanCSVVariables();

            // Initialize all variables retrieved from CSV file
            sWebServiceName = ResourceService.getString(sResourceFile, sServiceRefKey + CREATESERVICE_CSV);
            sVariant = ResourceService.getString(sResourceFile, sServiceRefKey + SERVICEVARIANT_CSV);
            sPartition = ResourceService.getString(sResourceFile, sServiceRefKey + SERVICEPARTITION_CSV);
            sAllowedOperations = ResourceService.getString(sResourceFile, sServiceRefKey + ALLOWEDOPERATIONS_CSV);
            sSOAPReqElements = ResourceService.getString(sResourceFile, sServiceRefKey + CREATESOAPREQELEMENTS_CSV);
            sSOAPMsgFrame = ResourceService.getString(sResourceFile, sServiceRefKey + CREATESOAPMSGFRAME_CSV);
            sSOAPRplElements = ResourceService.getString(sResourceFile, sServiceRefKey + CREATESOAPRPLYELEMENTS_CSV);
            sReferenceFieldList = ResourceService.getString(sResourceFile, sServiceRefKey + REFERENCEFIELDS_CSV);
            sRequiredFields = ResourceService.getString(sResourceFile, sServiceRefKey + REQUIREDFIELDS_CSV);

            // Operation is operation set in custom object. Example: BuysenseWSAddress.Operation can have 'Add'/'Update'/'Delete' operations
            String sCustomObjOperation = (String) cObj.getDottedFieldValue("Operation");
            log(sClassName + "sCustomObjOperation " + sCustomObjOperation);
            // check if it's valid operation - one of the 3 - 'Add'/'update'/'delete'
            if (!BuysenseUMDMWSOperations.isValidCustomOperation(sCustomObjOperation) || sServiceRefKey == null)
            {
                // not a valid operation in web service call. Register error
                appendStatus(cObj, iError, "", "Not a valid operation");
                return;
            }

            // check if this operation is allowed for particular web service. Example: 'Update' is not allowed for 'Address'
            // Only 'ADD' and 'DELETE' are supported. Stored in 'AddressServiceAllowedOperations' in csv file

            List lAllowedOperations = ListUtil.arrayToList(sAllowedOperations.split(","));

            if (!BuysenseUMDMWSOperations.isValidCustomOperationForService(lAllowedOperations, sCustomObjOperation))
            {
                // not a valid operation in web service call. Register error
                appendStatus(cObj, iError, "", "Operation not allowed:" + sCustomObjOperation);
                return;
            }

            // check for required fields irrespective of the operation.
            // Make sure required fields are same for all operations

            if (!StringUtil.nullOrEmptyOrBlankString(sRequiredFields) && !hasRequiredFieldValuesCheck(cObj))
            {
                // not a valid operation in web service call. Register error
                appendStatus(cObj, iError, "", "One or more of the required fields are not sent or null or blank:"
                        + sRequiredFields);
                return;
            }

            log(sClassName + " checking for clientID");
            // check for ClientID if available in required fields. It should be required for all web service requests
            // hard coding 'ClientID' here
            if (sRequiredFields != null && !StringUtil.nullOrEmptyOrBlankString(sRequiredFields)
                    && sRequiredFields.matches("(.*)ClientID(.*)"))
            {
                Object oClientID = cObj.getDottedFieldValue("ClientID");
                log(sClassName + "oClientID " + oClientID);
                if (oClientID == null || !(oClientID instanceof String)
                        || StringUtil.nullOrEmptyOrBlankString((String) oClientID)
                        || (!(((String) oClientID).matches("EVA001") || ((String) oClientID).matches("EVA002"))))
                {
                    // incorrect client ID
                    appendStatus(cObj, iError, "", "Incorrect ClientID. Only value of EVA001 or EVA002 is allowed");
                    return;
                }
            }

            log(sClassName + " check for reference fields");

            // unique field checks - if 'add' then object with uniquename should not be found
            // if 'update'/'delete' then uniquename should exist. what about activationin case of update??? -- JB
            // check for reference data if operation is add/update
            if (BuysenseUMDMWSOperations.isAddOperation(sCustomObjOperation)
                    || BuysenseUMDMWSOperations.isUpdateOperation(sCustomObjOperation))
            {
                // ex for Address - "UniqueName:ariba.common.core.Address:UniqueName:pcsv:Required,Name:Required,ClientNameUniqueName:ariba.core.BuysenseClient:UniqueName:pcsv:Required"
                if (!StringUtil.nullOrEmptyOrBlankString(sReferenceFieldList))
                {
                    log(sClassName + " sReferenceFieldList:" + sReferenceFieldList);
                }
                else
                {
                    log(sClassName + " sReferenceFieldList is blank or empty");
                }
                if (!StringUtil.nullOrEmptyOrBlankString(sReferenceFieldList)
                        && !isRefDataCheckSuccessful(cObj, sReferenceFieldList))
                {
                    log(sClassName + "One or more of the reference fields are not validated");
                    // need to cleanup the list sent in status message. current one sends field, type etc.
                    appendStatus(cObj, iError, "", "One or more of the reference fields are not validated:"
                            + sReferenceFieldList);
                    return;
                }
            }

            // invoke ariba specific operation operation - 'create'/'update'/'delete
            if (BuysenseUMDMWSOperations.isAddOperation(sCustomObjOperation))
            {
                // invoke ariba create operation
                // check if unique object exists before creating
                if (checkIfUniqueAribaInstanceExists(cObj))
                {
                    // unique instance exists, return with error
                    appendStatus(cObj, iError, "", "This data already exists in eMall, cannot execute ADD operation");
                    return;
                }
                create();

            }
            else if (BuysenseUMDMWSOperations.isUpdateOperation(sCustomObjOperation))
            {
                // check if unique object exists before updating
                if (!checkIfUniqueAribaInstanceExists(cObj))
                {
                    // unique instance exists, return with error
                    appendStatus(cObj, iError, "", "This data does not exist in eMall, cannot execute UPDATE operation");
                    return;
                }
                update();
            }
            else if (BuysenseUMDMWSOperations.isDeleteOperation(sCustomObjOperation))
            {
                if (!checkIfUniqueAribaInstanceExists(cObj))
                {
                    // unique instance exists, return with error
                    appendStatus(cObj, iError, "", "This data does not exist in eMall, cannot execute DELETE operation");
                    return;
                }
                delete();
            }
        }
        catch (Exception e)
        {
            appendStatus((ClusterRoot) valuesource, iError, "FAILURE",
                    "EXCEPTION in Ariba while processing the service");
            log(sClassName + "EXCEPTION in Ariba while processing the service " + e.getMessage());
            return;
        }
        finally
        {
            // any cleanup if needed
        }
        
    }// end of fire function

    @SuppressWarnings("rawtypes")
    private boolean hasRequiredFieldValuesCheck(ClusterRoot cObj2)
    {

        // Resource file has following format
        // "AddressServiceCreateRequiredFields","UniqueName,ClientID,ClientName"

        log(sClassName + " sRequiredFields " + sRequiredFields);
        List lRequiredFields = ListUtil.arrayToList(sRequiredFields.split(","));

        if (cObj2 != null && (lRequiredFields != null && !lRequiredFields.isEmpty()))
        {
            for (int i = 0; i < lRequiredFields.size(); i++)
            {
                String sCustomObjFieldName = (String) lRequiredFields.get(i);
                Object oFieldValueObj = cObj2.getDottedFieldValue(sCustomObjFieldName);
                if (oFieldValueObj == null)
                {
                    // Add error message
                    log(sClassName + " Required Field not found:" + lRequiredFields.get(i));
                    return false;
                }

                // Check for empty value if it is string
                if (oFieldValueObj instanceof String && StringUtil.nullOrEmptyOrBlankString((String) oFieldValueObj))
                {
                    // Add error message saying
                    log(sClassName + " Required Field not found:" + lRequiredFields.get(i));
                    return false;
                } // end of empty or null string
            }// end of for loop
        }
        return true;
    }

    protected void create()
    {
        // call the create service. Always assume it is going to be internal web service call
        // Other classes can override 'create' method if custom logic is to be executed
        callCreateService();
    }

    protected void update()
    {
        // UPDATE has to be implemented in respective sub class

    }

    @SuppressWarnings({ "unused" })
    protected void delete()
    {
        String sMissingRequiredFields = "";
        // for now passing hard coded required field list for Address object.

        // mandate field/s format - "ClassType:partition"
        // ex for Address Delete - "ariba.common.core.Address:pcsv"

        String sDeleteObjectTypes = ResourceService.getString(sResourceFile, sServiceRefKey + "DeleteObjectTypes");
        log(sClassName + "sDeleteObjectTypes " + sDeleteObjectTypes);
        //ariba.user.core.Role:None,ariba.user.core.Group:None

        boolean bDeactivated = true;
        try
        {
            // get the object(s) to deactivate.
            String sObjtoDeactivate[] = sDeleteObjectTypes.split(",");
            for (int j = 0; j < sObjtoDeactivate.length; j++)
            {
                String s[] = sObjtoDeactivate[j].split(":");
                //ariba.user.core.Role:None
                String sObjectClassType = s[0];
                String sObjectPartition = s[1];
                String sUniqueName = (String) cObj.getFieldValue("UniqueName");
                Partition partition = Base.getService().getPartition(sObjectPartition);

                ClusterRoot clr = Base.getService().objectMatchingUniqueName(sObjectClassType, partition, sUniqueName);
                if (clr != null)
                {
                    deactivateClusterRoot(clr);
                }
                else
                {
                    // unable to find the object in Ariba.
                    bDeactivated = false;
                    continue;
                }
            }
            if (bDeactivated)
            {
                appendStatus(cObj, iSuccess, "SUCCESS", "Object Deactivated SUCCESSFULLY");
                return;
            }
            else
            {
                appendStatus(cObj, iError, "FAILURE", "Unable to find some or all of the objects to deactivate ");
                return;
            }
        }
        catch (Exception e)
        {
            log(sClassName + "EXCEPTION while deactivating objects");
            appendStatus(cObj, iError, "FAILURE", "EXCEPTION while deactivating objects");
            return;
        }
    }

    private void deactivateClusterRoot(ClusterRoot clr)
    {
        //this object can be active or inactive.
        clr.setActive(false);
        //bDeactivated = true; // 0X (need to set to FALSE, if it is FALSE in previous iterations)
        /*if (clr.getActive())
        {
            clr.setActive(false);
            appendStatus(cObj, iSuccess, "SUCCESS", "Object Deactivated SUCCESSFULLY");
            return;
        }
        else
        {
            // object is already inactive
            clr.setActive(false);
            appendStatus(cObj, iWarning, "SUCCESS with WARNING", "Object already in Deactivated status");
            return;
        }*/

        //set appropriate AdapterSource value
        if (clr instanceof Address)
        {
            clr.setAdapterSource("pcsv:AddressDelete.csv");
        }
        if (clr.getUiName().equalsIgnoreCase("BuysenseFieldDataTable"))
        {
            clr.setAdapterSource("pcsv:BuysenseFieldDataTableDelete.csv");
        }
        if (clr instanceof Role)
        {
            clr.setAdapterSource("None:Role.csv");
        }
        if (clr instanceof Group)
        {
            clr.setAdapterSource("None:Group.csv");
        }
    }

    protected void callCreateService()
    {

        List<String> lCheckFields = ListUtil.list();
        lCheckFields.add(sVariant);
        lCheckFields.add(sPartition);
        lCheckFields.add(sWebServiceName);
        lCheckFields.add(sSOAPMsgFrame);
        lCheckFields.add(sSOAPReqElements);

        boolean bCheckWSCSVFields = checkWSCSVFields(lCheckFields);
        if (!bCheckWSCSVFields)
        {

            appendStatus(cObj, iError, "",
                    "All elments required to fire web service not there in CSV. Ask eMall team to take a look");
            return;
        }

        String sSOAPRequestMsg = BuysenseUMDMWSSoap.getSOAPmsg(sVariant, sPartition, sSOAPMsgFrame, sSOAPReqElements,
                sSOAPRplElements, sInernalServiceReply, cObj);

        log("SOAP msg - " + sSOAPRequestMsg.substring(0, sSOAPRequestMsg.length() / 2));
        log("SOAP msg - " + sSOAPRequestMsg.substring((sSOAPRequestMsg.length() / 2)));

        // call respective service
        log(sClassName + "calling service");
        boolean bWSCallSuccessful = BuysenseUMDMWSSoap.invokeWebservice(sWebServiceName, sPartition, sSOAPRequestMsg,
                sSOAPRplElements, sInernalServiceReply);
        log(sClassName + "done calling service invokeWebservice");

        // process response and set appropriate status
        if (!bWSCallSuccessful)
        {
            appendStatus(cObj, iError, "", "Error in invoking ariba web service. Ask eMall team to take a look");
            return;
        }
    }

    @SuppressWarnings({ "rawtypes" })
    protected boolean isRefDataCheckSuccessful(ClusterRoot cObj2, String refDataList)
    {
        // lRefDataFields has reference data details for particular web service
        // There could be multiple values separated by comma that need to be checked. One value is defined by 6 fields
        // Example:"AddressServiceReferenceFields","UniqueName:String:ariba.core.BuysenseClient:UniqueName:pcsv:Single:String","Ref data check fields"
        // s0->UniqueName (field on customObject)
        // s1->ariba.core.BuysenseClient (reference object to be checked)
        // s2->UniqueName (reference object field to be checked)
        // s3->pcsv (partition for reference object)
        // s4->Single (type - single field or a vector??) NOT SURE if we need it
        // s5->String (reference object type)
        boolean isRefDataCheckSuccess = false;

        if (StringUtil.nullOrEmptyOrBlankString(refDataList))
        {
            log(sClassName + " reference data list is blank");
            return isRefDataCheckSuccess;
        }

        // Break the to get one value
        List lRefDataFields = ListUtil.arrayToList(refDataList.split(","));

        for (int i = 0; i < lRefDataFields.size(); i++)
        {
            // ex for Address - ClientName:String:ariba.core.BuysenseClient:UniqueName:pcsv:Single:String
            String s = (String) lRefDataFields.get(i);
            String s1[] = s.split(":");
            String sCustomObjFieldName = s1[0]; // ClientName
            String sCustomObjFieldType = s1[1]; // String
            String sRefClassName = s1[2]; // ariba.core.BuysenseClient
            String sRefFieldName = s1[3]; // need this field??
            String sRefPartition = s1[4]; //pcsv
            String sMultiple = s1[5];// valid values are 'Single', 'Multiple'
            String sRefObjFieldType = s1[6];

            List<String> lCheckFields = ListUtil.list();
            lCheckFields.add(sCustomObjFieldName);
            lCheckFields.add(sCustomObjFieldType);
            lCheckFields.add(sRefClassName);
            lCheckFields.add(sRefPartition);
            lCheckFields.add(sMultiple);
            lCheckFields.add(sRefObjFieldType);

            boolean bCheckWSCSVFields = checkWSCSVFields(lCheckFields);
            if (!bCheckWSCSVFields)
            {
                //
                appendStatus(cObj, iError, "",
                        "All elments required to check reference data are not there in CSV. Ask eMall team to take a look");
                return false;
            }

            Object oCustomFieldValueObj = cObj2.getDottedFieldValue(sCustomObjFieldName);
            Partition sPart = Base.getService().getPartition(sRefPartition);

            if (sPart == null)
            {
                log(sClassName +" No partition object exist as defined in CSV");
                return false;
            }

            // safe to assume the custom unique lookup would be based on String??. If not, failing the ref check
            if (sCustomObjFieldType == null || !sCustomObjFieldType.equalsIgnoreCase("String"))
            {
                log(sClassName +" sCustomObjFieldType is not a string");
                return false;
            }

            if (StringUtil.equalsIgnoreCase(sMultiple, "Single") && (oCustomFieldValueObj instanceof String))
            {
                // got to make sure partition is defined correctly, else value may not be retrieved
                ClusterRoot clr = Base.getService().objectMatchingUniqueName(sRefClassName, sPart,
                        (String) oCustomFieldValueObj);
                if (clr == null) // reference object exists then return true
                {
                    log(sClassName +" unique object not found. sRefClassName:" + sRefClassName
                            + " oCustomFieldValueObj:" + (String) oCustomFieldValueObj);
                    return false;
                }
                else
                {
                    isRefDataCheckSuccess = true;
                }
            }
            else if (StringUtil.equalsIgnoreCase(sMultiple, "Multiple"))
            {
                // THIS should be done in individual classes
                // return false
                if (true)
                {
                    return false;
                }
            } // end of elseif for 'Multiple'
        }// end of for
        return isRefDataCheckSuccess;

    }

    protected void setStatus(ClusterRoot cObj2, int iStatusCode, String sStatusMessage, String sStatusLongMessage)
    {
        log(sClassName + " Setting status " + iStatusCode + ";" + sStatusMessage);
        cObj2.setDottedFieldValue("WSStatusCode", iStatusCode);
        cObj2.setDottedFieldValue("WSStatusMessage", sStatusMessage);
        cObj2.setDottedFieldValue("WSStatusLongMessage", sStatusLongMessage);
    }

    // append status if you want to add more messages, update code only if goes from success-> warning
    // OR warning->error
    // Append WSStatusMessage and WSStatusLongMessage
    protected void appendStatus(ClusterRoot cObj2, int iStatusCode, String sStatusMessage, String sStatusLongMessage)
    {
        log(sClassName + " appending status " + iStatusCode + ";" + sStatusMessage);

        Object oStatusCode = cObj2.getDottedFieldValue("WSStatusCode");

        if (oStatusCode == null)
        {
            cObj2.setDottedFieldValue("WSStatusCode", iStatusCode);
        }
        else
        {
            int sCode = ((Integer) cObj2.getDottedFieldValue("WSStatusCode")).intValue();
            if (sCode < iStatusCode) // only if current code is higher then update code
            {
                cObj2.setDottedFieldValue("WSStatusCode", iStatusCode);
            }
        }

        String sAppendStatusMessage = (String) cObj2.getDottedFieldValue("WSStatusMessage");
        String sAppendStatusLongMessage = (String) cObj2.getDottedFieldValue("WSStatusLongMessage");

        sAppendStatusMessage = StringUtil.nullOrEmptyOrBlankString(sAppendStatusMessage) ? sStatusMessage
                : sAppendStatusMessage + ';' + sStatusMessage;
        sAppendStatusLongMessage = StringUtil.nullOrEmptyOrBlankString(sAppendStatusLongMessage) ? sStatusLongMessage
                : sAppendStatusLongMessage + ';' + sStatusLongMessage;

        cObj2.setDottedFieldValue("WSStatusMessage", sAppendStatusMessage);
        cObj2.setDottedFieldValue("WSStatusLongMessage", sAppendStatusLongMessage);
    }

    // check if any csv fields are null or blank/empty
    @SuppressWarnings("rawtypes")
    private boolean checkWSCSVFields(List lCsvFields)
    {
        for (int i = 0; i < lCsvFields.size(); i++)
        {
            if (lCsvFields == null || StringUtil.nullOrEmptyOrBlankString((String) lCsvFields.get(i)))
            {
                return false;
            }
            log(sClassName + " field value:" + lCsvFields.get(i));
        }
        return true;
    }

    private void cleanCSVVariables()
    {
        sWebServiceName = "";
        sVariant = "";
        sPartition = "";
        sAllowedOperations = "";
        sSOAPReqElements = "";
        sSOAPMsgFrame = "";
        sSOAPRplElements = "";
        sReferenceFieldList = "";
        sRequiredFields = "";
    }

    private void clearStatus(ClusterRoot cObj2)
    {
        cObj2.setDottedFieldValue("WSStatusCode", 0);
        cObj2.setDottedFieldValue("WSStatusMessage", "");
        cObj2.setDottedFieldValue("WSStatusLongMessage", "");
    }

    protected void log(String s)
    {
        Log.customer.debug(s);
    }

    protected ValueInfo getValueInfo()
    {
        return valueInfo;
    }

    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    private static final ValueInfo   valueInfo     = new ValueInfo(0);
    private static final ValueInfo[] parameterInfo = { new ValueInfo(SERVICEREFNAME_CSV, IsScalar, StringClass) };

}
