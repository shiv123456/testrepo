package config.java.ams.custom;

import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import ariba.common.core.SupplierLocation;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;


public class BuysenseSetLLC extends Action
{
    public void fire(ValueSource object, PropertyTable params)
    {
    	Log.customer.debug("Calling BuysenseSetLLC.");
        if(object instanceof SupplierLocation)
        {
        	SupplierLocation loSL = (SupplierLocation) object;
        	String lsOrgType = (String) loSL.getFieldValue("OrganizationType");
        	if(lsOrgType != null && !lsOrgType.startsWith("Limited Liability Co."))
            {
        		Log.customer.debug("BuysenseSetLLC : setting value for LLC");
        		loSL.setFieldValue("LLCClassification", null);
            }
        }
    }
}

