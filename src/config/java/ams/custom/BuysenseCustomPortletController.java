package config.java.ams.custom;

import ariba.util.log.Log;
import ariba.htmlui.orms.portlets.BuyerPortletController;

public class BuysenseCustomPortletController extends BuyerPortletController
{

    protected String viewComponentName()
    {
        return BuysenseEformLinksCustomPortletContent.class.getName();
    }

    protected String editComponentName()
    {
        Log.customer.warning(8888, " Edit not allowed for BuysenseCustomPortletController Portlet");
        return null;
    }

    public static final String ClassName = "config.java.ams.custom.BuysenseCustomPortletController";

}
