package config.java.ams.custom;

import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.Partition;
import ariba.common.core.CommonSupplier;
import ariba.user.core.OrganizationID;
import ariba.user.core.OrganizationIDPart;
import ariba.util.log.Log;
import ariba.util.scheduler.ScheduledTask;
import ariba.util.scheduler.Scheduler;
import java.util.Map;


/**
Scheduled Task Entry
    BuysenseSetDunsDF = {
        ScheduledTaskClassName = "config.java.ams.custom.BuysenseSetDunsDF";
        CS_QUERY = "Select c from CommonSupplier c where SystemID like '%buyer%' and AdapterSource != ''";
    };
*/

public class BuysenseSetDunsDF extends ScheduledTask
{
	private final String cn = this.getClass().getName();
	public static final String Duns_Domain = "duns";
	public static final String Internal_Domain = "internalsupplierid";

	private String msQuery = null;

    public void init (Scheduler scheduler, String scheduledTaskName, Map parameters)
    {
    	super.init(scheduler, scheduledTaskName, parameters);
        String lsKey = null;

        for(java.util.Iterator e = parameters.keySet().iterator(); e.hasNext();)
        {
        	lsKey = (String)e.next();
        	if (lsKey.equals("CS_QUERY")) msQuery = (String)parameters.get(lsKey);
        }
    }
    
    /**
     * See query for the results coming inside for processing
     * Each CS from results is looped through to check whether it contains DUNS value
     * If not then new OrganizationIDPart is created with DUNS domain\value pair and added to the CS
     */
    public void run ()
    {
    	Log.customer.debug("***%s***run***Called run::Query %s", cn, msQuery);
    	AQLOptions loOptions = new AQLOptions(Partition.None);
	    AQLQuery loCSQuery = AQLQuery.parseQuery(msQuery);
        AQLResultCollection loResults = Base.getService().executeQuery(loCSQuery, loOptions);
        if (loResults.getErrors() == null && loResults.getSize()>0)
        {
            while (loResults.next())
            {
                BaseId id = (BaseId) loResults.getObject(0);
                CommonSupplier loCS = (CommonSupplier) id.get();
                OrganizationID loOrgIDs = loCS.getOrganizationID();
                String lsInternalValue = loOrgIDs.getValueForDomain(Internal_Domain);
                String lsDunsValue = loOrgIDs.getValueForDomain(Duns_Domain);
                Log.customer.debug("***%s***run***CS Name %s Internal Sup ID %s Duns ID %s", cn, loCS.getName(), lsInternalValue, lsDunsValue);
                if(lsDunsValue == null && lsInternalValue != null)
                {
                	createAddDunsDomain(lsInternalValue, loOrgIDs, Duns_Domain);
                	loCS.save();
                	Log.customer.debug("***%s***run:After Creation***CS Name %s Internal Sup ID %s Duns ID %s", cn, loCS.getName(), loOrgIDs.getValueForDomain(Internal_Domain), loOrgIDs.getValueForDomain(Duns_Domain));
                }
            }
        }
    }
    
    /**
     * Made it as public static method so that we can try to use this from java script
     * This is not tested and confirmed through java script 
     */
    public static void createAddDunsDomain(String internalSupID, OrganizationID orgID, String dunsDomain)
    {
    	OrganizationIDPart loOrgIDPart = new OrganizationIDPart(Partition.None, dunsDomain, internalSupID);
    	orgID.addIfAbsent(loOrgIDPart);
    	Log.customer.debug("***BuysenseSetDunsDF***createAddDunsDomain***Internal Sup ID %s Duns ID %s", orgID.getValueForDomain(Internal_Domain), orgID.getValueForDomain(Duns_Domain));
    }
}