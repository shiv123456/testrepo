package config.java.ams.custom;

import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.Comment;
import ariba.approvable.core.PrintApprovableHook;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseSession;
import ariba.base.core.LongString;
import ariba.approvable.core.ApprovalRequest;
import ariba.basic.core.Money;
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;
import ariba.util.formatter.BigDecimalFormatter;
import ariba.util.log.Log;
import ariba.util.net.HTMLPrintWriter;

public class BuysenseODUEmergencyProcurementPrint implements PrintApprovableHook
{
    private static final List NoErrorResult = ListUtil.list(Constants.getInteger(NoError));
    Hashtable hHdrParams = new Hashtable();

    public List run(Approvable approvable, PrintWriter out, Locale locale, boolean printForEmail)
    {
        HTMLPrintWriter loHout = new HTMLPrintWriter(out);
        String lsHtmlPage=null;
        String lsReqType=approvable.getTypeName();
        if (printForEmail)
        {
            Log.customer.debug("PrintForEmail is true");
            return NoErrorResult;
        }
        try
        {
            if (lsReqType.equals("ariba.core.BuysenseODUEmergencyProcurement"))
            {
                int liTag = 0, liTagEnd = -1;
                String lsTag = null;
                lsHtmlPage = PSGFunctions.ReadFile("config/htmlui/resource/ariba.core.ODUEmergencyProcurement.html");
                liTag = lsHtmlPage.lastIndexOf("<!--Hdr.");
                liTagEnd = -1;
                Object loFieldVal=null;
                while (liTag >= 0)
                {
                    liTagEnd = lsHtmlPage.indexOf("-->", liTag + 1);
                    lsTag = lsHtmlPage.substring(liTag + 4, liTagEnd);
                    Log.customer.debug("HdrKey: " + lsTag);
                    hHdrParams.put(lsTag, "[?]");
                    liTag = lsHtmlPage.lastIndexOf("<!--Hdr.", liTag - 1);
                }
                Enumeration loEnumParams = hHdrParams.keys();
                while (loEnumParams.hasMoreElements())
                {
                    lsTag = loEnumParams.nextElement().toString();
                    String sFieldName = lsTag.substring(4);
                    if(sFieldName.contains("$"))
                    {
                        int liFirstIndex=sFieldName.indexOf('$');
                        int liLastIndex=sFieldName.indexOf('$', liFirstIndex+1);
                        String lsFieldName=sFieldName.substring(liLastIndex+2);
                        String lsStringObj=sFieldName.substring(liFirstIndex+1,liLastIndex);
                        loFieldVal = getFieldValue(lsFieldName,lsStringObj,approvable);
                        Log.customer.debug("field value "+loFieldVal);
                    }
                    else if(sFieldName.equals("TotalAmount") && approvable.getDottedFieldValue(sFieldName) != null)
                        {
                            Money loProcMoney=(Money)approvable.getDottedFieldValue(sFieldName);
                            loProcMoney = new Money(BigDecimalFormatter.round(loProcMoney.getAmount(), 2), loProcMoney.getCurrency());
                            loFieldVal= (loProcMoney.asString()).substring(0, (loProcMoney.asString().indexOf(".")+3)).concat(" ").concat(loProcMoney.getCurrency().getSuffix());
                            Log.customer.debug("Value of ProcurementMoney field  : "+loFieldVal);
                        }
                    else if(sFieldName.contains("Preparer") || sFieldName.contains("Requester"))
                        {
                            String lsObjval=sFieldName.substring(0,sFieldName.indexOf("@"));
                            Log.customer.debug("Object value: "+lsObjval);
                            String lsFieldVal=sFieldName.substring(sFieldName.indexOf("@")+1);
                            Log.customer.debug("field value: "+lsFieldVal);
                            loFieldVal=getReqValue(lsObjval,lsFieldVal,approvable);
                        }
                    else if(sFieldName.equals("username"))
                        {
                            String loStatus=(String)approvable.getDottedFieldValue("StatusString");
                            if(loStatus.equals("Denied"))
                            {
                                loFieldVal="Denied by:";
                            }
                            else
                            {
                                loFieldVal="Approved by:";
                            }
                        }
                    else if(sFieldName.equals("date"))
                        {
                            String loStatus=(String)approvable.getDottedFieldValue("StatusString");
                            if(loStatus.equals("Denied"))
                            {
                                loFieldVal="Denied Date:";
                            }
                            else
                            {
                                loFieldVal="Approved Date:";
                            }
                        }
                    else
                        {
                            loFieldVal = approvable.getDottedFieldValue(sFieldName);
                        }
                    if (loFieldVal == null)
                    {
                        loFieldVal = "";
                    }
                    hHdrParams.put(lsTag, String.valueOf(loFieldVal));
                }
                lsHtmlPage = PSGFunctions.ReplaceHtml(lsHtmlPage, hHdrParams);
                loHout.println(lsHtmlPage);
            }
        }
        catch (Exception e)
        {
            Log.customer.debug(e);
        }
        return NoErrorResult;
    }

    public String getFieldValue(String fieldName,String stringObj,Approvable approvable)
    {
        Log.customer.debug("Inside getFieldValue method, fieldName :"+fieldName);
        String lsResultValue="";
        List loObjlist=null;
        Object loObjectValue=null;
        if(stringObj.equalsIgnoreCase("ApprovalRequests"))
        {
            loObjlist = getAllApprovalRequests(approvable);
        }
        else
        {
            loObjlist = (List)approvable.getFieldValue(stringObj);
        }
        Log.customer.debug("list:" +loObjlist);
        BaseSession loSession = Base.getSession();
        for (int i = 0; i < loObjlist.size(); i++)
        {
            if(i>0)
            {
                if(fieldName.equals("Text"))
                {
                    lsResultValue=lsResultValue+"<br>";
                }
                else if(!loObjectValue.equals(""))
                    {
                        lsResultValue=lsResultValue+", ";
                    }
            }
            Object obj = loObjlist.get(i);
            BaseObject loObjectName = null;
            try
            {
                if(obj instanceof Comment)
                {
                    Comment loCmt = (Comment)loObjlist.get(i);
                    loObjectName = loCmt;
                }
                else if(obj instanceof ApprovalRequest)
                    {
                        ApprovalRequest loAppReq=(ApprovalRequest)loObjlist.get(i);
                        loObjectName = loAppReq;
                    }
                else
                    {
                        BaseId loId = (BaseId)loObjlist.get(i);
                        loObjectName = loSession.objectFromId(loId);
                    }
                Log.customer.debug("Inside for of getFieldValue, object:"+loObjectName);

                if(fieldName.equals("Text"))
                {
                    LongString llsText = (LongString) loObjectName.getDottedFieldValue(fieldName);
                    if(llsText != null)
                    {
                        loObjectValue = (String) llsText.string();
                    }
                }
                else
                    {
                        loObjectValue = loObjectName.getDottedFieldValue(fieldName);
                    }
                Log.customer.debug("field name " +loObjectValue);
                if (loObjectValue == null)
                    loObjectValue = ""; // replace NullValue with blank.
                lsResultValue=lsResultValue+String.valueOf(loObjectValue);
            }
            catch(Exception e)
            {
                Log.customer.debug("Excception: "+e.getMessage());
                lsResultValue = "EXCEPTION";
            }
        }
        return lsResultValue;
    }

    public String getReqValue(String objName,String fieldName,Approvable approvable)
    {
        ariba.user.core.User loUser = (ariba.user.core.User)approvable.getDottedFieldValue(objName);
        ariba.common.core.User loPartitionUser = ariba.common.core.User.getPartitionedUser(loUser,approvable.getPartition());
        Object loFieldValue = loPartitionUser.getDottedFieldValue(fieldName);
        if (loFieldValue == null)
        {
            loFieldValue = "";
        }
        Log.customer.debug("Object value in getReqValue mathod: " +loFieldValue);
        String lsResultValue=loFieldValue.toString();
        return lsResultValue;
    }
    public List getAllApprovalRequests(Approvable approvable)
    {
        List lvApprovalRequestsVector;
        List lvecApprovalRequests;
        ApprovalRequest loApprovalRequest;

        lvApprovalRequestsVector = (List)approvable.getFieldValue("ApprovalRequests");
        lvecApprovalRequests = ListUtil.cloneList(lvApprovalRequestsVector);
        for (int i = 0; i < lvecApprovalRequests.size(); i ++)
        {
            loApprovalRequest = (ApprovalRequest) lvecApprovalRequests.get(i);
            lvApprovalRequestsVector = getDependencies(lvecApprovalRequests, loApprovalRequest);
        }
        return lvecApprovalRequests;
    }

    public List getDependencies(List lvec, BaseObject appReq)
    {
        List vecNewDependencies = ListUtil.list();
        ApprovalRequest newDependency = new ApprovalRequest();
        vecNewDependencies = (List)appReq.getFieldValue("Dependencies");
        int size = vecNewDependencies.size();

        for (int i = 0; i < size; i ++)
        {
            newDependency = (ApprovalRequest) vecNewDependencies.get(i);
            if (lvec.contains(newDependency))
            {
                continue;
            }
            else
            {
                lvec.add(newDependency);
                getDependencies(lvec, newDependency);
            }
        }
        return lvec;
    }
}

