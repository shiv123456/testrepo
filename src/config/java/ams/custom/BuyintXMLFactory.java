/*
 * @(#)BuyintXMLFactory.java      2004/09/03
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information"). You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 * PVCS Modification :
 *
 * $Log:   R:\Software\development\archives\Ariba 8.x Customizations\app\buyer\Server\classes\extensions\config\java\ams\custom\BuyintXMLFactory.java-arc  $
 * 
 *    Rev 1.6   28 Apr 2006 15:54:44   rlee
 * Adde ReqNum for PO also. Dev SPL 629 - INT - Add REQUISITION_NUMBER Column to EXPORT_TABLE.
 * 
 *    Rev 1.5   23 Mar 2006 11:32:38   rlee
 * Dev SPL 629 - INT - Add REQUISITION_NUMBER Column to EXPORT_TABLE.
 *
 *    Rev 1.4   13 Mar 2006 16:58:02   rlee
 * Dev SPL 629 - INT - Add REQUISITION_NUMBER Column to EXPORT_TABLE.
 *
 *    Rev 1.3   16 Jun 2005 17:10:16   dchamber
 * VEPI DEV SPL 576: Modified the submit and getClientName methods to set the new 'ClientName' field in the Export_Data table.
 *
 *    Rev 1.2   28 Sep 2004 16:44:12   nrao
 * A8 Dev SPL #109: Added code to output DTD at start of XML.
 *
 *    Rev 1.1   08 Sep 2004 11:12:24   nrao
 * A8 Dev SPL #86: Changed to use BuyintDBRec subclass. Also, changed to use Log.customer.debug rather than System.err.println.
 *
 *    Rev 1.0   07 Sep 2004 17:30:12   cm
 * Initial revision.
 *
 *
 */
package config.java.ams.custom;

import ariba.purchasing.core.Requisition;
import org.w3c.dom.*;
import org.xml.sax.*;
import java.io.*;
import java.util.*;
import javax.xml.parsers.*;
import ariba.util.log.Log;
import ariba.base.core.*;
import ariba.base.fields.*;
import ariba.util.core.*;
import java.math.BigDecimal;
/**
 * <code>BuyintXMLFactory</code> provides XML generation capability. It has two primary
 * functions. First, it acts as the manager of <code>BuyintXMLContainer</code> objects,
 * allowing XML templates to be parsed once, and used multiple times. Second, it implements
 * the submit method for use by classes that need to render and persist Ariba/buysense
 * documents as XML for pushing to external ERP systems.
 *
 * This class was developed for the Ariba 8.1 Upgrade.
 *
 * @author Simha Rao
 * @version 1.0
 */
//81->822 changed Hashtable to Map
public class BuyintXMLFactory extends Action implements BuyintConstants
{
    private static final String XML_PREFIX_SEP = "_";    // separator between prefix and xml name
    private static final String XML_SUFFIX = ".xml";     // expected file extension for xml files

    private static java.util.Map moDocSet;         // collection of BuyintXMLContainer objects
    private static java.util.Map moDTDSet;         // collection of DTD File content strings
    private static DocumentBuilderFactory moDOMFactory;  // see DOM
    private static DocumentBuilder moDOMBuilder;         // see DOM

    private static Partition moPart = null;              // Ariba Partition object
    private static BuyintDBIO moDBIO = null;             // DBIO object for persistence of XML

    /* Run time parameters */
    private static String msXMLDir = null;               // name of XML directory
    private static String msDBJNDIName = null;           // JNDI name for DB Connection
    private static String msDBURL = null;                // DB URL for DB Connection
    private static String msDBUser = null;               // DB User name for DB Connection
    private static String msDBPassword = null;           // DB Password for DB Connection

    /**
     * Static initializer for <code>BuyintXMLFactory</code>.
     */
    static
    {
        init();
    }

    /**
     * <code>init</code> allows initialization/re-initialization of <code>BuyintXMLfactory.
     * It is not public as system startup is the only thing that should be initializing
     * the factory. It has been provided as a convenience for later implementation of
     * re-initialization in the event of initialization errors.
     *
     */
    private static synchronized void init()
    {
        try
        {
            // get the partition object for the session
            moPart = Base.getSession().getPartition();
            if (moPart == null)
            {
                debug("moPart is null");
                moPart = Base.getService().getPartition("pcsv");
            }

            // get parameters from Parameter.table
            msDBJNDIName = Base.getService().getParameter(moPart,"Application.AMSParameters.BuyintDBJNDIName");
            msDBURL = Base.getService().getParameter(moPart,"Application.AMSParameters.BuyintDBURL");
            msDBUser = Base.getService().getParameter(moPart,"Application.AMSParameters.BuyintDBUser");
            msDBPassword = Base.getService().getParameter(moPart,"Application.AMSParameters.BuyintDBPassword");
            msXMLDir = Base.getService().getParameter(moPart, "Application.AMSParameters.BuyintXMLDir");

            debug("Values from Parameter table - "
                 + "XMLDir = " + msXMLDir + ", "
                 + "JNDIName = " + msDBJNDIName + ", "
                 + "DBURL = " + msDBURL + ", "
                 + "DBUser = " + msDBUser + ", "
                 + "DBPassword = " + msDBPassword);

            msDBJNDIName = null;          // ngr set JNDI name to null for now ...
            // instantiate dbio object ... use jndi name if provided
            if (msDBJNDIName != null)
            {
                debug("About to instantiate DBIO for JNDI Name = " + msDBJNDIName);
                moDBIO = new BuyintDBIO(msDBJNDIName);
            }
            else
            {
                debug("About to instantiate DBIO for JDBC Params " + msDBURL + "|" + msDBUser + "|" + msDBPassword);
                moDBIO = new BuyintDBIO(msDBURL, msDBUser, msDBPassword);
            }

            // create document builder stuff
            debug("About to instantiate DocumentBuilderFactory ... ");
            moDOMFactory = DocumentBuilderFactory.newInstance();
            moDOMFactory.setValidating(true);
            debug("About to create new DocumentBuilder ... ");
            moDOMBuilder = moDOMFactory.newDocumentBuilder();

            // read and load all templates
            loadAllTemplates();
        }
        catch (Exception loEx)
        {
            loEx.printStackTrace();
            destroy();
        }
    }

    /** Private destroy method ... not expected to be called except if static initializer
     *  fails. If destroy is called, XML generation will not be possible unless
     * <code>init</code> can be invoked again.
     */
    private static synchronized void destroy()
    {
        moDocSet = null;
        moDOMFactory = null;
        moDOMBuilder = null;
        moPart = null;
        moDBIO = null;
    }


    /** Not sure why this method is here ...
     */
    public void fire(ValueSource object, PropertyTable params)
    {
    }

   /**
    * <code>submit</code> is the primary interface to XML generation from within
    * a custom Ariba extension class. The combination of transaction type and
    * Ariba document object determine the template to be used for XML generation.
    *
    * @param  fsTransactionType A String matching one of the predefined TXNTYPE
    *                           constants in <code>BuyintConstants</code>.
    * @param  foClusterRoot     The Ariba document (Requisition, ERPOrder etc.) that is the
    *                           basis for XML generation.
    *
    * @return true if the submit is successful, false if it fails.
    *
    */
    public static synchronized boolean submit(String fsTransactionType, ClusterRoot foClusterRoot)
        //throws BuyintException
    {
        boolean lboolResult = false;
        Requisition loReq = null;
        String lsTemplate = null;
        BuyintXMLWriter loXMLWriter = null;
        BuyintExceptionHandler loHandler = new BuyintExceptionHandler();
        StringBuffer loXMLBuffer = new StringBuffer();
        String lsDocID = foClusterRoot.getUniqueName();
        String lsDocType = (String)foClusterRoot.getTypeName();
        String lsClientName = (String)getClientName(foClusterRoot);
        String lsReqNumber = null; 
        String lsPONumber = (String)getPONumber(foClusterRoot);
        int liReqVersion = 0;
        int liPOVersion = (int)getPOVersion(foClusterRoot);
        lsDocType = (String)BuyintXWalk.XWalk.changeNewToOld(lsDocType);
        if (!StringUtil.nullOrEmptyOrBlankString(lsPONumber))
        {
            loReq = (Requisition)foClusterRoot.getDottedFieldValue("LineItems[0].Requisition");
            lsReqNumber = (String)getReqNumber(loReq);
            liReqVersion = (int)getReqVersion(loReq);
        }
        else
        {
            lsReqNumber = (String)getReqNumber(foClusterRoot);
            liReqVersion = (int)getReqVersion(foClusterRoot);
        }
        BigDecimal ldTotalCost = (BigDecimal)getTotalCost(foClusterRoot);

        debug("Entered submit ... "
             + "transaction type is " + fsTransactionType
             + ", docType is " + lsDocType
             + ", docID is " + lsDocID);
        try
        {
            lsTemplate = getTemplateName(fsTransactionType, foClusterRoot);
            loXMLWriter = new BuyintXMLWriter(foClusterRoot);
            loXMLWriter.setContainer(getContainer(lsTemplate));
            debug("Finished setting up XMLContainer ... about to render document/message");

            // At this point, only buysenseDoc.dtd is supported
            loXMLBuffer = loXMLWriter.renderDoc();

            debug("Result of loXMLWriter.render is: " + loXMLBuffer);
            debug("About to save transaction ... ");

            lboolResult = save(lsDocType, lsDocID, fsTransactionType, loXMLBuffer, lsClientName, lsReqNumber, liReqVersion, lsPONumber, liPOVersion, ldTotalCost);
            debug("save() returned a " + lboolResult);
        }
        catch (BuyintException loEx1)
        {
            loEx1.printStackTrace();
            loHandler.handleException(loEx1, fsTransactionType, foClusterRoot, loXMLBuffer.toString(), "");
        }
        catch (Exception loEx2)
        {
            loEx2.printStackTrace();
            BuyintException loEx2a = new BuyintException(loEx2.getMessage(), BuyintException.EXCPT_XMLGEN_ERROR);
            loHandler.handleException(loEx2a, fsTransactionType, foClusterRoot, loXMLBuffer.toString(), "");
        }
        return lboolResult;
    }

   /**
    * <code>save</code> servers as a wrapper for the save method in the <code>BuyintDBRecExportData</code>
    * object used for persisting the submitted transaction.
    *
    * @param  fsDocType          The type of document or Ariba Object name.
    * @param  fsDocID            The Unique ID of the document.
    * @param  fsTransactionType  The current transaction type.
    * @param  fsStringBuffer     The buffer containing the XML rendered by <code>BuyintXMLWriter</code>.
    *
    * @return true if the save is successful, false if it fails.
    *
    * @throws BuyintException    if any unexpected error is encountered.
    */
    private static boolean save(String fsDocType, String fsDocID, String fsTransactionType, StringBuffer foXMLBuffer, String fsClientName, String fsReqNumber, int fiReqVersion, String fsPONumber, int fiPOVersion, BigDecimal fdTotalCost)
        throws BuyintException
    {
        BuyintDBRecExportData loRec = new BuyintDBRecExportData(moDBIO);

        loRec.setDocumentType(fsDocType);
        loRec.setDocumentID(fsDocID);
        loRec.setTransactionType(fsTransactionType);
        loRec.setTransactionData(foXMLBuffer.toString());
        loRec.setStatus(STATUS_NEW);
        loRec.setRetryCount(0);
        loRec.setErrorMessage("New transaction");
        loRec.setClientName(fsClientName);
        loRec.setReqNumber(fsReqNumber);
        loRec.setPONumber(fsPONumber);
        loRec.setTotalCost(fdTotalCost);

        if (!(StringUtil.nullOrEmptyOrBlankString(fsReqNumber)))
        {
           loRec.setReqVersion(fiReqVersion);
        }
        else
        {
           String fsReqVersion = "";
           loRec.setReqVersion(fsReqVersion);
        }

        if (!(StringUtil.nullOrEmptyOrBlankString(fsPONumber)))
        {
           loRec.setPOVersion(fiPOVersion);
        }
        else
        {
           String fsPOVersion = "";
           loRec.setPOVersion(fsPOVersion);
        }

        boolean loFlag = loRec.save();
        loRec.destroy();
        return loFlag;
    }

   /**
    * This method is only expected to be called during <code>init</code>.
    * It reads all the XML templates in the XML directory specified. For each template
    * read, it parses the template into a DOM Document, initializes a <code>BuyintXMLContainer</code>
    * to contain the Document, and caches the pre-parsed document against the template name.
    * The <code>BuyintXMLContainer</code> objects can be utilized by <code>BuyintXMLWriter</code>
    * to render the output XML.
    *
    */
    private static synchronized void loadAllTemplates()
        throws BuyintException
    {
        try
        {
            moDocSet = new HashMap(5);
            moDTDSet = new HashMap(2);

            FilenameFilter loFilter = new FilenameFilter()
            {
                public boolean accept(File foDir, String fsName)
                {
                     return (!fsName.startsWith(".") && fsName.endsWith(XML_SUFFIX));
                }
            };

            File loDir = new File(msXMLDir);

            String[] loFiles = loDir.list(loFilter);
            if (loFiles == null) {
                // Either loDir does not exist or is not a directory
            }
            else
            {
                for (int lix=0; lix<loFiles.length; lix++)
                {
                    debug("Loading " + loFiles[lix] + " ...");
                    File loXMLFile = new File(msXMLDir + System.getProperty("file.separator") + loFiles[lix]);
                    Document loDomDoc = moDOMBuilder.parse(loXMLFile);
                    BuyintXMLContainer loXMLContainer = new BuyintXMLContainer(loDomDoc);
                    moDocSet.put(loFiles[lix], loXMLContainer);
                    saveDTD(loDomDoc);
                }
            }
        }
        catch (SAXException loEx1)
        {
            loEx1.printStackTrace();
            throw new BuyintException("Error loading XML files from directory " + msXMLDir, BuyintException.EXCPT_XMLGEN_ERROR);
        }
        catch (IOException loEx2)
        {
            loEx2.printStackTrace();
            throw new BuyintException("Error loading XML files from directory " + msXMLDir, BuyintException.EXCPT_XMLGEN_ERROR);
        }
    }

   /**
    * <code>reloadTemplate</code> is a convenience method that allows a specific XML template
    * to be reloaded into the cache. Note that this will affect all subsequent invocations
    * of <code>submit</code>. Use with caution.
    *
    * @param  fsXMLFile The name of the XML template file. The file is assumed to be present
    *                   in the current XML directory.
    * @throws BuyintException if any error is encountered.
    *
    */
    public static synchronized void reloadTemplate(String fsXMLFile)
        throws BuyintException
    {
        try
        {
            debug("Reloading " + fsXMLFile + " ...");
            File loXMLFile = new File(msXMLDir + System.getProperty("file.separator") + fsXMLFile);
            Document loDomDoc = moDOMBuilder.parse(loXMLFile);
            BuyintXMLContainer loXMLContainer = new BuyintXMLContainer(loDomDoc);
            moDocSet.put(fsXMLFile, loXMLContainer);
            saveDTD(loDomDoc);
        }
        catch (SAXException loEx1)
        {
            loEx1.printStackTrace();
            throw new BuyintException("Error reloading file " + fsXMLFile + " from directory " + msXMLDir, BuyintException.EXCPT_XMLGEN_ERROR);
        }
        catch (IOException loEx2)
        {
            loEx2.printStackTrace();
            throw new BuyintException("Error reloading file " + fsXMLFile + " from directory " + msXMLDir, BuyintException.EXCPT_XMLGEN_ERROR);
        }
    }

   /**
    * Checks to see if the named template is already stored in the cache.
    *
    * @param  fsTemplate The name of the XML template.
    *
    */
    private static boolean isCached(String fsTemplate)
    {
        return moDocSet.containsKey(fsTemplate);
    }

   /**
    * Gets the default template name for the passed Ariba document, assuming a transaction
    * type of <code>BuyintConstants.TXNTYPE_SEND</code>.
    *
    * @param  foAribaObject The Ariba document (Requisition/ERPOrder) object to be rendered.
    * @return The name of the SEND template for the passed document.
    *
    */
    private static String getTemplateName(ClusterRoot foAribaObject)
        throws BuyintException
    {
        // SEND is the default transaction type
        return getTemplateName(TXNTYPE_SEND, foAribaObject);
    }

   /**
    * Gets the template name for the passed transaction type and Ariba document.
    *
    * @param  foAribaObject     The Ariba document (Requisition/ERPOrder) object.
    * @param  fsTransactionType A valid TXNTYPE from <code>BuyintConstants</code>.
    * @return The name of the template for the passed transaction type and Ariba document.
    *
    */
    private static String getTemplateName(String fsTransactionType, ClusterRoot foAribaObject)
        throws BuyintException
    {
        String lsTemplate;         // generic template name
        String lsClientTemplate;   // client-specific template

        // Attempt to get the template name from the Class name
        lsTemplate = foAribaObject.getClass().getName();
        if (lsTemplate.indexOf(".") > 0)
        {
            lsTemplate =
                lsTemplate.substring(lsTemplate.lastIndexOf(".")+1);
        }

        // append transaction type to front of template name if not SEND (default)
        if (!fsTransactionType.equals(TXNTYPE_SEND))
        {
            lsTemplate = fsTransactionType + XML_PREFIX_SEP + lsTemplate;
        }

        // Append xml file type to template name
        lsTemplate += XML_SUFFIX;
        lsClientTemplate = getClientName(foAribaObject);

        if (lsClientTemplate != null)
        {
            // ClientName not null? Prefix ClientName to XML name ...
            lsClientTemplate += XML_PREFIX_SEP + lsTemplate;
            if (isCached(lsClientTemplate))
            {
                lsTemplate = lsClientTemplate;
            }
            else
            {
                lsClientTemplate = null;
            }
        }

        if (lsClientTemplate == null)
        {
            if (!isCached(lsTemplate))
            {
                throw new BuyintException("Unable to find matching XML template for object", BuyintException.EXCPT_XMLGEN_ERROR);
            }
        }

        return lsTemplate;
    }

   /**
    * Gets the ClientName for the current Ariba document. Used to find out if there is a clientname-specific
    * template in the XML directory.
    *
    * @param  foAribaObject     The Ariba document (Requisition/ERPOrder) object.
    *
    * @return The buysense ClientName String.
    *
    */
    private static String getClientName(ClusterRoot foAribaObject)
    {
        ariba.user.core.User loUser = null;
        ariba.common.core.User loRequester = null;
        String lsClientName = "";

        // Set the Requester/Preparer based on object type
        if (foAribaObject.instanceOf("ariba.purchasing.core.PurchaseOrder"))
        {
           loUser = (ariba.user.core.User)foAribaObject.getDottedFieldValue("LineItems[0].Requisition.Requester");
           loRequester = ariba.common.core.User.getPartitionedUser(loUser,foAribaObject.getPartition());
        }
        else
        {
           loUser = (ariba.user.core.User)foAribaObject.getDottedFieldValue("Requester");
           loRequester = ariba.common.core.User.getPartitionedUser(loUser,foAribaObject.getPartition());
        }

        // Set clientid, clientname, etc. based on requester
        lsClientName = (String)loRequester.getDottedFieldValue("BuysenseOrg.ClientName.ClientName");

        return lsClientName;
    }

    // getReqNumber
    private static String getReqNumber(ClusterRoot foAribaObject)
    {
        String lsReqNumber = "";
        if (foAribaObject.instanceOf("ariba.purchasing.core.Requisition"))
        {
            lsReqNumber = (String)foAribaObject.getDottedFieldValue("UniqueName");
        }
        return lsReqNumber;
    }

    // getPONumber
    private static String getPONumber(ClusterRoot foAribaObject)
    {
        String lsPONumber = "";
        if (foAribaObject.instanceOf("ariba.purchasing.core.PurchaseOrder"))
        {
            lsPONumber = (String)foAribaObject.getDottedFieldValue("UniqueName");
        }
        return lsPONumber;
    }

    // getReqVersion
    private static int getReqVersion(ClusterRoot foAribaObject)
    {
        int liReqVersion = 1;
        if (foAribaObject.instanceOf("ariba.purchasing.core.Requisition"))
        {
            liReqVersion = ((Integer)foAribaObject.getDottedFieldValue("VersionNumber")).intValue();
        }
        return liReqVersion;
    }

    // getPOVersion
    private static int getPOVersion(ClusterRoot foAribaObject)
    {
        int liPOVersion = 1;
        if (foAribaObject.instanceOf("ariba.purchasing.core.PurchaseOrder"))
        {
            liPOVersion = ((Integer)foAribaObject.getDottedFieldValue("VersionNumber")).intValue();
        }
        return liPOVersion;
    }

    // getTotalCost
    private static java.math.BigDecimal getTotalCost(ClusterRoot foAribaObject)
    {
        BigDecimal ldTotalCost = (java.math.BigDecimal)foAribaObject.getDottedFieldValue("TotalCost.Amount");
        debug("rleee inside getTotalCost = " + ldTotalCost);

        return ldTotalCost;
    }
   /**
    * Gets the <code>BuyintXMLContainer</code> for the current template from cache.
    *
    * @param  fsTemplateName The name of the XML template.
    *
    * @return The cached <code>BuyintXMLContainer</code> object for the template name.
    *
    */
    private static synchronized BuyintXMLContainer getContainer(String fsTemplateName) throws BuyintException
    {
        BuyintXMLContainer loContainer = null;

        if (!isCached(fsTemplateName))
        {
            throw new BuyintException("Unable to get document for template \"" + fsTemplateName + "\"", BuyintException.EXCPT_XMLGEN_ERROR);
        }
        else
        {
            loContainer = (BuyintXMLContainer)moDocSet.get(fsTemplateName);
        }

        return loContainer;
    }

   /**
    * Saves the DTD Contents in cache.
    *
    * @param  foDoc  The Document object whose reference DTD is to be saved.
    *
    */
    private static void saveDTD(Document foDoc)
    {
        DocumentType loDoctype = foDoc.getDoctype();
        String lsFile = loDoctype.getSystemId();
        String lsContent = null;

        if (!isCachedDTD(lsFile))    // if not already saved
        {
            lsContent = getFileAsString(msXMLDir + System.getProperty("file.separator") + lsFile);
            debug("Saving " + lsFile + ": " + lsContent);
            moDTDSet.put(lsFile, lsContent);
        }
    }

   /**
    * Gets the DTD file contents.
    *
    * @param  foDoc  The Document object whose reference DTD is to be retrieved.
    *
    */
    public static String getDTD(Document foDoc) throws BuyintException
    {
        return getDTD(foDoc.getDoctype().getSystemId());
    }

   /**
    * Gets the DTD file contents.
    *
    * @param  fsFile  The name of the DTD file.
    *
    */
    public static String getDTD(String fsFile) throws BuyintException
    {
        String lsContent = null;

        if (!isCachedDTD(fsFile))
        {
            throw new BuyintException("Unable to get DTD contents for file \"" + fsFile + "\"", BuyintException.EXCPT_XMLGEN_ERROR);
        }
        else
        {
            lsContent = (String)moDTDSet.get(fsFile);
        }

        return lsContent;
    }

   /**
    * Checks to see if the named DTD is already stored in the cache.
    *
    * @param  fsFile The name of the DTD file.
    *
    */
    private static boolean isCachedDTD(String fsFile)
    {
        return moDTDSet.containsKey(fsFile);
    }

   /**
     * Returns the contents of a file as linefeed separated string
     */
    public static String getFileAsString(String fileName)
    {
        String filestring="";
        File _file=new File(fileName);
        FileInputStream in;
        ByteArrayOutputStream out;

        try
        {
            in=new FileInputStream(_file);
            byte[] buffer=new byte[4096];
            out = new ByteArrayOutputStream();

            int bytes_read;
            while ((bytes_read = in.read(buffer)) != -1)
            {
                out.write(buffer, 0, bytes_read);
                //String(byte[] bytes, int offset, int length) method can also be used
            }
            filestring = out.toString();
            in.close();
            out.close();
         }
         catch (Exception e) {
             e.printStackTrace();
         }
         return filestring;
    }

   /**
    * Convenience method for debug message output.
    *
    * @param  fsMsg The text of the message to be printed.
    *
    */
    public static void debug(String fsMsg)
    {
        Log.customer.debug("BuyintXMLFactory: " + fsMsg);     // ONLINE
    }

   /*
    *
    */
    public static void main( String args[])
    {
        BuyintXMLContainer loContainer = null;
        try
        {
//          PrintStream loStream = new PrintStream(new FileOutputStream("simha.out", true));
            debug("About to get Requisition.xml");
            loContainer = BuyintXMLFactory.getContainer("Requisition.xml");
            loContainer.print();
            debug("About to get ERPOrder.xml");
            loContainer = BuyintXMLFactory.getContainer("ERPOrder.xml");
            loContainer.print();
            debug("About to get CNCL_Requisition.xml");
            loContainer = BuyintXMLFactory.getContainer("CNCL_Requisition.xml");
            loContainer.print();
            debug("About to get CNCL_ERPOrder.xml");
            loContainer = BuyintXMLFactory.getContainer("CNCL_ERPOrder.xml");
            loContainer.print();
            debug("About to get Bad.xml");
            loContainer = BuyintXMLFactory.getContainer("Bad.xml");
            loContainer.print();
        }
        catch (BuyintException loEx1)
        {
            loEx1.printStackTrace();
        }
//      catch (FileNotFoundException loEx2)
//      {
//          loEx2.printStackTrace();
//      }
        System.exit(0);
    }
}
