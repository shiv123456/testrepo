/*
    Responsible: Shane Liu 8.1, Dev SPL #10, FieldPropertiesFilter for dynamically setting labels on field.

    In AML, field has property "fieldPropertiesFilterClass="config.java.ams.custom.BuysenseLabelProperty" set up,
    then, this java method getPropertyForkey will be called, and string returned will become that field's label.

    TransientData is used to retrieved info on the Requester of the current Requisition being viewed.
*/


package config.java.ams.custom;

import ariba.base.core.*;
import ariba.base.fields.*;
import ariba.htmlui.fieldsui.Log;

public class BuysenseLabelProperty implements FieldPropertiesFilter
{
    public Object getPropertyForKey(String propName, String fieldName, FieldProperties fp)
    {
        TransientData td =  Base.getSession().getTransientData();

		String clientUniqueName = (String)td.get("AMSLabel");
		Log.customer.debug("Retrieve client Name "+clientUniqueName);
        if (clientUniqueName == null)
        {
	        Log.customer.debug("Found this client: %s"+clientUniqueName);
        	return fp.getPropertyForKey("Label");
		}

        return BuysenseUtil.findFieldLabel(clientUniqueName, fieldName);
    }
}
