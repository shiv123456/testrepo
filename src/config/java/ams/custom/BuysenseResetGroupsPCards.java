package config.java.ams.custom;

import java.util.List;
import java.util.Map;

import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
import ariba.base.core.Base;

public class BuysenseResetGroupsPCards extends Action
{
	public final static String ClassName = "config.java.ams.custom.BuysenseResetGroupsPCards";

    public void fire(ValueSource object, PropertyTable params)
    {
    	Log.customer.debug("Calling %s", ClassName);

        Map<?, ?> loUserInfo = (Map<?, ?>)params.getPropertyForKey("UserInfo");
        String lsTopicName = null;
        if(loUserInfo != null)
        {
           lsTopicName = (String)loUserInfo.get("TopicName");
        }
        Log.customer.debug("%s :: lsTopicName %s", ClassName, lsTopicName);

        if(object instanceof ariba.user.core.User && lsTopicName != null && lsTopicName.trim().equals("SharedUserLoad_CSV"))
        {
    		ariba.user.core.User loSharedUser = (ariba.user.core.User) object;
    		List<?> loSensitiveUsers = Base.getService().getVectorParameter(null, "System.Admin.BuysenseSensitiveUsers");
    		if(loSharedUser != null && loSharedUser.getUniqueName() != null && !loSensitiveUsers.contains(loSharedUser.getUniqueName()))
    		{
	    		if(ariba.common.core.User.getPartitionedUser(loSharedUser, Base.getService().getPartition("pcsv")) != null &&
	   			   ariba.common.core.User.getPartitionedUser(loSharedUser, Base.getService().getPartition("pcsv")).getPCards().size() > 0)
	   			{
	   				ariba.common.core.User.getPartitionedUser(loSharedUser,Base.getService().getPartition("pcsv")).getPCards().clear();
	   				Log.customer.debug("%s :: After clearing pcards :: size %s", ClassName, ariba.common.core.User.getPartitionedUser(loSharedUser,Base.getService().getPartition("pcsv")).getPCards().size());
	   				loSharedUser.save();
	   			}
    		}
   		}
    }
}

