package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class EvaSendMessageAribaReqToQQ extends EvaSendMessageAribaImpl
{

    private static String sClassName      = "EvaSendMessageAribaReqToQQ";
    private static String queueName = null;

    protected String getQueueName() throws Exception
    {
        Log.customer.debug(sClassName + " in getQueueName() - queueName= "+queueName);
        
        if (!StringUtil.nullOrEmptyOrBlankString(queueName))
        {
            return queueName;
        }

        // for ariba get the Endpoint from P.table

        Partition moPart = null;
        moPart = Base.getService().getPartition("pcsv");
        
        if (moPart == null)
        {
            moPart = Base.getSession().getPartition();
        }
        
        Log.customer.debug(sClassName + ": fetching QueueName from P.table");
        queueName = (String) Base.getService().getParameter(moPart, "Application.AMSParameters.EvaMessageQueueNameReqToQQ");
        if(StringUtil.nullOrEmptyOrBlankString(queueName))
        {
            queueName = "evamq.dev.ariba.qqreqs";
        }
        Log.customer.debug(sClassName + " in getQueueName() - returning queueName= "+queueName);
        return queueName;
    }
}
