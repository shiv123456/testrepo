/*
    Copyright (c) 1996-2000 Ariba, Inc.
    All rights reserved. Patents pending.

    $Id$

    Responsible: imohideen
*/

/* 08/13/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 09/11/2003: Updates for Ariba 8.0 (Ricard Lee) */
/* 10/10/2003: Updates for Ariba 8.1 (Ricard Lee) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */
/* labraham, 08/24/2004: Ariba 8.1 Upgrade Dev SPL # 93 - Replaced Role class with new Group class */
/* labraham, 10/19/2004: Ariba 8.1 Upgrade Dev SPL # 57 - Rewrote the class to account for the new 8.1 User functionality - 
                         can no longer use getobjectmatchinguniquename() to lookup User objects */

package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.*;
import ariba.util.log.Log;
import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
// Ariba 8.0: not needed the two import ariba.server.objectserver...
//import ariba.server.objectserver.adapter.*;
//import ariba.server.objectserver.core.AdapterError;
// Ariba 8.0: not needed the OrderReciptientOnServer
//import ariba.procure.server.OrderRecipientOnServer;
import ariba.util.core.PropertyTable;
//Ivan Start
// Ariba 8.1 import ariba.common.core.User;
import ariba.user.core.User;
// Ariba 8.1 Dev SPL #93 - replaced Role class with Group class
// Ariba 8.1 import ariba.common.core.Role;
//import ariba.user.core.Role;
import ariba.user.core.Group;
// Ariba 8.1 Dev SPL #57 - imported new Approver class
import ariba.user.core.Approver;
//Ivan End

// Mark Harley Jan 5, 2001 - modified the code to work with other object types.  If the CSV
// column "FieldName" contains a ";" then the code will parse the object name (first half) and
// the field name (second half).  If it does not contain a ";" then it will assume the object
// type is ariba.core.BuysenseOrg.

/**
    @aribaapi internal
*/
public class BuysenseOrgLinkApproversPostLoad extends Action
{
    Partition loPartition = Base.getService().getPartition("supplierdirect");
    String PASSWORDADAPTER = Base.getService().getParameter(loPartition, "Application.Authentication.PasswordAdapter"); 
    public static final String ClassName =
        "config.java.ams.custom.BuysenseIntPostLoad";
    
    public static final String RequisitionImportErrorEmail =
        "RequisitionImportErrorEmail";
    
    protected static final String SubmitParam = "Submit";
    
    private static final String[] requiredParameterNames = 
        {
        SubmitParam 
    };
    
    /**
        Fire Method
        object - BuysenseOrgLinkApprovers
        params - Ariba defined
    */
    public void fire (ValueSource object, PropertyTable params)
    {
        Log.customer.debug("The post load got called");
        Log.customer.debug("Received this object %s", object);
        
        String  ObjectFieldNameCombo;
        String  ObjectName;
        String  FieldName;
        //
        //Ivan Start
        //
        BaseObject BSOLA = (BaseObject) object;
        Log.customer.debug("IIB: After BSOLA cast");
        String BSOLAUniqueName = (String)BSOLA.getFieldValue("UniqueName");
        
        Log.customer.debug("UniqueName:FieldName: " + BSOLAUniqueName);
        
        //Strip out all but the first element in the UniqueName.
        int Index = BSOLAUniqueName.indexOf(":");
        Log.customer.debug("Index: " + Index);
        if(Index != -1)
        {
            Log.customer.debug("Got in Index if");
            BSOLAUniqueName = BSOLAUniqueName.substring(0,Index).trim();
        }
        
        ObjectFieldNameCombo = (String) BSOLA.getFieldValue("FieldName");
        
        Log.customer.debug("Combo = " + ObjectFieldNameCombo);
        
        // MH - parse string, determine what object type is being passed.
        int i = ObjectFieldNameCombo.indexOf(";");
        
        // Evaluate what object type is being passed and process accordingly
        if (i != -1)
        {
            /*
               Processing an object type that is NOT a BuysenseOrgLinkApprover 
            */
            ObjectName = new String(ObjectFieldNameCombo.substring(0,i));
            FieldName = new String(ObjectFieldNameCombo.substring(i + 1));

	    Log.customer.debug("IIB: BSOLAUniqueName is: " + BSOLAUniqueName);
	    Log.customer.debug("BOLA Partition: " + BSOLA.getPartition());
	    Log.customer.debug("Mark object name is:" + ObjectName);
	    Log.customer.debug("Field name = " + FieldName);

            if (ObjectName.equals("ariba.common.core.User"))
            {
                /*
                   Processing a User ExpenditureLimitExceededApprover update
                */
                Log.customer.debug("Processing User Expenditure object");

                //get the SharedUser object for the UserID entered
                User BSO = User.getUser((BSOLAUniqueName),PASSWORDADAPTER);

                //get the PartitionedUser object for the UserID entered
                ariba.common.core.User BSOPartitionUser = ariba.common.core.User.getPartitionedUser(BSO,BSOLA.getPartition());

                // Check if the User object to update exists.  If it does, get the Approver object.
                if (bsoExists(BSOPartitionUser))
                {
                    Approver appr = null; 
                    if(BSOLA.getFieldValue("UserRoleFlag")!=null) 
                    {
                        appr = getRoleOrUserApprover((String)BSOLA.getFieldValue("UserRoleFlag"),BSOLA);
                    }
                    BSOPartitionUser.setDottedFieldValue(FieldName, appr);
                }
            }
            else {
                // Processing an object type that has not yet been defined
                Log.customer.debug("Processing an object type that has not been defined");
            }
        }
        else
        {
            /*
               Processing a BuysenseOrgLinkApprovers object 
            */
            Log.customer.debug("Processing BSOrg Approver object");
            ObjectName = new String("ariba.core.BuysenseOrg");
            FieldName = new String(ObjectFieldNameCombo);

	    Log.customer.debug("IIB: BSOLAUniqueName is: " + BSOLAUniqueName);
	    Log.customer.debug("BOLA Partition: " + BSOLA.getPartition());
	    Log.customer.debug("Mark object name is:" + ObjectName);
	    Log.customer.debug("Field name = " + FieldName);

            BaseObject BSO = (BaseObject)Base.getService().objectMatchingUniqueName
                (ObjectName, BSOLA.getPartition(), BSOLAUniqueName);

            // Check if the BSOrg object to update exists.  If it does, get the Approver object.
            if (bsoExists(BSO))
            {
                Approver appr = null; 
                if(BSOLA.getFieldValue("UserRoleFlag")!=null) 
                {
                    appr = getRoleOrUserApprover((String)BSOLA.getFieldValue("UserRoleFlag"),BSOLA);
                }
                BSO.setDottedFieldValue(FieldName, appr);
            }
        }
    }

    /** 
     * Ariba 8.1 Dev SPL #57 - New method to see if Object to update exists
     * @param bsoObject The Object to update
     */  
    public boolean bsoExists (BaseObject bsoObject) 
    {
        boolean bsoFlag = false;
        if (bsoObject != null) 
           bsoFlag = true;
        return bsoFlag;
    }

    /** 
     * Ariba 8.1 Dev SPL #57 - New method to get the Approver object - either a Group or SharedUser
     * @param roleOrUser String to designate if the Approver value is a Role(group) or User
     * @param inputObj The UniqueName of the Approver - either a Group or SharedUser
     */  
    public Approver getRoleOrUserApprover (String roleOrUser, BaseObject inputObj) 
    {
        Approver bsoApprover = null;
        if (roleOrUser.equals("User") || roleOrUser.equals("USER"))
        {
            Log.customer.debug("IIB: User Approver");
            bsoApprover = (Approver)User.getUser((String)inputObj.getFieldValue("Approver"), PASSWORDADAPTER);
        }
        else if (roleOrUser.equals("Role") || roleOrUser.equals("ROLE")) 
        {
            Log.customer.debug("IIB: Role Approver");
                    
            bsoApprover = (Group)Base.getService().objectMatchingUniqueName
                ("ariba.user.core.Group",
                 inputObj.getPartition(),
                 (String)inputObj.getFieldValue("Approver"));
        }
        else 
        {
            Log.customer.debug("IIB: Neither User, nor Role Approver");
        }
        Log.customer.debug("The approver is " + bsoApprover);
        return bsoApprover;
    }

}

