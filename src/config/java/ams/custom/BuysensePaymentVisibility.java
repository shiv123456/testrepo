
// 10/10/2003: Updates for Ariba 8.1 (Richard Lee)

package config.java.ams.custom;

import java.util.List;
import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.core.BaseObject;
import ariba.htmlui.fieldsui.Log;

//81->822 changed Vector to List
//81->822 changed ConditionValueInfo to ValueInfo
public class BuysensePaymentVisibility extends Condition
{
    
    public static final String TargetValue1Param = "TargetValue1";
    public static final String TargetValue2Param = "TargetValue2";
    public static final String TargetValue3Param = "TargetValue3";
    private static final ValueInfo parameterInfo[];
    private static final String requiredParameterNames[] = 
        {
        "TargetValue"
    };
    public boolean evaluate(Object value, PropertyTable params)
    
    {
        return testValue(value, params);
    }
    
    protected boolean testValue(Object value, PropertyTable params)
    {
        String clientUniqueName = (String)params.getPropertyForKey("TargetValue");
        String fieldName        = (String)params.getPropertyForKey("TargetValue1");
        BaseObject bo   = (BaseObject)params.getPropertyForKey("TargetValue2");
        String appType          = (String)params.getPropertyForKey("TargetValue3");
        
        Log.visibility.debug("ClientUN: %s fieldName: %s BaseObject: %s",
                             clientUniqueName,fieldName,bo);
        if (clientUniqueName == null)
            // Ariba 8.1 changed Util.assertNonFatal(false,"Did not receive the Client Name");
            Assert.assertNonFatal(false,"Did not receive the Client Name");
        
        //boolean ret = BuysenseUIConditions.isFieldVisible(clientUniqueName , fieldName,bo,appType);
        //Log.visibility.debug("Returning %s from Visibility",new Boolean(ret));
        
        PaymentDefaulter pd = new PaymentDefaulter();
        pd.fire((ValueSource)bo,params);
        pd.setAccounting((ValueSource)bo,params);
        pd.setRefAccounting((ValueSource)bo,params);
        return true;
    }
    
    public ConditionResult evaluateAndExplain(Object value,
                                              PropertyTable params)
    {
        if(!testValue(value, params))
        {
            String msg = params.stringPropertyForKey("Message");
            String subject = subjectForMessages(params);
            if(msg != null)
                return new ConditionResult(Fmt.Si(msg, subject));
            else
                return new ConditionResult(Fmt.Sil("ariba.common.core.condition",
                                                   "EqualToMsg1", subject));
        }
        else
        {
            return null;
        }
    }
    
    private List getTargetValues(PropertyTable params)
    {
        
        // Ariba 8.1 changed List targetValues = new List();
        List targetValues = ListUtil.list();
        Object targetValue = params.getPropertyForKey("TargetValue");
        if(targetValue != null)
            // Ariba 8.1 changed addElement to add
            targetValues.add(targetValue);
        targetValue = params.getPropertyForKey("TargetValue1");
        if(targetValue != null)
            targetValues.add(targetValue);
        targetValue = params.getPropertyForKey("TargetValue2");
        if(targetValue != null)
            targetValues.add(targetValue);
        targetValue = params.getPropertyForKey("TargetValue3");
        if(targetValue != null)
            targetValues.add(targetValue);
        return targetValues;
    }
    
    
    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }
    
    protected String[] getRequiredParameterNames()
    {
        return requiredParameterNames;
    }
    
    public BuysensePaymentVisibility()
    {
    }
    
    static
        {
        parameterInfo = (new ValueInfo[] 
                         {
                new ValueInfo("TargetValue",
                                       0),
                    new ValueInfo("TargetValue1",
                                           0),
                    new ValueInfo("TargetValue2",
                                           0),
                    new ValueInfo("TargetValue3",
                                           0),
                    new ValueInfo("Message", 0, Behavior.StringClass)
            }
        );
    }
}
