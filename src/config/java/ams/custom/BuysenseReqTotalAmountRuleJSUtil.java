package config.java.ams.custom;

import java.math.BigDecimal;

import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseReqTotalAmountRuleJSUtil
{

    public static boolean isReqTotalAmountMore(String userUniqueName, String userBSO, String sClientName, String reqTotalAmount)
    {
        // EVA001:B222DGS:ReqTotalAmtSignerRule:2000
        // start get BFTD table data
    	// clientname is coming as EVA001:B222DGS
        try
        {
            String sQuery = "SELECT b.UniqueName,b from ariba.core.BuysenseFieldDataTable b where b.UniqueName like \'" + sClientName + ":ReqTotalAmtSignerRule:%\' Order by b.Name";
            AQLQuery aqlQuery = AQLQuery.parseQuery(sQuery);
            Partition partition = Base.getService().getPartition("pcsv");
            AQLOptions aqlOptions = new AQLOptions(partition);
            ariba.util.javascript.Log.javascript.debug("isReqTotalAmountMore aqlQuery: " + aqlQuery);
            AQLResultCollection aqlResults = Base.getService().executeQuery(aqlQuery, aqlOptions);
            boolean bReqAmountGreater = false;
            
            BigDecimal fPrevAmount = new BigDecimal(0); // add it in another function
            
            if (aqlResults.getErrors() == null)
            {
                while (aqlResults.next())
                {
                    String sUN = aqlResults.getString(0);
                    BaseId sBID = aqlResults.getBaseId(1);
                    String[] splitUniqueName = sUN.split(":");
                    // // EVA001:B222DGS:ReqTotalAmtSignerRule:2000

                    if (splitUniqueName.length < 4) // 
                    {
                        Log.customer.warning(8888, "BuysenseReqTotalAmountRuleJSUtil::Uniquename not set correctly");
                        // continue to the next entry
                        continue;
                    }
                    
                    /*
                     * CSPL-7532: Update Dollar Threshold logic in ReqTotalAmtSignerRule logic
                     * DGS identified that the ReqTotalAmtSignerRule wasn't firing for a Req TotalCost of $1 million even though their
                     * dollar threshold was setup for $999,999.99. 
                     * It was found that the logic defines the Amount variables as type Float and uses a '>' to compares the dollar values.
                     * The > sign doesn't work well with float variables with decimal values.
                     * Hence changed the variable to BigDecimal and used compareTo() method insted of '>' 
                     * 
                     */
                    
                    if (!StringUtil.nullOrEmptyOrBlankString(splitUniqueName[3]))
                    {
                        BigDecimal loReqTotalAmount = new BigDecimal(reqTotalAmount);
                        BigDecimal loBFDTsplitUniqueName = new BigDecimal(splitUniqueName[3]);

                        if (loReqTotalAmount.compareTo(loBFDTsplitUniqueName) > 0)
                        {

                            if (loBFDTsplitUniqueName.compareTo(fPrevAmount) > 0)
                            {
                                fPrevAmount = loBFDTsplitUniqueName;
                                bReqAmountGreater = true;
                            }
                        }
                    }
                }
            }
			return bReqAmountGreater;
        }
        catch (Exception e)
        {
            Log.customer.warning(8888, "Exception in BuysenseReqTotalAmountRuleJSUtil.isReqTotalAmountMore() method" + e.getMessage());
            ariba.util.javascript.Log.javascript.debug("Exception in BuysenseReqTotalAmountRuleJSUtil.isReqTotalAmountMore() method " + e.getMessage());
        }
        return false;
    }

    public static String getReqTotalAmountRuleApprover(String userUniqueName, String userBSO, String sClientName, String reqTotalAmount)
    {
        // EVA001:B222DGS:ReqTotalAmtSignerRule:2000
        // start get BFTD table data
    	// clientname is coming as EVA001:B222DGS
    	
    	String sApproverVal = "";
        try
        {
            String sQuery = "SELECT b.UniqueName,b,b.Name from ariba.core.BuysenseFieldDataTable b where b.UniqueName like \'" + sClientName + ":ReqTotalAmtSignerRule:%\' Order by b.Name";
            AQLQuery aqlQuery = AQLQuery.parseQuery(sQuery);
            Partition partition = Base.getService().getPartition("pcsv");
            AQLOptions aqlOptions = new AQLOptions(partition);
            ariba.util.javascript.Log.javascript.debug("LLLL aqlQuery: " + aqlQuery);
            AQLResultCollection aqlResults = Base.getService().executeQuery(aqlQuery, aqlOptions);            
            BigDecimal fPrevAmount = new BigDecimal(0); // add it in another function            
            
            if (aqlResults.getErrors() == null)
            {
                while (aqlResults.next())
                {
                    String sUN = aqlResults.getString(0);
                    BaseId sBID = aqlResults.getBaseId(1);                    
                    String[] splitUniqueName = sUN.split(":");
                    // // EVA001:B222DGS:ReqTotalAmtSignerRule:2000

                    if (splitUniqueName.length < 4) // 
                    {
                        Log.customer.warning(8888, "BuysenseReqTotalAmountRuleJSUtil::Uniquename not set correctly");
                        // continue to the next entry
                        continue;
                    }

                    /*
                     * CSPL-7532: Update Dollar Threshold logic in ReqTotalAmtSignerRule logic
                     * DGS identified that the ReqTotalAmtSignerRule wasn't firing for a Req TotalCost of $1 million even though their
                     * dollar threshold was setup for $999,999.99. 
                     * It was found that the logic defines the Amount variables as type Float and uses a '>' to compares the dollar values.
                     * The > sign doesn't work well with float variables with decimal values.
                     * Hence changed the variable to BigDecimal and used compareTo() method insted of '>' 
                     * 
                     */

                    if (!StringUtil.nullOrEmptyOrBlankString(splitUniqueName[3]))
                    {
                        BigDecimal loReqTotalAmount = new BigDecimal(reqTotalAmount);
                        BigDecimal loBFDTsplitUniqueName = new BigDecimal(splitUniqueName[3]);

                        if (loReqTotalAmount.compareTo(loBFDTsplitUniqueName) > 0)
                        {
                            if (loBFDTsplitUniqueName.compareTo(fPrevAmount) > 0)
                            {
                                fPrevAmount = loBFDTsplitUniqueName;
                                sApproverVal = aqlResults.getString(2);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.customer.warning(8888, "Exception in BuysenseReqTotalAmountRuleJSUtil.isReqTotalAmountMore() method" + e.getMessage());
            ariba.util.javascript.Log.javascript.debug("Exception in BuysenseReqTotalAmountRuleJSUtil.isReqTotalAmountMore() method " + e.getMessage());
        }
        return sApproverVal;
    }

}
