package config.java.ams.custom;

import ariba.base.core.BaseObject;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.util.core.PropertyTable;

import ariba.util.log.Log;

public class BuysenseEmallToQQFieldsVisibility extends Condition{

        private static final ValueInfo parameterInfo[];
        public boolean evaluate(Object value, PropertyTable params) throws ConditionEvaluationException
        {
            BaseObject obj   = (BaseObject)params.getPropertyForKey("SourceObject");
            Log.customer.debug("inside BuysenseEmallToQQFieldsVisibility SourceObject"+obj);
            
            if(obj.instanceOf("ariba.purchasing.core.Requisition"))
            {
                Log.customer.debug("inside BuysenseEmallToQQFieldsVisibility");
                Boolean bIsReqHeadcb3 = (Boolean) obj.getFieldValue("ReqHeadCB3Value");
                Boolean bIsReqHeadcb5 = (Boolean) obj.getFieldValue("ReqHeadCB5Value");
             if ((obj.getFieldValue("TransactionSource")) != null)
                {
                    Log.customer.debug("inside if of TransactionSource :: BuysenseEmallToQQFieldsVisibility");

                       return false;
                }
                if(bIsReqHeadcb3!=null && bIsReqHeadcb3.booleanValue() || bIsReqHeadcb5!=null && bIsReqHeadcb5.booleanValue())
                { 
                    Log.customer.debug("inside bIsReqHeadcb!=null && bIsReqHeadcb.booleanValue()");
                    return true;
                }
                else
                  {    
                    Log.customer.debug("inside else of visibility :bIsReqHeadcb!=null && bIsReqHeadcb.booleanValue() ");
                      return false;
                  }                  
            }
            else
            {
                Log.customer.debug("inside ariba.purchasing.core.Requisition");
                return false;
            } 
        }            
        protected ValueInfo[] getParameterInfo()
        {
           return parameterInfo;
        }   
        static
        {
             parameterInfo = (new ValueInfo[] {new ValueInfo("SourceObject", 0)});
        }
    }
