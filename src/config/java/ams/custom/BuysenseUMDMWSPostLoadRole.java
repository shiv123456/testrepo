package config.java.ams.custom;

import java.util.List;

import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.base.core.LongString;
import ariba.base.core.LongStringElement;
import ariba.base.core.MultiLingualString;
import ariba.base.core.Partition;
import ariba.user.core.Group;
import ariba.user.core.Role;
import ariba.util.core.ListUtil;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;

public class BuysenseUMDMWSPostLoadRole extends BuysenseUMDMWSPostLoad
{
    private final String sClassName = "BuysenseUMDMWSPostLoadRole";
    private Object       clr;

    boolean checkIfUniqueAribaInstanceExists(ClusterRoot oCustomObj)
    {
        //check if Role object exists with the same uniquename as custom object.
        Object cObjUN = oCustomObj.getFieldValue("UniqueName");
        if (cObjUN != null && (cObjUN instanceof String))
        {
            String sUniqueName = (String) cObjUN;
            Object oRole = Base.getService().objectMatchingUniqueName("ariba.user.core.Role", Partition.None,
                    sUniqueName);
            if (oRole == null)
            {
                log(sClassName + " Role do not exist in Ariba");
                return false;
            }
            // need to check Group?? Think, not required.
        }
        return true;
    }

    @SuppressWarnings({ "rawtypes", "unchecked", "unused" })
    protected void create()
    {
        String sReferenceFieldList = ResourceService.getString(sResourceFile, sServiceRefKey + REFERENCEFIELDS_CSV);
        //reference data check is done in super class. Just create objects here.
        // create objects through java

        String sUniqueName = (String) cObj.getFieldValue("UniqueName");
        String sName = (String) cObj.getFieldValue("Name");
        Partition part = Partition.None;
        Object o = Base.getService().objectMatchingUniqueName("ariba.user.core.Role", part, sUniqueName);
        if (o != null)
        {
            // Object already exists, may be inactive. Throw error.

            appendStatus(cObj, iError, "FAILURE",
                    "Object already exists in Ariba and may be Inactive. Try Update operation");
            log(sClassName + " Object already exists in Ariba and may be Inactive. Try Update operation");
            return;
        }
        Object bo = Role.create("ariba.user.core.Role", part);
        if (bo instanceof Role)
        {
            Role r = (Role) bo;
            r.setUniqueName(sUniqueName);
            MultiLingualString mls = new MultiLingualString(part);
            mls.setPrimaryString(sName);
            r.setName(mls);
            r.setAdapterSource("None:Role.csv");
            log(sClassName + " role " + r);
            r.save();
            List lPermissions = r.getPermissions();
            Object oPermissions = cObj.getDottedFieldValue("Permissions");  
            //Role can have zero Permissions. Skip mapping Permissions if custom wrapper has null.
            if (oPermissions != null)
            {
                LongString pLS = (LongString) oPermissions;
                List lStrings = pLS.getStrings();
                for (int p = 0; p < lStrings.size(); p++)
                {
                    LongStringElement sLongElement = (LongStringElement) lStrings.get(p);
                    String sUnseperatedPermission = sLongElement.getString();
                    String sPermissions[] = sUnseperatedPermission.split(",");
                    for (int k = 0; k < sPermissions.length; k++)
                    {
                        String sPermissionUN = sPermissions[k];
                        log("sPermissionUN " + sPermissionUN);
                        ClusterRoot cPermission = Base.getService().objectMatchingUniqueName(
                                "ariba.user.core.Permission", part, sPermissionUN);
                        if (cPermission != null)
                        {
                            lPermissions.add(cPermission);
                        }
                    }

                }
            }
            Group oGroup = Group.createGroup(sUniqueName);
            log(sClassName + " group " + oGroup);
            oGroup.setName(mls);
            oGroup.setAdapterSource("None:Group.csv");
            List lRoles = oGroup.getRoles();
            lRoles.add(r);
            oGroup.save();
        }
        appendStatus(cObj, iSuccess, "SUCESS", "Objects created/mapped succesfully");
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected void update()
    {
        log(sClassName + " In update method");

        String sUniqueName = (String) cObj.getFieldValue("UniqueName");
        String sName = (String) cObj.getFieldValue("Name");
        Partition part = Partition.None;
        Object o = Base.getService().objectMatchingUniqueName("ariba.user.core.Role", part, sUniqueName);
        if (o == null)
        {
            // Object DO NOT exist. Throw error.
            appendStatus(cObj, iError, "FAILURE", "Object does not exit in Ariba.Try Create operation");
            log(sClassName + " Object does not exit in Ariba.Try Create operation");
            return;
        }
        if (o instanceof Role)
        {
            Role r = (Role) o;
            r.setActive(true);
            r.setUniqueName(sUniqueName);
            MultiLingualString mls = new MultiLingualString(part);
            mls.setPrimaryString(sName);
            r.setName(mls);
            r.setAdapterSource("None:Role.csv");
            log(sClassName + " role " + r);
            r.save();
            List lPermissions = r.getPermissions();
            lPermissions.clear(); // Clear all the Permissions, before mapping new.
            Object oPermissions = cObj.getDottedFieldValue("Permissions"); 
            //Role can have zero Permissions. Skip mapping Permissions if custom wrapper has null.
            if (oPermissions != null)
            {

                LongString pLS = (LongString) oPermissions;
                List lStrings = pLS.getStrings();
                for (int p = 0; p < lStrings.size(); p++)
                {
                    LongStringElement sLongElement = (LongStringElement) lStrings.get(p);
                    String sUnseperatedPermission = sLongElement.getString();
                    String sPermissions[] = sUnseperatedPermission.split(",");
                    for (int k = 0; k < sPermissions.length; k++)
                    {
                        String sPermissionUN = sPermissions[k];
                        log(sClassName + " sPermissionUN " + sPermissionUN);
                        ClusterRoot cPermission = Base.getService().objectMatchingUniqueName(
                                "ariba.user.core.Permission", part, sPermissionUN);
                        if (cPermission != null)
                        {
                            lPermissions.add(cPermission);
                        }
                    }

                }
            }
            Group g = Group.getGroup(sUniqueName);
            log(sClassName + " group " + g);
            if (g != null)
            {
                g.setActive(true);
                g.setName(mls);
                g.setAdapterSource("None:Group.csv");
                List lRoles = g.getRoles();
                //Since Group and Role are one to one mapping in Ariba, clearing all existing Roles, if any, and adding fresh.                
                lRoles.clear();
                lRoles.add(r);
                g.save();
            }
            else
            {
                // Group does not exists. Create one?
                log(sClassName + " Group does not exists. creating one");
                Group group = Group.createGroup(sUniqueName);
                log(sClassName + " group " + group);
                group.setName(mls);
                group.setAdapterSource("None:Group.csv");
                List lRoles = group.getRoles();
                lRoles.add(r);
                group.save();
            }

        }
        // set success status
        appendStatus(cObj, iSuccess, "SUCESS", "Objects updated/mapped succesfully");

    }

    @SuppressWarnings({ "rawtypes" })
    protected boolean isRefDataCheckSuccessful(ClusterRoot cObj2, String lRefDataList)
    {
        log(sClassName + " in isRefDataCheckSuccessful");
        // lRefDataFields has reference data details for particular web service
        // There could be multiple values separated by comma that need to be checked. One value is defined by 6 fields
        // Example:"AddressServiceReferenceFields","UniqueName:String:ariba.core.BuysenseClient:UniqueName:pcsv:Single:String","Ref data check fields"
        // s0->UniqueName (field on customObject)
        // s1->ariba.core.BuysenseClient (reference object to be checked)
        // s2->UniqueName (reference object field to be checked)
        // s3->pcsv (partition for reference object)
        // s4->Single (type - single field or a vector??) NOT SURE if we need it
        // s5->String (reference object type)

        // Break the to get one value
        List lRefDataFields = ListUtil.arrayToList(lRefDataList.split(","));
        boolean isRefDataCheckSuccess = false;

        for (int i = 0; i < lRefDataFields.size(); i++)
        {
            // s = UniqueName:String:ariba.core.ariba.user.core.Role:UniqueName:None:Single:String - THIS entry does not work for ADD
            // s = Permissions:LongString:ariba.user.core.Permission:UniqueName:None:Multiple:LongString
            String s = (String) lRefDataFields.get(i);
            String s1[] = s.split(":");
            String sCustomObjFieldName = s1[0];
            String sCustomObjFieldType = s1[1];
            String sRefClassName = s1[2];
            String sRefFieldName = s1[3];
            String sRefPartition = s1[4];
            String sMultiple = s1[5];// valid values are 'Single', 'Multiple'
            String sRefObjFieldType = s1[6];

            Object oCustomFieldValueObj = cObj2.getDottedFieldValue(sCustomObjFieldName);
            Partition partition = Base.getService().getPartition(sRefPartition);

            log("sCustomObjFieldName " + sCustomObjFieldName + ", sCustomObjFieldType " + sCustomObjFieldType
                    + ", sRefClassName " + sRefClassName);
            log("sRefFieldName " + sRefFieldName + ", sRefPartition " + sRefPartition + ",sMultiple " + sMultiple
                    + ", sRefObjFieldType " + sRefObjFieldType);
            log("oCustomFieldValueObj " + oCustomFieldValueObj);
            //Bypassing the null value check for Permissions for any operation, as Role can have no Permissions mapped.
            if (sCustomObjFieldName.equalsIgnoreCase("Permissions") && oCustomFieldValueObj == null)
            {
                return true;
            }
            if (oCustomFieldValueObj == null || StringUtil.nullOrEmptyOrBlankString(sMultiple) || partition == null)
            {
                return false;
            }
            // safe to assume the custom unique lookup would be based on String??. If not, failing the ref check
            /*if (sCustomObjFieldType == null
                    || (sCustomObjFieldType != null && !sCustomObjFieldType.equalsIgnoreCase("String")))
            {
                return false;
            }*/

            if (StringUtil.equalsIgnoreCase(sMultiple, "Single") && (oCustomFieldValueObj instanceof String))
            {
                // got to make sure partition is defined correctly, else value may not be retrieved
                ClusterRoot clr = Base.getService().objectMatchingUniqueName((String) oCustomFieldValueObj, partition,
                        (String) oCustomFieldValueObj);
                if (clr == null) // reference object exists then return true
                {
                    return false;
                }
                isRefDataCheckSuccess = true;
            }
            else if (StringUtil.equalsIgnoreCase(sMultiple, "Multiple"))
            {
                // multiple implies custom object fields had multiple values in there separate by comma
                // Example: BuysenseWSRole.Permissions - 'eVA-QueryAll,CatalogManager,CreateTimeSheet' will have permissions listed in a list
                // oCustomFieldValueObj has comma separated values
                // If 'Multiple', is it safe to assume that custom object field is a longstring?

                // /Do a check if custom object field is long string, If not, log message and return false
                if (!(oCustomFieldValueObj instanceof LongString))
                {
                    // log message
                    return false;
                }

                LongString lsCustomValues = (LongString) oCustomFieldValueObj;
                List lStrings = lsCustomValues.getStrings();
                for (int l = 0; l < lStrings.size(); l++)
                {
                    LongStringElement sLongElement = (LongStringElement) lStrings.get(l);
                    String sUnseperatedPermission = sLongElement.getString();
                    log(sClassName + " sUnseperatedPermission " + sUnseperatedPermission);
                    String sPermissions[] = sUnseperatedPermission.split(",");
                    for (int k = 0; k < sPermissions.length; k++)
                    {
                        String sPermissionUN = sPermissions[k];
                        log(sClassName + " Searching for object with values: " + sCustomObjFieldType + "," + partition
                                + "," + sPermissionUN);
                        clr = Base.getService().objectMatchingUniqueName(sRefClassName, partition, sPermissionUN);
                        // Need to check for inactive?? if so ERROR or WARNING??
                        if (clr == null)
                        {
                            log(sClassName + " Reference data not found for " + sCustomObjFieldName);
                            return false;
                        }
                        isRefDataCheckSuccess = true;
                    }
                }
            } // end of elseif for 'Multiple'
        }// end of for
        log(sClassName + " isRefDataCheckSuccess " + isRefDataCheckSuccess);
        return isRefDataCheckSuccess;
    }

}
