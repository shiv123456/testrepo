package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.common.core.CommonSupplier;
import ariba.common.core.Supplier;
import ariba.purchasing.core.ReqLineItem;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
import ariba.workforce.core.LaborLineItemDetails;

/**
 * Date : 10-Aug-2009 Contracts URL: Description : Trigger to set value for
 * LaborLineitemDetails.Contracts field from parameters.table. By OOTB functionality, while copying
 * a collaboratiive line item with Collaborate set to yes, the supplier for the line is being set to
 * UnSpecified supplier thus unable to submit the reqs. So this functionality is customized in such
 * a way that, it will set supplier as supplier in Category item.
 * 
 * CSPL # 1772-Work Location is not defaulted with Requester ShipTo
 * @author Pavan Aluri @Date 03-Feb-2010 @version 1.1
 * @Explanation Added a method setDefaultWorkLocation(), for setting Work Location field same as Ship To.
 */
public class SetContractsURL extends Action
{
    public void fire(ValueSource object, PropertyTable params) throws ActionExecutionException
    {
        Log.customer.debug("Calling ContractsURL.java" + object);
        if (object != null
                && (object instanceof LaborLineItemDetails || object.toString().indexOf("ConsultingLineItemDetails") >= 0))
        {
            setContractsURL(object);
            setSupplier(object);
            setDefaultWorkLocation(object);
        }
    }

    private void setContractsURL(ValueSource object)
    {
        Partition moPartition = Base.getSession().getPartition();
        String sContractsURL = null;

        if (object instanceof LaborLineItemDetails)
        {
            sContractsURL = Base.getService().getParameter(moPartition, "Application.Base.Data.Contracts");
        }
        else if (object.toString().indexOf("ConsultingLineItemDetails") >= 0)
        {
            sContractsURL = Base.getService().getParameter(moPartition, "Application.Base.Data.ConsultingContracts");
        }
        object.setFieldValue("Contracts", sContractsURL);
        Log.customer.debug("ContractsURL Value is set");
    }

    private void setSupplier(ValueSource object)
    {
        Supplier partSupplier = null;

        Boolean bCollaborate = (Boolean) object.getFieldValue("Collaborate");

        Log.customer.debug("ContractsURL::Collaborate " + bCollaborate);

        CommonSupplier commonSupplier = (CommonSupplier) object
                .getDottedFieldValue("CategoryItem.CategoryCatalogItem.CommonSupplier");
        Supplier lineSupplier = (Supplier) object.getDottedFieldValue("LineItem.Supplier");

        if (bCollaborate.booleanValue() && commonSupplier != null && lineSupplier != null)
        {
            partSupplier = (Supplier) commonSupplier.getPartitionSupplier();

            String lineSupplierName = (String) lineSupplier.getFieldValue("Name");
            Log.customer.debug("inside ContractsURL.java lineSupplier" + lineSupplier);
            if (lineSupplierName.equals("[Unspecified]"))
            {
                Log.customer.debug("inside seconf if ContractsURL.java lineSupplier" + lineSupplier);
                object.setDottedFieldValue("LineItem.Supplier", partSupplier);
            }

        }
        else
        {
            Log.customer.debug("ContractsURL- Line Supplier is not set");
            return;
        }
    }

    private void setDefaultWorkLocation(ValueSource object)
    {
        if ((object.getDottedFieldValue("LineItem") instanceof ReqLineItem))
        {
            ReqLineItem reqLI = (ReqLineItem) object.getDottedFieldValue("LineItem");
            if (reqLI != null)
            {
                object.setFieldValue("WorkLocation", reqLI.getShipTo());
            }
        }
    }

}
