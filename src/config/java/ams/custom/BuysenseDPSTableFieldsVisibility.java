package config.java.ams.custom;

import java.util.List;

import ariba.base.core.BaseObject;
import ariba.base.core.ClusterRoot;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.util.core.ListUtil;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.base.fields.ValidChoices;

import ariba.util.log.Log;

public class BuysenseDPSTableFieldsVisibility extends ValidChoices{

    public boolean useOrderedChoices()
    {
        return true;
    }

    @Override
    protected Object choices()
    {
        List<String> choices = ListUtil.list();

        Object cobj = getValueSourceContext();
        Log.customer.debug(ClassName + " cobj " + cobj);
        if (cobj instanceof ClusterRoot)
        {
            ClusterRoot loEfromCR = (ClusterRoot) cobj;
            List<String> loRadioValues = (List<String>) loEfromCR.getDottedFieldValue("RadioButtonsforDPS");
            if (loRadioValues != null && !loRadioValues.isEmpty())
            {
                for (int i = 0; i < loRadioValues.size(); i++)
                {
                    String sRadioFieldVal = loRadioValues.get(i);
                    Log.customer.debug(ClassName + " sRadioFieldVal " + sRadioFieldVal);
                    if(sRadioFieldVal.equals("DPS State Contract"))
                    {
                    	return true;
                    }
                }
            }
        }

        Log.customer.debug(ClassName + "choices() " + choices);
        return true;
    }
}