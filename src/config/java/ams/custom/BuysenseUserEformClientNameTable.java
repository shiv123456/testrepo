package config.java.ams.custom;

import java.util.ArrayList;
import java.util.List;

import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLNameTable;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.core.aql.SearchTermQuery;
import ariba.base.fields.ValueSource;
import ariba.util.log.Log;

public class BuysenseUserEformClientNameTable extends AQLNameTable
{
    public static final String sClassName      = "config.java.ams.custom.BuysenseUserEformClientNameTable ";
    public static final String sFilterRoleName = "eVAUserProfileRequestor";
    Partition                  partition       = Base.getSession().getPartition();

    public void addQueryConstraints(AQLQuery query, String field, String pattern, SearchTermQuery searchTermQuery)
    {
        ValueSource valueSourceCurrent = getValueSourceContext();
        super.addQueryConstraints(query, field, pattern, searchTermQuery);
        if (valueSourceCurrent instanceof ariba.approvable.core.Approvable
                && valueSourceCurrent.getTypeName().equals("ariba.core.BuysenseUserProfileRequest"))
        {

            ariba.user.core.User oPreparer = (ariba.user.core.User) valueSourceCurrent.getFieldValue("Preparer");
            if (oPreparer == null)
            {
                query.andFalse();
                return;
            }
            else
            {

                ariba.common.core.User cUser = ariba.common.core.User.getPartitionedUser(oPreparer, partition);
                String lsClientName = (String) cUser.getDottedFieldValue("ClientName.ClientName");
                AQLCondition clientORCondition = AQLCondition.buildLike(query.buildField("ClientName"), lsClientName.substring(0, 4) + "%");

                List<String> lvAccessAgencies = getAgencies(oPreparer, sFilterRoleName);
                if (lvAccessAgencies != null)
                {
                    for (int i = 0; i < lvAccessAgencies.size(); i++)
                    {
                        clientORCondition = clientORCondition.or(AQLCondition
                                .buildLike(query.buildField("ClientName"), lvAccessAgencies.get(i) + "%"));
                    }
                }
                query.and(clientORCondition);
                Log.customer.debug(sClassName + "The final query is: " + query);
            }
        }
        else
        {
            query.andFalse();
            Log.customer.debug(sClassName + " No constraints are added to the Query : ");
        }
    }

    private List<String> getAgencies(ariba.user.core.User oPreparer, String sFilterRoleName)
    {
        List<String> lsAgencies = new ArrayList<String>();
        AQLQuery aqlQuery = ariba.user.core.User.getAllParentGroupsQuery(oPreparer);
        AQLOptions aqlOptions = new AQLOptions(partition);
        aqlQuery.andLike("UniqueName", "%" + sFilterRoleName);
        aqlQuery.addSelectElement("UniqueName");
        Log.customer.debug(sClassName + "::getAgencies() aqlQuery:" + aqlQuery);
        AQLResultCollection aqlResults = Base.getService().executeQuery(aqlQuery, aqlOptions);

        if (aqlResults != null && aqlResults.getFirstError() == null)
        {
            while (aqlResults.next())
            {
                String sGroupUN = aqlResults.getString(1);
                int iClientIndex = sGroupUN.indexOf("-");
                if (iClientIndex > 0)
                {
                    sGroupUN = sGroupUN.substring(0, iClientIndex);
                    lsAgencies.add(sGroupUN);
                    Log.customer.debug(sClassName + "::getAgencies() added " + sGroupUN + " to Agencies list");
                }
            }
        }

        return lsAgencies;
    }
}
