package config.java.ams.custom;
/**
 * This class is used for all exception handling in PSG package classes.
 *
 *
 * @version 1.0
 */
public class PSGException extends java.lang.Exception
    {
    /**
     * Simple Constructor for PSGException
     *
     */
    public PSGException()
    {
        super();
    }
    /**
     * Constructor for PSGException which accepts a string message
     *
     * @param   String Exception message
     */
    public PSGException(String sMessage)
    {
        super(sMessage);
    }
    /**
     * Constructor for which accepts an Exception
     *
     * @param   Exception Exception object
     */
    public PSGException(Throwable t)
    {
        super(t.getMessage());
    }
}
