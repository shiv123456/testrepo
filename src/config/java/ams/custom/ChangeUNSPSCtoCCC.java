package config.java.ams.custom;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Map;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.core.BaseId;
import ariba.base.core.BaseVector;
import ariba.base.core.Partition;
import ariba.base.core.Base;
import ariba.basic.core.CommodityCode;
import ariba.purchasing.core.POLineItem;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.util.core.MapUtil;
import ariba.util.scheduler.ScheduledTask;
import ariba.util.scheduler.Scheduler;
import ariba.util.log.Log;

public class ChangeUNSPSCtoCCC extends ScheduledTask {

	public static final String ClassName = "config.java.ams.custom.ChangeUNSPSCtoCCC";
	
	public Connection moConnection;

	private String msReqLineQuery = null ;
	private String msCommodityQuery = null ;
	private String msDummyCommodityQuery = null ;
	private String msDBPassword = null ;
	private String msDBURL = null ;
	private String msDBUser = null ;
	
	private String msDomain = null ;
	
	private int miTransactionIndicator = 0;
	private int miUnNecessaryExec = 0;

	AQLOptions moOptions = null;
	AQLQuery moQuery = null;
	AQLResultCollection moAQLResults = null;
	Partition moNonePartition = null;
	CommodityCode mo822DummyCommodity = null;
	Map mCommodityMapTable = MapUtil.map();

	public void init(Scheduler scheduler, String scheduledTaskName, Map arguments)
	{
        super.init(scheduler, scheduledTaskName, arguments);
        String lsKey = null;
        for(java.util.Iterator e = arguments.keySet().iterator(); e.hasNext();)
        {
           lsKey = (String)e.next();
           if (lsKey.equals("ProcessQuery"))
           {
        	   msReqLineQuery = (String)arguments.get(lsKey);
           }else if(lsKey.equals("CommodityQuery"))
           {
        	   msCommodityQuery = (String) arguments.get(lsKey);
           }else if(lsKey.equals("DummyCommodityQuery"))
           {
        	   msDummyCommodityQuery = (String) arguments.get(lsKey);
           }else if(lsKey.equals("DBPassword"))
           {
        	   msDBPassword = (String) arguments.get(lsKey);
           }else if(lsKey.equals("DBURL"))
           {
        	   msDBURL = (String) arguments.get(lsKey);
           }else if(lsKey.equals("DBUser"))
           {
        	   msDBUser = (String) arguments.get(lsKey);
           }
        }
	}

	public void run()
	{
		try
		{
		   Log.customer.debug("Class %s on method run :: variable msReqLineQuery is %s", ClassName, msReqLineQuery);
		   moNonePartition = Base.getService().getPartition("None");
		   mCommodityMapTable = mPutCommodityCodesInMapTable();
		   mo822DummyCommodity = getDummyCommodityCode();
		   moConnection = (Connection) DriverManager.getConnection(msDBURL,msDBUser,msDBPassword);
		   moConnection.setAutoCommit (false);
		   java.sql.Statement loReqStatement = moConnection.createStatement();
		   Log.customer.debug("Class %s on method run :: variable loReqStatement is %s", ClassName, loReqStatement);
		   ResultSet loReqResultSet = loReqStatement.executeQuery(msReqLineQuery);
		   Log.customer.debug("Class %s on method run :: variable loReqResultSet is %s", ClassName, loReqResultSet);
		   while (loReqResultSet != null && loReqResultSet.next())
		   {
			 String lsReqLine = loReqResultSet.getString("ROOTID");
			 //Log.customer.debug("Class %s on method run :: variable lsReqLine is %s", ClassName, lsReqLine);
			 BaseId loReqBaseID = BaseId.parse(lsReqLine);
			 //Log.customer.debug("Class %s on method run :: variable loReqLineBaseID is %s", ClassName, loReqBaseID);
			 Requisition loRequisition = (Requisition) Base.getSession().objectFromId(loReqBaseID);
			 //Log.customer.debug("Class %s on method run :: variable loReq is %s", ClassName, loRequisition);
			 BaseVector vReqLineItems = (BaseVector)loRequisition.getLineItems();
 	         for (int i=0; i < vReqLineItems.size(); i++)
		       {
		          ReqLineItem oReqLineItem =(ReqLineItem) vReqLineItems.get(i);
		          msDomain = (String) oReqLineItem.getDottedFieldValue("Description.CommonCommodityCode.Domain");
		          POLineItem oPOLineItem = (POLineItem) oReqLineItem.getPOLineItem();
		          if(oReqLineItem.getCommodityCode() != null && msDomain != null && msDomain.equalsIgnoreCase("unspsc"))
		          {
		            String lsPCCUniqueName = (String)oReqLineItem.getDottedFieldValue("CommodityCode.UniqueName");
		            if(lsPCCUniqueName != null && lsPCCUniqueName.endsWith("0000"))
		            {
		            	oReqLineItem.setDottedFieldValue("Description.CommonCommodityCode", mo822DummyCommodity);
		            	if(oPOLineItem != null)
		            		oPOLineItem.setDottedFieldValue("Description.CommonCommodityCode", mo822DummyCommodity);
		            }
		            else if(lsPCCUniqueName != null)
		            {
		            	CommodityCode loCommodityCode = (CommodityCode) mCommodityMapTable.get(lsPCCUniqueName);
		            	if(loCommodityCode != null)
		            	{
		            		oReqLineItem.setDottedFieldValue("Description.CommonCommodityCode", loCommodityCode);
		            		miTransactionIndicator = miTransactionIndicator + 1;
			            	if(oPOLineItem != null)
			            		oPOLineItem.setDottedFieldValue("Description.CommonCommodityCode", loCommodityCode);
			            		miTransactionIndicator = miTransactionIndicator + 1;
		            	}
		            }
		            else
		            {
		            	Log.customer.debug("Not able to set CommodityCode for Order %s and for line # %s for PCC %s", oPOLineItem.getOrderID(), oPOLineItem.getNumberInCollection(), lsPCCUniqueName );
		            }
	            }
	            else
	            {
	            	miUnNecessaryExec = miUnNecessaryExec + 1;
	            }
	         }
		   }
	        if(miTransactionIndicator > 500)
	        {
	        	Base.getSession().transactionCommit();
	        	Log.customer.debug("Class %s on method run :: Unnecessary executions for this commit %s ", ClassName, miUnNecessaryExec);
            	Log.customer.debug("Class %s on method run :: Transaction commit done successfully::# of lines executed %s ", ClassName, miTransactionIndicator);
            	miTransactionIndicator = 0;
            	miUnNecessaryExec = 0;
	        }
		}
		catch (Exception loEx)
        {
            Log.customer.debug("Class %s on method run :: Exception %s ", ClassName, loEx.toString() );
        }
        finally
        {
          Log.customer.debug("Class %s on method run :: attempting to close moConnection in finally block ", ClassName);
          try
          {
           if (! moConnection.isClosed())
           moConnection.close();
           Log.customer.debug("Class %s on method run :: closed moConnection in finally block of run method ", ClassName);
          }
          catch (Exception loEx)
          {
           Log.customer.debug("Class %s on method run :: failed to close moConnection in finally block of run method ", ClassName);           
          }
        }
	}

	public Map mPutCommodityCodesInMapTable()
	{
		AQLResultCollection loCommodities = mAQLCommodityResults();
		while (loCommodities.next())
		{
			BaseId loCommodityBaseID = (BaseId) loCommodities.getBaseId(0);
			CommodityCode loCommodityCode = (CommodityCode)Base.getSession().objectFromId(loCommodityBaseID);
			String lsCommodityUniqueName = (String)loCommodityCode.getUniqueName();
			mCommodityMapTable.put(lsCommodityUniqueName, loCommodityCode);
		}
		return mCommodityMapTable;
	}
	
	public CommodityCode getDummyCommodityCode()
	{
	    moOptions = new AQLOptions(moNonePartition);
        moQuery = AQLQuery.parseQuery(msDummyCommodityQuery);
        moAQLResults = Base.getService().executeQuery(moQuery, moOptions);
        while (moAQLResults.next())
        {
			BaseId loDummyCommodityBaseID = (BaseId) moAQLResults.getBaseId(0);
			mo822DummyCommodity = (CommodityCode)Base.getSession().objectFromId(loDummyCommodityBaseID);
        }
        return mo822DummyCommodity;
	}
	
	public AQLResultCollection mAQLCommodityResults()
	{
	    moOptions = new AQLOptions(moNonePartition);
        moQuery = AQLQuery.parseQuery(msCommodityQuery);
        moAQLResults = Base.getService().executeQuery(moQuery, moOptions);
        return moAQLResults;
	}
}