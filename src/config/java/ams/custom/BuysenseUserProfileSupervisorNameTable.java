
/* rlee, 12/23/2004, Prod SPL 542.
 */
/* 08/11/2003: Updates for Ariba 8.0 (Jeff Namadan) */
/* 09/05/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import java.util.List;
import ariba.base.core.*;
import ariba.base.core.aql.*;
import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.base.fields.*;
import ariba.common.core.*;
import java.lang.String;

/*
rgiesen Jan 23 2004 - Ariba 8.x Dev SPL #12
Major re-write and cleanup of this program for Ariba 8.1
Functionality - On the requisition, the On Behalf Of field chooser needs to be limited
	to only those users that have the same ClientName as the current user (the preparer).
So, the program is a little mis-named, since it is NOT for the UserProfile.

It is implemented in ReqExtrinsicsFields.aml

In Ariba 8.1, with the modifications to the user object, the query had to be modified because
the ClientName field is on the Partitioned User Object, but the NameTable is on the Requester
which is the Shared User Object.  So, the old code of just adding an query as shown below no
longer works:
	AQLCondition supervisorCond = AQLCondition.buildEqual(query.buildField("ClientName"),id);
    query.and(supervisorCond);

This does not work because the default case to query on is the Shared user, and the ClientName
is on the Partitioned user.

So, in it's place, the query is "hardcoded" and the link between the Shared user object
and the partitioned user object is made.

The hardcoded query is the String UserQueryString, and it was important in this query
to list the Partitioned user first in the FROM statement, so that when later, when the
ClientName condition is added, it is added to that user object, and not the Shared user.

When the final query is compiled, it will look like this:
	SELECT su, su.Name, su.PasswordAdapter
		FROM ariba.common.core."User" AS pu, ariba."user".core."User" AS su
			WHERE pu."User" = su
				AND pu.ClientName = baseid('27h44.2n')
*/

/* Ariba 8.0: Replaced NamedObjectNameTable */
//81->822 changed Vector to List
/**
 * Date : 13-Aug-2009
 * ACP Requirement: TimeSheet On Behalf Of should be filtered by contractors in the hiring manager's agency.
 */
public class BuysenseUserProfileSupervisorNameTable extends AQLNameTable
{
    private static final String UserQueryString =
        "SELECT su, %s FROM " + ariba.common.core.User.class.getName() + " pu Partition pcsv, " +
        	ariba.user.core.User.class.getName() + " su where pu.User = su";
    private static final String TimeSheetUserQueryString = "SELECT su, %s FROM " + ariba.common.core.User.class.getName() + " pu Partition pcsv, " +
	                     ariba.user.core.User.class.getName() + " su where pu.User = su and su.IsContractor = true";

    private List selectFields = ListUtil.list();
    private static String PartitionedUserSuffix = "pu";
    private static String SharedUserSuffix = "su";

    public List getSelectFields ()
    {
        if (ListUtil.nullOrEmptyList(selectFields)) {
            List fields = getFetchFields();
            if (fields != null) {
                for (int i = 0; i < fields.size(); i++) {
                    String field = (String)fields.get(i);
                    addField(field);
                }
            }
        }
        return selectFields;
    }

    private void addField (String field)
    {
        if (field.startsWith(
            "PartitionedUser")) {
            int index = field.indexOf(".");
            if (index == -1) {
                selectFields.add(PartitionedUserSuffix);
            }
            String massagedField = field.substring(index);
            massagedField = StringUtil.strcat(PartitionedUserSuffix,
                massagedField);
            selectFields.add(massagedField);
        }
        else {
            String massagedField = StringUtil.strcat(SharedUserSuffix, ".", field);
            selectFields.add(massagedField);
        }
    }

    private String getSelectString ()
    {
        List selectFields = getSelectFields();

        for (int j = 0; j < selectFields.size(); j++) {
		    Log.customer.debug("*** selectFields[" + j + "]: " + (String)selectFields.get(j));
        }

        int sz = selectFields.size();
        FastStringBuffer fsb = new FastStringBuffer();
        for (int i = 0; i < sz; i++) {
            String field = (String)selectFields.get(i);
            fsb.append(field);
            if (i != sz -1) {
                fsb.append(",");
            }
        }
        String retVal = fsb.toString();
        return retVal;
    }


    public AQLQuery buildQuery (String field, String pattern)
    {
	/*
	 * Prod SPL 542
	 * replace partitionUser with pu
	 * replace * with % as wildcard for pattern
	 * Add field like pattern to Select statement
	 */
    String lsDeactivated = "(Deactivated)%";	
	String retVal = Fmt.S(UserQueryString,getSelectString());
	ValueSource TimeSheetContext = getValueSourceContext();
	if(TimeSheetContext instanceof ariba.workforce.core.TimeSheet)
	{
		retVal = Fmt.S(TimeSheetUserQueryString,getSelectString());
	}
	String lsPattern = pattern;
	int liFieldPre = field.indexOf(".");
	if(liFieldPre != -1)
	{
	    String lsField = "pu" + field.substring(liFieldPre);
	    if(lsPattern != null)
	    {
			lsPattern = lsPattern.replace('*','%');
			lsPattern = " AND " + lsField + " like '" + lsPattern + "'";
			retVal = retVal + lsPattern;
  	    }
	}
        AQLQuery query = AQLQuery.parseQuery(retVal);
	BaseObject BOClientName = (BaseObject)getClientName();
	Log.customer.debug("BuysenseUserProfileSupervisorNameTable: clientName: %s",BOClientName);

	//If clientName is null, all available users are displayed
	if (BOClientName == null)
	{
	Log.customer.debug("BuysenseUserProfileSupervisorNameTable: The clientName is null");
	    Log.customer.debug("BuysenseUserProfileSupervisorNameTable: The context is " +TimeSheetContext.getTypeName());
	    
	    if(TimeSheetContext instanceof ariba.search.core.SearchTerm)
	     {
	    	Log.customer.debug("BuysenseUserProfileSupervisorNameTable: The query when client is null is:" +query.toString()+ "and context is " +TimeSheetContext.getTypeName());
	return query;
	}
	    else
	     {
    	    query.andNotLike("Name.PrimaryString", lsDeactivated);
    	    Log.customer.debug("BuysenseUserProfileSupervisorNameTable: The query when client is null is:" +query.toString()+ " and context is " +TimeSheetContext.getTypeName());
		    return query;
	     }
	}
	//Get the Client Name
	    BaseId BIClientID = BOClientName.getBaseId();
        AQLCondition clientCond = AQLCondition.buildEqual(query.buildField("ClientName"),BIClientID);
        query.and(clientCond);
        query.andNotLike("Name.PrimaryString", lsDeactivated);
        Log.customer.debug("BuysenseUserProfileSupervisorNameTable query: " + query);
        return query;
    }

    private ValueSource getClientName()
    {
        ValueSource context = getValueSourceContext();
        if (context instanceof UserProfile)
        {
            Log.customer.debug("In first if getClientName");

            //return (ValueSource)context;

            //David test
            ariba.user.core.User RequesterUser = (ariba.user.core.User)context.getDottedFieldValue("Requester");

            Log.customer.debug("Got Requester: " + RequesterUser.toString());
            ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());

            Log.customer.debug("Got Requester: " + RequesterPartitionUser.toString());

            return (ValueSource)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName");
        }
        if (context instanceof ariba.user.core.UserProfileDetails)
        {
            Log.customer.debug("In second if getClientName");

            return (ValueSource)context.getFieldValue("ClientName");
        }
        if (context instanceof ariba.common.core.UserProfileDetails)
        {
            Log.customer.debug("In second if getClientName");

            return (ValueSource)context.getFieldValue("ClientName");
        }
        if (context instanceof ariba.purchasing.core.Requisition || 
        		(context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.BuysenseExemptionRequest")))
        {

            Log.customer.debug("In third if getClientName");

            //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User

            //Partitioned User to obtain the extrinsic BuysenseOrg field value.

            //return (ValueSource)context.getDottedFieldValue("Requester.BuysenseOrg.ClientName");

            ariba.user.core.User RequesterUser = (ariba.user.core.User)context.getDottedFieldValue("Requester");

            ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());

            return (ValueSource)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName");

        }
        if (context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.BuysenseUserProfileRequest"))
        {
            Log.customer.debug("In BuysenseUserProfileRequest if getClientName");
            return (ValueSource) context.getDottedFieldValue("Client");
        }
        if (context instanceof ariba.workforce.core.TimeSheet)
        {
        	ariba.user.core.User PreparerUser = (ariba.user.core.User)context.getDottedFieldValue("Preparer");
        	ariba.common.core.User PreparerPartitionUser = ariba.common.core.User.getPartitionedUser(PreparerUser,Base.getSession().getPartition());
        	return (ValueSource)PreparerPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName");        	
        }

        if (context instanceof ariba.approvable.core.ApprovalRequest)

        {
            Log.customer.debug("In fourth if getClientName");
            BaseObject bo = (BaseObject)((BaseObject)context).getClusterRoot();

            //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User

            //Partitioned User to obtain the extrinsic BuysenseOrg field value.

            //return (ValueSource)bo.getDottedFieldValue("Requester.BuysenseOrg.ClientName");

            ariba.user.core.User RequesterUser = (ariba.user.core.User)bo.getDottedFieldValue("Requester");

            ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());

            return (ValueSource)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName");

        }

        else
            return null;
    }
}
