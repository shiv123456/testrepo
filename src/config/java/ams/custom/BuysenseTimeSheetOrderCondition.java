package config.java.ams.custom;


import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionResult;
import ariba.base.fields.ValueInfo;
import ariba.purchasing.core.PurchaseOrder;
import ariba.util.core.Fmt;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.workforce.core.Log;
import ariba.workforce.core.TimeSheet;
/**
 * CSPL #: 1321
 * Author: Sarath Babu Garre
 * Date:   24/11/2009
 * Explanation: By OOTB functionality, whenever a user tries to create a Time Sheet(TS) the order field
 *              in the time sheet will be auto populated with the first Order for this contractor(The Order 
 *              for which contractor is actually created).As our requirement is not to allow the user to create
 *              a TS with order already received, we have created this custom condition so that it will retun an
 *              error message when a Received order is being selected for a TS.
 *                           
 */
public class BuysenseTimeSheetOrderCondition extends Condition
{
	private static final ValueInfo parameterInfo[];
	String message = null;
	public boolean evaluate(Object value, PropertyTable params)  
	{
		BaseObject obj   = (BaseObject)params.getPropertyForKey("SourceObject");
		Log.customer.debug("inside BusenseTimeSheetOrderCondition SourceObject"+obj);
		if(obj.instanceOf("ariba.workforce.core.TimeSheet"))
		{		
			TimeSheet ts =(TimeSheet)obj;
			Log.customer.debug("Object received is a TimeSheet");			
			PurchaseOrder timesheetOrder =(PurchaseOrder)ts.getOrder();
			Log.customer.debug("timesheetOrder received is "+timesheetOrder);
			if(timesheetOrder!=null)
			{
				String OrderStatus =(String)timesheetOrder.getFieldValue("StatusString");
				Log.customer.debug("Order Status is "+OrderStatus);
				if(OrderStatus.equalsIgnoreCase("Received")&& ts.getStatusString().equalsIgnoreCase("Composing"))
				{
					message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","OrderAlreadyReceived");
					return false;
				}
			}				
		}
		return true;
	}
	 public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
	   {		 
			if (!evaluate(value,params))
			  {
				 if(!StringUtil.nullOrEmptyOrBlankString(message));
				 {
					 return new ConditionResult(message);
				 }		     
			  }
			  else
			  {
			     return null;
			  }		
	   }
	 protected ValueInfo[] getParameterInfo()
	   {
	      return parameterInfo;
	   }

	   static
	   {
	      parameterInfo = (new ValueInfo[] {
	         new ValueInfo("SourceObject", 0)
	        });
	   }

}
