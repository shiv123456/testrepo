/*
 * @(#)BuysenseXMLImport.java     1.0 05/14/2001
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 * PVCS Modification :
 *
 * $Log:   R:\Software\development\archives\Ariba 8.x Customizations\app\buyer\Server\classes\extensions\config\java\ams\custom\BuysenseXMLImport.java-arc  $
 *
 *    Rev 1.12   06 Mar 2007 10:03:54   rlee
 * VePI Dev SPL 945 - External Import OutOfDateException.
 *
 *    Rev 1.11   18 Aug 2005 14:40:40   rlee
 * ER#119 - P&C - Ariba changes.
 *
 *    Rev 1.10   16 Jun 2005 15:43:26   nrao
 * VEPI PROD SPL #729: Incorrect messages on change order when Req in Received/Receiving/Composing. Also fixed logic for multiple change order requests in same session.
 *
 *    Rev 1.9   10 Jun 2005 15:49:46   ahiranan
 * VEPI PROD 752 - Added logic to error out when an external Req in Ordering state is cancelled.
 *
 *    Rev 1.8   09 Jun 2005 14:38:28   rlee
 * CR 22 - change to punchout XML for contract no.
 *
 *    Rev 1.7   12 Apr 2005 14:14:26   ahiranan
 * PROD # 431 - Added logic to correctly set the position of the Split Accounting lines vector.
 *
 *    Rev 1.6   16 Mar 2005 13:39:44   rlee
 * ePro Dev SPL 296 - Multiple Change Orders not processed correctly.
 *
 *    Rev 1.5   07 Jan 2005 16:03:42   rlee
 * ST SPL 235 - Req in Composing status but should be in Submitted status.
 *
 *    Rev 1.4   13 Dec 2004 16:53:44   rlee
 * ST SPL 235 - Req in Composing status but should be in Submitted status.
 *
 *    Rev 1.3   18 Oct 2004 14:09:32   ahiranan
 * Dev SPL 59, 97; ST SPL 173,175 - Merged versions 1.39 and 1.40 from e2e archives. Modified the autoSubmit() logic to be in synch with Ariba 7.1 code.
 *
 * Anup - Merged versions 1.39 and 1.40 from e2e archives.
 *        Versions 1.35, 1.36 and 1.38 were marked DO NOT USE - confirmed with rlee.
 *        Version 1.41 changes were under a failed SPL - Confirmed with rlee.
 *        Corrected the autoSubmit() logic to be in synch with 7.1 functionality.
 *
 *    Rev 1.2   07 Sep 2004 13:07:32   rlee
 * Dev SPL 90 - Implement Pull Task for ERP messages for Integration.
 *
 *    Rev 1.1   13 Apr 2004 15:12:06   rgiesen
 * Ariba 8.1 Dev SPL # 28 - Order Import, VCEUtil
 * Significant changes in this file for Order Import to work in 8.1, including replacing VCEUtil with Field Release.
 *
 //RGIESEN - MERGED WITH PROD - APRIL 2, 2004
 *    Rev 1.35   22 Dec 2003 15:47:46   ahiranan
 * Production # 246 - Set Session partition when null in autoSubmit().
 *
 *    Rev 1.34   11 Aug 2003 12:21:42   ahiranan
 * Production # 58 - Added commit after each approvable is processed in a batch.
 *
 *    Rev 1.33   11 Jul 2003 15:20:10   ahiranan
 * UAT SPL # 254 - Modified logic to load comments based on OriginatingSystemLineNumber matching the comment_level.
 *
 *    Rev 1.32   30 Apr 2003 11:53:48   ahiranan
 * Production # 58 - Added logic to prevent import of req with a duplicate Unique Name.
 //RGIESEN - MERGED WITH PROD - APRIL 2, 2004
 *
 *    Rev 1.31   27 Mar 2003 18:28:52   ahiranan
 * Production # 83 - Default Acctg values while defaulting line because imported Req may not have subdetails - where the acctg defaults now occur.
 *
 *    Rev 1.30   05 Mar 2003 11:39:56   ahiranan
 * ST SPL# 457 - Added a new method defaultActgLine() and inserted call to it in startSubDetail(). Commented out Acctg Line defaulting in defaultProcureLineItems().
 *
 *    Rev 1.29   28 Feb 2003 16:14:10   rlee
 * eProc Shakedown 96 - POB fails on import to Ariba.
 * v.1.29 added Null check for mvErrorList.
 *
 *    Rev 1.27   19 Feb 2003 10:54:46   ahiranan
 * UAT SPL # 176 - Overrides changes in version 1.25(errored during check in) and 1.26(wrong version updated). Captures VCEUtil.submitChangeApprovable() and logs the error.
 *
 *    Rev 1.24   30 Jan 2003 17:05:26   rlee
 * eProc Shakedown #96 - POB fails on import to Ariba due to invalid supplier.  If severity = 3, do not update history of original Requisition to indicate Import Successful.
 *
 *    Rev 1.24  30 Jan 2003, rlee, eProc Shakedown # 96 - POB fails on import to Ariba due to invalid supplier,
 *        but the history of Original Requisition is updated with Import was Successful.
 *
 *    Rev 1.23   17 Jan 2003 12:25:46   ahiranan
 * UAT APL # 159 - Set active flag when req is changed in startHeader().
 *
 *    Rev 1.22   16 Dec 2002 16:58:42   ahiranan
 * ST SPL # 666 - Removed return carriages in Ariba error messages for correct display by Notepad in TOAD.
 *
 *    Rev 1.21   14 Dec 2002 15:20:34   rlee
 * eProcurement ST SPL #663 - Duplicate attachments in change order.
 *
 *    Rev 1.21  (rlee 12/14/2002) eProc ST SPL #663, Duplicate attachements from Change order
 *              Around line 1040, remove comments from previous version.
 *
 *    Rev 1.20   11 Dec 2002 16:50:44   nrao
 * (VA eProc Impl ST SPL #608) Set header object handle to null so wrong object is not deactivated upon exception.
 *
 *    Rev 1.19   10 Dec 2002 13:05:10   nrao
 * (VA eProc ST SPL #608, #619) Handles issue of earlier transaction being deactivated when later transaction (related or unrelated) fails within the same BuysenseCreateApprovable run.
 *
 *    Rev 1.18   09 Dec 2002 11:03:56   iberaha
 * (eProcurementImpl, ST SPL # 554) - added a switch statement immediately after the call to VCEUtil.validateApprovable() is made to change the 2,3,5 return code to -2, -3, -5 to correspond to error not warning return codes.
 *
 * Also, took out the try-catch surrounding the call to autoSubmit in finishHeader() method that resolves the issue of Submit Hook failing, force_import = false and Req not being deleted as it should be.
 *
 *    Rev 1.17   05 Dec 2002 18:02:52   cmeva
 * (VA eProc ST SPL #555)
 *
 *    Rev 1.16   04 Dec 2002 13:50:04   nrao
 * (VA eProc ST SPL #555) Existing code was not handling null value in OriginatingSystemLineNumber on Req line. Fixed.
 *
 *    Rev 1.14   18 Nov 2002 10:25:52   smokkapa
 * (eVA, Dev SPL #100) - Order transactions by version number
 *
 *    Rev 1.12   24 Oct 2002 14:50:02   ahiranan
 * ST SPL # 58 - Prevent submit() call in case severity of existing errors is 3.
 *
 *    Rev 1.11   14 Oct 2002 17:08:46   nrao
 * (e2e ST SPL #3) - Corrected error in history message setting for Solicitation statuses. Also changed for differentiating between old and new req messages.
 *
 *    Rev 1.10   11 Oct 2002 21:19:00   iberaha
 * (eVA, Dev SPL # 95) - Added Change Order logic.  This change included changes to DTD, ability to find an existing Req, change or delete lines, etc.  This version also fixes the SplitAccounting issue where Orders were not copying split accounting information from Requisitions.
 *
 *    Rev 1.9   12 Jul 2002 14:03:46   iberaha
 * (eVA, Dev SPL # 87) - Enhanced the Error processing, included FORCE_IMPORT logic.
 *
 *    Rev 1.7   May 09 2002 16:34:48   iberaha
 * (eVA, ER # 59) - Added error accumulation logic
 *
 *    Rev 1.6   Dec 03 2001 15:15:28   iberaha
 * (eVA, ST SPL # 551) - Finalized the fix for ProcureLineItem defaulting functionality.
 *
 *    Rev 1.5   Nov 30 2001 19:06:32   iberaha
 * (temp) - this version contains the preliminary fix to ProcureLineItem defaulting issue reported by eVA.
 *
 *    Rev 1.4   Oct 31 2001 18:01:56   iberaha
 * (e2e, Dev SPL # 26) - Added logic responsible for handling Comments and Attachments.
 *
 *    Rev 1.3   Sep 04 2001 13:55:46   iberaha
 * (e2e, Dev SPL # 12) - Added the section that updates Requisition history (almost identical to Purchase Order history update).
 *
 *    Rev 1.2   Aug 07 2001 11:50:52   ghodum
 * Cleaned up some methods, variables, code, etc. Added in update history logic
 *
 *    Rev 1.1   Aug 01 2001 15:47:24   ghodum
 * Added order history updates
 *
 *    Rev 1.0   Jul 18 2001 12:11:52   smokkapa
 * Initial revision.
 *
 */

/* 09/08/2003: Updates for Ariba 8.0 (Richard Lee) */
/* 09/09/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 09/11/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 09/24/2003: Updates for Ariba 8.0 (Richard Lee) */
/* 02/25/2004: Ariba 8.1 rgiesen dev SPL #28 - Replaced VCEUtil with Field Release Component, which
  will NOT use return codes, instead use exception handling.
*/
/*
	Charlton 2009.10.06 prevent triggers on field change for quantity.  This is is required for cooperation with ACP, as
						ACP includes Contracts triggers to select master agreements for procurement line items.  Generally,
						these MAselection triggers will function properly against requisitions imported and created by the
						BuysenseXMLImport class.  However, BuysenseXMLImport handles the session transaction in such a way
						that a race condition occurs with the triggers called by BaseObject, and the Approvable is not committed
						before the triggers are called.
*/

/**
* CSPL:2726 - Ariba 9r1: order import split accounting
* 
* @author Pavan Aluri
* @Date 17-May-2011
* @Explanation  In 9r1, split type (Amount, Percentage or Quantity) has to be set explicitly. In our case, based on first field tag under first subdetail tag
*  coming from External system data (XML), split type is set. Here first field tag (immediately after <subdetail>) represent split and subdetail tag represent SplitAccounting
*/

package config.java.ams.custom ;

import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.approvable.core.LineItem;
import ariba.approvable.core.Access;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableClassProperties;
import ariba.approvable.core.LineItemCollection;
import java.io.* ;
import java.util.* ;
import org.w3c.dom.* ;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
// Ariba 8.0 depreciated import org.xml.sax.Parser;
import org.xml.sax.Locator;
// Ariba 8.0: added DefaultHandler
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.parsers.* ;
import ariba.base.core.*;
import ariba.base.core.aql.*;
import ariba.common.core.*;
import ariba.approvable.core.Comment;
import ariba.procure.core.*;
import ariba.util.core.*;
import java.util.List ;
import ariba.util.log.Log;
import ariba.base.core.BaseObject;
//import ariba.server.ormsserver.ApprovableOnServer;
import ariba.fieldrelease.core.*;
import ariba.fieldrelease.core.ApprovableActionException;
import ariba.fieldrelease.core.ApprovableHookException;
import ariba.util.core.ListUtil;
import ariba.util.core.StringUtil;
import ariba.util.core.Date;

/**
 * The BuysenseXMLImport class provides XML import capabilties into
 * Ariba to create new Ariba objects.
 *
 * @author Greg Hodum
 *
 */
//81->822 changed Vector to List
//81->822 changed Hashtable to Map
public class BuysenseXMLImport implements ContentHandler, AMSE2EConstants
// Ariba 8.0: Replaced DocmentHandler with ContentHandler
//public class BuysenseXMLImport implements DocumentHandler
{
    /**
     * Debugging flag
     */
    protected static final boolean DEBUG = true ;

    /**
     * Flag to set a validating (w/DTD) parser
     */
    private boolean mboolIsValidatingParser = true ;

    /**
     * SAX parser instance
     */
    private XMLReader moSAXParser = null ;
    private XMLReader SAXParser = null ;

    /**
     * Stack containing the context
     * (ie. header, detail, subdetail, object, field, etc. )
     */
    protected Stack moContextStack = new Stack() ;

    /**
     * Ariba stuff
     */
    protected Partition moPartition = null ;

    /**
     * Buysense client id
     */
    protected String msBuysenseClientId = null ;

    /**
     * Buysense client name
     */
    protected String msBuysenseClientName = null ;

    /**
     * Current header object
     */
    protected ClusterRoot moHeader = null ;

    /**
     * Current detail object
     */
    protected BaseObject moDetail = null ;

    /**
     * Current comment object
     */
    private BaseObject moComment = null ;

    /**
     * Current attachment object
     */
    private BaseObject moAttachment = null ;

    /**
     * Container List object storing all Line Items to be deleted
     */
    //8.1 List private List moLineItmToDeleteVector = new List() ;
    private List moLineItmToDeleteVector = ListUtil.list() ;

    /**
     * Container List object storing all Line Items (or general Details) contained in XML
     */
    //8.1 List private List moLineItmInXMLVector = new List() ;
    private List moLineItmInXMLVector = ListUtil.list() ;

    /**
     * Stores the name of the Req that was last cancelled
     */
    private String msReqCancelName = "" ;

    /**
     * XML element name constant
     */
    protected static final String BUYSENSE_IMPORT = "buysense_import" ;

    /**
     * XML element name constant
     */
    protected static final String BUYSENSE_RECEIPT_IMPORT = "buysense_receipt_import" ;

    /**
     * XML element name constant
     */
    protected static final String PROCESS = "process" ;

    /**
     * XML element name constant
     */
    protected static final String HEADER = "header" ;

    /**
     * XML element name constant
     */
    protected static final String FIELD = "field" ;

    /**
     * XML element name constant
     */
    protected static final String DETAILS = "details" ;

    /**
     * XML element name constant
     */
    protected static final String DETAIL = "detail" ;

    /**
     * XML element name constant
     */
    protected static final String COMMENTS = "comments" ;

    /**
     * XML element name constant
     */
    protected static final String COMMENT = "comment" ;

    /**
     * XML element name constant
     */
    protected static final String ATTACHMENTS = "attachments" ;

    /**
     * XML element name constant
     */
    protected static final String ATTACHMENT = "attachment" ;

    /**
     * XML attribute name constant
     */
    protected static final String COMMENT_LEVEL = "comment_level" ;

    /**
     * XML element name constant
     */
    protected static final String OBJECT = "object" ;

    /**
     * XML element name constant
     */
    protected static final String SUBDETAILS = "subdetails" ;

    /**
     * XML element name constant
     */
    protected static final String SUBDETAIL = "subdetail" ;

    /**
     * XML attribute name constant
     */
    protected static final String ARIBA_FIELD_NAME = "ariba_field_name" ;

    /**
     * XML attribute name constant
     */
    protected static final String ARIBA_TYPE = "ariba_type" ;

    /**
     * XML attribute name constant
     */
    protected static final String ARIBA_REFERENCE = "ariba_reference" ;

    /**
     * XML attribute name constant
     */
    protected static final String ARIBA_VAL_PREFIX = "ariba_value_prefix" ;

    /**
     * XML attribute name constant
     */
    protected static final String ARIBA_VAL_REQUIRED = "ariba_value_required" ;

    /**
     * XML attribute name constant
     */
    protected static final String ERP_FIELD_NAME = "erp_field_name" ;

    /**
     * XML attribute name constant
     */
    protected static final String AUTO_SUBMIT = "auto_submit" ;

    /**
     * XML attribute name constant
     */
    protected static final String FORCE_IMPORT = "force_import" ;

    /**
     * XML attribute name constant
     */
    protected static final String CHANGE_REQ = "change_req" ;

    /**
     * XML attribute name constant
     */
    protected static final String REQ_UNIQUE_NAME = "req_unique_name" ;

    /**
     * XML attribute name constant
     */
    protected static final String COMMAND = "command" ;

    /**
     * XML attribute value constant
     */
    protected static final String ORDER_HIST_TEXT = "order_hist_text" ;

    /**
     * XML attribute value constant
     */
    protected static final String REQUISITION_HIST_TEXT = "requisition_hist_text" ;

    /**
     * XML attribute name constant
     */
    protected static final String CLIENT_ID = "client_id" ;

    /**
     * XML attribute name constant
     */
    protected static final String CLIENT_NAME = "client_name" ;

    /**
     * XML attribute value constant
     */
    protected static final String COMMAND_CANCEL_REQ = "cancel_req" ;

    /**
     * XML attribute value constant
     */
    protected static final String COMMAND_UPDATE_ORDER_HIST = "update_order_hist" ;

    /**
     * XML attribute value constant
     */
    protected static final String COMMAND_UPDATE_REQUISITION_HIST = "update_requisition_hist" ;

    /**
     * Requisition field that contains the original requisition unique name
     */
    protected static final String ORIGINAL_REQ_NAME_FIELD = "OriginatingSystemHeaderUniqueName" ;

    /**
     * Requisition field that contains the originating system line number
     */
    protected static final String ORIGINAL_REQ_LINE_NBR = "OriginatingSystemLineNumber" ;

    /**
     * XML attribute value constant
     */
    protected static final String ORIG_SYS_LINE_NUMBER = "orig_sys_line_number" ;

    /**
     * XML attribute value constant
     */
    protected static final String ADD_CHANGE_DELETE = "add_change_delete" ;

    /**
     * List containing LineItem fields for defaulting
     */
    private static final List lvLineFields = FieldListContainer.getLineFieldDefaultingValues();

    /**
     * List containing accouning LineItem fields for defaulting
     */
    private static final List lvActgFields = FieldListContainer.getAcctgFieldDefaultingValues();

    /**
     * List containing accumulated Error messages
     */
    public List mvErrorList = null ;

    /*
     * Var for Split Accounting List synch up
     */
    private String msAddChangeDelete = null ;

    /*
     * Map containing ids of skipped orders
     */
    private java.util.Map moSkipHash = MapUtil.map();

    /*
     * Assumed time between scheduled runs of BuysenseCreateApprovableTask
     */
    private static final long MIN_GRACE_PERIOD = 900000;     // 15 minutes

    /*
     * Global/common commodity code type and name.  XML passed in will contain only PartitionedCommodityCode,
     * and this program will handle setting CommonCommodityCode.
     */
    private static final String GLOBAL_COMM_CODE_TYP = "ariba.basic.core.CommodityCode";
    private static final String GLOBAL_COMM_CODE_NAME = "Description.CommonCommodityCode";
    
    private String sSplitAccountingType = null;
    //Flag to set Split Type only once
    private boolean bIsSplitTypeSet = false;

    private String sReceivingTypeErrorLines = "" ;
    /**
     * The BuysenseXMLImport constructor.
     */
    public BuysenseXMLImport()
    {
        super() ;
        moPartition = Base.getSession().getPartition() ;
    }


    /**
     * The BuysenseXMLImport constructor.
     */
    public BuysenseXMLImport( Partition foPartition )
    {
        super() ;
        moPartition = foPartition ;
    }


    /**
     * This method returns the name of the import class in use.
     */
    public String getImportClassName()
    {
        return "BuysenseXMLImport" ;
    }


    /**
     * This method creates a new SAX parser instance,
     * if none is yet available or it returns the one
     * currently in use.
     *
     * @return Parser the SAX XML parser
     * @exception BuysenseXMLImportException
     */
    private XMLReader getSAXParser()
        throws BuysenseXMLImportException
    {
        try
        {
            if ( moSAXParser == null )
            {
                SAXParserFactory loSAXFactory = SAXParserFactory.newInstance() ;
                loSAXFactory.setValidating( mboolIsValidatingParser ) ;
                SAXParser loSAXParser = loSAXFactory.newSAXParser() ;
                moSAXParser = (XMLReader) loSAXParser.getParser() ;
                moSAXParser.setContentHandler( this ) ;
                moSAXParser.setErrorHandler( new MyErrorHandler() ) ;
            }
            return moSAXParser ;
        }
        catch ( ParserConfigurationException loParserConfigExcep )
        {
            if ( DEBUG )
            {
                Log.customer.debug( "Error while creating XML parser:" + loParserConfigExcep.getMessage() ) ;
                loParserConfigExcep.printStackTrace() ;
            }

            throw new BuysenseXMLImportException( "Error: could not create XML parser - " +
                                                 loParserConfigExcep.getMessage() ) ;
        }
        catch ( SAXException loSAXExcep )
        {
            if ( DEBUG )
            {
                Log.customer.debug( "could not get parser instance" ) ;
                loSAXExcep.printStackTrace() ;
            }

            throw new BuysenseXMLImportException( "could not get parser instance" ) ;
        }
        catch ( Exception loExcep )
        {
            if ( DEBUG )
            {
                Log.customer.debug( "exception while getting XML parser" ) ;
                loExcep.printStackTrace() ;
            }

            throw new BuysenseXMLImportException( "exception while getting XML parser" ) ;
        }
    }
    /**
     * This method imports from the
     * specified file XML file.
     *
     * @param fsXMLFileName The XML file name to import
     * @exception BuysenseXMLImportException
     */
    public void importFromXML( String fsXMLFileName )
        throws BuysenseXMLImportException
    {
        FileInputStream loFile = null ;
        try
        {
            //Get XML Input File
            loFile = new FileInputStream( fsXMLFileName ) ;
        }
        catch ( FileNotFoundException loFileNotFoundExcep )
        {
            if ( DEBUG )
            {
                Log.customer.debug( "Error: Could not find specified file." ) ;
            }
            return ;
        }
        catch ( SecurityException loSecurityExcep )
        {
            if ( DEBUG )
            {
                Log.customer.debug( "Error: Not enough permissions to access specified file." ) ;
            }
            return ;
        }

        try
        {
            importFromXML( loFile ) ;
        }
        catch ( BuysenseXMLImportException loImportExcep )
        {
            throw loImportExcep ;
        }
        finally
        {
            try
            {
                if ( loFile != null )
                {
                    // Close the XML Input File
                    loFile.close() ;
                    loFile = null ;
                }
            }
            catch ( IOException loIOExcep )
            {
                if ( DEBUG )
                {
                    Log.customer.debug( "Error: Could not close import XML file." ) ;
                }
            }
        }
    }


    /**
     * This method imports from the
     * specified file XML file.
     *
     * @param foXMLInputStream The XML stream to import
     * @exception BuysenseXMLImportException
     */
    public void importFromXML( InputStream foXMLInputStream )
        throws BuysenseXMLImportException
    {
        // Create List object to capture and accumulate non-fatal Errors
        mvErrorList = ListUtil.list();

        parseXML( foXMLInputStream ) ;
    }

    /**
     * This method imports from the XML loaded into in a Reader object
     *
     * @param foXMLReader The XML stream to import
     * @exception BuysenseXMLImportException
     */
    public void importFromXML( Reader foXMLReader )
        throws BuysenseXMLImportException
    {
        // Create List object to capture and accumulate non-fatal Errors
        mvErrorList = ListUtil.list();
        parseXML( foXMLReader ) ;
    }


    /**
     * This method creates a SAX parser instance and attempts
     * to parse the specified XML file.
     *
     * @param foInputStream The XML stream to import
     * @exception BuysenseXMLImportException
     */
    private void parseXML( InputStream foInputStream )
        throws BuysenseXMLImportException
    {
        String lsMessage;
        try
        {
            XMLReader      loParser   = getSAXParser() ;
            InputSource loXMLInput = new InputSource( foInputStream ) ;
            /*
             * Parse XML input
             */
            loParser.parse( loXMLInput ) ;
            loXMLInput = null ;
        }
        catch ( BuysenseXMLImportException loBuysenseExcep )
        {
            lsMessage = loBuysenseExcep.getMessage();
            if (lsMessage != null && lsMessage.startsWith("Info"))
            {
               if ( DEBUG )
               {
                  Log.customer.debug( "A Buysense import error occured:" + lsMessage ) ;
                  loBuysenseExcep.printStackTrace() ;
               }
            }
            throw loBuysenseExcep ;
        }
        catch ( SAXParseException loSAXParseExcep )
        {
            lsMessage = loSAXParseExcep.getMessage();
            if ( DEBUG )
            {
                Log.customer.debug( "An error while parsing the XML data:" + lsMessage ) ;
                loSAXParseExcep.printStackTrace() ;
            }

            throw new BuysenseXMLImportException( "Error: could not parse XML - " + lsMessage );
        }
        catch ( SAXException loSAXExcep )
        {
            lsMessage = loSAXExcep.getMessage();
            if ( DEBUG )
            {
                Log.customer.debug( "An error while parsing the XML data:" + lsMessage ) ;
                loSAXExcep.printStackTrace() ;
            }

            throw new BuysenseXMLImportException( "Error: could not parse XML - " + lsMessage ) ;
        }
        catch ( IOException loIOExcep )
        {
            lsMessage = loIOExcep.getMessage();
            if ( DEBUG )
            {
                Log.customer.debug( "I/O error while parsing XML:" + lsMessage ) ;
                loIOExcep.printStackTrace() ;
            }

            throw new BuysenseXMLImportException( "Error: could not parse XML - " + lsMessage ) ;
        }
        catch ( Exception loExcep )
        {
            lsMessage = loExcep.getMessage();
            if ( DEBUG )
            {
                Log.customer.debug( "Unknown error while parsing XML:" + lsMessage ) ;
                loExcep.printStackTrace() ;
                Log.customer.debug( "Exception: " + SystemUtil.stackTrace(loExcep) );
            }
            throw new BuysenseXMLImportException( "Error: could not parse XML - " + lsMessage );
        }
    }


    /**
     * This method creates a SAX parser instance and attempts
     * to parse the specified XML file. It is a mindless clone of above method
     * that uses the InputStream object.
     *
     * @param foReader The Reader to import
     * @exception BuysenseXMLImportException
     */
    private void parseXML( Reader foReader )
        throws BuysenseXMLImportException
    {
        String lsMessage;
        try
        {
            XMLReader      loParser   = getSAXParser() ;
            InputSource loXMLInput = new InputSource( foReader ) ;
            /*
             * Parse XML input
             */
            loParser.parse( loXMLInput ) ;
            loXMLInput = null ;
        }
        catch ( BuysenseXMLImportException loBuysenseExcep )
        {
            lsMessage = loBuysenseExcep.getMessage();
            if (lsMessage != null && lsMessage.startsWith("Info"))
            {
               if ( DEBUG )
               {
                  Log.customer.debug( "A Buysense import error occured:" + lsMessage ) ;
                  loBuysenseExcep.printStackTrace() ;
               }
            }
            throw loBuysenseExcep ;
        }
        catch ( SAXParseException loSAXParseExcep )
        {
            lsMessage = loSAXParseExcep.getMessage();
            if ( DEBUG )
            {
                Log.customer.debug( "An error while parsing the XML data:" + lsMessage ) ;
                loSAXParseExcep.printStackTrace() ;
            }

            throw new BuysenseXMLImportException( "Error: could not parse XML - " + lsMessage );
        }
        catch ( SAXException loSAXExcep )
        {
            lsMessage = loSAXExcep.getMessage();
            if ( DEBUG )
            {
                Log.customer.debug( "An error while parsing the XML data:" + lsMessage ) ;
                loSAXExcep.printStackTrace() ;
            }

            throw new BuysenseXMLImportException( "Error: could not parse XML - " + lsMessage ) ;
        }
        catch ( IOException loIOExcep )
        {
            lsMessage = loIOExcep.getMessage();
            if ( DEBUG )
            {
                Log.customer.debug( "I/O error while parsing XML:" + lsMessage ) ;
                loIOExcep.printStackTrace() ;
            }

            throw new BuysenseXMLImportException( "Error: could not parse XML - " + lsMessage ) ;
        }
        catch ( Exception loExcep )
        {
            lsMessage = loExcep.getMessage();
            if ( DEBUG )
            {
                Log.customer.debug( "Unknown error while parsing XML:" + lsMessage ) ;
                loExcep.printStackTrace() ;
                Log.customer.debug( "Exception: " + SystemUtil.stackTrace(loExcep) );
            }

            throw new BuysenseXMLImportException( "Error: could not parse XML - " + lsMessage );
        }
    }


    /**
     * This inner class handles any validation errors
     * and warnings
     */
    public class MyErrorHandler extends DefaultHandler
    //public class MyErrorHandler extends HandlerBase
    {
        /*
         * Fatal error handler
         */
        public void fatalError( SAXParseException loSAXExcep )
            throws SAXParseException
        {
            Log.customer.debug( "\n" +
                           "** Parser Fatal Error: " + loSAXExcep.getMessage() + "\n" +
                           "**              Line : " + loSAXExcep.getLineNumber() + "\n" +
                           "**              URI  : " + loSAXExcep.getSystemId() ) ;
            throw loSAXExcep ;
        }

        /*
         * Treat validation errors as fatal
         */
        public void error( SAXParseException loSAXExcep )
            throws SAXParseException
        {
            Log.customer.debug( "\n" +
                           "** Parser Error: " + loSAXExcep.getMessage() + "\n" +
                           "**        Line : " + loSAXExcep.getLineNumber() + "\n" +
                           "**        URI  : " + loSAXExcep.getSystemId() ) ;
            throw loSAXExcep ;
        }

        /*
         * Dump warnings too
         */
        public void warning( SAXParseException loSAXExcep )
            throws SAXParseException
        {
            Log.customer.debug( "\n" +
                           "** Parser Warning: " + loSAXExcep.getMessage() + "\n" +
                           "**          Line : " + loSAXExcep.getLineNumber() + "\n" +
                           "**          URI  : " + loSAXExcep.getSystemId() ) ;
        }
    }


    /**
     * Returns the newly created header object.
     *
     * @return ClusterRoot the header object as a ClusterRoot,
     * or null if none was created
     */
    public ClusterRoot getHeaderObject()
    {
        return moHeader ;
    }


    /**
     * SAX XML parser event handler.
     */
    public void setDocumentLocator( Locator foLocator )
    {
        // we'd record this if we needed to resolve relative URIs
        // in content or attributes, or wanted to give diagnostics.
    }


    /**
     * SAX XML parser event handler.
     * @exception SAXException
     */
    public void startDocument()
        throws SAXException
    {
    }

    /**
     * SAX XML parser event handler.
     * @exception SAXException
     */
    public void endDocument()
        throws SAXException
    {
    }
    /**
     * SAX XML parser event handler.
     * @exception SAXException
     */
    public void skippedEntity( String skippedEnt )
        throws SAXException
    {
        //processStartElement( namsspaceURI, fsTagName, rawName, foAttrList ) ;
        // processStartElement( fsTagName, foAttrList ) ;
    }


    /**
     * SAX XML parser event handler.
     * @exception SAXException
     * rlee
     */
    public void startElement( String namespaceURI,
                             String fsTagName,
                             String rawName, Attributes foAttrList )
        throws SAXException
    {
        processStartElement( namespaceURI, fsTagName, rawName, foAttrList ) ;
    }

    /**  Ariba 8.0: added this because of ContentHandler replaced DocumentHandler
     * SAX XML parser event handler.
     * @exception SAXException
     */
    public void startPrefixMapping( String rawName, String namespaceURI )
        throws SAXException
    {
    }

    /**
     * SAX XML parser event handler.
     * @exception SAXException
     */
    public void endElement( String namespaceURI,
                           String fsTagName, String rawName )
        //public void endElement( String fsTagName, AttributeList foAttrList )
        throws SAXException
    {
        processEndElement( namespaceURI, fsTagName, rawName ) ;
        //processEndElement( fsTagName ) ;
    }

    /**  Ariba 8.0: added this because of ContentHandler replaced DocumentHandler
     * SAX XML parser event handler.
     * @exception SAXException
     */
    public void endPrefixMapping( String prefix )
        throws SAXException
    {
    }

    /**
     * SAX XML parser event handler.
     * @exception SAXException
     */
    public void characters( char fcBuf[], int fiOffset, int fiLen )
        throws SAXException
    {
        processCharData( fcBuf, fiOffset, fiLen ) ;
    }


    /**
     * SAX XML parser event handler.
     * @exception SAXException
     */
    public void ignorableWhitespace( char foBuf[], int fiOffset, int fiLen )
        throws SAXException
    {
        // Just ignore the whitespace, we don't need it
        return ;
    }


    /**
     * SAX XML parser event handler.
     * @exception SAXException
     */
    public void processingInstruction( String fsTarget, String fsData )
        throws SAXException
    {
        // Implement this method if processing instructions are required
        return ;
    }


    /**
     * This method processes the start of an XML tag.
     * The developer should not call this method directly.
     *
     * @exception BuysenseXMLImportException
     */
    protected void processStartElement( String namespaceURI,
                                       String localName,
                                       String rawName, Attributes foAttrList )
        throws BuysenseXMLImportException
    {

        Log.customer.debug( "BuysenseXMLImport:processStartElement(): localName: " + localName + " rawName: " + rawName + " namespaceURI: " +namespaceURI);

      //Ariba 8.1 Most likely, the namespaceURI will be empty.  It uses this if you are pointing to a url
      //In our case, we are pointing to a database column, so it is an input stream.  In that
      //case, the tag will be stored in the rawname, instead of the localname

      String fsTagName;
        if ("".equals(namespaceURI))
        {
         fsTagName=rawName;
      }
      else
      {
         fsTagName=localName;
      }

        ContextObject loNewContext = addToContext( fsTagName, foAttrList ) ;

        if ( fsTagName.equalsIgnoreCase( FIELD ) )
        {
            startField( loNewContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( DETAIL ) )
        {
            startDetail( loNewContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( COMMENT ) )
        {
            startComment( loNewContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( ATTACHMENT ) )
        {
            startAttachment( loNewContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( SUBDETAIL ) )
        {
            startSubDetail( loNewContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( DETAILS ) )
        {
            startDetails( loNewContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( COMMENTS ) )
        {
            startComments( loNewContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( ATTACHMENTS ) )
        {
            startAttachments( loNewContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( SUBDETAILS ) )
        {
            startSubDetails( loNewContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( OBJECT ) )
        {
            startObject( loNewContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( HEADER ) )
        {
            startHeader( loNewContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( PROCESS ) )
        {
            startProcess( loNewContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( BUYSENSE_IMPORT ) )
        {
            startBuysenseImport( loNewContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( BUYSENSE_RECEIPT_IMPORT ) )
        {
            startBuysenseImport( loNewContext ) ;
        }
        else
        {
            /*
             * Unknown/unused tag encountered, throw exception.
             * The DTD validation should catch this anyway.
             */
            throw new BuysenseXMLImportException(
                                                 "processStartElement() Error: unknown tag encountered: " + fsTagName ) ;
        }
    }


    /**
     * This method processes the end of an XML tag.
     * The developer should not call this method directly.
     *
     * @param fsTagName The XML tag name parsed
     * @exception BuysenseXMLImportException
     */
    protected void processEndElement( String namespaceURI,
                                     String localName, String rawName )
        throws BuysenseXMLImportException
    {

        Log.customer.debug( "BuysenseXMLImport:processEndElement(): localName: " + localName + " rawName: " + rawName + " namespaceURI: " +namespaceURI);

      //Ariba 8.1 Most likely, the namespaceURI will be empty.  It uses this if you are pointing to a url
      //In our case, we are pointing to a database column, so it is an input stream.  In that
      //case, the tag will be stored in the rawname, instead of the localname

      String fsTagName;

        if ("".equals(namespaceURI))
        {
         fsTagName=rawName;
      }
      else
      {
         fsTagName=localName;
      }

        ContextObject loOldContext = removeFromContext( fsTagName ) ;

        Log.customer.debug( "BuysenseXMLImport.processEndElementElement() = " + loOldContext.getName() ) ;
        Log.customer.debug( "BuysenseXMLImport.processEndElementext()    = " + loOldContext.getText() ) ;

        if ( fsTagName.equalsIgnoreCase( FIELD ) )
        {
            finishField( loOldContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( DETAIL ) )
        {
            finishDetail( loOldContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( COMMENT ) )
        {
            finishComment( loOldContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( ATTACHMENT ) )
        {
            finishAttachment( loOldContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( SUBDETAIL ) )
        {
            finishSubDetail( loOldContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( DETAILS ) )
        {
            finishDetails( loOldContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( COMMENTS ) )
        {
            finishComments( loOldContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( ATTACHMENTS ) )
        {
            finishAttachments( loOldContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( SUBDETAILS ) )
        {
            finishSubDetails( loOldContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( OBJECT ) )
        {
            finishObject( loOldContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( HEADER ) )
        {
            finishHeader( loOldContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( PROCESS ) )
        {
            finishProcess( loOldContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( BUYSENSE_IMPORT ) )
        {
            finishBuysenseImport( loOldContext ) ;
        }
        else if ( fsTagName.equalsIgnoreCase( BUYSENSE_RECEIPT_IMPORT ) )
        {
        	finishBuysenseImport( loOldContext ) ;
        }
        else
        {
            /*
             * Unknown/unused tag encountered, throw exception.
             * The DTD validation should catch this anyway.
             */
            throw new BuysenseXMLImportException(
                                                 "processEndElement() Error: unknown tag encountered: " + fsTagName ) ;
        }
    }


    /**
     * This method processes the character data of XML
     * tag data.
     *
     * @param buf[] The tag data buffer
     * @param offset The offset of the tag data
     * @param len The length of the tag data
     */
    protected void processCharData( char fcBuf[], int fiOffset, int fiLen )
    {
        String lsTempData = new String( fcBuf, fiOffset, fiLen ) ;

        if ( DEBUG )
        {
            Log.customer.debug( "char data = \"" + lsTempData + "\"" ) ;
        }

        ContextObject loContext = (ContextObject)moContextStack.peek() ;

        if ( loContext != null )
        {
            loContext.appendText( lsTempData ) ;
        }
    }


    /**
     * Adds the current element to the top of the context stack.
     */
    private ContextObject addToContext( String fsName, Attributes foAttrList )
    {
        if ( DEBUG )
        {
            Log.customer.debug( "addToContext" ) ;
        }

        /*
         * Add to the current context
         */
        ContextObject loCntxObj = new ContextObject( fsName, foAttrList ) ;

        /*
         * Push it onto the context stack
         */
        moContextStack.push( loCntxObj ) ;

        return loCntxObj ;
    }


    /**
     * Removes the current element from the top of the context stack.
     */
    private ContextObject removeFromContext( String fsName )
    {
        if ( DEBUG )
        {
            Log.customer.debug( "removeFromContext") ;
        }

        /*
         * Remove from the context
         */
        return ((ContextObject)moContextStack.pop() ) ;
    }


    /**
     * Returns the element from the top of the context stack.
     */
    private ContextObject getCurrentContext()
    {
        if ( DEBUG )
        {
            Log.customer.debug( "getCurrentContext") ;
        }

        /*
         * Gets the current context
         */
        return ((ContextObject)moContextStack.peek() ) ;
    }


    /**
     * This method processes the start of the buysense import section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void startBuysenseImport( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        /*
         * Get and store the Buysense client id and name
         */
        msBuysenseClientId   = foMyContext.getAttrValue( CLIENT_ID ) ;
        msBuysenseClientName = foMyContext.getAttrValue( CLIENT_NAME ) ;

        msReqCancelName = "" ;
    }


    /**
     * This method processes the start of the process element.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void startProcess( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {

        // Set the member level Approvable Object handle to null for each new XML ...
        // this prevents the wrong one from being deactivated upon Exception
        moHeader = null;                 // ST SPL 608
        return ;
    }


    /**
     * This method processes the start of the header section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void startHeader( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        /*
         * Create a new Base object representing the header
         */
        try
        {
            ClusterRoot loNewObj      = null ;
            ClusterRoot loExistingObj = null ;
            String      lsAribaType   = BuyintXWalk.XWalk.changeOldToNew( foMyContext.getAttrValue(ARIBA_TYPE) ) ;
            String      lsChangeReq   = foMyContext.getAttrValue( CHANGE_REQ ) ;

            // Set the member level Approvable Object handle to null for each new XML ...
            // this prevents the wrong one from being deactivated upon Exception
            moHeader = null;                 // ST SPL 619

            try
            {
                wait( 1000 ) ;
            }
            catch( Exception ex )
            {
            }

            //Find if an approvable with the UniqueName exists
            loExistingObj = findApprovable( foMyContext ) ;

            if ( loExistingObj != null )
            {
                if ( DEBUG )
                {
                    Log.customer.debug( "BuysenseXMLImport.startHeader() Trying to change-requisition ..." ) ;
                }
                loNewObj = changeReq( loExistingObj ) ;
                Log.customer.debug("BuysenseXMLImport::Changed REQ UniqueName "+loNewObj.getUniqueName());
                // jackie 81->822 ((Vector)loNewObj.getFieldValue("Comments")).removeAllElements();
                ((List)loNewObj.getFieldValue("Comments")).clear();
                loNewObj.setActive(true);
                Log.customer.debug("BuysenseXMLImport::After setting to Active");
            }
            else
            {
                loNewObj = (ClusterRoot)BaseObject.create( lsAribaType,
                                                          moPartition ) ;
            }

            if ( loNewObj == null )
            {
                if ( DEBUG )
                {
                    Log.customer.debug( "Header object is null " ) ;
                }
                throw new BuysenseXMLImportException(
                                                     "Error: null value returned when creating header" ) ;
            }

            moHeader = loNewObj ;
            foMyContext.setObject( loNewObj ) ;
            Log.customer.debug("BuysenseXMLImport::After setting context to foMyContext");

            if ( DEBUG )
            {
                Log.customer.debug( "Finish create header..." ) ;
            }
        }
        catch ( BuysenseXMLImportException loImpException )
        {
            String lsMessage = loImpException.getMessage();
            if (lsMessage != null && lsMessage.startsWith("Info"))
            {
               // If informational message, no need to print stack trace ...
            }
            else
            {
               loImpException.printStackTrace();
               lsMessage = "Error: Exception while creating header: " + lsMessage;
            }
            throw new BuysenseXMLImportException(lsMessage);
        }
        catch ( Exception loException )
        {
            loException.printStackTrace();
            throw new BuysenseXMLImportException( "Error: Exception while creating header: " + loException.getMessage() ) ;
        }
    }


    /**
     * This method processes the start of the field element.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void startField( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        return ;
    }


    /**
     * This method processes the start of the deatails section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void startDetails( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        /*
         * Create a new List object to hold the line items to be created later
         */
        foMyContext.setObject( ListUtil.list() ) ;

    }


    /**
     * This method processes the start of a detail line item.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void startDetail( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {

            BaseObject loBaseObj = null ;
            BaseVector loLineItemCollection = null ;
            LineItem   loLineItem = null ;

            String     lsAddChangeDelete   = foMyContext.getAttrValue( ADD_CHANGE_DELETE ) ;
            if (lsAddChangeDelete == null)
            {
                lsAddChangeDelete = "";          // if null, set to blank to enable String compare down the line
            }
            else
            {
                lsAddChangeDelete = lsAddChangeDelete.trim();
            }

            String     lsOrigSysLineNumber = foMyContext.getAttrValue( ORIG_SYS_LINE_NUMBER ) ;
            if ( lsOrigSysLineNumber == null )
            {
                // assume line is being added
                Log.customer.debug( "BuysenseXMLImport.startDetail() Line number is null ... will assume ADD and attempt to create new line") ;
                lsAddChangeDelete = "ADD";
            }

            else
            {
                lsOrigSysLineNumber = lsOrigSysLineNumber.trim();   // to prevent Exception due to leading/trailing spaces
                if ( DEBUG )
                {
                    Log.customer.debug( "Looking for original system line number: "
                                   + lsOrigSysLineNumber ) ;
                }
                try
                {
                    int liOrigSysLineNumber = Integer.parseInt( lsOrigSysLineNumber ) ;
                    loLineItemCollection = ( (LineItemCollection)getHeaderObject() ).getLineItems() ;
                    Log.customer.debug("LineItemCollection is " + loLineItemCollection);

                    for ( int i=0; i<loLineItemCollection.size(); i++ )
                    {
                        loLineItem = (LineItem)loLineItemCollection.get( i ) ;

                        // Verify that the Orig Sys line nbr on the req line is valid (non-null integer)
                        // for comparing with the one specified on the xml

                        if (loLineItem.getFieldValue(ORIGINAL_REQ_LINE_NBR) == null)
                        {
                            throw new Exception( "OriginatingSystemLineNumber not valid on Req Line ... cannot process" );
                        }

                        // OriginatingSystemLineNumber on Req is definitely an integer if not null ... safe to cast
                        int liNumberOnReq =  ( (Integer)loLineItem.getFieldValue(ORIGINAL_REQ_LINE_NBR) ).intValue();
                        int liMatchedLineNumber = -1;

                        if (liNumberOnReq == liOrigSysLineNumber )
                        {
                            Log.customer.debug( "Found match for original system line number: " + liOrigSysLineNumber ) ;
                            Log.customer.debug( "Temp - with base id: " +  loLineItem ) ;
                            if (lsAddChangeDelete.equalsIgnoreCase("ADD"))
                            {
                                throw new Exception("Cannot ADD line " + liOrigSysLineNumber + " ... already exists") ;
                            }
                            else
                            {
                                loBaseObj = loLineItem ;
                                if (lsAddChangeDelete.equalsIgnoreCase("DELETE"))
                                {
                                    // nothing to do now ... line will be added to the delete items vector at finish
                                }
                                else
                                {
                                    // assume line is being changed
                                    lsAddChangeDelete = "CHANGE";
                                }
                                break;
                            }
                        }
                    }

                    if (loBaseObj == null)   // we did not find a matching line
                    {
                        Log.customer.debug( "Did not find  match for original system line number: " + liOrigSysLineNumber ) ;
                        Log.customer.debug( "Temp - with base id: " +  loLineItem ) ;
                        if (lsAddChangeDelete.equalsIgnoreCase("CHANGE")
                                || lsAddChangeDelete.equalsIgnoreCase("DELETE"))
                        {
                            throw new Exception("Cannot CHANGE or DELETE line " + liOrigSysLineNumber + " ... does not exist") ;
                        }
                        else
                        {
                            // assume line is being added
                            Log.customer.debug( "Will attempt to add line number " + liOrigSysLineNumber + " to Req" ) ;
                            lsAddChangeDelete = "ADD";
                        }
                    }
                }

                catch ( NumberFormatException loNumberFormatException )
                {
                    Log.customer.debug( "Could not resolve : " + lsOrigSysLineNumber + "as int." ) ;
                    throw new Exception("Cannot process detail line ... \"" + lsOrigSysLineNumber + "\" is not a valid line number") ;
                }
            }

            // set the value in the context in case it has changed
            foMyContext.setAttrValue( ADD_CHANGE_DELETE, lsAddChangeDelete ) ;

            msAddChangeDelete = lsAddChangeDelete;

            if (lsAddChangeDelete.equalsIgnoreCase("ADD"))
            {
                /*
                 * Create a new BaseObject object representing the detail
                 */
                loBaseObj = (BaseObject)BaseObject.create(
                                                          BuyintXWalk.XWalk.changeOldToNew( foMyContext.getAttrValue(ARIBA_TYPE) ),
                                                          moPartition ) ;
                /*
                 * If the detail element is an instance of ProcureLineItem
                 * call the defaulting logic as this is not done by the ussual trigger
                 * in Ariba.
                 */
                if ( loBaseObj instanceof ProcureLineItem )
                {
                    defaultProcureLineItems( (ProcureLineItem)loBaseObj ) ;
                }
            }
            // Add to a running List of XML Details
            moLineItmInXMLVector.add( loBaseObj ) ;
            moDetail = loBaseObj ;
            foMyContext.setObject( loBaseObj ) ;
        }
        catch ( Exception loException )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: exception while creating detail: " +
                                                 loException.getMessage() ) ;
        }
    }

    /**
     * This method processes the start of the comments section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void startComments( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        if ( DEBUG )
        {
            Log.customer.debug( "In startComments()" ) ;
        }
        /*
         * Create a new List object to hold the comments to be created later
         */
        foMyContext.setObject( ListUtil.list() ) ;

    }


    /**
     * This method processes the start of a comment.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void startComment( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {
            BaseObject loBaseObj = null ;

            /*
             * Create a new BaseObject object representing the comment
             */
            loBaseObj = (BaseObject)BaseObject.create(
                                                      BuyintXWalk.XWalk.changeOldToNew( foMyContext.getAttrValue(ARIBA_TYPE) ),
                                                      moPartition ) ;
            moComment = loBaseObj ;
            foMyContext.setObject( loBaseObj ) ;
        }
        catch ( Exception loException )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: exception while creating comment: " +
                                                 loException.getMessage() ) ;
        }
    }


    /**
     * This method processes the start of the attachments section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void startAttachments( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        if ( DEBUG )
        {
            Log.customer.debug( "In startAttachments()" ) ;
        }

        /*
         * Create a new List object to hold
         * the attachments to be created later
         */
        if ( moComment != null )
        {
            List loAttachments = ((Comment)moComment).getAttachments() ;
            foMyContext.setObject( loAttachments ) ;
        }
    }


    /**
     * This method processes the start of an attachment.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void startAttachment( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {
            BaseObject loBaseObj = null ;

            /*
             * Create a new BaseObject object representing the comment
             */
            loBaseObj = (BaseObject)BaseObject.create(
                                                      BuyintXWalk.XWalk.changeOldToNew( foMyContext.getAttrValue(ARIBA_TYPE) ),
                                                      moPartition ) ;
            moAttachment = loBaseObj ;
            foMyContext.setObject( loBaseObj ) ;
        }
        catch ( Exception loException )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: exception while creating attachment: " +
                                                 loException.getMessage() ) ;
        }
    }


    /**
     * This method processes the start of an object element section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void startObject( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {
            BaseObject loBaseObj = null ;

            /*
             * Create a new BaseObject object representing the object
             */
            loBaseObj = (BaseObject)BaseObject.create(
                                                      BuyintXWalk.XWalk.changeOldToNew( foMyContext.getAttrValue(ARIBA_TYPE) ),
                                                      moPartition ) ;

            foMyContext.setObject( loBaseObj ) ;
        }
        catch ( Exception loException )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: exception while creating object: " +
                                                 loException.getMessage() ) ;
        }
    }


    /**
     * This method processes the start of the subdetails section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void startSubDetails( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        if ( DEBUG )
        {
            Log.customer.debug( "In startSubDetails()" ) ;
        }

        if ( moDetail != null )
        {
            SplitAccountingCollection loActgs =
                ((ProcureLineItem)moDetail).getAccountings() ;

            /*
             * Delete any existing account lines, if any exist
             */
            loActgs.getSplitAccountings().clear() ;
            if (msAddChangeDelete.equalsIgnoreCase("ADD"))
            {
               loActgs.setFieldValue("HighestDeletedSplitNumber", new Integer(0));
            }
        }
    }


    /**
     * This method processes the start of the subdetail line item.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void startSubDetail( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {
            BaseObject loBaseObj = null ;

            if ( moDetail != null )
            {
                //SplitAccountingCollection loActgs =
                //   ((ProcureLineItem)moDetail).getAccountings() ;

                /*
                 * Add new accounting line
                 */
                //loActgs.addSplit() ;
                //loBaseObj = (BaseObject)loActgs.getSplitAccountings().lastElement() ;
                loBaseObj = (BaseObject) new SplitAccounting( moPartition ) ;
            }

            //Default Accounting line
            defaultActgLine( (SplitAccounting) loBaseObj ) ;

            foMyContext.setObject( loBaseObj ) ;
        }
        catch ( Exception loException )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: exception while creating sub-detail: " +
                                                 loException.getMessage() ) ;
        }
    }


    /**
     * This method processes the end of the buysense import section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void finishBuysenseImport( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        msBuysenseClientId   = null ;
        msBuysenseClientName = null ;

        msReqCancelName = "" ;
    }


    /**
     * This method processes the end of the process tag.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void finishProcess( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {
            BaseObject loBaseObject = null ;
            String     lsAribaVal   = foMyContext.getText() ;
            String     lsValPrefix  = foMyContext.getAttrValue( ARIBA_VAL_PREFIX ) ;
            String     lsCommand    = foMyContext.getAttrValue( COMMAND ) ;
            String     lsAribaType  = "ariba.purchasing.core.Requisition" ;
            AQLQuery   loAQL        = new AQLQuery( lsAribaType ) ;
            AQLOptions loAQLOptions = new AQLOptions( moPartition ) ;

            if ( lsCommand != null )
            {
                if ( lsCommand.equalsIgnoreCase( COMMAND_CANCEL_REQ ) )
                {
                    if ( ( lsValPrefix != null )
                            && ( lsValPrefix.length() > 0 ) )
                    {
                        /*
                         * Prepend the prefix onto value before going further
                         */
                        lsAribaVal = lsValPrefix + lsAribaVal ;
                    }

                    /*
                    loBaseObject =
                       (BaseObject)(Base.getService().objectMatchingUniqueName(
                                                  "ariba.purchasing.core.Requisition",
                                                   moPartition, lsAribaVal ) ) ;
                    */

                    loAQL.andEqual( "InitialUniqueName", lsAribaVal ) ;
                    loAQL.addOrderByElement( "VersionNumber", false ) ;
                    loAQL.andEqual( "Active", Boolean.TRUE ) ;

                    loAQLOptions.setUserPartition( moPartition ) ;
                    // Ariba 8.1 Dev SPL #28 - replace VCEUtil with Field Release
                    loBaseObject = FRCommonFacade.getFirstAQLResult( loAQL, loAQLOptions );

                    if ( loBaseObject != null )
                    {
                    	Approvable loExisitingReq = (Approvable)loBaseObject;
                    	boolean bypassApprovers = ((Boolean) loExisitingReq.getFieldValue("BypassApprovers")).booleanValue();
         		       String lsStatusString = (String)loExisitingReq.getDottedFieldValue( "StatusString" ) ;
         		       if ( DEBUG )
       		            {
       		                Log.customer.debug( "Status of requisition to be cancelled = " +
       		                           lsStatusString ) ;
       		            }
         		       if(!bypassApprovers)
         		       {
       		              if(lsStatusString != null && lsStatusString.equalsIgnoreCase("Composing"))
       		                {
       		    	          Log.customer.debug("BuysenseXMLImport::Requisition is about to be cancelled since the requisition status is composing");
       		    	          loExisitingReq.setActive(false);
       		    	          return;
       		                }
       		              else
       		              {
       		            	throw new BuysenseXMLImportException("ERROR: Requisition cannot be cancelled as Req is not in composing state" ) ;
       		              }
       		           
         		       }
                        moHeader = null ;
                        cancelReq((Requisition)loBaseObject ) ;
                    }
                    else
                    {
                        throw new BuysenseXMLImportException(
                                                             "ERROR: Could not lookup requistion " +
                                                             lsAribaVal + " to cancel." ) ;
                    }
                }
                else if ( lsCommand.equalsIgnoreCase( COMMAND_UPDATE_ORDER_HIST ) )
                {
                    String lsHistText = foMyContext.getAttrValue( ORDER_HIST_TEXT ) ;

                    if ( ( lsValPrefix != null )
                            && ( lsValPrefix.length() > 0 ) )
                    {
                        /*
                         * Prepend the prefix onto value before going further
                         */
                        lsAribaVal = lsValPrefix + lsAribaVal ;
                    }

                    loBaseObject =
                        (BaseObject)(Base.getService().objectMatchingUniqueName(
                                                                                "ariba.purchasing.core.ERPOrder",
                                                                                moPartition, lsAribaVal ) ) ;

                    if ( loBaseObject != null )
                    {
                        updateApprovableHistory( loBaseObject,
                                                "SolicitationStatus",
                                                lsHistText ) ;
                    }
                    else
                    {
                        throw new BuysenseXMLImportException(
                                                             "ERROR: Unable to update Order history. " +
                                                             "Cannot lookup Order: " + lsAribaVal + "." ) ;
                    }
                }
                else if ( lsCommand.equalsIgnoreCase( COMMAND_UPDATE_REQUISITION_HIST ) )
                {
                    String lsHistText = foMyContext.getAttrValue( REQUISITION_HIST_TEXT ) ;

                    if ( ( lsValPrefix != null )
                            && ( lsValPrefix.length() > 0 ) )
                    {
                        /*
                         * Prepend the prefix onto value before going further
                         */
                        lsAribaVal = lsValPrefix + lsAribaVal ;
                    }

                    loBaseObject =
                        (BaseObject)(Base.getService().objectMatchingUniqueName(
                                                                                "ariba.purchasing.core.Requisition",
                                                                                moPartition, lsAribaVal ) ) ;

                    if ( loBaseObject != null )
                    {
                        updateApprovableHistory( loBaseObject,
                                                "SolicitationStatus",
                                                lsHistText ) ;
                    }
                    else
                    {
                        throw new BuysenseXMLImportException(
                                                             "ERROR: Unable to update Order history. " +
                                                             "Cannot lookup Order: " + lsAribaVal + "." ) ;
                    }
                }
                else
                {
                    throw new BuysenseXMLImportException(
                                                         "Invalid command specified: " + lsCommand ) ;
                }
            }
            else
            {
                throw new BuysenseXMLImportException(
                                                     "Invalid command specified: " + lsCommand ) ;
            }
        }
        catch ( BuysenseXMLImportException loBuyException )
        {
            throw loBuyException ;
        }
        catch ( Exception loException )
        {
         Log.customer.debug( "Exception: " +SystemUtil.stackTrace(loException));
            throw new BuysenseXMLImportException(
                                                 "Error: exception while finishing \"process\" tag: " +
                                                 loException.getMessage() ) ;
        }
    }


    /**
     * This method processes the end of the header section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void finishHeader( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {
            ClusterRoot loNewObj    = null ;
            String      lsAribaType = BuyintXWalk.XWalk.changeOldToNew( foMyContext.getAttrValue(ARIBA_TYPE) ) ;

            /*
             * We are done, so now try to save the cluster root to
             * persist all changes
             */
            if ( ( loNewObj = (ClusterRoot)foMyContext.getObject() ) != null )
            {
                /*
                 * If this is a modifcation of a previous Req, update the
                 * history to reflect this fact
                 */
                if ( msReqCancelName.length() > 0 )
                {
                    updateApprovableHistory( loNewObj,
                                            "ReqModification",
                                            "This Request is a modification of Request: " +
                                            msReqCancelName ) ;
                }
                msReqCancelName = "" ;
                if ( !loNewObj.save() )
                {
                    if ( DEBUG )
                    {
                        Log.customer.debug( "Error: Could not save the header." ) ;
                    }

                    throw new BuysenseXMLImportException(
                                                         "Error: Unable to save the header." );
                }
                else
                {
                    // update the current requisition with the original requisition number passed in xml
                    updateApprovableHistory( loNewObj,
                                            "ReqImportSuccess",
                                            "This request is a result of the original request with ID: " +
                                            loNewObj.getDottedFieldValue(ORIGINAL_REQ_NAME_FIELD) ) ;
                    // Since the requisition is created, update the original requisition history
                    //rlee eProc Shakedown # 96 Mod: update Orig Req for success only if Severity is not 3
                    if ( ! mvErrorList.isEmpty() )
                    {
                        if ((new Integer((String) mvErrorList.get(0))).intValue() != 3 )
                        {
                            updateOrigReqHistory( loNewObj,
                                                 "ReqImportSuccess",
                                                 "New Request " + loNewObj.getDottedFieldValue("UniqueName") +
                                                 " created after solicitation process" );
                        }
                    }
                    else
                    {
                        updateOrigReqHistory( loNewObj,
                                             "ReqImportSuccess",
                                             "New Request " + loNewObj.getDottedFieldValue("UniqueName") +
                                             " created after solicitation process" );
                    }
                    /*
                     * Attempt to auto-submit, if nescessary
                     */
                    String lsAutoSbmt    = foMyContext.getAttrValue( AUTO_SUBMIT ) ;
                    String lsForceImport = foMyContext.getAttrValue( FORCE_IMPORT ) ;
                    String lsChangeReq   = foMyContext.getAttrValue( CHANGE_REQ ) ;

                    if ( ( ( Boolean.valueOf( lsAutoSbmt ) ).booleanValue() ) &&
                            ( loNewObj instanceof Approvable ) )
                    {
                        if ( DEBUG )
                        {
                            Log.customer.debug( "Trying to auto-submit..." ) ;
                        }
                        autoSubmit((Approvable)loNewObj, lsForceImport, lsChangeReq);
                    }
                }
            }
            else
            {
                if ( DEBUG )
                {
                    Log.customer.debug("Error: Could not get header object to save.");
                }

                throw new BuysenseXMLImportException(
                                                     "Error: No header object to save." ) ;
            }
         loNewObj.save();
         /*
          * rlee UAT SPL 333 Import Req unsuccessful but not getting deleted in Ariba
          * replaced transactionCommit with sessionCommit
          */
         Base.getSession().sessionCommit();
        }
        catch ( BuysenseXMLImportException loBuyException )
        {
            throw loBuyException ;
        }
        catch ( Exception loException )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: exception while finishing header: " +
                                                 loException.getMessage() ) ;
        }
    }

    /**
     * This method processes the end of the field element.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void finishField( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {
            BaseObject    loBaseObj       = null ;
            ContextObject loParentContext = getCurrentContext() ;
            String        lsAribaVal      = foMyContext.getText() ;
            
            if ( ( loBaseObj = (BaseObject)loParentContext.getObject() ) != null )
            {
                Object loValueObj  = null ;
                Object loGlobalCommCodeObj = null ;
                String lsAribaName = foMyContext.getAttrValue( ARIBA_FIELD_NAME ) ;
                String lsAribaRef  = foMyContext.getAttrValue( ARIBA_REFERENCE ) ;
                String lsValPrefix = foMyContext.getAttrValue( ARIBA_VAL_PREFIX ) ;
                String lsAribaTyp  = BuyintXWalk.XWalk.changeOldToNew( foMyContext.getAttrValue(ARIBA_TYPE) ) ;
                
                //Check for ariba field name in field tag and set split type accordingly while updating SplitAccounting object in finishSubDetail() method
                if (loBaseObj instanceof SplitAccounting)
                {
                    Log.customer.debug( "loBaseObj is instanceof SplitAccounting") ;
                    if (lsAribaName.indexOf("Amount") >= 0)
                    {
                        sSplitAccountingType = "Amount";
                    }
                    else if (lsAribaName.indexOf("Percentage") >= 0)
                    {
                        sSplitAccountingType = "Percentage";
                    }
                    else if (lsAribaName.indexOf("Quantity") >= 0)
                    {
                        sSplitAccountingType = "Quantity";
                    }   
                }
                
                if ( ( lsAribaVal == null ) || ( lsAribaVal.length() < 1 ) )
                {
                    // There is no value to set from ERP,
                    // so make sure value is reset in BaseObject
                    if ( BuysenseXMLFieldParser.resetFieldValue( loBaseObj,
                                                                lsAribaName,
                                                                lsAribaTyp ) )
                    {
                        return ;
                    }
                    else
                    {
                        if ( DEBUG )
                        {
                            Log.customer.debug( "Error: could not reset field: " +
                                           lsAribaName ) ;
                        }

                        // Instead of throwing an Exception, start accumulating Errors
                        addToErrorList( "Error: could not reset field: " +
                                       lsAribaName ) ;
                    }
                }

                if ( ( lsValPrefix != null ) && ( lsValPrefix.length() > 0 ) )
                {
                    /*
                     * Prepend the prefix onto value before going further
                     */
                    lsAribaVal = lsValPrefix + lsAribaVal ;
                }

                if ( ( lsAribaRef != null ) && ( lsAribaRef.equals( "true" ) ) )
                {
                    /*
                     * Try to lookup the referenced object
                     */
                    // ABR - CSPLU-36
                    // For CommodityCode field, we also need to set Description.CommonCommodityCode.
                    // As we are a single variant, single partition implementation using only one commodity code set,
                    // the values for the two fields will be the same. Using this as a workaround and the correct
                    // resolution is that setting PartitionedCommodityCode field should set CommonCommodityCode.
                    // SR#1-569501706 is tracking this issue with Ariba.
                    if (lsAribaName.indexOf("CommodityCode") >= 0)
                    {
                        loGlobalCommCodeObj = BuysenseXMLFieldParser.lookupFieldValue( lsAribaVal,
                                                                                       GLOBAL_COMM_CODE_TYP,
                                                                                       moPartition ) ;
                    }
                    loValueObj = BuysenseXMLFieldParser.lookupFieldValue( lsAribaVal,
                                                                         lsAribaTyp,
                                                                         moPartition ) ;
                }
                else
                {
                    /*
                     * Try to get this field's value as an Object
                     */
                    loValueObj = BuysenseXMLFieldParser.parseFieldValue( lsAribaVal,
                                                                        lsAribaTyp ) ;
                }

                /*
                 * Try to set this field's value object into the current BaseObject
                 */
                if ( loValueObj != null )
                {
                    if (lsAribaName.indexOf("CommodityCode") >= 0)
                    {
                        // Set the common/global commodity code using the same value as the partitioned comm code passed in the XML
                        loBaseObj.setDottedFieldValueWithoutTriggering( GLOBAL_COMM_CODE_NAME, loGlobalCommCodeObj ) ;

                        // Set the partitioned comm code, the field passed in the XML
                        loBaseObj.setDottedFieldValueWithoutTriggering( lsAribaName, loValueObj ) ;
                    }
                    // Charlton 2009.10.06 prevent triggers on field change for quantity
                    else if (lsAribaName.indexOf("Quantity") >= 0)
                    {
                        loBaseObj.setDottedFieldValueWithoutTriggering( lsAribaName, loValueObj ) ;
                    }
                    // rlee setEffectiveUser for OOTB trigger SetCEMEOnLineAmountChange; otherwise will throw stack trace for FieldChange:Amount 
                    else if (lsAribaName.indexOf("Requester") >= 0)
                    {
                        loBaseObj.setDottedFieldValue( lsAribaName, loValueObj );
                        if(getHeaderObject() != null)
                        {
                            BaseSession loSession = Base.getSession();
                            ariba.approvable.core.LineItemCollection loReq = (ariba.purchasing.core.Requisition)getHeaderObject();
                            loSession.setEffectiveUser(loReq.getRequester().getBaseId());
                        }
                        else
                        {
                            if ( DEBUG )
                            {
                                Log.customer.debug( "Error: could not setEffectiveUser from: " +
                                lsAribaName + " due to getHeaderObject() is null." ) ;
                            }
                            addToErrorList( "Error: could not setEffectiveUser from: " +
                                lsAribaName + " due to getHeaderObject() is null." ) ;
                        }
                    }
                    else if(lsAribaName.indexOf("ReqLineText3Value") >= 0)
                    {
                    	Log.customer.debug( "Updating value for ReqLineNIGPCommodityCode: ");
                    	Log.customer.debug( "Value for ReqLineText3Value is" +loValueObj.toString());
                    	String lsReqLineNIGPCode = "ReqLineNIGPCommodityCode";
                    	loValueObj = getNIGPCommodityCode(loValueObj);
                    	Log.customer.debug( "Value for ReqLineNIGPCommodityCode is" +loValueObj);
                    	if(loValueObj != null)
                    	{
                    		loBaseObj.setDottedFieldValue( lsReqLineNIGPCode, loValueObj ) ;
                    	}
                    }
                    else if(lsAribaName.indexOf("BuysenseReceivingType") >= 0)
                    {                       
                        ReqLineItem loReqLine = (ReqLineItem) loBaseObj;
                    if(loReqLine.getOldValues() != null && isReceivingMethodNotMatching(loReqLine, loValueObj))
                    {
                        Log.customer.debug("Error: Attempting to change BuysenseReceivingType, does not match previous version. Check if previous version is in receiving status.");
                        
                        sReceivingTypeErrorLines = sReceivingTypeErrorLines + loReqLine.getNumberInCollection() + ",";
                        //addToErrorList("Error: Attempting to change BuysenseReceivingType, does not match previous version. Check if previous version is in receiving status.");
                    }
                    	else
                    	{
                    		loBaseObj.setDottedFieldValue( lsAribaName, loValueObj ) ;
                    	}
                    }
                    else
                    {
                        loBaseObj.setDottedFieldValue( lsAribaName, loValueObj ) ;
                    }
                }
                else
                {
                    if ( DEBUG )
                    {
                        Log.customer.debug( "Error: could not set field: " +
                                       lsAribaName + " with value: " +
                                       lsAribaVal  ) ;
                    }

                    // Instead of throwing an Exception, start accumulating Errors
                    addToErrorList( "Error: could not set field: " +
                                   lsAribaName +
                                   " with value: " + lsAribaVal ) ;
                }
            }
            else
            {
                if ( DEBUG )
                {
                    Log.customer.debug( "Error: could not find a BaseObject " +
                                   "to set the field value." ) ;
                }

                throw new BuysenseXMLImportException(
                                                     "Error: could not find a BaseObject to set the field value." ) ;
            }
        }
        catch ( BuysenseXMLImportException loBuyException )
        {
            throw loBuyException ;
        }
        catch ( Exception loException )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: exception while setting field: " + foMyContext.getAttrValue( ARIBA_FIELD_NAME ) +
                                                 loException.getMessage() ) ;
        }
    }


    private Object getNIGPCommodityCode(Object loValueObj) {
		// TODO Auto-generated method stub
    	
    	Log.customer.debug( "In getNIGPCommodityCode()" ) ;
    	String lsNIGPCode = loValueObj.toString();
    	Log.customer.debug( "In getNIGPCommodityCode(): lsNIGPCode is " +lsNIGPCode) ;
    	String lsAribaType = "ariba.core.BuysenseNIGPCommodityCode";
        AQLQuery   loAQL        = new AQLQuery( lsAribaType ) ;
        AQLOptions loAQLOptions = new AQLOptions( moPartition ) ;
        loAQL.andEqual( "UniqueName", lsNIGPCode ) ;
        loAQL.andEqual( "Active", Boolean.TRUE ) ;

        loAQLOptions.setUserPartition( moPartition ) ;
        BaseObject loBaseObject = FRCommonFacade.getFirstAQLResult( loAQL, loAQLOptions );
        Log.customer.debug( "In getNIGPCommodityCode(): NIGP base code is " +loBaseObject) ;
        
        if(loBaseObject != null)
        {
        	return loBaseObject;
        }
        
        else return null;
	}
     /* This method processes the end of the details section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void finishDetails( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {
            if ( DEBUG )
            {
                Log.customer.debug( "In finishDetails()" ) ;
            }
            BaseObject    loBaseObj       = null ;
            ContextObject loParentContext = getCurrentContext() ;
            if ( ( loBaseObj = (BaseObject)loParentContext.getObject() ) != null )
            {
                List loLineItmsToAdd = (List)foMyContext.getObject() ;
                List loLineItmVector =
                    (List)((LineItemCollection)loBaseObj).getLineItems() ;
                /*
                 * Now set the line item vector into the BaseObject
                 */
                if ( loLineItmsToAdd != null )
                {
                    for ( int i=0; i<loLineItmsToAdd.size(); i++ )
                    {
                        loLineItmVector.add( loLineItmsToAdd.get( i ) );
                    }
                }
                //temp IIB
                /*
                 * Now loop through the line items on header approvable (i.e. actual
                 * object) and compare the lines which have OriginatingSystemLineNumber
                 * (i.e. they were a result of an import, not the UI action, which would
                 * be null) to the ones present in XML; the ones that do not have a match
                 * will be deleted.
                 */
                if ( loLineItmVector != null )
                {
                    for ( int i=0; i<loLineItmVector.size(); i++ )
                    {
                        LineItem tempLineItem = (LineItem)loLineItmVector.get( i ) ;
                        Log.customer.debug( "ACC-TEMP: tempLineItem object: " + tempLineItem ) ;
                        if ( tempLineItem.getFieldValue( ORIGINAL_REQ_LINE_NBR ) != null )
                        {
                            int liOrigSysLineNumber = ( (Integer)( tempLineItem.getFieldValue( ORIGINAL_REQ_LINE_NBR ) ) ).intValue() ;
                            Log.customer.debug( "TEMP: liOrigSysLineNumber object: " + liOrigSysLineNumber ) ;

                            //String lsOrigSysLineNumber = (String)(((LineItem)loLineItmVector.elementAt( i ))
                            //                                      .getFieldValue( ORIGINAL_REQ_LINE_NBR )) ;
                            //temp IIB
                            Log.customer.debug( "TEMP: loLineItmVector size: "
                                           + loLineItmVector.size() + " lines on a Req") ;
                            Log.customer.debug( "TEMP: moLineItmInXMLVector size: "
                                           + moLineItmInXMLVector.size() + " lines on a Req") ;
                            //temp IIB
                            if ( ! findLineBasedOnOrigSysLineNumber( liOrigSysLineNumber, moLineItmInXMLVector ) )
                            {
                                moLineItmToDeleteVector.add( loLineItmVector.get( i ) ) ;
                                Log.customer.debug( "TEMP: moLineItmToDeleteVector size: " + moLineItmToDeleteVector.size() ) ;
                            }
                        }
                    }
                    Log.customer.debug( "TEMP: after the for loop for loLineItmVector..." ) ;
                }

                // re-set the List of XML Line Items
                // jackie 81->822   moLineItmInXMLVector.removeAllElements() ;
                moLineItmInXMLVector.clear() ;

                //temp
                /*
                 * Now delete the line items from the Collection
                 */
                if ( moLineItmToDeleteVector != null )
                {
                    for ( int i=0; i<moLineItmToDeleteVector.size(); i++ )
                    {
                        loLineItmVector.remove( moLineItmToDeleteVector.get( i ) );
                    }
                }
                // re-set the List
                moLineItmToDeleteVector.clear() ;

                //temp IIB
                //temp
                //CSPL-7455; 
                // At the end of processing all the line items, checking for ReceivingType Error Lines collected while processing each line.  
                // if there is any ReceivingType Error Line, then adding the error message along with Line Numbers.
                if(!StringUtil.nullOrEmptyOrBlankString(sReceivingTypeErrorLines))
                {
                    addToErrorList("Error:Attempting to change BuysenseReceivingType on Line(s): " +sReceivingTypeErrorLines.substring(0, sReceivingTypeErrorLines.length() - 1) +  ". Requisition is in Receiving status");
                }
            }
            else
            {
                if ( DEBUG )
                {
                    Log.customer.debug( "Error: could not find a BaseObject " +
                                   "to set the field value." ) ;
                }
                throw new BuysenseXMLImportException(
                                                     "Error: could not find a BaseObject to set the field value." ) ;
            }
        }
        catch ( BuysenseXMLImportException loBuyException )
        {
            throw loBuyException ;
        }
        catch ( Exception loException )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: exception while finishing details: " +
                                                 loException.getMessage() ) ;
        }
    }

    /**
     * This method processes the end of a detail line item.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void finishDetail( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {
            List        loLineItmVector   = null ;
            ContextObject loParentContext   = getCurrentContext() ;
            String        lsAddChangeDelete = foMyContext.getAttrValue( ADD_CHANGE_DELETE ) ;

            if ( ( loLineItmVector = (List)loParentContext.getObject() ) != null )
            {
                Object loObject = foMyContext.getObject() ;

                //temp iib
                // Determine whether the line we have in context is to be added, changed,
                // or deleted, based on the element's attribute
                // Also, default the value to "ADD" if not present
                //
                if ( lsAddChangeDelete == null )
                {
                    lsAddChangeDelete = "ADD" ;
                }

                if ( lsAddChangeDelete != null )
                {
                    // If ADD, do as done before; add to an existing Context List
                    if ( lsAddChangeDelete.compareToIgnoreCase("ADD") == 0 )
                    {
                        Log.customer.debug( "TEMP: Line Item do an ADD..." ) ;
                        //
                        // Now add the line item to the line item vector
                        //
                        loLineItmVector.add( loObject ) ;
                    }

                    // If CHANGE, do nothing, as change already occured on the real LineItem
                    else if ( lsAddChangeDelete.compareToIgnoreCase("CHANGE") == 0 )
                    {
                        Log.customer.debug( "TEMP: Line Item do a CHANGE..." ) ;
                    }

                    // If Delete, accumulate all LineItems to a temp List that will be deleted
                    else if ( lsAddChangeDelete.compareToIgnoreCase("DELETE") == 0 )
                    {
                        Log.customer.debug( "TEMP: Line Item do a DELETE..." ) ;
                        //
                        // Now add the line item to the line item vector
                        //
                        moLineItmToDeleteVector.add( loObject ) ;
                    }

                    // Unexpected value passed in XML; throw exception
                    else
                    {
                        if ( DEBUG )
                        {
                            Log.customer.debug( "Error: could not reslove " +
                                           "ADD_CHANGE_DELETE for the LineItem: " + lsAddChangeDelete ) ;
                        }
                        throw new BuysenseXMLImportException(
                                                             "Error: could not reslove " +
                                                             "ADD_CHANGE_DELETE for the LineItem: " + lsAddChangeDelete ) ;
                    }
                }
                //temp iib

                /*
                 * Save this object
                 */
                if ( loObject instanceof ClusterRoot )
                {
                    ((ClusterRoot)loObject).save() ;
                }

                /*
                 * Save the header object
                 */
                if ( getHeaderObject() != null )
                {
                    getHeaderObject().save() ;
                }

                moDetail = null ;
            }
            else
            {
                if ( DEBUG )
                {
                    Log.customer.debug( "Error: could not find " +
                                   "List to set the LineItem." ) ;
                }
                throw new BuysenseXMLImportException(
                                                     "Error: could not find List to set the LineItem." ) ;
            }
        }
        catch ( BuysenseXMLImportException loBuyException )
        {
            throw loBuyException ;
        }
        catch ( Exception loException )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: exception while finishing detail: " +
                                                 loException.getMessage() ) ;
        }
    }


    /**
     * This method processes the end of the comments section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void finishComments( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {
            if ( DEBUG )
            {
                Log.customer.debug( "In finishComments()" ) ;
            }

            BaseObject    loBaseObj       = null ;
            ContextObject loParentContext = getCurrentContext() ;
            Date          ldNow           = null;

            if ( ( loBaseObj = (BaseObject)loParentContext.getObject() ) != null )
            {
                List loCommentsToAdd = (List)foMyContext.getObject() ;
                Comment loImportComment = null;

                /*
                 * Now set the comment vector into the BaseObject
                 * In order to keep a reference to a proper Parent object,
                 * we had to add to an existing vector of Comments,
                 * hence all the casting logic that follows
                 */
                if ( loCommentsToAdd != null )
                {
                    for ( int i=0; i<loCommentsToAdd.size(); i++ )
                    {
                        // ER#119 Proprietary and Confidential. Add time stamp to Comment Date field during import XML
                        loImportComment = (Comment) loCommentsToAdd.get(i);
                        ldNow = Date.getNow();
                        loImportComment.setDottedFieldValue("Date", ldNow);
                        Thread.sleep(1);
                        if ( getHeaderObject() != null )
                        {
                            ( ( Approvable ) getHeaderObject() ).addComment( ( Comment ) loCommentsToAdd.get( i ) ) ;
                        }
                    }
                }
            }
            else
            {
                if ( DEBUG )
                {
                    Log.customer.debug( "Error: could not find a BaseObject " +
                                   "to set the field value." ) ;
                }

                throw new BuysenseXMLImportException(
                                                     "Error: could not find a BaseObject to set the field value." ) ;
            }
        }
        catch ( BuysenseXMLImportException loBuyException )
        {
            throw loBuyException ;
        }
        catch ( Exception loException )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: exception while finishing comments: " +
                                                 loException.getMessage() ) ;
        }
    }


    /**
     * This method processes the end of a comment.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void finishComment( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {
            List        loCommentVector   = null ;
            ContextObject loParentContext   = getCurrentContext() ;

            if ( ( loCommentVector = (List)loParentContext.getObject() ) != null )
            {
                BaseObject loObject = (BaseObject) foMyContext.getObject() ;
                String lsCommentLevel = foMyContext.getAttrValue( COMMENT_LEVEL ) ;
                LineItem loLineItem = null ;

            if ( lsCommentLevel != null && !lsCommentLevel.equals("0") )
                {
               List loLineItems = (List)(((LineItemCollection)getHeaderObject()).getLineItems());
               boolean lbFoundLine = false;
            for(int i=0; i< loLineItems.size(); i++)
            {
                 loLineItem = (LineItem)loLineItems.get(i);
                 if((loLineItem.getFieldValue("OriginatingSystemLineNumber")).toString().equals(lsCommentLevel))
                 {
                 lbFoundLine = true;
                 break;
              }
                    }
            if (!lbFoundLine)
                    loLineItem = null;
                }
                loObject.setFieldValue( "LineItem", loLineItem ) ;

                /*
                 * Now add the comment to the comment vector
                 */
                loCommentVector.add( loObject ) ;

                /*
                 * Save this object
                 */
                if ( loObject instanceof ClusterRoot )
                {
                    ((ClusterRoot)loObject).save() ;
                }

                /*
                 * Save the header object
                 */
                if ( getHeaderObject() != null )
                {
                    getHeaderObject().save() ;
                }

                moComment = null ;
            }
            else
            {
                if ( DEBUG )
                {
                    Log.customer.debug( "Error: could not find " +
                                   "List to set the Comment." ) ;
                }
                throw new BuysenseXMLImportException(
                                                     "Error: could not find List to set the Comment." ) ;
            }
        }
        catch ( BuysenseXMLImportException loBuyException )
        {
            throw loBuyException ;
        }
        catch ( Exception loException )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: exception while finishing comment: " +
                                                 loException.getMessage() ) ;
        }
    }


    /**
     * This method processes the end of the attachments section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void finishAttachments( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        if ( DEBUG )
        {
            Log.customer.debug( "In finishAttachments()" ) ;
        }
    }


    /**
     * This method processes the end of an attachment.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void finishAttachment( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {

            if ( DEBUG )
            {
                Log.customer.debug( "In finishAttachment()" ) ;
            }

            List        lvAttachmentVector = null ;
            ContextObject loParentContext    = getCurrentContext() ;

            if ( ( lvAttachmentVector = (List)loParentContext.getObject() ) != null )
            {
                BaseObject loObject = (BaseObject) foMyContext.getObject() ;

                String lsExtension = (String) loObject.getFieldValue( "Extension" ) ;

                if ( lsExtension != null )
                {
                    /*
                     * Look up the ContentType on the Map we loaded at start-up
                     */
                    Map lhContentType = ContentTypeContainer.getContentType() ;
                    String lsContentType = (String) lhContentType.get( lsExtension ) ;
                    loObject.setFieldValue( "ContentType", lsContentType ) ;
                }

                /*
                 * Now add the comment to the comment vector
                 */
                lvAttachmentVector.add( loObject ) ;

                /*
                 * Save this object
                 */
                if ( loObject instanceof ClusterRoot )
                {
                    ((ClusterRoot)loObject).save() ;
                }

                /*
                 * Save the header object
                 */
                if ( getHeaderObject() != null )
                {
                    getHeaderObject().save() ;
                }

                moAttachment = null ;
            }
            else
            {
                if ( DEBUG )
                {
                    Log.customer.debug( "Error: could not find " +
                                   "List to set the Attachment." ) ;
                }
                throw new BuysenseXMLImportException(
                                                     "Error: could not find List to set the Attachment." ) ;
            }
        }
        catch ( BuysenseXMLImportException loBuyException )
        {
            throw loBuyException ;
        }
        catch ( Exception loException )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: exception while finishing attachment: " +
                                                 loException.getMessage() ) ;
        }
    }

    /**
     * This method processes the end of an object element section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void finishObject( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {
            BaseObject    loBaseObj       = null ;
            ContextObject loParentContext = getCurrentContext() ;

            if ( ( loBaseObj = (BaseObject)loParentContext.getObject() ) != null )
            {
                Object loValueObj = foMyContext.getObject() ;

                /*
                 * Try to set this field's value object into the current BaseObject
                 */
                if ( loValueObj != null )
                {
                    loBaseObj.setDottedFieldValue(
                                                  foMyContext.getAttrValue( ARIBA_FIELD_NAME ), loValueObj ) ;
                }
            }
            else
            {
                if ( DEBUG )
                {
                    Log.customer.debug( "Error: could not find a BaseObject " +
                                   "to set the object value." ) ;
                }

                throw new BuysenseXMLImportException(
                                                     "Error: could not find a BaseObject to set the object value." ) ;
            }
        }
        catch ( BuysenseXMLImportException loBuyException )
        {
            throw loBuyException ;
        }
        catch ( Exception loException )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: exception while setting object: " +
                                                 loException.getMessage() ) ;
        }
    }


    /**
     * This method processes the end of the subdetails section.
     * Mainly, it sets the DefaultingSetBits field to 1, which in effect
     * persists the Accounting Splits entered on an inported Requisition.
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void finishSubDetails( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        if ( DEBUG )
        {
            Log.customer.debug( "In finishSubDetails()" ) ;
        }

        SplitAccountingCollection loActgs =
            ((ProcureLineItem)moDetail).getAccountings() ;

        loActgs.markDottedAttributeSetWithoutTriggering( "SplitAccountings",
                                                        true ) ;
        bIsSplitTypeSet = false;
    }


    /**
     * This method processes the end of the subdetail line item.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void finishSubDetail( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {
            Object loObject = null ;

            if ( ( loObject = foMyContext.getObject() ) != null )
            {
                /*
                 * Process the post-add split accountings method
                 */
                if ( moDetail != null )
                {
                    // Set LineItem Reference
                    ((SplitAccounting)loObject).setLineItem( (ProcureLineItem)moDetail ) ;

                    SplitAccountingCollection loActgs =
                        ((ProcureLineItem)moDetail).getAccountings() ;

                    Log.customer.debug( "sSplitAccountingType: " + sSplitAccountingType) ;
                    Log.customer.debug( "bIsSplitTypeSet: " + bIsSplitTypeSet) ;
                    if (!StringUtil.nullOrEmptyOrBlankString(sSplitAccountingType) && !bIsSplitTypeSet)
                    {
                        if (sSplitAccountingType.equalsIgnoreCase("Amount"))
                        {
                            SplitAccountingType splitType = (SplitAccountingType) Base.getService().objectMatchingUniqueName(
                                    "ariba.common.core.SplitAccountingType", moPartition, "_Amount");
                            loActgs.setType(splitType);
                            bIsSplitTypeSet = true;
                        }
                        else if (sSplitAccountingType.equalsIgnoreCase("Percentage"))
                        {
                            SplitAccountingType splitType = (SplitAccountingType) Base.getService().objectMatchingUniqueName(
                                    "ariba.common.core.SplitAccountingType", moPartition, "_Percentage");
                            loActgs.setType(splitType);
                            bIsSplitTypeSet = true;
                        }
                        else if (sSplitAccountingType.equalsIgnoreCase("Quantity"))
                        {
                            SplitAccountingType splitType = (SplitAccountingType) Base.getService().objectMatchingUniqueName(
                                    "ariba.common.core.SplitAccountingType", moPartition, "_Quantity");
                            loActgs.setType(splitType);
                            bIsSplitTypeSet = true;
                        }
                    }
                    
                    loActgs.getSplitAccountings().add((SplitAccounting)loObject) ;

                    loActgs.addedSplitAccountingsElement((SplitAccounting)loObject);
                    ((SplitAccounting)loObject).setNumberInCollection(((SplitAccounting)loObject).getNumberInCollection() - 1);
                }

                /*
                 * Save the header object
                 */
                if ( getHeaderObject() != null )
                {
                    getHeaderObject().save() ;
                }
            }
            else
            {
                if ( DEBUG )
                {
                    Log.customer.debug( "Error: could not find sub-detail object." ) ;
                }

                throw new BuysenseXMLImportException(
                                                     "Error: could not find sub-detail object." ) ;
            }
        }
        catch ( BuysenseXMLImportException loBuyException )
        {
            throw loBuyException ;
        }
        catch ( Exception loException )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: exception while finishing sub-detail: " +
                                                 loException.getMessage() ) ;
        }
    }

    /**
     * This method peroforms a submit on the given approvable
     *
     * @param foApprovable the Approvable to submit
     * @param fsForceImport the value of force_import attribute
     * @exception BuysenseXMLImportException
     * rlee 12/2004 added fsAutoSbmt
     */
    private void autoSubmit(Approvable foApprovable,
                            String fsForceImport,
                            String fsChangeReq) throws BuysenseXMLImportException
    {
       Log.customer.debug("BuysenseXMLImport: In autoSubmit()...");

       BaseSession loSession    = Base.getSession();
       List      loResults    = null;
       int         liReturnCode = 0;
       int         liAccess    = 0;
       String lsMessage = null;
       java.util.Map invalidFields = null;
       int liErrorList = 0;

       //8.1 change to SharedUser
       loSession.setEffectiveUser(foApprovable.getRequester().getBaseId());

       if(loSession.getPartition() == null)
          loSession.setPartition(foApprovable.getPartition());

       foApprovable.save();

       // Ariba 8.1  rgiesen dev SPL #28 -
       // New code for 8.1 using FieldRelease FRCommonFacade, replacing VCEUtil
       // Ariba 8.1 - Anup DEV SPLs 59, 97 ST SPLs 173, 175 -
       // Modified the logic to manifest the original functionality.

       try
       {
          //Validate approvable
          loResults = foApprovable.checkSubmit();
          if(loResults == null || !(ListUtil.firstElement(loResults) instanceof Integer))
          {
             throw new ApprovableHookException("Error: Approvable: " + foApprovable.getUniqueName() +
                                               " failed validation.");
          }

          liReturnCode = ((Integer)ListUtil.firstElement(loResults)).intValue();

          if (liReturnCode < 0)
          {
             //Errors, something is wrong, throw exception
             throw new ApprovableHookException((String)loResults.get(1));
          }

          //Check to see if there are any invalid fields on the approvable
          invalidFields = (java.util.Map)FRCommonFacade.getBaseObjectInvalidFields(foApprovable,
                                                                                               null,
                                                                                               foApprovable);

          if (!MapUtil.nullOrEmptyMap(invalidFields))
          {
             throw new ApprovableHookException("Error: Approvable: " + foApprovable.getUniqueName() +
                                               " has invalid fields:" + invalidFields.toString());
          }

          //Check to see if you have the right access to change the file
          liAccess = foApprovable.getAccess(15, foApprovable.getRequester());
          if(liAccess != Access.Now)
          {
             //You do not have access, throw exception
             lsMessage = Access.getMessage(liAccess, foApprovable.getPreparer().getLocale());
             if (liAccess == Access.CantSubmitNotChanged)
             {
                //Remove apostroph in can't
                lsMessage = StringUtil.replaceCharByString(lsMessage,'\'', " no");
             }
             throw new ApprovableHookException("Error: Approvable: " + foApprovable.getUniqueName() +
                                               ": " + lsMessage);
          }

          if(! mvErrorList.isEmpty())
          {
             liErrorList = (new Integer((String) mvErrorList.get(0))).intValue();
          }
          // Dev #945; No need to submit or change approvable if parsing error has severity 3
          if ((liReturnCode > 0) && liErrorList != 3)
          {
             // Warning/s
             if(DEBUG)
                Log.util.debug("Warning: warning while validating approvable;" +
                                         (String)loResults.get(1));

                approvableSubmitOrChange(foApprovable, fsChangeReq);

                // autosubmitted successfully - so update original requisition accordingly
                updateOrigReqHistory(foApprovable,
                                     "ReqImportSuccess",
                                     "New Request " + foApprovable.getUniqueName() +
                                        " auto-submitted successfully.");

                loSession.setEffectiveUser(null);
          }
          else if(! mvErrorList.isEmpty())
          {
             //ST SPL # 58 - Get out before submit, if severity = 3
             if((new Integer((String) mvErrorList.get(0))).intValue() == 3)
             {
                if(DEBUG)
                   Log.customer.debug("Error: Delete severity approvable - no auto-submit.");

                throw new BuysenseXMLImportException("Error: No submit for delete severity error approvable.");
             }
          }
          else if(liReturnCode == 0)
          {
             Log.util.debug("Approvable validation successful: do submit or change.");
             approvableSubmitOrChange(foApprovable, fsChangeReq);
             // autosubmitted successfully - so update original requisition accordingly
             updateOrigReqHistory(foApprovable,
                                  "ReqImportSuccess",
                                  "New Request " + foApprovable.getUniqueName() +
                                      " auto-submitted successfully.");
             loSession.setEffectiveUser(null);
          }
          if(DEBUG)
             Log.util.debug("Done with auto-submitting.");
       }
       catch(ApprovableHookException foHookException)
       {
          // Validation errors
          if(DEBUG)
             Log.util.debug("Error: could not validate approvable:" + foHookException.getMessage()) ;

          // Based on the value of force_import decide whether to retain or delete an approvable
          if(((Boolean.valueOf(fsForceImport)).booleanValue()))
          {
             if(DEBUG)
                Log.util.debug("Despite auto-submit returning errors, " +
                                     "retain Approvable in Composing status...");

             // update history of the original requistion: since there were errors and
             // force import flag is set to true, the request will be in composing
             updateOrigReqHistory(foApprovable,
                                  "ReqImportFailure",
                                    "Errors on auto-submitting new Request: " + foApprovable.getUniqueName() +
                                    ". The Request will be in \"Composing\" status.");

             // Instead of throwing an Exception, accumulate Errors
             // Severity 2 - Error acumulated, Approvable NOT marked for deletion
             addToErrorList("Error: Approvable validation failed: " + foHookException.getMessage(), 2);
             Log.util.debug("autoSubmit() - Added the Error of Severity of: 2");
          }
          else
          {
             if(DEBUG)
                Log.util.debug("autoSubmit() - Approvable will be deleted...");

             // update history of the original requistion: since there were errors and
             // force import flag is set to false, the request will be deleted
             updateOrigReqHistory(foApprovable,
                                  "ReqImportFailure",
                                  "Errors on auto-submitting new Request: " + foApprovable.getUniqueName() +
                                  ". The Request will be deleted.");

             throw new BuysenseXMLImportException("Error: Approvable validation failed: " +
                                                 foHookException.getMessage());
          }

       }
       catch(BuysenseXMLImportException loXMLEx)
       {
          throw loXMLEx;
       }
       catch(Exception loEx)
       {
          // should never be here - indicates Ariba infrastructure unhandled exception
          throw new BuysenseXMLImportException("Error: Unexpected error: " + loEx.getMessage());
       }
    }



    private void approvableSubmitOrChange(Approvable foApprovable, String fsChangeReq)
                      throws BuysenseXMLImportException
    {
       int liErrCode = 0;
       String lsMessage = null;
       try
       {
          // Call Change Req API if the flag for change req is set
          if (((Boolean.valueOf(fsChangeReq)).booleanValue()))
          {
             Log.util.debug("About to call submitChange()...");
             liErrCode = foApprovable.submitChange(null);
          }
          else
          {
             Log.util.debug("About to call approvableSubmit()...");
           //jackie - need to ask Ariba consultant or in house ariba experts
             FRCommonFacade.approvableSubmit(foApprovable, null);
          }

          if(liErrCode != Access.Now)
          {
             Log.util.debug("submit/change approvable returned with error code: " + liErrCode);

             // autosubmitted with errors - so update original requisition accordingly
             updateOrigReqHistory(foApprovable,
                                  "ReqImportFailure",
                                  "Errors on auto-submitting new Request: " + foApprovable.getUniqueName() + ".");

             lsMessage = (String)Access.getMessage(liErrCode, foApprovable.getPreparer().getLocale());
             if (liErrCode == Access.CantSubmitNotChanged)
             {
                //Remove apostroph in can't
                lsMessage = StringUtil.replaceCharByString(lsMessage, '\'', " no");
             }

             throw new BuysenseXMLImportException("Error: could not submit/change approvable: " +
                                                  foApprovable.getUniqueName() +
                                                  "; " + lsMessage );
          }
       }
       catch(BuysenseXMLImportException loXMLImportException)
       {
          throw loXMLImportException;
       }
       catch(Exception loException)
       {
          throw new BuysenseXMLImportException("Error: Failed to submit/change approvable : " +
                                               foApprovable.getUniqueName() +
                                               "; " + loException.getMessage());
       }
       return;
    }

    /**
     * This method peroforms a cancel on the given requisition
     *
     * @param foReq the Requisition to cancel
     * @exception BuysenseXMLImportException
     */
    private void cancelReq( Requisition foReq )
        throws BuysenseXMLImportException
    {
        BaseSession loSession      = Base.getSession() ;
        List      loResults      = null ;
        int         liReturnCode   = 0 ;
        int         liErrCode      = 0 ;
        String      lsStatusString = null ;

        msReqCancelName = "" ;

        lsStatusString = (String)foReq.getDottedFieldValue( "StatusString" ) ;

        if ( DEBUG )
        {
            Log.customer.debug( "Status of requisition to be cancelled = " +
                           lsStatusString ) ;
        }

        if ( lsStatusString == null )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: could not cancel Requisition. " +
                                                 "Unknown Requisition status." ) ;
        }
        else if (! lsStatusString.equalsIgnoreCase("Ordered") )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: could not cancel Requisition. " +
                                                 "Requisition status is not \"Ordered\"." ) ;
        }

      //8.1 get SharedUser
        loSession.setEffectiveUser(foReq.getRequester().getBaseId()) ;
        msReqCancelName = (String)foReq.getDottedFieldValue( "UniqueName" ) ;

      // Ariba 8.1  rgiesen - NEW CODE
      /* Similar to the Change req, the Cancel req could not be called using the
      Field Release.  Instead, the code from the VCEUtil was "copied" here and then
      the req was cancelled using approvable.cancel()
      */

      try {
         //Set a comment for the cancelled req
         Comment loComment = new Comment(foReq.getPartition());
         loComment.setTitle("Cancel Requisition");
         loComment.setBody("This request has been cancelled through ERP integration.");
         loComment.setType(Comment.TypeCancel);
            loComment.setDate(new ariba.util.core.Date());
         Log.customer.debug( "BuysenseXMLImport: Cancel Req" ) ;

         String logLocation = "cancelReq(): ";

         //Verify that the approvable is valid
         Assert.that(foReq.getPreparer() != null && foReq.getRequester() != null, "%s Missing required approvable preparer or requester", logLocation);

         //Make sure that you have proper access to the req to be cancelled
         ApprovableClassProperties cp = (ApprovableClassProperties)foReq.getClassProperties();
         List results = foReq.runHook(cp.getCancelHook());

         int resultCode = 0;

         if(results == null || results.size() < 2 || !(ListUtil.firstElement(results) instanceof Integer))
            resultCode = ((Integer)ListUtil.firstElement(results)).intValue();
         if(resultCode < 0)
            throw new ApprovableHookException((String)results.get(1));
         if(resultCode > 0)
            throw new ApprovableActionException(resultCode, (String)results.get(1));

         //Cancel the req
         foReq.cancel(loComment);

            Log.customer.debug( "BuysenseXMLImport: After Cancel Req" ) ;
        }
        catch ( ApprovableHookException hookException )
        {
            if ( DEBUG )
            {
                Log.customer.debug( "Error: could not cancel Requisition;" +hookException.toString()) ;
            }
            throw new BuysenseXMLImportException(
                                                 "Error: could not cancel Requisition:" +hookException.toString());
        }
        catch ( ApprovableActionException actionException )
        {
               Log.customer.debug( "Error: could not cancel Requisition: " ) ;
               Log.customer.debug( actionException.toString()) ;
               throw new BuysenseXMLImportException("Error: could not cancel Requisition: " +
                                                    actionException.toString() );
 
        }
        if ( DEBUG )
        {
            Log.customer.debug( "Done with Requisition cancellation." ) ;
        }
    }


    private void updateApprovableHistory( BaseObject foApprovable,
                                         String fsRecordType,
                                         String fsHistoryComment )
        throws BuysenseXMLImportException
    {
        try
        {
            String     lsApprovableName = null ;
            BaseObject loHistory        = null ;

            lsApprovableName = (String)foApprovable.getDottedFieldValue("UniqueName");

            loHistory = (BaseObject)BaseObject.create(
                                                      "ariba.procure.core.SimpleProcureRecord",
                                                      moPartition ) ;

            loHistory.setFieldValue( "Approvable", foApprovable ) ;
            loHistory.setFieldValue( "ApprovableUniqueName",
                                    lsApprovableName ) ;
            loHistory.setFieldValue( "Date", new ariba.util.core.Date() ) ;
            //rgiesen User:  8.1 Change to return SharedUser with HardCoded Partition
            Partition partition = Base.getService().getPartition("supplierdirect");
            String passwordadapter = Base.getService().getParameter(partition,"Application.Authentication.PasswordAdapter");
            //String passwordadapter = "CryptPasswordAdapter";
            Log.customer.debug( "BuysenseXMLImport: Password Adapter used: " + passwordadapter) ;
            loHistory.setFieldValue( "User", ariba.user.core.User.getUser("aribasystem",passwordadapter) );
            loHistory.setFieldValue( "RecordType", fsRecordType ) ;

            // Set the status message
            ((List)loHistory.getFieldValue( "Details" ) ).add( fsHistoryComment ) ;

            // Now add it to the Approvable
            ((List)foApprovable.getFieldValue( "Records" ) ).add( loHistory ) ;
        }
        catch ( Exception loExcep )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: could not update history on Approvable: " +
                                                 loExcep.getMessage() );
        }
    }

    protected Approvable findApprovable( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {
            ContextObject loMyContext     = foMyContext ;
            BaseObject    loBaseObject    = null ;
            ClusterRoot   loNewObj        = null ;
            String        lsAribaVal      = foMyContext.getText() ;
            String        lsAribaType     = BuyintXWalk.XWalk.changeOldToNew( foMyContext.getAttrValue(ARIBA_TYPE) ) ;
            String        lsValPrefix     = foMyContext.getAttrValue( ARIBA_VAL_PREFIX ) ;
            String        lsReqUniqueName = foMyContext.getAttrValue( REQ_UNIQUE_NAME );
           String        lsChangeReq     = foMyContext.getAttrValue( CHANGE_REQ ) ;
            AQLQuery      loAQL           = new AQLQuery( lsAribaType ) ;
            AQLOptions    loAQLOptions    = new AQLOptions( moPartition ) ;

            if ( ( lsValPrefix != null ) && ( lsValPrefix.length() > 0 ) )
            {
                /*
                 * Prepend the prefix onto value before going further
                 */
                lsAribaVal = lsValPrefix + lsReqUniqueName ;
            }

            Log.customer.debug( "Looking to change: " + lsAribaVal + " approvable..." ) ;

            loAQL.andEqual( "InitialUniqueName", lsAribaVal ) ;
            loAQL.addOrderByElement( "VersionNumber", false ) ;
            loAQL.andEqual( "Active", Boolean.TRUE ) ;

            loAQLOptions.setUserPartition( moPartition ) ;
            loBaseObject = FRCommonFacade.getFirstAQLResult( loAQL, loAQLOptions );

            if ( loBaseObject != null && loBaseObject instanceof Approvable )
           {
            if ( ( ( Boolean.valueOf( lsChangeReq ) ).booleanValue() ) )
            {
                return ( (Approvable)loBaseObject ) ;
            }
            else
            {
                if ( DEBUG )
                {
                    Log.customer.debug( "Header object with value " + lsAribaVal + " was not found." ) ;
                }
                throw new BuysenseXMLImportException(
                 "ERROR: Can not create Approvable with Unique Name : " + lsAribaVal + " as it already exists." ) ;
            }
        }
           else
           {
            if ( ( ( Boolean.valueOf( lsChangeReq ) ).booleanValue() ) )
               {
                   if ( DEBUG )
                   {
                       Log.util.debug( "Header object with value " + lsAribaVal + " was not found." ) ;
                   }
                   throw new BuysenseXMLImportException(
                     "ERROR: Approvable with Unique Name " + lsAribaVal + " was not found." ) ;
            }
            else
            {
               return null;
               }
           }
       }
        catch ( Exception loExcep )
        {
           throw new BuysenseXMLImportException( loExcep.getMessage() );
        }
    }

    private boolean findLineBasedOnOrigSysLineNumber( int    fiOrigSysLineNumber,

                                                     List fvLineItemCollection )
        throws BuysenseXMLImportException
    {
        int      liOrigSysLineNumber  = fiOrigSysLineNumber ;
        List   lvLineItemCollection = fvLineItemCollection ;
        LineItem loLineItem           = null ;

        if ( DEBUG )
        {
            Log.customer.debug( "Looking for original system line number: "
                           + liOrigSysLineNumber ) ;
        }
        try
        {
            for ( int i=0; i<lvLineItemCollection.size(); i++ )
            {
                loLineItem = (LineItem)lvLineItemCollection.get( i ) ;

                if ( ( (Integer)loLineItem.getFieldValue(ORIGINAL_REQ_LINE_NBR) ).intValue()
                        == liOrigSysLineNumber )
                {
                    Log.customer.debug( "Found match for original system line number: " + liOrigSysLineNumber ) ;
                    Log.customer.debug( "Temp - with base id: " +  loLineItem ) ;
                    return true ;
                }
            }
        }

        catch ( NumberFormatException loNumberFormatException )
        {
            Log.customer.debug( "Could not resolve : " + liOrigSysLineNumber + "as int." ) ;
            return false ;
        }
        return false ;
    }

    private Approvable changeReq( ClusterRoot foExistingObj )
        throws BuysenseXMLImportException
    {

       /*
        * // VEPI PROD #729: Incorrect messages on change order for Reqs in Received/Receiving.
        *                    Also changing multiple-change order request processing.
        *
        * New approach for multiple change order processing is as described below:
        *
        * Assuming 15 minutes (900000 milliseconds) interval between runs of BuysenseCreateApprovableTask,
        * there should be at least a 15 minute grace period for the original version (or previous version) to get
        * to the correct status so that a change action may be issued against the Approvable.
        * Such transactions could be in Approved, Submitted, or Ordering state.
        *
        * Since we don't want to hold up other transactions in the current run of the task, change order requests
        * that land up within the grace period must be skipped, and retried in the next run of the scheduled task.
        * However, an error should not be thrown. Instead, the transaction must be placed in retry mode without
        * the number of (unsuccessful) attempts being incremented.
        *
        * Hence, the new logic goes:
        *    if the reference Approvable has just been modified within last 15 minutes
        *    AND BypassSolicitation = true
        *    AND the Approvable is in one of the following transitional statuses (Approved, Submitted, Ordering)
        *    then
        *       throw an "Info:" (instead of an "Error:") Exception.
        *    endif
        * Subsequently, such an exception will be handled by the Reader.
        *
        * Note that if one change request against a req is skipped because of falling within the grace period, all
        * subsequent requests should be similarly skipped.
        */

       Approvable loExistingReq  = (Approvable)foExistingObj ;
       boolean lbBypassSolicit = false;

       String lsStatusString = (String)loExistingReq.getDottedFieldValue( "StatusString" ) ;
       Log.customer.debug("Set up to process multiple change orders. Status of order = " + lsStatusString);

       boolean lboolValidStatus = false;

       if ( lsStatusString == null )
       {
          throw new BuysenseXMLImportException( "Error: could not change Requisition. Unknown Requisition status." ) ;
       }
       else
       {
          if (!isSkippedReq(loExistingReq))
          {
             if (lsStatusString.equalsIgnoreCase("Ordered") ||
                 lsStatusString.equalsIgnoreCase("Receiving"))
             {
                lboolValidStatus = true;
             }
             else
             {
                // check to see if the order could potentially be getting to Ordered state
                if ( lsStatusString.equalsIgnoreCase("Approved") ||
                     lsStatusString.equalsIgnoreCase("Submitted") ||
                     lsStatusString.equalsIgnoreCase("Ordering") )
                {
                   lbBypassSolicit = ((Boolean)loExistingReq.getDottedFieldValue("ReqHeadCB1Value")).booleanValue();
                   long llRightNow = Date.getNow().getTime();
                   long llLastMod = ((ariba.util.core.Date)loExistingReq.getDottedFieldValue("LastModified")).getTime() ;
                   if ( lbBypassSolicit && (llRightNow - llLastMod) < MIN_GRACE_PERIOD )
                   {
                      skipReq(loExistingReq);
                   }
                }
             }
          }
       }

       if (isSkippedReq(loExistingReq))
       {
          throw new BuysenseXMLImportException("Info: Skipping processing of " + loExistingReq.toString() +
                    " due to pending prior change requests. Will process in later run of Scheduled Task.");
       }

       if (!lboolValidStatus)
       {
          throw new BuysenseXMLImportException("Error: could not change Requisition. " +
                                                  "Requisition status is not \"Ordered\" or \"Receiving\".") ;
       }

        // end from Multiple Change Order SPL

        //Ariba 8.1  call approvable.change() for new version of the Approvable
        //Check to make sure you have proper access to the req
        String logLocation = "BuysenseXMLImport.changeReq(): ";
        Assert.that(loExistingReq != null, "%s Missing required approvable parameter", logLocation);
        Assert.that(loExistingReq.getPreparer() != null && loExistingReq.getRequester() != null, "%s Missing required approvable preparer or requester", logLocation);

        //Change the req
        //START: Added by srini for CSPL-3870 
        Log.customer.debug("BuysenseXMLImport::Starting to change the Req ");
        if (ariba.user.core.User.getEffectiveUser() == null)
        {
           Log.customer.debug("BuysenseXMLImport::Effective user is null and setting from existing REQ temporarily");
           BaseSession loSession = Base.getSession();
		   loSession.setEffectiveUser(loExistingReq.getRequester().getBaseId());
        }
        else
        {
        	Log.customer.debug("BuysenseXMLImport::Found effective user " +ariba.user.core.User.getEffectiveUser().getUniqueName());
        }
        //END: Added by srini for CSPL-3870
        int result = loExistingReq.change();
        Log.customer.debug("BuysenseXMLImport::Successfully changed the REQ "+result);
        if(result != Access.Now)
        {
           throw new BuysenseXMLImportException( "Error: could not change Requisition. " +
                                                    "You can't edit an old version of a request.") ;
        }
        else
        {
           Log.customer.debug("BuysenseXMLImport::Returning the change version");
           return (Approvable)loExistingReq.getNextVersion();
        }
    }

    /**
    * This method records the id of the Approvable whose processing is being skipped.
    * @param foCurrApprovable the current requisition approvable
    */
    private void skipReq( Approvable foCurrApprovable )
    {
        moSkipHash.put(foCurrApprovable.getUniqueName(), "skip");
    }

    /**
    * This method compares the passed ID with the saved skipped rec IDs to know if the current req is being skipped.
    * @param foCurrApprovable the current requisition approvable
    */
    private boolean isSkippedReq( Approvable foCurrApprovable )
    {
        if (moSkipHash.containsKey(foCurrApprovable.getUniqueName()))
        {
           return true;
        }
        return false;
    }

    /**
    * This method update the original requistion with a history comment. Should be invoked
    * only for e2e type of requisition imports that do have an original requisition
    * @param foCurrApprovable the current requisition approvable
    * @param fsRecordType the type of history record
    * @param fsHistoryComment the history comment string
    */
    private void updateOrigReqHistory( BaseObject foCurrApprovable,
                                      String fsRecordType,
                                      String fsHistoryComment)
    {
        try
        {
            // get the original req unique name from the curr approvable
            String lsOrigReqUniqueName = (String)foCurrApprovable.getDottedFieldValue( ORIGINAL_REQ_NAME_FIELD );

            // Only if this field is present should an attempt be made to write history
            if( lsOrigReqUniqueName == null)
            {
                return;
            }

            if ( DEBUG )
            {
                Log.customer.debug( "Updating history of original Requisition: " + lsOrigReqUniqueName ) ;
            }

            //  get the requisition object
            // if this is a performance hit, we can optimize by saving this as an instance variable
            Requisition loOrigReq = (ariba.purchasing.core.Requisition)
            Base.getService().objectMatchingUniqueName( "ariba.purchasing.core.Requisition",
                                                       moPartition,
                                                       lsOrigReqUniqueName);
            if( loOrigReq != null )
            {
                updateApprovableHistory( loOrigReq,
                                        fsRecordType, fsHistoryComment);
            }
            else
            {
                // ?? SVM Log warning ()
            }
        }
        catch( Exception loEx )
        {
            // ?? SVM Log warning ()
        }
    }

    /**
    * The following method is intended for temporarely fixing the default ProcureLineItem
    * triggering issue, where ProculreLineItem is not being defaulted properly by Ariba
    * trigger
    *
    * @param foProcureLineItem current LineItem which needs to be defaulted
    * @exception BuysenseXMLImportException
    */
    private void defaultProcureLineItems( ProcureLineItem foProcureLineItem )
        throws BuysenseXMLImportException
    {
        try
        {
            List          lvDefaultLineItems = ListUtil.list() ;
            ProcureLineItem loDefaultLineItem = null ;

            if ( DEBUG )
            {
                Log.customer.debug( "In defaultProcureLineItems." ) ;
            }

            // Get DefaultLineItems - this returns a List object,
            // so take the first element only
            if ( getHeaderObject() != null )
            {
                lvDefaultLineItems = ( List )( ( Approvable ) getHeaderObject() ).getFieldValue("DefaultLineItems") ;
                loDefaultLineItem = ( ProcureLineItem ) lvDefaultLineItems.get(0) ;

                // Default LineItem fields first
                int liLineFieldsSize = lvLineFields.size();
                for ( int i = 0; i < liLineFieldsSize; i++ )
                {
                    if ( DEBUG )
                    {
                        Log.customer.debug( "LineItem field: " + lvLineFields.get(i) ) ;
                    }
                    foProcureLineItem.setDottedFieldValue( (String)lvLineFields.get(i),
                    loDefaultLineItem.getDottedFieldValue( (String)lvLineFields.get(i) ) ) ;
                }

                // Default Accounting fields next
                int liActgFieldsSize = lvActgFields.size();
                for ( int i = 0; i < liActgFieldsSize; i++ )
                {
                    if ( DEBUG )
                    {
                        Log.customer.debug( "Accounting field: " + lvActgFields.get(i) ) ;
                    }
                    foProcureLineItem.setDottedFieldValue( "Accountings.SplitAccountings[0]." +
                           (String)lvActgFields.get(i),
                           loDefaultLineItem.getDottedFieldValue( "Accountings.SplitAccountings[0]." +
                           (String)lvActgFields.get(i) ) ) ;
                }

                if ( DEBUG )
                {
                    Log.customer.debug( "Finished defaulting ProcureLineItems fields" ) ;
                }
            }

        }
        catch ( Exception loExcep )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: could not default ProcureLineItem: " +
                                                 loExcep.getMessage() );
        }
    }

    /**
    * The following method allows non-fatal Errors to be added to a List
    * which accumulates them.  It assumes Severity 3 (highest severity - delete Approvable)
    *
    * @param fsErrorMsg Non-fatal Error message to be accumulated
    * @exception BuysenseXMLImportException
    */
    public void addToErrorList( String fsErrorMsg )
        throws BuysenseXMLImportException
    {
        addToErrorList( fsErrorMsg, 3 ) ;
    }

    /**
       * The following method allows non-fatal Errors to be added to a List
       * which accumulates them.
       *
       * @param fsErrorMsg Non-fatal Error message to be accumulated
       * @param fiSeverity Severity of the Error message being passed
       * Severity 1 - Warning - for future use
       * Severity 2 - Error accumulated, Approvable NOT marked for deleted
       * Severity 3 - Error accumulated, Approvable marked for deletion
       * @exception BuysenseXMLImportException
       */
    public void addToErrorList( String fsErrorMsg, int fiSeverity )
        throws BuysenseXMLImportException
    {
        Log.customer.debug( "IIB - addToErrorList() called: " + "ErrorMsg: " + fsErrorMsg + " Severity: " + fiSeverity ) ;
        try
        {
            String  lsErrorMsg = fsErrorMsg ;
            Integer liSeverity = new Integer( fiSeverity ) ;

            if ( lsErrorMsg != null )
            {
                // If this is a first Error add it's Severity as a 0-th
                // element of the List
                if ( mvErrorList.isEmpty() )
                {
                    mvErrorList.add( liSeverity.toString() ) ;
                    Log.customer.debug( "IIB - Added the Severity of: " + liSeverity.intValue() ) ;
                }

                // If not, compare its Severity with the one already in
                // the List; save only the highest Severity at 0-th index
                else
                {
                    if ( liSeverity.compareTo( new Integer( ( String )mvErrorList.get( 0 ) ) ) > 0 )
                    {
                	// jackie 81->822  mvErrorList.setElementAt( liSeverity.toString(), 0 ) ;
                        mvErrorList.set(0,liSeverity.toString()) ;
                        Log.customer.debug( "IIB - Severity greater than the one in List: " + liSeverity.intValue() ) ;
                    }
                }
                // Finally, add the actual Error message
                mvErrorList.add( lsErrorMsg ) ;
            }
        }
        catch ( Exception loExcep )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: could not add to ErrorList: " +
                                                 loExcep.getMessage() );
        }
    }

    /**
    * The following method retrieves the List of accumulated Errors
    *
    * @exception BuysenseXMLImportException
    */
    public List getErrorList()
        throws BuysenseXMLImportException
    {
        try
        {
            return mvErrorList ;
        }
        catch ( Exception loExcep )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: could not get the ErrorList: " +
                                                 loExcep.getMessage() );
        }
    }

    /**
     * The ContextObject class represents an element and its data
     * that has been parsed from the XML file. This ContextObject
     * is then added to the context stack to provide a context for
     * the processing.
     */
    protected class ContextObject
    {
        /**
         * Name of the XML element tag.
         */
        private String msName = null ;

        /**
         * Element text data.
         */
        private StringBuffer msbText = new StringBuffer( 64 ) ;

        /**
         * The element attributes.
         */
        private Map moAttributes = new HashMap( 10, (float).75 ) ;

        /**
         * The stored Object.
         */
        private Object moObject = null ;


        /**
         * ContextObject constructor
         */
        public ContextObject( String fsName,
                             Attributes foAttrList )
        {
            msName = fsName ;
            initAttrHashtable( foAttrList ) ;
        }

        /**
         * Returns the name of the element
         */
        public String getName()
        {
            return msName ;
        }

        /**
         * Sets the name of the element
         */
        public void setName( String fsName )
        {
            msName = fsName ;
        }

        /**
         * Returns the element text
         */
        public String getText()
        {
            return msbText.toString() ;
        }

        /**
         * Adds to the element text
         */
        public void appendText( String fsTextValue )
        {
            msbText.append( fsTextValue ) ;
        }

        /**
         * Returns the object assoiciated with this element
         */
        public Object getObject()
        {
            return moObject ;
        }

        /**
         * Sets the object assoiciated with this element
         */
        public void setObject( Object foObject )
        {
            moObject = foObject ;
        }

        /**
         * Gets the value of a specified attribute of the element
         */
        public String getAttrValue( String fsAttrName )
        {
            return (String)moAttributes.get( fsAttrName ) ;
        }

        /**
         * Sets the value of a specified attribute of the element
         */
        public void setAttrValue( String fsAttrName, String fsAttrVal )
        {
            if ( fsAttrName != null && fsAttrVal != null )
            {
                moAttributes.put( fsAttrName, fsAttrVal ) ;
            }
        }

        /**
         * Stores the element's attribute names and values so they
         * can be used later on in processing.
         */
        private void initAttrHashtable( Attributes foAttrList )
        {
            if ( foAttrList != null )
            {
                for ( int i = 0; i < foAttrList.getLength(); i++ )
                {
                    // Not sure which one should be used : getQName or getLocalName or something else
                    //rgiesen - added check for empty string, same as other place
                    String lsAttrName;
                    if ("".equals(foAttrList.getURI(i)))
                    {
                        lsAttrName=foAttrList.getQName( i ) ;;
                    }
                    else
                    {
                        lsAttrName = foAttrList.getLocalName( i ) ;
                    }
                    String lsAttrVal  = foAttrList.getValue( i ) ;
                    moAttributes.put( lsAttrName, lsAttrVal ) ;
                }
            }
        }
    }

    public String cleanErrMsg(String inString)
    {
        if ( StringUtil.nullOrEmptyOrBlankString(inString) )
            return inString;

        String find = "\n";
        int i = inString.indexOf(find);

        if (i == -1)
            return inString;
        else
        {
            String returnString = "";
            int n = inString.indexOf(find, i+1);
            while (n > 0)
            {
                if ( i+1 == n && ! ((inString.substring(n)).trim().equals("")) )
                    returnString += " ---> ";
                else
                    returnString += inString.substring(i+1, n);
                i = n;
                n = inString.indexOf(find, i+1);
            }
            return returnString;
        }
    }

    /**
           * The following method is intended to default ActgLineItem
           *
           * @param foActgLine current ActgLineItem which needs to be defaulted
           * @exception BuysenseXMLImportException
           */
    private void defaultActgLine( SplitAccounting foActgLine )
        throws BuysenseXMLImportException
    {
        try
        {
            List          lvDefaultLineItems = ListUtil.list();
            ProcureLineItem loDefaultLineItem = null ;

            if ( DEBUG )
            {
                Log.customer.debug( "In defaultActgLine." ) ;
            }

            // Get DefaultLineItems vector and take the first element
            if ( getHeaderObject() != null )
            {
                lvDefaultLineItems = ( List )( ( Approvable ) getHeaderObject() ).getFieldValue("DefaultLineItems") ;
                loDefaultLineItem = ( ProcureLineItem ) lvDefaultLineItems.get(0) ;

                // Default Accounting fields next
                int liActgFieldsSize = lvActgFields.size();
                for ( int i = 0; i < liActgFieldsSize; i++ )
                {
                    if ( DEBUG )
                    {
                        Log.customer.debug( "AKH- Accounting field: " + lvActgFields.get(i) ) ;
                    }

                    foActgLine.setDottedFieldValue( (String)lvActgFields.get(i),
                        loDefaultLineItem.getDottedFieldValue( "Accountings.SplitAccountings[0]." +
                        (String)lvActgFields.get(i) ) ) ;

                }
                if ( DEBUG )
                {
                    Log.customer.debug( "Finished defaulting ActgLineItem fields" ) ;
                }
            }
        }
        catch ( Exception loExcep )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: could not default ActgLineItem: " +
                                                 loExcep.getMessage() );
        }
    }
    
    /*
     * Added by Srini for CSPL-7067
     */
    public boolean isReceivingMethodNotMatching(ReqLineItem loReqLineItem, Object incomingXMLValue)
    {
    	int liBuysenseRecevingType = ((Integer) loReqLineItem.getFieldValue("BuysenseReceivingType")).intValue();
    	if(liBuysenseRecevingType != ((Integer) incomingXMLValue).intValue() && ((loReqLineItem.getAmountAccepted() != null && loReqLineItem.getAmountAccepted().getAmount().intValue() != 0) 
        			|| (loReqLineItem.getAmountRejected() != null && loReqLineItem.getAmountRejected().getAmount().intValue() != 0)
        			|| (loReqLineItem.getNumberAccepted() != null && loReqLineItem.getNumberAccepted().intValue() != 0)
        			|| (loReqLineItem.getNumberRejected() != null && loReqLineItem.getNumberRejected().intValue() != 0)))
    	{
    		Log.customer.debug("BuysenseXMLImport: isReceivingMethodNotMatching: returning true");
    		return true;
    	}
    	Log.customer.debug("BuysenseXMLImport: isReceivingMethodNotMatching: returning false: BRT %s, BRT-XML %s", liBuysenseRecevingType, incomingXMLValue);
    	return false;
    }
}
