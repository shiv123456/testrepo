package config.java.ams.custom;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.basic.core.CommodityCode;
import ariba.common.core.CommodityExportMapEntry;
import ariba.common.core.PartitionedCommodityCode;
import ariba.purchasing.core.POLineItem;
import ariba.purchasing.core.PurchaseOrder;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.util.core.ListUtil;
import ariba.util.log.Log;
import ariba.util.scheduler.ScheduledTask;
import ariba.util.scheduler.ScheduledTaskException;
import ariba.util.scheduler.Scheduler;

public class BuysenseReqPOLineDescCCCFixTask extends ScheduledTask
{

    private final String ClassName         = this.getClass().getName();
    private String       msDBType          = null;
    private String       msDBDriver        = null;
    private String       msDBPassword      = null;
    private String       msDBURL           = null;
    private String       msDBUser          = null;
    private String       msDateRangeQuery  = null;
    private String       msApprovableTypes = null;
    private int          miCommitThreshold = 0;
    public Connection    moConnection;
    SimpleDateFormat     sdf_ET            = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");

    @SuppressWarnings("rawtypes")
    public void init(Scheduler scheduler, String scheduledTaskName, Map arguments)
    {
        Log.customer.warning(8000, ClassName + ":: init()");

        for (Iterator loItr = arguments.keySet().iterator(); loItr.hasNext();)
        {
            String lsKey = (String) loItr.next();

            if (lsKey.equals("DBType"))
            {
                msDBType = (String) arguments.get(lsKey);
                Log.customer.warning(8000, ClassName + " Connection parameter specified as " + msDBType + ".");
            }

            if (lsKey.equals("DBDriver"))
            {
                msDBDriver = (String) arguments.get(lsKey);
                Log.customer.warning(8000, ClassName + " Connection parameter specified as " + msDBDriver + ".");
            }

            if (lsKey.equals("DBPassword"))
            {
                msDBPassword = (String) arguments.get(lsKey);
                //Log.customer.warning(8000, ClassName + " Connection parameter specified as " + msDBPassword + ".");
            }

            if (lsKey.equals("DBURL"))
            {
                msDBURL = (String) arguments.get(lsKey);
                Log.customer.warning(8000, ClassName + " Connection parameter specified as " + msDBURL + ".");
            }

            if (lsKey.equals("DBUser"))
            {
                msDBUser = (String) arguments.get(lsKey);
                Log.customer.warning(8000, ClassName + " Connection parameter specified as " + msDBUser + ".");
            }
            if (lsKey.equals("DateRangeQuery"))
            {
                msDateRangeQuery = (String) arguments.get(lsKey);
                Log.customer.warning(8000, ClassName + " Connection parameter specified as " + msDateRangeQuery + ".");
            }
            if (lsKey.equals("ApprovableTypes"))
            {
                msApprovableTypes = (String) arguments.get(lsKey);
                Log.customer.warning(8000, ClassName + " Connection parameter specified as " + msApprovableTypes + ".");
            }

        }

    }

    @Override
    public void run() throws ScheduledTaskException
    {

        java.sql.Date loDBFromDate = null;
        java.sql.Date loDBToDate = null;

        List<java.sql.Date> dates = getProcessPeriod();
        if (dates != null && !dates.isEmpty())
        {
            loDBFromDate = dates.get(0);
            loDBToDate = dates.get(1);
        }

        HashMap<String, Object> lHM_CommodityExportMapEntry = new HashMap<String, Object>();
        getCommodityCodeforPartCC(lHM_CommodityExportMapEntry);

        String[] lsApprovableTypes = msApprovableTypes.split(",");
        for (int i = 0; i < lsApprovableTypes.length; i++)
        {
            String lsAppr = lsApprovableTypes[i];
            if (lsAppr.equalsIgnoreCase("Req"))
            {
                Log.customer.warning(8000, ClassName + ":: running task to process Req's between = " + loDBFromDate + " and " + loDBToDate);
                String lsReqQuery = "select r, r.UniqueName, r.StatusString from ariba.purchasing.core.Requisition r include inactive "
                        + " where r.LineItems.Description.CommonCommodityCode is null" + " AND (r.TimeCreated > Date('" + sdf_ET.format(loDBFromDate)
                        + "') and r.TimeCreated < Date('" + sdf_ET.format(loDBToDate) + "') ) order by r.TimeCreated ";

                Log.customer.warning(8000, ClassName + "::run() lsReqQuery = " + lsReqQuery);
                processReqs(lsReqQuery, lHM_CommodityExportMapEntry);
            }
            else if (lsAppr.equalsIgnoreCase("PO"))
            {
                Log.customer.warning(8000, ClassName + ":: running task to process PO's between = " + loDBFromDate + " and " + loDBToDate);
                String lsPOQuery = "select r, r.UniqueName, r.StatusString from ariba.purchasing.core.PurchaseOrder r include inactive "
                        + " where r.LineItems.Description.CommonCommodityCode is null" + " AND (r.TimeCreated > Date('" + sdf_ET.format(loDBFromDate)
                        + "') and r.TimeCreated < Date('" + sdf_ET.format(loDBToDate) + "') ) order by r.TimeCreated ";

                Log.customer.warning(8000, ClassName + "::run() lsPOQuery = " + lsPOQuery);
                processPOs(lsPOQuery, lHM_CommodityExportMapEntry);
            }
            else
            {
                Log.customer.warning(8000, ClassName + ":: UNRECOGNIZED ApprovableTypes  param");
                return;
            }

        }
    }

    private void processReqs(String lsQuery, HashMap<String, Object> lHM_CommodityExportMapEntry)
    {

        try
        {

            Partition partition = Base.getSession().getPartition();
            AQLOptions aqlOptions = new AQLOptions(partition);
            AQLQuery aqlQuery = AQLQuery.parseQuery(lsQuery);
            AQLResultCollection loResults = Base.getService().executeQuery(aqlQuery, aqlOptions);

            while (loResults.next())
            {

                Requisition loReq = (Requisition) loResults.getBaseId(0).get();
                String lsReqNumber = loReq.getUniqueName();
                for (Iterator<?> i = loReq.getLineItemsIterator(); i.hasNext();)
                {
                    ReqLineItem loReqLine = (ReqLineItem) i.next();
                    int lsLineNumber;
                    if (loReqLine.getDottedFieldValue("Description.CommonCommodityCode") == null)
                    {
                        lsLineNumber = loReqLine.getNumberInCollection();
                        CommodityExportMapEntry loCEME = loReqLine.getCommodityExportMapEntry();
                        CommodityCode cc = null;
                        String sCEMEBaseId = "";
                        String sCCUN = "";
                        String lsPartCCUN = "";
                        if (loCEME != null)
                        {
                            Log.customer.warning(8000, ClassName + "::processReqs() getting CC from CEME.");
                            cc = loCEME.getCommodityCode();
                            sCCUN = cc.getUniqueName();
                        }
                        else
                        {
                            PartitionedCommodityCode linePartCC = loReqLine.getCommodityCode();
                            if(linePartCC== null)
                            {
                                Log.customer.warning(9001, "CommodityCode & CommodityExportMapEntry are null in line " + lsLineNumber
                                        + " of Requisition " + lsReqNumber + " , NOT updated.");
                                continue;
                            }
                            //Get CC map through HashMap
                            lsPartCCUN = linePartCC.getUniqueName();
                            loCEME = (CommodityExportMapEntry) lHM_CommodityExportMapEntry.get(lsPartCCUN);
                            //Log.customer.warning(8000, ClassName + "::processReqs() loCEME = " + loCEME);
                            //Get CC directly using OOTB way through CommodityExportMapEntry API passing partitioned CC UniqueName
                            /*BaseVector ccemVector = CommodityExportMapEntry.getCommodityExportMap(linePartCC);
                            if(ccemVector!=null && !ccemVector.isEmpty())
                            {
                                loCCEM = (CommodityExportMapEntry) ((BaseId) ccemVector.get(0)).get();
                            }*/

                            if (loCEME != null)
                            {
                                cc = loCEME.getCommodityCode();
                                sCEMEBaseId = loCEME.getBaseId().toString();
                                sCCUN = cc.getUniqueName();
                                loReqLine.setDottedFieldValueWithoutTriggering("CommodityExportMapEntry", loCEME);
                            }

                        }

                        if (cc == null)
                        {
                            Log.customer.warning(9001, "Did not find CCC for partitioned code: " + lsPartCCUN + " in line " + lsLineNumber
                                    + " of Requisition " + lsReqNumber + " , NOT updated.");

                        }
                        else
                        {

                            loReqLine.setDottedFieldValueWithoutTriggering("Description.CommonCommodityCode", cc);
                            Log.customer.warning(9000, "Line " + lsLineNumber + " of Requisition " + lsReqNumber
                                    + " is updated with CommodityExportMapEntry: " + sCEMEBaseId + " & commodity: " + sCCUN);
                            miCommitThreshold++;
                        }
                    }//Check if any Req line exists with - Desc.CCC is not null but CommodityExportMapEntry is null

                }
                //commit set of records? Add code below.
                if (miCommitThreshold > 1000)
                {
                    Log.customer.warning(8000, ClassName + "::processReqs() commiting " + miCommitThreshold + " records.");
                    Base.getSession().transactionCommit();
                    miCommitThreshold = 0;
                }
            }

            //final commit happens here; no explicit code required. System automatically commits all records before exiting.
        }
        catch (Exception e)
        {
            Log.customer.warning(8000, ClassName + "::processReqs() EXCEPTION: " + e.getMessage());
        }

    }

    private void processPOs(String lsQuery, HashMap<String, Object> lHM_CommodityExportMapEntry)
    {

        try
        {

            Partition partition = Base.getSession().getPartition();
            AQLOptions aqlOptions = new AQLOptions(partition);
            AQLQuery aqlQuery = AQLQuery.parseQuery(lsQuery);
            AQLResultCollection loResults = Base.getService().executeQuery(aqlQuery, aqlOptions);

            while (loResults.next())
            {

                PurchaseOrder loPO = (PurchaseOrder) loResults.getBaseId(0).get();
                String lsPONumber = loPO.getUniqueName();
                for (Iterator<?> i = loPO.getLineItemsIterator(); i.hasNext();)
                {
                    POLineItem loPOLine = (POLineItem) i.next();
                    int lsLineNumber;
                    if (loPOLine.getDottedFieldValue("Description.CommonCommodityCode") == null)
                    {
                        lsLineNumber = loPOLine.getNumberInCollection();
                        CommodityExportMapEntry loCEME = loPOLine.getCommodityExportMapEntry();
                        CommodityCode cc = null;
                        String sCEMEBaseId = "";
                        String sCCUN = "";
                        String lsPartCCUN = "";
                        if (loCEME != null)
                        {
                            Log.customer.warning(8000, ClassName + "::processPOs() getting CC from CEME.");
                            cc = loCEME.getCommodityCode();
                            sCCUN = cc.getUniqueName();
                        }
                        else
                        {
                            PartitionedCommodityCode linePartCC = loPOLine.getCommodityCode();
                            if(linePartCC== null)
                            {
                                Log.customer.warning(9101, "CommodityCode & CommodityExportMapEntry are null in line " + lsLineNumber
                                        + " of Order " + lsPONumber + " , NOT updated.");
                                continue;
                            }
                            //Get CC map through HashMap
                            lsPartCCUN = linePartCC.getUniqueName();
                            loCEME = (CommodityExportMapEntry) lHM_CommodityExportMapEntry.get(lsPartCCUN);
                            //Log.customer.warning(8000, ClassName + "::processPOs() loCEME = " + loCEME);
                            //Get CC directly using OOTB way through CommodityExportMapEntry API passing partitioned CC UniqueName
                            /*BaseVector ccemVector = CommodityExportMapEntry.getCommodityExportMap(linePartCC);
                            if(ccemVector!=null && !ccemVector.isEmpty())
                            {
                                loCCEM = (CommodityExportMapEntry) ((BaseId) ccemVector.get(0)).get();
                            }*/

                            if (loCEME != null)
                            {
                                cc = loCEME.getCommodityCode();
                                sCEMEBaseId = loCEME.getBaseId().toString();
                                sCCUN = cc.getUniqueName();
                                loPOLine.setDottedFieldValueWithoutTriggering("CommodityExportMapEntry", loCEME);
                            }

                        }

                        if (cc == null)
                        {
                            Log.customer.warning(9101, "Did not find CCC for partitioned code: " + lsPartCCUN + " in line " + lsLineNumber
                                    + " of Order " + lsPONumber + " , NOT updated.");

                        }
                        else
                        {

                            loPOLine.setDottedFieldValueWithoutTriggering("Description.CommonCommodityCode", cc);
                            Log.customer.warning(9100, "Line " + lsLineNumber + " of Order " + lsPONumber
                                    + " is updated with CommodityExportMapEntry: " + sCEMEBaseId + " & commodity: " + sCCUN);
                            miCommitThreshold++;
                        }
                    }//Check if any Order line exists with - Desc.CCC is not null but CommodityExportMapEntry is null

                }
                //commit set of records? Add code below.
                if (miCommitThreshold > 1000)
                {
                    Log.customer.warning(8000, ClassName + "::processPOs() commiting " + miCommitThreshold + " records.");
                    Base.getSession().transactionCommit();
                    miCommitThreshold = 0;
                }
            }

            //final commit happens here; no explicit code required. System automatically commits all records before exiting.
        }
        catch (Exception e)
        {
            Log.customer.warning(8000, ClassName + "::processPOs() EXCEPTION: " + e.getMessage());
        }

    }

    private List<java.sql.Date> getProcessPeriod()
    {
        Log.customer.warning(8000, ClassName + "::getProcessPeriod() start");

        if (msDateRangeQuery == null)
        {
            return null;
        }
        List<java.sql.Date> lDates = ListUtil.list();
        try
        {

            String lsDriver = msDBDriver;
            String lsUrl = msDBURL;
            String lsUser = msDBUser;
            String lsPassword = msDBPassword;

            Log.customer.warning(8000, ClassName + "Driver = " + lsDriver + ", URL = " + lsUrl + ", User = " + lsUser + " password = " + lsPassword);
            moConnection = (Connection) DriverManager.getConnection(lsUrl, lsUser, lsPassword);

            Log.customer.warning(8000, ClassName + "::getProcessPeriod() connected. now setAutoCommit to false.");
            java.sql.Statement loSQLStatement = moConnection.createStatement();
            Log.customer.warning(8000, ClassName + "::getProcessPeriod() lsDateRangeQuery: " + msDateRangeQuery);
            ResultSet lResults = loSQLStatement.executeQuery(msDateRangeQuery);
            Log.customer.warning(8000, ClassName + "::getProcessPeriod() lResults: " + lResults == null);
            if (lResults != null && lResults.next())
            {
                java.sql.Date lFD = lResults.getDate("startdate");
                java.sql.Date lTD = lResults.getDate("enddate");
                Log.customer.warning(8000, ClassName + "::getProcessPeriod() : " + lFD + "," + lTD);
                lDates.add(lFD);
                lDates.add(lTD);
            }

        }
        catch (Exception loEx)
        {
            Log.customer.warning(8000, ClassName + "::getProcessPeriod() EXCEPTION = " + loEx.getMessage());
            return null;
        }
        finally
        {
            try
            {
                moConnection.close();
            }
            catch (SQLException e)
            {
                Log.customer.warning(8000, ClassName + "::getProcessPeriod() EXCEPTION closing connection = " + e.getMessage());
            }
        }
        return lDates;
    }

    private void getCommodityCodeforPartCC(HashMap<String, Object> lHM_CommodityExportMapEntry)
    {

        try
        {

            Partition partition = Base.getSession().getPartition();
            AQLOptions aqlOptions = new AQLOptions(partition);
            AQLQuery aqlQuery = AQLQuery.parseQuery(" SELECT s from ariba.common.core.CommodityExportMapEntry s order by s.PartitionedCommodityCode.UniqueName ");
            AQLResultCollection loResults = Base.getService().executeQuery(aqlQuery, aqlOptions);

            while (loResults.next())
            {
                BaseId loCEMEBId = loResults.getBaseId(0);
                if (loCEMEBId != null)
                {
                    CommodityExportMapEntry loCEMP = (CommodityExportMapEntry) loCEMEBId.get();
                    String sPCC = loCEMP.getPartitionedCommodityCode().getUniqueName();
                    lHM_CommodityExportMapEntry.put(sPCC, loCEMP);
                }
            }
            Log.customer.warning(8000, ClassName + "::getCommodityCodeforPartCC() Active lHM_CommodityExportMapEntry.size(): " + lHM_CommodityExportMapEntry.size());
            aqlQuery = AQLQuery.parseQuery(" SELECT s from ariba.common.core.CommodityExportMapEntry s include inactive where s.Active=false order by s.PartitionedCommodityCode.UniqueName ");
            loResults = Base.getService().executeQuery(aqlQuery, aqlOptions);

            while (loResults.next())
            {
                BaseId loCEMEBId = loResults.getBaseId(0);
                if (loCEMEBId != null)
                {
                    CommodityExportMapEntry loCEMP = (CommodityExportMapEntry) loCEMEBId.get();
                    String sPCC = loCEMP.getPartitionedCommodityCode().getUniqueName();
                    if(lHM_CommodityExportMapEntry.get(sPCC) == null)
                    {
                        lHM_CommodityExportMapEntry.put(sPCC, loCEMP);                        
                    }else
                    {
                        Log.customer.warning(8000, ClassName + "::getCommodityCodeforPartCC() CEME already exists in HM for: " + sPCC);
                    }

                }
            }
            Log.customer.warning(8000, ClassName + "::getCommodityCodeforPartCC() Active & Inactive lHM_CommodityExportMapEntry.size(): " + lHM_CommodityExportMapEntry.size());
        }
        catch (Exception e)
        {
            Log.customer.warning(8000, ClassName + "::getCommodityCodeforPartCC() exception = " + e.getMessage());
        }
    }

}
