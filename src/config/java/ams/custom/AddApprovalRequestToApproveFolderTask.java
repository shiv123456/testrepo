package config.java.ams.custom;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;

import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovalRequest;
import ariba.approvable.core.Folder;
import ariba.approvable.core.FolderItem;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.core.aql.AQLScalarExpression;
import ariba.user.core.Approver;
import ariba.user.core.Group;
import ariba.user.core.Role;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;
import ariba.util.scheduler.ScheduledTask;
import ariba.util.scheduler.ScheduledTaskException;
import ariba.util.scheduler.Scheduler;

/**
    @aribaapi private
*/
public class AddApprovalRequestToApproveFolderTask extends ScheduledTask
{
 
 private static String FromDate = "FromDate";
 private String fromDateArgument = null;
 
 private static String ToDate = "ToDate";
 private String toDateArgument = null;
 
 private static String FixData = "FixData";
 private String fixDataStr = null;
 private boolean fixDataArgument = false;
 
 // 81->822 changed HashMap to Map
    public void init (Scheduler scheduler,
                   String    scheduledTaskName,
                   Map arguments)
 
    {
        super.init(scheduler, scheduledTaskName, arguments);
 
        Iterator e = arguments.keySet().iterator();
 
  while (e.hasNext()) {
 
            String key = (String)e.next();
            if (key.equals(FromDate)) {
             fromDateArgument = (String)arguments.get(key);
            }
            if (key.equals(ToDate)) {
             toDateArgument = (String)arguments.get(key);
            }
            if (key.equals(FixData)) {
    fixDataStr= (String)arguments.get(key);
    if(fixDataStr.equals("true"))
    {
     fixDataArgument = true;
    }
    else
    {
     fixDataArgument = false;
    }
   }
     }
    }
 
    public void run () throws ScheduledTaskException
    {
  Log.customer.debug("AddApprovalRequestToApproveFolderTask run");
  try{
        //Logs.buysense.setDebugOn();
        Logs.buysense.setLevel(Level.DEBUG);
   // initialize file to write log information to
 
   Log.customer.debug("AddApprovalRequestToApproveFolderTask in try");
 
   Log.customer.debug("AddApprovalRequestToApproveFolderTask getting partition");
      Partition partition = Base.getSession().getPartition();
      Log.customer.debug("AddApprovalRequestToApproveFolderTask got partition: " + partition);
 

            Log.customer.debug("AddApprovalRequestToApproveFolderTask calling getRequisitions");
            AQLResultCollection requisitions = getRequisitions(partition,fromDateArgument,toDateArgument);
            Log.customer.debug("AddApprovalRequestToApproveFolderTask done calling getRequisitions");
 
   while (requisitions.next()) {
 
    Log.customer.debug("AddApprovalRequestToApproveFolderTask looping through AQLResultCollection");
 
       BaseId tempItem = (BaseId) requisitions.getBaseId(0);
       Log.customer.debug("AddApprovalRequestToApproveFolderTask looping through AQLResultCollection tempItem: " + tempItem);
 
    Approvable app = (Approvable)Base.getSession().objectFromId(tempItem);
    Log.customer.debug("AddApprovalRequestToApproveFolderTask looping through AQLResultCollection app: " + app);
 

    Log.customer.debug("AddApprovalRequestToApproveFolderTask calling getApprovalRequestsEnumeration");
    // 81->822 changed Enumeration to Iterator
    Iterator apRequests = app.getApprovalRequestsIterator(ApprovalRequest.StateActive, null, Boolean.TRUE);
    Log.customer.debug("AddApprovalRequestToApproveFolderTask calling done calling getApprovalRequestsEnumeration");
 
    while(apRequests.hasNext())
    {
 

      Log.customer.debug("AddApprovalRequestToApproveFolderTask looping through apRequests");
           ApprovalRequest ar = (ApprovalRequest)apRequests.next();
      // get the active approval request for the requisition
           Log.customer.debug("AddApprovalRequestToApproveFolderTask looping through apRequests ar: " + ar);
 

      Approver approver = ar.getApprover();
      ariba.user.core.User usr;
 
      Log.customer.debug("AddApprovalRequestToApproveFolderTask looping through apRequests approver: " + approver);
 

      if(approver instanceof ariba.user.core.User)
      {
     usr = (ariba.user.core.User) approver;
 
     Log.customer.debug("AddApprovalRequestToApproveFolderTask looping through apRequests approver is user calling checkItemAndPrintStatus");
 
     checkItemAndPrintStatus(ar, usr, app,"User");
      }
      else if(approver instanceof ariba.user.core.Role)
      {
     Log.customer.debug("AddApprovalRequestToApproveFolderTask looping through apRequests approver is role calling checkItemAndPrintStatus");
 
     Role role = (ariba.user.core.Role) approver;
 
        List roleUsersToCheck = role.getAllUsers();
        int roleUserSize = roleUsersToCheck.size();
 
     for(int i=0; i<roleUserSize; i++)
     {
      BaseId rolebid = (BaseId) roleUsersToCheck.get(i);
      usr = (ariba.user.core.User)Base.getSession().objectFromId(rolebid);
      Log.customer.debug("The user object is %s", usr);
      checkItemAndPrintStatus(ar, usr, app,"Role");
     }
      }
      else if(approver instanceof ariba.user.core.Group)
      {
        Log.customer.debug("AddApprovalRequestToApproveFolderTask looping through apRequests approver is group calling checkItemAndPrintStatus");
 
           Group grp = (ariba.user.core.Group) approver;
 
          List groupUsersToCheck = grp.getAllUsers();
        int groupUserSize = groupUsersToCheck.size();
       Log.customer.debug("The role users size: %s", groupUserSize);
 
     for(int j=0; j<groupUserSize; j++)
     {
       BaseId groupbid = (BaseId) groupUsersToCheck.get(j);
       usr = (ariba.user.core.User)Base.getSession().objectFromId(groupbid);
         Log.customer.debug("The user object is %s", usr);
         checkItemAndPrintStatus(ar, usr, app, "Group");
        }
      }
      else
      {
 
     Log.customer.debug("AddApprovalRequestToApproveFolderTask looping through apRequests approver is nothing to be processed later");
 
      }
    }
   }
 

   Log.customer.debug("AddApprovalRequestToApproveFolderTask Done");
 
  }
  catch(Exception e){ e.printStackTrace();}
  finally
  {
     //Logs.buysense.setDebugOff();
      Logs.buysense.setLevel(Level.DEBUG.OFF);
  }
 }

    // 81->822 changed Vector to List
    private List getIdentifiersFromArgument (String argument)
    {
        List identifiers = ListUtil.list();
 
        if (!StringUtil.nullOrEmptyOrBlankString(argument)) {
            int end = 0;
            int begin = 0;
            
            String identifier = null;
            do {
                end = argument.indexOf(",", begin);
                if (end != -1) {
                    identifier = argument.substring(begin, end);
                    begin = end+1;
                }
                else {
                    identifier = argument.substring(begin, argument.length());
                }
                identifier = identifier.trim();
                if (!StringUtil.nullOrEmptyOrBlankString(identifier)) {
                    identifiers.add(identifier);
                }
            }
            while (end != -1 && !StringUtil.nullOrEmptyOrBlankString(identifier));
        }
        return identifiers;
    }
 
    private AQLResultCollection getRequisitions (Partition partition, String FromDate, String ToDate)
    {
 
        Log.customer.debug("AddApprovalRequestToApproveFolderTask getRequisitions");
      // Get run date
      boolean lboolSetTime = false;
      String lsToTime = null;
      GregorianCalendar loToday = new GregorianCalendar();
      String lsMonth = "" + (loToday.get(Calendar.MONTH) + 1);
      String lsDate = "" + loToday.get(Calendar.DATE);

      if (lsMonth.length() == 1)
      {
         lsMonth = 0 + lsMonth;
      }

      if (lsDate.length() == 1)
      {
         lsDate = 0 + lsDate;
      }

      String lsToday = loToday.get(Calendar.YEAR) + "-" + lsMonth + "-" + lsDate;

      if ((FromDate.trim()).equalsIgnoreCase("today") || (FromDate.trim()).equalsIgnoreCase(lsToday))
      {
         FromDate = lsToday;
         ToDate = FromDate;
         lboolSetTime = true;
      }
      else if ((ToDate.trim()).equalsIgnoreCase(lsToday))
      {
         lboolSetTime = true;
      }

      if (lboolSetTime)
      {
         loToday.add(Calendar.MINUTE, - 15);
         String lsHour = "" + loToday.get(Calendar.HOUR_OF_DAY);
         String lsMinute = "" + loToday.get(Calendar.MINUTE);
         String lsSecond = "" + loToday.get(Calendar.SECOND);
         if (lsHour.length() == 1)
         {
            lsHour = 0 + lsHour;
         }
         if (lsMinute.length() == 1)
         {
            lsMinute = 0 + lsMinute;
         }
         if (lsSecond.length() == 1)
         {
            lsSecond = 0 + lsSecond;
         }

         lsToTime = " " + lsHour + ":" + lsMinute + ":" + lsSecond + " EDT";
      }
      else
      {
         lsToTime = " 23:59:59 EDT";
      }


      String fromDate = (FromDate + " 00:00:00 EDT");
      String toDate = ToDate + lsToTime;
 
        String querytext = "Select Requisition " +
                           "from Requisition " +
                           "where Requisition.StatusString = 'Submitted' " +
                           "and LastModified BETWEEN date(%s) AND date(%s) order by UniqueName";
 
        querytext = Fmt.S(querytext,
                          AQLScalarExpression.buildLiteral(fromDate),
                          AQLScalarExpression.buildLiteral(toDate));
 
        AQLOptions options = new AQLOptions(partition);
     AQLQuery query = AQLQuery.parseQuery(querytext);
 
        Log.customer.debug("AddApprovalRequestToApproveFolderTask getRequisitions query: " + query);
 
        AQLResultCollection results = Base.getService().executeQuery(query,
                                                                  options);
        return results;
 
    }
 
    private boolean existsInFolder(FolderItem fi, Folder f)
    {
  if(f != null)
  {
    List fitems = f.getItems();
 
   if(fitems.contains(fi))
   {
    return true;
   }
   return false;
  }
  Log.customer.debug("AddApprovalRequestToApproveFolderTask folder %s doesn't exist", f);
  return false;
 }
 
 private void checkItemAndPrintStatus(ApprovalRequest ar, ariba.user.core.User usr, Approvable app, String approvalTypeStr)
 {
  String reqStr;
  String approverStr;
  String statusStr = "";
 

  try{
 
   // returns an item if a folderitem exists corresponding to the parent folder, returns null if its not in the folder
   // * however, this does not mean its in the user's folder as the folderitem object is correct, but the actual link might not exist
 
         Log.customer.debug("AddApprovalRequestToApproveFolderTask checkItemAndPrintStatus");
 
   FolderItem fi = Folder.lookupItem(2, usr, app);
   Log.customer.debug("AddApprovalRequestToApproveFolderTask checkItemAndPrintStatus fi: " + fi);
 
   Folder f = Folder.getFolder(usr,"FolderNameApprove");
   Log.customer.debug("AddApprovalRequestToApproveFolderTask checkItemAndPrintStatus f: " + f);

   reqStr = app.getUniqueName();
   Log.customer.debug("AddApprovalRequestToApproveFolderTask checkItemAndPrintStatus reqStr: " + reqStr);
 
   approverStr = usr.getUniqueName();
   Log.customer.debug("AddApprovalRequestToApproveFolderTask checkItemAndPrintStatus approverStr: " + approverStr);
 

   // case 1 and 2: either the item is o.k., or  but doesn't exist in the folder
   if(fi != null)
   {
    // case 1: item is o.k.
    if(existsInFolder(fi, f))
    {
      statusStr = "ok";
      Log.customer.debug("AddApprovalRequestToApproveFolderTask checkItemAndPrintStatus fi is ok statusStr: " + statusStr);
 
    }
    // case 2: folder item exists, but its not in the folder
    else
    {
      if(fixDataArgument)
      {
       f.getItems().add(fi.id);
        statusStr = "Fixed (Repaired)";
        Log.customer.debug("AddApprovalRequestToApproveFolderTask checkItemAndPrintStatus folder item exists, but its not in the folder fix: " + statusStr);
        Logs.buysense.debug("Fixed AddApprovalRequestToApproveFolder - Approvable : " + reqStr + " User : " + approverStr); 
      }
      else
      {
 
        statusStr = "NotFixed";
        Log.customer.debug("AddApprovalRequestToApproveFolderTask checkItemAndPrintStatus folder item exists, but its not in the report do not fix statusStr: " + statusStr);
        Logs.buysense.debug("Report:AddApprovalRequestToApproveFolder - Approvable : " + reqStr + " User : " + approverStr); 
      }
 
    }
   }
   else
   {
 
      Log.customer.debug("AddApprovalRequestToApproveFolderTask checkItemAndPrintStatus folder item does not exist, try add a new item");
    // case 3: folder item does not exist, try add a new item
    try{
     if(fixDataArgument)
     {
 
     Log.customer.debug("AddApprovalRequestToApproveFolderTask checkItemAndPrintStatus folder item does not exist, adding a new item");
      if(f.addItem(app.id))
       {
        statusStr = "Fixed";
        Log.customer.debug("AddApprovalRequestToApproveFolderTask checkItemAndPrintStatus folder item does not exist adding a new item fixed statusStr: " + statusStr);
        Logs.buysense.debug("Fixed AddApprovalRequestToApproveFolder - Approvable : " + reqStr + " User : " + approverStr); 
          }
        }
        else
        {
      statusStr = "NotFixed";
      Log.customer.debug("AddApprovalRequestToApproveFolderTask checkItemAndPrintStatus folder item does not exist adding a new item not fixed report only statusStr: " + statusStr);
        Logs.buysense.debug("Report:AddApprovalRequestToApproveFolder - Approvable : " + reqStr + " User : " + approverStr); 
     }
    }
    // case 4: folder item does not exist, but exists in baseid tab, possible stack trace requires manual fixing
    catch(Exception e){
 
     statusStr = "Problem during fixing";
     Log.customer.debug("AddApprovalRequestToApproveFolderTask checkItemAndPrintStatus folder item does not exist, but exists in baseid tab, possible stack trace requires manual fixing statusStr: " + statusStr);
        Logs.buysense.debug("Fix Manual - Report:AddApprovalRequestToApproveFolder - Approvable : " + reqStr + " User : " + approverStr); 
    }
 
   }
  }
  catch(Exception e){ e.printStackTrace();}
 }
 
}
