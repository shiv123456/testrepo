package config.java.ams.custom;

import java.util.List;

import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.fields.ValidChoices;
import ariba.htmlui.baseui.Log;
import ariba.util.core.ListUtil;

    public class BuysenseEformRadio2ButtonValidChoices extends ValidChoices
    {
        public static final String ClassName = "config.java.ams.custom.BuysenseEformRadio2ButtonValidChoices";

        public boolean useOrderedChoices()
        {
            return true;
        }

        @Override
        protected Object choices()
        {
            List<String> choices = ListUtil.list();

            Object cobj = getValueSourceContext();
            Log.customer.debug(ClassName + " cobj " + cobj);
            if (cobj instanceof ClusterRoot)
            {
                ClusterRoot loEfromCR = (ClusterRoot) cobj;
                List<String> loRadio2Values = getRadioFieldVAlues(loEfromCR);
                if (loRadio2Values != null && !loRadio2Values.isEmpty())
                {
                    for (int i = 0; i < loRadio2Values.size(); i++)
                    {
                        String sRadio2FieldVal = loRadio2Values.get(i);
                        Log.customer.debug(ClassName + " sRaidFieldVal " + sRadio2FieldVal);
                        choices.add(sRadio2FieldVal);
                    }
                }
            }

            Log.customer.debug(ClassName + "choices() " + choices);
            return choices;
        }

        private List<String> getRadioFieldVAlues(ClusterRoot cobj)
        {
            List<String> lsFieldValues = ListUtil.list();
            
            String sClientID = (String) cobj.getDottedFieldValue("EformChooser.ClientID");
            String sProfName = (String) cobj.getDottedFieldValue("EformChooser.ProfileName");
            String sNullValueName = getNullValueName();
            Log.customer.debug(ClassName + " sNullValueName: " + sNullValueName);
            String sFieldName = "EformHeadGrp2RadioVal";
            
            String queryText = "Select Description, FieldName, UniqueName from ariba.core.BuysEformFieldDataTable where " +
            "ClientID = '" + sClientID + "' AND ProfileName='"+ sProfName + "' " +
            "AND FieldName like '" + sFieldName + "%' Order by UniqueName ";
            
            AQLQuery query = AQLQuery.parseQuery(queryText);
            Log.customer.debug(ClassName + " query: " + query);
            Partition pcsv = Base.getService().getPartition("pcsv");
            AQLResultCollection arc = Base.getService().executeQuery(query, new AQLOptions(pcsv));

            if (arc != null && arc.getFirstError() == null)
            {
                while (arc.next())
                {
                    String name = arc.getString("Description");
                    Log.customer.debug(ClassName + " Name: " + name);
                    lsFieldValues.add(name);
                }

            }

            return lsFieldValues;
        }

    }
