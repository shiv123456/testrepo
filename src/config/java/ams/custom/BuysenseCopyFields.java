package config.java.ams.custom;

// 08/07/2003: Updates for Ariba 8.0 (David Chamberlain)
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

import java.util.List;

import ariba.approvable.core.Record;
import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.core.ClusterRoot;
import ariba.base.core.TransientData;
import ariba.base.fields.*;
import ariba.procure.core.SimpleProcureRecord;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.util.core.*;
import ariba.common.core.Address;
import ariba.common.core.SplitAccounting;
import ariba.common.core.action.CopyFields;

//81->822 changed Vector to List
public class BuysenseCopyFields extends CopyFields
{
	TransientData moTransientData = null;
    public void fire(ValueSource object, PropertyTable params)
    {
        /*
        This method had to be overriden for the Ariba 8.1 upgrade. The shared/partitioned User split
        caused problems in this class. The fields that are copied by this class, are on the
        BuysenseOrg and the BuysenseOrg is no longer simply on the User object (the shared User), it's
        on the Partitioned User. Therefore, we had the change the logic to look on the Partitioned
        User object.  The AML (AmsReqDefaulter.aml) that triggers this code has also been changed for
        this mod.  02/17/2004: Updates for Ariba 8.1 (David Chamberlain)
        */
        String sourcePath = params.stringPropertyForKey("SourcePath");
        Object source = params.getPropertyForKey("Source");

        if(source == null)
            source = object;

        String targetPath = params.stringPropertyForKey("TargetPath");
        Log.customer.debug("targetPath: %s", targetPath);
        Object target = params.getPropertyForKey("Target");

        if(target == null)
            target = object;

        source = resolveSource(source, sourcePath);
        if ((source == null) && (sourcePath.startsWith("Requester.PartitionedUser."))
                && (object instanceof BaseObject)) {
                ariba.user.core.User user = (ariba.user.core.User)((BaseObject)object).getFieldValue("Requester");
                if (user != null) {
                    ariba.common.core.User cuser = ariba.common.core.User.getPartitionedUser(user, ((BaseObject)object).getPartition());
                        Log.customer.debug("**** In BuysenseCopyFields::Partitioned User"+cuser.getUniqueName());
                        source = cuser.getDottedFieldValue(sourcePath.substring(26));
                }
        }

        Log.customer.debug("**** In BuysenseCopyFields::fire test ***source: "+source+" target: "+target+" targetPath: "+targetPath);
        copyObjects(source, target, targetPath, params);
    }

    protected void copyFields(BaseObject source,
                              BaseObject target, PropertyTable params)
    {
        String sourceGroup = params.stringPropertyForKey("SourceGroup");
        String targetGroup = params.stringPropertyForKey("TargetGroup");
        boolean defaulting = params.booleanPropertyForKey("Defaulting");
        if(targetGroup == null)
            targetGroup = sourceGroup;
        List sourceFields = FieldProperties.getFieldsInGroup(sourceGroup,
                                                               source);
        List targetFields = FieldProperties.getFieldsInGroup(targetGroup,
                                                               target);
        // Ariba 8.0: Fixed deprecated API. Util.equalElements(v1, v2) is replaced by
        // ariba.util.core.ListUtil.equalElements(v1, v2).
        List fieldNames = ListUtil.equalElements(sourceFields,
                                                     targetFields);

        /*SRINI::START::Added below code for CSPL-2622
         * In 9r1 this trigger is firing during copy process and overriding REQ, ReqLine and SplitAccounting
         * BSO fields with Requester's BSO fields. Objective of below code is to protect overriding the
         * field only during copy process. Introduced a new field called "IsCopied" which will have initial
         * value as null for a fresh and copied REQ. First if condition sets value to "NotCopied" for fresh
         * REQ's so system will override the values in all cases (while REQ creation and subsequent Requester
         * changes). Second if sets value to "Copied" and bypasses field value overriding during copy process
         * and allows field overriding with subsequent Requester changes.
         *
         *  REQ Title will be always null for fresh REQ's and user need to fill in the value
         *  REQ Title will never be null for Copy REQ's and always starts with "Copy of "
         */
        Requisition lsReq = getReq(target);
        boolean lbCopiedReq = isCopiedReq(lsReq);
        boolean lbExternalreq = isExternalReq(lsReq);
        boolean lbCopiedFromEform = isCopiedFromEform();
        
        if(lsReq.getFieldValue("IsCopied") == null && (lsReq.getName() == null || lbCopiedReq || lbExternalreq || lbCopiedFromEform))
        {
        	Log.customer.debug("BuysenseCopyFields::copyFields::Setting Copied field");
        	if(lbCopiedReq)
        	{
            	lsReq.setFieldValue("IsCopied", "Copied");
        	}
        	else
        	{
            	lsReq.setFieldValue("IsCopied", "NotCopied");
            	moTransientData.put("EformToRequisition", "No");
        	}
        }

        if(lsReq.getName() != null && lsReq.getName().startsWith("Copy of ") &&
           lsReq.getFieldValue("IsCopied") == null)
        {
        	Log.customer.debug("BuysenseCopyFields::copyFields::Returning without overriding");
        	return;
        }
        /*
         * SRINI::END::Added below code for CSPL-2622
         */

        for(int i = 0; i < fieldNames.size(); i++)
        {
        	// Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            String fieldName = (String)fieldNames.get(i);
            Log.customer.debug("BuysenseCopyFields::copyFields::FieldName = %s FieldValue = %s",fieldName,target.getDottedFieldValue(fieldName));
            //if(fieldName.equalsIgnoreCase("BillingAddress") && (source.getDottedFieldValue(fieldName) != null))
            if(fieldName.equalsIgnoreCase("BillingAddress"))
            {
                	Log.customer.debug("FieldName: %s",fieldName);
                	setBillingAddress(source,target,lsReq,fieldName);
            }
            else
            if((!defaulting || !nullField(source, fieldName)))
            {
                //Log.customer.debug("FieldName: %s",fieldName);
            	Log.customer.debug("BuysenseCopyFields::copyFields::Overriding");
                BaseObject.copyField(source,
                                     target, fieldName, !defaulting, false);
            }
        }
    }

    public boolean nullField(BaseObject sourceObj, String sourceFieldName)
    {
    	Object loSourceObj = sourceObj.getDottedFieldValue(sourceFieldName);
    	boolean nullField = false;
    	if(loSourceObj == null)
    	{
    		nullField = true;
    	}
    	else if(loSourceObj != null && loSourceObj instanceof String && StringUtil.nullOrEmptyOrBlankString((String)loSourceObj))
    	{
    		nullField = true;
    	}
    	Log.customer.debug("BuysenseCopyFields::nullField::returning "+nullField);
    	return nullField;
    }
    /*
     * SRINI::START::Added below code for CSPL-2622
     */
    public Requisition getReq(BaseObject targetObject)
    {
    	Requisition lsReq = null;
    	if(targetObject instanceof Requisition)
    	{
    		lsReq = (Requisition) targetObject;
    	}
    	else if(targetObject instanceof ReqLineItem)
    	{
    		lsReq = (Requisition) ((ReqLineItem)targetObject).getLineItemCollection();
    	}
    	else if(targetObject instanceof SplitAccounting)
    	{
    		ReqLineItem lsItem = (ReqLineItem) ((SplitAccounting)targetObject).getLineItem();
    		lsReq = (Requisition) lsItem.getLineItemCollection();
    	}
    	Log.customer.debug("BuysenseCopyFields::getReq::returning REQ %s", lsReq.getUniqueName());
    	return lsReq;
    }

    public boolean isCopiedReq(Requisition reqObject)
    {
    	BaseVector lvREcords = (BaseVector) reqObject.getRecords();
    	for(int i = 0; i < lvREcords.size(); i++)
    	{
    		Record lsRecord = (Record) lvREcords.get(i);
    		if (lsRecord instanceof SimpleProcureRecord)
    		{
    			String lsType = (String)lsRecord.getFieldValue("Type");
    			String lsRecordType = (String)lsRecord.getFieldValue("RecordType");
    			if(lsType.equals("Copied") && lsRecordType.equals("CopyReqRecord"))
    			{
    				Log.customer.debug("BuysenseCopyFields::SRINI::getReq::returning true");
    				return true;
    			}
    		}
    	}
    	Log.customer.debug("BuysenseCopyFields::SRINI::getReq::returning false");
    	return false;
    }
    /*
     * SRINI::END::Added below code for CSPL-2622
     */
    public void setBillingAddress(BaseObject source,BaseObject target,Requisition lsReq,String fieldName)
    {
    	Address loFinalBillingAddress = getFinalBillingAddress(source, lsReq, fieldName);

    	if(loFinalBillingAddress != null)
    	{
    		Log.customer.debug("BuysenseCopyFields::setBillingAddress::loFinalBillingAddress %s", loFinalBillingAddress.getUniqueName());
    		List lvReqLines = lsReq.getLineItems();
	    	if(lvReqLines.size() == 0)
	    	{
	    		target.setFieldValue(fieldName, loFinalBillingAddress);
	    	}
	    	else
	    	for (int i = 0; i < lvReqLines.size(); i++)
	        {
	        	ReqLineItem loEachLine = (ariba.purchasing.core.ReqLineItem) lvReqLines.get(i);
	        	target.setFieldValue(fieldName, loFinalBillingAddress);
	        	loEachLine.setFieldValue(fieldName, loFinalBillingAddress);
	        }
    	}
     }

    public Address getFinalBillingAddress(BaseObject bso, Requisition req, String billFieldName)
    {
    	Address loAddress = null;
    	String lsPAddress = Base.getService().getParameter(req.getPartition(), "Application.Base.Data.DefaultBillToAddress");
    	ariba.common.core.User loPartUser = ariba.common.core.User.getPartitionedUser(req.getRequester(), req.getPartition());
    	//Log.customer.debug("BuysenseCopyFields::getFinalBillingAddress::lsPAddress %s loPartUser addr %s", lsPAddress, loPartUser.getBillingAddress().getUniqueName());
    	if(loPartUser != null && lsPAddress != null && loPartUser.getBillingAddress() != null &&
    			!loPartUser.getBillingAddress().getUniqueName().trim().equals(lsPAddress.trim()))
    	{
    		loAddress = loPartUser.getBillingAddress();
    	}
    	else if(loAddress == null && bso.getFieldValue(billFieldName) != null)
    	{
    		loAddress = (Address) bso.getFieldValue(billFieldName);
    	}
    	return loAddress;
    }

    public boolean isExternalReq(Requisition reqObject)
    {
		if (reqObject!=null)
		{

			String sUniqueName  = (String) reqObject.getFieldValue("UniqueName") ;
			if (sUniqueName.startsWith("IRQ:") || sUniqueName.startsWith("E2E:") || sUniqueName.startsWith("QQ:"))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}

    }
        
    public boolean isCopiedFromEform()
    {
        moTransientData =  Base.getSession().getTransientData();
        String lsEformToRequisition = (String)moTransientData.get("EformToRequisition");
        Log.customer.debug("BuysenseCopyFields::isCopiedFromEform::EformToRequisition %s", lsEformToRequisition);
        if(lsEformToRequisition != null && lsEformToRequisition.trim().equals("Yes"))
        {
        	Log.customer.debug("BuysenseCopyFields::returning true for EformToReq");
        	return true;
        }
    	Log.customer.debug("BuysenseCopyFields::returning false for EformToReq");
        return false;
    }
    
    public BuysenseCopyFields()
    {
    }


}
