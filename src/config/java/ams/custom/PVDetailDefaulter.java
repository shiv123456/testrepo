/* Version 1.7 */
/*****************************************************************
DevSPL 188  ahiranandani  Added logic to update #Accepted, #Paid and #InPaymentProcess
                          from Order. Now this class is also called by CalculateTotalTrigger
                          - in this case it only updates the three fields.

WA ST SPL # 475/Baseline Dev SPL # 229   ahiranandani  PaymentAccountings --> Accountings.PaymentAccountings.
                          qty initialization moved into the for loop.

WA ST SPL # 476/Baseline Dev SPL # 230   ahiranandani  Initialize LineNumber, Description &
                          NumberAccepted for PaymentItem[].Accountings

DevSPL 219  ahiranandani  isFirstTime related logic removed. Moved logic to update #Accepted, #Paid
                          and #InPaymentProcess (from Order) to CalculateTotals.java.

DevSPL 233  ahiranandani  Set SupplierPartNumber.
DevSPL 233  imohideen     Set only 60 characters of the Description.  The field length is set to only 60.

eVA Dev SPL # 4 anup      get #Accepted from NumberAccepted instead of BuysenseNumberAccepted field on poline.

******************************************************************/

/*
10/10/2003: Updates for Ariba 8.1 (Richard Lee)
03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/03/2004: rgiesen dev SPL #37 - Cleaned up file
03/04/2004: rgiesen dev SPL #38 - replace getLeadCurrency with Currency.getDefaultCurrency(partition)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method

*/

package config.java.ams.custom;

import ariba.base.fields.ValueInfo;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import java.util.List;
import ariba.base.core.ClusterRoot;
import ariba.base.core.BaseObject;
import ariba.util.log.Log;
import ariba.purchasing.core.POLineItem;
import ariba.base.core.Partition;
import java.math.BigDecimal;
import ariba.base.core.BaseVector;
// Ariba 8.1 Changed Money and Currency from common to basic
import ariba.basic.core.Money;
import ariba.basic.core.Currency;
import ariba.common.core.SplitAccountingCollection;
import ariba.common.core.Accounting;
import ariba.common.core.*;
//import ariba.core.BuysenseFieldDataTable;
// Ariba 8.1: Need ListUtil
import ariba.util.core.ListUtil;

//81->822 changed Vector to List
//81->822 changed ConditionValueInfo to ValueInfo
public class PVDetailDefaulter extends CalculateTotals
{
    static final String Value = "Value";

    private static final ValueInfo[] parameterInfo =
    {
        new ValueInfo(TargetParam, true, IsScalar, StringClass)
    };

    private static final String[] requiredParameterNames = { TargetParam };

	private static final ValueInfo valueInfo =
     new ValueInfo(IsScalar, "ariba.core.PaymentEform");


    public void fire (ValueSource object, PropertyTable params)
    {
		//String day = params.stringPropertyForKey(TargetParam);
		Log.customer.debug("PVDetailDefaulter.fire()");
		ClusterRoot cr = (ClusterRoot)object;

		//Get the order
		ClusterRoot order  = (ClusterRoot)cr.getFieldValue("Order");
		if(order==null)
		{
			return;
		}

		// New Supplier ? - ST SPL # 342
		String supplierID = (String)order.getDottedFieldValue("Supplier.UniqueName");
		if ((supplierID == null) || (supplierID.trim().equals("")))
		{
			cr.setFieldValue("Supplier", null);
			cr.setFieldValue("SupplierLocation", null);
		}
		else
		{
			Supplier supplier = (Supplier)order.getFieldValue("Supplier");
			BuysensePaymentSetSupplierLocation setLocation = new BuysensePaymentSetSupplierLocation();
			SupplierLocation location = (SupplierLocation) setLocation.getCorrectLocation(supplier);
			cr.setFieldValue("SupplierLocation", location);
		}
		//rgiesen dev SPL #39
		Partition part = cr.getPartition();

		//Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
		//Partitioned User to obtain the extrinsic BuysenseOrg field value.
		ariba.user.core.User RequesterUser = (ariba.user.core.User)cr.getDottedFieldValue("Requester");
		//rgiesen dev SPL #39
		//ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
		ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,part);
		ClusterRoot client = (ClusterRoot)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName");
		Log.customer.debug("PVDetailDefaulter.fire():Got the client obejct %s",client);

		// This is sum of all line net amounts.
		BaseVector polines = (BaseVector)cr.getDottedFieldValue("Order.LineItems");
		Log.customer.debug("PVDetailDefaulter.fire():The size of polines vector is: %s ",polines.size());

		BaseVector pvlines = (BaseVector)cr.getFieldValue("PaymentItems");


		int i;
		BigDecimal itemQty,itemQtyAcc,itemQtyPaid,itemQtyInp;
		Integer lineno;
		String line, supplierPartNum;
		String desc=new String();
		Money orderItemUnitAmt, orderItemAmt;
		Money paymentItemUnitAmt;
		SplitAccountingCollection acctgs;

		if(pvlines.size()!=0)
		{
			pvlines.removeAllElements();
		}

		//Default Payment Header Buysense org fields data
		List  paymentHeaderFieldValues = FieldListContainer.getPaymentHeaderFieldValues();

		//rgiesen dev SPL #37 - moved definition to outside for loop in order to compile
		ariba.common.core.User poRequesterPartitionUser=null;
		for(int j=0; j<paymentHeaderFieldValues.size(); j++)
		{
			//Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
			//Partitioned User to obtain the extrinsic BuysenseOrg field value.
			ariba.user.core.User poRequesterUser = (ariba.user.core.User)cr.getDottedFieldValue("Order.LineItems[0].Requisition.Requester");
			////rgiesen dev SPL #39
			//poRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(poRequesterUser,Base.getSession().getPartition());
			poRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(poRequesterUser,part);
			cr.setFieldValue((String)paymentHeaderFieldValues.get(j),poRequesterPartitionUser.getDottedFieldValue("BuysenseOrg."+(String)paymentHeaderFieldValues.get(j)));
		}

		for(i=0;i<polines.size();i++)
		{

			POLineItem item = (POLineItem)polines.get(i);
			BaseObject paymentItem = (BaseObject) BaseObject.create("ariba.core.PaymentItem", part);
			Log.customer.debug("PVDetailDefaulter.fire():After paymentItem is set");

			// Begin : Default the line
			List  paymentLineFieldValues = FieldListContainer.getPaymentLineFieldValues();
			for(int j=0; j<paymentLineFieldValues.size(); j++)
			{
				//Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
				//Partitioned User to obtain the extrinsic BuysenseOrg field value.
				paymentItem.setFieldValue((String)paymentLineFieldValues.get(j),poRequesterPartitionUser.getDottedFieldValue("BuysenseOrg."+(String)paymentLineFieldValues.get(j)));
			}


			lineno=(Integer)item.getFieldValue("NumberInCollection");
			line = lineno.toString();
			paymentItem.setFieldValue("LineNumber",line);
			// ASU ER #210 - Added logic to set the OrderID and OrderLineNum
			paymentItem.setFieldValue("OrderLineNum",line);
			String orderlineno=(String)order.getFieldValue("UniqueName");
			paymentItem.setFieldValue("OrderID",orderlineno);

			desc=(String)item.getDottedFieldValue("Description.Description");

			//ST SPL 233
			if (desc.length() > 60)
				desc = desc.substring(0,60);
			paymentItem.setFieldValue("Description",desc);
			// Set Supplier Part Number
			supplierPartNum=(String)item.getDottedFieldValue("Description.SupplierPartNumber");
			paymentItem.setFieldValue("SupplierPartNumber",supplierPartNum);
			itemQty=(BigDecimal)item.getFieldValue("Quantity");
			paymentItem.setFieldValue("NumberOrdered",itemQty);
			Log.customer.debug("PVDetailDefaulter.fire():Quantity set to : %s ", itemQty);
			itemQtyAcc=(BigDecimal)item.getFieldValue("NumberAccepted");
			//itemQtyAcc=(BigDecimal)item.getFieldValue("BuysenseNumberAccepted");
			if (itemQtyAcc == null)
				itemQtyAcc = new BigDecimal(0);
			paymentItem.setFieldValue("NumberAccepted",itemQtyAcc);
			Log.customer.debug("PVDetailDefaulter.fire():Number Accepted set to : %s", itemQty);
			itemQtyPaid=(BigDecimal)item.getFieldValue("NumberPaid");
			paymentItem.setFieldValue("NumberPaid",itemQtyPaid);
			Log.customer.debug("PVDetailDefaulter.fire():Number Paid set to : %s", itemQty);
			itemQtyInp=(BigDecimal)item.getFieldValue("NumberInPaymentProcess");
			paymentItem.setFieldValue("NumberInPaymentProcess",itemQtyInp);
			Log.customer.debug("PVDetailDefaulter.fire():NumberInPaymentProcess set to : %s", itemQty);
			BigDecimal qty = new BigDecimal(0);
			Log.customer.debug("PVDetailDefaulter.fire():Accepted: %s", itemQtyAcc) ;
			Log.customer.debug("PVDetailDefaulter.fire():Paid : %s", itemQtyPaid);
			Log.customer.debug("PVDetailDefaulter.fire():INP : %s", itemQtyInp);
			qty = qty.add(itemQtyAcc).subtract(itemQtyPaid).subtract(itemQtyInp);
			paymentItem.setFieldValue("CurPayingQuantity",qty);

			//setting the client here for the line
			paymentItem.setDottedFieldValue("ClientName",client);

			// defaulting all PO amount fields
			orderItemUnitAmt=(Money)item.getDottedFieldValue("Description.Price");
			paymentItem.setFieldValue("OrderUnitCost",orderItemUnitAmt);
			orderItemAmt=(Money)item.getFieldValue("Amount");
			paymentItem.setFieldValue("OrderAmount",orderItemAmt);

			// defaulting the PVline unit price and amount to the PO line Unit Price and amount
			paymentItemUnitAmt=orderItemUnitAmt;
			paymentItem.setFieldValue("InvoiceUnitCost",paymentItemUnitAmt);

			acctgs=(SplitAccountingCollection)item.getFieldValue("Accountings");
			List orderLineAcctgVector = acctgs.getSplitAccountings();
			Log.customer.debug("PVDetailDefaulter.fire():The order accounting lines have been got into a vector and cloned");

			// Ariba 8.1: List::List() is deprecated by ListUtil::newVector()
			List paymentAccountings=ListUtil.list();


			//default the PaymentAccountings vector on the PaymnetItem
			for(int k=0;k<orderLineAcctgVector.size();k++)
			{
				Accounting orderAcctg= (Accounting)orderLineAcctgVector.get(k);
				BaseObject paymentAcctg = (BaseObject) BaseObject.create("ariba.core.PaymentAccounting", part);
				//Order may matter. We need to set the client name before FieldDefault1
				paymentAcctg.setDottedFieldValue("ClientName",client);
				List acctgFieldValues = FieldListContainer.getAcctgFieldValues();
				for(int j=0; j<acctgFieldValues.size(); j++)
				{
					paymentAcctg.setFieldValue((String)acctgFieldValues.get(j),orderAcctg.getFieldValue((String)acctgFieldValues.get(j)));
				}
				//paymentAcctg.setFieldValue("PaymentAmount",orderAcctg.getFieldValue("Amount"));

                //rgiesen dev SPL #38 - replace getLeadCurrency with Currency.getDefaultCurrency(partition)
				//paymentAcctg.setFieldValue("PaymentAmount",new Money(0.00, lcLeadCurrency.getLeadCurrency()));
				paymentAcctg.setFieldValue("PaymentAmount",new Money(0.00, Currency.getDefaultCurrency(part)));


				paymentAcctg.setFieldValue("PaymentAmountPercent",orderAcctg.getFieldValue("Percentage"));
				paymentAcctg.setDottedFieldValue("ClientName",client);
				// Ariba 8.1 List::addElement() is deprecated by List::add()
				paymentAccountings.add(paymentAcctg);
			}
			paymentItem.setDottedFieldValue("Accountings.PaymentAccountings",paymentAccountings);
			paymentItem.setDottedFieldValue("Accountings.LineNumber",line);
			paymentItem.setDottedFieldValue("Accountings.Description",desc);
			paymentItem.setDottedFieldValue("Accountings.NumberAccepted",itemQtyAcc);

			Log.customer.debug("PVDetailDefaulter.fire():POLineItem about to be set to : %s ", item);
			//paymentItem.setFieldValue("POLineItem",item);
			Log.customer.debug("PVDetailDefaulter.fire():POLineItem has been set to : %s ", paymentItem.getFieldValue("POLineItem"));
			//paymentItem.setExtrinsic("tempLineItem",item);
			// Ariba 8.1 List::addElement() is deprecated by List::add()
			pvlines.add(paymentItem);
			Log.customer.debug("PVDetailDefaulter.fire():Paymnet item added to vector. Current vector size: %s", pvlines.size());
			//paymentItem=null;
		}
		//rgiesen dev SPL#38
		//cr.setFieldValue("ShippingAmt",new Money(0.00, lcLeadCurrency.getLeadCurrency()));
		//cr.setFieldValue("TaxAmt",new Money(0.00, lcLeadCurrency.getLeadCurrency()));
		//cr.setFieldValue("DiscountAmt",new Money(0.00, lcLeadCurrency.getLeadCurrency()));
		cr.setFieldValue("ShippingAmt",new Money(0.00, Currency.getDefaultCurrency(part)));
		cr.setFieldValue("TaxAmt",new Money(0.00, Currency.getDefaultCurrency(part)));
		cr.setFieldValue("DiscountAmt",new Money(0.00, Currency.getDefaultCurrency(part)));

		cr.setFieldValue("DiscountPercent",new BigDecimal(0));

		cr.save();
		super.fire(object,params);
		Log.customer.debug("PVDetailDefaulter.fire():Outside For Loop Before Save");
		cr.save();
		Log.customer.debug("PVDetailDefaulter.fire():Outside For Loop After Save");
		PaymentSummary ps = new PaymentSummary();
		ps.fire(object,params);

    }

	/**
		Return the list of valid value types.
	*/
	protected ValueInfo getValueInfo ()
	{
		return valueInfo;
	}

	/**
		Return the parameter names.
	*/
	protected ValueInfo[] getParameterInfo ()
	{
		return parameterInfo;
	}

	/**
		Return the required parameter names.
	*/
	protected String[] getRequiredParameterNames ()
	{
		return requiredParameterNames;
	}
}
