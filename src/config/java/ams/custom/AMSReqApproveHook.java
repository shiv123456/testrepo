/*
    Copyright (c) 1996-1999 Ariba, Inc.
    All rights reserved. Patents pending.

   Falahyar March 2000
*/
// rlee, 10/24/03, ER18 and Prod 218 - Copying and using expired pcards and other PCard validation related issues
// Version 1.3 rlee, 11/4/03, ER18 & Prod 218. moved the EditClassGenerator.invokeHookEdits outside the for loop.

package config.java.ams.custom;

// 08/07/2003: Updates for Ariba 8.0 (David Chamberlain)
/* 09/03/2003: Updates for Ariba 8.0 (David Chamberlain)
   Replaced all Util.vector()'s with ListUtil.vector()
   Replaced all Util.getInteger()'s with Constants.getInteger()
   Replaced all Util.integer()'s with Constants.getInteger()
   Replaced import of ApprovableHook by deprecation  */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/

import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.core.Partition;
import ariba.base.core.BaseVector;
//import ariba.common.base.Variant;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovalRequest;
// Ariba 8.0: Replaced deprecated ApprovableHook
import ariba.approvable.core.ApprovableHook;
//import ariba.approvable.core.ApprovableHook;
import ariba.procure.core.Procure;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.util.log.Log;
//import ariba.util.core.Util;
import java.util.List;
//import ariba.common.util.*;
//import ariba.common.base.*;
// Ariba 8.0: Deprecations, methods have changed packages.
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;
// Ariba 8.1: Added ariba.user.core.* to import the new User and Role classes;
import ariba.user.core.*;

//81->822 changed Vector to List
public class AMSReqApproveHook implements ApprovableHook
{
    // Ariba 8.0: Changed Util.integer() to Util.getInteger()
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));

    public List retVector;
    // rlee added warning vector
    // Ariba 8.1: Replaced deprecated new List() Constructor with ListUtil.newVector();
    private List lvWarningVector = ListUtil.list();

    public List run (Approvable approvable)
    {
        if (approvable instanceof ariba.purchasing.core.Requisition)
        {
            /* CSPL-2409, Pavan Aluri, 24-Jan-2011 
             * Explanation: validate for expired/inactive lines*/
            
            retVector = isValidCatalogLine(approvable);
            if (((Integer)retVector.get(0)).intValue() == -1)
            {
               return retVector;
            }
            
        	// Validate Supplier/Location
            AMSReqSubmitHook loSubmitHook = new AMSReqSubmitHook();
            retVector = loSubmitHook.performSSLValidations(approvable);
            // CSPL-593 (Manoj) 10/03/08 Preform validation for each BSO field in Approvable
            loSubmitHook.performFieldValidation(approvable);
            if (((Integer)retVector.get(0)).intValue() == -1)
            {
               return retVector;
            }

            // first retrieve client name
            //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
            //Partitioned User to obtain the extrinsic BuysenseOrg field value.
            ariba.user.core.User RequesterUser = (ariba.user.core.User)approvable.getDottedFieldValue("Requester");
            //rgiesen dev SPL #39
            Partition appPartition = approvable.getPartition();
            //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
            ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,appPartition);
            String clientName = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.Name");
            String ApproverUser= (String) (Base.getSession().getEffectiveUser().getFieldValue("UniqueName"));

            String Change=(String)approvable.getDottedFieldValue("ChangedByUser.UniqueName");
            Log.customer.debug(" Changedby = ....%s  and the Approver is= %s",
                           Change,ApproverUser);



            /* 10/23/2000 - Rohit Angle -  Added code to check status of the requisition. If the status is denied,
            then do not allow any further Approvals by other Approvers in parallel
            */
            String reqStatus = (String)approvable.getStatusString();
            // Ariba 8.0: Replaced deprecated method
            if (reqStatus.equals("Denied"))
            {
                return ListUtil.list(Constants.getInteger(-1),
                                         "Can not Approve " +
                                         "this requisition. " +
                                         "Requisition  is already in Denied status.");
            }

            //rlee Add a call to eVA_AMSReqApproveHook to check for expired pcards
            //Anup - Generic invocation of dynamically determined Client/Agency edits. - Dev SPL #173
            List result = EditClassGenerator.invokeHookEdits(approvable, this.getClass().getName());
            // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
            int errorCode = ((Integer)result.get(0)).intValue();
            Log.customer.debug("Validations in " + this.getClass().getName() + ", error Code --> %s",errorCode);
            if(errorCode<0)
            {
                Log.customer.debug("Validations Failed in " + this.getClass().getName() + " - returning error to UI");
                return result;
            }
            //Capture Warnings here if present
            else if (errorCode > 0)
            {
                Log.customer.debug("Validations encountered a Warning in " + this.getClass().getName() + " - returning error to UI");
                //  81->822  lvWarningVector = (Vector)result.clone();
                lvWarningVector = ListUtil.cloneList(result);
            }
            //rlee ends for PCardExpired check

            //NOTE: CHANGED FIELD ON THE REQUISITION WILL BE SET TO FALSE AT THE END OF THIS HOOK.




            /* 01/16/02 - labraham - eVA ST SPL#162: Commented out the code below that checks if there is a
                                     zero dollar line item.
               03/14/02 - mberlove - Added back the following code block, and commented out the specific section
                                                             that check for the zero dollar orders.
                            */

            //Added by Rohit 10/04/2000.  Logic to check for zero dollar orders. If a requisition has zero dollar line
            //items and the approver is the last approver in the chain, then issue a error not allowing the requisition
            // to be submitted.
            boolean isApprover=false;
            boolean isActiveApprover=false;
            boolean isZeroDollarAmount=false;
            boolean isNullSupplier=false;
            boolean isNullSupplierLocation=false;
            
            boolean isFinalApproverActive =false;

            //get the information about the current user and get list of Approval Requests for the requisition
            BaseObject currUser = (BaseObject) (Base.getSession().getEffectiveUser());
            List ApprovalRequestsList= (List)approvable.getFieldValue("ApprovalRequests");

            BaseVector currUserRoles =  (BaseVector)currUser.getFieldValue("Roles");
            String currUserName  =  (String)currUser.getFieldValue("UniqueName");
            Log.customer.debug("The currUserName is %s",currUserName);

            //check if any of the final approvers are ACTIVE. If not, then do not proceed
            // Ariba 8.1: List::count() is deprecated by List::size()
            for (int i=0;i<(ApprovalRequestsList.size());i++)
            {
                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                ApprovalRequest currApprover =(ApprovalRequest)ApprovalRequestsList.get(i);

                if (currApprover.isActive())
                {
                    isFinalApproverActive = true;
                }
            }

            Log.customer.debug("final approver active flag is : %s",
                           String.valueOf(isFinalApproverActive));

            if (isFinalApproverActive)
            {
                Log.customer.debug("This is the final approvers");
                //check if the current user is an approver
                // Ariba 8.1: List::count() is deprecated by List::size()
                for (int i=0;i<(ApprovalRequestsList.size());i++)
                {
                    // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                    ApprovalRequest currApprover =(ApprovalRequest)ApprovalRequestsList.get(i);

                    // Ariba 8.1: Modified logic to get the Approver and determine the Type (Role or User) in order to
                    // obtain the Approver UniqueName.
                    Approver appr =  currApprover.getApprover();
                    String apprUniqueName = null;
                    if (appr instanceof User) {
                        ariba.user.core.User user = (ariba.user.core.User)appr;
                        apprUniqueName = user.getUniqueName();
                    }
                    else if (appr instanceof Role) {
                        ariba.user.core.Role role = (ariba.user.core.Role)appr;
                        apprUniqueName = role.getUniqueName();
                    }
                    else{
                        Log.customer.debug("The Approver is not of type User or Role");
                    }

                    Log.customer.debug("The Approver is %s", apprUniqueName);

                    //Check if the unique name of the user matches that of the Approver Object
                    if(currUserName.equals(apprUniqueName))
                    {
                        Log.customer.debug("Inside if for currUserName");
                        isApprover=true;
                    }

                    //Since the approver could be a role - Check if any of the roles of the current user match the Approver role
                    if (currUserRoles !=null)
                    {
                        Log.customer.debug("Inside if for currUserRoles NOT NULL");

                        for (int l=0;l<currUserRoles.size();l++)
                        {
                            Log.customer.debug("Inside FOR for currUserRoles");
                            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                            Log.customer.debug("currUserRole object Before cast is: %s",currUserRoles.get(l));

                            //FOR SOME REASON -  The lements of tje Roles vector are stored as BaseID s and not as BaseObjects
                            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                            ariba.base.core.BaseId currUserRole = (ariba.base.core.BaseId)currUserRoles.get(l);

                            Log.customer.debug("currUserRole object is: %s",
                                           currUserRole);
                            Log.customer.debug("curr approver : %s",
                                           currApprover.getApprover());
                            Log.customer.debug("curr approver  base id  : %s",
                                           currApprover.getApprover().getBaseId());

                            if (currUserRole.equals(currApprover.getApprover().getBaseId()) )
                            {
                                Log.customer.debug("Inside IF for currUserRole==currApprover");

                                isApprover = true;
                            }
                        }
                    }

                    if (isApprover && currApprover.isActive())
                    {
                        isActiveApprover = true;
                    }
                }


                // get the vector of line items on the requisition
                List reqLineItems = (List)approvable.getFieldValue("LineItems");

                //check for zero dollar line items
                //Also, Check for NULL supplier and Supplier Location on the line items
                // Ariba 8.1: List::count() is deprecated by List::size()
                for (int i=0;i<(reqLineItems.size());i++)
                {

                    // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                    BaseObject eachLine = (ariba.approvable.core.LineItem) reqLineItems.get(i);
                                /* Commented out this if statement to remove the requirement for a non-zero line item
                                        BigDecimal liAmount = (BigDecimal)eachLine.getDottedFieldValue("Amount.Amount");
                        if (liAmount.doubleValue() == 0.00) {
                            isZeroDollarAmount = true;
                        }

                    */
                    if (eachLine.getDottedFieldValue("Supplier") == null)
                    {
                        isNullSupplier = true;
                    }
                    if (eachLine.getDottedFieldValue("SupplierLocation") == null)
                    {
                        isNullSupplierLocation = true;
                    }
                }



                //Check if the current approver is one of the final approvers and there is zero dollar line item in the req.
                // Ariba 8.0: Replaced deprecated method
                if (isActiveApprover
                        && isZeroDollarAmount)
                {
                    return ListUtil.list(Constants.getInteger(-1),
                                             "Can not Approve " +
                                             "this requisition. " +
                                             "It contains at least one" +
                                             " zero dollar line item.");
                }

                //Check if the current approver is the last approver and there is NULL supplier in the req.

                else if (isActiveApprover
                        && isNullSupplier  )
                {
                    return ListUtil.list(Constants.getInteger(-1),
                                             "Cannot apply final " +
                                             "approval to this requisition. " +
                                             "It contains at least one" +
                                             "line item with no supplier.");
                }

                //Check if the current approver is the last approver and there is NULL supplier location in the req.

                else if (isActiveApprover
                        && isNullSupplierLocation  )
                {
                    return ListUtil.list(Constants.getInteger(-1),
                                             "Can not Approve " +
                                             "this requisition. " +
                                             "It contains at least one" +
                                             "line item with no Supplier Location (Contact).");
                }

            }

        }
        else
        {

            // pb 051700 I dont think we need this
            /* return ListUtil.vector(Constants.getInteger(-1),
                           "Can not do Approve " +
                           "on the requisition. " +
                           "because, the requisition is " +
                           "in Use by other user. ");*/

        }// end of if requisition


        // ROHIT 10/25/2000 - Set the changed flag on the requisition to be false at this point.
        approvable.setFieldValue("Changed",new Boolean(false));
        Log.customer.debug("Set the Changed Flag to be : %s",
                       approvable.getFieldValue("Changed"));

        /*Added by srini for CSPL-1033
         * In existing code we are capturing the warning messages but nowhere we are returing them
         */
        if ( ! lvWarningVector.isEmpty() )
        {
            return lvWarningVector;
        }
        return NoErrorResult;
    }
    public static List isValidCatalogLine(Approvable approvable)
    {
		/**
		 * Added by Partho for CSPL-3849(New 9r1 functionality to require approvers to have same catalog access as Req Preparers)
		 * This method is added to modify the isValidCatalogLine in AMSReqSubmitHook
		 * to make the currUser as the Preparer so as to by pass the custom catalog filter validation for Req Approve hook.
		 */

        {
            Requisition req = (Requisition) approvable;
            List reqLines = req.getLineItems();
            StringBuffer sb = new StringBuffer();
            String slines = " ";
            for (int i = 0; i < reqLines.size(); i++)
            {
                Log.customer.debug("AMSReqApproveHook - Line: %s validation", i);
                ReqLineItem reqLine = (ReqLineItem) reqLines.get(i);
                if (reqLine.getOldValues() == null)
                {
                    Log.customer.debug("AMSReqApproveHook - Line: %s is not changed order line ", i);
                    label1:
                    {
                        ariba.base.core.Partition partition = reqLine.getPartition();
                        ariba.common.core.User currUsr = ariba.common.core.User.getPartitionedUser(req.getPreparer(),   partition);
                        if (reqLine.getIsAdHoc() || reqLine.isPunchOut())
                            break label1;
                        ariba.catalog.base.CatalogItemRef cir = reqLine.getDescription().getCatalogItemRef();
                        if (cir == null)
                            break label1;
                       ariba.catalog.base.search.ResultRow rr = Procure.getService().getResultRowForUser(partition,
                                currUsr, req.getBaseId(), cir);
                        Log.customer.debug("AMSReqApproveHook - rr: %s: ", rr);
                        if (rr != null)
                            break label1;
                        sb.append(i+1);
                        sb.append(",");
                    }
                }
            }
            slines = sb.toString();
            Log.customer.debug("AMSReqApproveHook - slines:%s: ", slines);
            if (!StringUtil.nullOrEmptyOrBlankString(slines))
            {
                String error = ResourceService.getString("ariba.procure.core", "InvalidCatalogItemMsg");
                return ListUtil.list(Constants.getInteger(-1), "At line(s) " + slines.substring(0, slines.length() - 1)
                        + " - " + error);
            }
            else
            {
                return ListUtil.list(Constants.getInteger(0));
            }
        }
    }
}
