/*
    Copyright (c) 1996-1999 Ariba, Inc.
    All rights reserved. Patents pending.

   Falahyar March 2000
*/

// 08/07/2003: Updates for Ariba 8.0 (David Chamberlain)
/* 09/02/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

//import ariba.common.base.Variant;
import ariba.approvable.core.Approvable;
// Ariba 8.0: Replaced deprecated ApprovableHook 
//import ariba.approvable.core.ApprovableHook;
import ariba.approvable.core.ApprovableHook;
import ariba.util.log.Log;
// Ariba 8.1: ariba.util.core.Util has been deprecated and removed
//import ariba.util.core.Util;
import java.util.List;
// Ariba 8.0: Deprecations, methods have changed packages.
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;

public class AMSPaymentApproveHook implements ApprovableHook
{
    // Ariba 8.0: Replaced Util.integer() with Constants.getInteger()
    // Ariba 8.0: Replaced Util.List() with ListUtil.vector() 
    // 81->822 changed Vector to List
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0)); 
    
    public List retVector;
    
    public List run (Approvable approvable)
    {
        Log.customer.debug("Calling AMSPaymentApproveHook");
        if (approvable.instanceOf("ariba.core.PaymentEform"))
        {
            
            String paymentStatus = (String)approvable.getStatusString();
            // Ariba 8.0: Replaced Util.integer() with Util.getInteger
            // Ariba 8.0: Replaced Util.getInteger with Constants.getInteger() 
            if (paymentStatus.equals("Denied")) 
            {
                // Ariba 8.0: Replace Util.vector with ListUtil.vector 
                return ListUtil.list(Constants.getInteger(-1),
                                         "Can not Approve " +
                                         "this Payment. " +  
                                         "Payment is already in Denied Status"); 
            }
            
            //Anup - Generic invocation of dynamically determined Client/Agency edits. - Dev SPL #173 
            List result = EditClassGenerator.invokeHookEdits(approvable,
                                                               this.getClass().getName());
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            int errorCode = ((Integer)result.get(0)).intValue();
            Log.customer.debug("Validations in " + this.getClass().getName() + ", error Code --> %s",errorCode); 
            if(errorCode<0)
            {
                Log.customer.debug("Validations Failed in " + this.getClass().getName() + " - returning error to UI"); 
                return result;
            }
            
            return NoErrorResult;
        }
        
        Log.customer.debug("Done with AMSPaymentApproveHook");
        return NoErrorResult;
    }
}
