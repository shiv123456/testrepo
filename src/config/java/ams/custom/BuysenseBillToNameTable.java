// Decompiled by Decafe PRO - Java Decompiler
// Classes: 1   Methods: 4   Fields: 1

/*
/* 08/11/2003: Updates for Ariba 8.0 (Jeff Namadan) */
/* 09/05/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */
/*
*  rlee, 5/22/2004 ST SPL 1321 - VITA: 2621 - Bill To Address not displayed
*  rlee, 5/25/2004 ST SPL 1334 - VITA:Bill To list incomplete for E136 approver
*        Use LineItem.ClientName instead of EffectiveUser's ClientName to
*        check if Requester's clientName belongs to VITA. If yes, do not
*        getVitaAddress Billing Address to avoid getting duplicates,
*        since matchPattern is already returning all the Requester's billing addresses.
*  rlee, 5/27/2004 ST SPL 1344 - VITA:E136 Bill To addresses not in BSO
*  rlee, 6/28/2004 Prod SPL 390 VITA Billing Address not displaying in Prod
*                  Mod around line 200 to get the Clientid using a more generic approach rather than hard code to return a
*                  substring of 17-25.
*/
package config.java.ams.custom;

import ariba.base.fields.*;
import ariba.common.core.Address;
/* Ariba 8.0: Replaced ariba.util.core.Util */
import ariba.util.core.ListUtil;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;
import java.io.*;
import java.util.Locale;
/* Ariba 8.0: Replaced ariba.common.core.nametable.NamedObjectNameTable */
import ariba.base.core.aql.AQLNameTable;
import java.util.List;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLResultCollection;
import ariba.common.core.*;
// Ariba 8.1: Enumeration is not being used in this module
//import ariba.util.core.Enumeration;
import ariba.base.core.ClusterRoot;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

//import ariba.util.log.Log;
// Referenced classes of package ariba.common.core.nametable:
//            NamedObjectNameTable

/* Ariba 8.0: Replaced NamedObjectNameTable */
//81->822 changed Vector to List
//81->822 changed Hashtable to Map
public class BuysenseBillToNameTable extends AQLNameTable
{

    // eProcurement Dev SPL# 273, and 286 VITA: Used to build the VITA query.
    private String selectColumns;

    public static final String ClassName = "ariba.common.core.nametable.AddressNameTable";
    String lsValueSourceClientName = null;

    public BuysenseBillToNameTable()
    {
        Log.customer.debug("In constructor for BuysenseAddressNameTable");
    }

    /* Ariba 8.0: Removed deprecated getTitleForObject(Named object, Locale locale) method */
    public String getTitleForObject(ValueSource object, Locale locale)
    {Log.customer.debug("In getTitleForObject- valuesource");
        String title = super.getTitleForObject(object, locale);
        if((object instanceof Address) && StringUtil.nullOrEmptyOrBlankString(title))
        {
        Log.customer.debug("In getTitleForObject- valuesource TITLE:%s",getDerivedTitle((Address)object));
            return getDerivedTitle((Address)object);
        }
        else
        {
        Log.customer.debug("In getTitleForObject- valuesource TITLE:%s",title);
            return title;
        }
    }


    private String getDerivedTitle(Address address)
    {
        if(address == null)
            return null;
        String name = address.getName();
        /* Ariba 8.0: Replaced Util.nullOrEmptyOrBlankString */
        if(!StringUtil.nullOrEmptyOrBlankString(name))        
            return name;
        String lines = address.getLines();
        /* Ariba 8.0: Replaced Util.nullOrEmptyOrBlankString */
        if(!StringUtil.nullOrEmptyOrBlankString(lines))        
        {
            BufferedReader reader = new BufferedReader(new StringReader(lines));
            if(reader != null)
                try
                {
                    name = reader.readLine();
                }
                catch(IOException _ex)
                {
//                    Log.constraints.warning(4000, lines);
                }
            else
//                Log.constraints.warning(4000, lines);
            /* Ariba 8.0: Replaced Util.nullOrEmptyOrBlankString */
            if(!StringUtil.nullOrEmptyOrBlankString(name))
                return name;
        }
        return address.getCity();
    }
//*************************************************************************************
    public List matchPattern (String field, String pattern) {
        Log.customer.debug("Hello from BUYSENSEADDRESS NAME TABLE CLASS 1");
        List lvVitaAddr = ListUtil.list();
        List results = ListUtil.list();
        lvVitaAddr = null;

        try
        {
            // eProcurement Dev SPL# 273, and 286 VITA: Append the VITA addresses to the regular addresses
            results = super.matchPattern(field, pattern);
            lvVitaAddr = getVitaAddresses(field, pattern);
          //jackie - need to ask Ariba consultant or in house ariba experts. is this correct usage?
          // 81 -> 822
          // if( (results.lastElement()).equals((lvVitaAddr.lastElement())))

            if (!ListUtil.nullOrEmptyList(lvVitaAddr) && !ListUtil.nullOrEmptyList(results)) 
            {
                if( (ListUtil.lastElement(results)).equals(ListUtil.lastElement(lvVitaAddr)))
                {
                     Log.customer.debug("Vita Address has already been added. Not adding it again !!");
                }
                else
                {
                     results.add(lvVitaAddr);
                }
            }
        }
        catch ( Exception loExcep )
        {
            Log.customer.debug( "Exception while getting Vita Addresses" ) ;
        }
        return results;
    }

    public List getVitaAddresses(String field, String pattern)
    {
        AQLQuery loQuery;
        List queryResults = ListUtil.list();
        List finalResults = ListUtil.list();
        AQLResultCollection resultCollection;
        ClusterRoot SRAClient = null;
        BaseId loBaseId = null;
        Map loParamHashtable = new HashMap();
        String lsQuery = "";
        String lsClientUniqName = null;
        String sraParm = null;
        AQLOptions loOptions = new AQLOptions( Base.getSession().getPartition(), false ) ;

        // eProcurement Dev SPL# 273, and 286 VITA: This method added the retrieve VITA addresses
        Log.customer.debug("***In BuysenseBillToNameTable::getVitaAddresses()");
        try
        {
            // Get the SolicitationRoutingAgency from Parameters.table
            sraParm = Base.getService().getParameter(Base.getSession().getPartition(),"Application.AMSParameters.SolicitationRoutingAgency");
            if(sraParm == null)
            {
                Log.customer.debug(" Parameters table has no SolicitationRoutingAgency");
            }
            else
            {
                StringTokenizer st = new StringTokenizer(sraParm, ",");

                // Loop through all the clients found in the SRA parm from Parameters.table
                // querying for the addresses belonging to them.
                while (st.hasMoreTokens())
                {
                    lsClientUniqName = null;
                    // Get the Client object matching this sra entry
                    SRAClient = (ClusterRoot)Base.getService().objectMatchingUniqueName("ariba.core.BuysenseClient", Base.getSession().getPartition(), st.nextToken());
                    if (SRAClient != null)
                    {
                        lsClientUniqName = (String) SRAClient.getDottedFieldValue("UniqueName");
                    }
                    // if SRA clientname same as ValueSourceClientName e.g. BuysenseOrgEform or Requester's clientname,
                    // skip getting SRA address to avoid duplicates.
                    if((lsClientUniqName.trim()).equals(lsValueSourceClientName.trim()))
                    {
                        Log.customer.debug("This Line Item belongs to this Solicitation Routing Agency; skip getting own SRA Address");
                    }
                    else
                    {
                        // rlee add check if match not found
                        if (SRAClient == null)
                        {
                            Log.customer.debug("No SRAClient found that would match Parameters table.");
                        }
                        else
                        {
                            loBaseId = SRAClient.getBaseId() ;
                            loParamHashtable.put("BuysenseClientBaseId", loBaseId) ;
                            loOptions = buildOptions(field, pattern);
                            loOptions.setActualParameters( loParamHashtable ) ;                            

                            // Construct the Query string
                            lsQuery = "SELECT distinct " + 
                                      selectColumns + 
                                      " FROM ariba.common.core.Address AS Address PARTITION pcsv SUBCLASS NONE " +
                                      "WHERE Address.Creator IS NULL AND " +
                                      "Address.ClientName = :BuysenseClientBaseId AND " +
                                      "Address.ShipToAddressType = 'B' " +
                                      "ORDER BY Address.Name ASC" ;                             

                            // Parse the query into an aql object
                            loQuery = AQLQuery.parseQuery( lsQuery );

                            // Execute the Query get the results vector
                            //Base.getService().executeQuery(query, options);
                            resultCollection = Base.getService().executeQuery( loQuery, loOptions );

                            queryResults = resultCollection.getRawResults();

                            // Append the vector result from the query for this client onto the return vector
                            finalResults.add(queryResults);
                        }//else SRAClient is not null
                    }//if User belongs to SRAgency
                }//while
            }// if SRA is null
        }//try
        catch ( Exception loExcep )
        {
            Log.customer.debug( "Exception while getting Vita Addresses" ) ;
        }

        return finalResults;
    }



    public void addQueryConstraints (AQLQuery query, String field, String pattern) {
        Log.customer.debug("Hello from BUYSENSEADDRESS NAME TABLE CLASS 2");

        // build the default set of field constraints for the pattern
        super.addQueryConstraints(query, field, pattern);
        beginSystemQueryConstraints(query);

        // eProcurement Dev SPL# 273, and 286 VITA:
        // When the inital query is created, the columns to be selected are pre-set to correspond
        // to those in the aml definition. We need those in the getVitaAddresses() method to create
        // the VITA aql statement. Thus, here they are put into a memeber variable to be used by
        // getVitaAddresses() downstream.
        String lsQuery = query.toString();
        selectColumns = lsQuery.substring(lsQuery.lastIndexOf("SELECT") + 6, lsQuery.indexOf("FROM") - 1);

       // add the constraints
         String addressType = "B";
         //  ST SPL #9 - Added filter by Agency
         BaseObject clientName = (BaseObject)clientName();
         if (clientName != null) {
            lsValueSourceClientName = (String) clientName.getDottedFieldValue("UniqueName");
            BaseId id = clientName.getBaseId();
            Log.customer.debug("Hello Washington -  id is %s",id);
            // Create the AQL based on the ClientName that was returned
            AQLCondition clientNameCond =
            AQLCondition.buildEqual(query.buildField("ClientName"),id);
            query.and(clientNameCond);
         }

        AQLCondition addressTypeCond = AQLCondition.buildEqual(query.buildField("ShipToAddressType"),addressType);
        query.and(addressTypeCond);
        endSystemQueryConstraints(query);
    }

    /* Ariba 8.0: NamedObjectNameTable was deprecated by AQLNameTable.
       This class was changed to derive from AQLNameTable and the
       method constraints() is not in this class and isn't needed.
       public List constraints (String field, String pattern) {
           Log.customer.debug("Hello from BuysenseAddress NAME TABLE CLASS 3");
           List constraints = super.constraints(field, pattern);
           return constraints;
       } */

    private ValueSource clientName() {
        ValueSource context = getValueSourceContext();
        Log.customer.debug("Washington - This is the ValueSource Context:%s",context);
        if (context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.BuysenseUserProfileRequest"))
        {
            Log.customer.debug("Getting client name where context is BuysenseUserProfileRequest");
            
            //ariba.user.core.User RequesterUser = (ariba.user.core.User)context.getDottedFieldValue("Preparer");
            //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
            //Log.customer.debug("Preparer's Agency is"+(ValueSource)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName"));
            //return (ValueSource)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName");
            return (ValueSource) context.getDottedFieldValue("Client");
        }
        else if (context instanceof UserProfile) {
            return (ValueSource)context.getFieldValue("UserProfileDetails.ClientName");
        }
        else if (context instanceof UserProfileDetails) {
            return (ValueSource)context.getFieldValue("ClientName");
        }
        else if (context instanceof ariba.purchasing.core.ReqLineItem) {
            return (ValueSource)context.getFieldValue("ClientName");
        }
        else if (context instanceof ariba.approvable.core.DynamicApprovable) {
            return (ValueSource)context.getFieldValue("ClientName");
        }
        else
            return null;
    }

}
