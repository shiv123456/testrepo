package config.java.ams.custom;

import ariba.base.fields.Condition;
import ariba.base.fields.ConditionResult;
import ariba.purchasing.core.ReqLineItem;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
/**
* @author Manoj Gaur
* @version 1 DEV
* @reference CSPL-395
* @see Date 08-12-2008
* @Explanation   Validity condtion for commodity field showing different message depends on PunchOut and non PunchOut objects.
* 
*/

public class CommodityValidity extends Condition
{
    String ERROR_MESSAGE = null;
    int DELETEDSTATE = 8;
	
    public boolean evaluate(Object value, PropertyTable params)
    {
       return evaluateImplementation(value, params);
    }
    private boolean evaluateImplementation(Object value, PropertyTable params)
    {
        ReqLineItem objReqLI;
        int liChangedState = 0;
        if(value instanceof ReqLineItem)
        {
            objReqLI = (ReqLineItem) value;
            liChangedState = Integer.parseInt((objReqLI.getFieldValue("ChangedState")).toString());

            // Only validate active line items
            if( liChangedState != DELETEDSTATE )
            {
                // CSPL-395 error message: punchout commoditycode should not be null
                if(objReqLI.getFieldValue("CommodityCode")==null)
                {
                    if(objReqLI.getFieldValue("PunchOut")!=null)
                    {
                        ERROR_MESSAGE = "Please update line item with commodity code description";
                        return false;
                    }
                    else
                    {
                        ERROR_MESSAGE = "Value is not acceptable";
                        return false;					
                    }
                }
                else if(!((Boolean)objReqLI.getDottedFieldValue("CommodityCode.Active")).booleanValue())
                {
                    // CSPL-1944 Prevent submit or approval of Req with inactive commodity code
                    ERROR_MESSAGE = "The Commodity Code is no longer valid. Please select a new value.";
                    return false;		
                }
            }
        }
        return true;
    }
	public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
	{
		Log.customer.debug(this.getClass().getName() + ":evaluateAndExplain" + " - start");
		// if evaluation fails display corresponding error message
             if (!evaluateImplementation(value, params))
             {
			if (ERROR_MESSAGE != null)
			{
			    return new ConditionResult (ERROR_MESSAGE);
			}
		}
		return null;
	}
}
