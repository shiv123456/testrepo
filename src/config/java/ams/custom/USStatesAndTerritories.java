/*
 * @(#)USStatesAndTerritories.java      1.0 2006/04/13
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information"). You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *
 *
 * PVCS Modification :
 *
 * $Log:   R:\Software\development\archives\Ariba 8.x Customizations\app\buyer\Server\classes\extensions\config\java\ams\custom\USStatesAndTerritories.java-arc  $
 * 
 *    Rev 1.0   05 May 2006 06:24:18   cm
 * Initial revision.
 *
 */

package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.util.log.Log;
import java.util.Map;
import ariba.util.core.MapUtil;
import java.util.List;
import ariba.util.core.ListUtil;

//81->822 changed Vector to List
//81->822 changed Hashtable to Map
public class USStatesAndTerritories
{
    static Map mhtUSStatesAndTerritories = MapUtil.map() ;
    static List mvUSStatesAndTerritories = ListUtil.list() ;
    //static Partition moPartition = Base.getService().getPartition() ;
    static Partition moPartition = Base.getSession().getPartition() ;
    static String msFileName = Base.getService().getParameter( moPartition,
                                                               "Application.AMSParameters.USStatesAndTerritories" ) ;

    /**
     * Static initializer for <code>USStatesAndTerritories</code>.
     * The static initialization block is necessary to invoke the init() method.
     */
    static
    {
        init() ;
    }

    /**
     * <code>init</code> allows initialization/re-initialization of <code>USStatesAndTerritories.
     * It is not public as system startup is the only thing that should be initializing
     * this utility class. 
     */
    private static synchronized void init()
    {
        if ( mhtUSStatesAndTerritories.isEmpty() )
            mvUSStatesAndTerritories = readUSStatesAndTerritoriesFile();
            mhtUSStatesAndTerritories = MapUtil.convertListToMap( mvUSStatesAndTerritories ) ;
            Log.customer.debug("Map size %s ", mhtUSStatesAndTerritories.size());
            Log.customer.debug("Map string %s ", mhtUSStatesAndTerritories.toString());
    }

    /**
        This method is used to read the state and territories abbreviations
        and return a List which is used to set a class variable
    */
    public static List readUSStatesAndTerritoriesFile()
    {
        Log.customer.debug("readUSStatesAndTerritoriesFile() was called...");
        Log.customer.debug("States file name : %s",msFileName);

        // Call the CSV reader utility class to get the list of states
        CSVReader mCSVR = new CSVReader();
        // jackie 81->822  Vector lvUSStatesAndTerritories = VectorUtil.newVector( mCSVR.readCSV( msFileName ) );
        List lvUSStatesAndTerritories = ListUtil.collectionToList( mCSVR.readCSV( msFileName ) );
        List lvStateCodes = ListUtil.list() ;

        /**
            Remove the header of the CSV file and thus start at 1-st not 0-th position.
            Since CSV file structure is StateCode, StateName, and only the StateCode
            is necessary, secondary List is necessary to remove the first element i.e.
            which is used for creation of the Hastable
        */
        
        for (int i = 1; i < lvUSStatesAndTerritories.size(); i++)
        {
            // jackei 81->822     Vector lvStateCodeAndStateName = VectorUtil.newVector( ( java.util.Vector ) lvUSStatesAndTerritories.get( i ) ) ;
            List lvStateCodeAndStateName = ListUtil.collectionToList( ( java.util.List ) lvUSStatesAndTerritories.get( i ) ) ;
            String lsStateCode = ( String )lvStateCodeAndStateName.get( 0 ) ;
            ListUtil.addElementIfAbsent( lvStateCodes, lsStateCode ) ;
        }
        return lvStateCodes ;
    }

    /**
        This method is used to validate state and territories abbreviations
        It is called in static fashion by consumer classes
    */
    public static boolean isValidUSStatesAndTerritoriesCode( String lsUSStatesAndTerritoriesCode )
    {
        Log.customer.debug("isValidUSStatesAndTerritoriesCode() was called passing: %s", lsUSStatesAndTerritoriesCode);
        boolean lbValidCode = mhtUSStatesAndTerritories.containsKey( lsUSStatesAndTerritoriesCode ) ;
        Log.customer.debug("Is code on Map: %s", lbValidCode+"");
        return mhtUSStatesAndTerritories.containsKey( lsUSStatesAndTerritoriesCode );
    }

    /**
        This method is used to validate state and territories name
        It is called in static fashion by consumer classes
        Not used at this point
    */
    public static boolean isValidUSStatesAndTerritoriesName( String lsUSStatesAndTerritoriesName )
    {
        Log.customer.debug("isValidUSStatesAndTerritoriesName() was called passing: %s", lsUSStatesAndTerritoriesName);
        return mvUSStatesAndTerritories.contains( lsUSStatesAndTerritoriesName );
    }
}
