package config.java.ams.custom;

import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import ariba.approvable.core.Approvable;
import ariba.approvable.core.Comment;
import ariba.approvable.core.PrintApprovableHook;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseSession;
import ariba.base.core.LongString;
import ariba.approvable.core.ApprovalRequest;
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;
import ariba.util.net.HTMLPrintWriter;

public class BuysenseVMIAssetTrackingPrintHook implements PrintApprovableHook
{
    private static final List NoErrorResult = ListUtil.list(Constants.getInteger(NoError));
    Hashtable hHdrParams = new Hashtable();
        
    public List run(Approvable approvable, PrintWriter out, Locale locale, boolean printForEmail)
    {            
         HTMLPrintWriter loHout = new HTMLPrintWriter(out);
         String lsHtmlPage=null;
         String lsReqType=approvable.getTypeName();
         if (printForEmail)
         {
            Log.customer.debug("PrintForEmail is true");
            return NoErrorResult;
         }
         try
         {           
        	//Checking if the approvable is "ariba.core.BuysenseVMIAssetTracking" 
            if (lsReqType.equals("ariba.core.BuysenseVMIAssetTracking"))
            {    
               int liTag = 0, liTagEnd = -1;
               String lsTag = null;
                 lsHtmlPage = PSGFunctions.ReadFile("config/htmlui/resource/ariba.core.BuysenseVMIAssetTracking.html");
                 
                 //Figuring which sections to include in the final HTML
                 lsHtmlPage = getFinalHTML(lsHtmlPage,approvable);
                 
                 //Figuring which lines to include in the final HTML
                 lsHtmlPage = getFinalHTMLLines(lsHtmlPage,approvable);
                 
                 //Getting the field values
                 hHdrParams = getFieldValues(approvable,lsHtmlPage); 
                 
                 //Replacing the tags with the field values 
                 lsHtmlPage = PSGFunctions.ReplaceHtml(lsHtmlPage, hHdrParams); 

                 loHout.println(lsHtmlPage);
          }
            
        }
        catch (Exception e) {
            Log.customer.debug(e);
          
          }
         return NoErrorResult;
     }
    


	private Hashtable getFieldValues(Approvable approvable,String lsHtmlPage)
    {
    	int liTag = 0, liTagEnd = -1;
    	String lsTag = null;
        liTag = lsHtmlPage.lastIndexOf("<!--Hdr.");
        liTagEnd = -1;
        Object loFieldVal=null;
        while (liTag >= 0)
        {
          liTagEnd = lsHtmlPage.indexOf("-->", liTag + 1);
          lsTag = lsHtmlPage.substring(liTag + 4, liTagEnd);
          Log.customer.debug("HdrKey: " + lsTag);
          hHdrParams.put(lsTag, "[?]");
          liTag = lsHtmlPage.lastIndexOf("<!--Hdr.", liTag - 1);
        }
        Enumeration loEnumParams = hHdrParams.keys();
        while (loEnumParams.hasMoreElements())
        {                  
            lsTag = loEnumParams.nextElement().toString();
            String sFieldName = lsTag.substring(4);

                 if(sFieldName.equals("username"))
                 {
              	   Log.customer.debug("sFieldName is : "+sFieldName);  
                   String loStatus=(String)approvable.getDottedFieldValue("StatusString"); 
                   Log.customer.debug("status is : "+loStatus);
                   if(loStatus.equals("Denied"))
                   {
                      loFieldVal="Denied by:";
                   }
                   else
                   {
                     loFieldVal="Approved by:";                                         
                   }                                           
                 }
           else if(sFieldName.equals("date"))
                 {
          	       Log.customer.debug("sFieldName is : "+sFieldName);
                   String loStatus=(String)approvable.getDottedFieldValue("StatusString"); 
                   if(loStatus.equals("Denied"))
                   {
                     loFieldVal="Denied Date:";
                   }
                  else
                  {
                     loFieldVal="Approved Date:";                                              
                  }                                               
                }
           else if((sFieldName.contains("Preparer")||sFieldName.contains("Requester"))&& (!sFieldName.equalsIgnoreCase("RequesterPhone")))
               {
          	      Log.customer.debug("The file is either Preparer or Requester : "+sFieldName);
                  String lsObjval=sFieldName.substring(0,sFieldName.indexOf("@"));
                  Log.customer.debug("Object value: "+lsObjval);
                  String lsFieldVal=sFieldName.substring(sFieldName.indexOf("@")+1);
                  Log.customer.debug("field value: "+lsFieldVal);
                  loFieldVal=getReqValue(lsObjval,lsFieldVal,approvable);
                  Log.customer.debug("field value for preparer or requester is: "+loFieldVal);
               }
           else if(sFieldName.contains("$"))
           {   
          	   Log.customer.debug("The field name is : "+sFieldName);
               int liFirstIndex=sFieldName.indexOf('$');
               int liLastIndex=sFieldName.indexOf('$', liFirstIndex+1);
               String lsFieldName=sFieldName.substring(liLastIndex+2);  
               String lsStringObj=sFieldName.substring(liFirstIndex+1,liLastIndex);                      
               loFieldVal = getFieldValue(lsFieldName,lsStringObj,approvable);
               Log.customer.debug("field value "+loFieldVal);
           }
               else 
               {   
              	  Log.customer.debug("converting boolean true false to yes and no,sFieldName is :"+sFieldName); 
                  loFieldVal = approvable.getDottedFieldValue(sFieldName);                        
                  if((sFieldName.equals("ItemITrelated1") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                  		(sFieldName.equals("ItemITrelated2") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                  		(sFieldName.equals("ItemITrelated3") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                  		(sFieldName.equals("ItemITrelated4") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                  		(sFieldName.equals("ItemITrelated5") && loFieldVal != null && loFieldVal.toString().trim().equals("true")))
                  {
                  	Log.customer.debug("converting boolean true to yes for field name :"+sFieldName);	
                    loFieldVal="Yes";
                  }
                  else if((sFieldName.equals("Sub3checkbox1") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                  		(sFieldName.equals("Sub3checkbox2") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                  		(sFieldName.equals("Sub3checkbox3") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                  		(sFieldName.equals("Sub3checkbox4") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                  		(sFieldName.equals("Sub3checkbox5") && loFieldVal != null && loFieldVal.toString().trim().equals("true")))
                  {
                  	  Log.customer.debug("converting boolean true to yes for field name :"+sFieldName);	
                      loFieldVal="Yes";                       	
                  }
                  	
                  else if((sFieldName.equals("ItemITrelated1") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                  		(sFieldName.equals("ItemITrelated2") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                  		(sFieldName.equals("ItemITrelated3") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                  		(sFieldName.equals("ItemITrelated4") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                  		(sFieldName.equals("ItemITrelated5") && loFieldVal != null && loFieldVal.toString().trim().equals("false")))
                  {
                  	 Log.customer.debug("converting boolean false to no for field name :"+sFieldName);	
                     loFieldVal="No";
                  }
                  else if((sFieldName.equals("Sub3checkbox1") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                  		(sFieldName.equals("Sub3checkbox2") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                  		(sFieldName.equals("Sub3checkbox3") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                  		(sFieldName.equals("Sub3checkbox4") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                  		(sFieldName.equals("Sub3checkbox5") && loFieldVal != null && loFieldVal.toString().trim().equals("false")))
                  {
                  	  Log.customer.debug("converting boolean false to no for field name :"+sFieldName);	
                      loFieldVal="No";                       	
                  }                        
               }
                                
          if (loFieldVal == null)
          {
             loFieldVal = "";
          }              
           hHdrParams.put(lsTag, String.valueOf(loFieldVal));
       }
		return hHdrParams;

    }
    private String getFinalHTMLLines(String lsHtmlPage, Approvable approvable) 
    {
    	String lsHtmlPagenew = null;
    	Approvable loAppr = approvable;
		int n = lsHtmlPage.length();
		Boolean lbLine1 = false;
		Boolean lbLine2 = false;
		Boolean lbLine3 = false;
		Boolean lbLine4 = false;
		Boolean lbLine5 = false;
		int counter = 0;
		String lsCounter = null;
		String part2 = null;
		Log.customer.debug("The total length of the print string is "+n);
		Log.customer.debug("Splitting it into 3 parts");
    	String part1 =  lsHtmlPage.substring(0, lsHtmlPage.indexOf("<!--Line0E-->"));
    	
    	String iterablepart = lsHtmlPage.substring((lsHtmlPage.indexOf("<!--Line1-->")),lsHtmlPage.indexOf("<!--Line1E-->"));
    	
    	String part3 = lsHtmlPage.substring((lsHtmlPage.indexOf("<!--Line1E-->")),n);
    	
    	if((loAppr.getDottedFieldValue("ItemITrelated1") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated1")).booleanValue())||(!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemVMIIDNum1"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemDescription1"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemModelNum1"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemSerialNum1"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemReason1"))))
    	{
    		Log.customer.debug("Line 1 is no empty");
    		lbLine1 = true;
    		part2 = iterablepart;
    		counter = 1;
    		part2 = replaceStrings(part2,"1","1",counter);
    		
    	}
    	if((loAppr.getDottedFieldValue("ItemITrelated2") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated2")).booleanValue())||(!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemVMIIDNum2"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemDescription2"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemModelNum2"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemSerialNum2"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemReason2"))))
    	{
    		Log.customer.debug("Line 2 is non empty");
    		String lsinterim = null;
    		lbLine2 = true;
    		if(lbLine1)
    		{
    			Log.customer.debug("Line 1 & 2 are non empty");
    			counter = 2;
    			lsinterim = iterablepart;
    			lsinterim = replaceStrings(lsinterim,"2","2",counter);
    			part2 = part2.concat(lsinterim);
    		}
    		else
    		{
    			Log.customer.debug("Only Line 2 is non empty");
    			counter = 1;
    			part2 = iterablepart;
    			part2 = replaceStrings(part2,"1","2",counter);
    		}
    		
    	} 
    	if((loAppr.getDottedFieldValue("ItemITrelated3") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated3")).booleanValue())||(!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemVMIIDNum3"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemDescription3"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemModelNum3"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemSerialNum3"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemReason3"))))
    	{
    		
    		String lsinterim = null;
    		lbLine3 = true;
    		if(lbLine1 && lbLine2)
    		{
    			Log.customer.debug("Line 1 , 2 & 3 are non empty");
    			counter = 3;
    			lsinterim = iterablepart;
    			lsinterim = replaceStrings(lsinterim,"3","3",counter);
    			part2 = part2.concat(lsinterim);
    		}
    		else if(lbLine1 || lbLine2)
    		{
    			Log.customer.debug("Either Line 1 or 2 & 3 is non empty");
    			lsinterim = iterablepart;
    			counter = 2;
    			lsinterim = replaceStrings(lsinterim,"2","3",counter);
    			part2 = part2.concat(lsinterim);    			
    		}
    		else
    		{
    			Log.customer.debug("Only Line 3 is non empty");
    			counter = 1;
    			part2 = iterablepart;
    			part2 = replaceStrings(part2,"1","3",counter);
    		}
    		
    	}
    	if((loAppr.getDottedFieldValue("ItemITrelated4") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated4")).booleanValue())||(!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemVMIIDNum4"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemDescription4"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemModelNum4"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemSerialNum4"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemReason4"))))
    	{
    		String lsinterim = null;
    		lbLine4 = true;
    		if(counter == 3 )
    		{
    			Log.customer.debug("Line 1,2,3 & 4 are non empty");
    			counter = 4;
    			lsinterim = iterablepart;
    			lsinterim = replaceStrings(lsinterim,"4","4",counter);
    			part2 = part2.concat(lsinterim);
    		}
    		else if(counter == 2)
    		{
    			Log.customer.debug("two among line 1,2 & 3 are non empty, Line 4 is non empty");
    			lsinterim = iterablepart;
    			counter = 3;
    			lsinterim = replaceStrings(lsinterim,"3","4",counter);
    			part2 = part2.concat(lsinterim);
    		}
    		else if(counter == 1)
    		{
    			Log.customer.debug("only one among line 1,2 & 3 are non empty, Line 4 is non empty");
    			lsinterim = iterablepart;
    			counter = 2;    			
    			lsinterim = replaceStrings(lsinterim,"2","4",counter);
    			part2 = part2.concat(lsinterim);
      		}
    		else
    		{
    			Log.customer.debug("only Line 4 is non empty");
    			counter = 1;
    			part2 = iterablepart; 
    			part2 = replaceStrings(part2,"1","4",counter);
    		}
    		
    	}  
    	if((loAppr.getDottedFieldValue("ItemITrelated5") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated5")).booleanValue())||(!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemVMIIDNum5"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemDescription5"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemModelNum5"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemSerialNum5"))) || (!StringUtil.nullOrEmptyOrBlankString((String)loAppr.getDottedFieldValue("ItemReason5"))))
    	{
    		String lsinterim = null;
    		lbLine4 = true;
    		if(counter == 4 )
    		{
    			Log.customer.debug("Line 1,2,3,4 & 5 are non empty");
    			counter = 5;
    			lsinterim = iterablepart;
    			lsinterim = replaceStrings(lsinterim,"5","5",counter);
    			part2 = part2.concat(lsinterim);
    		}
    		else if(counter == 3)
    		{
    			Log.customer.debug("three among line 1,2,3 & 4 are non empty, Line 5 is non empty");
    			lsinterim = iterablepart;
    			counter = 4;
    			lsinterim = replaceStrings(lsinterim,"4","5",counter);
    			part2 = part2.concat(lsinterim);
    		}
    		else if(counter == 2)
    		{
    			Log.customer.debug("two among line 1,2,3 & 4 are non empty, Line 5 is non empty");
    			lsinterim = iterablepart;
    			counter = 3;
    			lsinterim = replaceStrings(lsinterim,"3","5",counter);
    			part2 = part2.concat(lsinterim);
    		}
    		else if(counter == 1)
    		{
    			Log.customer.debug("only one among line 1,2,3 & 4 are non empty, Line 5 is non empty");
    			lsinterim = iterablepart;
    			counter = 2; 
    			lsinterim = replaceStrings(lsinterim,"2","5",counter);
    			part2 = part2.concat(lsinterim);
    		}    		
    		else
    		{
    			Log.customer.debug("only Line 5 is non empty");
    			counter = 1;
    			part2 = iterablepart;  		
    			part2 = replaceStrings(part2,"1","5",counter);
    		}
    		
    	}   
    	
    	if(counter == 0)
    	{
    		lsHtmlPagenew = part1.concat(part3);
    		return lsHtmlPagenew;
    	}
    	else
    	{
    		part1 = part1.concat(part2);
    		lsHtmlPagenew = part1.concat(part3);
    		return lsHtmlPagenew;
    	}

	}
    private String replaceStrings(String lsinterim, String lsCounterLine, String lsFieldCounter, int counter)
    {
		String lsItemITrelated = "ItemITrelated" + lsFieldCounter;
		String lsItemVMIIDNum = "ItemVMIIDNum" + lsFieldCounter;
		String lsItemDescription = "ItemDescription" + lsFieldCounter;
		String lsItemModelNum = "ItemModelNum" + lsFieldCounter;
		String lsItemSerialNum = "ItemSerialNum" + lsFieldCounter;
		String lsItemReason = "ItemReason" + lsFieldCounter;
		
		Log.customer.debug("Replacing <!--#--> and other strings with required value"+lsCounterLine+"," +lsItemITrelated+","+lsItemVMIIDNum+","+lsItemDescription+","+lsItemModelNum+","+lsItemSerialNum+","+lsItemReason );

		lsinterim = lsinterim.replace("<!--#-->", lsCounterLine);
		lsinterim = lsinterim.replace("ItemITrelated1", lsItemITrelated);
		lsinterim = lsinterim.replace("ItemVMIIDNum1", lsItemVMIIDNum);
		lsinterim = lsinterim.replace("ItemDescription1", lsItemDescription);
		lsinterim = lsinterim.replace("ItemModelNum1", lsItemModelNum);
		lsinterim = lsinterim.replace("ItemSerialNum1", lsItemSerialNum);
		lsinterim = lsinterim.replace("ItemReason1", lsItemReason);    	
    	
		return lsinterim;
	}



	private String getFinalHTML(String lsHtmlPage, Approvable loAppr) 
    {
    	String lsApprovableType=loAppr.getTypeName();
    	String str1 = null;
    	String str2 = null;
    	String lsHtmlPageNewLine = lsHtmlPage;
    	
        if (lsApprovableType.equals("ariba.core.BuysenseVMIAssetTracking"))
        {
        	String lsFormAction = (String) loAppr.getFieldValue("FormAction");
 		    String lsRadio1 = ResourceService.getString("buysense.eform", "Radio1", Base.getSession().getLocale());
 		    String lsRadio2 = ResourceService.getString("buysense.eform", "Radio2", Base.getSession().getLocale());
 		    String lsRadio3 = ResourceService.getString("buysense.eform", "Radio3", Base.getSession().getLocale());
 		    //int n = lsHtmlPage.lastIndexOf(lsHtmlPage);
 		    int n = lsHtmlPage.length();
 		    Log.customer.debug("The total length of the print string is "+n);
 		    if(StringUtil.nullOrEmptyOrBlankString(lsFormAction))
 		    {
  			   Log.customer.debug("No action has been picked");
 			   str1 = lsHtmlPage.substring(0, lsHtmlPage.indexOf("<!--SubSectionStarts-->"));
 			   str2 = lsHtmlPage.substring((lsHtmlPage.indexOf("<!--Section3E-->")), n);
 			   String lsHtmlPageNew = str1.concat(str2);
 			   //lsHtmlPageNew = lsHtmlPageNew.replace("!--Section3E-->","");
 			   return lsHtmlPageNew;
 		    }
 		    
 		   else if((lsFormAction != null && lsFormAction.equalsIgnoreCase(lsRadio1))&&((loAppr.getDottedFieldValue("ItemITrelated1") != null && !((Boolean)loAppr.getFieldValue("ItemITrelated1")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated2") != null && !((Boolean)loAppr.getFieldValue("ItemITrelated2")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated3") != null && !((Boolean)loAppr.getFieldValue("ItemITrelated3")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated4") != null && !((Boolean)loAppr.getFieldValue("ItemITrelated4")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated5") != null && !((Boolean)loAppr.getFieldValue("ItemITrelated5")).booleanValue())))
 		   {
 			   //Hiding section based on Radio1 value
 			   Log.customer.debug("Now hiding section based on Radio1 ");
 			   Log.customer.debug("The indexes are SubSectionStarts:"+(lsHtmlPage.indexOf("<!--SubSectionStarts-->"))+",Section2E:"+(lsHtmlPage.lastIndexOf("<!--Section2E-->")+1)+"and the last index is:"+n);
 			   int s = lsHtmlPage.indexOf("<!--SubSectionStarts-->");
 			   Log.customer.debug("S is :"+s);
  			   str1 = lsHtmlPage.substring(0, lsHtmlPage.indexOf("<!--SubSectionStarts-->"));
 			   str2 = lsHtmlPage.substring((lsHtmlPage.indexOf("<!--Section2E-->")), n);
 			   String lsHtmlPageNew = str1.concat(str2);
 			  // lsHtmlPageNew = lsHtmlPageNew.replace("!--Section2E-->","");
 			   return lsHtmlPageNew;
 			   
 		   }
           else if((lsFormAction != null && lsFormAction.equalsIgnoreCase(lsRadio1))&&((loAppr.getDottedFieldValue("ItemITrelated1") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated1")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated2") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated2")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated3") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated3")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated4") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated4")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated5") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated5")).booleanValue())))
           {
        	   //Hiding section based on Radio1 value and IT checkbox
        	   Log.customer.debug("Now hiding section based on Radio1 and IT checkbox"); 
        	   Log.customer.debug("The indexes are SubSectionStarts:"+lsHtmlPage.indexOf("<!--SubSectionStarts-->")+",Section1E:"+(lsHtmlPage.lastIndexOf("<!--Section1E-->")+1)+"and the last index is:"+n);
 			   str1 = lsHtmlPage.substring(0, lsHtmlPage.indexOf("<!--SubSectionStarts-->"));
 			   str2 = lsHtmlPage.substring((lsHtmlPage.indexOf("<!--Section1E-->")), n);
 			   String lsHtmlPageNew = str1.concat(str2);
 			   //lsHtmlPageNew = lsHtmlPageNew.replace("!--Section1E-->","");
 			   return lsHtmlPageNew;
           }
           else if((lsFormAction != null && lsFormAction.equalsIgnoreCase(lsRadio1)))
           {
        	   //Hiding section based on Radio1 value
        	   Log.customer.debug("Now hiding section based on Radio1");
 			   str1 = lsHtmlPage.substring(0, lsHtmlPage.indexOf("<!--SubSectionStarts-->"));
 			   str2 = lsHtmlPage.substring((lsHtmlPage.indexOf("<!--Section2E-->")), n);
 			   String lsHtmlPageNew = str1.concat(str2);
 			   //lsHtmlPageNew = lsHtmlPageNew.replace("!--Section2E-->","");
 			   return lsHtmlPageNew;
           } 		   
 		   
 		   else if((lsFormAction != null && lsFormAction.equalsIgnoreCase(lsRadio2))&&((loAppr.getDottedFieldValue("ItemITrelated1") != null && !((Boolean)loAppr.getFieldValue("ItemITrelated1")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated2") != null && !((Boolean)loAppr.getFieldValue("ItemITrelated2")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated3") != null && !((Boolean)loAppr.getFieldValue("ItemITrelated3")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated4") != null && !((Boolean)loAppr.getFieldValue("ItemITrelated4")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated5") != null && !((Boolean)loAppr.getFieldValue("ItemITrelated5")).booleanValue())))
		   {
 			   //Hiding section based if value
 			   Log.customer.debug("Now hiding section based on Radio2");
			   str1 = lsHtmlPage.substring(0, lsHtmlPage.indexOf("<!--Section1E-->"));
			   str2 = lsHtmlPage.substring((lsHtmlPage.indexOf("<!--Section3E-->")), n);
			   String lsHtmlPageNew = str1.concat(str2);
			   //lsHtmlPageNew = lsHtmlPageNew.replace("!--Section3E-->","");
			   return lsHtmlPageNew;
			   
		   }
           else if((lsFormAction != null && lsFormAction.equalsIgnoreCase(lsRadio2))&&((loAppr.getDottedFieldValue("ItemITrelated1") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated1")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated2") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated2")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated3") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated3")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated4") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated4")).booleanValue()) || (loAppr.getDottedFieldValue("ItemITrelated5") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated5")).booleanValue())))
           {
        	   Log.customer.debug("Now hiding section based on Radio2 and IT checkbox");
			   str1 = lsHtmlPage.substring(0, lsHtmlPage.indexOf("<!--Section2E-->"));
			   str2 = lsHtmlPage.substring((lsHtmlPage.indexOf("<!--Section3E-->")), n);
			   String lsHtmlPageNew = str1.concat(str2);
			   //lsHtmlPageNew = lsHtmlPageNew.replace("!--Section3E-->","");
			   return lsHtmlPageNew;
           }
           else if((lsFormAction != null && lsFormAction.equalsIgnoreCase(lsRadio2)))
           {
        	   Log.customer.debug("Now hiding section based on Radio2");
			   str1 = lsHtmlPage.substring(0, lsHtmlPage.indexOf("<!--Section1E-->"));
			   str2 = lsHtmlPage.substring((lsHtmlPage.indexOf("<!--Section3E-->")), n);
			   String lsHtmlPageNew = str1.concat(str2);
			   //lsHtmlPageNew = lsHtmlPageNew.replace("!--Section3E-->","");
			   return lsHtmlPageNew;
           } 		   
 		  if(lsFormAction != null && lsFormAction.equalsIgnoreCase(lsRadio3))
		   {
 			   Log.customer.debug("Now hiding section based on Radio3");
			   str1 = lsHtmlPage.substring(0, lsHtmlPage.indexOf("<!--SubSectionStarts-->"));
			   str2 = lsHtmlPage.substring((lsHtmlPage.indexOf("<!--Section3E-->")), n);
			   String lsHtmlPageNew = str1.concat(str2);
			   //lsHtmlPageNew = lsHtmlPageNew.replace("!--Section3E-->","");
			   return lsHtmlPageNew;			   
		   }
		    
        }
		return lsHtmlPage;
	}

	public String getFieldValue(String fieldName,String stringObj,Approvable approvable)
    {
        Log.customer.debug("Inside getFieldValue method, fieldName :"+fieldName);        
        String lsResultValue="";           
        List loObjlist = (List)approvable.getFieldValue(stringObj);     
               
        BaseSession loSession = Base.getSession();       
        if(loObjlist != null)
         {       
            for (int i = 0; i < loObjlist.size(); i++)
            {             
                if(i>0)
                {
                    if(fieldName.equals("Text"))
                    {
                     lsResultValue=lsResultValue+"<br>";
                    }
                    else
                    {
                    lsResultValue=lsResultValue+", ";
                    }
                }                
                Object obj = loObjlist.get(i);                             
                BaseObject loObjectName = null;
                try
                {
                    if(obj instanceof Comment)
                    {
                       Comment loCmt = (Comment)loObjlist.get(i);   
                       loObjectName = loCmt;
                    }
                    else if(obj instanceof ApprovalRequest)
                        {
                          ApprovalRequest loAppReq=(ApprovalRequest)loObjlist.get(i);
                          loObjectName = loAppReq;
                        }
                        else
                        {    
                          BaseId loId = (BaseId)loObjlist.get(i);
                          loObjectName = loSession.objectFromId(loId);
                        }                                                   
                    //Log.customer.debug("Inside for of getFieldValue, object:"+loObjectName);
                    Object loObjectValue=null;
                    if(fieldName.equals("Text"))
                    {                                    
                        LongString llsText = (LongString) loObjectName.getDottedFieldValue(fieldName);
                        if(llsText != null)
                        {
                           loObjectValue = (String) llsText.string();
                        }                    
                    }
                    else
                    {
                        loObjectValue = loObjectName.getDottedFieldValue(fieldName);
                    } 
                    Log.customer.debug("field name " +loObjectValue);                   
                    if (loObjectValue == null)
                        loObjectValue = ""; // replace NullValue with blank.
                    lsResultValue=lsResultValue+String.valueOf(loObjectValue);                    
                }catch(Exception e)
                {
                    Log.customer.debug("Excception: "+e.getMessage());
                    lsResultValue = "EXCEPTION";
                }
                                                                
             }           
         }         
            return lsResultValue;
     }
    public String getReqValue(String objName,String fieldName,Approvable approvable)
    {
        ariba.user.core.User loUser = (ariba.user.core.User)approvable.getDottedFieldValue(objName);
        ariba.common.core.User loPartitionUser = ariba.common.core.User.getPartitionedUser(loUser,approvable.getPartition());
        Object loFieldValue = loPartitionUser.getDottedFieldValue(fieldName);
        if (loFieldValue == null)
        {
            loFieldValue = "";
        }
        Log.customer.debug("Object value in getReqValue mathod: " +loFieldValue);
        String lsResultValue=loFieldValue.toString(); 
        return lsResultValue;
    } 


}
