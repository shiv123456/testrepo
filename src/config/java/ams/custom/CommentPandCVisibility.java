/* 04/11/2005: New class for implementing P & C flag visibility constraint */

package config.java.ams.custom;

import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.core.BaseObject;
//import ariba.htmlui.fieldsui.Log;
import ariba.util.log.Log;

//81->822 changed ConditionValueInfo to ValueInfo
public class CommentPandCVisibility extends Condition
{
    
    /* Ariba 8.0: Replaced ValueInfo */
    private static final ValueInfo parameterInfo[];
    private static final String EqualToMsg1 = "EqualToMsg1";
    
    public boolean evaluate(Object value, PropertyTable params)
    {
        // Logic: The P&C flag should only be visible on Reqs, not on POs. However, when you click "Add Comments",
        // the new Comment object's "Parent" field is null, and would not indicate whether the object belongs to a Req
        // or a PO. So we set the visibility to true in this case, making the P&C flag visible during comment entry on PO.
        Log.customer.debug("CommentPandCVisibility: This is the object we get in the evaluate: %s ", value);
        if (value == null) 
        {
           return true;
        }
        else
        {
            BaseObject bo = (BaseObject)params.getPropertyForKey("TargetValue");
            if (bo == null || bo.instanceOf("ariba.purchasing.core.Requisition"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    /* Ariba 8.0: Replaced ValueInfo */
    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }
    
    static
    {
        parameterInfo = (new ValueInfo[] { new ValueInfo("TargetValue", 0),
                                                    new ValueInfo("Message", 0, Behavior.StringClass)});
    }
}
