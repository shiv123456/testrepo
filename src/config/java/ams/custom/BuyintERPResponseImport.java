/************************************************************************************
 * Author:  Anup H. and Richard Lee
 * Date:    July 30, 2004
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 07/30/2004        Richard Lee            Integration Interface
 * 3/31/2005	     Richard Lee	    Dev SPL 543 - INT PCard change order before
 *							ERP response
 * 3/31/2005	     Richard Lee	    Dev SPL 544 - INT ERP response for
 *							non existing order
 *
 * @(#)BuyintERPResponseImport.java     1.0 08/20/2004
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/

package config.java.ams.custom ;

import ariba.purchasing.core.Requisition;
import ariba.purchasing.core.PurchaseOrder;
import java.util.Map;
import ariba.pcard.core.PCardOrder;
import ariba.approvable.core.Approvable;
import ariba.purchasing.core.ERPOrder;
import ariba.base.core.*;
import ariba.base.core.aql.*;
import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.base.core.BaseObject;

//81->822 changed Hashtable to Map
public class BuyintERPResponseImport extends BuysenseXMLImport implements BuyintConstants
{
    Map loHashtable = null;
    ClusterRoot moObj    = null ;
    String      msERPMessage = null;
    String      msResponseType = null;
    String      msISR = null;

    /**
     * CSPL-592 (Manoj)
     * Check RESP for external ERP response
     */
    public static boolean bCorrectRESP = false;

    public BuyintERPResponseImport()
    {
        super() ;
        moPartition = Base.getSession().getPartition() ;
    }

    public BuyintERPResponseImport( Partition foPartition )
    {
        super() ;
        moPartition = foPartition ;
    }
    // find an existing approvable with same UniqueName
    protected Approvable findApprovable( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {
            BaseObject    loBaseObject    = null ;
            String        lsAribaVal      = foMyContext.getText() ;
            String        lsAribaType     = foMyContext.getAttrValue( ARIBA_TYPE ) ;
            String        lsReqUniqueName = foMyContext.getAttrValue( REQ_UNIQUE_NAME );
            String        lsPOUniqueName  = foMyContext.getAttrValue( PO_UNIQUE_NAME );
            AQLQuery      loAQL           = new AQLQuery( lsAribaType ) ;
            AQLOptions    loAQLOptions    = new AQLOptions( moPartition ) ;

            lsAribaType = (String) BuyintXWalk.XWalk.changeOldToNew(lsAribaType);

            Log.customer.debug( "Calling BuyintERPResponseImport" ) ;
            moPartition = Base.getSession().getPartition();
              Log.customer.debug( "moPartition = " + moPartition);

            if(((lsReqUniqueName != null) && (lsPOUniqueName.length() > 0)) &&
               ((lsPOUniqueName != null) && (lsReqUniqueName.length() > 0))
              )
            {
                throw new BuysenseXMLImportException(
                 "ERROR: ReqUniqueName and POUniqueName can not be used." ) ;
            }
            if ((lsReqUniqueName == null) && (lsPOUniqueName == null))
            {
                throw new BuysenseXMLImportException(
                 "ERROR: ReqUniqueName and POUniqueName can not both be null." ) ;
            }

            if (lsReqUniqueName != null)
            {
               if ( lsReqUniqueName.length() > 0 &&
                    lsAribaType.trim().equalsIgnoreCase("ariba.purchasing.core.Requisition"))
               {
                     lsAribaVal = lsReqUniqueName.trim();
                     if ( DEBUG )
                     {
                         Log.customer.debug( "Requisition " + lsAribaVal + " under process." ) ;
                     }
               }
            }
            if (lsPOUniqueName != null)
            {
               if ( lsPOUniqueName.length() > 0 &&
                   (lsAribaType.trim().equalsIgnoreCase("ariba.purchasing.core.ERPOrder") ||
                    lsAribaType.trim().equalsIgnoreCase("ariba.pcard.core.PCardOrder")))
               {
                  lsAribaVal = lsPOUniqueName.trim();
                  if ( DEBUG )
                  {
                     Log.customer.debug( "Order " + lsAribaVal + " under process." ) ;
                  }
               }
            }

            if(moPartition == null)
            {
                Log.customer.debug( "moPartition is null " ) ;
                moPartition = Base.getService().getPartition("pcsv");
            }

            loBaseObject = (BaseObject) Base.getService().objectMatchingUniqueName(lsAribaType, moPartition, lsAribaVal);
            Log.customer.debug( "loBaseObject = " + loBaseObject ) ;

            if ( loBaseObject != null && loBaseObject instanceof Approvable )
            {
                return ( (Approvable)loBaseObject ) ;
            }
            else
            {
                if ( DEBUG )
                {
                    Log.customer.debug( "Header object with value " + lsAribaVal + " was not found." ) ;
                }
                throw new BuysenseXMLImportException(
               		"Error: The Document ID does not exist in Ariba.", ERR_CODE_SKIP_RETRY);
            }
        }
        catch (BuysenseXMLImportException loBXMLIE)
        {
           throw loBXMLIE;
        }
        catch ( Exception loExcep )
        {
           throw new BuysenseXMLImportException( loExcep.getMessage() );
        }
    }
    /**
     * This method processes the start of the header section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void startHeader( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        /*
         * Create a new Base object representing the header
         */
        try
        {
            ClusterRoot loExistingObj = null ;
            String      lsAribaType   = foMyContext.getAttrValue( ARIBA_TYPE ) ;

           // CSPL-592 (Manoj) Checked whether RESP is correct or not
            bCorrectRESP = !StringUtil.nullOrEmptyOrBlankString(foMyContext.getAttrValue(BuyintConstants.RESPONSE_TYPE));
            Log.customer.debug("ContextObject:%s and RESP: %s and bCorrectRESP: %s",foMyContext,foMyContext.getAttrValue(BuyintConstants.RESPONSE_TYPE),new Boolean(bCorrectRESP).toString());

            // Set the member level Approvable Object handle to null for each new XML ...
            // this prevents the wrong one from being deactivated upon Exception
            moHeader = null;

            lsAribaType = (String) BuyintXWalk.XWalk.changeOldToNew(lsAribaType);
            try
            {
                wait( 1000 ) ;
            }
            catch( Exception ex )
            {
            }
            //Find if an approvable with the UniqueName exists
            loExistingObj = findApprovable( foMyContext ) ;
	    	boolean lbActive = ((Boolean)loExistingObj.getFieldValue("Active")).booleanValue();

            if ( loExistingObj == null )
            {
                if ( DEBUG )
                {
                    Log.customer.debug( "Existing object not found." ) ;
                }
                throw new BuysenseXMLImportException(
                                                     "Error: null value returned when creating header", ERR_CODE_SKIP_RETRY ) ;
            }
            else
            {
               //Error out if the approvable response was not expected/has already been processed
               String lsIntStatus = null;
               if(lsAribaType.trim().equalsIgnoreCase("ariba.purchasing.core.Requisition"))
               {
                  lsIntStatus = (String)((Requisition)loExistingObj).getFieldValue("PreEncumbranceStatus");
                  if (! lsIntStatus.equalsIgnoreCase(STATUS_ERP_INPROGRESS) )
                  {
                     throw new BuysenseXMLImportException(
                          "Error: Cannot process. Requisition not awaiting ERP approve/deny response.", ERR_CODE_SKIP_RETRY);
                  }

               }
               else if(lsAribaType.trim().equalsIgnoreCase("ariba.purchasing.core.ERPOrder"))
               {
                  lsIntStatus = (String)((PurchaseOrder)loExistingObj).getFieldValue("EncumbranceStatus");
                  if (! lsIntStatus.equalsIgnoreCase(STATUS_ERP_INPROGRESS))
                  {
                     throw new BuysenseXMLImportException(
                          "Error: Cannot process. Order not awaiting ERP approve/deny response.", ERR_CODE_SKIP_RETRY);
                  }
               }
               else if(lsAribaType.trim().equalsIgnoreCase("ariba.pcard.core.PCardOrder"))
               {
                  lsIntStatus = (String)((PurchaseOrder)loExistingObj).getFieldValue("EncumbranceStatus");
		  		  // True if either one or both are false
                  if (!(lsIntStatus.equalsIgnoreCase(STATUS_ERP_INPROGRESS) && lbActive))
                  {
                     if(lsIntStatus.equalsIgnoreCase(STATUS_ERP_INPROGRESS))
                     {
                        loExistingObj.setFieldValue("EncumbranceStatus", "NOT RECEIVED");
                     }

                     throw new BuysenseXMLImportException(
                          "Error: Cannot process. Order not awaiting ERP approve/deny response.", ERR_CODE_SKIP_RETRY);
                  }
               }
            }
            moHeader = loExistingObj ;
            foMyContext.setObject( loExistingObj ) ;
        }
        catch (BuysenseXMLImportException loBXMLIE)
        {
           throw loBXMLIE;
        }
        catch ( Exception loException )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: Exception while creating header: " +
                                                 loException.getMessage() ) ;
        }
    }

    protected void finishHeader( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {
            ClusterRoot loObj    = null ;
            msERPMessage = foMyContext.getAttrValue( ERP_MESSAGE ) ;
            msResponseType = foMyContext.getAttrValue( RESPONSE_TYPE ) ;
            msISR = foMyContext.getAttrValue( INTEGRATION_SIGNER_RULE ) ;

            /*
             * We are done, so now persist and call preencumbrance/encumbrance processing
             */
            if ( ( loObj = (ClusterRoot)foMyContext.getObject() ) != null )
            {
                moObj = loObj;

                loHashtable = MapUtil.map();
                if(msERPMessage != null)
                    loHashtable.put(ERP_MESSAGE, msERPMessage);
                if(msResponseType != null)
                    loHashtable.put(RESPONSE_TYPE, msResponseType);
                if(msISR != null)
                    loHashtable.put(INTEGRATION_SIGNER_RULE, msISR);

                Log.customer.debug( "ERP: Got Req. " + loObj) ;

                if (loObj instanceof Requisition)
                {
                   // Preencumbrance
                   BuyintCustomApproveOrDeny loBCA = new BuyintCustomApproveOrDeny();
                   loBCA.fire(loObj, loHashtable);
                }
                else if (loObj instanceof ERPOrder)
                {
                   // Encumbrance
                   BuyintEncumbranceAction loBEA = new BuyintEncumbranceAction();
                   loBEA.fire(loObj, loHashtable);
                }
                else if (loObj instanceof PCardOrder)
                {
                   // Encumbrance
                   BuyintEncumbranceAction loBEA = new BuyintEncumbranceAction();
                   loBEA.fire(loObj, loHashtable);
                }
            }
            else
            {
                Log.customer.debug("Error: header object is null.");

                throw new BuysenseXMLImportException("Error: header object is null." ) ;
            }
            loObj.save();
            // removing it because of CSPL-4552
            // Base.getSession().transactionCommit();
        }
        catch ( BuysenseXMLImportException loBuyException )
        {
            throw loBuyException ;
        }
        catch ( Exception loException )
        {
            throw new BuysenseXMLImportException(
                                                 "Error: exception while finishing header: " +
                                                 loException.getMessage() ) ;
        }
    }
}
