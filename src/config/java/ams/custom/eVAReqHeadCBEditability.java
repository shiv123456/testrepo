/**
Version :  1.0

eProcure UAT SPL 349 - New class required to implement the new editability condition for ReqHeadCB1Value and ReqHeadCB3Value
                       so that both can not be edited on QQ transactions.
*/



package config.java.ams.custom;

import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.htmlui.fieldsui.Log;

//81->822 changed ConditionValueInfo to ValueInfo
public class eVAReqHeadCBEditability extends Condition
{

    private static final ValueInfo parameterInfo[];

    public boolean evaluate(Object value, PropertyTable params)
    {
        Log.util.debug("eVAReqHeadCBEditability: This is the object we get in the evaluate: %s ", value);
        BaseObject obj   = (BaseObject)params.getPropertyForKey("SourceObject");
        boolean lbAllowEdit = true;
        if (obj.instanceOf("ariba.purchasing.core.Requisition"))
        {
           if ((obj.getFieldValue("TransactionSource")) != null)
           {
              String lsTransSource = (String)obj.getFieldValue("TransactionSource");
              if (lsTransSource.equals("QQ"))
              {
                  lbAllowEdit = false;
              }
           }
        }
        return lbAllowEdit;
    }
    public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
    {
       if (!evaluate(value,params))
       {
          return new ConditionResult(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core", "NullRequesterMsg"));
       }
       else
       {
          return null;
       }
    }

    protected ValueInfo[] getParameterInfo()
    {
       return parameterInfo;
    }

    static
    {
       parameterInfo = (new ValueInfo[] {
          new ValueInfo("SourceObject", 0)
         });
    }
}
