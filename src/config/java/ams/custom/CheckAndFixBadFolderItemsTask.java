
/* 09/05/2003: Updates for Ariba 8.0 (Richard Lee) */

package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLResultCollection;
import ariba.approvable.core.Approvable;
import ariba.util.core.Constants;
import ariba.util.core.Fmt;
import java.util.Map;
// Ariba 8.1: Don't need Util
//import ariba.util.core.Util;
import ariba.procure.core.Log;
import ariba.approvable.core.Folder;
import ariba.approvable.core.FolderItem;
import java.util.List;
import ariba.approvable.core.ApprovalRequest;
// Ariba 8.1: Role moved from ariba.common.core.Role
// Ariba 8.1: Addded the new ariba.user.core.User and ariba.user.Approver class
import ariba.user.core.*;
// Ariba 8.1: Need ListUtil
import ariba.util.core.ListUtil;
// Ariba 8.1: Need Iterator
import java.util.Iterator;

//Ariba 8.0: added import scheduler *
import ariba.util.scheduler.*;

//81->822 changed Vector to List
//81->822 changed Hashtable to Map
//81->822 changed Enumeration to Iterator
public class CheckAndFixBadFolderItemsTask extends ScheduledTask
//Ariba 8.0: SimpleScheduledTask deprecated 
//public class CheckAndFixBadFolderItemsTask extends SimpleScheduledTask
{
    
    private static final String FixFolder  = "FixFolder";
    private boolean shouldfixfolder = false;
    
    public void init (Scheduler scheduler,
                      String scheduledTaskName, Map arguments)
    // Ariba 8.0: replaced Partition with Scheduler
    //public void init (Partition partition, String scheduledTaskName, Map arguments)
    {
        super.init(scheduler, scheduledTaskName, arguments);
        // Ariba 8.0: replaced Partition with Scheduler
        //super.init(partition, scheduledTaskName, arguments);
        
        // process the arguments if they exist
        // Ariba 8.1: arguments::keys() is deprecated by arguments::KeySet().iterator()
        //Enumeration e = arguments.keys();
        Iterator i = arguments.keySet().iterator();

        while (i.hasNext()) 
        {
            String key = (String)i.next();
            if (key.equals(FixFolder)) 
            {
                String argument = (String)arguments.get(key);
                shouldfixfolder = Constants.getBoolean(argument).booleanValue();
                //shouldfixfolder = Util.getBoolean(argument).booleanValue();
            }
        }
    }
    
    public void run ()
    {
        Log.customer.debug("CheckAndFixBadFolderItemsTask RUNNING");
        
        Partition partition = Base.getSession().getPartition();
        //Partition partition = partition();
        
        List folderstoprocess = getFolders(partition);
        
        Log.customer.debug("CheckAndFixBadFolderItemsTask folders to process vector size: " + folderstoprocess.size());
        
        // Ariba 8.1: We are using Iterator instead of Enumeration now.
        //Enumeration folderstoprocessenum = folderstoprocess.elements();
        Iterator folderstoprocessenum = folderstoprocess.iterator(); 
        
        while (folderstoprocessenum.hasNext()) 
        {
            
            Folder foldertoprocess = (Folder)Base.getSession().objectForWrite((BaseId)folderstoprocessenum.next());
            //               Folder foldertoprocess = (Folder)folderstoprocessenum.next();
            //               Folder foldertoprocess = (Folder)folderstoprocessenum.next();
            //               Log.customer.debug("CheckAndFixBadFolderItemsTask foldertoprocess: " + foldertoprocess);
            
            Iterator folderitemsnum = foldertoprocess.getItemsIterator();
            //               Log.customer.debug("CheckAndFixBadFolderItemsTask folderitemsnum: " + folderitemsnum);
            
            ariba.user.core.User folderowner = (ariba.user.core.User)foldertoprocess.getOwner();
            Log.customer.debug("CheckAndFixBadFolderItemsTask folderowner: " + folderowner);
            
            // Ariba 8.1: List constructor is deprecated by ListUtil.newVector()
            List folderitemstoremove = ListUtil.list();
            
            while (folderitemsnum.hasNext()) 
            {
                Object folderitemtoprocessobject = (Object)folderitemsnum.next();
                Log.customer.debug("CheckAndFixBadFolderItemsTask folderitemtoprocessobject: " + folderitemtoprocessobject);
                
                FolderItem folderitemtoprocess = (FolderItem)Base.getSession().objectFromId((BaseId)folderitemtoprocessobject);
                //                      Log.customer.debug("CheckAndFixBadFolderItemsTask folderitemtoprocess: " + folderitemtoprocess);
                
                //                      Log.customer.debug("CheckAndFixBadFolderItemsTask folderitemtoprocess checkAndOrRemoveBadFolderItem: "+  checkAndOrRemoveBadFolderItem(folderowner,foldertoprocess,folderitemtoprocess));
                
                Approver folderownerappr = (Approver)folderowner;
                if (!(IsFolderOwnerAnApprover(folderowner,
                                              (Approvable)folderitemtoprocess.getItem()))) 
                {
                    Log.customer.debug("CheckAndFixBadFolderItemsTask folderitemtoprocess remove folderitem: " + folderitemtoprocess);
                    // Ariba 8.1: List::addElement() is deprecated by List::add()
                    folderitemstoremove.add(folderitemtoprocess);
                    
                }
                
            }
            
            for (int i = 0; i < folderitemstoremove.size(); i++) 
            {
                
                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                FolderItem folderitemtoremovefromfolder = (FolderItem)folderitemstoremove.get(i);
                Log.customer.debug("CheckAndFixBadFolderItemsTask folderitemtoprocess bad folderitems in vector: " + folderitemtoremovefromfolder+" "+folderowner.getUniqueName()+" "+folderitemtoremovefromfolder.getItem().getUniqueName());
                
                if (shouldfixfolder) 
                {
                    Log.customer.debug("CheckAndFixBadFolderItemsTask folderitemtoprocess removing bad folderitems in vector: " + folderitemtoremovefromfolder);
                    foldertoprocess.getItems().removeElement(folderitemtoremovefromfolder);
                    Base.getSession().delete(folderitemtoremovefromfolder);
                }
                
            }
            
            if (shouldfixfolder) 
            {
                foldertoprocess.save();
                Base.getSession().transactionCommit();
            }
            
        }
        
    }
    
    
    private List getFolders (Partition partition)
    {
        
        
        String query = Fmt.S("SELECT DISTINCT Folder " +
                             "From Folder " +
                             "join FolderItem using Folder.Items " +
                             "where Folder.FolderName = 'FolderNameApprove' " +
                             "and Folder.Owner.Active = true");
        
        AQLOptions options = new AQLOptions(Partition.Any);
        // Ariba 8.0: this AQLOptions constructor is deprecated, AQLOptions options = new AQLOptions(Partition.Any, false);
        
        AQLResultCollection results = Base.getService().executeQuery(query,
                                                                     options);
        // Ariba 8.1: List constructor is deprecated by ListUtil.newVector() 
        List result = ListUtil.list();
        while (results.next()) 
        {
            // Ariba 8.1: List::addElement() is deprecated by List::add()
            result.add(results.getBaseId(0));
        }
        
        
        return result;
        
    }
    
    
    public boolean  IsFolderOwnerAnApprover (ariba.user.core.User folderowner, Approvable ap)
    {
        
        //      Log.customer.debug("CheckAndFixBadFolderItemsTask in IsFolderownerAnApprover: " + folderowner.getUniqueName());
        
        boolean isapprover = false;
        
        Approver folderownerappr = (Approver)folderowner;
        isapprover=isApprover(folderownerappr,ap);
        //        Log.customer.debug("CheckAndFixBadFolderItemsTask IsFolderownerAnApprover isapprover: " + isapprover);
        
        if (isapprover) 
        {
            //            Log.customer.debug("CheckAndFixBadFolderItemsTask IsFolderownerAnApprover returning true folderowner user is a approver: " + isapprover);
            
            return true;
        }
        
        //        Log.customer.debug("CheckAndFixBadFolderItemsTask IsFolderownerAnApprover check folderowner roles to see if a approver");
        
        if (folderowner.getRoles().size() > 0) 
        {
            
            Iterator rolesenum = folderowner.getRolesIterator();
            
            while (rolesenum.hasNext()) 
            {
                
                BaseId rolebid = (BaseId)rolesenum.next();
                //Role role = (Role)Base.getSession().objectFromId((BaseId)rolebid);
                Approver role = (Approver)Base.getSession().objectFromId((BaseId)rolebid);
                
                isapprover=isApprover(role,ap);
                
                if (isapprover) 
                {
                    break;
                }
            }
            
        }
        
        
        
        //      Log.customer.debug("CheckAndFixBadFolderItemsTask IsFolderownerAnApprover method returning: " + isapprover);
        return isapprover;
    }
    
    
    
    public boolean isApprover (Approver userorrole, Approvable ap)
    {
        
        //       Log.customer.debug("CheckAndFixBadFolderItemsTask in isApprover");
        
        
        
        Iterator anyApprovals =
            ap.getApprovalRequestsIterator((ApprovalRequest.StateActive|
                                               ApprovalRequest.StateDenied|
                                               ApprovalRequest.StateNotActive|
                                               ApprovalRequest.StateApproved),
                                              userorrole,
                                              Boolean.TRUE);
        
        
        
        //       Log.customer.debug("CheckAndFixBadFolderItemsTask isApprover returning: " + anyApprovals.hasMoreElements());
        return anyApprovals.hasNext();
        
        
    }
    
}



