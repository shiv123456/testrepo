/*
    Copyright (c) 1996-1999 Ariba/Buysense, Inc.
    Falahyar March 2000
    --------------------------------------------------------------------------------------------------
   RLee, 4/19/02: eVA Dev SPL# 62: java error in eVAReqSubmitHook.java
   Added check for zero line item in line 219.
   This fix is on baseline version 1.11 of BuysenseRulesEngine.java

*/

/* 09/03/2003: Updates for Ariba 8.0 (David Chamberlain)
   Replaced all Util.getInteger()'s with Constants.getInteger()
   Replaced all Util.vector()'s with ListUtil.vector()
   Replaced all Util.getInteger()'s with Constants.getInteger()
*/
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/

package config.java.ams.custom;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import ariba.approvable.core.Approvable;
import ariba.base.core.BaseObject;
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;
import ariba.util.log.Log;

//81->822 changed Vector to List
public class BuysenseRulesEngine
{
private static String appType = new String();
private static final List NoErrorResult = ListUtil.list(Constants.getInteger(0));

public static List runBSOrgFilteringRules (Approvable approvable) {
  Log.customer.debug("In BuysenseOrg section of the RulesEngine");
  BaseObject bo = (BaseObject)approvable;
  List fieldVector = FieldListContainer.getChooserFieldValues();
  appType ="BuysenseOrg";
  String clientUniquename = (String)approvable.getDottedFieldValue("ClientName.UniqueName");
  return  runFilteringRules (bo,fieldVector,clientUniquename);
}

public static List runReqFilteringRules(Approvable approvable) {
   List resultVector;
   resultVector = runReqHeaderFilteringRules(approvable);
   // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
   if (((Integer)resultVector.get(0)).intValue() == -1) {
        return resultVector;
    }
    return( runReqLineFilteringRules(approvable));
}

private static List runReqHeaderFilteringRules (Approvable approvable) {

  appType = "ReqHeader";
  List fieldVector = FieldListContainer.getReqHeaderChooserFieldValues();
  return runHeaderFilteringRules (approvable,fieldVector);
}

private static List runReqLineFilteringRules(Approvable approvable) {

List lineFieldsVector = FieldListContainer.getReqLineChooserFieldValues();
List actgFieldsVector = FieldListContainer.getAcctgChooserFieldValues();
String lineField = "LineItems";
String actgField = "Accountings.SplitAccountings";
String appPrefix = "Req";
return runLineFilteringRules (approvable,lineFieldsVector,actgFieldsVector,lineField,actgField,appPrefix);
}



public static List runFilteringRules (BaseObject approvable,List fieldVector,String clientUniquename)
{
 Log.customer.debug("runFiltering rules got called 1");
 String whereClauseField ;
 String whereClause = null;
 String filterString ;
 String label = null;
 boolean continueFlag = false;

// List fieldVector = FieldListContainer.getChooserFieldValues();
 String fieldName = new String();

 // Ariba 8.1: List::elements() is deprecated by AbstractList::iterator()
 //for (Enumeration fieldEnum = fieldVector.elements();fieldEnum.hasMoreElements();)
 for (Iterator fieldItr = fieldVector.iterator();fieldItr.hasNext();)
 {
  try {
   if (appType.equals("PaymentHeader") ||appType.equals("PaymentLine") ||appType.equals("PaymentAccounting") )
                    whereClauseField = "FieldTable.PayWhereClause";
   else if (appType.equals("BuysenseOrg"))  whereClauseField = "FieldTable.WhereClause1";
   else whereClauseField = "FieldTable.ReqWhereClause";

   // Ariba 8.1: changed to Iterator
   fieldName = (String)fieldItr.next();
   Log.customer.debug("Working on the field: %s", fieldName );
   BaseObject baseObj = (BaseObject)approvable.getDottedFieldValue(fieldName);
   if (baseObj == null){ Log.customer.debug("Field %s has null value",fieldName); }
   else {

     whereClause = (String) baseObj.getDottedFieldValue(whereClauseField);
     label = (String) baseObj.getDottedFieldValue("FieldTable.ERPValue");
     if (whereClause == null || whereClause.length() == 0 ) {Log.customer.debug("Got a null where clause for %s",fieldName);}
     else {
        Log.customer.debug("Where clause for field: %s is : %s",fieldName, whereClause);
        String fieldNum = (String)approvable.getDottedFieldValue(fieldName+".FieldNumber");
       // String clientUniquename = (String)approvable.getDottedFieldValue("ClientName.UniqueName");

        List tokensVector = BuysenseUtil.tokenize(whereClause);
        filterString = new String() ;
        // Ariba 8.1: We are going from using Enumerations to Iterators since List::elements() has been deprecated
        for (Iterator e = tokensVector.iterator();e.hasNext();) {


            // Ariba 8.1: Changed from to e.next() because we are using Iterators now instead of Enumerations
            fieldVector = (List)e.next();
            /* Element 1 -> fieldName
            Element 2 -> Offset
            Element 3 -> length  */

            // String fieldValue = (String)approvable.getDottedFieldValue((String) fieldVector.elementAt(0));
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            String fieldValue = BuysenseUtil.findFieldValue((String) fieldVector.get(0), approvable,appType);
            Log.customer.debug("Field value: %s",fieldValue);

            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            int offset = (new Integer((String)fieldVector.get(1))).intValue();
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            BigDecimal fl = new BigDecimal((String)fieldVector.get(2));
            int fieldLength = fl.intValue();
            fieldValue = fieldValue.substring(offset);
            Log.customer.debug("Field value from offset: %s",fieldValue);
            BigDecimal valueLength = new BigDecimal(fieldValue.length());
            if ((valueLength.compareTo(fl)) < 0){
                // The fieldValue is less than the designated fieldLength, pad the value with '??'.
                while (fieldValue.length() < fieldLength) fieldValue+= "?";
                filterString = filterString + fieldValue;
            }
            else if (valueLength.compareTo(fl) > 0){
                // The fieldValue is greater than the designated fieldLength, trim the value.
                filterString  += fieldValue.substring(offset,(offset+fieldLength));
            }
            else{
                filterString = filterString + fieldValue;
            }
            Log.customer.debug("Buysense: The filter string: %s",filterString);
        }

       String fieldValue = clientUniquename+":"+fieldNum+":" + filterString;
       String fieldUniqueName = (String)baseObj.getDottedFieldValue("UniqueName");
       Log.customer.debug("This is the current value of the %s : %s",fieldName,fieldUniqueName);
       String compareString = fieldUniqueName.substring(0,fieldValue.length());
       Log.customer.debug("Value of field:%s should match to :%s",fieldName,compareString);
       if (!fieldValue.equals(compareString) && !continueFlag ) {
           //CSPL-4292:Change the error message text for Dependent Data Submit edit. Updated error message.
         String errorString = "Field: " + label + " value is not being accepted by the system due to selection method. "+
       "Since this field is dependent on a value in another field, the value must be selected by using the 'Search for More...' link," +
       " and not by keying or selecting it from the drop-down menu.";
         return ListUtil.list(Constants.getInteger(-1),errorString);
       }
     }

   }
  }
 catch (StringIndexOutOfBoundsException sbe) {
            Log.customer.debug("Got a string out of bound exception. This happens when the whereClause" +
                          "specification and the field value are not in synch. We will raise this "+
                          " as an error.");
             String errorString = "Field: " + label + " has an invalid value. Please select another value." +
                                    "Field is not long enough.";
             return ListUtil.list(Constants.getInteger(-1),errorString);


    }
 catch (NullPointerException npe) {
            Log.customer.debug("Got a NullPointer Exception. This happens when a source field" +
                          "contains null. We will let this go as we do not want to enforce this like "+
                          "a validity constraint");

            String errorString = "Field: " + label + " has an invalid value. Please select another value." +
                                  "Missing values.";
            return ListUtil.list(Constants.getInteger(-1),errorString);
  }

 }

return NoErrorResult;
}

public static List runPaymentFilteringRules(Approvable approvable) {
   List resultVector;
   resultVector = runPaymentHeaderFilteringRules(approvable);
   // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
   if (((Integer)resultVector.get(0)).intValue() == -1) {
        return resultVector;
    }
    return( runPaymentLineFilteringRules(approvable));
}

private static List runPaymentHeaderFilteringRules (Approvable approvable) {

  appType = "PayHeader";
  List fieldVector = FieldListContainer.getPaymentHeaderChoosers();
  return runHeaderFilteringRules (approvable,fieldVector);
}

private static List runPaymentLineFilteringRules(Approvable approvable) {

List lineFieldsVector = FieldListContainer.getPaymentLineChoosers();
List actgFieldsVector = FieldListContainer.getAcctgChooserFieldValues();
String lineField = "PaymentItems";
String actgField = "PaymentAccountings";
String appPrefix = "Payment";
return runLineFilteringRules (approvable,lineFieldsVector,actgFieldsVector,lineField,actgField,appPrefix);
}

private static List runHeaderFilteringRules (Approvable approvable,List listVector) {

  BaseObject bo = (BaseObject)approvable;
  //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
  //Partitioned User to obtain the extrinsic BuysenseOrg field value.
  //String clientUniquename = (String)approvable.getDottedFieldValue("Requester.BuysenseOrg.ClientName.UniqueName");
  ariba.user.core.User RequesterUser = (ariba.user.core.User)approvable.getDottedFieldValue("Requester");
  //rgiesen dev SPL #39
  //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
  ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,approvable.getPartition());
  String clientUniquename = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.UniqueName");

  return  runFilteringRules (bo,listVector,clientUniquename);
}

private static List runLineFilteringRules (Approvable approvable,List lineFieldsVector,List actgFieldsVector,
                    String lineField,String actgField, String appPrefix) {
	
	/*Ravi Alapati modified on 13th Aug 2019
	CSPL-9763  -- Filtering Errors Not Displaying Line Item Identification
	changed the logic and display all error to UI */
	
	TreeMap<String,HashSet<String>> treeMap = new TreeMap<String,HashSet<String>>();

  // Ariba 8.1: List::List() is deprecated by ListUtil.newVector()
  List retVector = ListUtil.list();
  // Ariba 8.1: List::List() is deprecated by ListUtil.newVector()
  List retActgVector = ListUtil.list();
  BaseObject bo = (BaseObject)approvable;
  //List fieldVector = FieldListContainer.getReqLineChooserFieldValues();
  List liVector = (List)bo.getDottedFieldValue(lineField);
  int liVectorSize = liVector.size();
       Log.customer.debug("RLee: 215, runLineFilteringRules: liVectorSize:%s",liVectorSize);
  //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
  //Partitioned User to obtain the extrinsic BuysenseOrg field value.
  //String clientUniquename = (String)approvable.getDottedFieldValue("Requester.BuysenseOrg.ClientName.UniqueName");
  ariba.user.core.User RequesterUser = (ariba.user.core.User)approvable.getDottedFieldValue("Requester");
  //rgiesen dev SPL #39
  //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
  ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,approvable.getPartition());
  String clientUniquename = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.UniqueName");
  if (liVectorSize < 1)
  {
	return ListUtil.list(Constants.getInteger(-1), "This requisition has no line item!");
   }
  else
  {
  for (int i=0;i < liVectorSize ; i++) {
    appType = appPrefix+"Line";
    // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
    BaseObject li = (BaseObject)liVector.get(i);
    retVector = runFilteringRules (li,lineFieldsVector,clientUniquename);
    // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int) {
    if (((Integer)retVector.get(0)).intValue() == -1) {
        return retVector;
    }
    List acctgVector = (List)li.getDottedFieldValue(actgField);
    int acctgVectorSize = acctgVector.size();
    for (int j=0; j< acctgVectorSize;j++){
        appType = appPrefix+"Accounting";
        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
        BaseObject actg = (BaseObject)acctgVector.get(j);
        //List actgFieldVector = FieldListContainer.getAcctgChooserFieldValues();
        
    	/*Ravi Alapati modified on 13th Aug 2019
    	CSPL-9763  -- Filtering Errors Not Displaying Line Item Identification
    	changed the logic and display all error to UI */
        treeMap = runFilteringRulesByAcc(actg,actgFieldsVector,clientUniquename,treeMap);
        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
        /*if (((Integer)retActgVector.get(0)).intValue() == -1) {
           return retActgVector;
        }*/
     }
    }
  }
  //Log.customer.debug(" trMap :: "+treeMap.size()+" -- "+treeMap.toString());
  if(treeMap.size()>0){
	  String accErrors = treeMap.toString();
	  accErrors = accErrors.replace("{", "[");
	  accErrors = accErrors.replace("=[", " Field(s):");
	  accErrors = accErrors.replace("],", " | ");
	  accErrors = accErrors.replace("]}", "]");
	  //Log.customer.debug(" accErrors :: "+accErrors);
  	  String errorString = accErrors+" value is not being accepted by the system due to selection method. "+
			       "Since this field is dependent on a value in another field, the value must be selected by using the 'Search for More...' link," +
			       " and not by keying or selecting it from the drop-down menu.";
  	  Log.customer.debug(" errorString :: "+errorString);
  	  retActgVector = ListUtil.list(Constants.getInteger(-1),errorString);
  	  if (((Integer)retActgVector.get(0)).intValue() == -1) {
        return retActgVector;
     }
  }
  return retVector;
  }
/*Ravi Alapati modified on 13th Aug 2019
CSPL-9763  -- Filtering Errors Not Displaying Line Item Identification
created new method and return all errors to UI */
private static TreeMap<String, HashSet<String>> runFilteringRulesByAcc(BaseObject approvable, List fieldVector, String clientUniquename,TreeMap<String, HashSet<String>> treeMap) {
	 //Log.customer.debug("runFiltering rules got called 1");
	 String whereClauseField ;
	 String whereClause = null;
	 String filterString ;
	 String label = null;
	 boolean continueFlag = false;
	 HashSet<String> fieldsSet = null;
	 String lineNumber = "";
	// List fieldVector = FieldListContainer.getChooserFieldValues();
	 String fieldName = new String();

	 // Ariba 8.1: List::elements() is deprecated by AbstractList::iterator()
	 //for (Enumeration fieldEnum = fieldVector.elements();fieldEnum.hasMoreElements();)
	 for (Iterator fieldItr = fieldVector.iterator();fieldItr.hasNext();)
	 {
	  try {
	   if (appType.equals("PaymentHeader") ||appType.equals("PaymentLine") ||appType.equals("PaymentAccounting") )
	                    whereClauseField = "FieldTable.PayWhereClause";
	   else if (appType.equals("BuysenseOrg"))  whereClauseField = "FieldTable.WhereClause1";
	   else whereClauseField = "FieldTable.ReqWhereClause";

	   // Ariba 8.1: changed to Iterator
	   fieldName = (String)fieldItr.next();
	   //Log.customer.debug("Working on the field: %s", fieldName );
	   BaseObject baseObj = (BaseObject)approvable.getDottedFieldValue(fieldName);
	   if (baseObj == null){ Log.customer.debug("Field %s has null value",fieldName); }
	   else {

	     whereClause = (String) baseObj.getDottedFieldValue(whereClauseField);
	     label = (String) baseObj.getDottedFieldValue("FieldTable.ERPValue");
	     if (whereClause == null || whereClause.length() == 0 ) {Log.customer.debug("Got a null where clause for %s",fieldName);}
	     else {
	        //Log.customer.debug("Where clause for field: %s is : %s",fieldName, whereClause);
	        String fieldNum = (String)approvable.getDottedFieldValue(fieldName+".FieldNumber");
	       // String clientUniquename = (String)approvable.getDottedFieldValue("ClientName.UniqueName");

	        List tokensVector = BuysenseUtil.tokenize(whereClause);
	        filterString = new String() ;
	        // Ariba 8.1: We are going from using Enumerations to Iterators since List::elements() has been deprecated
	        for (Iterator e = tokensVector.iterator();e.hasNext();) {
	        	// Ariba 8.1: Changed from to e.next() because we are using Iterators now instead of Enumerations
	            fieldVector = (List)e.next();
	            /* Element 1 -> fieldName
	            Element 2 -> Offset
	            Element 3 -> length  */

	            // String fieldValue = (String)approvable.getDottedFieldValue((String) fieldVector.elementAt(0));
	            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
	            String fieldValue = BuysenseUtil.findFieldValue((String) fieldVector.get(0), approvable,appType);
	            //Log.customer.debug("Field value: %s",fieldValue);

	            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
	            int offset = (new Integer((String)fieldVector.get(1))).intValue();
	            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
	            BigDecimal fl = new BigDecimal((String)fieldVector.get(2));
	            int fieldLength = fl.intValue();
	            fieldValue = fieldValue.substring(offset);
	            //Log.customer.debug("Field value from offset: %s",fieldValue);
	            BigDecimal valueLength = new BigDecimal(fieldValue.length());
	            if ((valueLength.compareTo(fl)) < 0){
	                // The fieldValue is less than the designated fieldLength, pad the value with '??'.
	                while (fieldValue.length() < fieldLength) fieldValue+= "?";
	                filterString = filterString + fieldValue;
	            }
	            else if (valueLength.compareTo(fl) > 0){
	                // The fieldValue is greater than the designated fieldLength, trim the value.
	                filterString  += fieldValue.substring(offset,(offset+fieldLength));
	            }
	            else{
	                filterString = filterString + fieldValue;
	            }
	            Log.customer.debug("Buysense: The filter string: %s",filterString);
	        }

	       String fieldValue = clientUniquename+":"+fieldNum+":" + filterString;
	       String fieldUniqueName = (String)baseObj.getDottedFieldValue("UniqueName");
	       Log.customer.debug("This is the current value of the %s : %s",fieldName,fieldUniqueName);
	       String compareString = fieldUniqueName.substring(0,fieldValue.length());
	       Log.customer.debug("Value of field:%s should match to :%s",fieldName,compareString);
	       if (!fieldValue.equals(compareString) && !continueFlag ) {
	    	   if(approvable instanceof ariba.common.core.SplitAccounting){
	    		   Log.customer.debug(" treeMap :: "+treeMap.size());
	    		   ariba.common.core.SplitAccounting spAccItem = (ariba.common.core.SplitAccounting)approvable;
	    		   lineNumber = "Line:"+spAccItem.getLineItem().getNumberInCollection();
				if(treeMap.size() == 0){
					fieldsSet = new HashSet<String>();
					fieldsSet.add(label);
					treeMap.put("Line:"+spAccItem.getLineItem().getNumberInCollection(),fieldsSet);
				}else{
					if(treeMap.get("Line:"+spAccItem.getLineItem().getNumberInCollection())!=null){
						treeMap.get("Line:"+spAccItem.getLineItem().getNumberInCollection()).add(label);
					}else{
						fieldsSet = new HashSet<String>();
						fieldsSet.add(label);
						treeMap.put("Line:"+spAccItem.getLineItem().getNumberInCollection(), fieldsSet);
					}
		           }
	       }else{
	    	   String errorString = "Field: " + label + " value is not being accepted by the system due to selection method. "+
	           "Since this field is dependent on a value in another field, the value must be selected by using the 'Search for More...' link," +
	           " and not by keying or selecting it from the drop-down menu.";
	    	   Log.customer.debug(" errorString :: "+approvable+errorString);
	           //return ListUtil.list(Constants.getInteger(-1),errorString); 
	       }
	           /*//CSPL-4292:Change the error message text for Dependent Data Submit edit. Updated error message.
	         String errorString = "Field: " + label + " value is not being accepted by the system due to selection method. "+
	       "Since this field is dependent on a value in another field, the value must be selected by using the 'Search for More...' link," +
	       " and not by keying or selecting it from the drop-down menu.";
	         return ListUtil.list(Constants.getInteger(-1),errorString);*/
	       }
	     }

	   }
	  }
	 catch (StringIndexOutOfBoundsException sbe) {
	            Log.customer.debug("Got a string out of bound exception. This happens when the whereClause" +
	                          "specification and the field value are not in synch. We will raise this "+
	                          " as an error.");
	             String errorString = "Field: " + label + " has an invalid value. Please select another value." +
	                                    "Field is not long enough.";
	             //return ListUtil.list(Constants.getInteger(-1),errorString);
	            if(treeMap.get(lineNumber)!=null){
						treeMap.get(lineNumber).add(label);
				}else{
						fieldsSet = new HashSet<String>();
						fieldsSet.add(label);
						treeMap.put(lineNumber, fieldsSet);
				}
	            return treeMap;
	    }
	 catch (NullPointerException npe) {
	            Log.customer.debug("Got a NullPointer Exception. This happens when a source field" +
	                          "contains null. We will let this go as we do not want to enforce this like "+
	                          "a validity constraint");

	            String errorString = "Field: " + label + " has an invalid value. Please select another value." +
	                                  "Missing values.";
	           // return ListUtil.list(Constants.getInteger(-1),errorString);
	            if(treeMap.get(lineNumber)!=null){
					treeMap.get(lineNumber).add(label);
	            }else{
					fieldsSet = new HashSet<String>();
					fieldsSet.add(label);
					treeMap.put(lineNumber, fieldsSet);
	            }
	            return treeMap;
	  }

	 }
	 Log.customer.debug(" treeMap :: "+treeMap.size());
	 return treeMap;
	//return NoErrorResult;

}
}