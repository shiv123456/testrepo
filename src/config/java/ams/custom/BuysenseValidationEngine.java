/*
    Copyright (c) 1996-1999 Ariba/Buysense, Inc.
    Falahyar March 2000
    --------------------------------------------------------------------------------------------------
   RLee, 5/10/02: eVA Dev SPL# 62: java error in eVAReqSubmitHook.java
   Added check for zero line item in line 241.
*/

/* 09/05/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/

package config.java.ams.custom;

import ariba.purchasing.core.ReqLineItem;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;


import ariba.base.core.ClusterRoot;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.Partition;
//import ariba.common.base.Variant;
import ariba.approvable.core.Approvable;
import ariba.util.log.Log;
// Ariba 8.0: Replaced Deprecated Util with StringUtil.
import ariba.util.core.StringUtil;
//import ariba.util.core.Util;
import java.util.List;
// Ariba 8.1: Various methods have changed from the java.util.List package to the ariba.util.core.ListUtil package.
import ariba.util.core.ListUtil;

// Ariba 8.1: Use Iterators instead of Enumerations
import java.util.Iterator;

// Ariba 8.0: Deprecations, methods have changed packages.
import ariba.util.core.Constants;

//81->822 changed Vector to List
public class BuysenseValidationEngine
{

    //BuysenseValidationEngine
    //
    // July 2001
    //
    //The BuysenseValidationEngine implements the dynamic checking of agency required fields.
    //The required fields are designated in the BuysenseFieldTable boolean value Required/NotRequired
    //The program creates local version of the FieldList containers, and creates a AQL of required
    //fields for the agency in the user submitting the approvable belongs to. The logic then loops
    //thru the local FieldList containers looking for a match.  Once a match is found, the value of
    //the matching field on the approvable is checked for a null or empty string value. If a null
    //or empty string value is found a error message is generated.
    //
    //
    //The BuysenseValidationEngine is called from the AMSReqSubmitHook and AMSReqCheckinHook programs
    //after the BuyseneRulesEngine.
    //
    //The following methods are contained in the BuysenseValidationEngine:
    //
    //runReqHeaderValidation - Checks the Approvable Header Fields
    //
    //runReqLineValidation - Checks the Approvable Line Fields
    //
    //runReqAcctValidation - Checks the Approvable Accounting Line Fields
    //

    // Ariba 8.0: Replaced deprecated method
    private static final List NoErrorResult = ListUtil.list(Constants.getInteger(0));

    String clientName = null;

    String errorMessage = "";

    // Ariba 8.1: Replaced deprecated new List() Constructor with ListUtil.newVector();
    List fieldTableVector = ListUtil.list();

    // Private strings for local FieldList Containers
    private static String [] local_HeaderFields =
        {
        "ReqHeadField1",
            "ReqHeadField2","ReqHeadField3","ReqHeadField4","ReqHeadField5","ReqHeadField6","ReqHeadText1",
            "ReqHeadText2",
            "ReqHeadText3","ReqHeadText4","ReqHeadCB1","ReqHeadCB2","ReqHeadCB3",
            "ReqHeadCB4","ReqHeadCB5"
    };

    private static String [] local_HeaderFieldValues =
        {
        "ReqHeadFieldDefault1","ReqHeadFieldDefault2","ReqHeadFieldDefault3",
            "ReqHeadFieldDefault4","ReqHeadFieldDefault5","ReqHeadFieldDefault6",
            "ReqHeadText1Value","ReqHeadText2Value","ReqHeadText3Value",
            "ReqHeadText4Value",
            "ReqHeadCB1Value","ReqHeadCB2Value","ReqHeadCB3Value",
            "ReqHeadCB4Value","ReqHeadCB5Value"
    };

    private static String [] local_LineFields =
        {
        "ReqLineField1",
            "ReqLineField2","ReqLineField3","ReqLineField4","ReqLineText1",
            "ReqLineText2",
            "ReqLineText3","ReqLineText4","ReqLineCB1","ReqLineCB2"
    };

    private static String [] local_LineFieldValues =
        {
        "ReqLineFieldDefault1","ReqLineFieldDefault2","ReqLineFieldDefault3",
            "ReqLineFieldDefault4",
            "ReqLineText1Value","ReqLineText2Value","ReqLineText3Value",
            "ReqLineText4Value","ReqLineCB1Value","ReqLineCB2Value"
    };

    private static String [] local_AcctgFields =
        {
        "Field1","Field2","Field3","Field4","Field5","Field6","Field7","Field8",
            "Field9",
            "Field10","Field11","Field12","Field13","Field14","Field15",
            "Field16",
            "Field17",
            "Field18","Field19","Field20","Field21","Field22","Field23",
            "Field24","Field25","LineAcctText1","LineAcctText2",
            "LineAcctText3","LineAcctText4","LineAcctText5","LineAcctText6",
            "LineAcctText7",
            "LineAcctText8","LineAcctText9","LineAcctText10","LineAcctCB1",
            "LineAcctCB2","LineAcctCB3","LineAcctCB4","LineAcctCB5"
    };

    private static String [] local_AcctgFieldValues =
        {
        "FieldDefault1",
            "FieldDefault2","FieldDefault3","FieldDefault4","FieldDefault5",
            "FieldDefault6",
            "FieldDefault7","FieldDefault8","FieldDefault9","FieldDefault10",
            "FieldDefault11",
            "FieldDefault12","FieldDefault13","FieldDefault14","FieldDefault15",
            "FieldDefault16",
            "FieldDefault17","FieldDefault18","FieldDefault19","FieldDefault20",
            "FieldDefault21",
            "FieldDefault22","FieldDefault23","FieldDefault24","FieldDefault25",
            "LineAcctText1Value",
            "LineAcctText2Value",
            "LineAcctText3Value","LineAcctText4Value","LineAcctText5Value",
            "LineAcctText6Value",
            "LineAcctText7Value","LineAcctText8Value","LineAcctText9Value",
            "LineAcctText10Value",
            "LineAcctCB1Value",
            "LineAcctCB2Value","LineAcctCB3Value","LineAcctCB4Value",
            "LineAcctCB5Value"
    };




    public List runReqHeaderValidation(Approvable approvable)
    {

        Log.customer.debug("RLee, 122: In runReqHeaderValidation of BuysenseValidationEngine");

        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        ariba.user.core.User RequesterUser = (ariba.user.core.User)approvable.getDottedFieldValue("Requester");
        //rgiesen dev SPL #39
        Partition appPartition = approvable.getPartition();
        //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,appPartition);
        clientName = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.Name");
        String clientUniqueName = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName");

        /* Ariba 8.0: The constructor for AQLOptions has been deprecated.
           No longer need to pass a second boolean parameter.  */
        //AQLOptions fieldTableOptions = new AQLOptions(Base.getSession().getPartition(),true);
        //rgiesen dev SPL #39
        //AQLOptions fieldTableOptions = new AQLOptions(Base.getSession().getPartition());
        AQLOptions fieldTableOptions = new AQLOptions(appPartition);
        AQLQuery fieldTableQuery = new AQLQuery("ariba.core.BuysenseFieldTable",
                                                true);
        fieldTableQuery.andEqual("RequiredNotRequired",new Boolean(true));
        fieldTableQuery.andEqual("ClientName", clientUniqueName );

        /* Ariba 8.0: Base.objectsMatching() was deprecated by Base.executeQuery().
           Changed code to reflect this. */
        AQLResultCollection fieldTableResults = Base.getService().executeQuery(fieldTableQuery, fieldTableOptions);

        //Check if the AQL result set is empty, if empty exit.

        boolean bAQLResult = fieldTableResults.isEmpty();

        if (bAQLResult)
        {
            Log.customer.debug("AQL fieldTableQuery RETURNED NO ROWS");
            return NoErrorResult;

        }


        //We create a vector of clusterroots and pass that around

        while (fieldTableResults.next())
        {
            //Get the field we are looking for from the AQL query
            BaseId bid  = (BaseId)fieldTableResults.getBaseId(0) ;
            ClusterRoot fieldTable = (ClusterRoot) bid.get();
            // Ariba 8.1: Replaced deprecated addElement() method with add() method.
            fieldTableVector.add(fieldTable);
            String fieldName = (String)fieldTable.getFieldValue("Field") ;
            Log.customer.debug("Adding it to the vector: %s",
                           fieldTable.getFieldValue("Field") );
        }

        //Create List of items from the local field containers

        // Ariba 8.1: Replaced deprecated new List() Constructor with ListUtil.newVector();
        List reqHeaderFieldsValuesVector = ListUtil.arrayToList(local_HeaderFieldValues);
        // Ariba 8.1: Replaced deprecated new List() Constructor with ListUtil.newVector();
        List reqHeaderFieldsVector = ListUtil.arrayToList(local_HeaderFields);



        //Loop thru the AQL Query Results
        Log.customer.debug("reqHeaderFieldsVector Size: %s",
                       reqHeaderFieldsVector.size());
        Log.customer.debug("reqHeaderFieldsValuesVector Size: %s",
                       reqHeaderFieldsValuesVector.size());

        // Ariba 8.1: List::elements() is deprecated by AbstractList::iterator().
        //for (Enumeration fieldTableEnum = fieldTableVector.elements();fieldTableEnum.hasMoreElements();)
        for (Iterator fieldTableEnum = fieldTableVector.iterator();fieldTableEnum.hasNext();)
        {
            //Get the field we are looking for from the AQL query
            // Ariba 8.1: changed Enumeration nextElement() to Iterator next().
            ClusterRoot fieldTable = (ClusterRoot) fieldTableEnum.next();
            String fieldName = (String)fieldTable.getFieldValue("Field") ;
            Log.customer.debug("Looking for the AQL field: %s",
                           fieldTable.getFieldValue("Field") );

            //Loop thru the reqHeaderFieldsVector
            int vectorSize = reqHeaderFieldsVector.size();
            for(int i=0;i<vectorSize;i++)
            {
                //Check the reqHeaderFieldsVector for Field Value
                // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                String reqHeaderFieldValue = (String)reqHeaderFieldsValuesVector.get(i);
                // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                String reqHeaderFieldName = (String)reqHeaderFieldsVector.get(i);

                Log.customer.debug("Checking on the ReqHeader field: %s",
                               reqHeaderFieldName );
                //Log.customer.debug("Working on the ReqHeader value: %s", reqHeaderFieldValue );

                //Issue error if field is Null, using the ERPValue ie.
                // "Entity is required for Agency: A151DOA"

                if ( reqHeaderFieldName.equals (fieldName) )
                {
                    Log.customer.debug("reqHeaderFieldName: %s",
                                   reqHeaderFieldName );
                    Log.customer.debug("reqHeaderFieldValue: %s",
                                   ( approvable.getDottedFieldValue (reqHeaderFieldValue )) );
                    Log.customer.debug("fieldname: %s", fieldName );

                    if ( approvable.getDottedFieldValue (reqHeaderFieldValue ) == null )
                    {
                        Log.customer.debug("%s is Null", fieldName);
                        errorMessage = errorMessage + fieldTable.getDottedFieldValue("ERPValue") + ", ";
                    }

                    //Check if the required field is an empty or null string
                    //Just checking for null does not trigger an error on a required
                    //textbox.

                    // Ariba 8.0: Replaced deprecated method
                    if ((approvable.getDottedFieldValue (reqHeaderFieldValue ) instanceof String) && StringUtil.nullOrEmptyOrBlankString((String)approvable.getDottedFieldValue (reqHeaderFieldValue)))
                    {
                        Log.customer.debug("regHeaderFieldValue is instance of String: %s", reqHeaderFieldValue );
                        Log.customer.debug("%s is a Empty String ",
                                       (String)reqHeaderFieldValue);
                        errorMessage = errorMessage + fieldTable.getDottedFieldValue("ERPValue") + ", ";
                    }

                    break;
                }

            }
        }

        return( runReqLineValidation(approvable, fieldTableResults));

    }


    private List runReqLineValidation(Approvable approvable,
                                        AQLResultCollection fieldTableResults)
    {

        Log.customer.debug("In runReqLineValidation of BuysenseValidationEngine");
        // Ariba 8.1: Replaced deprecated new List() Constructor with ListUtil.newVector();
        // The following 3 lines were modified accordingly.
        List retVector = ListUtil.list();
        List reqLineFieldsValuesVector = ListUtil.arrayToList(local_LineFieldValues);
        List reqLineFieldsVector = ListUtil.arrayToList(local_LineFields);
        List lineItems = (List)approvable.getFieldValue("LineItems");

        Log.customer.debug("reqLineFieldsVector Size: %s",
                       reqLineFieldsVector.size());
        Log.customer.debug("reqLineFieldsValuesVector Size: %s",
                       reqLineFieldsValuesVector.size());


        int lineItemsVectorSize = lineItems.size();

        Log.customer.debug("lineItemsVector Size: %s", lineItemsVectorSize);

        if (lineItemsVectorSize < 1)
        {
            // Ariba 8.0: Replaced deprecated method
            return ListUtil.list(Constants.getInteger(-1),
                                     "This requisition has no line item!");
        }
        else
        {

            // Ariba 8.1: Replaced deprecated List count() method with size() method.
            for (int i=0; i<lineItems.size(); i++)
            {
                Log.customer.debug("In line Items For Loop");

                // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                ReqLineItem li = (ReqLineItem)lineItems.get(i);
                Log.customer.debug ("ReqLineItem li is: %s", li);

                // Ariba 8.1: List::elements() is deprecated by AbstractList::iterator().
                //for (Enumeration fieldTableEnum = fieldTableVector.elements();fieldTableEnum.hasMoreElements();)
                for (Iterator fieldTableEnum = fieldTableVector.iterator();fieldTableEnum.hasNext();)

                {
                    //Get the field we are looking for from the AQL query
                    // Ariba 8.1: changed Enumeration nextElement() to Iterator next().
                    ClusterRoot fieldTable = (ClusterRoot) fieldTableEnum.next();
                    String fieldName = (String)fieldTable.getFieldValue("Field") ;
                    Log.customer.debug("Looking for the Line AQL field: %s",
                                   fieldTable.getFieldValue("Field") );

                    //Loop thru the reqLineFieldsVector
                    int vectorSize = reqLineFieldsVector.size();
                    for(int j=0;j<vectorSize;j++)
                    {
                        // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                        String reqLineFieldValue = (String)reqLineFieldsValuesVector.get(j);
                        // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                        String reqLineFieldName = (String) reqLineFieldsVector.get(j);

                        Log.customer.debug("Checking on the ReqLine field: %s",
                                       reqLineFieldName );
                        Log.customer.debug("Working on the ReqLine value: %s",
                                       reqLineFieldValue );

                        if ( reqLineFieldName.equals ( fieldName ) )
                        {
                            Log.customer.debug("***reqLineFieldName: %s***",
                                           reqLineFieldName );
                            Log.customer.debug("***reqLineFieldValue: %s***",
                                           ( li.getDottedFieldValue (reqLineFieldValue ) ));
                            Log.customer.debug("***fieldname: %s***", fieldName );

                            if (li.getDottedFieldValue(reqLineFieldValue) == null)
                            {
                                Log.customer.debug("%s is Null", fieldName);
                                errorMessage = errorMessage + fieldTable.getDottedFieldValue("ERPValue") + ", ";
                            }

                            // Ariba 8.0: Replaced deprecated method
                            if ((li.getDottedFieldValue (reqLineFieldValue ) instanceof String) && StringUtil.nullOrEmptyOrBlankString((String)li.getDottedFieldValue (reqLineFieldValue)))
                            {
                                Log.customer.debug("regLineFieldValue is instance of String: %s", reqLineFieldValue );
                                //Log.customer.debug("regLineFieldValue Trimmed String is Empty: %s", new Boolean(approvable.getDottedFieldValue(reqLineFieldValue.trim()).equals("")));
                                Log.customer.debug("%s is a Empty String ",
                                               (String)reqLineFieldValue);
                                errorMessage = errorMessage + fieldTable.getDottedFieldValue("ERPValue") + ", ";
                            }
                        }
                    }


                }

                retVector = runReqAcctValidation(li, fieldTableResults);
                // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                if (((Integer)retVector.get(0)).intValue() == -1)
                {
                    return retVector;
                }
            }
        }
        return retVector;
    }

    private List runReqAcctValidation(BaseObject li,
                                        AQLResultCollection fieldTableResults)
    {
        Log.customer.debug("In runReqAcctValidation of BuysenseValidationEngine");

        // Ariba 8.1: Replaced deprecated new List() Constructor with ListUtil.newVector();
        List acctFieldsValuesVector = ListUtil.arrayToList(local_AcctgFieldValues);
        // Ariba 8.1: Replaced deprecated new List() Constructor with ListUtil.newVector();
        List acctFieldsVector = ListUtil.arrayToList(local_AcctgFields);
        List SplitAccounting = (List)li.getDottedFieldValue("Accountings.SplitAccountings");

        Log.customer.debug("acctFieldsVector Size: %s", acctFieldsVector.size());
        Log.customer.debug("acctFieldsValuesVector Size: %s",
                       acctFieldsValuesVector.size());


        // Ariba 8.1: Replaced deprecated List count() method with size() method.
        for (int j=0; j<SplitAccounting.size(); j++)
        {
            // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
            BaseObject reqAcctg = (BaseObject) SplitAccounting.get(j);

            // Ariba 8.1: List::elements() is deprecated by AbstractList::iterator().
            //for (Enumeration fieldTableEnum = fieldTableVector.elements();fieldTableEnum.hasMoreElements();)
            for (Iterator fieldTableEnum = fieldTableVector.iterator();fieldTableEnum.hasNext();)

            {
                //Get the field we are looking for from the AQL query
                // Ariba 8.1: changed Enumeration nextElement() to Iterator next().
                ClusterRoot fieldTable = (ClusterRoot) fieldTableEnum.next();
                String fieldName = (String)fieldTable.getFieldValue("Field") ;
                Log.customer.debug("Looking for the Acct AQL field: %s",
                               fieldTable.getFieldValue("Field") );

                //Loop thru the reqAcctLineFieldsVector
                int vectorSize = acctFieldsVector.size();
                for(int k=0;k<vectorSize;k++)
                {
                    // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                    String reqAcctLineFieldValue = (String) acctFieldsValuesVector.get(k);
                    // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                    String reqAcctLineFieldName = (String) acctFieldsVector.get(k);


                    Log.customer.debug("Checking on the ReqAcct field: %s",
                                   reqAcctLineFieldName );
                    //Log.customer.debug("Working on the ReqAcct value: %s", reqAcctLineFieldValue );

                    if ( reqAcctLineFieldName.equals ( fieldName ) )
                    {
                        Log.customer.debug("reqAcctLineFieldName: %s",
                                       reqAcctLineFieldName );
                        Log.customer.debug("reqAcctLineFieldValue: %s",
                                       ( reqAcctg.getFieldValue (reqAcctLineFieldValue ) ));
                        Log.customer.debug("fieldname: %s", fieldName );

                        if ( reqAcctg.getFieldValue(reqAcctLineFieldValue) == null )
                        {
                            Log.customer.debug("%s is Null", fieldName);
                            errorMessage = errorMessage + fieldTable.getDottedFieldValue("ERPValue") + ", ";
                        }

                        // Ariba 8.0: Replaced deprecated method
                        if ((reqAcctg.getFieldValue (reqAcctLineFieldValue ) instanceof String) && StringUtil.nullOrEmptyOrBlankString((String) reqAcctg.getFieldValue (reqAcctLineFieldValue)))
                        {
                            Log.customer.debug("regAcctLineFieldValue is instance of String: %s", reqAcctLineFieldValue );
                            Log.customer.debug("%s is a Empty String ",
                                           (String)reqAcctLineFieldValue);
                            errorMessage = errorMessage + fieldTable.getDottedFieldValue("ERPValue") + ", ";
                        }
                    }
                }
            }
        }


        // Return concatenated error messages
        if (!(StringUtil.nullOrEmptyOrBlankString(errorMessage)))
        {
            //int ierrorMessageLength  = errorMessage.length
            Log.customer.debug("ErrorMessage.length: %s", errorMessage.length ());
            errorMessage = errorMessage.substring(0,
                                                  (errorMessage.length() - 2));
            // Ariba 8.0: Replaced deprecated method
            return ListUtil.list(Constants.getInteger(-1),
                                     "ERROR: " + clientName + " requires the following field(s): " + errorMessage + "." );
        }

        return NoErrorResult;

    }

}






