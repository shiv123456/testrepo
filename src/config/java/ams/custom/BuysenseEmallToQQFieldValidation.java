package config.java.ams.custom;
// Checks the field validation of ReqHeadCB3Value and ReqHeadCB5Value. If both are checked it throws error
import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ConditionResult;
import ariba.base.fields.ValueInfo;
import ariba.util.core.Fmt;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseEmallToQQFieldValidation extends Condition
{
    private static final ValueInfo parameterInfo[];
    protected String             sResourceFile = "ariba.procure.core";
    String msMessage = null;
    
    public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
    {
        Log.customer.debug("Calling BuysenseEmallToQQFieldValidation for ReqHeadCB5Value validation.");
        try
        {
            if(!evaluate(value, params))
            {
                String retMsg = null;
                if(!StringUtil.nullOrEmptyOrBlankString(msMessage))
                {
                    retMsg = msMessage;
                    msMessage = null;
                    return new ConditionResult(retMsg);
                }
                else
                    return null;
            }
        }
        catch (ConditionEvaluationException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
    public boolean evaluate(Object value, PropertyTable params) throws ConditionEvaluationException
    {

        BaseObject obj   = (BaseObject)params.getPropertyForKey("SourceObject");
        Log.customer.debug("inside BuysenseEmallToQQFieldValidation SourceObject"+obj);
        
        if(obj.instanceOf("ariba.purchasing.core.Requisition"))
        {
            Log.customer.debug("inside BuysenseEmallToQQFieldValidation");
            Boolean bIsReqHeadcb3 = (Boolean) obj.getFieldValue("ReqHeadCB3Value");
            Boolean bIsReqHeadcb5 = (Boolean) obj.getFieldValue("ReqHeadCB5Value");
            
            if(bIsReqHeadcb3!=null && bIsReqHeadcb3.booleanValue() && bIsReqHeadcb5!=null && bIsReqHeadcb5.booleanValue())
            { 
                Log.customer.debug("inside BuysenseEmallToQQFieldValidation ");
                msMessage = Fmt.Sil(Base.getSession().getLocale(),sResourceFile, "QQForceNBypass");
                return false;
            }   
 
        }
        else
        {
            Log.customer.debug("inside ariba.purchasing.core.Requisition");
            return false;
        }
        return false; 
    }         
    

    
    protected ValueInfo[] getParameterInfo()
    {
       return parameterInfo;
    }   
    static
    {
         parameterInfo = (new ValueInfo[] {new ValueInfo("SourceObject", 0)});
    }
}

