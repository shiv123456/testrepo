package config.java.ams.custom;

import ariba.base.core.BaseObject;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.procure.core.CategoryLineItemDetails;
import ariba.procure.core.ProcureLineItem;
import ariba.purchasing.core.ReqLineItem;
import ariba.util.core.PropertyTable;
import ariba.workforce.core.LaborLineItemDetails;
import ariba.util.log.Log;

public class BuysenseCheckLaborLineItem extends Condition
{
	  private static final ValueInfo parameterInfo[];

	  public boolean evaluate(Object value, PropertyTable params)throws ConditionEvaluationException
	  {
		  Log.customer.debug("BuysenseCheckLaborLineItem::entered");
		   BaseObject obj   = (BaseObject)params.getPropertyForKey("SourceObject");
		   if(obj != null && obj instanceof ProcureLineItem)
		   {
			   CategoryLineItemDetails loCategoryLineItemDetails = ((ReqLineItem)obj).getCategoryLineItemDetails();
			   if(loCategoryLineItemDetails != null && loCategoryLineItemDetails instanceof LaborLineItemDetails)
			   {
				   return true;
			   }
			   else
			   {
				   return false;
			   }
		   }
		   Log.customer.debug("BuysenseCheckLaborLineItem::evaluate returning false1");
		   return false;
	  }
		   
	  protected ValueInfo[] getParameterInfo()
	  {
	        return parameterInfo;
	  }

	  static
      {
          parameterInfo = (new ValueInfo[] {
             new ValueInfo("SourceObject", 0)
            });
       }
  }