package config.java.ams.custom;

import java.util.List;

import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueInfo;
import ariba.base.fields.ValueSource;
import ariba.base.meta.core.FieldMeta;
import ariba.util.core.ListUtil;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseUMDMWSPreLoad extends Action
{
    private String              sClassName         = "BuysenseUMDMWSPreLoad";
    protected ClusterRoot       clusterRootObj;

    private static final String SERVICEREFNAME_CSV = "ServiceRefName";
    protected String            sServiceRefKey     = " ";

    @Override
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        log(sClassName + " fire called " + valuesource);
        List<String>      lsExtrinsicFields  = ListUtil.list();
        if (valuesource == null || !(valuesource instanceof ClusterRoot))
        {
            return;
        }

        clusterRootObj = (ClusterRoot) valuesource;
        try
        {
            // Example for Address ServiceRefName is AddressService
            sServiceRefKey = propertytable.stringPropertyForKey(SERVICEREFNAME_CSV);

            //Check if any object exists with same Lookup Key (UniqueName). If exists, then clear all its extrinsic field values

            log(sClassName + "Searching for " + clusterRootObj.getClassProperties().getTypeName() + ", " + clusterRootObj.getPartition() + ", "
                    + clusterRootObj.getUniqueName());

            ClusterRoot previousObj = Base.getService().objectMatchingUniqueName(clusterRootObj.getClassProperties().getTypeName(),
                    clusterRootObj.getPartition(), clusterRootObj.getUniqueName());
            log(sClassName + "previousObj"+ previousObj);
            if (previousObj == null)
            {
                //no object was created before hence no need to clear extrinsic fields. 
                return;
            }
            
            FieldMeta extrinsicMetas[] = previousObj.getBaseMeta().getExtrinsics();
            for (int i = 0; i < extrinsicMetas.length; i++)
            {
                FieldMeta lFieldMeta = extrinsicMetas[i];
                lsExtrinsicFields.add(lFieldMeta.name());
            }

            if(lsExtrinsicFields == null || lsExtrinsicFields.isEmpty())
            {
                return;
            }
            
            //clear all extrinsic fields
            for (int k = 0; k < lsExtrinsicFields.size(); k++)
            {
                String sField = lsExtrinsicFields.get(k);
                previousObj.setDottedFieldValue(sField, null);
            }
            previousObj.save();
        }
        catch (Exception e)
        {
            log(sClassName +" EXCEPTION "+e.getMessage());
        }

    }

    protected void log(String s)
    {
        Log.customer.debug(s);
    }

    protected ValueInfo getValueInfo()
    {
        return valueInfo;
    }

    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    private static final ValueInfo   valueInfo     = new ValueInfo(0);
    private static final ValueInfo[] parameterInfo = { new ValueInfo(SERVICEREFNAME_CSV, IsScalar, StringClass) };

}
