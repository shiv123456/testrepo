package config.java.ams.custom;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.sql.*;

import ariba.approvable.core.ApprovableType;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseVector;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.core.aql.AQLClassReference;
import ariba.htmlui.orms.portlets.BuyerPortletContent;
import ariba.user.core.Permission;
import ariba.user.core.User;
import ariba.util.core.ListUtil;
import ariba.util.core.MapUtil;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;
import ariba.util.parameters.Parameters;

public class BuysenseEformLinksCustomPortletContent extends BuyerPortletContent
{
    /*((Application) application).putWellKnownPage("CreateDynamicEform", "ariba.core.BuysDynamicEform");
    ((Application) application).putWellKnownPage("createbuyseform", "ariba.core.BuysenseOrgEform");
    ((Application) application).putWellKnownPage("createevaeueform", "ariba.core.BuysenseUserProfileRequest");
    ((Application) application).putWellKnownPage("createvmiameform", "ariba.core.BuysenseVMIAssetTracking");
    ((Application) application).putWellKnownPage("createdpexeform", "ariba.core.BuysenseExemptionRequest");
    ((Application) application).putWellKnownPage("createoduemeform", "ariba.core.BuysenseODUEmergencyProcurement");
    ((Application) application).putWellKnownPage("createodusseform", "ariba.core.BuysenseODUSoleSourceRequest");
    ((Application) application).putWellKnownPage("createdgssseform", "ariba.core.BuysenseSoleSourceRequest");*/

    public static final String   ClassName           = "BuysenseEformLinksCustomPortletContent";

    public static ApprovableType _eFormApprovable;
    public static ClusterRoot    _eformProf;

    String                       sHost               = (String) Parameters.getInstance().getParameter("System.Base.Host");
    String                       sPartialAccessURL   = "https://" + sHost
                                                             + "/Buyer/Main/ad/webjumper?passwordadapter=SingleSignOnWSAdapter&COVAPage=yes";
    String                       sDefaultHomePageURL = "https://" + sHost + "/Buyer/Main/";

    User                         effectiveUser       = (User) Base.getSession().getEffectiveUser();
    Partition                    appPartition        = Base.getSession().getPartition();
    ariba.common.core.User       partitionUser       = ariba.common.core.User.getPartitionedUser(effectiveUser, appPartition);
    Map<String, String>          wellKnownPagesMap   = MapUtil.map();
    public Connection            moConnection;
    PreparedStatement            loAQLStatement;

    public BuysenseEformLinksCustomPortletContent()
    {
        wellKnownPagesMap.put("ariba.core.BuysDynamicEform", "CreateDynamicEform");
        wellKnownPagesMap.put("ariba.core.BuysenseOrgEform", "createbuyseform");
        wellKnownPagesMap.put("ariba.core.BuysenseUserProfileRequest", "createevaeueform");
        wellKnownPagesMap.put("ariba.core.BuysenseVMIAssetTracking", "createvmiameform");
        wellKnownPagesMap.put("ariba.core.BuysenseExemptionRequest", "createdpexeform");
        wellKnownPagesMap.put("ariba.core.BuysenseODUEmergencyProcurement", "createoduemeform");
        wellKnownPagesMap.put("ariba.core.BuysenseODUSoleSourceRequest", "createodusseform");
        wellKnownPagesMap.put("ariba.core.BuysenseSoleSourceRequest", "createdgssseform");
    }

    public String getContentType()
    {
        return "HTML";
    }

    public boolean useXmlEscaping()
    {
        return false;
    }

    public static ClusterRoot geteformProf()
    {
        return _eformProf;
    }

    public static void seteformProf(ClusterRoot _eformProf)
    {
        BuysenseEformLinksCustomPortletContent._eformProf = _eformProf;
    }

    public static ApprovableType geteFormApprovable()
    {
        return _eFormApprovable;
    }

    public static void seteFormApprovable(ApprovableType _eFormApprovable)
    {
        BuysenseEformLinksCustomPortletContent._eFormApprovable = _eFormApprovable;
    }

    public boolean hasDynamicEformAccess()
    {
        // Check if user has CreateElectronicForms Permission
        boolean hasElectronicFormsPermission = false;
        Partition partition = Base.getService().getPartition("pcsv");
        ApprovableType loBuysDynamicEform = (ApprovableType) Base.getService().objectMatchingUniqueName("ariba.approvable.core.ApprovableType",
                partition, "ariba.core.BuysDynamicEform");
        Permission eformAccessPermission = loBuysDynamicEform.getPermission();
        if (eformAccessPermission != null)
        {
            hasElectronicFormsPermission = effectiveUser.hasPermission(eformAccessPermission);
        }

        Log.customer.debug(ClassName + " in hasDynamicEformAccess() hasElectronicFormsPermission: " + hasElectronicFormsPermission);
        return hasElectronicFormsPermission;
    }

    public boolean hasEformProfiles()
    {

        BaseVector lvProfiles = (BaseVector) partitionUser.getFieldValue("BuysEformProfiles");
        if (lvProfiles != null && !lvProfiles.isEmpty())
        {
            return true;
        }
        return false;
    }

    public List<ClusterRoot> getUserBuysEformProfiles()
    {
        Map<String, ClusterRoot> loUserProfileMap = MapUtil.map();
        List<ClusterRoot> lMappedProfiles = ListUtil.list();
        BaseVector lvProfiles = (BaseVector) partitionUser.getFieldValue("BuysEformProfiles");
        if (lvProfiles != null)
        {

            for (int i = 0; i < lvProfiles.size(); i++)
            {
                ClusterRoot oProfile = Base.getSession().objectFromId((BaseId) lvProfiles.get(i));
                if (oProfile != null && oProfile.instanceOf("ariba.core.BuysEformProf"))
                {

                    loUserProfileMap.put((String) oProfile.getDottedFieldValue("Name"), oProfile);
                }
            }
        }

        List<String> lsProfNameArray = ListUtil.list();
        for (Iterator<String> e = loUserProfileMap.keySet().iterator(); e.hasNext();)
        {
            lsProfNameArray.add((String) e.next());
        }

        Collections.sort(lsProfNameArray, String.CASE_INSENSITIVE_ORDER);
        for (int i = 0; i < lsProfNameArray.size(); i++)
        {
            lMappedProfiles.add(loUserProfileMap.get(lsProfNameArray.get(i)));
        }

        return lMappedProfiles;
    }

    public String getDynamicEformAccessURL()
    {

        String sDynamicEformDirectAccessURL = sDefaultHomePageURL;
        String sEformUN = _eformProf.getUniqueName();
        sDynamicEformDirectAccessURL = StringUtil.nullOrEmptyOrBlankString(sEformUN) ? sDefaultHomePageURL : sPartialAccessURL
                + "&Page=CreateDynamicEform&profid=" + sEformUN;
        return sDynamicEformDirectAccessURL;
    }

    public String getDynamicEformUIName()
    {
        return (String) _eformProf.getDottedFieldValue("Name");
    }

    public List<ApprovableType> eformApprovableList()
    {
        List<ApprovableType> loEformApprovables = ListUtil.list();
        Map<String, ApprovableType> loApprovableTypeMap = MapUtil.map();

        Partition partition = ariba.base.core.Base.getService().getPartition("pcsv");
        AQLClassReference adcr = new AQLClassReference("ariba.approvable.core.ApprovableType", false, partition.vector());
        AQLQuery query = new AQLQuery(adcr);
        AQLOptions options = new AQLOptions();
        query.andEqual("Active", Boolean.TRUE);
        query.andEqual("ModuleName", "Eform");
        query.addOrderByElement(adcr.buildField("Name"), true);
        AQLResultCollection resultCollection = Base.getService().getQueryServer().objectsMatching(query, options);
        if (resultCollection.getFirstError() != null)
        {
            return loEformApprovables;
        }
        while (resultCollection.next())
        {
            BaseId id = resultCollection.getBaseId(0);
            ApprovableType apr = (ApprovableType) id.get();

            //exclude BuysDynamicEform and eforms user do not have access to
            if (apr != null && !apr.getUniqueName().equals("ariba.core.BuysDynamicEform"))
            {
                Permission eformAccessPermission = apr.getPermission();
                if (eformAccessPermission != null && effectiveUser.hasPermission(eformAccessPermission))
                {
                    loApprovableTypeMap.put((String) apr.getDottedFieldValue("Name"), apr);
                }
            }

        }

        List<String> lsApprTypeNameArray = ListUtil.list();
        for (Iterator<String> e = loApprovableTypeMap.keySet().iterator(); e.hasNext();)
        {
            lsApprTypeNameArray.add((String) e.next());
        }

        Collections.sort(lsApprTypeNameArray, String.CASE_INSENSITIVE_ORDER);
        for (int i = 0; i < lsApprTypeNameArray.size(); i++)
        {
            loEformApprovables.add(loApprovableTypeMap.get(lsApprTypeNameArray.get(i)));
        }

        return loEformApprovables;
    }

    public String getEformAccessURL()
    {
        String sWellKnownPage = "";
        String sEformDirectAccessURL = "";
        String sEformUN = _eFormApprovable.getUniqueName();

        if (!StringUtil.nullOrEmptyOrBlankString(sEformUN))
        {
            sWellKnownPage = wellKnownPagesMap.get(sEformUN);
        }
        sEformDirectAccessURL = StringUtil.nullOrEmptyOrBlankString(sWellKnownPage) ? sDefaultHomePageURL : sPartialAccessURL + "&Page="
                + sWellKnownPage;
        return sEformDirectAccessURL;
    }

    public String getEformUIName()
    {
        return _eFormApprovable.getName();
    }

}
