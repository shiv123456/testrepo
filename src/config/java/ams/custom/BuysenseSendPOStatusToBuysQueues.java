package config.java.ams.custom;

import java.util.List;

//import org.apache.log4j.Level;

import com.cgi.evamq.common.EvaQueueMessage;

import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.purchasing.core.POLineItem;
import ariba.purchasing.core.PurchaseOrder;
import ariba.purchasing.core.Requisition;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseSendPOStatusToBuysQueues extends Action
{
    private String msCN = "BuysenseSendPOStatusToBuysQueues";

    public void fire(ValueSource object, PropertyTable param) throws ActionExecutionException
    {
        //Level currLevel = Log.customer.getLevel(); 

        try
        {
            //Log.customer.setLevel(Level.DEBUG);
            
            Log.customer.debug("Calling " + msCN);
            if (object != null)
            {
                Log.customer.debug(msCN + " object is not null");
            }           
            PurchaseOrder POOrder = (PurchaseOrder) object;

            // create and set values that we want to pass in message class
            EvaQueueMessage QueueMsg = new EvaQueueMessage();
            QueueMsg.setMsgCreator(EvaQueueMessage.APPLICATION_ARIBA);
            QueueMsg.setObjID(POOrder.getUniqueName());

            List lvLines = POOrder.getLineItems();
            Requisition loReq = (Requisition) ((POLineItem) lvLines.get(0)).getRequisition();
            QueueMsg.setDocSourceID(loReq.getUniqueName());

            // set ObjOriginID
            String initialID = (String) loReq.getDottedFieldValue("InitialUniqueName");

            String transactionSource = (String) loReq.getDottedFieldValue("TransactionSource");
            // 'QQ','EXT' or 'E2E'. check what happens on copied req
            Log.customer.debug(msCN + " : retrieve transaction source");

            if (!StringUtil.nullOrEmptyOrBlankString(transactionSource))
            {
                Log.customer.debug(msCN + ": transaction source not null for " + POOrder.getUniqueName());
                if (StringUtil.equalsIgnoreCase(transactionSource, AMSE2EConstants.TXNSOURCE_E2EPROCURE))
                {
                    QueueMsg.setObjOrigin(QueueMsg.APPLICATION_ADV);
                }
                if (StringUtil.equalsIgnoreCase(transactionSource, AMSE2EConstants.TXNSOURCE_QUICKQUOTE))
                {
                    QueueMsg.setObjOrigin(QueueMsg.APPLICATION_QQ);
                    if (!StringUtil.nullOrEmptyOrBlankString(initialID))
                    {
                        String qqAwdId = initialID.substring(3);
                        if (!StringUtil.nullOrEmptyOrBlankString(qqAwdId))
                        {
                            QueueMsg.setObjOriginID(qqAwdId);
                        }
                    }
                }
                if (StringUtil.equalsIgnoreCase(transactionSource, AMSE2EConstants.TXNSOURCE_INTERFACES))
                {
                    QueueMsg.setObjOrigin(QueueMsg.APPLICATION_EXTARIBA);
                }
            }
            else
            {
                Log.customer.debug(msCN + ": transaction source is null for " + POOrder.getUniqueName());
                QueueMsg.setObjOrigin(QueueMsg.APPLICATION_ARIBA);
            }

            EvaSendMessageAribaPOStatusImpl AribaSendMsg = new EvaSendMessageAribaPOStatusImpl();
            AribaSendMsg.setMQMessg(QueueMsg);
            // hand it to a thread from here
            EvaAribaMQManager.sendMQMessage((EvaSendMessageAribaImpl) AribaSendMsg);
            Log.customer.debug(msCN + ": sent message to the MQ queue");
            if (!EvaAribaMQManager.hasMQThreadRun())
            {
                Log.customer.debug(msCN + ": creating new MQ thread");
                EvaAribaMQThread mqThread = new EvaAribaMQThread();
                mqThread.start();
                Log.customer.debug(msCN + ": finished creating new MQ thread");
            }
            // AribaSendMsg.initializeCamel();
            // AribaSendMsg.publishMessageToQueue(QueueMsg);
            // do not cleanup as we want to use the same context and producertemplates
        }
        catch (Exception exp)
        {
            Log.customer.warning(7100,"Exception calling ---------" + msCN);
            Log.customer.warning(7100,"Exception calling " + msCN + " :exc:" + exp.getMessage());
            exp.printStackTrace();
        }
        finally
        {
            //Log.customer.setLevel(currLevel);
        }
    }
}
