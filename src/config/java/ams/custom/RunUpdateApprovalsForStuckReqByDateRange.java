package config.java.ams.custom;
 
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseSession;
import ariba.base.core.Partition;
import java.util.Map;
import ariba.common.core.Log;
import java.util.List;
import ariba.purchasing.core.Requisition;
import ariba.util.scheduler.Scheduler;
import ariba.util.scheduler.SimpleScheduledTask;
import ariba.approvable.core.ApprovalRequest;
import ariba.util.core.*;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.core.aql.AQLScalarExpression;
import ariba.base.core.aql.AQLQuery;
import ariba.util.core.ListUtil;
import ariba.util.core.StringUtil;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.apache.log4j.Level;

//81->822 changed Vector to List
//81->822 changed Hashtable to Map
//81->822 changed Enumeration to Iterator
public class RunUpdateApprovalsForStuckReqByDateRange extends SimpleScheduledTask
{
   private static final String FixRequisitions  = "FixRequisitions";
   private boolean shouldfixRequisition = false;
   private static String FromDate = "FromDate";
   private String fromDateArgument = null;
   private static String ToDate = "ToDate";
   private String toDateArgument = null;

   public void init (Scheduler scheduler, String scheduledTaskName, Map arguments)
   {
      super.init(scheduler, scheduledTaskName, arguments);
         
      java.util.Iterator e = arguments.keySet().iterator();
 
      while (e.hasNext()) 
      {
         String key = (String)e.next();
         if (key.equals(FixRequisitions)) 
         {
            String argument = (String)arguments.get(key);
            shouldfixRequisition = Constants.getBoolean(argument).booleanValue();
         }
 
         if (key.equals(FromDate)) 
         {
            fromDateArgument = (String)arguments.get(key);
         }
 
         if (key.equals(ToDate)) 
         {
            toDateArgument = (String)arguments.get(key);
         }
      }
   }
 

   public void run ()
   {
      Logs.buysense.setLevel(Level.DEBUG);
      Log.customer.debug("In Run RunUpdateApprovalsForStuckERByDateRange");
      Partition partition = Base.getSession().getPartition();
      BaseSession session = Base.getSession();

      if ((fromDateArgument == null)||(toDateArgument == null)) 
      {
         Log.customer.debug("RunUpdateApprovalsForStuckERByDateRange FromDate or ToDate is null. Return with no processing");
         return;
      }
 
      AQLResultCollection requisitions = getRequisitions(partition,fromDateArgument,toDateArgument);
 
      while (requisitions.next()) 
      {
         BaseId reqbaseid = (BaseId)requisitions.getBaseId(0);
 
         Requisition req  = (Requisition)session.objectForWrite(reqbaseid);
 
         boolean isReqStuck = isReqStuck(req);

         if (shouldfixRequisition && isReqStuck) 
         {
            Log.customer.debug("RunUpdateApprovalsForStuckReqByDateRange found Requisitions stuck in submitted submit: " + req.getUniqueName());
            Logs.buysense.debug("Fixed ReqsStuckInSubmittedState - Requisition : " + req.getUniqueName());
            RunUpdateApprovals(req);
            Base.getSession().transactionCommit();
         }

         if (!shouldfixRequisition && isReqStuck) 
         {
            Log.customer.debug("RunUpdateApprovalsForStuckReqByDateRange found Requisitions stuck in submitted report only: " + req.getUniqueName());
            Logs.buysense.debug("Report:ReqsStuckInSubmittedState - Requisition : " + req.getUniqueName());
            Base.getSession().transactionCommit();
         }
      }

      Logs.buysense.setLevel(Level.DEBUG.OFF);
   }
 
   private void RunUpdateApprovals (Requisition req)
   {
      BaseId reqbaseid = req.getBaseId();
 
      Log.customer.debug("RunUpdateApprovalsForStuckReqByDateRange.RunUpdateApprovals reqbaseid: " + reqbaseid);
 
      Requisition reqforwrite = (Requisition)Base.getSession().objectForWrite(reqbaseid);
 
      reqforwrite.realUpdateApprovals();

      return;
   }
 

   private List getIdentifiersFromArgument (String argument)
   {
      List identifiers = ListUtil.list();

      if (!StringUtil.nullOrEmptyOrBlankString(argument)) 
      {
         int end = 0;
         int begin = 0;
         String identifier = null;
         do 
         {
            end = argument.indexOf(",", begin);
            if (end != -1) 
            {
               identifier = argument.substring(begin, end);
               begin = end+1;
            }
            else 
            {
               identifier = argument.substring(begin, argument.length());
            }
           
            identifier = identifier.trim();
            if (!StringUtil.nullOrEmptyOrBlankString(identifier)) 
            {
               identifiers.add(identifier);
            }
         }
         while (end != -1 && !StringUtil.nullOrEmptyOrBlankString(identifier));
      }
      return identifiers;
   }
 

   private boolean isReqStuck (Requisition req)
   {
      Log.customer.debug("In RunUpdateApprovalsForStuckReqByDateRange.isReqStuck");
 
      Iterator approvalrequests = req.getApprovalRequestsIterator((ApprovalRequest.StateActive), null, null);

      if (approvalrequests.hasNext()) 
      {
         Log.customer.debug("RunUpdateApprovalsForStuckReqByDateRange.isReqStuck - found active approval request.");
         return false;
      }
 
      Log.customer.debug("RunUpdateApprovalsForStuckReqByDateRange.isReqStuck - found no active approval request.");
      return true;
   }
 

   private AQLResultCollection getRequisitions (Partition partition, String FromDate, String ToDate)
   {
      // Get run date
      boolean lboolSetTime = false;
      String lsToTime = null;
      GregorianCalendar loToday = new GregorianCalendar();
      String lsMonth = "" + (loToday.get(Calendar.MONTH) + 1);
      String lsDate = "" + loToday.get(Calendar.DATE);

      if (lsMonth.length() == 1)
      {
         lsMonth = 0 + lsMonth;
      }

      if (lsDate.length() == 1)
      {
         lsDate = 0 + lsDate;
      }

      String lsToday = loToday.get(Calendar.YEAR) + "-" + lsMonth + "-" + lsDate;

      if ((FromDate.trim()).equalsIgnoreCase("today") || (FromDate.trim()).equalsIgnoreCase(lsToday))
      {
         FromDate = lsToday;
         ToDate = FromDate;
         lboolSetTime = true;
      }
      else if ((ToDate.trim()).equalsIgnoreCase(lsToday))
      {
         lboolSetTime = true;
      }
    
      if (lboolSetTime)
      { 
         loToday.add(Calendar.MINUTE, - 15);
         String lsHour = "" + loToday.get(Calendar.HOUR_OF_DAY);
         String lsMinute = "" + loToday.get(Calendar.MINUTE);
         String lsSecond = "" + loToday.get(Calendar.SECOND);
         if (lsHour.length() == 1)
         {
            lsHour = 0 + lsHour;
         }
         if (lsMinute.length() == 1)
         {
            lsMinute = 0 + lsMinute;
         }
         if (lsSecond.length() == 1)
         {
            lsSecond = 0 + lsSecond;
         }
       
         lsToTime = " " + lsHour + ":" + lsMinute + ":" + lsSecond + " EDT";
      }
      else
      {
         lsToTime = " 23:59:59 EDT";
      }

 
      String fromDate = (FromDate + " 00:00:00 EDT");
      String toDate = ToDate + lsToTime;

      String querytext = "Select Requisition " +
                         "from Requisition " +
                         "where Requisition.StatusString = 'Submitted' " +
                         "and LastModified BETWEEN date(%s) AND date(%s) ";

      querytext = Fmt.S(querytext, AQLScalarExpression.buildLiteral(fromDate), AQLScalarExpression.buildLiteral(toDate));

      AQLOptions options = new AQLOptions(partition, false);
 
      AQLQuery query = AQLQuery.parseQuery(querytext);

      AQLResultCollection results = Base.getService().executeQuery(query, options);
 
      return results;
   }
}

