package config.java.ams.custom;

import java.util.List;

import ariba.approvable.core.Approvable;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.util.core.PropertyTable;
import ariba.util.core.ResourceService;
import ariba.util.log.Log;
import ariba.user.core.User;

public class BuysenseIsValidCreateReqUser extends Condition{
	
	
	
    public boolean evaluate(Object paramObject, PropertyTable paramPropertyTable) throws ConditionEvaluationException
    {
        ariba.user.core.User currentEffectiveUser = (ariba.user.core.User) (Base.getSession().getEffectiveUser());
        
        

        Approvable loAppr = (Approvable)paramObject;
        
        Boolean lbValidUser = checkUserValidity(loAppr,currentEffectiveUser);
        
		return lbValidUser;
    	
    }
    public Boolean checkUserValidity(Approvable loAppr, ariba.user.core.User currentEffectiveUser)
    {
		
        String lsPermToCheck = ResourceService.getString("buysense.DynamicEform", "RoleToCheck", Base.getSession()
                .getLocale());
        boolean lbRoleMatch = false;
        ariba.common.core.User currentUser = ariba.common.core.User.getPartitionedUser(currentEffectiveUser, Base
                .getSession().getPartition());
		String currentUserName = (String) currentUser.getFieldValue("UniqueName");
		
        Partition nonePartition = Base.getService().getPartition("None");
        ClusterRoot oSharedUser = Base.getService().objectMatchingUniqueName("ariba.user.core.User", nonePartition,
                currentUserName);
        
        List lAllRoles = (List) oSharedUser.getDottedFieldValue("AllRoles");

        if(lAllRoles == null)
        {
            return false; // no grps found
        }

        for (int i = 0; i < lAllRoles.size(); i++)
        {
            // Ariba 8.1: List::elementAt() is deprecated by List::get()
            BaseId rolesBID = (BaseId) lAllRoles.get(i);
            ClusterRoot perm = rolesBID.get();
            String roleName = (String) (perm.getFieldValue("UniqueName"));

            if (roleName.equalsIgnoreCase(lsPermToCheck) )
            {
                Log.customer.debug("In checkUserValidity: role match found for user:" + currentUserName);
                lbRoleMatch = true;
                break;
            }
            else
            {
            	lbRoleMatch = false;
            }
        }
        
        if (lbRoleMatch == true)
        {
        	Log.customer.debug("This is a valid User");
        	return true;
        }
        else
        {
        	Log.customer.debug("In checkUserValidity: no role match found for user:" + currentUserName);
        	return false;
        }
        
	}

}
