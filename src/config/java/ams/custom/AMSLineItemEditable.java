/************************************************************************************/
// Name:             AMSLineItemEditable
// Description:
// Author:  David Leonard, Ariba, Inc.
// Date:    October 8, 2002
// Revision History:
//
//  Date              Responsible            Description
//  -------------------------------------------------------------------------------
//   10/08/2002       David Leonard, Ariba   Editability condition to disallow change orders
//                                           for line items where the supplier is the COVA solicitation vendor.
//   01/12/2002       Rob Giesen             Dev SPL #11 - Ariba 8.x customization
//                                           Problem with non-catalog items.  Need to check for a valid
//                                             supplier location before continuing
//
// Copyright of Ariba, Inc., 1996-2002       All Rights Reserved
/*************************************************************************************/

/*
03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/11/2004: rgiesen Ariba 8.1 dev SPL #39 - replaced Base.getSession().getPartition() with a better method
03/11/2004: rgiesen Ariba 8.1 dev SPL #19 - Cleaned up the log messages for this file.
labraham, 09/22/2004: Ariba 8.1 Upgrade ST SPL # 134 - Added an edit to not display the Change button if req is in Approved status
*/

package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.approvable.core.LineItemCollection;
import ariba.base.core.BaseVector;
import ariba.base.core.BaseObject;
import ariba.base.core.ClusterRoot;

import ariba.common.core.*;

//Ariba 8.1: Added import for ariba.user.core.User;
import ariba.user.core.User;

// import ariba.approvable.core.condition.LineItemsEditable;
import ariba.procure.core.condition.ProcureItemsEditable;

import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.PurchaseOrder;

import ariba.util.core.PropertyTable; // Ariba 8.1: Util has been deprecated
//import ariba.util.core.Util;
import java.util.List;
import ariba.util.core.FatalAssertionException;

public class AMSLineItemEditable extends ProcureItemsEditable implements AMSE2EConstants
{
    String                       SolicitationParameter = "Application.AMSParameters.SolicitationVendor";

    private static final boolean DEBUG                 = false;

    public boolean evaluate(int operation, User user, PropertyTable params)
    {

        Log.customer.debug("AMSLineItemEditable:Inside custom evaluate method, User=" + user + ", operation="
                + operation);

        /* Custom editability logic is based on the following decision table

        Originating         BypassApprovers     BypassApprovers
        System              = True              = False
        ===========         ===============     ===============
        QQ                  Allow               Allow
        INT                 Restrict            Allow
        E2E                 Restrict            Restrict
        UI                  Allow               Allow
        EXT-REQ		    Restrict	        Allow

        Customization documentation proposes extending LineItemsEditable, because ERPLineItemsEditable,
        the logical choice, is very restrictive. ERPLineItemsEditable also has edits that conflict
        with the purpose of the custom class. However, LineItemsEditable is a little too unrestrictive.
        Hence the decision to subclass ProcureItemsEditable.

        This class also needs to duplicate current ERPLineItemEditable editability constraints that do
        not conflict with the custom functionality. The only such editability function is the check for
        change order restrictions at the Supplier location level.

        Note: ReqLineItem.getIsSupplierSupportsChange() is not a "documented" method.
        
        CSPL-1792: SRINI: Hide change button if Requisition is in Ordered and Order/s are in Received
        Status. Check with variable lbOrdersInReceiving for code modifications of this CSPL.
        */

        Object licObject = params.getPropertyForKey("LineItemCollection");
        if (!(licObject instanceof LineItemCollection))
        {
            throw new FatalAssertionException(
                    "AMSLineItemEditable evaluate method expects a LineItemCollection object.");
        }

        LineItemCollection lic = (LineItemCollection) params.getPropertyForKey("LineItemCollection");
        String reqStatusString = lic.getStatusString();
        String txnSource = getFieldValueString(lic, "TransactionSource");
        boolean bypassApprovers = ((Boolean) lic.getFieldValue("BypassApprovers")).booleanValue();

        if (DEBUG)
        {
            Log.customer.debug("AMSLineItemEditable:evaluate: LineItemCollection = " + lic);
            Log.customer.debug("AMSLineItemEditable:evaluate: Requisition status = " + reqStatusString);
            Log.customer.debug("AMSLineItemEditable:evaluate: Transaction source = " + txnSource);
            Log.customer.debug("AMSLineItemEditable:evaluate: BypassApprovers = " + bypassApprovers);
            Log.customer.debug("AMSLineItemEditable - beginning of LOOP for line items");
        }

        // Count how many lines have Procurement Vendors
        BaseVector loBV = lic.getLineItems();
        // Ariba 8.1: List::count() has been deprecated by List::size()
        int lineCount = loBV.size();
        int liProcVendorCount = 0;
        int liNonProcVendorCount = 0;
        int liSupplierAllowsChangeCount = 0;
        int liSupplierDisallowsChangeCount = 0;
        boolean lbSolicitation = false;
        //CSPL-1792
        boolean lbOrdersInReceiving = false;

        for (int lisub = 0; lisub < lineCount; lisub++)
        {
            // Ariba 8.1: List::elementAt() has been deprecated by List::get()
            ReqLineItem rli = (ReqLineItem) loBV.get(lisub);

            if ((Boolean) rli.getFieldValue("SolicitationLine") == null)
            {
                lbSolicitation = false;
            }
            else
            {
                lbSolicitation = ((Boolean) rli.getFieldValue("SolicitationLine")).booleanValue();
            }

            if (lbSolicitation)
            {
                liProcVendorCount++;
            }
            else
            {
                liNonProcVendorCount++;
            }

            //Ariba 8.1: getIsSupplierSupportsChange() method is no longer valid so added the following code to do the same check.
            //if (rli.getIsSupplierSupportsChange())

            //RGiesen - DEV SPL #11 Ariba 8.1:
            //In the old version, getIsSupplierSupportsChange(), if the supplier location was null
            // then it would return true.  So, "move" that functionality to this program, checking
            // for null, and if the current line does NOT have a valid supplier location -
            // which could occur for a non-catalog line -
            // then increase the liSupplierAllowsChangeCount.
            // This will make it consist with 7.x
            // If it is a non-catalog line, it will not have a supplier yet.
            //Integer liSupplierChangeOrderRestrict = new Integer(rli.getSupplierLocation().getChangeOrderRestrictions());

            if (rli.getSupplierLocation() == null)
            {

                liSupplierAllowsChangeCount++;
            }
            else
            {
                Integer liSupplierChangeOrderRestrict = new Integer(rli.getSupplierLocation()
                        .getChangeOrderRestrictions());

                if (liSupplierChangeOrderRestrict.equals(new Integer("0")))
                {
                    liSupplierAllowsChangeCount++;
                }
                else
                {
                    liSupplierDisallowsChangeCount++;
                }
            }
            //RGiesen - DEV SPL #11 - END Ariba 8.1

            if (rli.getLatestOrder() != null && !rli.getLatestOrder().getStatusString().equalsIgnoreCase("RECEIVED"))
            {
                lbOrdersInReceiving = true;
            }
        }

        if (DEBUG)
        {
            Log.customer.debug("evaluate: Lines going to Procurement vendor = " + liProcVendorCount);
            Log.customer.debug("evaluate: Lines going to non-Procurement vendor = " + liNonProcVendorCount);
            Log.customer.debug("evaluate: Lines with Supplier supporting change in receiving = "
                    + liSupplierAllowsChangeCount);
            Log.customer.debug("evaluate: Lines with Supplier restricting change in receiving = "
                    + liSupplierDisallowsChangeCount);
        }

        if (operation == Change)
        /*eProcure ST SPL # 1325 - Changed Interface logic to no longer check the BypassApprovers flag and 
                                   never allow Changes or Cancelations */
        /*Ariba 8.1 Upgrade ST SPL # 134 - Added logic to check if the req status is APPROVED and not allow Changes if it is */
        {
            if ((reqStatusString.equalsIgnoreCase("ORDERING")) || // Ordering - can't change
                    reqStatusString.equalsIgnoreCase("APPROVED") || // Approved - can't change
                    (txnSource.equals(TXNSOURCE_INTERFACES)&& bypassApprovers) ||
                    (txnSource.equals(TXNSOURCE_E2EPROCURE)) || // e2e Req - can't change
                    (liNonProcVendorCount == 0 && liProcVendorCount > 0) || // All lines to Procurement - can't change
                    (liSupplierAllowsChangeCount == 0 && liSupplierDisallowsChangeCount > 0) // All lines have restrictive supplier - can't change
                    || (reqStatusString.equalsIgnoreCase("ORDERED") && !lbOrdersInReceiving))
            {
                Log.customer
                        .debug("evaluate: At least one custom editability restriction applies ... cannot change req");
                return false;
            }
        }

        if (operation == Cancel)
        {
            /*eProcure ST SPL # 1325 - Changed Interface logic to no longer check the BypassApprovers flag and 
                                       never allow Changes or Cancelations */
            //if ( (txnSource.equals(TXNSOURCE_INTERFACES)
            //      && bypassApprovers)
            if ((txnSource.equals(TXNSOURCE_INTERFACES) && bypassApprovers) || // Interfaces and bypassApprovers - can't cancel
                    (txnSource.equals(TXNSOURCE_E2EPROCURE)) || // e2e Req - can't cancel
                    (liProcVendorCount > 0) // at least 1 line to Proc vendor - can't cancel
            )
            {
                Log.customer.debug("evaluate: At least one line going to Procurement Office ... cannot cancel req");
                return false;
            }
        }

        Log.customer.debug("evaluate: No custom restrictions apply ... returning super.evaluate(...)");
        return super.evaluate(operation, user, params);
    }

    public boolean evaluateElement(Object element, List vector, int operation, User user, PropertyTable params)
    {
        Log.customer.debug("Inside custom evaluateElement method, User=" + user + ", operation=" + operation
                + ", element=" + element);

        //Util.assert(element instanceof ReqLineItem, "AMSLineItemEditable requires a ReqLineItem element.");
        if (!(element instanceof ReqLineItem))
        {
            throw new FatalAssertionException("AMSLineItemEditable requires a ReqLineItem element.");
        }

        ReqLineItem rli = (ReqLineItem) element;
        String orderStatusString = "";
        String LatestorderStatusString = "";
        if (rli.getFieldValue("LatestOrder") != null)
        {
            LatestorderStatusString = ((PurchaseOrder) rli.getFieldValue("LatestOrder")).getStatusString();
        }

        if (rli.getFieldValue("Order") != null)
        {
            orderStatusString = ((PurchaseOrder) rli.getFieldValue("Order")).getStatusString();
        }

        LineItemCollection lic = (LineItemCollection) rli.getLineItemCollection(); // get a handle to the req object
        String reqStatusString = lic.getStatusString();
        String txnSource = getFieldValueString(lic, "TransactionSource");
        boolean bypassApprovers = ((Boolean) lic.getFieldValue("BypassApprovers")).booleanValue();
        boolean statusChanging = (rli.getOldValues() != null && isChangeReq(lic)); // within change order scenario
        boolean statusApproved = (lic.getApprovedState() == Approvable.StateApproved);
        boolean statusComposing = (lic.getApprovedState() == Approvable.StateCompose);
        boolean lbSolicitation = false;

        if ((Boolean) rli.getFieldValue("SolicitationLine") == null)
        {
            lbSolicitation = false;
        }
        else
        {
            lbSolicitation = ((Boolean) rli.getFieldValue("SolicitationLine")).booleanValue();
        }
        if (DEBUG)
        {
            Log.customer.debug("evaluateElement: latestorder = " + LatestorderStatusString);
            Log.customer.debug("evaluateElement: order = " + orderStatusString);
            Log.customer.debug("evaluateElement: ReqLineItem = " + rli);
            Log.customer.debug("evaluateElement: Order status = " + orderStatusString);
            Log.customer.debug("evaluateElement: LineItemCollection = " + lic);
            Log.customer.debug("evaluateElement: Requisition status = " + reqStatusString);
            Log.customer.debug("evaluateElement: Transaction source = " + txnSource);
            Log.customer.debug("evaluateElement: Changing = " + statusChanging);
            Log.customer.debug("evaluateElement: Approved = " + statusApproved);
            Log.customer.debug("evaluateElement: Composing = " + statusComposing);
            Log.customer.debug("evaluateElement: lbSolicitation = " + lbSolicitation);
        }
        //Ariba 8.1: getIsSupplierSupportsChange() method is no longer valid so added the following code to do the same check.

        //rgiesen Dev SPL #11 - Ariba 8.1 comment out debugmsg - line item might not have a supplierlocation yet
        //debugMsg( "evaluateElement: liSupplierChangeOrderRestrict = %i" + rli.getSupplierLocation().getChangeOrderRestrictions());

        //Ariba 8.1: getIsSupplierSupportsChange() method is no longer valid - obtained the SupplierLocation.ChangeOrderRestriction
        //to be used in the following if statement in order to do the same check.

        //rgiesen Ariba 8.1 Dev SPL#11 - Similar to above, check to see if SupplierLocation is null
        // in the case of a non-catalog item.  If it is null, then set flag to "0"

        Integer liSupplierChangeOrderRestrict = new Integer("0");

        if (rli.getSupplierLocation() != null)
        {
            liSupplierChangeOrderRestrict = new Integer(rli.getSupplierLocation().getChangeOrderRestrictions());
        }
        else
        {
            //SupplierLocation is null, in the old function "getIsSupportsChange(), it
            // would have returned "true".
            liSupplierChangeOrderRestrict = new Integer("0");
        }

        if ((reqStatusString.equalsIgnoreCase("ORDERING")) || // Req Ordering - can't edit
                (LatestorderStatusString.equalsIgnoreCase("RECEIVED")) || //CSPLU-155 LatestOrder Received - can't edit
                (orderStatusString.equalsIgnoreCase("ORDERING")) || // Ordering - can't edit
                (orderStatusString.equalsIgnoreCase("ORDERED")) || // Ordered - can't edit
                (txnSource.equals(TXNSOURCE_INTERFACES) && bypassApprovers) || // Interfaces and bypassApprovers - can't edit
                (txnSource.equals(TXNSOURCE_E2EPROCURE) && !statusComposing) || // e2e and not composing - can't edit
                (lbSolicitation && (statusChanging || statusApproved)) || // Proc vendor - can't change unless composing
                //Ariba 8.1: getIsSupplierSupportsChange() method is no longer valid - added the following code to do the same check
                //(!rli.getlsSupplierSupportsChange())                              // Supplier does not support change while rcvng
                (!liSupplierChangeOrderRestrict.equals(new Integer("0"))) // Supplier does not support change while rcvng
        )
        {
            Log.customer
                    .debug("evaluateElement: At least one custom editability restriction applies ... cannot edit/delete line");
            return false;
        }

        Log.customer.debug("evaluateElement: No custom restrictions apply ... returning super.evaluateElement(...)");
        return super.evaluateElement(element, vector, operation, user, params);
    }

    /**
    * Checks whether Change Requisition is being dealt with and if it is, returns true
    * @param foApprovable - the Requisition
    * @return boolean - true is returned if there is a previous req version
    */
    private static boolean isChangeReq(Approvable foApprovable)
    {
        boolean lboolChangeReq = false;
        ClusterRoot loCluster = foApprovable.getPreviousVersion();

        if (loCluster != null)
        {
            String lsStatus = (String) loCluster.getFieldValue("StatusString");
            if (lsStatus.equals("Composing"))
            {
                Log.customer.debug("Not a change req ... previous version in composing due to integration errors");
            }
            else
            {
                lboolChangeReq = true;
            }
        }
        return lboolChangeReq;
    }

    /* This method returns a String value encapsulating null handling */
    private String getFieldValueString(BaseObject foBO, String fsName)
    {
        String lsValue = "";
        if (foBO.getFieldValue(fsName) != null)
        {
            lsValue = (String) foBO.getFieldValue(fsName);
        }

        return lsValue;
    }

    /* This method encapsulates debug message logging */
    private void debugMsg(String foMsg)
    {
        if (DEBUG)
        {
            Log.customer.debug(foMsg);
        }
    }
}
