/*
 * @(#)SetPrePrintedText      2003/10/16
 *
 * Copyright 2003 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information"). You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 */

 /**
  * This class updates the PREPRINTEDTEXT field on an order
  * object when the order is created.
  * @author  Jeff Namadan
 */

/* 12/22/2003: Updates for Ariba 8.1 (Jeff Namadan)
02/19/2004 rgiesen Dev SPL #28 - VCEUtil is no longer available in 8.1 - replaced call to VCEUtil
 with the code from a Field Release*/
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */
/*
jnamadan, 3/2/04, eProc Prod SPL 263 - Redesigned that the pre printed text logic is executed at a POLineItem level.  The POHeader i
s still updated with the preprinted text object, but we needed to execute the code at a line level.  This is per Ariba's suggestion
to better solve the problem where orders are being stuck in Approved status.  It appears that the preprinted text logic was failing
since a PO Line Item was not created on some change order records.  This redesign should hopefully resolve this issue.

We also removed the save() of the purchase order header as order is saved from other processes in the order creation process.
*/

package config.java.ams.custom;

import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.BaseObject;
import ariba.base.core.Partition;
import ariba.base.fields.*;
import ariba.util.core.*;
//rgiesen Dev SPL # 28 added Field Release
//jackie - need to ask Ariba consultant or in house ariba experts. 
import ariba.fieldrelease.core.FRCommonFacade;
//rgiesen Dev SPL #28 - no longer using VCEUtil
//import config.java.vce.VCEUtil;


public class SetPrePrintedText extends Action
{
   static String moALL = "ALL";

   public void fire(ValueSource object, PropertyTable params)
   {
      BaseObject bo = (BaseObject)object;
//    Log.customer.debug("JN About to process class: %s",bo.getClassName());

      if (bo.getClassName().equals("ariba.purchasing.core.Requisition"))
      {
//       Log.customer.debug("JN About to process Req PPT");
         processReqPrePrintedText(bo);
      }
      else
      {
         if (bo.getClassName().equals("ariba.purchasing.core.POLineItem"))
         {
//          Log.customer.debug("JN About to process PO PPT");
            processOrderPrePrintedText(bo);
         }
      }
   }

   public static BaseObject processReqPrePrintedText (BaseObject fsObject)
   {

         BaseObject loObject = (BaseObject)fsObject;
         BaseObject loPPText = null;

         ariba.purchasing.core.Requisition loReq = (ariba.purchasing.core.Requisition)loObject;

        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        ariba.user.core.User RequesterUser = (ariba.user.core.User)loReq.getDottedFieldValue("Requester");
        //MSRINI::Modified to get partition from object instead from session. Partition should be specified for CustomApprover
        //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser, loReq.getPartition());        
         String loClientName = (String)(RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName"));
         String loClientID = (String)(RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientID"));

/*
 *
 * Perform lookup on the PrePrintedText object
 *
 */
         //MSRINI::Modified to get partition from object instead from session. Partition should be specified for CustomApprover
         //loPPText = getPrePrintedText (loClientID, moALL);
         loPPText = getPrePrintedText (loClientID, moALL,loReq.getPartition());
         return loPPText;
   }

   public void processOrderPrePrintedText (BaseObject fsObject)
   {
         BaseObject loObject = (BaseObject)fsObject;
         BaseObject loPPText = null;

         ariba.purchasing.core.POLineItem loOrderLine = (ariba.purchasing.core.POLineItem)loObject;
         ariba.purchasing.core.PurchaseOrder loOrder = (ariba.purchasing.core.PurchaseOrder)loOrderLine.getLineItemCollection();

/*
 *
 * Retrieve the ClientID and Client Name from the Requester's BuysenseOrg off of the Requisition on the Order Line
 *
 */
         // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
         //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
         //Partitioned User to obtain the extrinsic BuysenseOrg field value.
         ariba.user.core.User liRequesterUser = (ariba.user.core.User)((BaseObject)loOrderLine.getDottedFieldValue("Requisition.Requester"));
         ariba.common.core.User liRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(liRequesterUser,loOrder.getPartition());
         String loClientName = (String)(liRequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName"));
         String loClientID = (String)(liRequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientID"));

/*
 *
 * Perform lookup on the PrePrintedText object
 *
 */
       //MSRINI::Modified to get partition from object instead from session. Partition should be specified for CustomApprover
       //loPPText = getPrePrintedText (loClientID, loClientName);
         loPPText = getPrePrintedText (loClientID, loClientName, loOrder.getPartition());
/*
 *
 * Retrieve the client specifc PrePrintedText object
 *
 */

         if ( loPPText != null )
         {
/*
 *
 * Set the PrePrintedText field on the order to the retrieved PrePrintedText object
 *
 */
            loOrder.setFieldValue("PrePrintedText",loPPText);
//          Log.customer.debug("JN About to save order: %s",loOrder);
//          Log.customer.debug("JN Saved Order with client");
         }
         else
         {
         //MSRINI::Modified to get partition from object instead from session. Partition should be specified for CustomApprover	 
         //loPPText = getPrePrintedText (loClientID, moALL);
           loPPText = getPrePrintedText (loClientID, moALL, loOrder.getPartition());	 

/*
 *
 * If Client specific PrePrintedText object doesn't exist, use the ALL object
 *
 */
            if ( loPPText != null )
            {
/*
 *
 * Set the PrePrintedText field on the order to the retrieved PrePrintedText object
 *
 */
               loOrder.setFieldValue("PrePrintedText",loPPText);
//             Log.customer.debug("JN About to save order: %s",loOrder);
//             Log.customer.debug("JN Saved Order with ALL");
            }
         }
   }
   //MSRINI::Modified to get partition from object instead from session. Partition should be specified for CustomApprover
   //public static BaseObject getPrePrintedText (String fsClientID, String fsClientName)
   public static BaseObject getPrePrintedText (String fsClientID, String fsClientName, Partition oPartition)
   {
     BaseObject loBaseObject = null;

     String     loAribaType  = "ariba.core.PrePrintedText" ;
     AQLQuery   loAQL        = new AQLQuery( loAribaType );
     //MSRINI::Modified to get partition from object instead from session. Partition should be specified for CustomApprover
     //AQLOptions loAQLOptions = new AQLOptions( Base.getSession().getPartition() ) ;
     AQLOptions loAQLOptions = new AQLOptions(oPartition) ;

     loAQL.andEqual( "ClientID", fsClientID ) ;
     loAQL.andEqual( "ClientName", fsClientName ) ;
     loAQL.andEqual( "TextType", "TC" ) ;
     loAQL.andEqual( "Active", Boolean.TRUE ) ;

//   Log.customer.debug("JN Client ID: %s", fsClientID);
//   Log.customer.debug("JN Client Name: %s", fsClientName);
     //MSRINI::Modified to get partition from object instead from session. Partition should be specified for CustomApprover
     //loAQLOptions.setUserPartition( Base.getSession().getPartition() ) ;
     loAQLOptions.setUserPartition(oPartition);

     //rgiesen Dev SPL #28 - VCEUtil is no longer available, use field release code
     //loBaseObject = VCEUtil.getFirstAQLResult( loAQL, loAQLOptions );
     loBaseObject = FRCommonFacade.getFirstAQLResult( loAQL, loAQLOptions );

     return loBaseObject;
   }
}
