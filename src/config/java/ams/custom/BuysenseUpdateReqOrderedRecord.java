package config.java.ams.custom;

import java.util.Iterator;

import ariba.approvable.core.Record;
import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.common.core.Core;
import ariba.common.core.User;
import ariba.purchasing.core.Requisition;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseUpdateReqOrderedRecord extends Action
{

    private String sClassName = "BuysenseUpdateReqOrderedRecord";

    @SuppressWarnings("unchecked")
    public void fire(ValueSource arg0, PropertyTable arg1) throws ActionExecutionException
    {
        Log.customer.debug(sClassName + " arg0 " + arg0);
        Requisition loReq = null;
        boolean bReqSendEmallReqToQQ = false;
        boolean bReqSendEmallReqToADV = false;

        if (arg0 != null && arg0 instanceof Requisition)
        {
            loReq = (Requisition) arg0;
            Boolean loReqSendEmallReqToADV = (Boolean) loReq.getDottedFieldValue("ReqHeadCB3Value");
            bReqSendEmallReqToADV = (loReqSendEmallReqToADV != null && loReqSendEmallReqToADV.booleanValue());
            
            Boolean loReqSendEmallReqToQQ = (Boolean) loReq.getDottedFieldValue("ReqHeadCB5Value");
            bReqSendEmallReqToQQ = (loReqSendEmallReqToQQ != null && loReqSendEmallReqToQQ.booleanValue());
            Log.customer.debug(sClassName + " bReqSendEmallReqToQQ: " + bReqSendEmallReqToQQ +", bReqSendEmallReqToADV: "+bReqSendEmallReqToADV);
            if (bReqSendEmallReqToQQ || bReqSendEmallReqToADV)
            {

                for (Iterator<Record> lRecords = loReq.getRecordsIterator(); lRecords.hasNext();)
                {
                    Record loRecord = (Record) lRecords.next();
                    String sRecordType = loRecord.getType();
                    Log.customer.debug(sClassName + " sRecordType " + sRecordType);
                    if (sRecordType.equalsIgnoreCase("Ordered"))
                    {
                        lRecords.remove();
                        //loReq.removedRecordsElement(loRecord);
                    }
                }

                String lsRecordType = "SourcingRecord";
                String lsMessage = "Transaction closed; sent to buyer for sourcing.";
                Partition loPartition = Base.getService().getPartition("pcsv");
                if (loPartition == null)
                {
                    loPartition = Base.getSession().getPartition();
                }
                User loAribaSystemUser = Core.getService().getAribaSystemUser(loPartition);
                BuysenseUtil.createHistory(loReq, loReq.getUniqueName(), lsRecordType, lsMessage, loAribaSystemUser);
                loReq.save();
                Log.customer.debug(sClassName + " req history updated " + loReq);
            }

        }

    }

}
