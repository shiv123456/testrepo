/* 08/11/2003: Updates for Ariba 8.0 (Jeff Namadan) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import java.util.List;
import ariba.base.fields.*;
import ariba.common.core.*;
import ariba.util.core.*;
import ariba.util.log.Log;

//81->822 changed Vector to List
//81->822 changed ConditionValueInfo to ValueInfo
public class eVAEPACondition extends Condition
{
    public static final String TargetRequesterParam = "TargetValue";
    public static final String TargetRequesterParam1 = "TargetValue1";
    
    private static final String requiredParameterNames[] = 
        {
        "TargetValue", "TargetValue1"  
    };
    
    /* Ariba 8.0: Replaced ValueInfo */
    private static final ValueInfo parameterInfo[];
    
    
    public boolean evaluate(Object value, PropertyTable params)
    {
        return evaluateAndExplain(value, params) == null;
    }
    
    public ConditionResult evaluateAndExplain(Object value,
                                              PropertyTable params)
    {
        
        
        
        
        //User currentUser=(User)Base.getSession().getEffectiveUser();
        
        
        // This pulls the EPA value from the AML
        Object oUserProfileRequester = params.getPropertyForKey("TargetValue1");
        Log.customer.debug("Requester  is: %s",oUserProfileRequester);
        //This pulls the Requester value form the AML
        Object oTargetEPA = params.getPropertyForKey("TargetValue");
        Log.customer.debug("TargetEPA is: %s",oTargetEPA);
        
        if(oTargetEPA != null)
        {
            if (oTargetEPA instanceof User) 
            {
                Log.customer.debug("oTargetEPA is a User object");
                // If objects are instance of user, cast it as user and compare the values of
                // TargetEPA and Requester
                User uTargetEPA = (User)oTargetEPA;
                User uUserProfileRequester = (User)oUserProfileRequester;
                
                if (uTargetEPA==uUserProfileRequester)
                {
                    Log.customer.debug("oTargetEPA is the same as Requester!!");
                    return new ConditionResult("The Expenditure Limit Exceeded Approver cannot be yourself.");
                }
                else
                {
                    Log.customer.debug("oTargetEPA is not the same");
                    return null;
                }
            }
        }
        return null;
    }
    private List getTargetValues(PropertyTable params)
    {
        
        List targetValues = ListUtil.list();
        Object targetValue = params.getPropertyForKey("TargetValue");
        if(targetValue != null)
            // Ariba 8.1: Replaced deprecated method - changed addElement to add
            targetValues.add(targetValue);
        targetValue = params.getPropertyForKey("TargetValue1");
        if(targetValue != null)
            // Ariba 8.1: Replaced deprecated method - changed addElement to add
            targetValues.add(targetValue);
        
        return targetValues;
    }
    
    /* Ariba 8.0: Replaced ValueInfo */
    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }
    
    public eVAEPACondition()
    {
    }
    
    static
        {
        /* Ariba 8.0: Replaced ValueInfo */
        parameterInfo = (new ValueInfo[] 
                         {
                /* Ariba 8.0: Replaced ValueInfo */
                new ValueInfo("TargetValue",
                                       0),
                    new ValueInfo("TargetValue1", 0)
            }
        );
    }
    
}
