package config.java.ams.custom;

import ariba.app.util.Attachment;
import ariba.workforce.core.*;
import ariba.util.core.*;
import java.util.*;
import ariba.base.fields.*;
import ariba.util.log.Log;

/*
 Purpose          : Copy attachments from one field to the other
 */

public class BuysenseCopyAttachment extends Action
{
    public static final String ClassName = "config.java.ams.custom.BuysenseCopyAttachment";

    public void fire(ValueSource object, PropertyTable params)
    {
        String strLogLocation = ClassName.concat(".fire()");
        Log.customer.debug("%s: called with %s", strLogLocation, object);
        if (object instanceof LaborLineItemDetails)
        {
            LaborLineItemDetails llid = (LaborLineItemDetails) object;
            List attachments = (List) llid.getFieldValue("Attachments");
            Log.customer.debug("%s: attachments: %s", strLogLocation, attachments);
            Attachment attachment = (Attachment) llid.getFieldValue("AddAttachment");
            Log.customer.debug("%s: attachment: %s", strLogLocation, attachment);
            if (attachment != null)
            {
                attachments.add(attachment);
                llid.setFieldValue("AddAttachment", null);
            }
        }
        
        if (object != null && object.toString().indexOf("ConsultingSharedGlobalItemProperties") >= 0)
        {
            List attachments = (List) object.getFieldValue("SupplimentInformationAttachments");
            Log.customer.debug("%s: attachments: %s", strLogLocation, attachments);
            Attachment attachment = (Attachment) object.getFieldValue("AddSupplimentInformationAttachment");
            if (attachment != null)
            {
                attachments.add(attachment);
                object.setFieldValue("AddSupplimentInformationAttachment", null);
            }
        }
    }
}
