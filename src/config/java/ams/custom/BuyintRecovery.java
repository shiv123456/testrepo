//************************************************************************************
// Name:             BuyintRecovery
// Description: Scheduled Task For Integration Exception Recovery.
// Author:  David Chamberlain
// Date:    July 30, 2004
// Revision History:
//
//  Date              Responsible            Description
//  ----------------------------------------------------------------------------------
//  07/30/2004        David Chamberlain      Initial Version. Ariba 8.1 Upgrade
//											 Dev SPL 89.
//
//
// Copyright 2001 by American Management Systems, Inc.,
// 4050 Legato Road, Fairfax, Virginia, U.S.A.
// All rights reserved.
//
// This software is the confidential and proprietary information
// of American Management Systems, Inc. ("Confidential Information"). You
// shall not disclose such Confidential Information and shall use
// it only in accordance with the terms of the license agreement
// you entered into with American Management Systems, Inc.
//*************************************************************************************

/* DETAILED DESCRIPTION:
   This is the class for the scheduled task that will do error recovery for integration
   processing. During integration processing, when an error situation is encountered,
   an exception is thrown and a determination is made whether the error happened when the
   transaction was in a recoverable state. If the error happened at a recoverable stage,
   an entry is insterted into the integration error database table. This scheduled task
   is then responsible for picking up those errors and processing them. These transactions
   will then be re-pushed. If they fail again, another error is written to the error table.
*/

package config.java.ams.custom;

import java.util.Map;
import ariba.util.scheduler.ScheduledTask;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Base;
import ariba.user.core.*;
import java.util.Iterator;
import ariba.util.scheduler.Scheduler;
import java.lang.Integer;
import ariba.base.core.Partition;

//81->822 changed Hashtable to Map
public class BuyintRecovery extends ScheduledTask implements BuyintConstants
{
	private int miMaxRecords;
	private BuyintDBRecExceptionRecovery moExceptions[] = null;
	private BuyintDBRecExceptionRecovery moDbRec = null;
	private String msDBPassword = "";
	private String msDBURL = "";
	private String msDBUser = "";
	private static Partition moPart = Base.getSession().getPartition();

	//Process the Exceptions from the BUYINT_EXCEPTION_RECOVERY table
	public void run()
	{
		Log.customer.debug("*** In BuyintRecovery::run.");

		if(moPart == null)
		{
			moPart = Base.getService().getPartition("pcsv");
        }

		ClusterRoot loTransaction;

		try{
			//Build the sql string
			String lsSQL = "WHERE ((STATUS = '" + STATUS_READY + "') OR (STATUS = '" + STATUS_INPROGRESS + "'))";

			//Get the exceptions for processing. Need to pass in get status, max records count, and to status
			moExceptions = moDbRec.getRecs(lsSQL ,miMaxRecords);

			if (moExceptions == null)
			{
				return;
			}

			//Process all exceptions
			for (int i = 0; i < moExceptions.length; i++)
			{
				//Set the status to in-progress
				moExceptions[i].setStatus(STATUS_INPROGRESS);
				moExceptions[i].save();

				//Get the Ariba object for this transaction
				loTransaction = (ClusterRoot)Base.getService().objectMatchingUniqueName(
																	moExceptions[i].getDocumentType(),
																	moPart,
																	moExceptions[i].getDocumentID());

				//Submit the transaction for re-push
				if(BuyintXMLFactory.submit(moExceptions[i].getTransactionType(), loTransaction))
				{
					Log.customer.debug("* In if(submit) ");

					//if the persistence was success then update the record to success.
					//else if the transaction failed, the exception handler will update
					//this exception appropriately. Do nothing else in recovery task.
					moExceptions[i].setStatus(STATUS_COMPLETED);
					moExceptions[i].save();
				}//end if
			}//end for
		}//end try
		catch(Exception e)
		{
			Log.customer.debug("BuyintRecovery::processExceptions() failed with an exception.");
			Log.customer.debug("BuyintRecovery::processException() - " + e.toString());
			e.printStackTrace();
		}//end catch
	}

	//Extend the init method to get parameters from the scheduled task .table file
    public void init(Scheduler scheduler, String scheduledTaskName, Map arguments)
    {
        super.init(scheduler, scheduledTaskName, arguments);
        String lsKey = null;

		if(moPart == null)
		{
			moPart = Base.getService().getPartition("pcsv");
        }

        //Get Database connection information from Parameters.table file
		msDBURL = Base.getService().getParameter(moPart,"Application.AMSParameters.IntegrationDBURL");
		msDBUser = Base.getService().getParameter(moPart,"Application.AMSParameters.IntegrationDBUser");
        msDBPassword = Base.getService().getParameter(moPart,"Application.AMSParameters.IntegrationDBPassword");

        for (Iterator loItr = arguments.keySet().iterator(); loItr.hasNext();)
        {
           lsKey = (String)loItr.next() ;

           if(lsKey.equals("MaxTransactions"))
	   	   {
	      	  String lsMax = (String)arguments.get(lsKey);
	      	  miMaxRecords = Integer.parseInt(lsMax);
           }
        }//end for

		//Create the DBIO object
		BuyintDBIO moDbIo = new BuyintDBIO(msDBURL,msDBUser,msDBPassword);

        //Create the DBRec object
        moDbRec = new BuyintDBRecExceptionRecovery(moDbIo);


        Log.customer.debug("BuyintRecovery: Finished constructing BuyintRec ... ");
    }//end init
}
