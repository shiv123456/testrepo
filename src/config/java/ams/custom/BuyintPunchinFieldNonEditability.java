package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.common.core.CommonSupplier;
import ariba.util.core.PropertyTable;

public class BuyintPunchinFieldNonEditability extends Condition
{
    private static final ValueInfo parameterInfo[];

    public boolean evaluate(Object value, PropertyTable params) throws ConditionEvaluationException
    {
        ariba.user.core.User currentUser=(ariba.user.core.User)Base.getSession().getEffectiveUser();
        if((currentUser.getFieldValue("Organization") instanceof CommonSupplier))
        {
            return false;
        }
        else 
        {
            return true;
        }
    }

    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }
 
    static
    {
      parameterInfo = (new ValueInfo[] {
         new ValueInfo("SourceObject", 0)
        });
    }
}
