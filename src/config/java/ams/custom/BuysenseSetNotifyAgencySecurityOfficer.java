package config.java.ams.custom;

import java.util.List;

import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.procure.core.CategoryLineItemDetails;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
import ariba.workforce.core.LaborLineItemDetails;

public class BuysenseSetNotifyAgencySecurityOfficer extends Action
{
    String sClass = "BSetNotifyAgencySecurityOfficer";

    public void fire(ValueSource object, PropertyTable params) throws ActionExecutionException
    {
        Log.customer.debug(sClass + ".fire()-object: " + object);
        try
        {
            Requisition req = null;
            Boolean bNotifyAgencySecOfficer = Boolean.FALSE;
            Boolean bCollaborate = Boolean.TRUE;
            CategoryLineItemDetails catLID = null;

            if (object != null && object instanceof Requisition)
            {
                req = (Requisition) object;
            }

            if (req != null)
            {
                List reqLIs = req.getLineItems();
                for (int j = 0; j < reqLIs.size(); j++)
                {
                    ReqLineItem reqLI = (ReqLineItem) reqLIs.get(j);
                    catLID = reqLI.getCategoryLineItemDetails();
                    if (catLID != null && catLID instanceof LaborLineItemDetails)
                    {
                        LaborLineItemDetails lLID = (LaborLineItemDetails) catLID;
                        bCollaborate = (Boolean) lLID.getFieldValue("Collaborate");

                        // Set NotifyAgencySecOfficer to TRUE, if Collaboration is false and Contractor is not null.
                        if (!bCollaborate.booleanValue() && lLID.getContractor() != null)
                        {
                            bNotifyAgencySecOfficer = Boolean.TRUE;
                            break;
                        }
                    }
                }
                Log.customer.debug(sClass + " setting NotifyAgencySecOfficer to " + bNotifyAgencySecOfficer);
                req.setDottedFieldValue("NotifyAgencySecOfficer", bNotifyAgencySecOfficer);
            }

        }
        catch (Exception e)
        {
            Log.customer.debug(sClass + " Exception in fire()-: " + e);
        }

    }
}
