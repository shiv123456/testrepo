package config.java.ams.custom;


import java.io.File;
import java.io.FileInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Hashtable;
import java.util.Vector;
import ariba.util.log.Log;

/**
  * This class implements a generic parser base for reading and parsing XML.  
  * @author AMS Inc.. All rights reserved.
  * 
  * @version 1.1.1
  * @since Ver 1.1.1
  * @see ERPXMLParser
  */

public class XMLParser
{
    protected String xmlString = null;
    
    public XMLParser () 
    {
    }
    
    public XMLParser (String inString) 
    {
        xmlString = inString;
    }
    
    public XMLParser (File inFile) 
    {
        init(inFile);
    }
    
    protected void init (File inFile) 
    {
        try 
        {
            xmlString = new String(getFileAsString(inFile));
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
    }
    
    /**
      * Returns the contents of a file as a String. 
  * @return String
      */
    public String getFileAsString(File inFile) throws Exception
    {
        String filestring=null;
        //              File _file=new File(filename);
        java.io.FileInputStream in;
        java.io.ByteArrayOutputStream out;
        
        in=new FileInputStream(inFile);
        byte[] buffer=new byte[4096];
        out = new ByteArrayOutputStream();
        
        int bytes_read;
        while ((bytes_read = in.read(buffer)) != -1) 
        {
            out.write(buffer, 0, bytes_read);
            //String(byte[] bytes, int offset, int length) method can also be used   
            filestring = out.toString();
        }
        in.close();
        out.close();
        return filestring;
    }
    
    /** Returns the "DTD" as a string. For documentation only ... not used.  
     */
    private String getDTD () 
    {
        return getElement(xmlString, "<?xml", "]>");
    }
    
    /** Returns the attributes of the element as a String. <u>getField</u> may then be used on the String.
     */
    public String getAtts (String elementTag) 
    {
        return getElement(xmlString, elementTag, ">");                  
    }
    
    /** Returns a Vector of all elements within a passed section. 
     */
    public Vector getAllElements (String inSection,
                                  String begTag, String endTag) 
    {
        // Assumes that inSection contains all elements bounded by section delimiters
        // begTag and endTag must be the delimiters for the lower level element.
        Vector v = new Vector();
        int currOffset = 0;
        String s=null;
        StringBuffer sb;
        loop1:
            while ((currOffset=inSection.indexOf(begTag, currOffset)) != -1) 
        {
            sb = new StringBuffer(inSection.substring(currOffset));
            s = getElement(sb.toString(),begTag,endTag);
            v.addElement(s);
            currOffset += s.length();
        }
        return v;
    }
    
    /** Returns a Hashtable of all elements within a passed section, identified by specific field. 
     */
    public Hashtable getAllElementsHash (String inString,
                                         String begtag,
                                         String endtag, String idField) 
    {
        // idField contains the name of the REQUIRED field to build the Hashtable by ... eg. "ERPFIELDNAME"
        Hashtable h = new Hashtable();
        int currOffset = 0;
        String s1, s2 =null;
        StringBuffer sb;
        loop1:
            while ((currOffset=inString.indexOf(begtag, currOffset)) != -1) 
        {
            sb = new StringBuffer(inString.substring(currOffset));
            s2 = getElementData(sb.toString(),begtag,endtag);
            s1 = getField(s2, idField);
            h.put(s1, s2);
            currOffset += s2.length();
        }
        return h;
    }
    
    /** Returns an entire section with delimiters (assumes input string is the entire xml) 
     */
    public String getSection (String begTag, String endTag) 
    {
        debug("getSection - begTag: " + begTag + ", endTag: " + endTag);
        return getElement(xmlString, begTag, endTag);
    }
    
    /** Returns an entire section without delimiters (assumes input string is entire xml)  
     */
    public String getSectionData (String begTag, String endTag) 
    {
        debug("getSectionData - begTag: " + begTag + ", endTag: " + endTag);
        return getElementData(xmlString, begTag, endTag);
    }
    
    /** Returns a section within a passed string with delimiters. 
     */
    public String getElement (String inString, String begTag, String endTag) 
    {
        debug("getElement - begTag: " + begTag + ", endTag: " + endTag);
        int x[] = getBounds(inString, begTag, endTag);
        if ((x[0] < 0) || (x[1] < 0)) return null;
        return inString.substring(x[0], x[1]).trim();                   
    }
    
    /** Returns a section within a passed string without delimiters 
     */
    public String getElementData (String inString,
                                  String begTag, String endTag) 
    {
        debug("getElementData - begTag: " + begTag + ", endTag: " + endTag);
        int x[] = getBounds(inString, begTag, endTag);
        if ((x[0] < 0) || (x[1] < 0)) return null;
        x[0] += begTag.length();
        x[1] -= endTag.length();
        return inString.substring(x[0], x[1]).trim();                   
    }
    
    /** Returns the first and last byte positions of a delimited String. 
     */
    public int [] getBounds (String inString, String begTag, String endTag) 
    {
        debug("getBounds - begTag: " + begTag + ", endTag: " + endTag); 
        int [] result = 
            {
            -1,-1
        };
        int i = inString.indexOf(begTag);                       
        if (i >= 0) 
        {
            int j = inString.indexOf(endTag,i);     
            j += endTag.length(); 
            result[0] = i;
            result[1] = j;
        }
        debug("getBounds - about to return 2-d array with values: " + result[0] + "," + result[1]);
        return result;
    }
    
    /** Returns the Integer value of a numeric field/variable from within an occurrence of an element. 
     */
    public int getIntField (String inString, String fieldName) 
    {
        int i = 0;
        // For convenience ... 
        if (getField(inString, fieldName)!=null) 
            i = Integer.valueOf(getField(inString, fieldName)).intValue();
        return i;
    }
    
    /** Returns the value of a field/variable from within an occurrence of an element. 
     */
    public String getField (String inString, String fieldName) 
    {
        String outString = new String();
        int i = inString.indexOf(fieldName+"=\"");       
        if (i < 0) return null;
        else i+= fieldName.length() + 2;
        int j = inString.indexOf("\"",i);         
        if (j < 0) return null;
        outString = inString.substring(i, j);
        // DO NOT TRIM!!!
        return outString;
    }
    
    /** Returns true if there is an entry for the field in the String. 
     */
    public boolean fieldExists (String fieldName, String inString) 
    {
        return (getField(inString, fieldName)!=null); 
    }
    
    public void debug(String fsMsg)
    {
        Log.customer.debug("XMLParser: " + fsMsg);
    }
}

