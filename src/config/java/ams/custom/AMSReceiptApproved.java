/* 08/13/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 09/23/2003: Updates for Ariba 8.0 (Richard Lee) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;
// Ariba 8.0: Commented out import for ValueInfo since it's not neede in this module.
//import ariba.base.fields.ValueInfo;
// Ariba 8.0: commented out the next two imports; they aren't needed anymore. 
// If we do need it in the future, workflow has moved to ariba.server.workflowserver.*
//import ariba.server.objectserver.workflow.*;
//import ariba.server.objectserver.core.*;
//import ariba.server.workflowserver.*;

import ariba.base.core.ClusterRoot;
import ariba.purchasing.core.PurchaseOrder;
import java.math.BigDecimal;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;

// Ariba 8.0
import ariba.base.fields.Action;
import ariba.util.core.PropertyTable;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.log.Log;


/* Ariba 8.0: can not use ActionInterface and execute any more
public class AMSReceiptApproved implements ActionInterface
{
     public boolean execute( WorkflowState workflowState,
                            ClusterRoot cr,
                            WorkflowParameter[] parameters)
                            throws WorkflowException
    {
*/
// 81->822 changed from ConditionValueInfo to ValueInfo
public class AMSReceiptApproved extends Action
{
    // Ariba 8.0: replaced public boolean execute
    public void fire (ValueSource object,
                      PropertyTable parameters)
        throws ActionExecutionException
    {
        ClusterRoot cr = (ClusterRoot)object;
        
        Log.customer.debug("In Fire of AMSReceiptApproved"+cr);
        //Get the order associated with the Receipt
        ClusterRoot order=(ClusterRoot)cr.getFieldValue("Order");
        BaseVector polines = (BaseVector)order.getFieldValue("LineItems");
        BaseVector rclines = (BaseVector)cr.getFieldValue("ReceiptItems");
        
        if(order instanceof ariba.purchasing.core.ERPOrder)
        {
            boolean encumbered = ((Boolean)order.getFieldValue("Encumbered")).booleanValue();            
            boolean closeOrder = ((Boolean)cr.getFieldValue("CloseOrder")).booleanValue();
            Log.customer.debug("The encumbered flag is %s",new Boolean(encumbered));
            if(encumbered && closeOrder)
            {
                //Generate the Liquidation transcation by calling AMSLiquidationGenerator
                Log.customer.debug("About to call the liquidationGenerator...");
                //Create the array of quantities
                //BigDecimal q[]={new BigDecimal(1)};
                int rcLines = rclines.size();
                Log.customer.debug("Size of ReceiptLineItem...%s",
                               new Integer(rcLines));
                BigDecimal quantities[]=new BigDecimal[rcLines];
                for(int i=0;i<rcLines;i++)
                {
                    Log.customer.debug("Inside for Loop...# : %s", new Integer(i));
                    // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                    BaseObject receiptItem = (BaseObject)rclines.get(i);
                    BigDecimal orderedQty = (BigDecimal)receiptItem.getDottedFieldValue("LineItem.Quantity");
                    BigDecimal prevAccQty = (BigDecimal)receiptItem.getFieldValue("NumberPreviouslyAccepted");
                    BigDecimal curAccQty = (BigDecimal)receiptItem.getFieldValue("NumberAccepted");                
                    quantities[i]=(orderedQty.subtract(prevAccQty)).subtract(curAccQty);
                }
                Log.customer.debug("After for loop...");
                AMSLiquidationGenerator liquidationGenerator= new AMSLiquidationGenerator(cr,(PurchaseOrder)order,quantities);     
                liquidationGenerator.generateLiquidation("PCX");
                Log.customer.debug("After calling the liquidationGenerator...");
            }
        }
        // Ariba 8.0: cannot use reture true with fire
        //return true;
        return;
        
    }
}
