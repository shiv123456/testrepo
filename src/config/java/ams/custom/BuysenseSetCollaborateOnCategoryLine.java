package config.java.ams.custom;

import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.procure.core.CategoryLineItemDetails;
import ariba.purchasing.core.ReqLineItem;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
import ariba.workforce.core.LaborLineItemDetails;

/**
    Sarath :After migrating to 9r1 we are facing an unhandeled exception when we create a Collaborative line item.
            We have custom trigger in 822 to set collaborate filed to Yes on create of a category line item. In 9r1 On change of 
            collaborative field, OOTB fires a trigger called 'ClearItemsOnItemizationChoice' to clear MilestoneItemization in ReqLine
            and is throwing unhandeled exception due to unavialbility of LineItem field by the time the trigger is being called.

            So modified the tigger so that it will be fired on change of CategoryLineItemDetailsVecotr on ReqLineItem.
*/
public class BuysenseSetCollaborateOnCategoryLine extends Action
{
    public void fire (ValueSource object, PropertyTable params) throws ActionExecutionException
    {
    	Log.customer.debug("*****FIRING CoVAAction*****");
    	if (object == null)
		return;
    	ReqLineItem rli = (ReqLineItem) object;
    	CategoryLineItemDetails categoryLine =(CategoryLineItemDetails) rli.getCategoryLineItemDetails();
    	if(categoryLine!=null)
    	{    	    
    	    if(categoryLine instanceof LaborLineItemDetails || categoryLine.toString().indexOf("ConsultingLineItemDetails") >= 0)
            {
                categoryLine.setFieldValue("Collaborate", Boolean.TRUE);                
            }            
    	}    	
    }
}