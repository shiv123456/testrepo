/**
* DefaultAgencyFilter Object generates Agency (Client Name) <br>
* filter field automatically for reporting.
*
* In addition, it sets the editability of Agency filter on the  <br>
* report filter screen. Only users with QueryAll permission will <br>
* see a chooser, allowing users to select Agency filter value(s). <br>
* Users without QueryAll permission will see a non-editable test field. <br><br>
*
* @version  @(#)DefaultAgencyFilter.java 1.0 4/1/2001<br><br>
*
* Copyright 2001 by American Management Systems, Inc.,<br>
* 4050 Legato Road, Fairfax, Virginia, U.S.A.<br>
* All rights reserved.<br><br>
*
* This software is the confidential and proprietary information <br>
* of American Management Systems, Inc. ("Confidential Information"). You<br>
* shall not disclose such Confidential Information and shall use<br>
* it only in accordance with the terms of the license aggreement<br>
* you entered into with American Management Systems, Inc.<br>
*
* April, 2001 Shih-Kwang (Sarah) Liu
*
* @author Shih-Kwang (Sarah) Liu
*/

/* 09/11/2003: Updates for Ariba 8.0 (Richard Lee)*/
/* 10/10/2003: Updates for Ariba 8.1 (Richard Lee)*/
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import java.util.Map;

import ariba.approvable.core.ApprovableType;
import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.fields.Action;
import ariba.base.fields.FieldProperties;
import ariba.base.fields.ValueSource;
import ariba.search.core.SearchExpression;
import ariba.search.core.SearchTerm;
import ariba.search.core.SearchTermBase;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

/**
 * DefaultAgencyFilter Object automatically generates Agency (Client Name) <br>
 * filter field for operational reports. Also sets the editability of
 * Agency filter based on user permission. If the user has QueryAll permission,
 * Agency will be displayed as a chooser, otherwise
 * it will be a non-editable text field.
 *
 * @version     1.1 01 April 2001
 * @author          Shih-Kwang Liu
 */

//81->822 changed Hashtable to Map
public class DefaultAgencyFilter extends Action 
{
    
    // Ariba 8.1 added luUser
    //Ariba 8.1: Added class definition to decipher between ariba.common.core.User class and new ariba.user.core.User class.
    ariba.user.core.User luUser = getUser();
    SearchTerm moSearchTerm = null;
    SearchExpression moSearchExpression = null;
    private static final String ST_TARGET = "ClientNameBaseID";
    private static final String ST_TARGET_PATH = "ClientName";
    private static final String ST_THE_VALUE = "TheValue";
    private static final String ST_GROUP = "Group";
    private static final String ST_REPORT_FILTERS = "ReportFilters";
    private static final String ST_EDITABLE = "Editable";
    
    /**
    * Creates a DefaultAgencyFilter object.
    */
    public DefaultAgencyFilter () 
    {
        Log.customer.debug("**********************************************************");
        Log.customer.debug("***DefaultAgencyFilter: Starting DefaultAgencyFilter class");
        Log.customer.debug("**********************************************************");
    }
    
    /**
    * Implements parent Action class method. Executes the given action.
    *
    * @param foObject The object receiving the action.
    * @param params Parameters used to perform the action.
    */
    public void fire (ValueSource foObject, PropertyTable foParams) 
    {
        Log.customer.debug (">>>>>>>>>>>> From DefaultAgencyFilter.java >>>>>>>>>>>>");
        Log.customer.debug (">>> DefaultAgencyFilter: fire()");
        
        moSearchTerm = getSearchTerm (foObject, foParams);
        if (moSearchTerm == null) 
        {
            Log.customer.debug ("*** moSearchTerm object cannot be initialized.");
            Log.customer.debug ("<<<<<<<<<<<< END OF TRIGGER DefaultAgencyFilter.Fire WITH ERROR <<<<<<<<<<<<");
            return;
        }
        else
            Log.customer.debug ("*** Current SearchTerm fieldName: %s",
                            moSearchTerm.getFieldName());
        
        moSearchExpression = getSearchExpression(moSearchTerm);
        if (moSearchExpression == null) 
        {
            Log.customer.debug ("*** moSearchExpression object not initialized.");
            Log.customer.debug ("<<<<<<<<<<<< END OF TRIGGER DefaultAgencyFilter.fire WITH ERROR <<<<<<<<<<<<");
            return;
        }
        else
            Log.customer.debug ("*** Current SearchExpression = %s",
                            moSearchExpression.toString());
        
        if (update (ST_TARGET, ST_TARGET_PATH)) 
        {
            Log.customer.debug ("***%s field udpated!", ST_TARGET_PATH);
        }
        Log.customer.debug ("<<<<<<<<<<<< END OF TRIGGER DefaultAgencyFilter.fire <<<<<<<<<<<<");
    }
    
    /**
    * Updates the target SearchTerm object if necessary.
    *
    * @param foTargetFieldName UniqueName of target field from ReportColumnMeta.csv
    * @param fsFilterFieldPath FilterFieldPath in ReportColumnMeta.csv for target field.
    * @return boolean True if updated. False if not.
    */
    public boolean update (String fsTargetFieldName, String fsFilterFieldPath) 
    {
        Log.customer.debug (">>> DefaultAgencyFilter.update");
        
        SearchTerm loTargetTerm = null;
        // Determine if SearchExpression has any SearchTerm. If not, then do nothing.
        // numberOfTerms returns -1 if vector is null, -2 if SearchExpression is null
        int liTermNum = numberOfTerms();
        if (liTermNum <= 0) 
        {
            Log.customer.debug ("<<< End DefaultAgencyFilter.update with no update");
            return false;
        }
        // If has SearchTerms, see if current SearchTerm is target search term
        // If current SearchTerm is the target, then do nothing.
        // SearchTerm that contains Agency filter need to be updated after it has
        // been created. Otherwise the update will not take in effect.
        // Therefore, update will not be done to the current SearchTerm if it
        // is the one holding Agency filter.
        if (isTargetField(fsTargetFieldName)) 
        {
            Log.customer.debug ("Current Search Term matches Target. Do not update.");
            Log.customer.debug ("<<< End DefaultAgencyFilter.update with no update");
            return false;
        }
        // If target is not current, see if Terms vector contains the target term
        // Do nothing if not.
        // If the search expression does not contain SearchTerm for Agency Filter,
        // then it is not yet created. Then it cannot be updated.
        loTargetTerm = getSearchTerm (fsFilterFieldPath);
        if (loTargetTerm == null) 
        {
            Log.customer.debug ("***Target %s SearchTerm is not found!",
                            fsFilterFieldPath);
            Log.customer.debug ("<<< End DefaultAgencyFilter.update with no update");
            return false;
        }
        // If target search term is present, see if the target term is active
        // If active, then do nothing (already activated!)
        // A SearchTerm will be active when it contains a value.
        if (loTargetTerm.isActive()) 
        {
            Log.customer.debug ("***%s SearchTerm is already active",
                            fsFilterFieldPath);
            Log.customer.debug ("<<< End DefaultAgencyFilter.update with no update");
            return false;
        }
        // If target term not active, activate it by setting the field value.
        //Ariba 8.1: Modified the method to get the partition User for the ariba.user.core.User
        //in order to access the getPCards() method from old ariba.common.core.User class.
        setFieldValue (loTargetTerm,
                       ariba.common.core.User.getPartitionedUser(getUser(),Base.getSession().getPartition()).getDottedFieldValue(ST_TARGET_PATH));
        
        // Sets Agency editiblity. Editable if user can QueryAll permission,
        // Not editable otherwise.
        Boolean lBoolIsEditable = isEditable();
        boolean lbIsEditableSet = setAgencyEditable ((BaseObject)loTargetTerm,
                                                     lBoolIsEditable.toString());
        // Checks to see if Agency filter editability is set successfully.
        if (lbIsEditableSet) 
        {
            Log.customer.debug ("*** set editable successfully!");
        }
        else 
        {
            Log.customer.debug ("*** editable not set -- error! ");
        }
        Log.customer.debug ("<<< End DefaultAgencyFilter.update");
        return true;
    }
    
    /**
    * Checks to see if the current search term field name is the same as the given field name.
*
    * @param fsTargetFieldName UniqueName of target field from ReportColumnMeta.csv
    * @return boolean True if current SearchTerm matches with the target.
    * False otherwise.
    */
    public boolean isTargetField(String fsTargetFieldName) 
    {
        Log.customer.debug (">>> DefaultAgencyFilter: isTargetField()");
        if (moSearchTerm == null) 
        {
            Log.customer.debug ("***SearchTerm param is null");
            Log.customer.debug ("<<< End of DefaultAgencyFilter.isTargetField");
            return false;
        }
        if (moSearchTerm.getFieldName().trim().equals(fsTargetFieldName)) 
        {
            Log.customer.debug ("<<< End of DefaultAgencyFilter.isTargetField");
            return true;
        }
        else 
        {
            Log.customer.debug ("<<< End of DefaultAgencyFilter.isTargetField");
            return false;
        }
    }
    
    /**
    * Determines if the user has QueryAll permission to edit Agency filter value.
    * @return True if user has QueryAll permission, false otherwise.
    */
    public Boolean isEditable() 
    {
        Log.customer.debug (">>> DefaultAgencyFilter.isEditable");
        //Ariba 8.1: Added class definition to decipher between ariba.common.core.User class and new ariba.user.core.User class.
        ariba.user.core.User luUser = getUser();
        // Ariba 8.1 modified canQueryAll method
        //boolean lbIsEditable = getUser().canQueryAll();
        boolean lbIsEditable = ApprovableType.canQueryAll(luUser);
        Log.customer.debug ("*** User can QueryAll: %s",
                        lbIsEditable ? "Yes -- User can select Agency filter" : "No -- Agency filter is protected");
        Log.customer.debug ("<<< End DefaultAgencyFilter.isEditable");
        return new Boolean (lbIsEditable);
    }
    
    /**
    * Determines the number of SearchTerms contained in the underlying
    * SearchExpression object.
    *
    * @return Number of Search Terms in the SearchExpression object.
    * Returns -2 if SearchExpression object is null.
    * Returns -1 if SearchTerms object is null in SearchExpression.
    */
    public int numberOfTerms() 
    {
        Log.customer.debug (">>> DefaultAgencyFilter.numberOfTerms");
        if (moSearchExpression == null) 
        {
            Log.customer.debug ("***SearchExpression is null");
            Log.customer.debug ("<<< End DefaultAgencyFilter.numberOfTerms Return Code -2");
            return -2;
        }
        BaseVector loBaseVector = moSearchExpression.getSearchTerms();
        if (loBaseVector == null) 
        {
            Log.customer.debug ("*** SearchTerm vector is null in SearchExpression");
            Log.customer.debug ("<<< End DefaultAgencyFilter.numberOfTerms Return Code -1");
            return -1;
        }
        Log.customer.debug ("*** Size of SearchTerms vector in SearchExpression: %s",
                        loBaseVector.size());
        Log.customer.debug ("<<< End of DefaultAgencyFilter.numberOfTerms");
        return loBaseVector.size();
    }
    
    // Private method to set Agency filter editability based on the parameter passed.
    // Filter editability is stored as String in Ariba. If in future this changes, this code needs to be
    // modified. In such case, Agency filter Editability will not be updated.
    //
    // @param foBaseObject BaseObject holding the agency filter value. In this case it is the SearchTerm.
    // @param fsEditable String representation of weather or not Agency should be editable or not.
    // In this case, it will be either "true" or "false" (String).
    private boolean setAgencyEditable (BaseObject foBaseObject,
                                       String fsEditable) 
    {
        Log.customer.debug (">>> DefaultAgencyFilter.setAgencyEditable");
        FieldProperties loFieldProperties = null;
        Object loGroupVal = null;
        Object loReportFilterVal = null;
        
        // Get FieldProperties associated with the given base object.
        loFieldProperties = foBaseObject.getFieldProperties(ST_THE_VALUE);
        if (loFieldProperties == null) 
        {
            Log.customer.debug ("Did not get field properties with key %s",
                            ST_THE_VALUE);
            Log.customer.debug ("<<< Exit DefaultAgencyFilter.setAgencyEditable() with error");
            return false;
        }
        else 
        {
            Log.customer.debug ("*** This is fieldProperties: %s",
                            loFieldProperties.toString());
        }
        
        // Get Group property from the field property object. This should be a hashtable object.
        loGroupVal = loFieldProperties.getPropertyForKey (ST_GROUP);
        if (loGroupVal == null) 
        {
            Log.customer.debug ("Did not get groupVal with key %s", ST_GROUP);
            Log.customer.debug ("<<< Exit DefaultAgencyFilter.setAgencyEditable() with error");
            return false;
        }
        else if (! (loGroupVal instanceof Map)) 
        {
            Log.customer.debug ("*** This is groupVal: %s", loGroupVal.toString());
            Log.customer.debug ("*** groupVal not instance of Map ***");
            Log.customer.debug ("*** groupVal class is %s", loGroupVal.getClass());
            Log.customer.debug ("<<< Exit DefaultAgencyFilter.setAgencyEditable() with error");
            return false;
        }
        else 
        {
            Log.customer.debug ("*** This is groupVal: %s", loGroupVal.toString());
        }
        
        // From the Group objec, get the ReportFilter hashtable.
        loReportFilterVal = ((Map)loGroupVal).get(ST_REPORT_FILTERS);
        // Checks to see if loReportFilterVal is obtained.
        if (loReportFilterVal == null) 
        {
            Log.customer.debug ("***Did not get reportFilterVal with key %s",
                            ST_REPORT_FILTERS);
            Log.customer.debug ("<<< Exit DefaultAgencyFilter.setAgencyEditable() with error");
            return false;
        }
        // If loReportFilterVal is not null but not instance of hashtable, exit method.
        else if (! (loReportFilterVal instanceof Map)) 
        {
            Log.customer.debug ("***Got loReportFilterVal: %s", loReportFilterVal);
            Log.customer.debug ("*** loReportFilterVal not instance of HashTable");
            Log.customer.debug ("*** loReportFilterVal is class of %s",
                            loReportFilterVal.getClass());
            Log.customer.debug ("<<< Exit DefaultAgencyFilter.setAgencyEditable() with error");
            return false;
        }
        // If loReportFilterVal is obtained and is instance of Map, set editability of the filter.
        else 
        {
            Log.customer.debug ("***Got loReportFilterVal: %s", loReportFilterVal);
            try 
            {
                Object loOldEditable = ((Map)loReportFilterVal).put(ST_EDITABLE, fsEditable);
                if (loOldEditable == null) 
                {
                    Log.customer.debug ("*** Did not replace any value in ReportFilter hashTable");
                }
                else if (! (loOldEditable instanceof String)) 
                {
                    Log.customer.debug ("***editable val is not instance of String but: %s", loOldEditable.getClass());
                    Log.customer.debug ("***Putting the original editable value back!");
                    ((Map)loReportFilterVal).put(ST_EDITABLE,
                                                       loOldEditable);
                    Log.customer.debug ("<<< Exit DefaultAgencyFilter.setAgencyEditable() with error -- wrong class type");
                    return false;
                }
                else 
                {
                    Log.customer.debug ("***Replaced value is %s",
                                    loOldEditable.toString());
                    Log.customer.debug ("***New editable value is %s", fsEditable);
                }
            }
            catch (Exception loException) 
            {
                Log.customer.debug ("*** Exception: %s", loException.toString());
                Log.customer.debug ("<<< Exit DefaultAgencyFilter.setAgencyEditable() with error");
                return false;
            }
        }
        Log.customer.debug ("<<< End of DefaultAgencyFilter.setAgencyEditable");
        return true;
    }
    
    /**
        * Set the field value of a given SearchTerm.
        *
        * @param foTerm SeachTerm that needs its value to be set.
        */
    public void setFieldValue (SearchTerm foTerm, Object foValue) 
    {
        Log.customer.debug (">>> DefaultAgencyFilter: setFieldValue()");
        if (foTerm == null) 
        {
            Log.customer.debug ("*** foTerm param is null. Cannot set value for a null SearchTerm");
            Log.customer.debug ("<<< End of DefaultAgencyFilter.setFieldValue with Error");
            return;
        }
        Log.customer.debug ("***Setting field: %s", foTerm.getFieldName());
        // if the value is null, then will do nothing (handled by SearchTerm class).
        try 
        {
            foTerm.setFieldValue (SearchTermBase.KeyTheValue, foValue);
        }
        catch (Exception loException) 
        {
            Log.customer.debug ("***setFieldValue generated an exception: %s",
                            loException.toString());
            Log.customer.debug ("<<< End of DefaultAgencyFilter.setFieldValue with Exception");
        }
        
        Log.customer.debug ("*** Adding term to SearchExpression");
        foTerm.getSearchExpression().addTerm(foTerm);
        
        Log.customer.debug ("***Value of field %s should become user's Agency ",
                        foTerm.getFieldName());
        Log.customer.debug ("<<< End of DefaultAgencyFilter.setFieldValue");
    }
    
    /**
    * Returns field value for a searchTerm.
    *
    * @return SearchTerm field value as an object.
    */
    public Object getFieldValue (SearchTerm foSearchTerm) 
    {
        Log.customer.debug (">>> DefaultAgencyFilter: getFieldValue()");
        if (foSearchTerm == null) 
        {
            Log.customer.debug ("***Cannot get value for a null SearchTerm");
            Log.customer.debug ("<<< End of DefaultAgencyFilter.getFieldValue with error");
            return null;
        }
        Log.customer.debug ("***name of field to get: %s",
                        foSearchTerm.getFieldName());
        Object loObject = foSearchTerm.getFieldValue(SearchTermBase.KeyTheValue);
        Log.customer.debug ("<<< End of DefaultAgencyFilter.getFieldValue");
        return loObject;
    }
    
    /**
        * Returns the Search Term associated with a given ValueSource Object.
        *
        * @param foObject Underlying ValueSource object.
        * @param foParams Underlying PropertyTable
        *
    * @return SearchTerm associated with the given Value Source object.
    * Will return null if the ValueSource object is not an instance of SearchTerm.
        */
    public SearchTerm getSearchTerm (ValueSource foObject,
                                     PropertyTable foParams) 
    {
        Log.customer.debug (">>> DefaultAgencyFilter: getSearchTerm()");
        if (foObject == null) 
        {
            Log.customer.debug ("*** ValueSource is null. Cannot generate SearchTerm!");
            Log.customer.debug ("<<< End of DefaultAgencyFilter.getSearchTerm with Error");
            return null;
        }
        else if (!(foObject instanceof SearchTerm)) 
        {
            Log.customer.debug ("***ValueSource not a SearchTerm object! SearchTerm is still null");
            Log.customer.debug ("<<< End of DefaultAgencyFilter.getSearchTerm with Exception");
            return null;
        }
        Log.customer.debug ("<<< End of DefaultAgencyFilter.getSearchTerm");
        return (SearchTerm)foObject;
    }
    
    /**
    * Returns Search Expression associated with the given SearchTerm.
    *
    * @param foSearchTerm Any SearchTerm in which the target SearchExpression holds.
    * @return SearchExpression holding the given SearchTerm.
    */
    public SearchExpression getSearchExpression (SearchTerm foSearchTerm) 
    {
        Log.customer.debug (">>> DefaultAgencyFilter: setSearchExpression()");
        if (foSearchTerm == null) 
        {
            Log.customer.debug ("*** Invalid parameter. Cannot get SearchExpression for Null SearchTerm.");
            Log.customer.debug ("<<< End DefaultAgencyFilter.setSearchExpression with Error");
            return null;
        }
        
        // jackie 81-822 typecasting might cause a problem
        SearchExpression loSearchExpression =(SearchExpression) foSearchTerm.getSearchExpression();
        Log.customer.debug ("<<< End of DefaultAgencyFilter.setSearchExpression");
        return loSearchExpression;
    }
    
    /**
    * Obtains the Effective user.
    *
    * @return Effective User object. Returns null if Effective user cannot be obtained.
    */
    //Ariba 8.1: Added class definition to decipher between ariba.common.core.User class and new ariba.user.core.User class.
    public ariba.user.core.User getUser () 
    {
        Log.customer.debug (">>> DefaultAgencyFilter.getUser");
        //Ariba 8.1: Added class definition to decipher between ariba.common.core.User class and new ariba.user.core.User class.
        ariba.user.core.User loUser = null;
        try 
        {
            //Ariba 8.1: Added class definition to decipher between ariba.common.core.User class and new ariba.user.core.User class.
            loUser = (ariba.user.core.User) Base.getSession().getEffectiveUser();
            Log.customer.debug ("***The user is: %s", loUser);
        }
        catch (Exception loException) 
        {
            Log.customer.debug ("***Unable to get effective user from base %s",
                            loException.toString());
            Log.customer.debug ("<<< End DefaultAgencyFilter.getUser with exception");
        }
        Log.customer.debug ("<<< End of DefaultAgencyFilter.getUser");
        return loUser;
    }
    
    /**
    * Returns the SearchTerm object represented by the given fieldName
    *
    * @param fsFilterFieldPath FilterFieldPath in ReportColumnMeta.csv for target field.
    */
    public SearchTerm getSearchTerm(String fsFilterFieldPath) 
    {
        Log.customer.debug (">>> DefaultAgencyFilter.getSearchTerm ");
        SearchTerm loTargetSearchTerm = (SearchTerm) moSearchExpression.termForField (fsFilterFieldPath);
        // Ariba 8.0: needed explicit casting SearchTerm
        // SearchTerm loTargetSearchTerm = moSearchExpression.termForField (fsFilterFieldPath);
        if (loTargetSearchTerm == null) 
        {
            Log.customer.debug ("***SearchTerm with fieldName '%s' is not found!",
                            fsFilterFieldPath);
        }
        else
            Log.customer.debug ("***SearchTerm with fieldName '%s' Found!",
                            fsFilterFieldPath);
        Log.customer.debug ("<<< End of DefaultAgencyFilter.getSearchTerm");
        return loTargetSearchTerm;
    }
}
