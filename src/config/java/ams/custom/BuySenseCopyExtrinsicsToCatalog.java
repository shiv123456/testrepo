package config.java.ams.custom;

// 08/07/2003: Updates for Ariba 8.0 (David Chamberlain)
/* 09/05/2003: Updates for Ariba 8.0 (Richard Lee) */
/* 09/11/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

import java.util.Map;
import java.util.List;
import  ariba.base.core.*;
//import  netscape.util.Enumeration;
import ariba.util.log.Log;
// Ariba 8.0: Aren't using ServerParameters and it's gone in 8.0.
//import  ariba.server.objectserver.core.ServerParameters;
import  ariba.procure.core.CatalogEntry;
import  ariba.base.core.aql.*;
// Ariba 8.0: SimpleScheduledTask moved from ariba.server.objectserver
// to ariba.util.scheduler. Changed the import statement to reflect this.
//import ariba.util.scheduler.SimpleScheduledTask;
//Ariba 8.0: commented out import SimpleScheduledTask since not using
//Ariba 8.0: added import scheduler *
import ariba.util.scheduler.*;
// Ariba 8.1: Need ListUtil
import ariba.util.core.ListUtil;

//  Scheduled Task
//  Copies Extrinsic Field Data which is stored as  concatenated string in the Supplier Part Auxiliary
//  field to the extrinsics defined on the CatalogEntry object for all the catalog items
//  Needs to run everytime an Ad-Hoc import is completed.

// Mark Harley - Feb 13, 2001 - Modified to extract concat string from the AvailableTerritories field.
//81->822 changed Vector to List
//81->822 changed Hashtable to Map
public class BuySenseCopyExtrinsicsToCatalog extends ScheduledTask
// Ariba 8.0: Replaced SimpleScheduledTask with ScheduledTask
//public class BuySenseCopyExtrinsicsToCatalog extends SimpleScheduledTask
{
    public void init(Scheduler scheduler,
                     String scheduledTaskName, Map arguments)
    // Ariba 8.0: Replace Partition with Scheduler
    //public void init(Partition partition, String scheduledTaskName, Map arguments)
    {
        super.init(scheduler, scheduledTaskName, arguments);
        //Ariba 8.0: Replaced partition with scheduler
        //super.init(partition, scheduledTaskName, arguments);
    }
    
    public void run()
    {
        Log.customer.debug("Starting scheduled task");
        System.out.println("System print - starting task");
        
        // Ariba 8.0: The method signature was changed; see method declaration comment below. 
        // updateCatalogEntry(partition());
        updateCatalogEntry();
        
        // Log.misc.M("...Done Updating All Catalog Entries.");
    }
    
    
    
    /* Ariba 8.0: The partition parameter wasn't being used in this method. It had to be removed
       because this class file now derives from ScheduledTask and the init() method signature
       has changed such that the first parameter is no longer a Partition but a Scheduler. Given
       that, no partition is used in ScheduledTask.class and one cannot be retreived as it was
       in SimpleScheduledTask::partition() anymore. */
    // private void updateCatalogEntry(Partition partition)
    private void updateCatalogEntry()
    {
        
        // Define vector hold the parsed extrinsic fields for each catalog item
        
        //java.util.List vCommonSupplierData = getCommonSupplierObjects(partition());
        //List catalogObjects;
        List parsedFields;
        String combinedField;
        
        
        // Define the AQL statement to retrieve all the catalog items imported in Buyer instance
        
        String text = "SELECT * "+ "FROM CatalogEntry PARTITION NONE";
        
        
        AQLOptions catalogOptions = new AQLOptions();
        Log.customer.debug("creating catalog query");
        AQLQuery catalogQuery = AQLQuery.parseQuery(text);
        Log.customer.debug("executing query");
        
        // Execute the AQL query and retrieve the results in the AQLResultCollection object
        AQLResultCollection catalogResults = Base.getService().executeQuery(catalogQuery, catalogOptions);
        Log.customer.debug("Query executed");
        // Process and display any errors resulting from the executed query
        if (catalogResults.getFirstError() != null) 
        {
            Log.customer.debug(catalogResults.getFirstError().toString());
            return;
        }
        
        // Process the results of the AQL query, one catalog item at a time. Each entry in the catalogresults vector is
        // a vector with the first element being the base id of the CatalogEntry object. Get the handle of the CatalogEntry
        // object by using call to the Base. service() methods
        
        while (catalogResults.next()) 
        {
            Log.customer.debug("First column class is "+ catalogResults.getField(0));
            Log.customer.debug("First column is "+ catalogResults.getField(0).getClassName());
            
            /* Ariba 8.0: BaseService::getObjectWithWriteLock() has been deprecated by
               BaseSession::objectForWrite(). The appropriate change is made below. */
            // CatalogEntry entry = (CatalogEntry)Base.getService().getObjectWithWriteLock((BaseId)catalogResults.getObject(0));
            CatalogEntry entry = (CatalogEntry)Base.getSession().objectForWrite((BaseId)catalogResults.getObject(0));
            
            Log.customer.debug("Auxillary id before update : "+ entry.getFieldValue("SupplierPartAuxiliaryID"));
            
            // Mark Harley - start
            Log.customer.debug("Get Available Territories");
            List AvailableTerritoriesVect = (List)entry.getFieldValue("AvailableTerritories");
            
            if (AvailableTerritoriesVect.size() == 0)
            {
                Log.customer.debug("No elements in Available Territory List");
                combinedField = "";
            }
            else
            {
                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                combinedField = (String)AvailableTerritoriesVect.get(0);
            }
            //String combinedField = (String)entry.getFieldValue("SupplierPartAuxiliaryID");
            // Mark Harley - End
            
            
            Log.customer.debug("combinedfield vALUE IS: "+ combinedField);
            
            // copy the dates to the description
            entry.setDottedFieldValue("Description.ExpirationDate",
                                      entry.getFieldValue("ExpirationDate"));
            entry.setDottedFieldValue("Description.EffectiveDate",
                                      entry.getFieldValue("EffectiveDate"));
            entry.save();
            
            //Call the parseCombinedField method . The methods takes the concatenated string that is retrieved from the
            //supplierpartauxiliary id field and parses it into individual fields. It then returns a vector populated with
            // the parsed fields. Copy each of the parsed fields into the appropriate Extrinsic field on the CatalogEntry object.
            
            parsedFields = parseCombinedField(combinedField);
            if (parsedFields.size() !=0)
            {
                
                // Modified the code to handle the cases where the number of parsed fields returned could vary from 1 to 7 or more
                // added all the IF conditions for that purpose
                
                if (parsedFields.size() >= 1)
                {
                    
                    // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                    Log.customer.debug("first element is " + parsedFields.get(0) );
                    
                    // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                    entry.setDottedFieldValue("Description.ContractNum",
                                              (String)parsedFields.get(0));
                    Log.customer.debug("Description.ContractNum: " + entry.getDottedFieldValue("Description.ContractNum"));
                    
                    
                    if (parsedFields.size() >= 2)
                    {
                        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                        Log.customer.debug("second " + parsedFields.get(1) );
                        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                        if (((String)parsedFields.get(1)).compareTo("") == 0 )
                        {
                            entry.setDottedFieldValue("Description.SmallBusiness",new Boolean("FALSE"));
                        }
                        else
                        {
                            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                            if (((String)parsedFields.get(1)).compareTo("S") == 0 )
                            {
                                entry.setDottedFieldValue("Description.SmallBusiness", new Boolean("TRUE"));
                            }
                            else
                            {
                                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                entry.setDottedFieldValue("Description.SmallBusiness",new Boolean((String)parsedFields.get(1)));
                            }
                        }
                        
                        if (parsedFields.size() >= 3)
                        {
                            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                            Log.customer.debug("third element is " + parsedFields.get(2) );
                            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                            if (((String)parsedFields.get(2)).compareTo("") == 0 )
                            {
                                entry.setDottedFieldValue("Description.WomanOwnedBusiness",new Boolean("FALSE"));
                            }
                            else
                            {
                                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                if (((String)parsedFields.get(2)).compareTo("W") == 0 )
                                {
                                    entry.setDottedFieldValue("Description.WomanOwnedBusiness", new Boolean("TRUE"));
                                }
                                else
                                {
                                    // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                    entry.setDottedFieldValue("Description.WomanOwnedBusiness",new Boolean((String)parsedFields.get(2)));
                                }
                            }
                            
                            if (parsedFields.size() >= 4)
                            {
                                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                Log.customer.debug("fourth element is " + parsedFields.get(3) );
                                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                if (((String)parsedFields.get(3)).compareTo("") == 0 )
                                {
                                    entry.setDottedFieldValue("Description.MinorityOwnedBusiness",new Boolean("FALSE"));
                                }
                                else
                                {
                                    // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                    if (((String)parsedFields.get(3)).compareTo("M") == 0 )
                                    {
                                        entry.setDottedFieldValue("Description.MinorityOwnedBusiness", new Boolean("TRUE"));
                                    }
                                    else
                                    {
                                        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                        entry.setDottedFieldValue("Description.MinorityOwnedBusiness",new Boolean((String)parsedFields.get(3)));
                                    }
                                }
                                
                                if (parsedFields.size() >= 5)
                                {
                                    // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                    Log.customer.debug("fith element is " + parsedFields.get(4) );
                                    // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                    entry.setDottedFieldValue("Description.ContractType",(String)parsedFields.get(4));
                                    
                                    if (parsedFields.size() >= 6)
                                    {
                                        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                        Log.customer.debug("sixth element is " + parsedFields.get(5) );
                                        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                        if (((String)parsedFields.get(5)).compareTo("") == 0 )
                                        {
                                            entry.setDottedFieldValue("Description.RecycledProduct",new Boolean("FALSE"));
                                        }
                                        else
                                        {
                                            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                            if (((String)parsedFields.get(5)).compareTo("R") == 0 )
                                            {
                                                entry.setDottedFieldValue("Description.RecycledProduct", new Boolean("TRUE"));
                                            }
                                            else
                                            {
                                                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                Log.customer.debug("sixth element is " + parsedFields.get(5) );
                                                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                entry.setDottedFieldValue("Description.RecycledProduct",new Boolean((String)parsedFields.get(5)));
                                                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                Log.customer.debug("sixth element is " + parsedFields.get(5) );
                                            }
                                        }
                                        if (parsedFields.size() >= 7)
                                        {
                                            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                            Log.customer.debug("seventh element is " + parsedFields.get(6) );
                                            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int) 
                                            entry.setDottedFieldValue("Description.HazardousProduct",(String)parsedFields.get(6));
                                            
                                            if (parsedFields.size() >= 8)
                                            {
                                                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                Log.customer.debug("eigth element is " + parsedFields.get(7) );
                                                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                entry.setDottedFieldValue("Description.PaymentTerm",(String)parsedFields.get(7));
                                                
                                                if (parsedFields.size() >= 9)
                                                {
                                                    // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                    Log.customer.debug("ninth element is " + parsedFields.get(8) );
                                                    // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                    entry.setDottedFieldValue("Description.MinimumOrderString",(String)parsedFields.get(8));
                                                    
                                                    
                                                    if (parsedFields.size() >= 10)
                                                    {
                                                        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                        Log.customer.debug("tenth element is " + parsedFields.get(9) );
                                                        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                        entry.setDottedFieldValue("Description.MaximumOrderString",(String)parsedFields.get(9));
                                                        
                                                        if (parsedFields.size() >= 11)
                                                        {
                                                            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                            Log.customer.debug("eleventh element is " + parsedFields.get(10) );
                                                            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                            entry.setDottedFieldValue("Description.PCard",(String)parsedFields.get(10));
                                                            
                                                            if (parsedFields.size() >= 12)
                                                            {
                                                                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                                Log.customer.debug("eleventh element is " + parsedFields.get(11) );
                                                                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                                entry.setDottedFieldValue("Description.ServiceAreas",(String)parsedFields.get(11));
                                                                
                                                                if (parsedFields.size() >= 13)
                                                                {
                                                                    // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                                    entry.setDottedFieldValue("Description.SupplierFIN",(String)parsedFields.get(12));
                                                                    
                                                                    if (parsedFields.size() >= 14)
                                                                    {
                                                                        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                                        entry.setDottedFieldValue("Filter1",(String)parsedFields.get(13));
                                                                        
                                                                        if (parsedFields.size() >= 15)
                                                                        {
                                                                            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                                            entry.setDottedFieldValue("Filter2",(String)parsedFields.get(14));
                                                                            
                                                                            if (parsedFields.size() >= 16)
                                                                            {
                                                                                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                                                Log.customer.debug("last element is " + parsedFields.get(15) );
                                                                                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                                                                                entry.setDottedFieldValue("Filter3",(String)parsedFields.get(15));
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                }
                entry.save();
            }
            //}
            
            
            
            
        }
        catalogResults.close();
        return;
    }
    
    
    private List parseCombinedField(String combinedField)
    {
        // Ariba 8.1: List constructor is deprecated by ListUtil.newVector()
        List parsedFields = ListUtil.list();
        int startParseIndex = 0;
        
        while (true)
        {
            int indexValue = combinedField.indexOf(':',startParseIndex);
            if (indexValue == -1 )
                break;
            String valueOfField = combinedField.substring(startParseIndex,
                                                          indexValue);
            // Ariba 8.1: List::addElement() is deprecated by List::add()
            parsedFields.add(valueOfField);
            startParseIndex = indexValue + 1;
        }
        return parsedFields;
    }
}

