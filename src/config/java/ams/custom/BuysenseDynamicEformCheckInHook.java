//package config.java.ams.custom;
package config.java.ams.custom;

import java.util.List;

import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableHook;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.base.fields.FieldProperties;
import ariba.util.core.Constants;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseDynamicEformCheckInHook implements ApprovableHook {
	private static final List    NoErrorResult = ListUtil.list(Constants
			.getInteger(0));
	protected String             sResourceFile = "buysense.DynamicEform";
  private static String        lsLeftBrace   = "(";
  private static String        lsRightBrace  = ")";
  private static String        lsDotSpace    = " ";
  private static final boolean DEBUG         = true;
  public List valVector;

	public List run(Approvable approvable) {
		
		List loRetVector = NoErrorResult;
		Log.customer.debug("Inside BuysenseDynamicEformCheckInHook");
		Approvable loAppr = approvable;
		Log.customer.debug("The Approvable object is" + loAppr.getClassName());
		
		  Boolean lbRefire = (Boolean)loAppr.getFieldValue("BuysenseReFireWorkflow");
		  FieldProperties lfpFP = loAppr.getFieldProperties("BuysenseReFireWorkflow");
		  String lfpProp = null;
		  //We are setting the field property 'NULLVALUENAME' for field 'BuysenseReFireWorkflow' based on the value in the 'BuysenseReFireWorkflow'.
		  //If the value is 'true' then we are setting the property value to 'Checked:RefireWorkFLow'. If the value is null or 'false' we are setting
		  //the value to 'Checked:NoRefireWorkFlow'
		  //We are then using this property value to validate whether to re-fire the approval flow or not.
		  
		  if(lbRefire != null && lbRefire.booleanValue() == true)
		  {		  
			  lfpFP.setPropertyForKey("nullValueName", "Checked:RefireWorkFLow");
			  lfpProp = (String)lfpFP.getPropertyForKey("nullValueName");
		      loAppr.setFieldValue("BuysenseReFireWorkflow", new Boolean("false"));
		  }
		  else if(lbRefire == null ||(lbRefire != null && lbRefire.booleanValue() == false))
		  {
			 lfpFP.setPropertyForKey("nullValueName", "Checked:NoRefireWorkFlow"); 
		  }		
		
		loRetVector = validateReq(loAppr);

      if (((Integer) loRetVector.get(0)).intValue() < 0)
      {
          return loRetVector;
      }
      else
      {
          loRetVector = NoErrorResult;
      }
      
      if (DEBUG)
      {
          Log.customer.debug("Finished eVA_AMSReqSubmitHook.");
      }
      return loRetVector;
	}

  private List validateReq(Approvable loAppr) {
  	
  	String lsProfile = null;
  	List llProfileList = ListUtil.list();
  	
  	
	    ariba.user.core.User RequesterUser = (ariba.user.core.User)loAppr.getDottedFieldValue("Requester");

	    Partition appPartition = loAppr.getPartition();

	    ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,appPartition);
	    BaseVector lvProfiles = (BaseVector)RequesterPartitionUser.getFieldValue("BuysEformProfiles");
	    if(lvProfiles != null)
	    {
	    	int liSize = lvProfiles.size();
	    	Log.customer.debug("The size of the list is :"+liSize);
	    	for(int i=0;i<liSize;i++)
	    	{
	    		BaseObject oProfile = (BaseObject)Base.getSession().objectFromId((BaseId) lvProfiles.get(i));
	    		if(oProfile!=null)
	    		{
   			Log.customer.debug("The object picked is :"+oProfile+" and the profile name is"+oProfile.getFieldValue("ProfileName"));
	  			llProfileList.add((String)oProfile.getFieldValue("ProfileName"));
	    		}
	    	}
	    }
	    else
	    {
  		String lsError = ResourceService.getString(sResourceFile, "NoUserProfileError");
  		return ListUtil.list(Constants.getInteger(-1),lsError);	    	
	    }
	    
  	lsProfile = (String)loAppr.getDottedFieldValue("EformChooser.ProfileName");
  	if(lsProfile == null)
  	{
  		String lsError = ResourceService.getString(sResourceFile, "NoProfileError");
  		return ListUtil.list(Constants.getInteger(-1),lsError);
  	}
  	else
  	{
           if(!(llProfileList.contains(lsProfile))){
      		String lsError = ResourceService.getString(sResourceFile, "ProfileMismatchError");
      		return ListUtil.list(Constants.getInteger(-1),lsError);    	    	
  	       }
           else
           {
       	    BuysenseEformValidationEngine eVAValEng = new BuysenseEformValidationEngine();

              valVector = eVAValEng.runEformValidation(loAppr,lsProfile); 
              if (((Integer)valVector.get(0)).intValue() == -1)
              {
                  return valVector;
              }
           }
  	    
  	}  

  	return NoErrorResult;
  	}


	public static String getFormattedNumberString(int liWarningNum)
  {
      StringBuffer lsbTemp = new StringBuffer();
      lsbTemp.append(lsLeftBrace + liWarningNum + lsRightBrace + lsDotSpace);
      return lsbTemp.toString();
  }
  
  

}
