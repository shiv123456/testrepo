/*
    Copyright (c) 1996-2001 Buysense.
    Anup - June 2001
    ---------------------------------------------------------------------------------------------------

    Anup - 10/19/04 - UAT SPL # 40 - Reset Solicitation Flag before determination for solicitation. Removed
    code to this effect from the methods performing solicitaion logic.

    rlee, 8/23/2004: Ariba 8.1 Upgrade Dev SPL 95 - Field changes due to Integration

    (rlee, 12/23/02) eProcurement ST SPL # 667 - Added checks to prevent Punch-out items from going to eProcurement.
    Also modified warning message in WebComponents/config/resource/en_US/strings/ariba.procure.core.csv

    rlee, 3/21/2003, eProc ST SPL 907 - Punchout field getting set to null before checking whether
    the punchout item is above the threshold amount. Should only be set to null if above the threshold amount.

    rlee, 10/27/03: Enh SPL 18 and Prod SPL 218 - mod to check PCard Expiration date

    jnamadan, 10/29/03: eProcurement CR 38 - Modified to check the PrePrintedText table for the ALL clientname. Issue
    an error if it is not present

    anup, 12/17/03 ST SPL # 698 - Set SolicitationLine based on Solicitation Line rules. Removed code and calls
    to markNonCatalogReqLineForSolicitation() and convertCatalogReqLineToNonCatalog() and incorporated in
    AMSRequisitionApproved.java.

    rlee, 12/24/03: Prod SPL 247 - No edit to check if user is disassociated with the pcard.  Added check to see if PCardUsed is
                    still associated with the Requester.
    rlee, 5/25/2004: ST SPL 1318 - VITA: 2598 - Order Import not going to ADV
                    Added lsTransactionSource = TXNSOURCE_INTERFACES (which is EXT) to around line 206
 */

/* 12/22/2003: Updates for Ariba 8.1 (Jeff Namadan)
 Replaced all Util.integer()'s with Constants.getInteger()
 Replaced all Util.vector()'s with ListUtil.vector()
 Replaced all Util.getInteger()'s with Constants.getInteger()

 rgiesen Feb 11, 2004 Dev SPL #9
 The extrinsics EPP, OWMBENum and OWMBEBoolean are no longer needed for eVA

 Simha Sep 21, 2004 ST SPL #13
 Added validation for Need By Date being null on solicitation line.
 */

/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
 03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
 */

package config.java.ams.custom.HookEdits;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import ariba.app.util.Attachment;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableHook;
import ariba.approvable.core.AttachmentWrapper;
import ariba.approvable.core.Comment;
import ariba.approvable.core.LineItem;
import ariba.approvable.core.LineItemCollection;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.base.core.TransientData;
import ariba.common.core.PCard;
import ariba.common.core.SupplierLocation;
import ariba.procure.core.CategoryLineItemDetails;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.user.core.User;
import ariba.util.core.Constants;
import ariba.util.core.Date;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;
import config.java.ams.custom.AMSE2EConstants;
import config.java.ams.custom.AMSReqSubmitHook;
import config.java.ams.custom.BuysenseSupplierLocationFieldValidation;
import config.java.ams.custom.SetPrePrintedText;

public class eVA_AMSReqSubmitHook implements ApprovableHook, AMSE2EConstants
{
    // Ariba 8.0: Replaced deprecated method
    private static final List    NO_ERROR_RESULT          = ListUtil.list(Constants.getInteger(0));
    private static final boolean DEBUG                    = true;
    // NGR private static final String IMPORT_REQ = "Import:" ;
    // NGR private static final String IMPORT_REQ_NA = "Import(NA):" ;
    private static final String  PROCUREMENT              = "Procurement";

    private static final String  ALL_LINES_CHECK          = "AllLines";
    private static final String  GROUP_CHECK              = "GroupNonContract";
    private static final int     CHECK_ALL                = 1;
    private static final int     CHECK_NON_CONTRACT       = 2;
    private static final int     CHECK_CONTRACT           = 3;

    private static String        lsLeftBrace              = "(";
    private static String        lsRightBrace             = ")";
    private static String        lsDotSpace               = " ";
    private static BaseVector    loComments               = null;
    private static Comment       loComment                = null;
    private static boolean       lbIsExternal;
    private static BaseVector    loAttachments            = null;
    private static int           liAttachmentSize         = 0;
    public static int            liTotalAttachmentSize    = 0;
    public static boolean        liAttachmentSizeExceeded = false;
    public static boolean        lburlSupplierLocation    = false;
    public static Comment        oReplycomment            = null;
    public static boolean        lbIsReplyExternal        = false;
    public static boolean        lbSemiColon              = false;
    public static BaseVector     lobvReplyAttachments     = null;
    public static int            loReplyAttachmentSize    = 0;
    protected static String             sResourceFile = "ariba.procure.core";
    public List run(Approvable foApprovable)
    {
        List loRetVector = NO_ERROR_RESULT;
        Log.customer.debug("In the eVA_AMSReqSubmitHook.java ");

        /*
         * eVA001 eVAVal = new eVA001(); List results = eVAVal.requisitionValidations(approvable);
         * // Ariba 8.1: Replaced deprecated elementAt() method with get() method. int eVAerrorCode
         * = ((Integer)results.get(0)).intValue(); if(eVAerrorCode<0) { //in the case of error
         * return vector. return results; }
         */
        // e2e
        /*
         * Change Order functionality is invoked here If no error is encountered, e2e logic follows
         * preceded by resetting of the return error List
         */
        ChangeOrderProcessor loCOP = new ChangeOrderProcessor();
        loRetVector = loCOP.run(foApprovable);
        // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
        int errorCode = ((Integer) loRetVector.get(0)).intValue();
        if (errorCode < 0)
        {
            // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
            Log.customer.debug("Change Order logic Failed with error: " + loRetVector.get(1));
            return loRetVector;
        }
        else
        {
            loRetVector = NO_ERROR_RESULT;
        }

        loRetVector = validateReq(foApprovable);

        // Added by srini for CSPL-963 for multiple warning messages.
        /**
         *There is a limitation in ariba that we cannot return more than one warning message at a
         * time. For example If there are two warning messages need to return, system returns first
         * warning message successfully and if users hits on submit, system allows users to submit
         * the REQ successfully without showing up the second warning message. To overcome this
         * issue and after long analysis of lengthy codes came to know we cannot insert
         * checkAdHocPCO() inside validateReq() method because there are numerous conditions for
         * each warning message and each message is returning independently. So that we came to
         * decision before returning a existing warning message we are checking for any warning
         * message exist already and we are appending checkAdHocPCO() to that message if
         * checkAdHocPCO() satisfies else it will return existing message. Future warning messages
         * will work as below code. Developer just need to check his condition in else if and need
         * to append his warning message to existing warning message as like we are doing now
         */
        int liNumbering = 0;
        String lsFormattedString = null;
        String lsSpace = "    ";

        String lsSWAMFormattedString = null;
        String lsCheckSensitiveData = null;
        
		try {
			lsCheckSensitiveData = AMSReqSubmitHook.checkSensitiveDataPattern(foApprovable);
			lsSWAMFormattedString = checkSWAMTypeMatch((Requisition) foApprovable);
			Log.customer.debug("The message to print is "+lsCheckSensitiveData);
		} catch (Exception e) {
			Log.customer.debug(e.getMessage());
			e.printStackTrace();
		}
		
		
        if (((Integer) loRetVector.get(0)).intValue() < 0)
        {
            return loRetVector;
        }
        else if (checkAdHocPCO(foApprovable) || getAttachmentsSize(foApprovable)
                || (!loRetVector.isEmpty() && ((Integer) loRetVector.get(0)).intValue() == 1)||lsCheckSensitiveData != null ||  lsSWAMFormattedString!= null )
        {
            StringBuffer lsbTemp = new StringBuffer();
            // Added by Srini for CSPL-1033
            if (getAttachmentsSize(foApprovable))
            {
                liNumbering = liNumbering + 1;
                lsFormattedString = getFormattedNumberString(liNumbering);
                lsbTemp.append(lsFormattedString);
                lsbTemp.append(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core", "AttachmentBeginMessage"));
                lsbTemp.append(getAttachmentWarningMSG(foApprovable));
                lsbTemp.append(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core", "AttachmentEndMessage"));
            }

            
            //JB: START: start changes for SWAM update  CSPL-6684
        	if(lsSWAMFormattedString != null)
        	{        		
        		lsbTemp.append(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core", "SWAMMisMatchBeginMessage"));       			
       			lsbTemp.append(lsSWAMFormattedString);
       			lsbTemp.append(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core", "SWAMMisMatchEndMessage"));
        	}        	
            //JB: END: changes for SWAM update
        	
		if(lsCheckSensitiveData != null)
            {
            	liNumbering = liNumbering + 1;
            	lsFormattedString = getFormattedNumberString(liNumbering);
            	lsbTemp.append(lsFormattedString);
            	lsbTemp.append(lsCheckSensitiveData);
            }
            if (checkAdHocPCO(foApprovable))
            {
                liNumbering = liNumbering + 1;
                lsFormattedString = getFormattedNumberString(liNumbering);
                lsbTemp.append(lsFormattedString);
                lsbTemp.append(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core", "CheckAdHocPCOMessage"));
            }
            if (!loRetVector.isEmpty() && ((Integer) loRetVector.get(0)).intValue() == 1)
            {
                liNumbering = liNumbering + 1;
                lsFormattedString = getFormattedNumberString(liNumbering);
                if (lsbTemp != null)
                {
                    lsbTemp.append(lsSpace);
                }
                lsbTemp.append(lsFormattedString);
                lsbTemp.append((String) loRetVector.get(1));
            }
            loRetVector = ListUtil.list(Constants.getInteger(1), lsbTemp.toString());
        }
        else
        {
            loRetVector = NO_ERROR_RESULT;
        }

        if (DEBUG)
        {
            Log.customer.debug("Finished eVA_AMSReqSubmitHook.");
        }
        // Added by Srini for CSPL-1033
        addHeaderComment(foApprovable);

        //JB: START: update line item SWAM values CSPL-6684
        AMSReqSubmitHook.setSWAMValues((Requisition) foApprovable);
        //JB: END: update line item SWAM values

        return loRetVector;
        // e2e

    }

    public static List validateReq(Approvable foApprovable)
    {
        boolean lboolIsApprovalRequest = false;
        List loRetVector = NO_ERROR_RESULT;
        String lsUniqName = null;
        boolean lboolADVPro = false;
        boolean lboolSendEmallReqToQQ = false;
        String lsClientID = null;
        String lsClientName = null;

        boolean lboolDoSolicitCheck = false;
        String lsSolicitationBypassFld = null;
        String lsSolicitationForceFld = null;
        String lsQQForceField = null;
        String lsBypassFieldValue = null;
        String lsForceFieldValue = null;
        String lsQQValue = null;
        String lsBypassValue = null;
        String lsQQFieldValue = null;
        String lsForceValue = null;
        String lsDisplayMessage = null;

        // Validate Ad Hoc Supplier Location
        loRetVector = validateAdHocLocation(foApprovable);

        // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
        if (((Integer) loRetVector.get(0)).intValue() == -1)
        {
            return loRetVector;
        }

        // Check PCard acceptability by all Suppliers on the Requisition
        if (((Boolean) foApprovable.getFieldValue("UsePCardBool")).booleanValue())
        {
            loRetVector = checkPCardAcceptance(foApprovable);

            // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
            if (((Integer) loRetVector.get(0)).intValue() == -1)
            {
                return loRetVector;
            }

            // start rlee Check PCard expiration date and PCard still associated with Requester
            loRetVector = checkPCardExpiration(foApprovable);
            // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
            if (((Integer) loRetVector.get(0)).intValue() == -1)
            {
                return loRetVector;
            }
            // end rlee Check PCard expiration date
        }

        // Check to see if clientname ALL entry exists on the PrePrintedText table
        loRetVector = checkPrePrintedText(foApprovable);
        // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
        if (((Integer) loRetVector.get(0)).intValue() == -1)
        {
            return loRetVector;
        }

        // Get the UniqueName of this approvable
        lsUniqName = (String) foApprovable.getDottedFieldValue("UniqueName");

        // Get the ERPName from requester's Buysense Org client
        // Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the
        // ariba.common.core.User
        // Partitioned User to obtain the extrinsic BuysenseOrg field value.
        ariba.user.core.User RequesterUser = (ariba.user.core.User) foApprovable.getDottedFieldValue("Requester");
        // rgiesen dev SPL #39
        Partition appPartition = foApprovable.getPartition();
        // ariba.common.core.User RequesterPartitionUser =
        // ariba.common.core.User.getPartitionedUser(
        // RequesterUser,Base.getSession().getPartition());
        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,
                appPartition);

        lboolADVPro = ((Boolean) RequesterPartitionUser.getDottedFieldValue("ClientName.ADVPro")).booleanValue();
        lboolSendEmallReqToQQ = ((Boolean) RequesterPartitionUser.getDottedFieldValue("ClientName.SendEmallReqToQQ")).booleanValue();

        if (DEBUG)
        {
            Log.customer.debug("Got ADVPro: " + lboolADVPro);
            Log.customer.debug("Got lboolSendEmallReqToQQ: " + lboolSendEmallReqToQQ);
        }

        // Get this requester's Buysense Org client id and name
        // Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the
        // ariba.common.core.User
        // Partitioned User to obtain the extrinsic BuysenseOrg field value.
        lsClientName = (String) (RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName"));
        lsClientID = (String) (RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientID"));
        // lsClientID = (String)foApprovable.getDottedFieldValue( "Requester.ClientName.ClientID" )
        // ;
        // lsClientName = (String)foApprovable.getDottedFieldValue(
        // "Requester.ClientName.ClientName" ) ;

        // If this is not a Req for a Procurement enabled client, skip the rest of the processing

        // Ariba 8.1 Dev SPL 95 - Field change due to Integration
       if (!lboolADVPro && !lboolSendEmallReqToQQ)
        {
            if (DEBUG)
            {
                Log.customer.debug("ADVPro and SendEmallReqToQQ is not set for Procurement and Quick Quote- abandon ADVProcurement and Quick Quote processing.");
            }
            return NO_ERROR_RESULT;
        }

        if ((lsClientID == null) || (lsClientName == null))
        {
            // Ariba 8.1: Replaced deprecated method
            return ListUtil.list(Constants.getInteger(-1), "Cannot perform submit because the Requester is not "
                    + "associated with a BuysenseOrg.");
        }

        // BEGIN WA ST SPL#179: Check for approvers in approval flow
        if (((BaseVector) foApprovable.getDottedFieldValue("ApprovalRequests")).size() > 0)
        {
            lboolIsApprovalRequest = true;
        }

        String lsTransactionSource = null;
        if (foApprovable.getDottedFieldValue("TransactionSource") != null)
        {
            lsTransactionSource = (String) foApprovable.getDottedFieldValue("TransactionSource");
        }

        // Only perform the following logic if this is a req that was initiated in
        // Buysense or QuickQuote; i.e. it is not a req that is being imported from ADVANTAGE
        // via E2E or other ERP system via interfaces
        // rlee, 5/24/2004: ST SPL 1318 Order import not going to ADV
        // if req originated in buysense or Quick Quote or Interfaces

        if ((lsTransactionSource == null) || ((lsTransactionSource.trim()).equals(TXNSOURCE_QUICKQUOTE))
                || ((lsTransactionSource.trim()).equals(TXNSOURCE_INTERFACES)))
        {
            // Perform funds checking against the ERP system to see if enough funds are
            // available to allow this requistion to process
            loRetVector = checkFundBalance(foApprovable);

            // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
            if (((Integer) loRetVector.get(0)).intValue() == -1)
            {
                return loRetVector;
            }

            // check the flag that determines if we should send all or no lines to
            // procurement/solicitation
            // get the field name that is used for this check
            // rgiesen dev SPL #39
            lsSolicitationBypassFld = Base.getService().getParameter(appPartition,
                    "Application.AMSParameters.SolicitationBypassField");
            lsSolicitationForceFld = Base.getService().getParameter(appPartition,
                    "Application.AMSParameters.SolicitationForceField");
            lsQQForceField = Base.getService().getParameter(appPartition,
                    "Application.AMSParameters.QQForceField");
            Log.customer.debug("Application.AMSParameters.QQForceField" + lsQQForceField );

               
            if(lboolSendEmallReqToQQ && lsQQForceField != null && !lsQQForceField.equals(""))
            {
                lsBypassValue = Base.getService().getParameter(appPartition,
                        "Application.AMSParameters.SolicitationBypassValue");
                Log.customer.debug("Application.AMSParameters.SolicitationBypassValue" + lsBypassValue );
                lsQQValue = Base.getService().getParameter(appPartition,
                        "Application.AMSParameters.QQValue");
                Log.customer.debug("Application.AMSParameters.QQValue" + lsQQValue );
                Boolean bIsReqHeadcb5 = (Boolean) foApprovable.getFieldValue("ReqHeadCB5Value");
                Log.customer.debug("inside eVA_AMSReqSubmitHook:lboolSendEmallReqToQQ " + bIsReqHeadcb5);
                if (lsQQValue != null && !lsQQValue.equals(""))
                {
                   String lsQQDisplayMessage = Base.getService().getParameter(appPartition,
                            "Application.AMSParameters.QQDisplayForceBypassMessage");
                    Log.customer.debug("Application.AMSParameters.QQDisplayForceBypassMessage" + lsQQDisplayMessage );
                        
                        lsBypassFieldValue = (foApprovable.getDottedFieldValue(lsSolicitationBypassFld)).toString();
                        Log.customer.debug("lsBypassFieldValue" +lsBypassFieldValue );
                        lsQQFieldValue = (foApprovable.getDottedFieldValue(lsQQForceField)).toString();
                        Log.customer.debug("lsQQFieldValue" +lsQQFieldValue );
                            // if set to force, change all line items to QQ
                            if (lsQQFieldValue.equals(lsQQValue))
                            {
                                if (DEBUG)
                                {
                                    Log.customer
                                            .debug("Forcing all lines to QQ because the QQForce Field is set - except Punch-Out items");
                                }
                                // Lines cannot be forced to QQ on change Req
                                if (isChangeRequisition(foApprovable))
                                {
                                    Log.customer.debug("Lines cannot be forced to QQ on a change Req");
                                    return ListUtil.list(Constants.getInteger(-1), (Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core",
                                            "ValidationRoutingQQErrorMessage")));
                                }

                                lboolDoSolicitCheck = false;
                                loRetVector = changeAllLinesToSolicitation(foApprovable);
                                if (!lsQQDisplayMessage.equals("True"))
                                {
                                    loRetVector = NO_ERROR_RESULT;
                                }
                                return loRetVector;
                            }
                            else
                            {
                                Log.customer.debug("else of QQ field");
                                // if set to anything else, just do the regular line check
                                lboolDoSolicitCheck = true;
                            }
                        }   
                else
                {
                    // the fields are missing in the parameters table
                    // lboolDoSolicitCheck = true;
                    // Ariba 8.1: Replaced deprecated method
                    return ListUtil.list(Constants.getInteger(-1),
                            "Either the Application.AMSParameters.QQForceField or "
                                    + "Application.AMSParameters.QQForceFieldValue is not set or is blank.");
                }
             }          
            
            if (lboolADVPro && lsSolicitationBypassFld != null && !lsSolicitationBypassFld.equals("")
                    && lsSolicitationForceFld != null && !lsSolicitationForceFld.equals(""))
            {
                lsBypassValue = Base.getService().getParameter(appPartition,
                        "Application.AMSParameters.SolicitationBypassValue");
                lsForceValue = Base.getService().getParameter(appPartition,
                        "Application.AMSParameters.SolicitationForceValue");
                if (lsBypassValue != null && !lsBypassValue.equals("") && lsForceValue != null
                        && !lsForceValue.equals(""))
                {
                    lsDisplayMessage = Base.getService().getParameter(appPartition,
                            "Application.AMSParameters.SolicitationDisplayForceBypassMessage");

                    if ((lsDisplayMessage == null) || lsDisplayMessage.equals(""))
                    {
                        lsDisplayMessage = "True";
                    }

                    // get the value of the override fields on this requisition
                    lsBypassFieldValue = (foApprovable.getDottedFieldValue(lsSolicitationBypassFld)).toString();
                    lsForceFieldValue = (foApprovable.getDottedFieldValue(lsSolicitationForceFld)).toString();
                    if (lsBypassFieldValue != null && !lsBypassFieldValue.equals("") && lsForceFieldValue != null
                            && !lsForceFieldValue.equals(""))
                    {
                        if (lsBypassFieldValue.equals(lsBypassValue) && lsForceFieldValue.equals(lsForceValue))
                        {
                            // Ariba 8.1: Replaced deprecated method
                            return ListUtil.list(Constants.getInteger(-1), (Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core",
                                    "SolicitationForceNBypass")));
                        }

                        // Befor performing Solicitation check set the flag to false on all lines
                        loRetVector = resetSolicitationFlag(foApprovable);

                        if (((Integer) loRetVector.get(0)).intValue() == -1)
                        {
                            return loRetVector;
                        }

                        // if set to bypass return
                        if (lsBypassFieldValue.equals(lsBypassValue))
                        {
                            if (DEBUG)
                            {
                                Log.customer.debug("Bypassing any solicitation rules because the Bypass Field is set");
                            }
                            // verify that no line has the supplier set to solicitation vendor
                            String lsMessage = verifySupplierOnApprovable(foApprovable, CHECK_ALL);
                            if (lsMessage.length() > 0)
                            {
                                // Ariba 8.1: Replaced deprecated method
                                return ListUtil.list(Constants.getInteger(-1),
                                        "Cannot submit this requisition. Supplier is invalid on lines: " + lsMessage
                                                + ".");
                            }
                            // Continue if there are no errors
                            if (lsDisplayMessage.equals("True"))
                            {
                                // Ariba 8.1: Replaced deprecated method
                                loRetVector = ListUtil.list(Constants.getInteger(0));
                                loRetVector.set(0, Constants.getInteger(1));
                                // Ariba 8.1: Replaced deprecated addElement() method with add()
                                // method.
                                loRetVector.add(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core", "SolicitationBypassMessage"));
                            }
                            return loRetVector;
                        }
                        else
                        {
                            // if set to force, change all line items to Solicitation
                            if (lsForceFieldValue.equals(lsForceValue))
                            {
                                if (DEBUG)
                                {
                                    Log.customer
                                            .debug("Forcing all lines to Solicitation because the Force Field is set - except Punch-Out items");
                                }

                                // Lines cannot be forced to Solicitation on a change Req
                                // eProcure ST SPL #1332 - Note: This logic should never fire since
                                // the Change button is not
                                // displayed for reqs where all lines items were sent to ADV.
                                if (isChangeRequisition(foApprovable))
                                {
                                    Log.customer.debug("Lines cannot be forced to Solicitation on a change Req");
                                    return ListUtil.list(Constants.getInteger(-1), (Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core",
                                            "ValidationRoutingErrorMessage")));
                                }

                                lboolDoSolicitCheck = false;
                                loRetVector = changeAllLinesToSolicitation(foApprovable);
                                if (!lsDisplayMessage.equals("True"))
                                {
                                    loRetVector = NO_ERROR_RESULT;
                                }
                                return loRetVector;
                            }
                            else
                            {
                                // if set to anything else, just do the regular line check
                                lboolDoSolicitCheck = true;
                            }
                        }
                    } // end if override field is not null
                    else
                    {
                        // if override field is not set, use the line logic
                        lboolDoSolicitCheck = true;
                    } // end else-if override field is not null
                } // end if bypass or force values are not null
                else
                {
                    // the fields are missing in the parameters table
                    // lboolDoSolicitCheck = true;
                    // Ariba 8.1: Replaced deprecated method
                    return ListUtil.list(Constants.getInteger(-1),
                            "Either the Application.AMSParameters.SolicitationBypassValue or "
                                    + "Application.AMSParameters.SolicitationForceValue is not set or is blank.");
                } // end else-if bypass or force values are not null

            } // end if solicitation override is specified
            else
            {
                lboolDoSolicitCheck = true;
            } // end else-if solicitation override is specified

            // eProcure ST SPL #1332 - Added logic to not do the Solicitation Check if the req is a
            // changed req
            if (isChangeRequisition(foApprovable))
            {
                Log.customer.debug("This is a changed req,  so skipping solicitation check");
                lboolDoSolicitCheck = false;
            }

            if (lboolDoSolicitCheck)
            {
                Log.customer.debug("This is an original req - starting the solicitation check");
                // Check if any line items need to be solicited
                // Use the parameter to determine if we should do a line check or group check
                String lsSolicitationCheck = Base.getService().getParameter(appPartition,
                        "Application.AMSParameters.SolicitationCheck");
                if ((lsSolicitationCheck == null) || lsSolicitationCheck.equals(""))
                {
                    // this is the default
                    lsSolicitationCheck = GROUP_CHECK;
                }
                if (lsSolicitationCheck.equals(ALL_LINES_CHECK))
                {
                    loRetVector = checkLineItemsForSolicitation(foApprovable, lsClientID, lsClientName);
                }
                else
                {
                    if (lsSolicitationCheck.equals(GROUP_CHECK))
                    {
                        loRetVector = checkNonContractLineItemsForSolicitation(foApprovable, lsClientID, lsClientName);
                    }
                }
            } // end if lboolDoSolicitCheck
            List loRetVector2 = NO_ERROR_RESULT;

            /*
             * Validate that the routing flags are properly set and that for Change Req, no lines
             * will be sent for Solicitation
             */
            // IIB-New-START
            Log.customer.debug("IIB: call validateRoutingFlags()");
            // rlee check if loRetVector is null
            if (loRetVector == NO_ERROR_RESULT)
            {
                loRetVector = validateRoutingFlags(foApprovable, lsBypassFieldValue, lsForceFieldValue);
                Log.customer.debug("IIB: done with validateRoutingFlags()");
            }
            else
            {
                loRetVector2 = validateRoutingFlags(foApprovable, lsBypassFieldValue, lsForceFieldValue);
                if (loRetVector2 == NO_ERROR_RESULT)
                {
                    Log.customer.debug("IIB: done with validateRoutingFlags()");
                }
                else
                {
                    loRetVector.add(loRetVector2);
                    Log.customer.debug("IIB: done with validateRoutingFlags()");
                }

            }

            // IIB-New-END

            // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
            if (((Integer) loRetVector.get(0)).intValue() == -1)
            {
                return loRetVector;
            }
        }// if lsTransactionSource is null

        return loRetVector;
    }// validateReq

    private static List checkFundBalance(Approvable foApprovable)
    {
        // Perform the ERP fund balance validation
        // TEMP: fake it for now with a good return code
        return NO_ERROR_RESULT;
    }

    /**
     * Checks all Non Contract Lines on the requisition one by one and performs the logic to
     * determine if these lines have to become solicitations. If they do, then they are marked
     * accordingly.
     * 
     * @param foApprovable
     *            the approvable object - in this case, the requisition
     * @param fsClientId
     *            The client id
     * @param fsClientName
     *            the client name
     * @return a vector object of message
     */
    private static List checkLineItemsForSolicitation(Approvable foApprovable, String fsClientID, String fsClientName)
    {
        List loLineItems = null;
        // Ariba 8.1: Replaced deprecated method
        List loRetVector = ListUtil.list(Constants.getInteger(0));
        StringBuffer lsbSolicitMsg = new StringBuffer(64);
        StringBuffer lsbInvalidSupplierMsg = new StringBuffer(64);
        StringBuffer lsbInvalidNeedByMsg = new StringBuffer(64); // VEPI ST SPL #1394/A8 ST SPL #13
        if (DEBUG)
        {
            Log.customer.debug("Checking ReqLineItems for solicitation...");
        }

        // Perform the Solicitation Control logic:
        // For each line item, looks up a Soliciatation
        // Control table entry by commodity code and department.
        // If a record exists and the line amount is greater
        // than the threshold amount, then this line needs to be
        // solictited. Lines to be marked as needing solicitation
        // will have the existing supplier code replaced with a
        // special supplier code reserved for items needing solicitation.
        loLineItems = (List) foApprovable.getDottedFieldValue("LineItems");

        // Ariba 8.1: Replaced deprecated count() method with size() method.
        for (int i = 0; i < loLineItems.size(); i++)
        {
            ReqLineItem loReqLineItem = null;
            String lsContractNum = null;
            boolean lboolIsAdHoc = false;

            if (DEBUG)
            {
                Log.customer.debug("Processing ReqLineItem #" + (i + 1));
            }

            // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
            loReqLineItem = (ReqLineItem) loLineItems.get(i);

            //
            // Get the contract number from the Requisition line which will determine
            // whether or not to continue with the solicitation logic.
            // Only items which do not have a contract number should be considered
            // for solicitation
            //
            lsContractNum = (String) loReqLineItem.getDottedFieldValue("Description.ContractNum");
            if (lsContractNum != null && lsContractNum.trim().length() > 0)
            {
                if (DEBUG)
                {
                    Log.customer.debug("Encountered a line with contract number, so "
                            + "skip the solicitation logic...");
                }
                // Need to validate that this non-solicitation line is not using the
                // supplier setup for solicitations
                if (!verifyValidSupplier(loReqLineItem))
                {
                    // Issue an error because this line item cannot use the solicitation supplier
                    // collect all these lines and then issue one common error
                    appendLineNbrToMsg(loReqLineItem, lsbInvalidSupplierMsg);
                }
                continue; // skip to the next line item
            }

            lboolIsAdHoc = loReqLineItem.getIsAdHoc();
            // check if the line is a candidate for solicitation - use the line amount, so pass null
            if (isSolicitation(loReqLineItem, foApprovable, fsClientID, fsClientName, null))
            {
                // Check if this is a change Requisition and if it is return the
                // appropriate error abending the process
                /*
                 * eProcure ST SPL #1332 - Removed checkIfChangeRequisition() logic since it is no
                 * longer required since we are checking for changed requisitions before evaluating
                 * the req for solicitation. lvChangeReqErrors = checkIfChangeRequisition(
                 * foApprovable, loReqLineItem ) ; if ( lvChangeReqErrors != null ) { return
                 * lvChangeReqErrors ; }
                 */

                if (DEBUG)
                {
                    Log.customer.debug("ReqLineItem #" + (i + 1) + " needs to be solicited.");
                }

                // VEPI ST SPL #1394/A8 ST SPL #13: It is a solicitation line ... make sure NeedBy
                // is not null
                if (loReqLineItem.getDottedFieldValue("NeedBy") == null)
                {
                    appendLineNbrToMsg(loReqLineItem, lsbInvalidNeedByMsg);
                }
                else
                {
                    // it is a valid solicitation line
                    loReqLineItem.setFieldValue("SolicitationLine", new Boolean(true));
                    // Add to the list of line numbers for message to be displayed
                    appendLineNbrToMsg(loReqLineItem, lsbSolicitMsg);
                }
            } // end if solicitation
            else
            {
                // Need to validate that this non-solicitation line is not using the
                // supplier setup for solicitations
                if (!verifyValidSupplier(loReqLineItem))
                {
                    // Issue an error because this line item cannot use the solicitation supplier
                    // collect all these lines and then issue one common error
                    appendLineNbrToMsg(loReqLineItem, lsbInvalidSupplierMsg);
                }
            }
        } // end for all line items

        // Now that we have processed all items check if we should give any invalid supplier message
        if (lsbInvalidSupplierMsg.length() > 0)
        {
            // Ariba 8.1: Replaced deprecated method
            return ListUtil
                    .list(Constants.getInteger(-1), "Cannot submit this requisition. Supplier is invalid on lines: "
                            + lsbInvalidSupplierMsg.toString());
        }

        // Check if we should issue an invalid NeedBy date message
        if (lsbInvalidNeedByMsg.length() > 0)
        {
            return ListUtil.list(Constants.getInteger(-1),
                    "Cannot submit this requisition. Need By Date is required. Enter date on Summary screen or on each line item "
                            + "(" +lsbInvalidNeedByMsg.toString() +").");
        }

        // AKH - Check if we need to log the info regarding sending lines to Advantage
        if (checkSolicitationOrderChange(foApprovable))
        {
            lsbSolicitMsg = new StringBuffer();
        }

        if (lsbSolicitMsg.length() > 0)
        {
            StringBuffer lsbTemp = new StringBuffer(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core", "SolicitationMessagePrefix"));
            lsbSolicitMsg = lsbTemp.append(lsbSolicitMsg);

            // Ariba 8.1: Replaced deprecated method
            loRetVector.set(0, Constants.getInteger(1));
            lsbSolicitMsg.append(".\n");
            // Ariba 8.1: Replaced deprecated addElement() method with add() method.
            loRetVector.add(lsbSolicitMsg.toString() + Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core", "SolicitationMessageSuffix"));
        }

        return loRetVector;
    }

    private static void appendLineNbrToMsg(ReqLineItem foReqLineItem, StringBuffer fsbMsg)
    {
        if (fsbMsg.length() != 0)
        {
            fsbMsg.append(", ");
        }
        fsbMsg.append("#");
        fsbMsg.append(foReqLineItem.getDottedFieldValue("NumberInCollection"));
    }

    private static boolean verifyValidSupplier(ReqLineItem foReqLineItem)
    {
        // Need to validate that this non-solicitation line is not using the
        // supplier setup for solicitations
        // MSRINI::Modified to get partition from object instead from session. Partition should be
        // specified for CustomApprover
        // String lsSolicitationVendor = Base.getService().getParameter(
        // Base.getSession().getPartition(),
        // "Application.AMSParameters.SolicitationVendor") ;
        String lsSolicitationVendor = Base.getService().getParameter(foReqLineItem.getPartition(),
                "Application.AMSParameters.SolicitationVendor");
        String lsCurrentSupplier = (String) foReqLineItem.getDottedFieldValue("Supplier.UniqueName");

        if ((lsSolicitationVendor != null) && (lsSolicitationVendor.equals(lsCurrentSupplier)))
        {
            // Issue an error because this line item cannot use the solicitation supplier
            return false;
        }
        else
        {
            // Supplier is ok to use for this line item
            return true;
        }
    }

    private static String getSolicitationControlEntryKey(String fsClientID, String fsClientName, String fsCommodityCode)
    {
        // Commodity code needs to be padded to max length with ? character ...
        StringBuffer lsbCommCode = new StringBuffer(fsCommodityCode.trim());
        while (lsbCommCode.length() < FLDLENGTH_COMMODITYCODE)
            lsbCommCode.append('?');

        return (fsClientID.trim() + ":" + fsClientName.trim() + ":BuysenseControlField:" + lsbCommCode.toString());
    }

    /**
     * Checks all Non Contract Lines (as a group) on the requisition and performs the logic to
     * determine if these lines have to become solicitations. If they do, then they are marked
     * accordingly.
     * 
     * @param foApprovable
     *            the approvable object - in this case, the requisition
     * @param fsClientId
     *            The client id
     * @param fsClientName
     *            the client name
     * @return a vector object of message
     */
    private static List checkNonContractLineItemsForSolicitation(Approvable foApprovable, String fsClientID,
            String fsClientName)
    {
        List loLineItems = null;
        // Ariba 8.1: Replaced deprecated method
        List loRetVector = ListUtil.list(Constants.getInteger(0));
        StringBuffer lsbSolicitMsg = new StringBuffer(64);
        StringBuffer lsbInvalidSupplierMsg = new StringBuffer(64);
        StringBuffer lsbInvalidNeedByMsg = new StringBuffer(64); // VEPI ST SPL #1394/A8 ST SPL #13

        ReqLineItem loReqLineItem = null;
        String lsContractNum = null;
        boolean lboolIsAdHoc = false;
        String lsMessage = null;

        if (DEBUG)
        {
            Log.customer.debug("Checking ReqLineItems for solicitation using Group Non Contract logic...");
        }

        // Perform the Solicitation Control logic:
        // All non-contract line items are checked. Their combined sum is checked
        // with the commodity code of the highest-amount line against the
        // solicitation control entry to determine if ALL the non-contract items have
        // to be marked as for solicitation.
        // Lines to be marked as needing solicitation
        // will have the existing supplier code replaced with a
        // special supplier code reserved for items needing solicitation.
        loLineItems = (List) foApprovable.getDottedFieldValue("LineItems");

        // get sum of all amounts of non-contract items

        // initialize the total non contract amount
        BigDecimal loTotalNonContractAmt = new BigDecimal("0");

        // initialize the maximum of all non contract amounts
        BigDecimal loMaxAmt = new BigDecimal("0");

        // the line number containing the maximum amount
        int liMaxLine = -1;

        // vector to hold the non contract line items
        // Ariba 8.1: Replaced deprecated new List() Constructor with ListUtil.newVector();
        List lvNonContractLineItems = ListUtil.list();

        // go through all the lines and process
        // Ariba 8.1: Replaced deprecated count() method with size() method.
        for (int i = 0; i < loLineItems.size(); i++)
        {
            if (DEBUG)
            {
                Log.customer.debug("Processing ReqLineItem #" + (i + 1) + " for non-contract solicitation processing");
            }
            // get the requisition line
            // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
            loReqLineItem = (ReqLineItem) loLineItems.get(i);

            // get the contract number : if present skip the line from solicitation logic
            lsContractNum = (String) loReqLineItem.getDottedFieldValue("Description.ContractNum");
            if (lsContractNum != null && lsContractNum.trim().length() > 0)
            {
                if (DEBUG)
                {
                    Log.customer.debug("Encountered a line with contract number, so "
                            + "skip the solicitation logic...");
                }

                // Need to validate that this non-solicitation line is not using the
                // supplier setup for solicitations
                if (!verifyValidSupplier(loReqLineItem))
                {
                    // Issue an error because this line item cannot use the solicitation supplier
                    // collect all these lines and then issue one common error
                    appendLineNbrToMsg(loReqLineItem, lsbInvalidSupplierMsg);
                }
                continue;
            } // end-if contract
            else
            {
                if (DEBUG)
                {
                    Log.customer.debug("Encountered a non-contract line so " + "perform the solicitation logic...");
                }

                // If punchout item - set PunchOut to null so that IsAdHoc can be set.

                if (loReqLineItem.getDottedFieldValue("PunchOut") == null)
                {
                    Log.customer.debug("This is not a punch-out item");
                }
                else
                {
                    // rlee 907, commented the line below, setting to null too early; check
                    // threshold first
                    // loReqLineItem.setDottedFieldValue("PunchOut", null);
                    // continue;
                }

                // get the amount field
                BigDecimal loLineAmt = (BigDecimal) loReqLineItem.getDottedFieldValue("Amount.Amount");
                // add to total
                loTotalNonContractAmt = loTotalNonContractAmt.add(loLineAmt);

                // if this amount is greater than the maximum, set the maximum to this amount and
                // save the line number
                if (loLineAmt.compareTo(loMaxAmt) == 1)
                {
                    loMaxAmt = loLineAmt;
                    liMaxLine = i;
                }

                // also remember all the non contract line numbers
                // Ariba 8.1: Replaced deprecated addElement() method with add() method.
                lvNonContractLineItems.add(new Integer(i));
            } // end else-if non-contract

        } // end for all line items

        // at this point we have the sum of all non contract line item amounts,
        // a list of all non contract line numbers
        // the maximum non contract amount and the line number of the maximum amount

        // It is possible that we had only contract items and therefore there is no need for a
        // solicitation check,
        // so we skip if there were no non-contract items

        if (liMaxLine != -1)
        {
            // Now determine if the line with the max amount is over the threshold : and hence will
            // need solicitation
            // get the requisition line of the Max Amount
            // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
            loReqLineItem = (ReqLineItem) loLineItems.get(liMaxLine);

            // we need to pass the override amount - not the line amount
            if (isSolicitation(loReqLineItem, foApprovable, fsClientID, fsClientName, loTotalNonContractAmt))
            {
                // all non-contract lines will have to go as solicitations
                // Ariba 8.1: Replaced deprecated count() method with size() method.
                for (int i = 0; i < lvNonContractLineItems.size(); i++)
                {
                    // Note: loReqLineItem here is set from Max line to the i-th line
                    // i.e. its value may change from the value that was used above
                    // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                    loReqLineItem = (ReqLineItem) loLineItems.get(((Integer) lvNonContractLineItems.get(i)).intValue());

                    // Check if this is a change Requisition and if it is return the
                    // appropriate error abending the process
                    /*
                     * eProcure ST SPL #1332 - Removed checkIfChangeRequisition() logic since it is
                     * no longer required since we are checking for changed requisitions before
                     * evaluating the req for solicitation. lvChangeReqErrors =
                     * checkIfChangeRequisition( foApprovable, loReqLineItem ) ; if (
                     * lvChangeReqErrors != null ) { return lvChangeReqErrors ; }
                     */

                    if (DEBUG)
                    {
                        // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                        Log.customer.debug("Processing Non contract ReqLineItem #"
                                + ((Integer) lvNonContractLineItems.get(i)).intValue()
                                + " to convert to catalog item, mark line for solicitation.");
                    }

                    lboolIsAdHoc = loReqLineItem.getIsAdHoc();

                    // VEPI ST SPL #1394/A8 ST SPL #13: It is a solicitation line ... make sure
                    // NeedBy is not null
                    if (loReqLineItem.getDottedFieldValue("NeedBy") == null)
                    {
                        appendLineNbrToMsg(loReqLineItem, lsbInvalidNeedByMsg);
                    }
                    else
                    {
                        // it is a valid solicitation line
                        loReqLineItem.setFieldValue("SolicitationLine", new Boolean(true));
                        // Add to the list of line numbers for message to be displayed
                        appendLineNbrToMsg(loReqLineItem, lsbSolicitMsg);
                    }
                } // end for all non-contract items
            } // end if solicitation
            else
            {
                // since none of the non-contract lines are going to solicitation verify that
                // supplier is valid on all non-contract items
                lsMessage = verifySupplierOnApprovable(foApprovable, CHECK_NON_CONTRACT);
                // concatenate it to the list of lines from before
                if (lsMessage.length() > 0)
                {
                    // Create a message to be displayed to the user
                    if (lsbInvalidSupplierMsg.length() > 0)
                    {
                        lsbInvalidSupplierMsg.append(", ");
                    }
                    lsbInvalidSupplierMsg.append(lsMessage);
                }
            } // end else-if solicitation
        } // endif only contract items - and hence no need to do solicitation check

        // Now that we have processed all items check if we should give any invalid supplier message
        if (lsbInvalidSupplierMsg.length() > 0)
        {
            // Ariba 8.1: Replaced deprecated method
            return ListUtil.list(Constants.getInteger(-1),
                    "Cannot submit this requisition. Supplier is invalid on lines: " + lsbInvalidSupplierMsg.toString()
                            + ".");
        }

        // Check if we should issue an invalid NeedBy date message
        if (lsbInvalidNeedByMsg.length() > 0)
        {
            return ListUtil.list(Constants.getInteger(-1),
                    "Cannot submit this requisition. Need By Date is required. Enter date on Summary screen or on each line item "
                            + "(" +lsbInvalidNeedByMsg.toString() +").");
        }

        // AKH - Check if we need to log the info regarding sending lines to Advantage
        if (checkSolicitationOrderChange(foApprovable))
        {
            lsbSolicitMsg = new StringBuffer();
        }

        if (lsbSolicitMsg.length() > 0)
        {
            StringBuffer lsbTemp = new StringBuffer(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core", "SolicitationMessagePrefix"));
            lsbSolicitMsg = lsbTemp.append(lsbSolicitMsg);

            // Ariba 8.1: Replaced deprecated method
            loRetVector.set(0, Constants.getInteger(1));
            lsbSolicitMsg.append(".\n");
            // Ariba 8.1: Replaced deprecated addElement() method with add() method.
            loRetVector.add(lsbSolicitMsg.toString() + Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core", "SolicitationMessageSuffix"));
        }

        return loRetVector;
    }

    /**
     * For a given line on an approvable (requisition), this method will perform the logic to
     * determine if the line(s) are candidates for solicitations. The amount on this line is checked
     * against a threshold amount on the Solicitation Control Entry If the amount on this line is
     * greater or equal to the threshold, then the line/and possibly other lines (depending upon
     * other parameters) are candidates for solicitation
     * 
     * @param foLine
     *            the line in the requisition that is being checked
     * @param foApprovable
     *            the approvable object - in this case, the requisition
     * @param fsClientId
     *            The client id
     * @param fsClientName
     *            the client name
     * @param foOverrideAmt
     *            NULL if you want to use the Line amount, else pass a BigDecimal
     * @return a boolean true - if solicitation control logic is satisfied, else false
     */
    private static boolean isSolicitation(ReqLineItem foLine, Approvable foApprovable, String fsClientID,
            String fsClientName, BigDecimal foOverrideAmt)
    {

        String lsCommodityCode = null;
        boolean lboolRetVal = false;
        BigDecimal loThresholdAmt = null;
        BigDecimal loLineAmt = null;

        //
        // Get the commodity code and dept. code so we can look up
        // on the solicitation control table
        // the order in which the checks are done:
        //
        if ((foLine.getIsAdHoc()) == true)
        {
            // This is a non-catalog item, commodity code is directly on line item
            lsCommodityCode = (String) foLine.getDottedFieldValue("CommodityCode.UniqueName");
        }
        else
        {
            // This is a catalog item, get commodity code from catalog description
            lsCommodityCode = (String) foLine.getDottedFieldValue("Description.CommonCommodityCode.UniqueName");
        }

        // lsDeptCode = (String)foApprovable.getDottedFieldValue( "Requester.Department.Name" ) ;

        // for this commodity code and clientid / client name get the threshold amount
        // and verify the line amount against this threshold amount
        if ((loThresholdAmt = getThresholdAmount(foApprovable, fsClientID, fsClientName, lsCommodityCode)) != null)
        {
            if (DEBUG)
            {
                Log.customer.debug("Threshold Amount is " + loThresholdAmt);
            }

            // When grouping non-contract items we do not want to use the line amount, instead we
            // should use a
            // group amount which is passed to this method.
            if (foOverrideAmt == null)
            {
                loLineAmt = (BigDecimal) foLine.getDottedFieldValue("Amount.Amount");
            }
            else
            {
                loLineAmt = foOverrideAmt;
            }
            // Now check the threshold amount against the current line item
            // If the line item amount is greater or equal to threshold,
            // this line needs to be solicited
            // parameters elsewhere will determine if this will result in other lines from going to
            // solicitation as well
            if (loLineAmt.compareTo(loThresholdAmt) != -1)
            {
                if (DEBUG)
                {
                    Log.customer.debug("ReqLineItem with commodity " + lsCommodityCode + " and amount "
                            + loLineAmt.toString() + " needs to be solicited.");
                }
                // rlee 907 added if else below
                if (foLine.getDottedFieldValue("PunchOut") == null)
                {
                    Log.customer.debug("This is not a punch-out item");
                }
                else
                {
                    // rlee 907 This is a punchout item and above threshold, set punchout to null
                    // so that IsAdHoc can be set.
                    foLine.setDottedFieldValue("PunchOut", null);

                }
                lboolRetVal = true;
            }
        }
        else
        {
            if (DEBUG)
            {
                Log.customer.debug(" No Threshold Amount found");
            }
            lboolRetVal = false;
        }
        return lboolRetVal;
    }

    /**
     * Changes all lines to Solicitation lines
     * 
     * @param foApprovable
     *            the approvable object - in this case, the requisition
     * @return List message
     */
    private static List changeAllLinesToSolicitation(Approvable foApprovable)
    {
        StringBuffer lsbInvalidNeedByMsg = new StringBuffer(64); // VEPI ST SPL #1394/A8 ST SPL #13
        // Ariba 8.1: Replaced deprecated method
        List loRetVector = ListUtil.list(Constants.getInteger(0));
        // get the line items
        List loLineItems = (List) foApprovable.getDottedFieldValue("LineItems");

        // for all line items, set the solicitation
        // Ariba 8.1: Replaced deprecated count() method with size() method.
        for (int i = 0; i < loLineItems.size(); i++)
        {
            ReqLineItem loReqLineItem = null;
            boolean lboolIsAdHoc = false;

            // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
            loReqLineItem = (ReqLineItem) loLineItems.get(i);

            // Check if this is a change Requisition and if it is return the
            // appropriate error abending the process
            /*
             * eProcure ST SPL #1332 - Note: This logic is unneccessary since the Change button is
             * not displayed for reqs where all lines items were sent to ADV. lvChangeReqErrors =
             * checkIfChangeRequisition( foApprovable, loReqLineItem ) ; if ( lvChangeReqErrors !=
             * null ) { return lvChangeReqErrors ; }
             */

            if (DEBUG)
            {
                Log.customer.debug("Changing ReqLineItem #" + (i + 1) + " to solicitation");
            }

            lboolIsAdHoc = loReqLineItem.getIsAdHoc();

            // VEPI ST SPL #1394/A8 ST SPL #13: Make sure NeedBy is not null
            if (loReqLineItem.getDottedFieldValue("NeedBy") == null)
            {
                appendLineNbrToMsg(loReqLineItem, lsbInvalidNeedByMsg);
            }
            
            
            else
            {

                // it is a valid solicitation line
                loReqLineItem.setFieldValue("SolicitationLine", new Boolean(true));
            }
        } // end for all items

        // Check if we should issue an invalid NeedBy date message
        if (lsbInvalidNeedByMsg.length() > 0)
        {
            return ListUtil.list(Constants.getInteger(-1),
                    "Cannot submit this requisition. Need By Date is required. Enter date on Summary screen or on each line item "
                            + "(" +lsbInvalidNeedByMsg.toString() +").");
        }
        else
        {
            Boolean bIsReqHeadcb5value = (Boolean) foApprovable.getFieldValue("ReqHeadCB5Value");
            if(bIsReqHeadcb5value!=null && bIsReqHeadcb5value.booleanValue())
            {
            // Ariba 8.1: Replaced deprecated method
            loRetVector.set(0, Constants.getInteger(1));
            // Ariba 8.1: Replaced deprecated addElement() method with add() method.
            loRetVector.add(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core", "QQForceMessage"));
            return (loRetVector);
            }
            else
            {
                // Ariba 8.1: Replaced deprecated method
                loRetVector.set(0, Constants.getInteger(1));
                // Ariba 8.1: Replaced deprecated addElement() method with add() method.
                loRetVector.add(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core", "SolicitationForceMessage"));
                return loRetVector;
            }
        }
    }

    /**
     * For a given clientid, clientname, and commodity code this method gets the threshold amount
     * Since we allow wildcard characters in the Solicitation Control entries, the logic performs
     * the checks in this order: --- get the threshold amount from a control entry that has the
     * exact Department and Commodity Code as the line/requisition --- get the threshold amount from
     * a control entry that has the same Department as on the line/requisition i and the wild card
     * Commodity Code (for "all commodities") --- get the threshold amount from a control entry that
     * has the wild card Department code (for "all departments") and same Commodity Code as on the
     * line/requisition --- get the threshold amount from a control entry that has the wild card
     * Department code (for "all departments") and the wild card Commodity Code (for
     * "all commodities")
     * 
     * @param foApprovable
     *            The approvable - in this case, the requisition
     * @param fsClientId
     *            The client id
     * @param fsClientName
     *            the client name
     * @param fsCommodityCode
     *            the commodity code
     * @return BigDecimal - the threshold amount, NULL if there is no matching solicitation control
     *         entry
     */
    private static BigDecimal getThresholdAmount(Approvable foApprovable, String fsClientID, String fsClientName,
            String fsCommodityCode)
    {
        BigDecimal loThresholdAmt = null;
        String lsSolicitCtrlKey = null;
        ClusterRoot loSolicitCtrlEntry = null;

        // Step 1 : Get the entry for specific Commodity Code and Client Name
        lsSolicitCtrlKey = getSolicitationControlEntryKey(fsClientID, fsClientName, fsCommodityCode);

        if (DEBUG)
        {
            Log.customer.debug("Performing lookup on solicitation control object for key: " + lsSolicitCtrlKey);
        }

        loSolicitCtrlEntry = Base.getService().objectMatchingUniqueName("ariba.core.BuysenseFieldDataTable",
                foApprovable.getPartition(), lsSolicitCtrlKey);

        // Step 2:
        // If solicitation control entry is not found, try another lookup with a wildcard in the
        // Commodity field of the key. This is to match more generic solicitation control entries
        //
        if (loSolicitCtrlEntry == null)
        {
            lsSolicitCtrlKey = getSolicitationControlEntryKey(fsClientID, fsClientName, STRING_ALLCOMMODITIES);

            if (DEBUG)
            {
                Log.customer.debug("Performing commodity wildcard lookup on solicitation control object for key: "
                        + lsSolicitCtrlKey);
            }

            loSolicitCtrlEntry = Base.getService().objectMatchingUniqueName("ariba.core.BuysenseFieldDataTable",
                    foApprovable.getPartition(), lsSolicitCtrlKey);
            // Step 3:
            // If Solicitation Control Key is not found, try another lookup with a wildcard in the
            // Client Name field

            if (loSolicitCtrlEntry == null)
            {
                lsSolicitCtrlKey = getSolicitationControlEntryKey(fsClientID, STRING_ALLCLIENTNAMES, fsCommodityCode);

                if (DEBUG)
                {
                    Log.customer.debug("Performing department wildcard lookup on solicitation control object for key: "
                            + lsSolicitCtrlKey);
                }

                loSolicitCtrlEntry = Base.getService().objectMatchingUniqueName("ariba.core.BuysenseFieldDataTable",
                        foApprovable.getPartition(), lsSolicitCtrlKey);
                // Step 4:
                // If Solicitation Control Key is not found, try with wildcard in both Client Name
                // and commodity
                if (loSolicitCtrlEntry == null)
                {
                    lsSolicitCtrlKey = getSolicitationControlEntryKey(fsClientID, STRING_ALLCLIENTNAMES,
                            STRING_ALLCOMMODITIES);

                    if (DEBUG)
                    {
                        Log.customer.debug("Performing dual wildcard lookup on solicitation control object for key: "
                                + lsSolicitCtrlKey);
                    }

                    loSolicitCtrlEntry = Base.getService().objectMatchingUniqueName(
                            "ariba.core.BuysenseFieldDataTable", foApprovable.getPartition(), lsSolicitCtrlKey);
                } // end if Step 3 yielded null
            } // end if Step 2 yielded null
        } // end if Step 1 yielded null

        if (loSolicitCtrlEntry != null)
        {
            if (DEBUG)
            {
                Log.customer.debug("Solicitation Control Entry found...");
            }

            loThresholdAmt = new BigDecimal(((String) loSolicitCtrlEntry.getDottedFieldValue("Description")).trim());
        }
        else
        {
            if (DEBUG)
            {
                Log.customer.debug("Solicitation Control Entry NOT found...");
            }
        }
        return loThresholdAmt;
    }

    /**
     * Verifies that the supplier on all the lines is valid
     * 
     * @param foApprovable
     *            the approvable object - in this case, the requisition
     * @param int specifies what lines have to be checked set to CHECK_NON_CONTRACT if only
     *        non-contract lines have to be checked set to CHECK_CONTRACT if only contract lines
     *        have to be checked set to CHECK_ALL if all lines have to be checked
     * @return String comma separated list of lines that have invalid supplier, else empty string
     */
    private static String verifySupplierOnApprovable(Approvable foApprovable, int fiWhatToCheck)
    {
        List loLineItems = (List) foApprovable.getDottedFieldValue("LineItems");
        StringBuffer lsbMessage = new StringBuffer(64);

        // Ariba 8.1: Replaced deprecated count() method with size() method.
        for (int i = 0; i < loLineItems.size(); i++)
        {
            ReqLineItem loReqLineItem = null;
            String lsContractNum = null;

            if (DEBUG)
            {
                Log.customer.debug("Verifying supplier on ReqLineItem #" + (i + 1));
            }

            // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
            loReqLineItem = (ReqLineItem) loLineItems.get(i);

            //
            // Get the contract number from the Requisition line
            lsContractNum = (String) loReqLineItem.getDottedFieldValue("Description.ContractNum");

            if (lsContractNum != null && lsContractNum.trim().length() > 0)
            {
                // contract line
                if ((fiWhatToCheck != CHECK_ALL) && (fiWhatToCheck != CHECK_CONTRACT))
                {
                    continue;
                }
            } // end if contract
            else
            {
                // non-contract line
                if ((fiWhatToCheck != CHECK_ALL) && (fiWhatToCheck != CHECK_NON_CONTRACT))
                {
                    continue;
                }
            } // end else if contract
            // Validate supplier
            if (!verifyValidSupplier(loReqLineItem))
            {
                if (DEBUG)
                {
                    Log.customer.debug("Supplier on ReqLineItem #" + (i + 1) + " is not valid");
                }
                // Create a message to be displayed to the user
                if (lsbMessage.length() != 0)
                {
                    lsbMessage.append(", ");
                }
                lsbMessage.append("#");
                lsbMessage.append(loReqLineItem.getDottedFieldValue("NumberInCollection"));
            } // end if supplier is not valid

        } // end for all line items

        return (lsbMessage.toString());
    }

    /**
     * eProcure ST SPL #1332 - Added new method Checks whether Change Requisition is being dealt
     * with and if it is, returns true
     * 
     * @param foReqLineItem
     *            the ReqLineItem object - in this case, the specific line item of the Requisition
     * @return boolean - true is returned if there is a previous req version
     */
    private static boolean isChangeRequisition(Approvable foApprovable)
    {
        boolean lsChangeReq = false;
        ClusterRoot loCluster = foApprovable.getPreviousVersion();
        if (loCluster != null)
        {
            String lsStatus = (String) loCluster.getFieldValue("StatusString");
            if (!lsStatus.equals("Composing"))
            {
                lsChangeReq = true;
            }
        }
        return lsChangeReq;
    }

    /**
     * Checks whether Change Requisition is being dealt with and if it is, returns the appropriate
     * error
     * 
     * @param foApprovable
     *            the approvable object - in this case, the requisition
     * @param foReqLineItem
     *            the ReqLineItem object - in this case, the specific line item of the Requisition
     * @return List containg the appropriate Error, or null if none are encountered
     */
    private static List checkIfChangeRequisition(Approvable foApprovable, ReqLineItem foReqLineItem)
    {

        // Ariba 8.1: Replaced deprecated new List() Constructor with ListUtil.newVector();
        List lvErrors = ListUtil.list();
        BaseId oldReqLineItemBaseId = foReqLineItem.getOldValues();
        ReqLineItem oldReqLineItem = null;

        if (oldReqLineItemBaseId != null)
        {
            oldReqLineItem = (ReqLineItem) foReqLineItem.getOldLineItemValues();

            if (oldReqLineItem != null && verifyValidSupplier(oldReqLineItem))
            {
                // Supplier on old req line was NOT a Solicitation supplier, so
                // issue an error

                // Ariba 8.1: Replaced deprecated method
                return ListUtil.list(Constants.getInteger(-1),
                        "Lines cannot be added or changed on an eProcurement order. "
                                + "Please contact your procurement officer for any changes ");
            }
            else
            {
                return null;
            }
        }
        else if (isChangeRequisition(foApprovable))
        {
            // Ariba 8.1: Replaced deprecated method
            return ListUtil.list(Constants.getInteger(-1), "New lines cannot be sent to eProcurement. "
                    + "Please submit any new lines on a separate Requisition");
        }
        else
        {
            return null;
        }
    }

    // The function determines if the prior requisition had Solicitation line/s and if there are
    // same number of
    // Solicitation lines in the current requisition, then no change is envisaged for Solicitation
    // order
    // in the current requisition. This is based on assumption that Solicitation line is not
    // editable, which
    // has been implemented.
    private static boolean checkSolicitationOrderChange(Approvable foApprovable)
    {
        List loLineItems = null;
        ReqLineItem loReqLineItem = null;
        int liOldSolic = 0;
        int liNewSolic = 0;

        if (isChangeRequisition(foApprovable))
        {
            // Find Solicitation lines in prior requisition
            Requisition oldReq = (Requisition) foApprovable.getPreviousVersion();
            loLineItems = (List) oldReq.getDottedFieldValue("LineItems");

            // Ariba 8.1: Replaced deprecated count() method with size() method.
            for (int i = 0; i < loLineItems.size(); i++)
            {
                if (DEBUG)
                    Log.customer.debug("AKH - Processing Old ReqLineItem #" + (i + 1));

                // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                loReqLineItem = (ReqLineItem) loLineItems.get(i);

                if (!verifyValidSupplier(loReqLineItem))
                    liOldSolic++;

            } // end - for

            if (liOldSolic > 0)
            {
                // Find Solicitation lines in current requisition
                loLineItems = null;
                loReqLineItem = null;

                loLineItems = (List) foApprovable.getDottedFieldValue("LineItems");

                // Ariba 8.1: Replaced deprecated count() method with size() method.
                for (int i = 0; i < loLineItems.size(); i++)
                {
                    if (DEBUG)
                        Log.customer.debug("AKH - Processing Current ReqLineItem #" + (i + 1));

                    // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                    loReqLineItem = (ReqLineItem) loLineItems.get(i);

                    if (!verifyValidSupplier(loReqLineItem))
                        liNewSolic++;

                } // end - for

                if (liOldSolic == liNewSolic)
                    return true;

            } // end - if ( liOldSolic > 0 )

        } // end - if ( isChangeRequisition(foApprovable) )

        return false;
    } // end checkSolicitationOrderChange

    private static List validateAdHocLocation(Approvable foApprovable)
    {
        BaseVector ReqItems = (BaseVector) foApprovable.getFieldValue("LineItems");
        SupplierLocation location = null;
        String invalidFields = null;
        BuysenseSupplierLocationFieldValidation locFieldsValidate = new BuysenseSupplierLocationFieldValidation();
        // Ariba 8.1: Replaced deprecated count() method with size() method.
        for (int i = 0; i < (ReqItems.size()); i++)
        {
            // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
            ReqLineItem lineItem = (ReqLineItem) ReqItems.get(i);

            /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
            // if (!StringUtil.nullOrEmptyOrBlankString((String)lineItem.getDottedFieldValue(
            // "SupplierLocation.UniqueName")))
            if (lineItem.getSupplierLocation().getCreator() == null)
                continue;

            location = (SupplierLocation) lineItem.getFieldValue("SupplierLocation");
            if (location == null)
                // Ariba 8.1: Replaced deprecated method
                return ListUtil.list(Constants.getInteger(-1), "Null SupplierLocation for line :" + (i + 1));

            //SRINI::START::To Skip duplicate TIN validation in submithook
            TransientData td = Base.getSession().getTransientData();
            String lsTIN = (String) location.getFieldValue("TIN");
            td.put("TIN", lsTIN);
            //SRINI::END::To Skip duplicate TIN validation in submithook
            
            invalidFields = locFieldsValidate.validateLocation(location);
            Log.customer.debug("SL Validations - invalidFields : " + invalidFields);

            if (invalidFields != null)
                // Ariba 8.1: Replaced deprecated method
                return ListUtil.list(Constants.getInteger(-1), "Invalid Fields : " + invalidFields
                        + " in SupplierLocation for line :" + (i + 1));
        }

        return NO_ERROR_RESULT;
    }

    private static List resetSolicitationFlag(Approvable foApprovable)
    {
        try
        {
            BaseVector ReqItems = (BaseVector) foApprovable.getFieldValue("LineItems");

            for (int i = 0; i < (ReqItems.size()); i++)
            {
                ReqLineItem lineItem = (ReqLineItem) ReqItems.get(i);
                lineItem.setFieldValue("SolicitationLine", new Boolean(false));
            }
        }
        catch (Exception loEx)
        {
            return ListUtil.list(Constants.getInteger(-1), "Error setting solicitaion flag: " + loEx.getMessage());

        }
        return NO_ERROR_RESULT;
    }

    private static List checkPCardAcceptance(Approvable foApprovable)
    {
        String lsLines = null;
        BaseVector ReqItems = (BaseVector) foApprovable.getFieldValue("LineItems");
        // Ariba 8.1: Replaced deprecated count() method with size() method.
        for (int i = 0; i < (ReqItems.size()); i++)
        {
            // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
            ReqLineItem lineItem = (ReqLineItem) ReqItems.get(i);

            if (((Integer) lineItem.getDottedFieldValue("SupplierLocation.PCardAcceptanceLevel")).intValue() == 0)
            {
                if (lsLines == null)
                    lsLines = "" + (i + 1);
                else
                    lsLines = lsLines + ", " + (i + 1);
            }
        }

        if (lsLines == null)
            return NO_ERROR_RESULT;
        else
        {
            StringBuffer loPrefix = new StringBuffer().append("Line");
            /* Ariba 8.1: Replaced Util.contains */
            if (StringUtil.contains(lsLines, ","))
                loPrefix.append("s");
            // Ariba 8.1: Replaced deprecated method
            return ListUtil.list(Constants.getInteger(-1), loPrefix.toString() + ": " + lsLines + " -- "
                    + (Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core", "PCardAcceptability")));
        }
    }

    /*
     * rlee: checkPCardExpiration checks if PCard being used has expired or disassociated with
     * Requester. Return error message if PCard used is null, else check if PCard used has expired.
     * If PCard has not expired, check if PCard used is still associated with the Requester; This is
     * done by a for loop checking each of the Requester's PCards to see if there is a match with
     * the PCard used. If at least one PCard will match, then set lbDeleted flag to FALSE; if there
     * is no PCard match, the lbDeleted flag stays TRUE and return an error message.
     */
    
    /* paluri, CSPL-2433, 12-Jan-2011. References of CardNumber filed is changed to UniqueName, as CardNumber is Encrypted in 9r1,
     * causing class cast exception.
     */    
    public static List checkPCardExpiration(Approvable foApprovable)
    {
        Date ldPCardExpDate;
        int liCheckExpired = 0;
        int liPCardSize = 0;
        boolean lbDeleted = true;
        String lsPCardNum = null;
        PCard loPCardCheck = null;

        PCard loUsingPCard = (PCard) foApprovable.getDottedFieldValue("PCardToUse");
        User loRequester = (User) foApprovable.getFieldValue("Requester");
        List lvRequesterCards = ariba.common.core.User.getPartitionedUser(loRequester, foApprovable.getPartition())
                .getPCardsVector();
        liPCardSize = lvRequesterCards.size();

        if ((loUsingPCard == null))
        {
            // Ariba 8.1: Replaced deprecated method
            return ListUtil.list(Constants.getInteger(-1), (Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core",
                    "RequesterMakeAnotherSelection")));
        }
        else
        {
            lsPCardNum = (String) loUsingPCard.getFieldValue("UniqueName");
            ldPCardExpDate = loUsingPCard.getExpirationDate();
            if (ldPCardExpDate != null)
            {
                liCheckExpired = Date.getNow().compareTo(ldPCardExpDate);
                if (liCheckExpired > 0)
                {
                		// Ariba 8.1: Replaced deprecated method
                        return ListUtil.list(Constants.getInteger(-1), (Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core",
                                "RequesterMakeAnotherSelection")));                   
                }
            }
            if (liPCardSize > 0)
            {
                for (int i = 0; i < liPCardSize; i++)
                {
                    loPCardCheck = (PCard) lvRequesterCards.get(i);
                    //SRINI: Added null check for PCard unique name  
                    if (lsPCardNum != null && loPCardCheck.getUniqueName() != null && lsPCardNum.equals((String) loPCardCheck.getFieldValue("UniqueName")))
                    {
                        Log.customer.debug("rlee found a PCard match = " + lsPCardNum);
                        // Sarath: CSPL-1109 -- Added condition to check the PCard being selected in
                        // Req is active or not.
                        if (loPCardCheck.getActive())
                        {
                            lbDeleted = false;
                        }
                    }
                }
            }
           /* if (lbDeleted)
            {
                Log.customer.debug("rlee Pcard is not associated with User ");
                return ListUtil.list(Constants.getInteger(-1), (Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core",
                        "RequesterMakeAnotherSelection")));
            }*/
        }
        return NO_ERROR_RESULT;
    }

    // This method calls the processReqPrePrintedText method in teh SetPrePrintedText class. This
    // will checks to see if the
    // clientname = 'ALL" entry exists on the PrePrintedText table. If it doesn't, issue an error.
    // This check is needed as
    // we don't want a requisition to be processed if the clientname = 'ALL' entry is non-existent.
    // The clientname = 'ALL'
    // PrePrintedText entry guarantees orders with Terms and Conditions. We can't have orders
    // without Terms and Conditions.

    public static List checkPrePrintedText(Approvable foApprovable)
    {
        BaseObject loObject = (BaseObject) foApprovable;

        loObject = SetPrePrintedText.processReqPrePrintedText(foApprovable);
        if (loObject == null)
        {
            // Ariba 8.1: Replaced deprecated method
            return ListUtil
                    .list(
                            Constants.getInteger(-1),
                            "!System Error:  Unable to submit; terms and conditions missing.  Please save changes and contact your system administrator immediately for assistance");
        }
        else
        {
            return NO_ERROR_RESULT;
        }
    }

    /**
     * Checks whether Change Requisition is being dealt with and whether it contains line items
     * flagged for solicitation. If both are true the appropriate error is issued
     * 
     * @param foApprovable
     *            the approvable object - in this case, the requisition
     * @return List containg the appropriate Error, or null if none are encountered
     */
    private static List validateRoutingFlags(Approvable foApprovable, String fsBypassFieldValue,
            String fsForceFieldValue)
    {
        Log.customer.debug("In validateRoutingFlags(): %s,%s,%s", foApprovable, fsBypassFieldValue, fsForceFieldValue);

        if (isChangeRequisition(foApprovable) && !noLinesFlaggedForSolicitation(foApprovable))
        {
            setRoutingFlags(foApprovable, true, false);

            /*
             * eProcure ST SPL #1332 - removed error restricting changes to reqs where some lines
             * were sent for Solicitation. And now returning the no error vector. return
             * ListUtil.vector( Constants.getInteger( -1 ), ( Fmt.Sil( "ariba.procure.core",
             * "ValidationRoutingErrorMessage" ) ) );
             */
            return NO_ERROR_RESULT;
        }
        else if (fsBypassFieldValue == null && fsForceFieldValue == null)
        {
            return ListUtil.list(Constants.getInteger(-1), (Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core",
                    "BothRoutingFlagsAreNullErrorMessage")));
        }
        else if ((new Boolean(fsBypassFieldValue).booleanValue() == true && (new Boolean(fsForceFieldValue)
                .booleanValue() == true)))
        {
            return ListUtil.list(Constants.getInteger(-1), (Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core",
                    "BothRoutingFlagsAreTrueErrorMessage")));
        }
        else
        {
            return NO_ERROR_RESULT;
        }
    }

    /**
     * Checks whether Change Requisition is being dealt with and if it is, returns the appropriate
     * error
     * 
     * @param foApprovable
     *            the approvable object - in this case, the requisition
     * @param fbSolicitationBypassField
     *            - a boolean value to be set to the SolicitationBypassField
     * @param fbSolicitationForceField
     *            - a boolean value to be set to the SolicitationForceField
     */
    private static void setRoutingFlags(Approvable foApprovable, boolean fbSolicitationBypassField,
            boolean fbSolicitationForceField)
    {
        Log.customer.debug("In setRoutingFlags()");

        String lsSolicitationBypassFld = null;
        String lsSolicitationForceFld = null;

        if (foApprovable instanceof ariba.purchasing.core.Requisition)
        {
            // check the flag that determines if we should send all or no lines to
            // procurement/solicitation
            // get the field name that is used for this check
            // MSRINI::Modified to get partition from object instead from session. Partition should
            // be specified for CustomApprover
            // lsSolicitationBypassFld = Base.getService().getParameter(
            // Base.getSession().getPartition(),
            // "Application.AMSParameters.SolicitationBypassField") ;
            lsSolicitationBypassFld = Base.getService().getParameter(foApprovable.getPartition(),
                    "Application.AMSParameters.SolicitationBypassField");
            // MSRINI::Modified to get partition from object instead from session. Partition should
            // be specified for CustomApprover
            // lsSolicitationForceFld = Base.getService().getParameter(
            // Base.getSession().getPartition(),
            // "Application.AMSParameters.SolicitationForceField") ;
            lsSolicitationForceFld = Base.getService().getParameter(foApprovable.getPartition(),
                    "Application.AMSParameters.SolicitationForceField");

            if (lsSolicitationBypassFld != null && !lsSolicitationBypassFld.equals("")
                    && lsSolicitationForceFld != null && !lsSolicitationForceFld.equals(""))
            {
                foApprovable.setFieldValue(lsSolicitationBypassFld, new Boolean(fbSolicitationBypassField));
                foApprovable.setFieldValue(lsSolicitationForceFld, new Boolean(fbSolicitationForceField));
            }
        }
    }

    /**
     * Checks whether the Requisition contains only lines which have been flagged for solicitation
     * and if it does, returns true
     * 
     * @param foApprovable
     *            the approvable object - in this case, the requisition
     * @return boolean - true if all lines will be sent for solicitation - false if at least one
     *         line will not be sent for solicitation
     */
    private static boolean allLinesFlaggedForSolicitation(Approvable foApprovable)
    {
        LineItem loLineItem = null;
        BaseVector lvLineItemCollection = null;

        lvLineItemCollection = ((Requisition) foApprovable).getLineItems();

        for (int i = 0; i < lvLineItemCollection.size(); i++)
        {
            loLineItem = (LineItem) lvLineItemCollection.get(i);

            if (((Boolean) loLineItem.getFieldValue("SolicitationLine")).booleanValue() == false)
            {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks whether the Requisition contains only lines which have not been flagged for
     * solicitation and if it does, returns true
     * 
     * @param foApprovable
     *            the approvable object - in this case, the requisition
     * @return boolean - true if no lines will be sent for solicitation - false if at least one line
     *         will be sent for solicitation
     */
    private static boolean noLinesFlaggedForSolicitation(Approvable foApprovable)
    {
        Log.customer.debug("In noLinesFlaggedForSolicitation()");

        LineItem loLineItem = null;
        BaseVector lvLineItemCollection = null;

        lvLineItemCollection = ((Requisition) foApprovable).getLineItems();

        for (int i = 0; i < lvLineItemCollection.size(); i++)
        {
            loLineItem = (LineItem) lvLineItemCollection.get(i);

            if (((Boolean) loLineItem.getFieldValue("SolicitationLine")).booleanValue() == true)
            {
                Log.customer.debug("About to return FALSE");
                return false;
            }
        }
        Log.customer.debug("About to return TRUE");
        return true;
    }

    /**
     * Checks whether the Requisition contains both solicitation and non-solicitation line items and
     * if it does, returns true. For mixed lines, both allLinesFlaggedForSolicitation and
     * noLinesFlaggedForSolicitation will return false.
     * 
     * @param foApprovable
     *            the approvable object - in this case, the requisition
     * @return boolean - true if the Requisition contains both solicitation and non-solicitation
     *         lines - false if at least one line will be sent for solicitation
     */
    private static boolean mixedSolicitationNonSolicitationRequisition(Approvable foApprovable)
    {
        if (allLinesFlaggedForSolicitation(foApprovable) == false
                && noLinesFlaggedForSolicitation(foApprovable) == false)
        {
            return true;
        }
        else
            return false;
    }

    /**
     * CSPL-785 (Srini) 11/13/08
     * 
     * @param appr
     * @This will return a warning message if SupplierLocation.AdapterSource is null and
     *       UsePCardBool is true
     */

    /**
     * CSPL-963 (Srini) Moved this method from AMSReqSubmitHook.java to this java file.
     */

    public static boolean checkAdHocPCO(Approvable appr)
    {
        BaseVector vLineItems = (BaseVector) appr.getFieldValue("LineItems");
        ReqLineItem oItem = null;
        for (int i = 0; i < vLineItems.size(); i++)
        {
            oItem = (ReqLineItem) vLineItems.get(i);
            String sAdapterSource = (String) oItem.getDottedFieldValue("SupplierLocation.AdapterSource");
            if (StringUtil.nullOrEmptyOrBlankString(sAdapterSource)
                    && ((Boolean) appr.getFieldValue("UsePCardBool")).booleanValue())
            {
                return true;
            }
        }
        return false;
    }

    /**
     * CSPL-963 (Srini) Written a new mehtod for this CSPL.
     */
    public static String getFormattedNumberString(int liWarningNum)
    {
        StringBuffer lsbTemp = new StringBuffer();
        lsbTemp.append(lsLeftBrace + liWarningNum + lsRightBrace + lsDotSpace);
        return lsbTemp.toString();
    }

    /**
     * CSPL-1033 (Srini) Get attachment size for po marked attachments.
     */

    public static boolean getAttachmentsSize(Approvable appr)
    {
        liTotalAttachmentSize = 0;
        liAttachmentSizeExceeded = false;
        lburlSupplierLocation = urlSupplierLocation(appr);
        loComments = (BaseVector) appr.getFieldValue("Comments");
        loAttachments = (BaseVector)appr.getAttachments();
        liTotalAttachmentSize = liTotalAttachmentSize + getconsultingAttachmentsFieldsSize(appr);
        for(int k=0; k< loAttachments.size(); k++)
        {
        	Log.customer.debug("eVA_AMSReqSubmitHook::Inside loAttachments new:: " + loAttachments.size());
        	AttachmentWrapper loAttachmentWrapper = (AttachmentWrapper)loAttachments.get(k);
        	lbIsExternal = loAttachmentWrapper.getExternalAttachment();        	
            /*
             * CSPL-4432; Pavan; Modified if condition to check line item aatachments 
             */
        	if (lburlSupplierLocation
                    && ((lbIsExternal && loAttachmentWrapper.getFieldValue("LineItemCollection") instanceof Requisition) ||(lbIsExternal && !preferredOrderingMethodPrint(loAttachmentWrapper))))                    		
            {
        		liAttachmentSize = ((Attachment)((AttachmentWrapper) loAttachments.get(k)).getAttachments().get(0)).getContentLength();
        		liTotalAttachmentSize = liTotalAttachmentSize + liAttachmentSize;
        		if (liTotalAttachmentSize > 9500000)
        		{
        			liAttachmentSizeExceeded = true;
        			break;
        		}        	
            }
        }
        for (int i = 0; i < loComments.size(); i++)
        {
            loComment = (Comment) loComments.get(i);
            lbIsExternal = loComment.getExternalComment();
            Log.customer.debug("eVA_AMSReqSubmitHook::lbIsExternal:: " + lbIsExternal);
            if (lburlSupplierLocation
                    && ((lbIsExternal && loComment.getParent() instanceof Requisition && loComment.getLineItem() == null) || (lbIsExternal && !preferredOrderingMethodPrint(loComment))))
            {
                Log.customer.debug("eVA_AMSReqSubmitHook::getAttachmentsSize::loComment " + loComment);
                loAttachments = (BaseVector) loComment.getAttachments();
                for (int j = 0; j < loAttachments.size(); j++)
                {
                    liAttachmentSize = ((Attachment) loAttachments.get(j)).getContentLength();
                    liTotalAttachmentSize = liTotalAttachmentSize + liAttachmentSize;
                    if (liTotalAttachmentSize > 9500000)
                    {
                        liAttachmentSizeExceeded = true;
                        break;
                    }
                }
                Log.customer.debug("eVA_AMSReqSubmitHook::getAttachmentsSize::liAttachmentSize "
                        + liTotalAttachmentSize);
            }
            if (loComment.getComments() != null && !loComment.getComments().isEmpty()
                    && loComment.getComments().size() > 0)
            {
                Log.customer.debug("eVA_AMSReqSubmitHook::getAttachmentsSize::Calling getReplyAttachmentsSize"
                        + loComment.getComments());
                getReplyAttachmentsSize(loComment.getComments());
            }
        }
        if (liTotalAttachmentSize > 9500000)
        {
            liAttachmentSizeExceeded = true;
        }
        return liAttachmentSizeExceeded;
    }
    /*
     * (Sarath) Include attachment size of attachments in Consulting Header.
     */
    public static int getconsultingAttachmentsFieldsSize(Approvable appr)
    {
        Log.customer.debug("eVA_AMSReqSubmitHook::getconsultingAttachmentsFieldsSize::Entered ");
        int iTotalConsultingAttachemntsSize = 0;
        List lvReqLineItems = (List) appr.getFieldValue("LineItems");
        for (int i = 0; i < lvReqLineItems.size(); i++)
        {
            CategoryLineItemDetails categoryLineItemDetails = ((ReqLineItem) lvReqLineItems.get(i))
                    .getCategoryLineItemDetails();
            // Added null check as part of CSPL-2066
            if (categoryLineItemDetails!=null && categoryLineItemDetails.toString().indexOf("ConsultingLineItemDetails") >= 0)
            {
                Attachment RequirementsDocumentAttachment = (Attachment) categoryLineItemDetails
                        .getDottedFieldValue("SharedGlobalProperties.RequirementsDocument");
                if (RequirementsDocumentAttachment != null)
                {
                    iTotalConsultingAttachemntsSize = iTotalConsultingAttachemntsSize
                            + RequirementsDocumentAttachment.getContentLength();
                }
                Attachment SOWAttachmentAttachment = (Attachment) categoryLineItemDetails
                        .getDottedFieldValue("SharedGlobalProperties.SOWAttachment");
                if (SOWAttachmentAttachment != null)
                {
                    iTotalConsultingAttachemntsSize = iTotalConsultingAttachemntsSize
                            + SOWAttachmentAttachment.getContentLength();
                }
                Attachment TermsAndConditionsAttachment = (Attachment) categoryLineItemDetails
                        .getDottedFieldValue("SharedGlobalProperties.TermsAndConditions");
                if (TermsAndConditionsAttachment != null)
                {
                    iTotalConsultingAttachemntsSize = iTotalConsultingAttachemntsSize
                            + TermsAndConditionsAttachment.getContentLength();
                }
                Attachment ApproachAttachmentAttachment = (Attachment) categoryLineItemDetails
                        .getDottedFieldValue("SharedGlobalProperties.ApproachAttachment");
                if (ApproachAttachmentAttachment != null)
                {
                    iTotalConsultingAttachemntsSize = iTotalConsultingAttachemntsSize
                            + ApproachAttachmentAttachment.getContentLength();
                }
                Attachment PastProjectsAttachment = (Attachment) categoryLineItemDetails
                        .getDottedFieldValue("SharedGlobalProperties.PastProjectsAttachment");
                if (PastProjectsAttachment != null)
                {
                    iTotalConsultingAttachemntsSize = iTotalConsultingAttachemntsSize
                            + PastProjectsAttachment.getContentLength();
                }
                Attachment ExpectedDeliverablesAttachments = (Attachment) categoryLineItemDetails
                        .getDottedFieldValue("SharedGlobalProperties.ExpectedDeliverablesAttachments");
                if (ExpectedDeliverablesAttachments != null)
                {
                    iTotalConsultingAttachemntsSize = iTotalConsultingAttachemntsSize
                            + ExpectedDeliverablesAttachments.getContentLength();
                }
                BaseVector SupplimentInformationAttachments = (BaseVector) categoryLineItemDetails
                        .getDottedFieldValue("SharedGlobalProperties.SupplimentInformationAttachments");
                Log.customer
                        .debug("eVA_AMSReqSubmitHook::getconsultingAttachmentsFieldsSize:: iTotalConsultingAttachemntsSize before entering Suppli info"
                                + iTotalConsultingAttachemntsSize);
                for (int j = 0; j < SupplimentInformationAttachments.size(); j++)
                {                    
                    iTotalConsultingAttachemntsSize = iTotalConsultingAttachemntsSize
                            + ((Attachment) SupplimentInformationAttachments.get(j)).getContentLength();
                }
                Log.customer
                        .debug("eVA_AMSReqSubmitHook::getconsultingAttachmentsFieldsSize:: iTotalConsultingAttachemntsSize before leaving"
                                + iTotalConsultingAttachemntsSize);
                break;
            }
        }
        Log.customer
                .debug("eVA_AMSReqSubmitHook::getconsultingAttachmentsFieldsSize:: iTotalConsultingAttachemntsSize before returning"
                        + iTotalConsultingAttachemntsSize);
        return iTotalConsultingAttachemntsSize;
    }

    public static void getReplyAttachmentsSize(List comments)
    {
        Log.customer.debug("eVA_AMSReqSubmitHook::getReplyAttachmentsSize::Inside getReplyAttachmentsSize");
        if (!comments.isEmpty())
        {
            for (int i = 0; i < comments.size(); i++)
            {
                oReplycomment = (Comment) comments.get(i);
                lbIsReplyExternal = oReplycomment.getExternalComment();
                lobvReplyAttachments = (BaseVector) oReplycomment.getAttachments();
                if (lburlSupplierLocation
                        && ((lbIsReplyExternal && oReplycomment.getParent() instanceof Requisition && oReplycomment
                                .getLineItem() == null) || (lbIsReplyExternal && !preferredOrderingMethodPrint(oReplycomment))))
                {
                    for (int j = 0; j < lobvReplyAttachments.size(); j++)
                    {
                        loReplyAttachmentSize = ((Attachment) lobvReplyAttachments.get(j)).getContentLength();
                        liTotalAttachmentSize = liTotalAttachmentSize + loReplyAttachmentSize;
                        if (liTotalAttachmentSize > 9500000)
                        {
                            liAttachmentSizeExceeded = true;
                            break;
                        }
                    }
                    Log.customer.debug("eVA_AMSReqSubmitHook::getAttachmentsSize::liTotalAttachmentSize "
                            + liTotalAttachmentSize);
                }
                if (oReplycomment.getComments() != null && !oReplycomment.getComments().isEmpty()
                        && oReplycomment.getComments().size() > 0)
                {
                    Log.customer
                            .debug("eVA_AMSReqSubmitHook::getAttachmentsSize::Calling again getReplyAttachmentsSize"
                                    + oReplycomment.getComments());
                    getReplyAttachmentsSize(oReplycomment.getComments());
                }
            }
        }
    }
    /*
     * CSPL-4432; Pavan; Updated getAttachmentWarningMSG method to read attachments directly from approvable, instead of comments.
     * And removed addReplyAttachmentsMSG method, as it is no longer required.
     */
    public static String getAttachmentWarningMSG(Approvable appr)
    {
        StringBuffer lsbWarningMSG = new StringBuffer();
        lbSemiColon = false;
        Iterator attachments = appr.getAttachmentsIterator();
        while (attachments.hasNext())
        {
            AttachmentWrapper attWrapper = (AttachmentWrapper)attachments.next();
            if(attWrapper != null && attWrapper.getAttachment() != null)
            {
             lbIsExternal = attWrapper.getExternalAttachment();
             Log.customer.debug("eVA_AMSReqSubmitHook::lbIsExternal " + lbIsExternal);
             if (lburlSupplierLocation
                    && ((lbIsExternal && attWrapper.getLineItemCollection() instanceof Requisition) || (lbIsExternal && !preferredOrderingMethodPrint(attWrapper))))
                 {
                    String lsFileName = attWrapper.getAttachment().getFilename();
                    liAttachmentSize = attWrapper.getAttachment().getContentLength();
                    if (lbSemiColon)
                    {
                        lsbWarningMSG.append(";" + lsDotSpace + getFormattedMessage(liAttachmentSize, lsFileName));
                    }
                    else
                    {
                        lsbWarningMSG.append(getFormattedMessage(liAttachmentSize, lsFileName));
                        lbSemiColon = true;
                    }
                    Log.customer.debug("eVA_AMSReqSubmitHook::lsAttachmentmessage:: " + lsbWarningMSG.toString());
                 }
                }
            }
        return lsbWarningMSG.toString();
    }

    public static String getFormattedMessage(int liAttachment, String lsFile)
    {
        StringBuffer lsbFileName = new StringBuffer();
        lsbFileName.append(lsFile + lsDotSpace + lsLeftBrace + getFormattedSizeMB(liAttachment) + lsRightBrace);
        return lsbFileName.toString();
    }

    public static String getFormattedSizeMB(int AttachmentSize)
    {
        Integer lIMBInt = new Integer(AttachmentSize);
        Float lFMBFloat = new Float(lIMBInt.floatValue() / 1000000f);
        String lsAttachmentSize = lFMBFloat.toString();
        int liDotIndex = lsAttachmentSize.indexOf('.');
        return lsAttachmentSize.substring(0, liDotIndex) + lsAttachmentSize.substring(liDotIndex, liDotIndex + 3)
                + lsDotSpace + "MB";
    }

    public static void addHeaderComment(Approvable appr)
    {
        loComments = (BaseVector) appr.getFieldValue("Comments");
        boolean lbFoundAttachmentMSG = false;
        if (loComments.isEmpty())
        {
            lbFoundAttachmentMSG = true;
        }
        boolean lbAttachmentExceedSize = getAttachmentsSize(appr);
        for (int i = 0; i < loComments.size(); i++)
        {
            loComment = (Comment) loComments.get(i);
            lbIsExternal = loComment.getExternalComment();
            if (lbIsExternal && (loComment.getTitle()).equalsIgnoreCase("Attachment Message")
                    && !lbAttachmentExceedSize)
            {
                Log.customer.debug("eVA_AMSReqSubmitHook::liDotIndex:: Removing the comment" + loComment);
                loComments.remove(i);
                lbFoundAttachmentMSG = true;
                appr.save();
                // break;
            }
            else if (lbAttachmentExceedSize && (loComment.getTitle()).equalsIgnoreCase("Attachment Message")
                    && lbIsExternal)
            {
                lbFoundAttachmentMSG = true;
                // break;
            }
            else if (!lbAttachmentExceedSize || !urlSupplierLocation(appr))
            {
                // Setting variable to true so that it will not add comment if attachment size is
                // not greater than 3.5MB
                lbFoundAttachmentMSG = true;
                // break;
            }
        }
        if (!lbFoundAttachmentMSG)
        {
            Comment loComment = new Comment(appr.getPartition());
            loComment.setUser(appr.getRequester());
            loComment.setTitle("Attachment Message");
            loComment.setBody(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core", "AttachmentComment"));
            loComment.setType(Comment.TypeSubmit);
            loComment.setDate(new ariba.util.core.Date());
            loComment.setExternalComment(true);
            appr.addComment(loComment);
            appr.save();
        }
    }

    public static boolean preferredOrderingMethodPrint(Comment lomComment)
    {
        if (lomComment.getLineItem() != null && lomComment.getLineItem() instanceof ReqLineItem)
        {
            ReqLineItem lobReqLineItem = (ReqLineItem) lomComment.getLineItem();
            Log.customer.debug("eVA_AMSReqSubmitHook::Return value "
                    + lobReqLineItem.getSupplierLocation().getPreferredOrderingMethod().equalsIgnoreCase("Print"));
            return lobReqLineItem.getSupplierLocation().getPreferredOrderingMethod().equalsIgnoreCase("Print");
        }
        else
        {
            // Dummy one..At any case above (if) will return some value
            return false;
        }
    }
    public static boolean preferredOrderingMethodPrint(AttachmentWrapper loAttachmentWrapper)
    {
    	Log.customer.debug("eVA_AMSReqSubmitHook::Inside preferredOrderingMethodPrint new:: " +loAttachmentWrapper.getLineItem());
        if (loAttachmentWrapper.getFieldValue("FirstLineItem") != null && loAttachmentWrapper.getLineItem() instanceof ReqLineItem)
        {
        	Log.customer.debug("eVA_AMSReqSubmitHook::Inside preferredOrderingMethodPrint new:: inside if");
            ReqLineItem lobReqLineItem = (ReqLineItem) loAttachmentWrapper.getLineItem();
            return lobReqLineItem.getSupplierLocation().getPreferredOrderingMethod().equalsIgnoreCase("Print");
        }
        else
        {
            // Dummy one..At any case above (if) will return some value
            return false;
        }
    }

    public static boolean urlSupplierLocation(Approvable appr)
    {
        List vLines = ((LineItemCollection) appr).getLineItems();
        String sOrderingMethod = null;
        LineItem li = null;
        boolean lboolIsURLOrdering = false;
        if (vLines.size() >= 1)
        {
            for (int iLine = 0; iLine < vLines.size(); iLine++)
            {
                li = (LineItem) vLines.get(iLine);
                sOrderingMethod = (String) li.getDottedFieldValue("SupplierLocation.PreferredOrderingMethod");
                if (sOrderingMethod.equalsIgnoreCase("URL"))
                {
                    lboolIsURLOrdering = true;
                    break;
                }
            }
        }
        return lboolIsURLOrdering;
    }
    
    
    /**
     * CSPL-6684 JB - check to see if SWAM has changed.
     */

    public  String checkSWAMTypeMatch(Approvable appr)
    {
    	List loReqLines = ((Requisition)appr).getLineItems();
    	String lsLineNumbers = null;
    	for (int i = 0; i < loReqLines.size(); i++)
        {
    		ReqLineItem loReqLine = (ReqLineItem) loReqLines.get(i);
        	if(AMSReqSubmitHook.isLineItemSWAMChanged(loReqLine))
        	{
        		lsLineNumbers = getFormattedLineNumber(loReqLine.getNumberInCollection(), lsLineNumbers);
        	}
        }
    	return lsLineNumbers;
    }
    
    public String getFormattedLineNumber(int lineNumber, String lineNumbers)
    {
    	StringBuffer lsbTemp = new StringBuffer();
    	if(lineNumbers != null)
    	{
    		lsbTemp.append(lineNumbers + "," + lineNumber);
    	}
    	else
    	{
    		lsbTemp.append(lineNumber);
    	}
    	return lsbTemp.toString();
    }
}
