/*

    Responsible: rangle
 */

/* 08/11/2003: Updates for Ariba 8.0 (Jeff Namadan) */
/* 09/05/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import java.util.List;

import ariba.approvable.core.Approvable;
import ariba.base.core.BaseId;
import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLNameTable;
import ariba.base.core.aql.AQLQuery;
import ariba.base.fields.InitializeValueSource;
import ariba.base.fields.ValueSource;
import ariba.common.core.Supplier;
import ariba.common.core.SupplierLocation;
import ariba.procure.core.ProcureLineItem;
import ariba.procure.core.RankedSupplier;
import ariba.purchasing.core.PurchaseOrder;
import ariba.util.log.Log;
import ariba.workforce.core.LaborLineItemDetails;

/*
 * 
 * This class changes the behavior of the supplier location chooser in UI. It filters the suppplier
 * location based on the supplier id and the address type of the location. The address type can be
 * 'P' for payment address or 'O' for Order address.
 * 
 */

/* Ariba 8.0: Replaced NamedObjectNameTable */
// 81->822 changed Vector to List
public class BuysenseSupplierLocationNameTable extends AQLNameTable implements InitializeValueSource
{

    // public static final String FieldName="FieldDefault2";

    /**
     * Overridden to check the results to see if the approvable is an eform.
     */
    public void initializeValueSource(ValueSource vs)
    {
        Log.customer.debug("Hello from CUSTOM SUPPLIER LOCATION NAME TABLE CLASS 0");
        if (vs instanceof SupplierLocation)
        {
            Supplier supplier = supplier();
            if (supplier != null)
            {
                ((SupplierLocation) vs).setSupplier(supplier);
            }
        }

    }

    public List matchPattern(String field, String pattern)
    {
        Log.customer.debug("Hello from CUSTOM SUPPLIER LOCATION NAME TABLE CLASS 1");
        ValueSource approvable = getValueSourceContext();
        Log.customer.debug("BuysenseSupplierLocationNameTable::approvable " + approvable);
        List results = super.matchPattern(field, pattern);
        return results;
    }

    public void addQueryConstraints(AQLQuery query, String field, String pattern)
    {
        Log.customer.debug("Hello from CUSTOM SUPPLIER LOCATION NAME TABLE CLASS 2");

        // build the default set of field constraints for the pattern
        super.addQueryConstraints(query, field, pattern);
        beginSystemQueryConstraints(query);

        // add the constraints
        String addressType = "ZZ";
        ValueSource approvable = getValueSourceContext();

        Log.customer.debug("Approvable name " + approvable);
        /*
         * If the approvable is a requisition then show only order addresses and if it is a
         * PaymentEform then show only payment address.
         * 
         */
         // Added null check as part of CSPL-2066
        if (approvable!=null && ((approvable instanceof RankedSupplier) || (approvable instanceof ProcureLineItem)
                || (approvable instanceof LaborLineItemDetails)
                || (approvable.toString().indexOf("ConsultingLineItemDetails") >= 0)))
        {
            addressType = "O";
        }
        else if (((Approvable) approvable).instanceOf("ariba.core.PaymentEform"))
        {
            addressType = "P";
        }

        // Get the supplier id selected for the approvable
        Supplier supplier = supplier();

        // if supplier selected is null, then we CURRENTLY FUDGE THE AQL to select those supplier
        // locations that have
        // a dummy address type of ZZ. This is because trying to select on a dummy supplier with
        // BaseId (0,0) was
        // throwing errors. (DEV SPL # 142)

        if (supplier == null)
        {
            addressType = "ZZ";
            AQLCondition addressTypeCond = AQLCondition.buildEqual(query.buildField("AddressType"), addressType);
            query.and(addressTypeCond);
        }
        // if supplier is not null then create the aql with the supplier condition and the address
        // type condition
        else if (supplier != null)
        {
            BaseId id = supplier.getBaseId();
            AQLCondition supplierCond = AQLCondition.buildEqual(query.buildField("Supplier"), id);
            AQLCondition addressTypeCond = AQLCondition.buildEqual(query.buildField("AddressType"), addressType);
            query.and(addressTypeCond);
            query.and(supplierCond);
        }

        endSystemQueryConstraints(query);

    }

    /*
     * Ariba 8.0: NamedObjectNameTable was deprecated by AQLNameTable. This class was changed to
     * derive from AQLNameTable and the method constraints() is not in this class and isn't needed.
     * public List constraints (String field, String pattern) {Log.customer.debug("Hello from CUSTOM
     * SUPPLIER LOCATION NAME TABLE CLASS 3"); List constraints = super.constraints(field, pattern);
     * return constraints; }
     */

    private Supplier supplier()
    {

        ValueSource context = getValueSourceContext();
        if (context instanceof Supplier)
        {
            return (Supplier) context;
        }
        if (context instanceof ProcureLineItem)
        {
            return ((ProcureLineItem) context).getSupplier();
        }
        if (context instanceof LaborLineItemDetails)
        {
            return (Supplier) ((LaborLineItemDetails) context).getDottedFieldValue("LineItem.Supplier");
        }
        if (context instanceof PurchaseOrder)
        {
            return ((PurchaseOrder) context).getSupplier();
        }
        if (context instanceof RankedSupplier)
        {
            return (Supplier) ((RankedSupplier) context).getFieldValue("PartitionedSupplier");
        }
        // Added null check as part of CSPL-2066
        if (context!=null && context.toString().indexOf("ConsultingLineItemDetails") >= 0)
        {
            Log.customer.debug("Inside Categorylineitem Details Check");
            return (Supplier) context.getDottedFieldValue("LineItem.Supplier");
        }
        if (((Approvable) context).instanceOf("ariba.core.PaymentEform"))
        {
            return (Supplier) (context).getFieldValue("Supplier");
        }
        else
            return null;
    }

}
