package config.java.ams.custom;

import ariba.base.core.BaseObject;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.util.core.PropertyTable;

import ariba.util.log.Log;

public class BuysenseVisibilityForReceiptField extends Condition{

        private static final ValueInfo parameterInfo[];
        public boolean evaluate(Object value, PropertyTable params) throws ConditionEvaluationException
        {
            BaseObject obj   = (BaseObject)params.getPropertyForKey("SourceObject");
            Log.customer.debug("inside BuysenseVisibilityForReceiptField SourceObject"+obj);
            
            if(obj.instanceOf("ariba.receiving.core.Receipt"))
            {
                Log.customer.debug("inside BuysenseVisibilityForReceiptField");
                Boolean bIsMassEdit = (Boolean) obj.getFieldValue("EnableMassEdit");
             if (bIsMassEdit != null && bIsMassEdit.booleanValue())
                {
                    Log.customer.debug("inside if :: BuysenseVisibilityForReceiptField");
                       return true;
                }
             else
             {    
               Log.customer.debug("inside else of visibility :BuysenseVisibilityForReceiptField ");
                 return false;
             }  
                                  
            }
            else
            {
                Log.customer.debug("ariba.receiving.core.Receipt");
                return false;
            } 
        }            
        protected ValueInfo[] getParameterInfo()
        {
           return parameterInfo;
        }   
        static
        {
             parameterInfo = (new ValueInfo[] {new ValueInfo("SourceObject", 0)});
        }
    }
