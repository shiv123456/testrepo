package config.java.ams.custom;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import ariba.approvable.core.ApprovalRequest;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.Partition;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.purchasing.core.Requisition;
import ariba.user.core.Approver;
import ariba.user.core.Group;
import ariba.user.core.Role;
import ariba.util.core.ListUtil;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
import java.net.*;

// CSPL-4873 eVA-MB Push Notification - Ariba Changes
public class BuysenseMobPushNotification extends Action
{

    private static String    msCN                    = "BuysenseMobPushNotification";
    private static int       ApprovalState           = 2;
    private static String    DerivedApprovalState    = "Ready";
    private static String    DerivedApprovalRequired = "Required";
    private String           msPushNotificationURL   = "";
    private static Partition moPart                  = null;
    private static String    notificationMessage     = "Approvals Notification";
    private static String    objType                 = "AribaReq";

    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        Log.customer.debug("***%s***Called fire:: valuesource obj %s", msCN, valuesource);
        ApprovalRequest loARObj = null;
        if (valuesource != null && valuesource instanceof ApprovalRequest)
        {
            loARObj = (ApprovalRequest) valuesource;
        }
        else
        {
            Log.customer.debug("***%s***Called fire:: Returning because of invalid obj", msCN);
            return;
        }
        Log.customer.debug("***%s***fire:: ApprovalState %s *** DerivedApprovalState %s *** DerivedApprovalRequired %s ", msCN, loARObj.getState(), loARObj.getDerivedApprovalState(), loARObj.getDerivedApprovalRequired());

        if ((loARObj.getApprovable()!=null && loARObj.getApprovable() instanceof Requisition) && (loARObj.getState() == ApprovalState) && (loARObj.getDerivedApprovalState() != null && loARObj.getDerivedApprovalState().equals(DerivedApprovalState)) && (loARObj.getDerivedApprovalRequired().equals(DerivedApprovalRequired)))
        {
            Log.customer.debug("***%s***Called fire:: Starting Push Notification", msCN);
            sendPushNotification(loARObj);
        }
        
        if ((loARObj.getApprovable()!=null && loARObj.getApprovable().instanceOf(("ariba.core.BuysenseExemptionRequest"))) && (loARObj.getState() == ApprovalState) && (loARObj.getDerivedApprovalState() != null && loARObj.getDerivedApprovalState().equals(DerivedApprovalState)) && (loARObj.getDerivedApprovalRequired().equals(DerivedApprovalRequired)))
        {
            Log.customer.debug("***%s***Called fire:: Starting Push Notification and inside BuysenseExemptionRequest ", msCN);
            
            sendPushNotification(loARObj);
        }
        if ((loARObj.getApprovable()!=null && loARObj.getApprovable().instanceOf(("ariba.core.BuysenseSoleSourceRequest"))) && (loARObj.getState() == ApprovalState) && (loARObj.getDerivedApprovalState() != null && loARObj.getDerivedApprovalState().equals(DerivedApprovalState)) && (loARObj.getDerivedApprovalRequired().equals(DerivedApprovalRequired)))
        {
            Log.customer.debug("***%s***Called fire:: Starting Push Notification and inside BuysenseSoleSourceRequest ", msCN);
            sendPushNotification(loARObj);
        }  
        if ((loARObj.getApprovable()!=null && loARObj.getApprovable().instanceOf(("ariba.core.BuysenseUserProfileRequest"))) && (loARObj.getState() == ApprovalState) && (loARObj.getDerivedApprovalState() != null && loARObj.getDerivedApprovalState().equals(DerivedApprovalState)) && (loARObj.getDerivedApprovalRequired().equals(DerivedApprovalRequired)))
        {
            Log.customer.debug("***%s***Called fire:: Starting Push Notification and inside BuysenseUserProfileRequest ", msCN);
            sendPushNotification(loARObj);
        }
        if ((loARObj.getApprovable()!=null && loARObj.getApprovable().instanceOf(("ariba.core.BuysenseODUSoleSourceRequest"))) && (loARObj.getState() == ApprovalState) && (loARObj.getDerivedApprovalState() != null && loARObj.getDerivedApprovalState().equals(DerivedApprovalState)) && (loARObj.getDerivedApprovalRequired().equals(DerivedApprovalRequired)))
        {
            Log.customer.debug("***%s***Called fire:: Starting Push Notification and inside BuysenseODUSoleSourceRequest ", msCN);
            sendPushNotification(loARObj);
        } 
        if ((loARObj.getApprovable()!=null && loARObj.getApprovable().instanceOf(("ariba.core.BuysenseODUEmergencyProcurement"))) && (loARObj.getState() == ApprovalState) && (loARObj.getDerivedApprovalState() != null && loARObj.getDerivedApprovalState().equals(DerivedApprovalState)) && (loARObj.getDerivedApprovalRequired().equals(DerivedApprovalRequired)))
        {
            Log.customer.debug("***%s***Called fire:: Starting Push Notification and inside BuysenseODUEmergencyProcurement ", msCN);
            sendPushNotification(loARObj);
        }
        if ((loARObj.getApprovable()!=null && loARObj.getApprovable().instanceOf(("ariba.core.BuysenseVMIAssetTracking"))) && (loARObj.getState() == ApprovalState) && (loARObj.getDerivedApprovalState() != null && loARObj.getDerivedApprovalState().equals(DerivedApprovalState)) && (loARObj.getDerivedApprovalRequired().equals(DerivedApprovalRequired)))
        {
            Log.customer.debug("***%s***Called fire:: Starting Push Notification and inside BuysenseVMIAssetTracking ", msCN);
            sendPushNotification(loARObj);
        }         
        else
        {
            Log.customer.debug("***%s***fire:: This Approval Request is not ready for push notification", msCN);
        }
    }

    public void sendPushNotification(ApprovalRequest arObj)
    {
        Approver loApprover = (Approver) arObj.getApprover();
        if (loApprover == null)
        {
            Log.customer.debug("***%s***sendPushNotification:: No approvers in the approval list", msCN);
            return;
        }

        // If approver != null then get the list of users and make the URL call
        // Example URL: http://evalabs-dev.epro.cgipdc.com:1080/pushnotifier/rws/nrs?action=push&ObjType=AribaReq&ObjId=ccc&ObjVersNo=1&ObjTitle=Ariba-Approval1&Message=You have got a notification.&UserList=buyer05,simha

        String loPushUsers = null;
        String loPushPR = arObj.getApprovable().getUniqueName();
        String loPushReason = arObj.getReason();

        if((loPushReason == "" || loPushReason == null) && arObj.getApprovable() instanceof Requisition)
        {
        	Log.customer.debug("***%s***sendPushNotification::Inside Requisition Block");	
        arObj.setReason("Requisition requires your approval");
        loPushReason = arObj.getReason();
        
        Log.customer.debug("***%s***sendPushNotification::Inside Requisition Block" +loPushReason);
        }
        
        if((loPushReason == "" || loPushReason == null) && arObj.getApprovable().instanceOf(("ariba.core.BuysenseSoleSourceRequest")))
        {
        	Log.customer.debug("***%s***sendPushNotification::Inside BuysenseSoleSourceRequest Block");	
        arObj.setReason("DGS Sole Source Request requires your approval");
        loPushReason = arObj.getReason();
        
        Log.customer.debug("***%s***sendPushNotification::Inside BuysenseSoleSourceRequest Block" +loPushReason);
        
        }
        if((loPushReason == "" || loPushReason == null) && arObj.getApprovable().instanceOf(("ariba.core.BuysenseExemptionRequest")))
        {
        	Log.customer.debug("***%s***sendPushNotification::Inside BuysenseExemptionRequest Block");	
        arObj.setReason("DPS Exemption Request requires your approval");
        loPushReason = arObj.getReason();
        
        Log.customer.debug("***%s***sendPushNotification::Inside BuysenseExemptionRequest Block" +loPushReason);
        
        }
        if((loPushReason == "" || loPushReason == null) && arObj.getApprovable().instanceOf(("ariba.core.BuysenseUserProfileRequest")))
        {
        	Log.customer.debug("***%s***sendPushNotification::Inside BuysenseUserProfileRequest Block");	
        arObj.setReason("UserProfileRequest requires your approval");
        loPushReason = arObj.getReason();
        
        Log.customer.debug("***%s***sendPushNotification::Inside BuysenseUserProfileRequest Block" +loPushReason);
        
        }
        if((loPushReason == "" || loPushReason == null) && arObj.getApprovable().instanceOf(("ariba.core.BuysenseODUSoleSourceRequest")))
        {
        	Log.customer.debug("***%s***sendPushNotification::Inside BuysenseODUSoleSourceRequest Block");	
        arObj.setReason("ODU Sole SourceRequest requires your approval");
        loPushReason = arObj.getReason();
        
        Log.customer.debug("***%s***sendPushNotification::Inside BuysenseODUSoleSourceRequest Block" +loPushReason);
        
        }
        if((loPushReason == "" || loPushReason == null) && arObj.getApprovable().instanceOf(("ariba.core.BuysenseODUEmergencyProcurement")))
        {
        	Log.customer.debug("***%s***sendPushNotification::Inside BuysenseODUEmergencyProcurement Block");	
        arObj.setReason("ODU Emergency Procurement Justification requires your approval");
        loPushReason = arObj.getReason();
        
        Log.customer.debug("***%s***sendPushNotification::Inside BuysenseODUEmergencyProcurement Block" +loPushReason);
        
        }
        if((loPushReason == "" || loPushReason == null) && arObj.getApprovable().instanceOf(("ariba.core.BuysenseVMIAssetTracking")))
        {
        	Log.customer.debug("***%s***sendPushNotification::Inside BuysenseVMIAssetTracking Block");	
        arObj.setReason("VMI Asset Tracking requires your approval");
        loPushReason = arObj.getReason();
        
        Log.customer.debug("***%s***sendPushNotification::Inside BuysenseVMIAssetTracking Block" +loPushReason);
        
        }
        String parameterURL = null;
        List loPushNotifiers = getPushNotifiers(loApprover);

        for (int i = 0; i < loPushNotifiers.size(); i++)
        {
            // form a comma separated list
            if (loPushUsers == null) // when i=0
            {
                loPushUsers = (String) loPushNotifiers.get(i);
            }
            // i>0
            else
            {
                loPushUsers = loPushUsers + ",";
                loPushUsers = loPushUsers + (String) loPushNotifiers.get(i);
            }
            Log.customer.debug("***%s***sendPushNotification:: printing user: %s", msCN, (String) loPushNotifiers.get(i));
        }
        if (loPushUsers == null) // no notifications need to be sent out
        {
            Log.customer.debug("***%s***sendPushNotification:: No users in user list to send Push Notification*** for PR:%s ***", msCN, loPushPR);
            return;
        }

        Log.customer.debug("***%s***sendPushNotification:: Sending Push Notification for Users:%s *** PR:%s *** Reason:%s ", msCN, loPushUsers, loPushPR, loPushReason);

        HttpURLConnection connection = null;
        URL webServiceURL = null;
        BufferedReader br = null;

        // Make page non-cachable
        try
        {
            if (moPart == null)
            {
                moPart = Base.getService().getPartition("pcsv");
            }

            Log.customer.debug("***%s***sendPushNotification:PARTITION: calling serviceURL URL:%s", msCN, moPart);

            msPushNotificationURL = Base.getService().getParameter(moPart, "Application.AMSParameters.PushNotificationURL");

            parameterURL = "&ObjType=" + URLEncoder.encode(objType, "UTF-8");
            parameterURL = parameterURL + "&ObjId=" + URLEncoder.encode(loPushPR, "UTF-8");
            parameterURL = parameterURL + "&ObjVersNo=" + URLEncoder.encode(Integer.toString(arObj.getApprovable().getVersion()), "UTF-8");
            parameterURL = parameterURL + "&ObjTitle=" + URLEncoder.encode(arObj.getApprovable().getName(), "UTF-8");
            parameterURL = parameterURL + "&Message=" + URLEncoder.encode(notificationMessage, "UTF-8");
            parameterURL = parameterURL + "&UserList=" + loPushUsers;

            // add basic URL with the parameters
            msPushNotificationURL = msPushNotificationURL + parameterURL;
            Log.customer.debug("***%s***sendPushNotification:: calling serviceURL URL:%s", msCN, msPushNotificationURL);

            webServiceURL = new URL(msPushNotificationURL);
            connection = (HttpURLConnection) webServiceURL.openConnection();
            connection.setRequestProperty("pncontrolmethod", "postgetoption");
            connection.setUseCaches(false);
            connection.setDoInput(true);

            if (connection.getResponseCode() != 200)
            {
                Log.customer.debug("*****%s*****run***** Exception: Failed : HTTP error code : %s", msCN, connection.getResponseCode());
            }

            br = new BufferedReader(new InputStreamReader((connection.getInputStream())));

            String output;
            while ((output = br.readLine()) != null)
            {
                Log.customer.debug("*****%s*****run***** Output from Server: %s", msCN, output);
            }

            Log.customer.debug("***%s***sendPushNotification:: DONE calling URL", msCN);

        }
        catch (MalformedURLException e)
        {
            Log.customer.debug("*****%s*****run***** Exception: %s", msCN, e.getMessage());
        }
        catch (ProtocolException e)
        {
            Log.customer.debug("*****%s*****run***** Exception: %s", msCN, e.getMessage());
        }
        catch (IOException e)
        {
            Log.customer.debug("*****%s*****run***** Exception: %s", msCN, e.getMessage());
        }
        finally
        {
            Log.customer.debug("***%s***sendPushNotification:: in finally block", msCN);
            // close the connection, set all objects to null
            connection.disconnect();
            br = null;
            connection = null;
        }

    }

    public List<String> getPushNotifiers(Approver foApprover)
    {
        List<String> loPushNotitifier = ListUtil.list();
        List<BaseId> loUsers = ListUtil.list();
        String lsPushNotifier = null;
        if (foApprover instanceof Role)
        {
            Role loRole = (Role) foApprover;
            loUsers = loRole.getAllUsers();
            Log.customer.debug("*****Class:%s********** approver: %s, users: %s", msCN, loRole, loUsers);
            loPushNotitifier = getPushUsers(loUsers);
        }
        if (foApprover instanceof Group)
        {
            Group loGroup = (Group) foApprover;
            loUsers = loGroup.getAllUsers();
            loPushNotitifier = getPushUsers(loUsers);
        }
        if (foApprover instanceof ariba.user.core.User && foApprover.getDottedFieldValue("PushNotificationEnabled") != null && (Boolean) foApprover.getDottedFieldValue("PushNotificationEnabled"))
        {
            lsPushNotifier = (String) ((ariba.user.core.User) foApprover).getUniqueName();
            loPushNotitifier.add(lsPushNotifier);
        }
        return loPushNotitifier;
    }

    public List<String> getPushUsers(List<BaseId> loUserList)
    {
        ariba.user.core.User loUser = null;
        BaseId loUsrbid = null;
        List<String> loPushList = ListUtil.list();

        for (int i = 0; i < loUserList.size(); i++)
        {
            loUsrbid = (BaseId) loUserList.get(i);
            Log.customer.debug("*****Class:%s********** loUserList: %s, loUsrbid : %s", msCN, loUserList, loUsrbid);
            loUser = (ariba.user.core.User) Base.getSession().objectFromId(loUsrbid);
            Log.customer.debug("*****Class:%s********** loUser : %s", msCN, loUser);
            // add to the list only if enabled for PUSH notification
            if (loUser.getDottedFieldValue("PushNotificationEnabled") != null && (Boolean) loUser.getDottedFieldValue("PushNotificationEnabled"))
            {
                loPushList.add((String) (loUser.getUniqueName()));
            }
            else
            {
                Log.customer.debug("*****Class:%s********** loUser : %s  not enabled for PUSH notification", msCN, loUser);
            }
        }
        return loPushList;
    }
}
