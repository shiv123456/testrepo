/* 08/29/2003: Updates for Ariba 8.0 (David Chamberlain) */
// 10/10/2003: Updates for Ariba 8.1 (Richard Lee)
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.base.fields.*;
import ariba.util.core.PropertyTable;
// Ariba 8.1 Util deprecated
//import ariba.util.core.Util;
// Ariba 8.0: The netscape package no longer exists.
//import netscape.util.Enumeration;
import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.common.core.PCard;
import java.util.List;
import ariba.util.core.Date;
import ariba.util.log.Log;
import ariba.purchasing.core.Requisition;
/****************************
Rob Giesen
December 2000

This trigger is used by PCardToUse to set the first PCard to be the default PCard.
It is also called when the On Behalf Of field (the Requester) changes, to set the value
of the chooser to the new Requester

January 2001
Added a check for expiration date.  If the date is null, or the expiration date has passed, then
it is not a valid PCard.


rlee, 2/25/03, eProc Production SPL 56, UsePCardBool is set to true on creation, so when changed order is created it is also set
to true even when Requester had choose false.

rlee, 8/7/03: ST SPL 1057 - User not seeing PCard Type 3. Mod to include Type 3 card.
*****************************/
/**
* CSPL:2996 - On Behalf Of toggling on the Title page is not properly refreshing PCards
* 
* @author Pavan Aluri
* @Date 13-May-2011
* @Explanation  In 9r1 at Req creation title is null and because of below reqname if condition, incorrect Pcard values were shown
* for selected On Behalf Of user as it always return effective user Pcard. Commented the condition to fix this.  
*/
//81->822 changed Vector to List
public class BuysensePCardInitialValue extends Action
{

    public void fire(ValueSource object, PropertyTable params)
    {
        List currentUserPCard=null;
        //The object is the Requisition
        Requisition currentReq=(Requisition)object;
        //Try to grab the name.  If the name is null, it means that it is upon creation, so the
        //requisition hasn't been populated yet with a Requester, so just use the EffectiveUser.
        //If the name is not null, then get the requester from the current requisition.

        Partition moPartition = Base.getSession().getPartition();
        String sReqID = currentReq.getUniqueId();
        String sBuysenseAccSummaryURL = Base.getService().getParameter(moPartition, "Application.Base.Data.BuysenseAcctSummaryBaseURL");
        
        if (sBuysenseAccSummaryURL != null)
        {
            sBuysenseAccSummaryURL = sBuysenseAccSummaryURL + sReqID;
            Log.customer.debug("setting account summary dynamic URL as: " + sBuysenseAccSummaryURL);
            currentReq.setFieldValue("BuysenseAccSummaryURL", sBuysenseAccSummaryURL);
        }
        //rlee, eProc Prod SPL 56
        Requisition previousV = (Requisition) currentReq.getDottedFieldValue("PreviousVersion");
        
        /*In 9r1 Req title is null at the creation, below if block always return effective user pcard even when On Behalf Of is changed.
          Correct Pcard values are not shown until Title is set, to fix this below if block is commented. On creation, Requester is not null
          and hence no reqname substitute or additional check is required  */
        
        /*String reqname = currentReq.getName();

        if (reqname==null)
        {
            Log.customer.debug("before getEffectiveUser") ;
            //It is a new requisition, so grab the current user, who will be the preparer as well
            //Ariba 8.1: Added class definition to decipher between ariba.common.core.User class and new ariba.user.core.User class.
            ariba.user.core.User currentUser=(ariba.user.core.User)Base.getSession().getEffectiveUser();
                        if ( currentUser != null )
               //Ariba 8.1: Replaced deprecated getPCards() method with getPCardsVector() method.
                //Ariba 8.1: Modified the method to get the partition User for the Requester
                //in order to access the getPCards() method from old ariba.common.core.User class.
                currentUserPCard =ariba.common.core.User.getPartitionedUser(currentUser,currentReq.getPartition()).getPCardsVector();
            Log.customer.debug("after getEffectiveUser") ;
        }
        else*/

            Log.customer.debug("before getEffectiveUser") ;
            //Ariba 8.1: Added class definition to decipher between ariba.common.core.User class and new ariba.user.core.User class.
            ariba.user.core.User RequesterUser = currentReq.getRequester();

                        if ( RequesterUser != null )
                        //Ariba 8.1: Replaced deprecated getPCards() method with getPCardsVector() method.
                        //Ariba 8.1: Modified the method to get the partition User for the Requester
                        //in order to access the getPCards() method from old ariba.common.core.User class.
                        currentUserPCard =ariba.common.core.User.getPartitionedUser(RequesterUser,currentReq.getPartition()).getPCardsVector();

            Log.customer.debug("after getEffectiveUser") ;

                if ( currentUserPCard == null )
                   return ;

        int numPCards=currentUserPCard.size();
        int flag=0;

        if (numPCards>0)
        {
            // rlee, eProc Production SPL 56, UsePCardBool is set to true on creation, so when changed order is created it is also set
            // to true even when Requester had choose false.
            if ( previousV == null )
            {
              Log.customer.debug("rlee previous version is null") ;
              currentReq.setDottedFieldValue("UsePCardBool", new Boolean (true));
            }
            else
            {
                //Copy PCard fields information from previous version of Requisition.
                currentReq.setDottedFieldValue("UsePCardBool", previousV.getDottedFieldValue("UsePCardBool"));
                currentReq.setDottedFieldValue("PCardToUse", previousV.getDottedFieldValue("PCardToUse"));

                Log.customer.debug("rl06, return get out of BuysensePCardInitialValue");
                return;

            }
            for(int i=0;i<numPCards;i++)
            {
                //Get the individual PCard
                Log.customer.debug("rl13, get individual PCard");
                // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                PCard currentPCard = (PCard)currentUserPCard.get(i);

                //Check to see if at least one of their PCards is of type 2
                //rlee, added CardType = 3
                if ((currentPCard.getCardType()==2)||(currentPCard.getCardType()==3))
                {
                    //Check if the expiration date is past today's date
                    //If it is past today's date, it is not a valid PCard

                    Date curPCardDate=currentPCard.getExpirationDate();

                    if (curPCardDate!=null)
                    {
                        int result=0;
                        result = Date.getNow().compareTo(curPCardDate);
                        //result <0 if the current date is less than expDate
                        if (result<=0)
                        {
                            object.setDottedFieldValue("PCardToUse", currentPCard);
                            flag=1;
                        }

                    }
                }
            }
        }
        //If the user has no PCards, or no PCards that are valid (type =2), then set the field to null
        if (flag==0)
        {
            object.setDottedFieldValue("UsePCardBool", new Boolean (false));
            object.setDottedFieldValue("PCardToUse",null);
        }
    }
}
