package config.java.ams.custom;

import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
public class SetAllLines extends Action
{

    public void fire(ValueSource object, PropertyTable params)
    {
    	Log.customer.debug("Calling SetAllLines.");
    	Boolean bSelectAllLines = (Boolean)object.getFieldValue("EnableMassEdit");
    	Log.customer.debug("the  bSelectAllLines:" + bSelectAllLines);
        BaseVector rclines = (BaseVector)object.getFieldValue("ReceiptItems");
        
        Log.customer.debug("the receipts lines." + rclines);
        int rcLines = rclines.size();
        if(rclines!=null)
        for(int i=0;i<rcLines;i++)
        {
            Log.customer.debug("Inside for Loop...# : %s", new Integer(i));
            BaseObject receiptItem = (BaseObject)rclines.get(i);
			//Boolean loMassEdit = ((Boolean) receiptItem.getFieldValue("MassEditForLine"));
               if(bSelectAllLines.booleanValue())
            	receiptItem.setFieldValue("MassEditForLine",true);
               else
            	   receiptItem.setFieldValue("MassEditForLine",false);

        }
    }
}
