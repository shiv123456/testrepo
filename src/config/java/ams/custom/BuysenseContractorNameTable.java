package config.java.ams.custom;

import ariba.base.core.aql.AQLNameTable;
import ariba.base.core.aql.AQLQuery;
import ariba.base.fields.ValueSource;
import ariba.base.core.aql.SearchTermQuery;
import ariba.util.log.Log;
import java.util.List;
import ariba.workforce.core.LaborLineItemDetails;

public class BuysenseContractorNameTable extends AQLNameTable
{
	protected List matchPattern(String field, String pattern, SearchTermQuery searchTermQuery)
	{
	    List results = super.matchPattern(field, pattern, searchTermQuery);
	    return results;
    }


	public void addQueryConstraints(AQLQuery query, String field, String pattern, SearchTermQuery searchTermQuery)
	{
		Log.customer.debug("BuysenseContractorNameTable: called");
		super.addQueryConstraints(query, field, pattern, searchTermQuery);
		query.getFirstClass().setIncludeInactive(true);
		ValueSource context = getValueSourceContext();
		LaborLineItemDetails laborli = (LaborLineItemDetails)context;
		String lOClientName = (String)laborli.getDottedFieldValue("WorkLocation.ClientName.UniqueName");
		Log.customer.debug("BuysenseContractorNameTable: lOClientName " +lOClientName);
		if(lOClientName!=null)
		{
		   query.andEqual(query.buildField("ClientName.UniqueName"),lOClientName);
		}
	}
}
