// Version 1.1

// Sunil - August 2000

// WA ST SPL # 475/Baseline Dev SPL # 229 - PaymenyAccountings --> Accountings.PaymentAccountings

// WA ST SPL # 219 - Added logic to update #Accepted, #Paid and #InPaymentProcess from Order.

//WA DEV SPL # 232 - Modified logic to add tax amount for discount calculations.
//                   Added call to PaymentSummary.java.

//eVA DEV SPL # 4 - Get #Accepted from NumberAccepted field on order line instead of BuysenseNumberAccepted.

/*
08/07/2003: Updates for Ariba 8.0 (David Chamberlain)
03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/04/2004: rgiesen dev SPL #37 - Do not use LeadCurrency(), use the current partition to get the currency
03/04/2004: rgiesen dev SPL #38 - replace getLeadCurrency with Currency.getDefaultCurrency(partition)
*/

package config.java.ams.custom;

import ariba.base.fields.Action;
import ariba.base.fields.ValueInfo;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import java.util.List;
import ariba.base.core.ClusterRoot;
import ariba.base.core.BaseObject;
import ariba.util.log.Log;
import ariba.purchasing.core.POLineItem;
import ariba.base.core.Partition;
import java.math.BigDecimal;
import ariba.base.core.BaseVector;
// Ariba 8.1 Money and Currency has changed
//import ariba.common.core.Money;
//import ariba.common.core.Currency;
import ariba.basic.core.Money;
import ariba.basic.core.Currency;

//81->822 changed Vector to List
//81->822 changed ConditionValueInfo to ValueInfo
public class CalculateTotals extends Action
{
    static final String Value = "Value";

    private static final ValueInfo[] parameterInfo =
        {
        new ValueInfo(TargetParam, true, IsScalar, StringClass)
    };

    private static final String[] requiredParameterNames =
        {
        TargetParam
    };

    private static final ValueInfo valueInfo =
        new ValueInfo(IsScalar, "ariba.core.PaymentEform");


    public void fire (ValueSource object, PropertyTable params)
    {
        Log.customer.debug("Inside fire of Calculate Totals");
        ClusterRoot cr = (ClusterRoot)object;
        //String paymentname = (String) cr.getFieldValue("UniqueName");

        String ordername = (String)cr.getDottedFieldValue("Order.Name");
        Log.customer.debug("You have the Order %s",ordername);

        // This is sum of all line net amounts.
        Money totalPaymentAmt;
        // Get the hdrShippingAmt, hdrTaxAmt, hdrDiscountAmt from payment eform


        BaseVector polines = (BaseVector)cr.getDottedFieldValue("Order.LineItems");
        if (polines == null)
            return;
        Log.customer.debug("The size of polines vector is %s",polines.size());

        BaseVector pvlines = (BaseVector)cr.getFieldValue("PaymentItems");
        Partition part = cr.getPartition();
        if(polines.size()>pvlines.size())
            return;

        int i;
        //Dev SPL 219
        BigDecimal itemQtyAcc,itemQtyPaid,itemQtyInp;
        if(((String)cr.getFieldValue("StatusString")).equalsIgnoreCase("Composing"))
        {
            for(i=0;i<polines.size();i++)
            {
                POLineItem item = (POLineItem)polines.get(i);
                BaseObject pymntItem = (BaseObject)pvlines.get(i);
                itemQtyAcc=(BigDecimal)item.getFieldValue("NumberAccepted");
                if (itemQtyAcc == null)
                    itemQtyAcc = new BigDecimal(0);
                pymntItem.setFieldValue("NumberAccepted",itemQtyAcc);
                pymntItem.setDottedFieldValue("Accountings.NumberAccepted",
                                              itemQtyAcc);

                itemQtyPaid=(BigDecimal)item.getFieldValue("NumberPaid");
                pymntItem.setFieldValue("NumberPaid",itemQtyPaid);
                itemQtyInp=(BigDecimal)item.getFieldValue("NumberInPaymentProcess");
                pymntItem.setFieldValue("NumberInPaymentProcess",itemQtyInp);

            }
        }

        Log.customer.debug("The accounting edits succeeded");
        //Calculate Amounts
        //Step 1 : Calculate linesTotalAmount which is the total of all the
        //invoice amounts

        //rgiesen dev SPL #38 - use the partition to get the default currency.

        //Money linesTotalAmount = new Money(0.00,lcLeadCurrency.getLeadCurrency());
        Money linesTotalAmount = new Money(0.00,Currency.getDefaultCurrency(part));

        Money paymentItemUnitAmt,paymentItemAmt;

        for(i=0;i<pvlines.size();i++)
        {
            BaseObject paymentItem = (BaseObject)pvlines.get(i);
            BigDecimal qty;
            paymentItemAmt= new Money(0.00, Currency.getDefaultCurrency(part));
            qty = (BigDecimal)paymentItem.getFieldValue("CurPayingQuantity");
            paymentItemUnitAmt=(Money)paymentItem.getFieldValue("InvoiceUnitCost");
            if (paymentItemUnitAmt == null)
            {
                paymentItemUnitAmt = new Money(0.00,
                                               Currency.getDefaultCurrency(part));
                paymentItem.setFieldValue("InvoiceUnitCost",
                                          paymentItemUnitAmt);
            }
            BigDecimal temp = (BigDecimal)paymentItemUnitAmt.getAmount();
            temp = temp.setScale(2, BigDecimal.ROUND_HALF_UP);
            paymentItemAmt.setAmount((qty.multiply(temp)).setScale(2,
                                                                   BigDecimal.ROUND_HALF_UP));
            paymentItem.setFieldValue("InvoiceAmount",paymentItemAmt);
            linesTotalAmount=linesTotalAmount.add(paymentItemAmt);
            Log.customer.debug("linesTotalAmount is %s",
                           linesTotalAmount.getAmount());
        }
        Log.customer.debug("linesTotalAmount is %s",linesTotalAmount.getAmount());

        // Anup - Call to set OrderAmount and structure for any client specific computations based on
        // Parameters.table entry.
        CustomClassRouter.routeRequest((BaseObject)cr,
                                       "Application.AMSParameters.PaymentCalculations");


        // Dev SPL 232 - Reorganize code to include Tax for Discount calculations

        Money hdrShippingAmt=(Money)cr.getFieldValue("ShippingAmt");
        Money hdrTaxAmt=(Money)cr.getFieldValue("TaxAmt");
        if(hdrShippingAmt==null)
        {
            hdrShippingAmt = new Money(0.00, Currency.getDefaultCurrency(part));
        }
        if(hdrTaxAmt==null)
        {
            hdrTaxAmt = new Money(0.00, Currency.getDefaultCurrency(part));
        }
        BigDecimal hdrDiscountPercent = (BigDecimal)cr.getFieldValue("DiscountPercent");
        if(hdrDiscountPercent==null)
        {
            hdrDiscountPercent = new BigDecimal(0);
            cr.setFieldValue("DiscountPercent",hdrDiscountPercent);
        }
        if(hdrDiscountPercent.doubleValue()> 0.0)
        {
            Money tempDiscountAmt = new Money(0.00,
                                              Currency.getDefaultCurrency(part));
            BigDecimal amount = linesTotalAmount.getAmount();
            BigDecimal totalTaxAmt = hdrTaxAmt.getAmount();
            amount = amount.add(totalTaxAmt);
            BigDecimal discValue = amount.multiply(hdrDiscountPercent);
            BigDecimal discValueNet = discValue.divide(new BigDecimal(100),
                                                       2,
                                                       BigDecimal.ROUND_HALF_UP);
            tempDiscountAmt.setAmount(discValueNet);
            cr.setFieldValue("DiscountAmt",tempDiscountAmt);
        }



        Money hdrDiscountAmt=(Money)cr.getFieldValue("DiscountAmt");

        if(hdrDiscountAmt==null)
        {
            hdrDiscountAmt = new Money(0.00, Currency.getDefaultCurrency(part));
        }

        Log.customer.debug("hdrShippingAmt: %s",hdrShippingAmt.getAmount());
        Log.customer.debug("hdrTaxAmt: %s",hdrTaxAmt.getAmount());
        Log.customer.debug("hdrDiscountAmt: %s",hdrTaxAmt.getAmount());


        //Step2 : Calculate totalPaymentAmt
        totalPaymentAmt=new Money(0.00, Currency.getDefaultCurrency(part));
        totalPaymentAmt=totalPaymentAmt.add(linesTotalAmount);
        totalPaymentAmt=totalPaymentAmt.add(hdrShippingAmt);
        totalPaymentAmt=totalPaymentAmt.add(hdrTaxAmt);
        /*totalPaymentAmt=totalPaymentAmt.subtract(totalPaymentAmt,
                                                 hdrDiscountAmt);*/
        totalPaymentAmt=Money.subtract(totalPaymentAmt,hdrDiscountAmt);
        cr.setFieldValue("TotalPaymentAmount",totalPaymentAmt);
        Log.customer.debug("Total Payment amount is %s",
                       totalPaymentAmt.getAmount());


        // Step3 : calculate the net amounts for each line. Add the net amounts to get total paymnet amt.
        // Step4 : calculated in same for loop- The totalNetAmount
        Money lineShippingAmt;
        Money lineTaxAmt;
        Money lineDiscountAmt;
        //Money lineNetAmount; - This is now defined at the beginning of this method.
        Money lineNetAmount;
        Money totalNetAmount=new Money(0.00, Currency.getDefaultCurrency(part));
        Money totalShipAmount=new Money(0.00, Currency.getDefaultCurrency(part));
        Money totalTaxAmount=new Money(0.00, Currency.getDefaultCurrency(part));
        Money totalDiscAmount=new Money(0.00, Currency.getDefaultCurrency(part));

        BaseObject lastValidPaymentItem;
        int lastValidPaymentItemNumber=0;
        for(i=0;i<pvlines.size();i++)
        {
            BaseObject paymentItem = (BaseObject)pvlines.get(i);
            paymentItemAmt= new Money(0.00, Currency.getDefaultCurrency(part));
            lineShippingAmt = new Money(0.00, Currency.getDefaultCurrency(part));
            lineTaxAmt = new Money(0.00, Currency.getDefaultCurrency(part));
            lineDiscountAmt = new Money(0.00, Currency.getDefaultCurrency(part));
            lineNetAmount = new Money(0.00, Currency.getDefaultCurrency(part));

            //Calculate the percentage.
            paymentItemAmt=(Money)paymentItem.getFieldValue("InvoiceAmount");
            //percent=(Money.percentage(paymentItemAmt,linesTotalAmount));
            BigDecimal percent = new BigDecimal(0);
            BigDecimal paymentItemAmt1 = paymentItemAmt.getAmount();
            BigDecimal linesTotalAmount1 = linesTotalAmount.getAmount();
            if (!(linesTotalAmount1 == null)
                    && !(linesTotalAmount1.doubleValue() == 0))
                percent = paymentItemAmt1.divide(linesTotalAmount1,
                                                 8, BigDecimal.ROUND_HALF_UP);
            //Calculate the Shipping, Tax, Discount, Net Amounts for the line using percent.
            //percent=percent/100;
            //BigDecimal percentage =  new BigDecimal(percent);
            //Money.divide( percentage,new BigDecimal(100));
            //lineShippingAmt=(hdrShippingAmt.multiply(percent)).round();
            BigDecimal amount1 = hdrShippingAmt.getAmount();
            BigDecimal shipValue = amount1.multiply(percent);
            shipValue = shipValue.setScale(2, BigDecimal.ROUND_HALF_UP);
            lineShippingAmt.setAmount(shipValue);
            //Money.divide( percentage,new BigDecimal(100));
            //lineTaxAmt=(hdrTaxAmt.multiply(percent)).round();
            BigDecimal amount2 = hdrTaxAmt.getAmount();
            BigDecimal taxValue = amount2.multiply(percent);
            taxValue = taxValue.setScale(2, BigDecimal.ROUND_HALF_UP);
            lineTaxAmt.setAmount(taxValue);
            //Money.divide( percentage,new BigDecimal(100));
            // New code
            BigDecimal amount3 = hdrDiscountAmt.getAmount();
            BigDecimal discValue1 = amount3.multiply(percent);
            discValue1 = discValue1.setScale(2, BigDecimal.ROUND_HALF_UP);
            lineDiscountAmt.setAmount(discValue1);
            // New code
            lineNetAmount=(paymentItemAmt.add(lineShippingAmt.add(lineTaxAmt)));
            //lineNetAmount=lineNetAmount.subtract(lineNetAmount,lineDiscountAmt);
            lineNetAmount=Money.subtract(lineNetAmount, lineDiscountAmt);

            paymentItem.setFieldValue("Shipping",lineShippingAmt);
            paymentItem.setFieldValue("Tax",lineTaxAmt);
            paymentItem.setFieldValue("Discount",lineDiscountAmt);
            paymentItem.setFieldValue("NetAmount",lineNetAmount);
            totalShipAmount=totalShipAmount.add(lineShippingAmt);
            totalTaxAmount=totalTaxAmount.add(lineTaxAmt);
            totalDiscAmount=totalDiscAmount.add(lineDiscountAmt);
            totalNetAmount=totalNetAmount.add(lineNetAmount);

            if(lineNetAmount.getAmount().doubleValue() > 0.0)
            {
                lastValidPaymentItemNumber = i;
            }
        }

        //Do this for last valid line -- critical to match up totals and resolve rounding errors.
        lastValidPaymentItem=(BaseObject)pvlines.get(lastValidPaymentItemNumber);
        Money diffHdrShippingAmt=Money.subtract(hdrShippingAmt,totalShipAmount);
        Money diffHdrTaxAmt=Money.subtract(hdrTaxAmt,totalTaxAmount);
        Money diffHdrDiscountAmt=Money.subtract(hdrDiscountAmt,totalDiscAmount);
        Money diffTotalPaymentAmt=Money.subtract(totalPaymentAmt,
                                                 totalNetAmount);

        lineShippingAmt=(Money)lastValidPaymentItem.getFieldValue("Shipping");
        lineTaxAmt=(Money)lastValidPaymentItem.getFieldValue("Tax");
        lineDiscountAmt=(Money)lastValidPaymentItem.getFieldValue("Discount");
        lineNetAmount=(Money)lastValidPaymentItem.getFieldValue("NetAmount");
        lineShippingAmt.addTo(diffHdrShippingAmt);
        lineTaxAmt.addTo(diffHdrTaxAmt);
        lineDiscountAmt.addTo(diffHdrDiscountAmt);
        lineNetAmount.addTo(diffTotalPaymentAmt);
        lastValidPaymentItem.setFieldValue("Shipping",lineShippingAmt);
        lastValidPaymentItem.setFieldValue("Tax",lineTaxAmt);
        lastValidPaymentItem.setFieldValue("Discount",lineDiscountAmt);
        lastValidPaymentItem.setFieldValue("NetAmount",lineNetAmount);

        //For every payment line the net amount has now been calculated.
        //For every payment line now compute the Accounting Amounts if
        //NetAmount is not 0 for that line.
        Log.customer.debug("About to calculate the amounts for the accountings on each line");
        for(i=0;i<pvlines.size();i++)
        {
            BaseObject paymentItem = (BaseObject)pvlines.get(i);
            lineNetAmount=(Money)paymentItem.getFieldValue("NetAmount");
            List paymentAccountings = (List)paymentItem.getDottedFieldValue("Accountings.PaymentAccountings");
            //if(lineNetAmount.getAmount().intValue()!=0)
            //{
			//rgiesen dev SPL #38 - add the partition to the parameters for the default currency
            computeAccountingAmounts(lineNetAmount,paymentAccountings,part);
            //}
        }

        PaymentSummary ps = new PaymentSummary();
        ps.fire((ValueSource)cr,params);


        Log.customer.debug("Outside For Loop Before Save");
        cr.save();
        Log.customer.debug("Outside For Loop After Save");
    }

    //Method added by Sunil to compute and set the Amounts across the accounting lines.
    //rgiesen dev SPL #38 - add the partition to the parameters for the default currency
    private void computeAccountingAmounts(Money amount,
                                          List paymentAccountings,Partition part)
    {
        BaseObject lastAcctgItem=null;
        Money runningTotalAmount=new Money(0.00,
                                           Currency.getDefaultCurrency(part));
        for(int i=0;i<paymentAccountings.size();i++)
        {
            BaseObject paymentAcctg = (BaseObject)paymentAccountings.get(i);
            // Washington specific code : following "if" condition - Sunil
            Boolean checkBoxValue = ((Boolean)paymentAcctg.getFieldValue("LineAcctCB1Value"));
            Log.customer.debug("For Accounting %s",i);
            Log.customer.debug("The checkBoxValue is %s",checkBoxValue);
            if(checkBoxValue != null)
            {
                if(checkBoxValue.booleanValue())
                {
                    paymentAcctg.setFieldValue("PaymentAmountPercent",
                                               new BigDecimal(0));
                    continue;
                }
            }

            Log.customer.debug("Before calculating Percentage" );
            BigDecimal pertcentage = (BigDecimal)paymentAcctg.getFieldValue("PaymentAmountPercent");
            if (pertcentage == null)
            {
                pertcentage = new BigDecimal(0);
            }
            Log.customer.debug("Percentage is : %s", pertcentage.intValue());
            Money acctglineAmt = amount.multiply(pertcentage);
            acctglineAmt = (Money.divide(acctglineAmt,
                                         new BigDecimal(100))).round();
            paymentAcctg.setFieldValue("PaymentAmount",acctglineAmt);
            runningTotalAmount.addTo(acctglineAmt);
            lastAcctgItem=paymentAcctg;
        }
        //Take care of the differences caused due to rounding off amounts on acctg lines.
        //BaseObject lastAcctgItem=(BaseObject)paymentAccountings.lastElement();
        if(lastAcctgItem!=null)
        {
            Money diffTotalAcctAmt=Money.subtract(amount,runningTotalAmount);
            Money lastAcctLineAmount=(Money)lastAcctgItem.getFieldValue("PaymentAmount");
            lastAcctLineAmount.addTo(diffTotalAcctAmt);
            lastAcctgItem.setFieldValue("PaymentAmount",lastAcctLineAmount);
        }
    }

    /**
        Return the list of valid value types.
    */
    protected ValueInfo getValueInfo ()
    {
        return valueInfo;
    }

    /**
        Return the parameter names.
    */
    protected ValueInfo[] getParameterInfo ()
    {
        return parameterInfo;
    }

    /**
        Return the required parameter names.
    */
    protected String[] getRequiredParameterNames ()
    {
        return requiredParameterNames;
    }

    public void addError(String s)
    {

    }
}
