package config.java.ams.custom;

import java.util.List;

import ariba.approvable.core.ApprovableClassProperties;
import ariba.approvable.core.CustomApprover;
import ariba.approvable.core.ApprovalRequest;
import ariba.approvable.core.CustomApproverDelegateAdapter;
import ariba.base.core.Base;
import ariba.procure.core.CategoryLineItemDetails;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.util.core.Fmt;
import ariba.util.log.Log;
import ariba.workforce.core.LaborLineItemDetails;

import java.util.Iterator;

public class CollaborationCustomApprover extends CustomApproverDelegateAdapter
{
    public void notifyApprovalRequired(ApprovalRequest approvalRequest, String token, boolean originalSubmission)
    {
        Log.customer.debug("CollaborationCustomApprover::Denying the requisition");
        String lsSpace = " ";
        String lsComment = null;
        Requisition loReq = null;
        if (approvalRequest.getApprovable() instanceof Requisition)
        {
            loReq = (Requisition) approvalRequest.getApprovable();
            ApprovableClassProperties loClassProperties = (ApprovableClassProperties) loReq.getClassProperties();
            Log.customer.debug("CollaborationCustomApprover::notifyApprovalRequired "
                    + loClassProperties.getSubmitHook());
            List loList = loReq.runHook(loClassProperties.getSubmitHook());
            if (((Integer) loList.get(0)).intValue() == -1)
            {
                lsComment = loReq.getUniqueName().trim() + lsSpace
                        + Fmt.Sil(Base.getSession().getLocale(),"aml.Collaboration", "CustomCollaborationError") + (String) loList.get(1)
                        + Fmt.Sil(Base.getSession().getLocale(),"aml.Collaboration", "CustomCollaborationErrorEndMSG");
                Log.customer.debug("CollaborationCustomApprover::notifyApprovalRequired::Error comment " + lsComment);
            }
            else if (((Integer) loList.get(0)).intValue() == 1)
            {
                lsComment = loReq.getUniqueName().trim() + lsSpace
                        + Fmt.Sil(Base.getSession().getLocale(),"aml.Collaboration", "CustomCollaborationWarning") + (String) loList.get(1);
                Log.customer.debug("CollaborationCustomApprover::notifyApprovalRequired::Warning comment " + lsComment);
            }
        }
        List loreqLineItems = loReq.getLineItems();
        for (int i = 0; i < loreqLineItems.size(); i++)
        {
            ReqLineItem loreqLineItem = (ReqLineItem) loreqLineItems.get(i);
            for (Iterator itera = loreqLineItem.getCategoryLineItemDetailsVectorIterator(); itera.hasNext();)
            {
                CategoryLineItemDetails locatLID = (CategoryLineItemDetails) itera.next();
                Log.customer.debug("CollaborationCustomApprover CLID obj: " + locatLID);
                if (locatLID != null
                        && (locatLID instanceof LaborLineItemDetails || locatLID.toString().indexOf(
                                "ConsultingLineItemDetails") >= 0))
                {
                    locatLID.setFieldValue("AutoDenialVerified", "VERIFIED");
                    if (locatLID.getFieldValue("ForceCollaborate") == null)
                    {
                        locatLID.setFieldValue("ForceCollaborate", new Boolean(false));
                    }
                }
            }
        }
        loReq.save();

        CustomApprover.denyAndCommit(approvalRequest.getApprovable().id.toDBString(), approvalRequest.getApprover().id
                .toDBString(), token, lsComment);
    }
}
