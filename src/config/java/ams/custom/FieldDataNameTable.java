/*



    Responsible: imohideen

*/

/* 12/22/2003: Updates for Ariba 8.1 (Jeff Namadan) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/

package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLFunctionCondition;
/* Ariba 8.1: Replaced ariba.common.core.nametable.NamedObjectNameTable) */
import ariba.base.core.aql.AQLNameTable;
/* Ariba 8.1: Replaced ariba.util.core.Util */
import java.util.List;
// Ariba 8.1: We are using Iterator instead of Enumeration now
import java.util.Iterator;
import ariba.util.core.Fmt;
import ariba.base.core.ClusterRoot;
import ariba.util.log.Log;
import ariba.base.fields.ValueSource;
//rgiesen dev SPL #39
import ariba.base.core.Partition;


import java.math.*;


/* Ariba 8.1: Replaced NamedObjectNameTable */
//81->822 changed Vector to List
public class FieldDataNameTable extends AQLNameTable

{
    public static final String ClassName="config.java.ams.custom.FieldDataNameTable";
    String appType = new String();

    public List matchPattern (String field, String pattern)
    {
        List results = super.matchPattern(field, pattern);
        return results;
    }

    public void addQueryConstraints (AQLQuery query, String field, String pattern)

    {
        //select UniqueName from ariba.core.BuysenseFieldDataTable
        //where FieldNumber='Field2' and ClientID = '1234'  and  substring(UniqueName,22,8)  = '2000310A'
        /** We are expecting the the whereClause field be of this format
            hedr.FullyQualifiedField,X,Y;actg.FullyQualifiedField,X,Y;....
            where hedr / line / actg - the component on which the field resides
                  X - offset
                  Y - length
                  for example "hedr.ReqHeadText1Value,0,4;actg.FieldDefault1.UniqueName,25,4"
         */

        String fieldValue=new String();
        String filterString=new String();
        List fieldVector;
        int initPos=0;
        int length=0;
        String substringClause=null;
        String fieldNum = null;
        String clientUniquename = null;
        String clientName = null;


        /* The where clause is on the associated FielNameTable row. This method will find the where clause
        of the associated FielNameTable row and return it */
       //String whereClause= "FieldDefault1.UniqueName,21,4;FieldDefault1.UniqueName,25,4";
        Log.customer.debug("Buysense: Entered the addQueryConstraints");

        ValueSource vs = getValueSourceContext();
        Log.customer.debug("This is vs: %s and the type: %s" ,vs,vs.getTypeName());
        BaseObject bo = (BaseObject)vs ;
        Log.customer.debug("Got this through getValueSource: %s ", bo);
        Log.customer.debug("This is the passed in field in addQueryConstraints: %s", field);
        String chooserField = fieldName();
        String filterField =chooserField+ ".FieldNumber";
        fieldNum = (String)bo.getDottedFieldValue(filterField);
        Log.customer.debug("NameTable is executing on %s. FieldNumber: %s ",chooserField,fieldNum);
        //rgiesen dev SPL #39
        Partition boPartition = bo.getPartition();

        if (vs.getTypeName().equals("ariba.core.PaymentEform")) {
            Log.customer.debug("Working on the Payment Item");
            //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
            //Partitioned User to obtain the extrinsic BuysenseOrg field value. 2/18/2004
	        ariba.user.core.User RequesterUser = (ariba.user.core.User)bo.getDottedFieldValue("Requester");
	        //rgiesen dev SPL #39
	        //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
	        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,boPartition);
            clientUniquename = (String)RequesterPartitionUser.getDottedFieldValue("Requester.BuysenseOrg.ClientName.UniqueName");
            clientName = (String)RequesterPartitionUser.getDottedFieldValue("Requester.BuysenseOrg.ClientName.ClientName");
            appType="PaymentHeader";
           }
        else if (vs.getTypeName().equals("ariba.core.BuysenseOrgEform")){
            Log.customer.debug("Working on BuysenseOrg");
            clientUniquename = (String)bo.getDottedFieldValue("ClientName.UniqueName");
            clientName = (String)bo.getDottedFieldValue("ClientName.ClientName");
            appType="BuysenseOrg";
           }
        else if (vs.getTypeName().equals("ariba.core.PaymentItem")) {
            Log.customer.debug("Working on the Payment Header");
            clientUniquename = (String)bo.getDottedFieldValue("ClientName.UniqueName");
            clientName = (String)bo.getDottedFieldValue("ClientName.ClientName");
            appType="PaymentLine";
           }
        else if (vs.getTypeName().equals("ariba.purchasing.core.Requisition")){
            Log.customer.debug("Working on Requisition");
            //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
            //Partitioned User to obtain the extrinsic BuysenseOrg field value. 2/18/2004
	        ariba.user.core.User RequesterUser = (ariba.user.core.User)bo.getDottedFieldValue("Requester");
	        //rgiesen dev SPL #39
	        //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
	        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,boPartition);
            clientUniquename = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.UniqueName");
            clientName = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName");
            appType="ReqHeader";
         }
        else if (vs.getTypeName().equals("ariba.purchasing.core.ReqLineItem")){
            Log.customer.debug("Working on ReqLineItem");
            Log.customer.debug("From getClusterRoot call: %s ", bo.getClusterRoot());
            clientUniquename = (String)bo.getDottedFieldValue("ClientName.UniqueName");
            clientName = (String)bo.getDottedFieldValue("ClientName.ClientName");
            appType="ReqLine";
        }
        else if (vs.getTypeName().equals("ariba.common.core.SplitAccounting")) {
            Log.customer.debug("Working on SplitAccounting ");
            Log.customer.debug("From getClusterRoot call: %s ", bo.getClusterRoot());
            clientUniquename = (String)bo.getDottedFieldValue("ClientName.UniqueName");
            clientName = (String)bo.getDottedFieldValue("ClientName.ClientName");
            appType="ReqAccounting";
        }
        else if (vs.getTypeName().equals("ariba.core.PaymentAccounting")){
          Log.customer.debug("Working on Accounting ");
          Log.customer.debug("From getClusterRoot call: %s ", bo.getClusterRoot());
          clientUniquename = (String)bo.getDottedFieldValue("ClientName.UniqueName");
          clientName = (String)bo.getDottedFieldValue("ClientName.ClientName");
          if (clientUniquename == null)
                {
                ClusterRoot cr = bo.getClusterRoot();
                //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
                //Partitioned User to obtain the extrinsic BuysenseOrg field value.
                ariba.user.core.User crRequesterUser = (ariba.user.core.User)cr.getDottedFieldValue("Requester");
                //rgiesen dev SPL #39
                //ariba.common.core.User crRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(crRequesterUser,Base.getSession().getPartition());
                ariba.common.core.User crRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(crRequesterUser,boPartition);
                clientUniquename = (String)crRequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.UniqueName");
                clientName = (String)crRequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName");
                }
          appType="PaymentAccounting";
        }
        else  { Log.customer.debug("This NameTable class can not handle the passed in Object");}

       //Test -IM
       if (clientUniquename == null) Log.customer.debug("Got a null client");

       //If we have a null for fieldNum it is because the chooser field
       //is set to null. we have to go through object matching to find out what the field
       //number is.
        if (fieldNum == null){
            List fieldsVector = FieldListContainer.getChooserFields();
            List fieldValuesVector = FieldListContainer.getChooserFieldValues();
            Log.customer.debug("Looking for field %s",chooserField);
            int fieldValuePosition = searchFieldValues(fieldValuesVector,chooserField);
            if (fieldValuePosition == -1 ){
                Log.customer.debug("Got a serious problem. FieldVector and FieldValues vector out of synch");
                return;
            }
            // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
            fieldNum = (String) fieldsVector.get(fieldValuePosition);
        }
        //String whereClause = getWhereClause(fieldNum, clientId);
        //BasObject fieldTable = getFieldTable(clientUniquename,fieldNum);
        //Log.customer.debug("This is the FieldTable object: %s", fieldTable);
        String whereClause = getWhereClause(clientUniquename,fieldNum);
        Log.customer.debug("Got this where clause %s from %s",whereClause,fieldNum);
        try {
        if (whereClause != null) {
          if (whereClause.length() != 0) {
            List tokensVector = BuysenseUtil.tokenize(whereClause);
            // Ariba 8.1: Since List::elements() has been deprecated, we are going to Iterator now
            for (Iterator e = tokensVector.iterator();e.hasNext();) {
                fieldVector = (List)e.next();

                /* Element 1 -> hedr.fieldName / line.fieldName / actg.fieldName
                   Element 2 -> Offset
                   Element 3 -> length  */
                //fieldValue = (String)bo.getDottedFieldValue((String) fieldVector.elementAt(0));
                // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                fieldValue = BuysenseUtil.findFieldValue((String) fieldVector.get(0), bo,appType);
                Log.customer.debug("Field value: %s",fieldValue);
                // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                int offset = (new Integer((String)fieldVector.get(1))).intValue();
                // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                BigDecimal fl = new BigDecimal((String)fieldVector.get(2));
                int fieldLength = fl.intValue();
                fieldValue = fieldValue.substring(offset);
                Log.customer.debug("Field value from offset: %s",fieldValue);
                BigDecimal valueLength = new BigDecimal(fieldValue.length());
                if ((valueLength.compareTo(fl)) < 0){
                    // The fieldValue is less than the designated fieldLength, pad the value with '??'.
                    while (fieldValue.length() < fieldLength) fieldValue+= "?";
                    filterString = filterString + fieldValue;
                }
                else if (valueLength.compareTo(fl) > 0){
                    // The fieldValue is greater than the designated fieldLength, trim the value.
                    filterString = filterString + fieldValue.substring(offset,(offset+fieldLength));
                }
                else{
                    filterString = filterString + fieldValue;
                }
                Log.customer.debug("Buysense: The filter string: %s",filterString);
            }
         substringClause = clientUniquename +":"+ fieldNum +":"+ filterString ;
         initPos  = 0;
         //We want the length before adding the quotes
         length = substringClause.length() ;
         //Now add the quotes
         substringClause = "'"+substringClause+"'";
         //Log.customer.debug("Buysense: This is the client id %s",clientId);
         //Log.customer.debug("Buysense: This is the value of filtering field: %s",val1);
         Log.customer.debug("Buysense: This is the value of fieldNumber: %s",fieldNum);
         Log.customer.debug("Buysense: This is the length of the substring: %s",length);
         Log.customer.debug("Buysense: Final filter substring: %s",substringClause);
         }
       }
        }
        catch (StringIndexOutOfBoundsException e) {
            Log.customer.debug("Got a string out of bound exception. This happens when the whereClause" +
                          "specification and the field value are not in synch. Because of this we will will force" +
                          "any valid choices");

             whereClause = null;
             //To ensure that we find no matches
             fieldNum ="*";
        }
        catch (NullPointerException e) {
            Log.customer.debug("Got a NullPointer Exception. This happens when a source" +
                          "contains null. We wil force no valid choices for this");

             whereClause = null;
             //To ensure that we find no matches
             fieldNum ="*";
        }
       // build the default set of field constraints for the pattern
        super.addQueryConstraints(query, field, pattern);
        beginSystemQueryConstraints(query);
        // add the constraints
        Log.customer.debug("Building conditions for %s and %s",fieldNum,clientName);
        AQLCondition fieldNumberCond =
        AQLCondition.buildEqual(query.buildField("FieldNumber"),
                                    fieldNum);
        AQLCondition clientNameCond =
            AQLCondition.buildEqual(query.buildField("ClientName"),
                                    clientName);
        if (whereClause == null || whereClause.trim().length() == 0 ) {
        Log.customer.debug("Adding just the Fieldnumber and ClientName conditions");
        query.and(fieldNumberCond);
        query.and(clientNameCond);
        }
        else {
            AQLCondition substringCond =  AQLFunctionCondition.parseCondition
                                   (Fmt.S("substring(UniqueName, %s, %s) = %s",new Integer(initPos),new Integer(length),substringClause));
            query.and(substringCond);
        }


       endSystemQueryConstraints(query);
    }

 /* Ariba 8.1: NamedObjectNameTable was deprecated by AQLNameTable.
    This class was changed to derive from AQLNameTable and the
    method constraints() is not in this class and isn't needed.
 public List constraints (String field, String pattern)
    {
        Log.customer.debug("We are in constraints method. field= %s",field);
        List constraints = super.constraints(field, pattern);
        return constraints;
    }
 */
 public AQLQuery  buildQuery(String field, String pattern)
   {
     Log.customer.debug("We are ib buildQuery: field = %s ", field);
     return super.buildQuery(field,pattern);
   }


private String getReqWhereClause(String ClientUniqueName,String fieldNumber)
    {
      BaseObject bo = getFieldTable( ClientUniqueName,fieldNumber);
      String whereClause = (String)bo.getDottedFieldValue("ReqWhereClause");
      Log.customer.debug("Whereclause for field:%s is %s",fieldNumber,whereClause);
      return whereClause;
    }
private String getPayWhereClause(String ClientUniqueName,String fieldNumber)
    {
      BaseObject bo = getFieldTable( ClientUniqueName,fieldNumber);
      String whereClause = (String)bo.getDottedFieldValue("PayWhereClause");
      Log.customer.debug("Whereclause for field:%s is %s",fieldNumber,whereClause);
      return whereClause;
    }
private String getBuysenseOrgWhereClause(String ClientUniqueName,String fieldNumber)
    {
      BaseObject bo = getFieldTable( ClientUniqueName,fieldNumber);
      String whereClause = (String)bo.getDottedFieldValue("WhereClause1");
      Log.customer.debug("Whereclause for field:%s is %s",fieldNumber,whereClause);
      return whereClause;
    }
private BaseObject getFieldTable(String ClientUniqueName,String fieldNumber)
    {
        String fieldTableUN = ClientUniqueName +":"+fieldNumber;
        Log.customer.debug("Looking for : %s",fieldTableUN);
        BaseObject fieldTable = (BaseObject)
        Base.getService().objectMatchingUniqueName( "ariba.core.BuysenseFieldTable",Base.getSession().getPartition(),fieldTableUN);
        Log.customer.debug("Found this object:%s",fieldTable);
        return fieldTable;
    }

private String getWhereClause(String ClientUniqueName,String fieldNumber)
    {
        if ( appType.equals("PaymentHeader") ||appType.equals("PaymentLine") ||
           appType.equals("PaymentAccounting")) {
          return getPayWhereClause(ClientUniqueName,fieldNumber);
        }
        if (appType.equals("BuysenseOrg"))
           return getBuysenseOrgWhereClause(ClientUniqueName,fieldNumber);

        else  return getReqWhereClause(ClientUniqueName,fieldNumber);

    }

private int searchFieldValues(List fieldValuesVector,String chooserField)
  {
    int vectorSize = fieldValuesVector.size();
    for (int i =0; i<vectorSize;i++) {
        // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
        String fieldValue = (String)fieldValuesVector.get(i);
        if (fieldValue.equals(chooserField)) { Log.customer.debug("Found a match"); return i;}
    }
    return -1;
 }
}

