package config.java.ams.custom;

import ariba.pcard.receiving.PCardReceivingTypeRule;
import ariba.purchasing.core.POLineItem;
import ariba.receiving.core.ReceivableLineItem;
import ariba.util.log.Log;

public class BuysensePCardReceivingTypeRule extends PCardReceivingTypeRule
{
	public static final String ClassName = "config.java.ams.BuysensePCardReceivingTypeRule";
	
    public int findReceivingType(ReceivableLineItem lineItem)
    {
    	Log.customer.debug("Class %s entered to findReceivingType method and incoming object %s", ClassName, lineItem);
    	int receivingType;
    	POLineItem loPOLineItem = (POLineItem) lineItem;
    	String lsBuysenseReceivingType = (String) loPOLineItem.getFieldValue("BuysenseReceivingMethod");
    	Log.customer.debug("Class %s got receiving type %", ClassName, lsBuysenseReceivingType);
    	if(lsBuysenseReceivingType != null && lsBuysenseReceivingType.equals("Quantity"))
    	{
    		receivingType = 2;
    	}
    	else if(lsBuysenseReceivingType != null && lsBuysenseReceivingType.equals("Amount"))
    	{
    		receivingType = 3;
    	}
    	else
    	{
    		receivingType = super.findReceivingType(lineItem);
    	}
    	Log.customer.debug("Class %s receiving type encountered %s", ClassName, receivingType);
    	return receivingType;
    }
}