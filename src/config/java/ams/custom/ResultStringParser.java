/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */
package config.java.ams.custom;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;

//Begin Dev SPL#70
import ariba.util.log.Log;
import ariba.util.core.*;
//End Dev SPL#70

//81->822 changed Vector to List
//81->822 changed Hashtable to Map
//81->822 changed Enumeration to Iterator
public class ResultStringParser extends XMLParser
{
    // element tags
    private final String tagRESULTHDR       = "<RESULTHDR";
    private final String tagRESULTHDREnd    = "/RESULTHDR>";
    private final String tagMESSAGESET      = "<MESSAGESET";
    private final String tagMESSAGESETEnd   = "/MESSAGESET>";
    private final String tagACTIONSET       = "<ACTIONSET";
    private final String tagACTIONSETEnd    = "/ACTIONSET>";

    private final String tagMESSAGE         = "<MESSAGE";
    private final String tagMESSAGEEnd      = "/MESSAGE>";
    private final String tagACTION          = "<ACTION";
    private final String tagACTIONEnd       = "/ACTION>";

    private final String tagFIELD           = "<FIELD";
    private final String tagFIELDEnd        = "/>";

    private final String tagHDRACTION      = "<HDRACTION";
    private final String tagHDRACTIONEnd   = "/HDRACTION>";
    private final String tagDTLACTION      = "<DTLACTION";
    private final String tagDTLACTIONEnd   = "/DTLACTION>";
    private final String tagSUBACTION      = "<SUBACTION";
    private final String tagSUBACTIONEnd   = "/SUBACTION>";

    private final String tagACTFLD         = "<ACTFLD";
    private final String tagACTFLDEnd      = "/>";

    // element attribute tags
    private final String tagFIELDNAME       = "FIELDNAME";
    private final String tagFIELDLENGTH     = "FIELDLENGTH";
    private final String tagFIELDOFFSET     = "FIELDOFFSET";
    private final String tagMODIFIER        = "MODIFIER";
    private final String tagLENGTH          = "LENGTH";
    private final String tagCOUNT           = "COUNT";
    private final String tagMAXSEGLEN       = "MAXSEGLEN";

    private final String tagMSGLINES        = "MSGLINES";
    private final String tagACTLINES        = "ACTLINES";
    private final String tagLINEID          = "LINEID";
    private final String tagSUBLINEID       = "SUBLINEID";
    private final String tagPOSITION        = "POSITION";
    private final String tagERPFIELDNAME    = "ERPFIELDNAME";

    private String resultHdrAtts            = new String();
    private String messageSetAtts           = new String();
    private String actionSetAtts            = new String();
    private String messageAtts              = new String();
    private String actionAtts               = new String();

    private int resultHdrLen = 0;
    private int msgMaxSegLen = 99;
    private int actMaxSegLen = 99;
    private int msgLen = 0;
    private int actLen = 0;

    private int msgAreaSize = 0;
    private int actAreaSize = 0;
    private int msgLines = 0;
    private int actLines = 0;

    private Map resultHdrProps        = new HashMap();
    private Map messageSetProps       = new HashMap();
    private Map actionSetProps        = new HashMap();
    private Map messageProps          = new HashMap();
    private Map actionProps           = new HashMap();

    private String hdrActionAtts            = new String();
    private String dtlActionAtts            = new String();
    private String subActionAtts            = new String();

    private int hdrActionCount = 0;
    private int dtlActionCount = 0;
    private int subActionCount = 0;

    private Map hdrActionProps       = new HashMap();
    private Map dtlActionProps       = new HashMap();
    private Map subActionProps       = new HashMap();
    private List hdrActionPropsVector    = ListUtil.list();
    private List dtlActionPropsVector    = ListUtil.list();
    private List subActionPropsVector    = ListUtil.list();

    private List vLines = ListUtil.list();
    private List msgVector = ListUtil.list();
    private List actVector = ListUtil.list();

    public ResultStringParser (String inString) {
        xmlString = inString;
        initialize();
    }

    public ResultStringParser (File inFile) {
        super.init(inFile);
        initialize();
    }

    public void initialize() {
        // initialize xml properties common to all
        String savexml = xmlString;
        xmlString = getSection("<BASELINE>","</BASELINE>");
        // get xml section for common result
        xmlString = getSection("<RESULT>","</RESULT>");
        initRESULTHDRProps();
        initMESSAGESETProps();
        initACTIONSETProps();
        initMESSAGEProps();
        initACTIONProps();
        // get xml section for clientname
        xmlString = savexml;
    }

    public void txnInit (String clientName, String txntype) {
        String savexml = xmlString;
        Log.customer.debug("*** In ResultStringParser::txnInit() ... clientName=" + clientName + ", txntype=" + txntype);
        debug("txnInit:00 - this object ID is: " + this.toString());

        debug("txnInit:01 - xmlString:" + xmlString);
        xmlString = getSection("<"+clientName+">","</"+clientName+">");
        debug("txnInit:02 - did getSection for clientName=" + clientName);

        //Begin Dev SPL#70
        if (xmlString == null)
        {
            //There was no client specific section; this client uses the default section
            Log.customer.debug("*** xmlString is null");
            xmlString=savexml;
            int ndx=clientName.indexOf("_", 0);
            String clientPrfx=clientName.substring(0, ndx);
            Log.customer.debug("*** clientPrfx: %s",clientPrfx);
            xmlString = getSection("<" + clientPrfx + "_COMMONAGENCIES>","</" + clientPrfx + "_COMMONAGENCIES>");
            debug("txnInit:03 - did getSection for clientName=COMMONAGENCIES");
        }
        //End Dev SPL#70

        // get xml section for transaction type within clientname
        xmlString = getSection("<"+txntype+"_ACTION>","</"+txntype+"_ACTION>");
        debug("txnInit:04 - did getSection for " + txntype + "_ACTION");
        Log.customer.debug("*** Got through Dev SPL#70 changes");
        // initialize properties that depend on transaction type
        initHDRACTIONProps();
        initDTLACTIONProps();
        initSUBACTIONProps();
        xmlString = savexml;
    }

    public void initData (String inString) {
            msgLines = Integer.parseInt(getResultHdrValue(tagMSGLINES,inString));
            actLines = Integer.parseInt(getResultHdrValue(tagACTLINES,inString));
            initMsgVector(inString.substring(resultHdrLen));
            initActVector(inString.substring(resultHdrLen + msgAreaSize));
    }

    /** Populates the RESULTHDR Properties
     */
    private void initRESULTHDRProps () {
        resultHdrAtts = getAtts(tagRESULTHDR);
        resultHdrLen = getIntField(resultHdrAtts, tagLENGTH);
        resultHdrProps = getAllElementsHash(getSection(tagRESULTHDR, tagRESULTHDREnd), tagFIELD, tagFIELDEnd, "FIELDNAME");
    }

    /** Populates the MESSAGESET Properties
     */
    private void initMESSAGESETProps () {
        messageSetAtts = getAtts(tagMESSAGESET);
        msgMaxSegLen = getIntField(messageSetAtts, tagMAXSEGLEN);
        messageSetProps = getAllElementsHash(getSection(tagMESSAGESET, tagMESSAGESETEnd), tagFIELD, tagFIELDEnd, "FIELDNAME");
    }

    /** Populates the ACTION Properties
     */
    private void initACTIONSETProps () {
        actionSetAtts = getAtts(tagACTIONSET);
        actMaxSegLen = getIntField(actionSetAtts, tagMAXSEGLEN);
        actionSetProps = getAllElementsHash(getSection(tagACTIONSET, tagACTIONSETEnd), tagFIELD, tagFIELDEnd, "FIELDNAME");
    }

    /** Populates the MESSAGE Properties
     */
    private void initMESSAGEProps () {
        messageAtts = getAtts(tagMESSAGE);
        msgLen = getIntField(messageAtts, tagLENGTH);
        messageProps = getAllElementsHash(getSection(tagMESSAGE, tagMESSAGEEnd), tagFIELD, tagFIELDEnd, "FIELDNAME");
    }

    /** Populates the ACTION Properties
     */
    private void initACTIONProps () {
        actionAtts = getAtts(tagACTION);
        actLen = getIntField(actionAtts, tagLENGTH);
        actionProps = getAllElementsHash(getSection(tagACTION, tagACTIONEnd), tagFIELD, tagFIELDEnd, "FIELDNAME");
    }

    public int getMsgMaxSegLen() {
        return msgMaxSegLen;
    }

    public int getActMaxSegLen() {
        return actMaxSegLen;
    }

    public Map getResultHdrProps() {
        return resultHdrProps;
    }

    public Map getMessageSetProps() {
        return messageSetProps;
    }

    public Map getActionSetProps() {
        return actionSetProps;
    }

    public Map getMessageProps() {
        return messageProps;
    }

    public Map getActionProps() {
        return actionProps;
    }

    public String getResultHdr (String inString) {
        return inString.substring(0, resultHdrLen);
    }

    public void initMsgVector  (String inString) {
        msgVector.clear();
        msgAreaSize = 0;
        //System.out.println("Entered initMsgVector with String: \"" + inString +
        //                 "\", length: " + inString.length());
        int i=1;
        String lineID = new String();
        String subLineID = new String();
        int msgCount = 0;
        int msgBuflen = 0;
        int offset = 0;
        String msgBuffer = new String();
        String expandedBuffer = new String();
        while (i <= msgLines) {
            lineID = getMessageSetValue("LINEID",inString);
            subLineID = getMessageSetValue("SUBLINEID",inString);
            msgCount = Integer.parseInt(getMessageSetValue("MSGCOUNT",inString));

            msgBuflen = Integer.parseInt(getMessageSetValue("MSGBUFLEN",inString));
            msgBuffer = getMessageSetValue("MSGBUFFER",inString).substring(0,msgBuflen);
            offset = getFieldOffset(messageSetProps.get("MSGBUFFER").toString());
            msgAreaSize += msgBuflen + offset;

            expandedBuffer = expand(msgBuffer, msgMaxSegLen);

            for (int j = 0; j < msgCount; j++) {
                String s = expandedBuffer.substring(msgMaxSegLen*j, msgMaxSegLen*(j+1)).trim();
                StringBuffer sb = new StringBuffer();
                setMessageValue("LINEID",lineID,sb);
                setMessageValue("SUBLINEID",subLineID,sb);
                setMessageValue("MSGTEXT",s,sb);
                msgVector.add(sb.toString());
            }
            if (i < msgLines) inString = inString.substring(msgBuflen+offset);
            i++;
        }
    }

    public List getAllMessages () {
        return msgVector;
    }

    public void initActVector (String inString) {
        actVector.clear();
        //System.out.println("Entered initActVector with String: \"" + inString +
        //         "\", length: " + inString.length());
        int i=1;
        String lineID = new String();
        String subLineID = new String();
        int actCount = 0;
        int actBuflen = 0;
        int offset = 0;
        String actBuffer = new String();
        String expandedBuffer = new String();
        while (i <= actLines) {
            lineID = getActionSetValue("LINEID",inString);
            subLineID = getActionSetValue("SUBLINEID",inString);
            actCount = Integer.parseInt(getActionSetValue("ACTCOUNT",inString));

            actBuflen = Integer.parseInt(getActionSetValue("ACTBUFLEN",inString));
            actBuffer = getActionSetValue("ACTBUFFER",inString).substring(0,actBuflen);
            offset = getFieldOffset(actionSetProps.get("ACTBUFFER").toString());
            actAreaSize += actBuflen + offset;

            expandedBuffer = expand(actBuffer, actMaxSegLen);

            loadActFlds(lineID, subLineID, expandedBuffer);
            if (i < actLines) inString = inString.substring(actBuflen+offset);
            i++;
        }
    }

    public void loadActFlds (String lineID, String subLineID, String databuffer) {
        List v;
        String s1, s2, s3;
        int i = 0;
        int count;

        if (Integer.parseInt(lineID) == 0) {
            v = getHdrActionPropsVector();          // header
            count = hdrActionCount;
        }
        else if (Integer.parseInt(subLineID) == 0) {
                v = getDtlActionPropsVector();      // line
                count = dtlActionCount;
            }
        else {
            v = getSubActionPropsVector();          // subline
            count = subActionCount;
        }

        // following line is workaround for test data
        while (databuffer.length() < actMaxSegLen*count) databuffer +=" ";

        for (i = 0; i < v.size();i++) {
            s1 = v.get(i).toString();
            StringBuffer sb = new StringBuffer();
            setActionValue("LINEID",lineID,sb);
            setActionValue("SUBLINEID",subLineID,sb);
            setActionValue("ORMSFIELDNAME",getField(s1,"ORMSFIELDNAME"),sb);
            s2 = databuffer.substring(actMaxSegLen*i, actMaxSegLen*(i+1)).trim();
            s3 = getField(s1,"ORMSFIELDMETHOD");
            if (s2.length() == 0 && s3.trim().equalsIgnoreCase("OBJECT"));  // don't pass back null ref
            else {
                setActionValue("ORMSFIELDVALUE",s2,sb);
                setActionValue("ORMSFIELDMETHOD",s3,sb);
                actVector.add(sb.toString());
            }
        }
    }

    public List getAllActions () {
        return actVector;
    }

    public String getMessage (int msgNumber) {
        if (msgNumber < 1) return null;
        return (String)msgVector.get(msgNumber - 1);
    }

    public String getAction (int actNumber) {
        if (actNumber < 1) return null;
        return (String)actVector.get(actNumber - 1);
    }

    public String getResultHdrValue (String fldName, String sourceString) {
        return getValue(sourceString, resultHdrProps.get(fldName).toString());
    }

    public String getMessageSetValue (String fldName, String sourceString) {
        return getValue(sourceString, messageSetProps.get(fldName).toString());
    }

    public String getActionSetValue (String fldName, String sourceString) {
        return getValue(sourceString, actionSetProps.get(fldName).toString());
    }

    public String getMessageValue (String fldName, String sourceString) {
        return getValue(sourceString, messageProps.get(fldName).toString());
    }

    public String getActionValue (String fldName, String sourceString) {
        return getValue(sourceString, actionProps.get(fldName).toString());
    }

    /** Gets the value corresponding to FIELDNAME in a string
     */
    public String getValue (String sourceString, String xmlelement) {
        String outString = new String();
        int len;
        if (getField(xmlelement, tagFIELDLENGTH).equalsIgnoreCase("VARIABLE"))
            len = -1;
        else
            len = getFieldLength(xmlelement);
        int offset = getFieldOffset(xmlelement);
//      next line pads spaces to field value ... not sure if this is needed at this time
        if (len >= 0) {
            while (sourceString.length() < offset+len) sourceString+= " ";
            outString = sourceString.substring(offset, offset+len);
        }
        else outString = sourceString.substring(offset);
        // DO NOT TRIM!!!
        return outString;
    }

    public void setResultHdrValue (String fldName, String newValue, StringBuffer targetString) {
        setValue(newValue, targetString, resultHdrProps.get(fldName).toString());
    }

    public void setMessageSetValue (String fldName, String newValue, StringBuffer targetString) {
        setValue(newValue, targetString, messageSetProps.get(fldName).toString());
    }

    public void setActionSetValue (String fldName, String newValue, StringBuffer targetString) {
        setValue(newValue, targetString, actionSetProps.get(fldName).toString());
    }

    public void setMessageValue (String fldName, String newValue, StringBuffer targetString) {
        setValue(newValue, targetString, messageProps.get(fldName).toString());
    }

    public void setActionValue (String fldName, String newValue, StringBuffer targetString) {
        setValue(newValue, targetString, actionProps.get(fldName).toString());
    }

    /** Sets the value corresponding to FIELDNAME in a StringBuffer
     */
    public void setValue (String newValue, StringBuffer targetString, String xmlelement) {
        int len = getFieldLength(xmlelement);
        int offset = getFieldOffset(xmlelement);
        String modifier = getModifier(xmlelement);

        if (modifier != null) {
            if (modifier.equals("") || modifier.equals("LJ") || modifier.equals("LJB"))
                while (newValue.length() < len) newValue = " " + newValue;
            if (modifier.equalsIgnoreCase("RJZ"))
                while (newValue.trim().length() < len) newValue = "0" + newValue.trim();
            if (modifier.equalsIgnoreCase("LJZ"))
                while (newValue.trim().length() < len) newValue += "0";
            if (modifier.equalsIgnoreCase("RJ") || modifier.equals("RJB")) {
                newValue = newValue.trim();
                while (newValue.length() < len) newValue = " " + newValue;
            }
        }
        else
            while (newValue.length() < len) newValue+= " ";

        while (targetString.length() < offset+len) targetString.append(" ");
        newValue = newValue.substring(0,len);
// jdk 1.1
        String s = targetString.toString();
        targetString.setLength(0);
        targetString.append(s.substring(0,offset)).append(newValue).append(s.substring(offset+len));
// jdk 1.2 equivalent
//      targetString.replace(offset, offset+len, newValue);
    }

    public String getResultHdrField (String erpFldName, String xmlFldName) {
        return getField(resultHdrProps.get(erpFldName).toString(), xmlFldName);
    }

    public String getMessageField (String erpFldName, String xmlFldName) {
        return getField(messageProps.get(erpFldName).toString(), xmlFldName);
    }

    public String getActionField (String erpFldName, String xmlFldName) {
        return getField(actionProps.get(erpFldName).toString(), xmlFldName);
    }

    public String getFieldName(String section) {
        return getField(section, tagFIELDNAME);
    }

    public int getFieldLength (String section) {
        return getIntField(section, tagFIELDLENGTH);
    }

    public int getFieldOffset (String section) {
        return getIntField(section, tagFIELDOFFSET);
    }

    public String getModifier (String section) {
        return getField(section, tagMODIFIER);
    }

    public String buildResult (List messages, List actions) {
        StringBuffer resultString = new StringBuffer("");
        while (resultString.length() < resultHdrLen) resultString.append(" ");
        StringBuffer sb1 = new StringBuffer();
        StringBuffer sb2 = new StringBuffer();
        int mc = accumMsg(messages,sb1);
        if (mc > 0) resultString = resultString.append(sb1.toString());
        int ac = accumAct(actions,sb2);
        if (ac > 0) resultString = resultString.append(sb2.toString());

        int mlen = Integer.parseInt(getResultHdrField(tagMSGLINES,"FIELDLENGTH"));
        int alen = Integer.parseInt(getResultHdrField(tagACTLINES,"FIELDLENGTH"));
        String mcString = zeroFill(mc,mlen);
        String acString = zeroFill(ac,alen);

        setResultHdrValue(tagMSGLINES, mcString, resultString);
        setResultHdrValue(tagACTLINES, acString, resultString);
        return resultString.toString();
    }

    public boolean resultOverflow (List messages, List actions, int maxSize) {
        boolean overflow = false;
        String x = buildResult (messages, actions);
        if (x.length() > maxSize) overflow = true;
        return overflow;
    }

    public int accumMsg (List inVector, StringBuffer inSB) {
        String prevLineKey = new String();
        String lineKey, lineData;
        String s;
        StringBuffer tempSB = new StringBuffer();
        int count = 0;
        int lineID, subLineID;
        int llen = Integer.parseInt(getMessageField(tagLINEID,"FIELDLENGTH"));
        int slen = Integer.parseInt(getMessageField(tagSUBLINEID,"FIELDLENGTH"));
        int linesWritten = 0;
        for (int i = 0;i < inVector.size(); i++) {
            s = (String)inVector.get(i);
            lineKey = s.substring(0,llen+slen);
            lineData = s.substring(llen+slen);
            if (i != 0) {
                if (!lineKey.equals(prevLineKey)) {
                    lineID = Integer.parseInt(prevLineKey.substring(0,llen));
                    subLineID = Integer.parseInt(prevLineKey.substring(llen,llen+slen));
                    s = buildMsgPfx (lineID, subLineID, count, tempSB.length());
                    inSB.append(s).append(tempSB.toString());
                    linesWritten += 1;
                    tempSB = new StringBuffer();
                    count = 0;
                }
            }
            tempSB.append(compress(lineData,msgMaxSegLen));
            prevLineKey = lineKey;
            count += 1;

            if ((inVector.size() - i) == 1) {       // last line
                lineID = Integer.parseInt(prevLineKey.substring(0,llen));
                subLineID = Integer.parseInt(prevLineKey.substring(llen,llen+slen));
                s = buildMsgPfx (lineID, subLineID, count, tempSB.length());
                inSB.append(s).append(tempSB.toString());
                linesWritten += 1;
            }
        }
        return linesWritten;
    }

    public int accumAct (List inVector, StringBuffer inSB) {
        String prevLineKey = new String();
        String lineKey, lineData;
        String s;
        StringBuffer tempSB = new StringBuffer();
        int count = 0;
        int lineID, subLineID;
        int llen = Integer.parseInt(getActionField(tagLINEID,"FIELDLENGTH"));
        int slen = Integer.parseInt(getActionField(tagSUBLINEID,"FIELDLENGTH"));
        int linesWritten = 0;
        for (int i = 0;i < inVector.size(); i++) {
            s = (String)inVector.get(i);
            lineKey = s.substring(0,llen+slen);
            lineData = s.substring(llen+slen);
            if (i != 0) {
                if (!lineKey.equals(prevLineKey)) {
                    lineID = Integer.parseInt(prevLineKey.substring(0,llen));
                    subLineID = Integer.parseInt(prevLineKey.substring(llen,llen+slen));
                    s = buildActPfx (lineID, subLineID, count, tempSB.length());
                    inSB.append(s).append(tempSB.toString());
                    linesWritten += 1;
                    tempSB = new StringBuffer();
                    count = 0;
                }
            }
            tempSB.append(compress(lineData,actMaxSegLen));
            prevLineKey = lineKey;
            count += 1;

            if ((inVector.size() - i) == 1) {       // last line
                lineID = Integer.parseInt(prevLineKey.substring(0,llen));
                subLineID = Integer.parseInt(prevLineKey.substring(llen,llen+slen));
                s = buildActPfx (lineID, subLineID, count, tempSB.length());
                inSB.append(s).append(tempSB.toString());
                linesWritten += 1;
            }
        }
        return linesWritten;
    }

    public String buildMsgPfx (int lineID, int subLineID, int count, int buflen) {
        StringBuffer sb = new StringBuffer();

        setMessageValue("LINEID",String.valueOf(lineID),sb);
        setMessageValue("SUBLINEID",String.valueOf(subLineID),sb);
        setMessageValue("MSGCOUNT",String.valueOf(count),sb);
        setMessageValue("MSGBUFLEN",String.valueOf(buflen),sb);
        return sb.toString();
    }

    public String buildActPfx (int lineID, int subLineID, int count, int buflen) {
        StringBuffer sb = new StringBuffer();
        setActionValue("LINEID",String.valueOf(lineID),sb);
        setActionValue("SUBLINEID",String.valueOf(subLineID),sb);
        setActionValue("ACTCOUNT",String.valueOf(count),sb);
        setActionValue("ACTBUFLEN",String.valueOf(buflen),sb);
        return sb.toString();
    }

    /** Populates the HDRACTION Properties
     */
    private void initHDRACTIONProps () {
        hdrActionAtts = getAtts(tagHDRACTION);
        hdrActionCount = getIntField(hdrActionAtts, tagCOUNT);
        hdrActionProps.clear();
        hdrActionProps = getAllElementsHash(getSection(tagHDRACTION, tagHDRACTIONEnd), tagACTFLD, tagACTFLDEnd, tagERPFIELDNAME);
        hdrActionPropsVector = getActPropsAsVector(hdrActionProps);
    }

    /** Populates the DTLACTION Properties
     */
    private void initDTLACTIONProps () {
        dtlActionAtts = getAtts(tagDTLACTION);
        dtlActionCount = getIntField(dtlActionAtts, tagCOUNT);
        dtlActionProps.clear();
        dtlActionProps = getAllElementsHash(getSection(tagDTLACTION, tagDTLACTIONEnd), tagACTFLD, tagACTFLDEnd, tagERPFIELDNAME);
        dtlActionPropsVector = getActPropsAsVector(dtlActionProps);
    }

    /** Populates the SUBACTION Properties
     */
    private void initSUBACTIONProps () {
        subActionAtts = getAtts(tagSUBACTION);
        subActionCount = getIntField(subActionAtts, tagCOUNT);
        subActionProps.clear();
        subActionProps = getAllElementsHash(getSection(tagSUBACTION, tagSUBACTIONEnd), tagACTFLD, tagACTFLDEnd, tagERPFIELDNAME);
        subActionPropsVector = getActPropsAsVector(subActionProps);
    }

    public List getActPropsAsVector (Map h) {
        // move section properties to vector ordered by position
        List v = ListUtil.list();
        for (int i = 0;i < h.size();i++) v.add("");

        for (Iterator ee = h.keySet().iterator();ee.hasNext();) {
            String s = ee.next().toString();
            s = h.get(s).toString();
            int i = getIntField(s,"POSITION");
            v.set(i-1,s);
        }
        return v;
    }

    public Map getHdrActionProps() {
        return hdrActionProps;
    }

    public Map getDtlActionProps() {
        return dtlActionProps;
    }

    public Map getSubActionProps() {
        return subActionProps;
    }

    public List getHdrActionPropsVector() {
        return hdrActionPropsVector;
    }

    public List getDtlActionPropsVector() {
        return dtlActionPropsVector;
    }

    public List getSubActionPropsVector() {
        return subActionPropsVector;
    }

    public String getHdrActionAtts() {
        return hdrActionAtts;
    }

    public String getDtlActionAtts() {
        return dtlActionAtts;
    }

    public String getSubActionAtts() {
        return subActionAtts;
    }

    public String getHdrActionValue (String fldName, String sourceString) {
        return getActValue(sourceString, hdrActionProps.get(fldName).toString());
    }

    public String getDtlActionValue (String fldName, String sourceString) {
        return getActValue(sourceString, dtlActionProps.get(fldName).toString());
    }

    public String getSubActionValue (String fldName, String sourceString) {
        return getActValue(sourceString, subActionProps.get(fldName).toString());
    }

    /** Gets the value corresponding to ERPFIELDNAME in a string
     */
    public String getActValue (String sourceString, String xmlelement) {
        String outString = new String();
        int len = actMaxSegLen;
        int offset = len*(getIntField(xmlelement, tagPOSITION)-1);
//      next line pads spaces to field value ... not sure if this is needed at this time
        while (sourceString.length() < offset+len) sourceString+= " ";
        outString = sourceString.substring(offset, offset+len);
        // DO NOT TRIM!!!
        return outString;
    }

    public StringBuffer initHdrActionBuffer () {
        StringBuffer buffer = new StringBuffer();
        while (buffer.length() < actMaxSegLen*hdrActionCount) buffer.append(" ");
        return buffer;
    }

    public StringBuffer initDtlActionBuffer () {
        StringBuffer buffer = new StringBuffer();
        while (buffer.length() < actMaxSegLen*dtlActionCount) buffer.append(" ");
        return buffer;
    }

    public StringBuffer initSubActionBuffer () {
        StringBuffer buffer = new StringBuffer();
        while (buffer.length() < actMaxSegLen*subActionCount) buffer.append(" ");
        return buffer;
    }

    public void setHdrActionValue (String fldName, String newValue, StringBuffer targetString) {
        if (hdrActionCount > 0) {
            setActValue(newValue, targetString, hdrActionProps.get(fldName).toString());
        }
    }

    public void setDtlActionValue (String fldName, String newValue, StringBuffer targetString) {
        if (dtlActionCount > 0) {
            setActValue(newValue, targetString, dtlActionProps.get(fldName).toString());
        }
    }

    public void setSubActionValue (String fldName, String newValue, StringBuffer targetString) {
        if (subActionCount > 0) {
            setActValue(newValue, targetString, subActionProps.get(fldName).toString());
        }
    }

    /** Sets the value corresponding to ERPFIELDNAME in a StringBuffer
     */
    public void setActValue (String newValue, StringBuffer targetString, String xmlelement) {
        int len = actMaxSegLen;
        int offset = len*(getIntField(xmlelement, tagPOSITION)-1);
        while (newValue.length() < len) newValue+= " ";
        newValue = newValue.substring(0,len);
// jdk 1.1
        String s = targetString.toString();
        targetString.setLength(0);
        targetString.append(s.substring(0,offset)).append(newValue).append(s.substring(offset+len));
// jdk 1.2 equivalent
//      targetString.replace(offset, offset+len, newValue);
    }

    public String getHdrActionField (String erpFldName, String xmlFldName) {
        return getField(hdrActionProps.get(erpFldName).toString(), xmlFldName);
    }

    public String getDtlActionField (String erpFldName, String xmlFldName) {
        return getField(dtlActionProps.get(erpFldName).toString(), xmlFldName);
    }

    public String getSubActionField (String erpFldName, String xmlFldName) {
        return getField(subActionProps.get(erpFldName).toString(), xmlFldName);
    }

    public String compress (String inString, int maxSegLen) {
        StringBuffer result = new StringBuffer("");
        String s1, s2;
        int i;
        while (inString.length()%maxSegLen > 0) inString += " ";    // make sure multiple of seglen
        int j = inString.length()/maxSegLen;                        // nbr of fields in buffer
        int k = String.valueOf(maxSegLen).length();

        for (i = 0; i < j; i++) {
            s1 = inString.substring(i*maxSegLen, (i+1)*maxSegLen).trim();
            s2 = zeroFill(s1.length(),k);
            result.append(s2+s1);
        }

        return result.toString();
    }

    public String expand (String inString, int maxSegLen) {
        StringBuffer result = new StringBuffer("");
        int i = 0;
        int j = 0;
        String s1;
        int k = String.valueOf(maxSegLen).length();

        while (i < inString.length()) {
            j += k + Integer.parseInt(inString.substring(i,i+k));
            i += k;
            s1 = inString.substring(i,j);
            while (s1.length() < maxSegLen) s1 += " ";
            i = j;
            result.append(s1);
        }

        return result.toString();
    }

    public String zeroFill (int num, int len) {
        String result = String.valueOf(num);
        while (result.length() < len) result = "0" + result;
        return result;
    }
   
    public void debug(String fsMsg)
    {
        Log.customer.debug("ResultStringParser: " + fsMsg);
    }
}


