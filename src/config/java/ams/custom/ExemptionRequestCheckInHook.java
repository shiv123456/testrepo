package config.java.ams.custom;

import java.util.List;

import ariba.admin.core.Log;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableHook;
import ariba.base.core.Base;
import ariba.util.core.Constants;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;

public class ExemptionRequestCheckInHook implements ApprovableHook
{
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));
    private static         String        errorStringTable            = "buysense.eform";
    private static         String        eVAExemptionReqValidationError  = "TypeOfExemptionRequestError";
    private static         String        eVAExemptionReqValidation  = "ExemptionRequestError";
    
    public List run(Approvable approvable)
    {
        Log.customer.debug("Inside ExemptionRequestCheckInHook "+approvable);
        String lsProposedProcurement = null;
        //Boolean lbVCERelease = (approvable.getFieldValue("VCERelease") == null)? false:(Boolean) approvable.getFieldValue("VCERelease");
        Boolean lbContractExemption = (approvable.getFieldValue("ContractExemption") == null)? false:(Boolean) approvable.getFieldValue("ContractExemption");
        Boolean lbExceedDelegatedAuthority = (approvable.getFieldValue("ExceedDelegatedAuthority") == null)? false:(Boolean) approvable.getFieldValue("ExceedDelegatedAuthority");
        Boolean lbCooperativeContractCB = (approvable.getFieldValue("CooperativeContractCB") == null)? false:(Boolean) approvable.getFieldValue("CooperativeContractCB");
        Boolean lbOther = (approvable.getFieldValue("Other") == null)? false:(Boolean) approvable.getFieldValue("Other");
      Boolean lbCooperativeContractCB1 = (approvable.getFieldValue("CooperativeContractCB1") == null)? false:(Boolean) approvable.getFieldValue("CooperativeContractCB1");
      Boolean lbExceedDelegatedAuthority1 = (approvable.getFieldValue("ExceedDelegatedAuthority1") == null)? false:(Boolean) approvable.getFieldValue("ExceedDelegatedAuthority1");
      Boolean lbContractModification1 = (approvable.getFieldValue("ContractModification1") == null)? false:(Boolean) approvable.getFieldValue("ContractModification1");
      Boolean lbFederalGrant = (approvable.getFieldValue("FederalGrant") == null)? false:(Boolean) approvable.getFieldValue("FederalGrant");
      Boolean lbStateGrant = (approvable.getFieldValue("StateGrant") == null)? false:(Boolean) approvable.getFieldValue("StateGrant");
      Boolean lbOtherField = (approvable.getFieldValue("OtherField") == null)? false:(Boolean) approvable.getFieldValue("OtherField");
        
      if(lbContractExemption.booleanValue())
      {
    	  lsProposedProcurement = "DPS State Contract";
      }
      if(lbExceedDelegatedAuthority.booleanValue())
      {
    	  lsProposedProcurement = "VIB";
      }
      if(lbCooperativeContractCB.booleanValue())
      {
    	  lsProposedProcurement = "VDC";
      }
      if(lbOther.booleanValue())
      {
    	  lsProposedProcurement = "OGC";
      }
      if(lbCooperativeContractCB1.booleanValue())
      {
    	  lsProposedProcurement = "CNTRT_EX";
      }
      if(lbExceedDelegatedAuthority1.booleanValue())
      {
    	  lsProposedProcurement = "EXCEEDS_DA";
      }
      if(lbContractModification1.booleanValue())
      {
    	  lsProposedProcurement = "CNTRT_MOD";
      }
      if(lbFederalGrant.booleanValue())
      {
    	  lsProposedProcurement = "FED_GRT";
      }
      if(lbStateGrant.booleanValue())
      {
    	  lsProposedProcurement = "ST_GRT";
      }
      if(lbOtherField.booleanValue())
      {
    	  lsProposedProcurement = "OTHER EX";
      }
      
      
        setCustomTitle(approvable, lsProposedProcurement);
        
        
        if((lbContractExemption == false)&&(lbExceedDelegatedAuthority == false)&&(lbCooperativeContractCB == false) && (lbStateGrant == false) && (lbOtherField == false) && (lbFederalGrant == false) && (lbContractModification1 == false) && (lbOther == false) && (lbExceedDelegatedAuthority1 == false) && (lbCooperativeContractCB1 == false))
        {     
            Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : None of the checkbox have been set");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidationError));
        }       
        
        if(lbContractExemption == true && lbExceedDelegatedAuthority == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbContractExemption == true && lbCooperativeContractCB == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbContractExemption == true && lbOther == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }

        if(lbExceedDelegatedAuthority == true && lbContractExemption == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        if(lbExceedDelegatedAuthority == true && lbCooperativeContractCB == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        if(lbExceedDelegatedAuthority == true && lbOther == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 

        if(lbCooperativeContractCB == true && lbContractExemption == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        if(lbCooperativeContractCB == true && lbExceedDelegatedAuthority == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        if(lbCooperativeContractCB == true && lbOther == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        
        if(lbOther == true && lbContractExemption == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        if(lbOther == true && lbExceedDelegatedAuthority == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        if(lbOther == true && lbCooperativeContractCB == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
       
        if(lbCooperativeContractCB1 == true && lbExceedDelegatedAuthority1 == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbCooperativeContractCB1 == true && lbContractModification1 == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbCooperativeContractCB1 == true && lbFederalGrant == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }   
        if(lbCooperativeContractCB1 == true && lbStateGrant == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        if(lbCooperativeContractCB1 == true && lbOtherField == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
         
        if(lbExceedDelegatedAuthority1 == true && lbCooperativeContractCB1 == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbExceedDelegatedAuthority1 == true && lbFederalGrant == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbExceedDelegatedAuthority1 == true && lbStateGrant == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }   
        if(lbExceedDelegatedAuthority1 == true && lbContractModification1 == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        if(lbExceedDelegatedAuthority1 == true && lbOtherField == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
 
        if(lbContractModification1 == true && lbCooperativeContractCB1 == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbContractModification1 == true && lbExceedDelegatedAuthority1 == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbContractModification1 == true && lbFederalGrant == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }   
        if(lbContractModification1 == true && lbStateGrant == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        if(lbContractModification1 == true && lbOtherField == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }        

        if(lbFederalGrant == true && lbCooperativeContractCB1 == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbFederalGrant == true && lbExceedDelegatedAuthority1 == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbFederalGrant == true && lbContractModification1 == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }   
        if(lbFederalGrant == true && lbStateGrant == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        if(lbFederalGrant == true && lbOtherField == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }        
        
        if(lbOtherField == true && lbCooperativeContractCB1 == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbOtherField == true && lbExceedDelegatedAuthority1 == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbOtherField == true && lbContractModification1 == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }   
        if(lbOtherField == true && lbStateGrant == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        if(lbOtherField == true && lbFederalGrant == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }  
        
        if(lbCooperativeContractCB1 == true && lbContractExemption == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbCooperativeContractCB1 == true && lbExceedDelegatedAuthority == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbCooperativeContractCB1 == true && lbCooperativeContractCB == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }   
        if(lbCooperativeContractCB1 == true && lbOther == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
       
        if(lbExceedDelegatedAuthority1 == true && lbContractExemption == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbExceedDelegatedAuthority1 == true && lbExceedDelegatedAuthority == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbExceedDelegatedAuthority1 == true && lbCooperativeContractCB == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }   
        if(lbExceedDelegatedAuthority1 == true && lbOther == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
    
        if(lbContractModification1 == true && lbContractExemption == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbContractModification1 == true && lbExceedDelegatedAuthority == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbContractModification1 == true && lbCooperativeContractCB == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }   
        if(lbContractModification1 == true && lbOther == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        
        if(lbFederalGrant == true && lbContractExemption == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbFederalGrant == true && lbExceedDelegatedAuthority == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbFederalGrant == true && lbCooperativeContractCB == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }   
        if(lbFederalGrant == true && lbOther == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        
        if(lbStateGrant == true && lbContractExemption == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbStateGrant == true && lbExceedDelegatedAuthority == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbStateGrant == true && lbCooperativeContractCB == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }   
        if(lbStateGrant == true && lbOther == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        
        if(lbOtherField == true && lbContractExemption == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbOtherField == true && lbExceedDelegatedAuthority == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbOtherField == true && lbCooperativeContractCB == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }   
        if(lbOtherField == true && lbOther == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        
        return NoErrorResult;
        
     }
    public void setCustomTitle(Approvable approvable, String proposedProcurement)
    {
        ariba.user.core.User loRequesterUser = (ariba.user.core.User)approvable.getRequester();
        if(loRequesterUser != null && proposedProcurement !=null)
        {
        	ariba.common.core.User loRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(loRequesterUser,approvable.getPartition());
        	String lsName = (String) approvable.getFieldValue("Name");
        	String lsClientName = (String)loRequesterPartitionUser.getDottedFieldValue("ClientName.ClientName");
        	if(lsClientName != null && lsName !=null)
        	{
        		approvable.setName(lsClientName + " - " +proposedProcurement+ " - " +lsName);
        	}
        }
    }
}


