/*
    Copyright (c) 1996-1999 Ariba, Inc.
    All rights reserved. Patents pending.

   Falahyar March 2000
 */

/*
 rlee, 10/14/2004: UAT SPL 26 and 36 - Errors from PreEncumbrance logic not handling null values from Ariba 7.1

 Rob Giesen, February 2001 - Added the BuysenseRulesEngine for
 eVA ST SPL # 119

 */

/* 09/03/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */
/* rlee July 2004: XML Integration Interface - PreEncumbrance
 * Approver with EditApprovable who comes after the Custom Approver want to edit this requisition.
 * When approver finished editing and tries to exit, this AMSReqCheckinHook will be called.
 * In reference to the XML Integration effort, a Cancel will be sent to the ERP system for the reason that
 * this requisition has been changed.
 */

package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.core.Partition; // import ariba.common.base.Variant;
import ariba.approvable.core.Approvable;
import ariba.common.core.Core; // Ariba 8.0: Replaced deprecated ApprovableHook
import ariba.approvable.core.ApprovableHook; // import ariba.approvable.core.ApprovableHook;
import ariba.purchasing.core.Requisition;
import ariba.util.log.Log; // import ariba.util.core.Util;
import java.util.List;
import config.java.ams.custom.BuysenseRulesEngine; // Ariba 8.0: Deprecations, methods have changed packages.
import config.java.ams.custom.HookEdits.eVA_AMSReqSubmitHook;
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;
import ariba.common.core.*;
import ariba.util.core.*;
import ariba.util.formatter.BooleanFormatter;

// 81->822 changed Vector to List
public class AMSReqCheckinHook implements ApprovableHook, BuyintConstants
{
    // Ariba 8.0: Replaced deprecated method
    private static final List NoErrorResult = ListUtil.list(Constants.getInteger(0));
    String                    b1;
    // Ariba 8.1: List::List() is deprecated by ListUtil::newVector()
    private List              warningVector = ListUtil.list();
    public List               retVector;
    public List               valVector;

    public List run(Approvable approvable)
    {
        Log.customer.debug("AMSReqCheckinHook.java called");
        
        //SRINI: Added for CSPL-7067 - To align with Mass Edit Functionality
        AMSReqSubmitHook.alignMassEditFunctionality((Requisition) approvable);
        //SRINI: Ended for CSPL-7067 - To align with Mass Edit Functionality
        
        /*
         * CSPL-2409, Pavan Aluri, 24-Jan-2011 Explanation: validate for expired/inactive lines
         */

        retVector = AMSReqSubmitHook.isValidCatalogLine(approvable);
        if (((Integer) retVector.get(0)).intValue() == -1)
        {
            Log.customer.debug("AMSReqCheckinHook:: isValidCatalogLine error ");
            return retVector;
        }

        // Validate Supplier/Location
        AMSReqSubmitHook loSubmitHook = new AMSReqSubmitHook();
        retVector = loSubmitHook.performSSLValidations(approvable);
        if (((Integer) retVector.get(0)).intValue() == -1)
        {
            Log.customer.debug("AMSReqCheckinHook:: performSSLValidations error ");
            return retVector;
        }

        // Rob Giesen, February 2001
        // Call the Business Rule engine to validate all chooser field selections
        // if we get a -1 that means we failed the validation. so we return without proceeding
        retVector = BuysenseRulesEngine.runReqFilteringRules(approvable);
        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
        if (((Integer) retVector.get(0)).intValue() == -1)
        {
            Log.customer.debug("AMSReqCheckinHook:: runReqFilteringRules error ");
            return retVector;
        }

        // Joe Perry, July 2001
        // Call the Validation Rule engine to validate all chooser field selections
        // if we get a -1 that means we failed the validation. so we return without proceeding
        BuysenseValidationEngine eVAValEng = new BuysenseValidationEngine();

        valVector = eVAValEng.runReqHeaderValidation(approvable);
        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
        if (((Integer) valVector.get(0)).intValue() == -1)
        {
            Log.customer.debug("AMSReqCheckinHook:: runReqHeaderValidation error ");
            return valVector;
        }

        /* BEGIN CSPL-5525 'BuysenseOrderCOnfirm' check JB */
        valVector = AMSReqSubmitHook.checkBuysenseOrderConfirm(approvable);
        int errorCodeOC = ((Integer) valVector.get(0)).intValue();
        if (errorCodeOC < 0)
        {
            Log.customer.debug("eVA_AMSReqCheckinHook:: AMSReqSubmitHook.checkBuysenseOrderConfirm logic Failed with error: " + valVector.get(1));
            Log.customer.debug("AMSReqCheckinHook:: checkBuysenseOrderConfirm error ");
            return valVector;
        }
        /* END CSPL-5525 'BuysenseOrderCOnfirm' check JB JBBB */

        // Set the changed flag on the requisition to be TRUE at this point.
        Approvable loReq = (Approvable) Base.getSession().objectForWrite(approvable.getBaseId());
        loReq.setFieldValue("Changed", new Boolean(true));

        Log.customer.debug("Set the Changed Flag to be : %s", approvable.getFieldValue("Changed"));
        loReq.save();
        // End of Rob Giesen, February 2001

        // Anup - Generic invocation of dynamically determined Client/Agency edits. - Dev SPL #173
        List result = EditClassGenerator.invokeHookEdits(approvable, this.getClass().getName());
        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
        int errorCodeFromHookEdits = ((Integer) result.get(0)).intValue();
        Log.customer.debug("Validations in " + this.getClass().getName() + ", error Code --> %s", errorCodeFromHookEdits);
        if (errorCodeFromHookEdits < 0)
        {
            Log.customer.debug("Validations Failed in " + this.getClass().getName() + " - returning error to UI");
            Log.customer.debug("AMSReqCheckinHook:: invokeHookEdits error");
            return result;
        }
        // errorCodeFromHookEdits is being used at the bottom afterwards. So don't assigne values yet

        // Do integration checks
        /*
         * If CheckinHook return no error and no warning, then check PreEncumbrance Integration
         * 
         * rlee : Send Cancel before leaving AMSCheckinHook.java check if Req has been preencumbered, if yes, send cancel req to ERP system
         * 
         */
        String lsPECheck = (String) loReq.getDottedFieldValue("PreEncumbranceStatus");
        Boolean lbPreEncumbered = (Boolean) loReq.getFieldValue("PreEncumbered");

        if (StringUtil.nullOrEmptyOrBlankString(lsPECheck))
        {
            loReq.setFieldValue("PreEncumbranceStatus", STATUS_ERP_READY);
            lsPECheck = STATUS_ERP_READY;
        }

        if (StringUtil.nullOrEmptyOrBlankString(BooleanFormatter.getStringValue(lbPreEncumbered)))
        {
            loReq.setFieldValue("PreEncumbered", new Boolean(false));
            lbPreEncumbered = new Boolean(false);
        }
        if ((lsPECheck.equals(STATUS_ERP_APPROVE)) && (lbPreEncumbered.booleanValue()))
        {
            // Call Simha BuysenseXMLFactory.push (loReq, Cancel);
            BuyintXMLFactory.submit(TXNTYPE_PREENC_CANCEL, loReq);

            // Update History Tab and PreEncumbranceStatus
            BaseObject loReqObject = (BaseObject) loReq;
            String lsReqUN = (String) loReq.getDottedFieldValue("UniqueName");
            String lsRecordType = "PreEncCRecord";
            // String lsMessage = Fmt.Sil("ariba.procure.core","PreEncEditMessage");
            String lsMessage = Fmt.Sil(Base.getSession().getLocale(), "ariba.procure.core", "PreEncEditMessage");
            Partition loPartition = loReq.getPartition();
            User loAribaSystemUser = Core.getService().getAribaSystemUser(loPartition);

            BuysenseUtil.createHistory(loReqObject, lsReqUN, lsRecordType, lsMessage, loAribaSystemUser);
            loReq.setDottedFieldValue("PreEncumbranceStatus", STATUS_ERP_CANCELLED);
            loReq.setDottedFieldValue("PreEncumbered", new Boolean(false));
        }
        else if ((lsPECheck.equals(STATUS_ERP_INPROGRESS)))
        {
            // waiting for ERP system's reply; this Req cannot be edit.
            Log.customer.debug("ReqIntTxn inprogress; Editing is not allowed. ");
            return ListUtil.list(Constants.getInteger(-1), "This Requisition is pending ERP response therefore cannot be edited at this time.");
        }
        else
        {
            Log.customer.debug("rlee Req has been sent to ERP; okay to edit Req.");
        }

        
        if(eVA_AMSReqSubmitHook.isSupplierChanged(approvable) && eVA_AMSReqSubmitHook.urlSupplierLocation(approvable))
        {
        	Log.customer.debug("AMSReqCheckinHook:In the lburlSupplierLocation ");
       	 return ListUtil.list(Constants.getInteger(1), "Supplier changed");
        }
        // Capture Warnings here if present
        if (errorCodeFromHookEdits > 0)
        {
            Log.customer.debug("Validations encountered a Warning in " + this.getClass().getName() + " - returning error to UI");
            // 81->822 warningVector = (Vector)result.clone();
            warningVector = ListUtil.cloneList(result);
            Log.customer.debug("AMSReqCheckinHook:: invokeHookEdits warnings");
            return warningVector;
        }

        Log.customer.debug("AMSReqCheckinHook:: finished, returning clean");
        return NoErrorResult;
    }
}
