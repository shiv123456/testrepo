package config.java.ams.custom;


import java.util.Iterator;
import java.util.Map;
import java.io.File;
import java.io.FileInputStream;
import java.io.ByteArrayOutputStream;
import ariba.base.core.BaseObject;
//import ariba.common.util.*;
//import ariba.util.core.Util;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import ariba.util.log.Log;
import ariba.util.core.*;
import ariba.util.formatter.BigDecimalFormatter;

/*
rgiesen 02/19/2004 Dev SPL #19 - Cleaned up file for Ariba 8.1 Integration
*/
/* 03/02/2004: Globally change Log.util.debug to Log.customer.debug (Rob Giesen) */
//81->822 changed Vector to List
//81->822 changed Hashtable to Map
//81->822 changed Enumeration to Iterator

class XMLMappingException extends Throwable
{
    XMLMappingException(String s)
    {
        Log.customer.debug("XMLMappingException: %s ", s);
    }
}

class XMLORMSFieldNotFoundException extends Throwable
{
    XMLORMSFieldNotFoundException (String ORMSFIELDNAME)
    {
        Log.customer.debug("XMLORMSFieldNotFoundException: %s", ORMSFIELDNAME);
    }
}

class XMLOffsetLengthProblemException extends Throwable
{
    XMLOffsetLengthProblemException ()
    {
        Log.customer.debug("XMLOffsetLengthProblemException");

    }
}

class XMLRequiredTagException extends Throwable
{
    XMLRequiredTagException (String tagName)
    {
        Log.customer.debug("XMLRequiredTagException: %s",tagName);
    }
}

class NoProblemException extends Throwable
{
    NoProblemException()
    {
    }
}

public class AMSTXNStringFactory{

    // flags - turn on if you want these checks to be performed
    boolean lengthAndOffsetChecking = false; // if true, checks length and offset in xml files at run time. gives an error if anything is wrong
    boolean checkIfFieldsExist = false; // if true, checks if the fields ORMSFIELDNAME exist in clusterroot. gives an error if not

    //Begin Dev SPL#70
    //This Map contains all the xmls.
    private Map XMLs =null;
    //End Dev SPL#70

    private final String KEY="KEY";
    private final String KEYFIELD="FIELD";
    private final String BATCH="BATCH";
    private final String BATCHFIELD="FIELD";
    private final String HEADER="HEADER";
    private final String HEADERDATA="FIELD";
    private final String DETAIL="DETAIL";
    private final String DETAILDATA="LINE";
    private final String SUBDETAIL="SUBDETAIL";
    private final String SUBDETAILDATA="SUBLINE";
    private FileInputStream in=null;
    private ByteArrayOutputStream out=null;
    // if this field is set to true, generateDataString throws a XMLMappingException at the end of processing
    private boolean errorsOccurred = false;

     // this field is used to display offset problems only one at a time
    private boolean offsetLengthProblem = false;

    public AMSTXNStringFactory()
    {
        //Begin Dev SPL#70
        //Ariba 8.1 Replace constructor for Hash table
        //XMLs=new Map();
        XMLs=MapUtil.map();
        //End Dev SPL#70
    }

    //Begin Dev SPL#70 Added hashkey as a parameter to this method End Dev SPL#70
    private void readXMLFile( File xmlFileName, String hashKey)
    {
        String fileBuffer = "";
        try
        {
            System.out.println("IXM want to know the file name: " + xmlFileName.getName());
            in=new FileInputStream(xmlFileName);
            byte[] buffer=new byte[4096];
            out = new ByteArrayOutputStream();

            int bytes_read;
            while ((bytes_read = in.read(buffer)) != -1)
            {
                out.write(buffer, 0, bytes_read);
                //String(byte[] bytes, int offset, int length) method can also be used
                fileBuffer  = out.toString();
            }

            //Begin Dev SPL#70
            //Log.customer.debug("*** hashKey from readXMLFile: %s ", hashKey);
            XMLs.put(hashKey, fileBuffer);
            //Log.customer.debug("*** hashtable from initialize: %s ", XMLs.toString());
            //End Dev SPL#70
        }
        catch (Exception e )
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                in.close();
                out.close();
            }
            catch (Exception e )
            {
                e.printStackTrace();
            }
        }
    }


    public void initialize(String XMLfilepath) {
        try
        {
           System.out.println("IXm this is the file path :" + XMLfilepath);

            // create fiel Obj
            File fileObj = new File(XMLfilepath);

            // retrieve
            String[] fileNames = fileObj.list();

            boolean filesFound = false;
            for (int i=0;i<fileNames.length;i++)
            {
                if (fileNames[i].endsWith(".xml") || fileNames[i].endsWith(".XML"))
                {
                    //Begin Dev SPL#70
                    int index = fileNames[i].indexOf(".");
                    String hashKey = fileNames[i].substring(0, index);
                    hashKey = hashKey.toUpperCase().trim();
                    readXMLFile(new File(fileObj, fileNames[i]), hashKey);
                    //End Dev SPL#70
                    filesFound = true;
                }
            }

            if (!filesFound)
            {
                Log.customer.debug("Problem in AMSTXNStringFactory:initialize: No XML files found in %s", XMLfilepath);
                System.exit(0);
            }

        }
        catch(Exception e)
        {
            Log.customer.debug("Problem in AMSTXNStringFactory.initialize: Invalid path of XML files %s", XMLfilepath );
            System.exit(0);
        }
    }


    public String generateDataString(String XMLsection, String clientName, String txnType, BaseObject txn) throws XMLMappingException
    {
        //Begin Dev SPL#70
        Log.customer.debug("In generateDataString: "+txn.toString());
        String generateddatastring=null;
        //End Dev SPL#70

        errorsOccurred = false;

        // this field is used to display offset problems only one at a time
        offsetLengthProblem = false;

        //Begin Dev SPL#70
        int transIndex1, transIndex2;
        String hashKey=clientName.toUpperCase() + txnType.toUpperCase();

        // BEGIN Workaround for eVA 1.6 ER #6:
        /* For BSORG update transactions, detail lines could pass one of 23 different
           txnType values("BO01" thru "BO23") to this method. However, all these
           types are processed against the same XML file, identified by the "BSO" txnType.
           Therefore, to build the hashKey, use "BSO", not the passed value.
        */
        if (txnType.startsWith("BO")) {
            hashKey=clientName.toUpperCase() + "BSO";
        }
        // END Workaround for eVA 1.6 ER #6:

        //Log.customer.debug("*** hashKey generateDataString(): %s ", hashKey);
        String xml=(String)XMLs.get(hashKey);
        //Log.customer.debug("*** xml from generateDataString(): %s ", xml);

        if (xml==null)
        {
            Log.customer.debug("*** There's no client xml so use default");
            //There's no client specific xml. Need to use the default xml.
            int ndx=clientName.indexOf("_", 0);
            String clientPrfx=clientName.substring(0, ndx);
            Log.customer.debug("*** clientPrfx from generateDataString(): %s ", clientPrfx);
            xml=(String)XMLs.get(clientPrfx + "_COMMONAGENCIES" + txnType.toUpperCase());
            if(xml==null)
            {
                Logs.buysense.debug("AMSTXNStringFactory - Common Agency XML file must exist.");
                throw new XMLMappingException("XML Error: A " + clientPrfx + "_COMMONAGENCIES.xml must exist for each transaction type.");
            }
            transIndex1=xml.indexOf("<"+txnType+">");
            transIndex2=xml.indexOf("</"+txnType+">", transIndex1);
        }
        else
        {
            Log.customer.debug("*** There was a client xml");
            //There was a client specific xml so get the trans section that's needed.
            transIndex1=xml.indexOf("<"+txnType+">");
            transIndex2=xml.indexOf("</"+txnType+">", transIndex1);
        }

        if(transIndex1==-1 || transIndex2==-1 || transIndex1 > transIndex2)
        {
            Logs.buysense.debug("AMSTXNStringFactory - Problems with <" + txnType + 
                                "> section : Begin index = " + transIndex1 +
                                "; End index = " + transIndex2);
            Logs.buysense.debug("AMSTXNStringFactory - XML file : " + xml);

            // Bad file - try to reload once - sacrifice this and potentially some transactions in flight
            AMSTXN.m_stringfactory = null;
            AMSTXN.initMappingData();
            throw new XMLMappingException("XML Error: There was no <"+txnType+"> section found.");
        }

        Log.customer.debug("*** transIndex 1 and 2 from generateDataString(): %s ", String.valueOf(transIndex1) + " " + String.valueOf(transIndex2));
        String section=xml.substring(transIndex1, transIndex2);
        Log.customer.debug("*** section from generateDataString(): %s ", section);
        //End Dev SPL#70

        if (XMLsection.equals("header")) {
            //header
            generateddatastring=setDataStringSection(section, this.KEY, this.KEYFIELD, txn, clientName, txnType);
            generateddatastring+=setDataStringSection(section, this.BATCH, this.BATCHFIELD, txn, clientName, txnType);
            generateddatastring+=setDataStringSection(section,this.HEADER, this.HEADERDATA, txn, clientName, txnType);
        }
        else if(XMLsection.equals("detail")) {
            generateddatastring=setDataStringSection(section, this.DETAIL, this.DETAILDATA, txn, clientName, txnType);
        }
        else if(XMLsection.equals("subdetail")){

            generateddatastring=setDataStringSection(section, this.SUBDETAIL, this.SUBDETAILDATA, txn, clientName, txnType);
            Log.customer.debug("In subdetail of generateDataString: %s",generateddatastring);
        }
        if (errorsOccurred)
        {
            /*if (head)
                Log.spam.M("Data String Header: "+generateddatastring);
            else
                Log.spam.M("Data String Detail: "+generateddatastring);*/

            throw new XMLMappingException("XML mapping errors occurred - please check log for details.");
        }

        return generateddatastring;
    }

    public String setDataStringSection (String xmlsection, String sectionname, String field, BaseObject txn, String clientName, String txnType)throws XMLMappingException
    {
        String ormsvalue = null;
        int index1=0; int index2=0;
        index1=xmlsection.indexOf("<"+sectionname);
        index2=xmlsection.indexOf("</"+sectionname, index1);
        String section=xmlsection.substring(index1, index2);
        index1=0; index2=0; int size=0;

        //get the size of the array
        index1 = section.indexOf("LENGTH=\"")+8;
        index2 = section.indexOf("\"", index1);
        size=Integer.valueOf(section.substring(index1, index2)).intValue();
        char dataarray[]=new char[size];
        for (int i=0; i<size; i++ ){
            dataarray[i]=' ';
        }
        int nextOffset=0;
        //set the values



        while((index1=section.indexOf("<"+field, index1))!=-1)
        {
            index2 = section.indexOf("/>", index1);
            String fieldSection=section.substring(index1, index2);
            // increase index so that we get next line in the next attempt
            index1+=10;

            String ORMSFIELDNAME = "";
            String ERPFIELDNAME = "";
            String ORMSGET = "";
            int ERPLENGTH = 0;
            int ERPOFFSET = 0;
            int ORMSOFFSET = 0;
            int temp = 0;
            int tempIndex1 = 0;
            int tempIndex2 = 0;
            String MODIFIER = "";
            String ERPDEFAULT = "";
            String FILLER = "";
            String AMSTAG1 = "";
            String AMSTAG2 = "";
            int PRECISION = 0;

            try {

                // ---------------------------------------------------------
                // retrieving tags
                // ---------------------------------------------------------

                if ((temp=fieldSection.indexOf("ORMSFIELDNAME=\""))>-1)
                {
                    tempIndex1=temp+15;
                    tempIndex2=fieldSection.indexOf("\"", tempIndex1);
                    ORMSFIELDNAME=fieldSection.substring(tempIndex1, tempIndex2);
                }

                if ((temp=fieldSection.indexOf("ERPFIELDNAME=\""))>-1)
                {
                    tempIndex1=temp+14;
                    tempIndex2=fieldSection.indexOf("\"", tempIndex1);
                    ERPFIELDNAME=fieldSection.substring(tempIndex1, tempIndex2);
                }
                else
                {
                    throw new XMLRequiredTagException("ERPLENGTH");
                }

                if ((temp=fieldSection.indexOf("ORMSGET=\""))>-1)
                {
                    tempIndex1=temp+9;
                    tempIndex2=fieldSection.indexOf("\"", tempIndex1);
                    ORMSGET=fieldSection.substring(tempIndex1, tempIndex2);
                }

                if ((temp=fieldSection.indexOf("ORMSOFFSET=\""))>-1)
                {
                    tempIndex1=temp+12;
                    tempIndex2=fieldSection.indexOf("\"", tempIndex1);
                    ORMSOFFSET=Integer.valueOf(fieldSection.substring(tempIndex1, tempIndex2)).intValue();
                }

                if ((temp=fieldSection.indexOf("ERPLENGTH=\""))>-1)
                {
                    tempIndex1=temp+11;
                    tempIndex2=fieldSection.indexOf("\"", tempIndex1);
                    ERPLENGTH=Integer.valueOf(fieldSection.substring(tempIndex1, tempIndex2)).intValue();
                }
                else
                {
                    throw new XMLRequiredTagException("ERPLENGTH");
                }

                if ((temp=fieldSection.indexOf("ERPOFFSET=\""))>-1)
                {
                    tempIndex1=temp+11;
                    tempIndex2=fieldSection.indexOf("\"", tempIndex1);
                    ERPOFFSET=Integer.valueOf(fieldSection.substring(tempIndex1, tempIndex2)).intValue();
                }
                else
                {
                    throw new XMLRequiredTagException("ERPOFFSET");
                }

                if ((temp=fieldSection.indexOf("MODIFIER=\""))>-1)
                {
                    tempIndex1=temp+10;
                    tempIndex2=fieldSection.indexOf("\"", tempIndex1);
                    MODIFIER=fieldSection.substring(tempIndex1, tempIndex2);
                }

                if ((temp=fieldSection.indexOf("ERPDEFAULT=\""))>-1)
                {
                    tempIndex1=temp+12;
                    tempIndex2=fieldSection.indexOf("\"", tempIndex1);
                    ERPDEFAULT=fieldSection.substring(tempIndex1, tempIndex2);
                }

                if ((temp=fieldSection.indexOf("FILLER=\""))>-1)
                {
                    tempIndex1=temp+8;
                    tempIndex2=fieldSection.indexOf("\"", tempIndex1);
                    FILLER=fieldSection.substring(tempIndex1, tempIndex2);
                }

                if ((temp=fieldSection.indexOf("AMSTAG1=\""))>-1)
                {
                    tempIndex1=temp+9;
                    tempIndex2=fieldSection.indexOf("\"", tempIndex1);
                    AMSTAG1=fieldSection.substring(tempIndex1, tempIndex2);
                }

                if ((temp=fieldSection.indexOf("AMSTAG2=\""))>-1)
                {
                    tempIndex1=temp+9;
                    tempIndex2=fieldSection.indexOf("\"", tempIndex1);
                    AMSTAG2=fieldSection.substring(tempIndex1, tempIndex2);
                }

                if ((temp=fieldSection.indexOf("PRECISION=\""))>-1)
                {
                    tempIndex1=temp+11;
                    tempIndex2=fieldSection.indexOf("\"", tempIndex1);
                    PRECISION=Integer.valueOf(fieldSection.substring(tempIndex1, tempIndex2)).intValue();
                }
                else
                    PRECISION = 2; // default precision

                //Log.spam.M("ORMSFIELDNAME: "+ORMSFIELDNAME+" ORMSGET: "+ORMSGET+" ERPLENGTH: "+ERPLENGTH+" ERPOFFSET: "+ERPOFFSET+" ORMSOFFSET: "+ORMSOFFSET+" MODIFIER: "+MODIFIER+" ERPDEFAULT: "+ERPDEFAULT+" FILLER: "+FILLER+" AMSTAG2: "+AMSTAG2+" AMSTAG1: "+AMSTAG1+" PRECISION: "+PRECISION);
                 System.out.println("ORMSFIELDNAME: "+ORMSFIELDNAME+" ORMSGET: "+ORMSGET+" ERPLENGTH: "+ERPLENGTH+" ERPOFFSET: "+ERPOFFSET+" ORMSOFFSET: "+ORMSOFFSET+" MODIFIER: "+MODIFIER+" ERPDEFAULT: "+ERPDEFAULT+" FILLER: "+FILLER+" AMSTAG2: "+AMSTAG2+" AMSTAG1: "+AMSTAG1+" PRECISION: "+PRECISION);

                // ---------------------------------------------------------
                // checking offset
                // ---------------------------------------------------------

                // only do these checkings if flagis turned on
                if (lengthAndOffsetChecking)
                {
                    if (nextOffset!=ERPOFFSET && !offsetLengthProblem)
                    {
                        offsetLengthProblem = true;
                        throw new XMLOffsetLengthProblemException();
                    }
                    nextOffset = ERPOFFSET+ERPLENGTH;
                }

                // ---------------------------------------------------------
                // now decide what to do with the tags
                // ---------------------------------------------------------

                if (ERPDEFAULT.equalsIgnoreCase("SPACE"))
                {
                    ormsvalue = "";
                }
                else if (!ERPDEFAULT.equalsIgnoreCase(""))
                {
                    ormsvalue = ERPDEFAULT;
                }
                else
                {
                    // deal here with normal and special cases like someVector[0].Element

                    // save reference to txn
                    BaseObject txnObject = txn;

                    int i1 = ORMSFIELDNAME.indexOf("[");
                    while (i1>-1)
                    {
                        // subscript found
                        int subScript=0;

                        // first retrieve var
                        String objectVar = ORMSFIELDNAME.substring(0, i1);

                        // then retrieve subscript
                        int i2 = ORMSFIELDNAME.indexOf("]");
                        if (i2==-1)
                        {
                            throw new Exception("right ] missing");
                        }

                        // now get subscript
                        try
                        {
                            String subScriptStr = ORMSFIELDNAME.substring(i1+1, i2);
                            Integer subScriptInt = (Integer.valueOf(subScriptStr));
                            subScript = subScriptInt.intValue();
                        }
                        catch (Exception e)
                        {
                            throw new Exception("invalid subscript ");
                        }

                        java.util.List v;

                        // now since we have var and subscript
                        try
                        {
                            // retrieve vector element
                            Object vectorObject = txnObject.getDottedFieldValue(objectVar.trim());
                            if (vectorObject==null)
                                throw new XMLMappingException("XML Error: List "+objectVar+" was not found or is null.");

                            v = (java.util.List)vectorObject;
                        }
                        catch (Exception e)
                        {
                            throw new Exception("retrieving vector "+e);
                        }

                        try
                        {
                            // retrieve subscript
                            //Ariba 8.1 replaced elementAt with get
                            //Object obj = v.elementAt(subScript);
                            Object obj = v.get(subScript);
                            if (obj instanceof ariba.base.core.BaseId)
                                txnObject = ((ariba.base.core.BaseId) obj).get();
                            else
                            	txnObject = (BaseObject)v.get(subScript);
                                //txnObject = (BaseObject)v.elementAt(subScript);
                        }
                        catch (ArrayIndexOutOfBoundsException e)
                        {
                            Log.customer.debug("We hit this on %s", txnObject.toString());
                            throw new NoProblemException();
                        }
                        catch (Exception e)
                        {
                            throw new Exception("retrieving vector element");
                        }

                        ORMSFIELDNAME = ORMSFIELDNAME.substring(i2+2);
                        i1 = ORMSFIELDNAME.indexOf("[");

                    } // end of while vector

                    if (ORMSGET.toUpperCase().equalsIgnoreCase("GETMONTH"))
                    {
                        ariba.util.core.Date date=(ariba.util.core.Date)txnObject.getDottedFieldValue(ORMSFIELDNAME);
                        ormsvalue=String.valueOf(ariba.util.core.Date.getMonth(date)+1);
                        if (ormsvalue.length()==1)
                            ormsvalue = "0" + ormsvalue;

                    }
                    else if (ORMSGET.toUpperCase().equalsIgnoreCase("GETDAYOFMONTH") || ORMSGET.toUpperCase().equalsIgnoreCase("GETDAY"))
                    {
                        ariba.util.core.Date date=(ariba.util.core.Date)txnObject.getDottedFieldValue(ORMSFIELDNAME);
                        ormsvalue=String.valueOf((ariba.util.core.Date.getDayOfMonth(date)));
                        if (ormsvalue.length()==1)
                            ormsvalue = "0" + ormsvalue;
                    }
                    else if (ORMSGET.toUpperCase().equalsIgnoreCase("GETYEAR"))
                    {
                        ariba.util.core.Date date=(ariba.util.core.Date)txnObject.getDottedFieldValue(ORMSFIELDNAME);
                        String year=String.valueOf(ariba.util.core.Date.getYear((java.util.Date)date));
                           if (year.length()==1)
                            ormsvalue = "0" + year;
                        else
                            ormsvalue = year.substring(year.length()-2, year.length());
                    }
                    //e2e Start:
                    else if (ORMSGET.toUpperCase().equalsIgnoreCase("E2EDATE"))
                    {
                        ariba.util.core.Date date=(ariba.util.core.Date)txnObject.getDottedFieldValue(ORMSFIELDNAME);
                        SimpleDateFormat loFormattedDate = new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss.SSS");
                        ormsvalue = loFormattedDate.format(date);
                    }
                    //e2e End:

                    //eProcurement ST SPL #821 Start:remove comma's from text number/amount fields
                    else if (ORMSGET.toUpperCase().equalsIgnoreCase("FORMATSTRINGNUM"))
                    {
                        String stringNumber = ((BigDecimal)txnObject.getDottedFieldValue(ORMSFIELDNAME)).toString();

                        ormsvalue = removecomma(stringNumber);
                    }
                    //eProcurement ST SPL #821 End:
                    else
                    {
                        Object retrievedValue = txnObject.getDottedFieldValue(ORMSFIELDNAME);

                        if (retrievedValue==null)
                        {
                            ormsvalue = "";
                            if (!checkIfFieldsExist)
                                Log.customer.debug("Warning: Field %s of %s for %s  of %s contains null or was not found in BaseObject",ORMSFIELDNAME,sectionname,txnType,clientName);
                            else
                            {    Log.customer.debug("IXM We hit the else section");
                                // now check if field exists or if it contains null if checkIfFieldsExist is turned on
                                boolean fieldFound = false;
								for (Iterator e = txnObject.getAllFieldNames(); e.hasNext() ;)
                                {
									//Ariba 8.1 replaced nextElement with next
                                    if (((String)e.next()).equalsIgnoreCase(ORMSFIELDNAME))
                                    {
                                        // field found but contains null
                                        fieldFound = true;
                                        Log.customer.debug("Warning: Field %s of %s  for %s  of %s  contains null",ORMSFIELDNAME,sectionname,txnType,clientName);

                                        // if field is null but exists, replace with spaces
                                        ormsvalue = "";
                                        break;
                                    }
                                 }

                                 // field not found, issue error
                                 if (!fieldFound)
                                    throw new XMLORMSFieldNotFoundException(ORMSFIELDNAME);
                            } // end if checkIfFieldsExist
                        }
                        //Ariba 8.1 replaced new money value
                          else if (retrievedValue instanceof ariba.basic.core.Money)
                        {
                            // retrieve amount field
                            retrievedValue = ((BaseObject)retrievedValue).getFieldValue("Amount");
                            ormsvalue = BigDecimalFormatter.getStringValue((BigDecimal)retrievedValue, PRECISION);
                        }
                        else if (retrievedValue instanceof BigDecimal)
                            ormsvalue = BigDecimalFormatter.getStringValue((BigDecimal)retrievedValue, PRECISION);
                        else if (retrievedValue instanceof Double)
                            ormsvalue = BigDecimalFormatter.getStringValue(new BigDecimal(((Double)retrievedValue).doubleValue()), PRECISION);
                        else
                        {
                            // for now assume that this value can by casted to a string
                            ormsvalue=String.valueOf(retrievedValue);
                            // to retrieve substring
                            if (ORMSOFFSET>0)
                                ormsvalue = ormsvalue.substring(ORMSOFFSET, Math.min(ORMSOFFSET+ERPLENGTH, ormsvalue.length()));
                        }
                    } // end if normal cases and vector
                } // end what to do with tags

                // now ormsvalue, ERPLENGTH, ERPOFFSET is set

                // here we are actually copying the retrieved value into the datastring

                Log.customer.debug("IXM this is where data is getting set : %s",ormsvalue);
                char tempchararray[]=ormsvalue.toCharArray();
                for (int j=0; j<tempchararray.length; j++) {
                    if (j>=ERPLENGTH) break;
                    dataarray[ERPOFFSET++]=tempchararray[j];
                }
            }
            catch (XMLRequiredTagException np ) {
                Log.customer.debug("XML Error: Tag %s  missing", np);
                Log.customer.debug("Please check xml file for %s for %s of  %s ",sectionname,txnType,clientName);
                errorsOccurred = true;
            }
            catch (ClassCastException np ) {
                Log.customer.debug("XMLMapping Error: ClassCastException when trying to retrieve field %s : %s .", sectionname, ERPFIELDNAME);
                Log.customer.debug("Please check xml file for %s  for %s of %s", sectionname,txnType,clientName);
                errorsOccurred = true;
            }
            catch (XMLOffsetLengthProblemException np ) {
                Log.customer.debug("XMLMapping Error: Offset of field "+sectionname+":"+ERPFIELDNAME+" or length of previous field are wrong");
                Log.customer.debug("Please check xml file for "+sectionname+" for "+txnType+" of "+clientName);
                errorsOccurred = true;
            }
            catch (XMLORMSFieldNotFoundException np ) {
                Log.customer.debug("XMLMapping Error: Field "+sectionname+":"+ERPFIELDNAME+" not found");
                Log.customer.debug("Please check xml file for "+sectionname+" for "+txnType+" of "+clientName);
                Log.customer.debug("IXM field not found");
                errorsOccurred = true;
            }
            catch (NullPointerException np ) {
                Log.customer.debug("XMLMapping Error(Null Pointer): Field "+sectionname+":"+ERPFIELDNAME+" not found");
                Log.customer.debug("Please check xml file for "+sectionname+" for "+txnType+" of "+clientName);
                Log.customer.debug("IXM Null pointer");
                errorsOccurred = false;
            }
            catch (Exception e ) {
                Log.customer.debug("XMLMapping Error: Error " + e + " in AMSTXNStringFactory when dealing with field ERPFIELDNAME: "+sectionname+":"+ORMSFIELDNAME+"  ORMSGET: "+ORMSGET+"  ERPLENGTH: "+ERPLENGTH+"  ERPOFFSET: "+ERPOFFSET+" ormsvalue: "+ormsvalue);
                Log.customer.debug("Please check xml file for "+sectionname+" for "+txnType+" of "+clientName);
                errorsOccurred = true;
            }
            catch (NoProblemException e ) {
                // do nothing
            }
        //for now - IXM
         // Log.customer.debug("XMLMapping " +"in AMSTXNStringFactory when dealing with field ERPFIELDNAME: "+sectionname+":"+ORMSFIELDNAME+"  ORMSGET: "+ORMSGET+"  ERPLENGTH: "+ERPLENGTH+"  ERPOFFSET: "+ERPOFFSET+" ormsvalue: "+ormsvalue);
        } // end while

        return String.valueOf(dataarray);
    }

    /**
        Returns true if a field name was found.
        Looks in the <RPETRIGGERFIELD> section for a specified document.
    */
    public boolean isTriggerField(String clientName, String documentName, String className, String fieldName)
    {
//        System.out.println("clientName: "+clientName+" documentName: "+documentName+" className: "+className+" fieldName: "+fieldName);

        String triggerSection = "RPETRIGGERS";

        // make uppercase to ignore case
        fieldName = fieldName.toUpperCase();
        documentName = documentName.toUpperCase();
        className = className.toUpperCase();

        try
        {
            int maxLoop=20;

            //Begin Dev SPL#70
            int transIndex1, transIndex2;
            String hashKey=clientName.toUpperCase() + documentName.toUpperCase();
            String xml=(String)XMLs.get(hashKey);

            if (xml==null)
            {
                //There's no client specific xml. Need to use the default xml.
                int ndx=clientName.indexOf("_");
                String prfx=clientName.substring(0, ndx);

                xml=(String)XMLs.get(prfx + "_COMMONAGENCIES" + documentName.toUpperCase());
                if(xml==null)
                {
                    throw new XMLMappingException("XML Error: A COMMONAGENCIES xml must exist for each transaction type.");
                }
                transIndex1=xml.indexOf("<"+triggerSection+">");
                transIndex2=xml.indexOf("</"+triggerSection+">", transIndex1);
            }
            else
            {
                //There was a client specific xml so get the trigger section.
                transIndex1=xml.indexOf("<"+triggerSection+">");
                transIndex2=xml.indexOf("</"+triggerSection+">", transIndex1);
            }

            if(transIndex1==-1)
            {
                throw new XMLMappingException("XML Error: There was no <"+triggerSection+"> section found.");
            }

            String section=xml.substring(transIndex1, transIndex2);
            //end Dev SPL#70

            // make uppercase to ignore case
            section = section.toUpperCase();

            // now find class
            maxLoop=20;
            int classIndex1=0;
            int classIndex2=0;
            String classSection="";
            do
            {
                maxLoop--;
                classIndex1=section.indexOf("<CLASS", classIndex1);
                classIndex2=section.indexOf("</CLASS", classIndex1);
                classSection=section.substring(classIndex1, classIndex2);
                classIndex1=section.indexOf("<CLASS", classIndex2);

            }
            while (classSection.indexOf(className)==-1 && maxLoop>0);

            // return true if field was not found. this means that this field does not trigger
            //System.out.println("Checking if "+fieldName+" is in "+classSection);
            if (classSection.indexOf(fieldName)>-1)
                return true;
            else
                return false;
        }
        catch (XMLMappingException np ) {
            Log.customer.debug("Section <"+triggerSection+"-"+documentName+"> of "+clientName+" was not found when checking for field "+fieldName);
        }
        return false;
    }

    //eProcurement ST SPL #821 Start:remove comma's from text number/amount fields
    private String removecomma(String inString) throws Exception
    {
        StringBuffer sb1 = new StringBuffer(inString);
        StringBuffer sb2 = new StringBuffer("");
        char chr1 = ' ';

        for (int i = 0; i < sb1.length(); i++)
        {
            chr1 = sb1.charAt(i);   // keep digits and decimal point ... drop all else
            if ((Character.isDigit(chr1)) || (chr1 == '.'))
            {
               sb2.append(chr1);
            }
        }
        return sb2.toString();
    }
    //eProcurement ST SPL #821 End:

/*    private String substring(String tag, String value) throws XMLMappingException
    {
        String section="";
        do
        {
            int index1=0,index2=0;

            index1=XMLString.indexOf("<"+tag+">", index1);
            if (index1==-1)
            {
                throw new XMLMappingException("XML Error: There was no <"+clientName.toUpperCase()+"> section found.");
            }

            index2=XMLString.indexOf("</"+tag+">", index1);
            section=XMLString.substring(index1, index2);

            if (maxLoop--<0)
            {
                throw new XMLMappingException("XML Error: There was no "+sectionName+" xml found for "+clientName);
            }
            index1+=5;
        }
        while ((docIndex1=clientSection.indexOf("<"+sectionName+">"))==-1);

    }*/
}

