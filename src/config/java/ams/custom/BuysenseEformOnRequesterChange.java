package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseEformOnRequesterChange extends Action
{

    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        Approvable loAppr = null;
        Log.customer.debug("BuysenseEformOnRequesterChange called");
        if (valuesource != null && valuesource instanceof Approvable)
        {
            loAppr = (Approvable) valuesource;
            clearTitleField(loAppr);
        }
    }

    public void clearTitleField(Approvable appr)
    {
        Log.customer.debug("Inside BuysenseEformOnRequesterChange::clearTitleField");
        appr.setFieldValue("Name", "");
    }
}
