/* 08/11/2003: Updates for Ariba 8.0 (Jeff Namadan) */
/* 08/13/2003: Updates for Ariba 8.0 (David Chamberlain) 
   Had to back out previous 8.0 changes. We can't change
   ValueInfo to ValueInfo at this time.      */

// 10/10/2003: Updates for Ariba 8.1 (Richard Lee)

package config.java.ams.custom;

import java.util.List;
import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.core.BaseObject;
import ariba.htmlui.fieldsui.Log;


//81->822 changed Vector to List
//81->822 changed ConditionValueInfo to ValueInfo
public class BuysenseReqAcctgVisibility extends Condition
{
    
    public static final String TargetValue1Param = "TargetValue1";
    public static final String TargetValue2Param = "TargetValue2";
    public static final String TargetValue3Param = "TargetValue3";
    /* Ariba 8.0: Replaced ValueInfo */
    private static final ValueInfo parameterInfo[];
    private static final String requiredParameterNames[] = 
        {
        "TargetValue"
    };
    public boolean evaluate(Object value, PropertyTable params)
    
    {
        Log.visibility.debug("Buysense: This is the object we get in the evaluate: %s ", value);
        return testValue(value, params);
    }
    
    protected boolean testValue(Object value, PropertyTable params)
    {
        String clientUniqueName = (String)params.getPropertyForKey("TargetValue");
        String fieldName        = (String)params.getPropertyForKey("TargetValue1");
        BaseObject bo   = (BaseObject)params.getPropertyForKey("TargetValue2");
        String appType          = (String)params.getPropertyForKey("TargetValue3");
        
        Log.visibility.debug("ClientUN: %s fieldName: %s BaseObject: %s",
                             clientUniqueName,fieldName,bo);
        if (clientUniqueName == null)
            // Ariba 8.1 changed  Util.assertNonFatal(false,"Did not receive the Client Name");
            Assert.assertNonFatal(false,"Did not receive the Client Name");
        
        //    boolean ret = BuysenseUIConditions.isFieldVisible(clientUniqueName , fieldName,bo,appType);
        //    Log.visibility.debug("Returning %s from Visibility",new Boolean(ret));
        
        ReqDefaulter rd = new ReqDefaulter();
        //pd.fire((ValueSource)bo,params);
        rd.setAccounting((ValueSource)bo,params);
        //pd.setRefAccounting((ValueSource)bo,params);
        return true;
    }
    
    public ConditionResult evaluateAndExplain(Object value,
                                              PropertyTable params)
    {
        Log.visibility.debug("Buysense: This is the object we get in the evaluate and explain: %s ", value);
        if(!testValue(value, params))
        {
            String msg = params.stringPropertyForKey("Message");
            String subject = subjectForMessages(params);
            if(msg != null)
                return new ConditionResult(Fmt.Si(msg, subject));
            else
                return new ConditionResult(Fmt.Sil("ariba.common.core.condition",
                                                   "EqualToMsg1", subject));
        }
        else
        {
            return null;
        }
    }
    
    private List getTargetValues(PropertyTable params)
    {
        Log.visibility.debug("In target values");
        // Ariba 8.1 Changed List targetValues = new List();
        List targetValues = ListUtil.list();
        Object targetValue = params.getPropertyForKey("TargetValue");
        if(targetValue != null)
            // Ariba 8.1 replaced addElement with add
            targetValues.add(targetValue);
        targetValue = params.getPropertyForKey("TargetValue1");
        if(targetValue != null)
            targetValues.add(targetValue);
        targetValue = params.getPropertyForKey("TargetValue2");
        if(targetValue != null)
            targetValues.add(targetValue);
        targetValue = params.getPropertyForKey("TargetValue3");
        if(targetValue != null)
            targetValues.add(targetValue);
        return targetValues;
    }
    
    
    /* Ariba 8.0: Replaced ValueInfo */
    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }
    
    protected String[] getRequiredParameterNames()
    {
        return requiredParameterNames;
    }
    
    
    static
        {
        /* Ariba 8.0: Replaced ValueInfo */
        parameterInfo = (new ValueInfo[] 
                         {
                /* Ariba 8.0: Replaced ValueInfo */
                new ValueInfo("TargetValue",
                                       0),
                    new ValueInfo("TargetValue1",
                                           0),
                    new ValueInfo("TargetValue2",
                                           0),
                    new ValueInfo("TargetValue3",
                                           0),
                    new ValueInfo("Message", 0, Behavior.StringClass)
            }
        );
    }
}
