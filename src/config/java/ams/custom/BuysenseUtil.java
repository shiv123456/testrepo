/*
    Responsible: imohideen

*/

// 10/10/2003: Updates for Ariba 8.1 (Richard Lee)
// Ariba 8.1 Replaced elementAt with get
// Ariba 8.1 Replaced addElements with addAll
// Ariba 8.1 Replaced addElement with add
// 09/04/2004: DEV SPL # 85 - Ariba 8.1 Upgrade - Added overloaded methods findField() and findFieldLabel()

package config.java.ams.custom;

import ariba.base.core.BaseObject;
// Ariba 8.1 commented out Util
//import ariba.util.core.Util;
import java.util.*;
import ariba.util.core.ListUtil;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.htmlui.fieldsui.Log;
import ariba.common.core.User;
import java.math.BigDecimal;

import java.text.*;
import org.apache.log4j.Level;

/*
    Shane Liu 8.1, Dev SPL #10 March 10, 2004
    Added findFieldLabel() to get the label
*/
/**
 * @author manoj.gaur
 * @date   1/12/09
 * @explanation CSPL-119 converted static method initFieldVectors into static block
 */

//81->822 changed Vector to List
public class BuysenseUtil
{
    // Ariba 8.1 private static List fieldsVector = new List();
    // Ariba 8.1 private static List fieldValuesVector = new List();
    private static List fieldsVector = ListUtil.list();
    private static List fieldValuesVector = ListUtil.list();
    static List tokenize(String whereClause)
    {
        // Ariba 8.1 List clauseVector = new List();
        List clauseVector = ListUtil.list();
        StringTokenizer str = new StringTokenizer(whereClause,";");
        while (str.hasMoreTokens())
        {
            String subToken = str.nextToken();
            StringTokenizer substr = new StringTokenizer(subToken,",");
            // Ariba 8.1 List fieldVector = new List();
            List fieldVector = ListUtil.list();
            while (substr.hasMoreTokens())
            {
                String tempStr = substr.nextToken();
                fieldVector.add( tempStr);
                Log.customer.debug("Buysense: Token : %s",tempStr);
            }
            clauseVector.add(fieldVector);
        }
        return clauseVector ;
    }

    /*
    getBD is a helper method used for generating BigDecimal objects form String parameters.
    It is capable of hadling fine formatted Strings (e.g. 1,000.00)
    */
    static BigDecimal getBD (String inString, String fieldTitle)
    {
        BigDecimal bd = new BigDecimal("0");
        try
        {
            bd = new BigDecimal(editBD(inString));
        }
        catch (NumberFormatException e)
        {
            Log.customer.debug(fieldTitle + " contains invalid numeric data (" + inString.trim() + ")" + e);
        }
        //return bd.setScale(2);              // returns bd in 0.00 format
        Log.customer.debug("Returning a BigDecimal: " + (String)bd.toString());
        return bd;
    }

    static String editBD (String inString)
    {
        String s = inString.trim();
        if (s.equals("")) s = "0";
        else
        {
            char [] cc = inString.trim().toCharArray();
            char [] dd = new char[cc.length];
            int j = 0;
            for (int i = 0; i < cc.length; i++)
                if (cc[i] != ',') dd[j++] = cc[i];
            if (j > 0) s = new String(dd).substring(0,j);
        }
        return s;
    }


    static String findFieldValue(String fieldName,
                                 BaseObject baseObj,String appType)
    {
        String fieldValue=new String();
        String field = fieldName.substring(5);
        Log.customer.debug("In findFieldValue: field: %s ",field);

        if ( appType.equals("BuysenseOrg"))
        {
            fieldValue = (String)baseObj.getDottedFieldValue(field);
            return fieldValue;
        }
        if (fieldName.startsWith("hedr"))
        {
            if (!appType.equals("ReqHeader")||!appType.equals("PaymentHeader"))
            {
                fieldValue = (String)baseObj.getClusterRoot().getDottedFieldValue(field);
            }
            else
                fieldValue = (String)baseObj.getDottedFieldValue(field);
        }
        if (fieldName.startsWith("line"))
        {
            if (appType.equals("ReqLine") || appType.equals("PaymentLine"))
            {
                fieldValue = (String)baseObj.getDottedFieldValue(field);
            }
        }
        if (fieldName.startsWith("actg"))
        {
            if (appType.equals("ReqAccounting")
                    || appType.equals("PaymentAccounting"))
            {
                fieldValue = (String)baseObj.getDottedFieldValue(field);
            }
        }
        return fieldValue;
    }

    static public ClusterRoot findField(Partition foPart, String clientUniqueName ,
                                        String fieldName)
    {
        int j=0 ;        
        /*if (!initVector) 
        	initFieldVectors();*/
        int fieldValuesCount = fieldValuesVector.size();

        for( j=0; j< fieldValuesCount ; j++)
        {
            String tempfield = (String)fieldValuesVector.get(j);
            if (tempfield.equals(fieldName)) break;
        }

        String targetFieldName = (String)fieldsVector.get(j);
        String UniqueName = (clientUniqueName+":"+targetFieldName);

        Log.customer.debug("*** UniqueName: " + UniqueName);

        ClusterRoot fieldTable = Base.getService().objectMatchingUniqueName
            ("ariba.core.BuysenseFieldTable", foPart, UniqueName);
        Log.visibility.debug("Found this fieldTable object: %s",fieldTable);
        return fieldTable;
    }

    static public String findFieldLabel(Partition foPart, String clientUniqueName ,
                                        String fieldName)
        {
        ClusterRoot fieldTable = findField(foPart, clientUniqueName, fieldName);
        Log.customer.debug("findFieldLabel: Found this fieldTable object: %s",fieldTable);
        if (fieldTable != null)
        {
                String label = (String)fieldTable.getFieldValue("ERPValue");
                Log.customer.debug("findFieldLabel: Label for fieldname "+fieldName+" is "+label);
                return label;
                }
                return fieldName;
        }
    static public ClusterRoot findField(String clientUniqueName ,
                                        String fieldName)
    {
        int j=0 ;        
        /*if (!initVector)
        	initFieldVectors();*/
        int fieldValuesCount = fieldValuesVector.size();

        for( j=0; j< fieldValuesCount ; j++)
        {
            //Log.customer.debug("findFieldLabel: class:%s",fieldValuesVector.get(j).getClass().toString());
            String tempfield = (String)fieldValuesVector.get(j);
            if (tempfield.equals(fieldName)) break;
        }

        String targetFieldName = (String)fieldsVector.get(j);
        String UniqueName = (clientUniqueName+":"+targetFieldName);
        ClusterRoot fieldTable = Base.getService().objectMatchingUniqueName
            ("ariba.core.BuysenseFieldTable",
             Base.getSession().getPartition(), UniqueName);
        Log.visibility.debug("Found this fieldTable object: %s",fieldTable);
        return fieldTable;
    }
   //8.1 method for getting label from the FieldTable
    static public String findFieldLabel(String clientUniqueName ,
                                        String fieldName)
   {
        ClusterRoot fieldTable = findField(clientUniqueName,
                                                        fieldName);
        Log.customer.debug("findFieldLabel: Found this fieldTable object: %s",fieldTable);
        if (fieldTable != null)
        {
         String label = (String)fieldTable.getFieldValue("ERPValue");
         Log.customer.debug("findFieldLabel: Label for fieldname "+fieldName+" is "+label);
         return label;
      }
      return fieldName;
   }
    // private static void initFieldVectors() CSPL-119 (Manoj 01/12/09)static method has converted into static block
    static {
        Log.visibility.debug("InitFieldVectors called");
        Log.customer.debug("InitFieldVectors called");
        fieldsVector = FieldListContainer.getHeaderFields();
        fieldsVector.addAll(FieldListContainer.getLineFields());
        fieldsVector.addAll(FieldListContainer.getAcctgFields());
        fieldsVector.addAll(FieldListContainer.getPaymentHeaderFields());
        fieldsVector.addAll(FieldListContainer.getPaymentLineFields());
        fieldsVector.addAll(FieldListContainer.getRefAcctgFields());

        fieldValuesVector = FieldListContainer.getHeaderFieldValues();
        fieldValuesVector.addAll(FieldListContainer.getLineFieldValues());
        fieldValuesVector.addAll(FieldListContainer.getAcctgFieldValues());
        fieldValuesVector.addAll(FieldListContainer.getPaymentHeaderFieldValues());
        fieldValuesVector.addAll(FieldListContainer.getPaymentLineFieldValues());
        fieldValuesVector.addAll(FieldListContainer.getRefAcctgFields());
       // initVector = true;
    }

    public static void createHistory (BaseObject foApprovableObject, String fsUniqueName, String fsRecordType, String fsMessage, User foUser)
    {
       BaseObject po_hist = (ariba.procure.core.SimpleProcureRecord) BaseObject.create("ariba.procure.core.SimpleProcureRecord", foApprovableObject.getPartition());
       po_hist.setFieldValue("Approvable", foApprovableObject);
       po_hist.setFieldValue("ApprovableUniqueName", fsUniqueName);

       ariba.util.core.Date  loDate =  new ariba.util.core.Date();
       po_hist.setFieldValue("Date", loDate);

       po_hist.setFieldValue("User", foUser.getUser());
       po_hist.setFieldValue("RecordType", fsRecordType);

       if (fsMessage != null)
       {
          ((List)po_hist.getFieldValue("Details")).add(fsMessage);
}

       //Now add it to the approvable
       ((List)foApprovableObject.getFieldValue("Records")).add(po_hist) ;
    }

   public static void showCallerInfo(Throwable t, String classname, Object obj) {
      StackTraceElement[] elements = t.getStackTrace();

      StringBuffer x = new StringBuffer(getTimeStamp());
      x.append(": Method ").append(elements[0].getMethodName());
      x.append(" in class ").append(classname);
      x.append(" was invoked by method ").append(elements[1].getMethodName());
      x.append(" in class ").append(elements[1].getClassName());
      x.append(", file name ").append(elements[1].getFileName());
      x.append(", line nbr ").append(elements[1].getLineNumber());

      // Logs.buysense.setLevel(Level.DEBUG);
      // Logs.buysense.debug(x.toString());
      // Logs.buysense.setLevel(Level.DEBUG.OFF);
      Log.customer.debug(x.toString());
   }

   public static String getTimeStamp()
   {
      try
      {
         SimpleDateFormat formatter
           = new SimpleDateFormat ("yyyy.MM.dd hh:mm:ss.S");
         Date currentTime_1 = new Date(System.currentTimeMillis());
         return(formatter.format(currentTime_1));
      }
      catch(Exception e)
      {
         return "";
      }
   }

}
