package config.java.ams.custom;

/**
 * Class containing generic PSGFunctions
 *
 * Static class containing generic functions for PSG package classes
 *
 *
 * @version 1.0
 */
import java.util.*;
import java.io.*;

public class PSGFunctions
{
    /**
     * Method for reading from an input stream and coverting the results to a String.
     *
     *
     * @param   InputStream Stream to read data from
     * @return  String  String representation of Data
     */
    public static String ReadFromInputStream(InputStream inputStream)
    {
        StringBuffer sbResult = new StringBuffer();
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        int iLength = 1, iMaxLength = 32768;
        byte arrByte[];
        try 
        {
            while (iLength > 0) 
            {
                arrByte = new byte[iMaxLength];
                iLength = bufferedInputStream.read(arrByte);
                if (iLength > 0)
                    sbResult.append(new String(arrByte,0,iLength));
            }
        }
        catch (EOFException xx) 
        {
        }
        catch (IOException x) 
        {
            //Log.Util.Debug("PSGFunctions" + "," + "ReadFromInputStream" + "," + x + "3");           
            return null;
        }
        try
        {
            bufferedInputStream.close();
        }
        catch(Exception x)
        {
        }
        return sbResult.toString();
    }
    /**
     * Method for reading a file contents into a string
     *
     *
     * @param   String Name of file
     * @return  String  FileContents
     * @exception PSGException Error occurred during file read
     */
    public static String ReadFile(String sFileName) throws PSGException
    {
        try
        {
            FileInputStream in = new FileInputStream(sFileName);
            return ReadFromInputStream(in);
        }
        catch(FileNotFoundException x) 
        {
            throw new PSGException("ReadFile " + sFileName + " FileNotFoundException error: " + x.getMessage());
        }
    }
    /**
     * Method for replacing string containing html tags <!--key--> with appropriate value 
     * from hashtable
     *
     * @param   String  String containing html tags to replace.
     * @param   Hashtable  hashtable containing keys and their respective values.
     * @return  String  Html data string with replaced values
     */
    public static String ReplaceHtml(String sHtml, Hashtable hParams)
    {
        String sKey;
        Enumeration eKeys = hParams.keys();
        Enumeration eValues = hParams.elements();
        while (eKeys.hasMoreElements())
        {
            sKey = (String)eKeys.nextElement();
            sKey = "<!--" + sKey + "-->";
            sHtml = ReplaceString(sHtml,
                                  sKey, (String)eValues.nextElement(), true);
        }
        return sHtml;
    }
    /**
     * Method for replacing a specified value in a string with another specified string.If the 
     * string is not found in the specified String, then the original string is returned.
     *
     * @param   String  String containing data to replace.
     * @param   String  Parameter to replace.
     * @param   String  New value.
     * @param   boolean  GlobalReplace.
     * @return  String  Replaced data string
     */
    public static String ReplaceString(String sData,
                                       String sParam,
                                       String sValue, boolean bGlobalReplace)
    {
        int iIndex;
        
        do
            {
            iIndex = sData.indexOf(sParam);
            if (iIndex == -1) break;
            sData = sData.substring(0,
                                    iIndex) + sValue + sData.substring(iIndex + sParam.length());
        }
        while(bGlobalReplace);
        return sData;
    }
    /**
     * Method for converting a String array to a string.
     *
     * @param   String[]  String array to be converted.
     * @param   String  delimiter.
     * @return  String  The array represented as a string delimited by the delimiter
     */    
}
