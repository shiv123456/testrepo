/************************************************************************************
 * Author:  Richard Lee
 * Date:    Nov 11, 2003
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 11/11/2003        Richard Lee            Prod SPL 141
 * 9/24/2004         Richard Lee            Ariba 8.x ST SPL 80
 *
 * @(#)ExpenditureLimitDPAPostLoad.java     1.0 11/11/2003
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/

/* Ariba 8.x ST SPL 80 - DPA Amount is null in portal but $0 in Ariba.
 * Need to propagate fix from 7.1 to 8.1 Ariba by modifying this java file to
 * reflect changes made in Ariba 8.1 for User and Partition.
 */

/* 12/22/2003: Updates for Ariba 8.1 (Jeff Namadan) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.base.core.*;
import ariba.util.log.Log;
import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.basic.core.Money;
import java.math.BigDecimal;
import ariba.common.core.User;

public class ExpenditureLimitDPAPostLoad extends Action
{
    public void fire (ValueSource object, PropertyTable params)
    {
        BigDecimal lbExpAmount;
        BigDecimal lbDPAAmount;
        Money lmExpenditure;
        Money lmDPA;
        try{
           ClusterRoot loObject = (ClusterRoot)object;
           User loPartUser = (User)loObject;
           lmExpenditure = (Money) loPartUser.getFieldValue("Expenditure_Limit");
           lmDPA = (Money) loPartUser.getFieldValue("Delegated_Purchasing_Authority");

           if(lmExpenditure !=null)
           {
              lbExpAmount = (BigDecimal)loPartUser.getDottedFieldValue("Expenditure_Limit.Amount");
              if ( lbExpAmount.doubleValue() == -123.45)
              {
                 loPartUser.setDottedFieldValue("Expenditure_Limit", null);
              }
           }

           if(lmDPA !=null)
           {
              lbDPAAmount = (BigDecimal)loPartUser.getDottedFieldValue("Delegated_Purchasing_Authority.Amount");
              if ( lbDPAAmount.doubleValue() == -123.45)
              {
                 loPartUser.setDottedFieldValue("Delegated_Purchasing_Authority", null);
              }
           }
        }
        catch ( Exception ex)
        {
           String lsMsg = ex.getMessage();
           Log.customer.debug("Error message =  " + lsMsg);
        }
    }
}
