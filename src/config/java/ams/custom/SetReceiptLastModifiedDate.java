package config.java.ams.custom;

import ariba.receiving.core.Receipt;
import ariba.base.fields.*;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
import java.lang.Integer;

/************************************************************************************************************
 *
 * This class is called by a trigger named, SetReceiptLastModDate, in eVAReceiptExtrinsicFields.aml
 * The purpose is to set Receipt LastModified date when the ProcessedReceipt scheduledTask runs.
 * The LastModified date is updated for data retention to pick up this receipt.
 *
 ************************************************************************************************************/

public class SetReceiptLastModifiedDate extends Action
{
    public void fire(ValueSource object, PropertyTable params)
    {
    	Log.customer.debug("Calling SetReceiptLastModifiedDate.");
        Receipt loReceipt = (Receipt)object;
        int liProcessedState = ((Integer) loReceipt.getFieldValue("ProcessedState")).intValue();

        if(liProcessedState == 4)
        {
            // Update Receipt last modified date
            loReceipt.setLastModified(new ariba.util.core.Date());
	    Log.customer.debug("Updated LastModified for Receipt %s" + loReceipt.getFieldValue("UniqueName") +
		   " since it was Processed.");
	}
    }
}
