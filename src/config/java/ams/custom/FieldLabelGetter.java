package config.java.ams.custom;

import java.util.List;
import ariba.base.core.*;
import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.base.fields.FieldProperties;
import java.util.Properties;

//81->822 changed Vector to List
public class FieldLabelGetter
{
   private static List moFieldsVector = ListUtil.list();
   private static List moFieldValuesVector = ListUtil.list();
   private static boolean mboolInitVector = false;

   /* Static initializer for class */
   static
   {
      moFieldValuesVector = FieldListContainer.getHeaderFieldValues();
      moFieldValuesVector.addAll(FieldListContainer.getLineFieldValues());
      moFieldValuesVector.addAll(FieldListContainer.getAcctgFieldValues());
   }

   
   public static boolean getFieldVisibility(String fsClientName, String lsFldName, BaseObject foObject, String appType)
   {
	   Log.customer.debug("FieldLabelGetter: getFieldVisibility() - Received Client %s Field %s Obj %s AppType %s", fsClientName, lsFldName, foObject, appType);
	   boolean lbFieldVisible = true;
	   String lsFieldName = null;
	   
	   lsFieldName = lsFldName;
	   if(isBSOField(lsFieldName))
	   {
	     if (lsFieldName.indexOf(".") != -1)
	     {
           lsFieldName = lsFieldName.substring(0,lsFieldName.lastIndexOf("."));
	     }
	     lbFieldVisible = BuysenseUIConditions.isFieldVisible(fsClientName, lsFieldName, foObject, appType);
	   }
	   return lbFieldVisible;
   }
   
   
   /* Gets label for a single field */
   public static String getLabel(BaseObject foObject, String fsClientName, String fsAribaName)
   {
      Log.customer.debug("FieldLabelGetter: getLabel() - Received this object %s", foObject);

      String lsFieldLabel = null;
      String lsFieldName = null;
      FieldProperties loFieldProps = null;
      Partition loPart = foObject.getPartition();

      lsFieldName = fsAribaName;

      if (isBSOField(lsFieldName))
      {
         if (lsFieldName.indexOf(".") != -1)
         {
            // Strip out last part of field name for BSO fields which use dotted notation
            // (Only BSO fields of type BuysenseFieldDataTable will fall into this logic)
            lsFieldName = lsFieldName.substring(0,lsFieldName.lastIndexOf("."));
         }
         lsFieldLabel = BuysenseUtil.findFieldLabel(loPart, fsClientName, lsFieldName);
      }
      else
      {
         loFieldProps = (FieldProperties)foObject.getFieldProperties(lsFieldName);
         lsFieldLabel = loFieldProps.getLabel();
         if (lsFieldLabel == null)
         {
            lsFieldLabel = lsFieldName;
         }
      }
      return lsFieldLabel;
   }


   /* Gets field value description for BSO fields that are of type BuysenseFieldDataTable */
   public static String getValueDescription(BaseObject foObject, String fsAribaName)
   {
      Log.customer.debug("FieldLabelGetter: getValueDescription() - Received this object %s", foObject);

      String lsFieldValDesc = null;
      String lsFieldName = fsAribaName;

      if (isBSOField(lsFieldName))
      {
         if (lsFieldName.indexOf(".") != -1)
         {
            // Strip out last part of field name for BSO fields which use dotted notation
            // (Only BSO fields of type BuysenseFieldDataTable will fall into this logic)
            lsFieldName = lsFieldName.substring(0,lsFieldName.lastIndexOf("."));
         }
         Object loBFDT = foObject.getFieldValue(lsFieldName);
         if (loBFDT instanceof ClusterRoot)     
         {  // BuysenseFieldDataTable is the only BSO object of type ClusterRoot 
            lsFieldValDesc = (String)foObject.getDottedFieldValue(lsFieldName + ".Description");
         }
      }
      return lsFieldValDesc;
   }

   /* Figures out whether field is BSO field or not */
   public static boolean isBSOField(String fsFieldName)
   {
      boolean lboolBSOField = false;
      String lsFieldName = fsFieldName;

      if (fsFieldName.lastIndexOf(".Name") != -1)
      {
          lsFieldName = fsFieldName.substring(0,fsFieldName.lastIndexOf(".Name"));
      }

      for (int j=0; j < moFieldValuesVector.size(); j++)
      {
         if (moFieldValuesVector.contains(lsFieldName))
         {
            lboolBSOField = true;
            break;
         }
      }
      return lboolBSOField;
   }

   /* Gets labels for a bunch of fields. The field names are expected in a List of Properties objects, 
      wherein there is one entry per "field" element on the xml. Not used at this time. */
   public static List getLabels (BaseObject foObject, String fsClientName, List foInVector)
   {
      Log.customer.debug("FieldLabelGetter: getLabels() - Received this object %s", foObject);

      List loOutVector = ListUtil.list();
      Properties loProps = null;
      String lsFieldName = null;
      String lsFieldLabel = null;

      for (int i = 0; i < foInVector.size(); i++)
      {
         loProps = (Properties)foInVector.get(i);
         lsFieldName = loProps.getProperty("ariba_field_name");

         if (lsFieldName == null)
           continue;

         lsFieldLabel = getLabel(foObject, fsClientName, lsFieldName);
         Log.customer.debug("Field Name : " + lsFieldName + "; FieldLabel : " + lsFieldLabel);
         loProps.setProperty("ariba_field_label", lsFieldLabel);
         loOutVector.add(loProps);
      }
      return loOutVector;
   }
}

