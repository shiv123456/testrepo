

package config.java.ams.custom;

import ariba.base.fields.Condition;
import ariba.util.log.Log;
import ariba.base.fields.ValueInfo;
import ariba.util.core.PropertyTable;
import ariba.common.core.SupplierLocation;

public class BuysenseSLFieldEvaluation extends Condition
{
    private static final ValueInfo parameterInfo[];


    public boolean evaluate(Object fieldValue, PropertyTable params)
    {
       String lsFieldName = params.stringPropertyForKey("TargetValue");
       SupplierLocation loSL = (SupplierLocation) params.getPropertyForKey("TargetValue1");

       //Validations
       return evaluateFields((String)fieldValue, lsFieldName, (SupplierLocation)loSL);

    }
    
    public boolean evaluateFields(String fieldValue, String fieldName, SupplierLocation slObj)
    {
    	//Commented below code for UAT requirement
    	/* if(fieldName.equals("TINLookup"))
    	{
    		String lsTIN = (String) slObj.getFieldValue("TIN");
    		if(lsTIN != null && BuysenseSupplierLocationFieldValidation.duplicateTIN(lsTIN, slObj))
    		{
            	Log.customer.debug("BuysenseSLFieldEvaluation : returning true for Duplicate TIN");
    			return true;
    		}
    	}
    	else if(fieldName.equals("LLCClassification")) **/
    	if(fieldName.equals("LLCClassification"))
    	{
        	String lsOrgType = (String) slObj.getFieldValue("OrganizationType");
        	if(lsOrgType != null && lsOrgType.startsWith("Limited Liability Co."))
            {
        		Log.customer.debug("BuysenseSLFieldEvaluation : returning true for LLCClassification");
        		return true;
            }
    	}
    	else
    	{
    		Log.customer.debug("BuysenseSLFieldEvaluation : returning false in final else");
    		return false;
    	}
    	
    	return false;
    }

    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    static
    {
        parameterInfo = (new ValueInfo[] {
            new ValueInfo("TargetValue", 0),
            new ValueInfo("TargetValue1", 0)
        });
    }

}

