/************************************************************************************
 * Author:  Richard Lee
 * Date:    June 2005
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 05/25/2007        Richard Lee            CheckChangeOrderFields
 *
 *
 * @(#)CheckChangeOrderFields.java     1.0 05/25/2007
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/

package config.java.ams.custom;
import java.util.List;

import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.base.fields.*;
import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.PurchaseOrder;
import ariba.purchasing.core.POLineItem;
import ariba.purchasing.core.Requisition;

//81->822 changed ConditionValueInfo to ValueInfo
public class CheckChangeOrderFields extends Condition
{
    private static final ValueInfo parameterInfo[];
    String message = null;
    private static final String requiredParameterNames[] =
    {
        "TargetValue"
    };
    private  static String ClassName = "config.java.ams.custom.CheckChangeOrderFields";
    
    public boolean evaluate (Object value, PropertyTable params)
    {
    	Log.customer.debug("Calling %s :: in evaluate, value = %s ", ClassName, value);
        String fieldName = (String)params.getPropertyForKey("TargetValue1");
        Requisition loReq = null;
        ReqLineItem loCurrentLI = null;
        POLineItem lPOLI = null;
        BaseObject loBO = null;
        boolean lbFinalReturnValue = true;

        if(value != null)
        {
            loBO = (BaseObject)params.getPropertyForKey("TargetValue");
            Log.customer.debug("In %s :: evaluate :: incoming object = %s and fieldName = %s ", ClassName, loBO, fieldName);
            if(loBO.instanceOf("ariba.purchasing.core.ReqLineItem") || loBO.instanceOf("ariba.purchasing.core.Requisition"))
            {
                if(fieldName.equalsIgnoreCase("UsePCardBool"))
                {
                    loReq = (Requisition) loBO;
                }
                else
                {
                	loCurrentLI = (ReqLineItem) loBO;
                	loReq = (Requisition) loCurrentLI.getLineItemCollection();
                }
                
                if(loReq.hasPreviousVersion() && isReceivingReceived((Requisition)loReq.getPreviousVersion()) && orderHasPreviousVersion((Requisition)loReq.getPreviousVersion()))
                {
                	if(loCurrentLI != null)
                	{
                		lPOLI = (POLineItem) loCurrentLI.getPOLineItem();
                		if(lPOLI == null)
                		{
                			Log.customer.debug("In %s :: evaluate :: returning true since it is a fresh line item", ClassName);
                			return true;
                		}
                	}
                	
                	if(fieldName.equalsIgnoreCase("UsePCardBool") && !(loReq.getFieldValue("UsePCardBool")).equals(loReq.getPreviousVersion().getFieldValue("UsePCardBool")))
                    {
                    	Log.customer.debug("In %s :: evaluate :: returning false for UsePCardBool due to value mismatch", ClassName);
                    	message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","NotAllowedToChangePCardFlag");
                    	lbFinalReturnValue = false;
                    }
                    else if(fieldName.equalsIgnoreCase("BillingAddress") && !(lPOLI.getBillingAddress()).equals(value))
                    {
                    	Log.customer.debug("In %s :: evaluate :: returning false for BillingAddress due to value mismatch", ClassName);
                    	message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","NotAllowedToChangeBilling");
                    	lbFinalReturnValue = false;
                    }
                    else if(fieldName.equalsIgnoreCase("Supplier") && !(lPOLI.getSupplier()).equals(value))
                    {
                    	Log.customer.debug("In %s :: evaluate :: returning false for Supplier due to value mismatch", ClassName);
                    	message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","NotAllowedToChangeContact");
                    	lbFinalReturnValue = false;
                    }
                    else if(fieldName.equalsIgnoreCase("SupplierLocation") && !(lPOLI.getSupplierLocation()).equals(value))
                    {
                    	Log.customer.debug("In %s :: evaluate :: returning false for SupplierLocation due to value mismatch", ClassName);
                    	message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","NotAllowedToChangeContact");
                    	lbFinalReturnValue = false;
                    }
                    else
                    {
                    	lbFinalReturnValue = true;
                    }
                }
            }
        }
        Log.customer.debug("In %s :: evaluate :: returning %s", ClassName, lbFinalReturnValue);
        return lbFinalReturnValue;
    }

    public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
    {
    	Log.customer.debug("%s in evaluateAndExplain, value = %s and params = %s", ClassName, value, params);
    	if(!evaluate(value, params))
        {
            if(!StringUtil.nullOrEmptyOrBlankString(message))
            {
                String retMsg = message;
                message = null;
                return new ConditionResult(retMsg);
            }
            else
            {
                return new ConditionResult(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SSLDefaultMessage"));
            }
        }
        return null;
    }

    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    static
    {
        parameterInfo = (new ValueInfo[] {
            new ValueInfo("TargetValue", 0),
            new ValueInfo("TargetValue1", 0),
            new ValueInfo("Message", 0, Behavior.StringClass)
        });
    }

    private boolean isReceivingReceived(Requisition loReq)
    {
        String lsStatusString = (String) loReq.getStatusString();
        if((lsStatusString.trim().equalsIgnoreCase("Received") || lsStatusString.trim().equalsIgnoreCase("Receiving")))
        {
        	Log.customer.debug("In %s :: isReceivingReceived :: returning true", ClassName);
        	return true;
        }
        Log.customer.debug("In %s :: isReceivingReceived :: returning false", ClassName);
        return false;
    }
    
    private boolean orderHasPreviousVersion(Requisition loReq)
    {
    	List lvOrders = loReq.getOrders();
    	for (int i = 0; i < lvOrders.size(); i++)
    	{
    		PurchaseOrder loPO = (PurchaseOrder) lvOrders.get(i);
    		if(loPO.hasPreviousVersion())
    		{
    			Log.customer.debug("In %s :: orderHasPreviousVersion :: returning true", ClassName);
    			return true;
    		}
    	}
        Log.customer.debug("In %s :: orderHasPreviousVersion :: returning false", ClassName);
    	return false;
    }
}
