package config.java.ams.custom;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.ClusterRoot;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ConditionResult;
import ariba.common.core.Supplier;
import ariba.common.core.SupplierLocation;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.user.core.User;
import ariba.util.core.ListUtil;
import ariba.util.core.PropertyTable;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseContractNumListValidation extends Condition
{
    private String        sClass                      = "BCNumListValidation";
    private String        queryString                 = null;
    private String        errorStringTable            = "ariba.procure.core";
    private String        expiredOnSubmitMsgKey       = "ExpiredContractOnSubmit";
    private String        expiredOnApproveMsgKey      = "ExpiredContractOnApprove";
    private String        selectAContractMsg          = "SelectAContract";
    private String        sNotInListUniqueNameKey     = "NotInListUniqueName";
    private String        sNotonContractUniqueNameKey = "NotonContractUniqueName";
    private String        sNotinListUniqueName        = ResourceService.getString(errorStringTable,
                                                              sNotInListUniqueNameKey);
    private String        sNotonContractUniqueName    = ResourceService.getString(errorStringTable,
                                                              sNotonContractUniqueNameKey);
    private static String sErrorMessage               = null;

    public boolean evaluate(Object obj, PropertyTable propertytable) throws ConditionEvaluationException
    {
        if (obj instanceof ReqLineItem && ((ReqLineItem) obj).getIsAdHoc())
        {
            ReqLineItem reqLI = (ReqLineItem) obj;
            Requisition req = (Requisition) reqLI.getLineItemCollection();
            if (req != null)
            {
                String sReqStatus = req.getStatusString();
                Log.customer.debug(sClass + " ReqLineItem state: " + sReqStatus);
                Object oldValues = reqLI.getDottedFieldValue("OldValues");
                String sTransactionSource = (String) req.getDottedFieldValue("TransactionSource");
                boolean bBypassApprovers = (Boolean) req.getFieldValue("BypassApprovers");
                Log.customer.debug(sClass + " ReqLineItem state: " + sTransactionSource);
                // For CER23, will not evaluate line item if it is an extenal import, or POB or QQ imports
                if(!StringUtil.nullOrEmptyOrBlankString(sTransactionSource))
                 {
                    if ((sTransactionSource.equalsIgnoreCase("E2E") || sTransactionSource
                                    .equalsIgnoreCase("QQ"))|| (sTransactionSource.equalsIgnoreCase("EXT") && bBypassApprovers))
                    {
                    	Log.customer.debug("External order imports found");
                    return true;
                }
                 }   
                else if ((sReqStatus.equalsIgnoreCase("Composing") || sReqStatus.equalsIgnoreCase("Submitted"))
                        && oldValues == null)
                {
                    return evaluateImplementation(reqLI, propertytable);
                }
            }
            return true;
        }
        return true;
    }

    public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
    {
        if (value instanceof ReqLineItem && ((ReqLineItem) value).getIsAdHoc())
        {
            ReqLineItem reqLI = (ReqLineItem) value;
            Requisition req = (Requisition) reqLI.getLineItemCollection();
            if (req != null)
            {
                String sReqStatus = req.getStatusString();
                Log.customer.debug(sClass + " ReqLineItem Status: " + sReqStatus);
                Object oldValues = reqLI.getDottedFieldValue("OldValues");
                Log.customer.debug(sClass + " ReqLineItem oldValues: " + oldValues);
                String sTransactionSource = (String) req.getDottedFieldValue("TransactionSource");
                Log.customer.debug(sClass + " ReqLineItem state: " + sTransactionSource);
                if (!StringUtil.nullOrEmptyOrBlankString(sTransactionSource)
                        && (sTransactionSource.equalsIgnoreCase("EXT") || sTransactionSource.equalsIgnoreCase("E2E") || sTransactionSource
                                .equalsIgnoreCase("QQ")))
                {
                    return null;
                }
                else if ((sReqStatus.equalsIgnoreCase("Composing") || sReqStatus.equalsIgnoreCase("Submitted"))
                        && oldValues == null)
                {
                    if (!evaluateImplementation((ReqLineItem) value, params))
                    {
                        if (sErrorMessage != null)
                        {
                            Log.customer.debug(sClass + " evaluateAndExplain-sErrorMessage:" + sErrorMessage);
                            return new ConditionResult(sErrorMessage);
                        }
                    }
                    else
                        return null;
                }
            }
            return null;
        }
        return null;
    }

    private boolean evaluateImplementation(ReqLineItem reqLI, PropertyTable propertytable)
    {
        Supplier supplier = reqLI.getSupplier();
        SupplierLocation supplierLocation = reqLI.getSupplierLocation();
        Object objValue = reqLI.getDottedFieldValue("ContractNumList");
        BaseId objBaseID = (BaseId) reqLI.getDottedFieldValue("ContractNumList.BaseId");
        String sReqStatus = reqLI.getLineItemCollection().getStatusString();
        AQLResultCollection aqlResults = null;
        aqlResults = getResults(reqLI);
        List lNonContracts = ListUtil.list();
        //lNonContracts = Not In list, Not On Contract
        List lValidContracts = ListUtil.list();
        //lValidContracts = Actual contracts        
        if (aqlResults != null)
        {
            if (aqlResults.getErrors() != null)
            {
                Log.customer.debug(sClass + " Errors in results: " + aqlResults.getErrors());
                return true;
            }
            else
            {
                try
                {
                    while (aqlResults.next())
                    {
                        BaseId b = (BaseId) (BaseId) aqlResults.getObject(0);
                        ClusterRoot cObj = (ClusterRoot) b.get();
                        String sUniqueName = (String) cObj.getDottedFieldValue("UniqueName");
                        if (!StringUtil.nullOrEmptyOrBlankString(sUniqueName)
                                && (sUniqueName.equals(sNotinListUniqueName) || sUniqueName
                                        .equals(sNotonContractUniqueName)))
                        {
                            lNonContracts.add(b);
                        }
                        else
                        {
                            lValidContracts.add(b);
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.customer.debug(sClass + " Exception: " + e);
                }
            }
        }
        Log.customer.debug(sClass + " lValidContracts: " + lValidContracts.size());
        Log.customer.debug(sClass + " lNonContracts: " + lNonContracts.size());

        if (supplier == null || supplierLocation == null)
        {
            return true;
        }
        else
        {
            if (objValue == null)
            {
                if (lValidContracts.isEmpty())
                {
                    //if selected supplier does not have Valid Contracts, return true
                    return true;
                }
                else
                {
                    //There are some valid contracts available to user, but user has selected none, throw error.
                    sErrorMessage = ResourceService.getString(errorStringTable, selectAContractMsg);
                    return false;
                }
            }
            else
            {
                // if the selected object is NIL or NOC, return true
                // else if the selected object is in validList, return true
                // else if the selected object might have expired or is of different supplier, return false
                if (lNonContracts.contains(objBaseID))
                {
                    return true;
                }
                else if (lValidContracts.contains(objBaseID))
                {
                    return true;
                }
                else
                {
                    if (sReqStatus.equalsIgnoreCase("Composing"))
                    {
                        sErrorMessage = ResourceService.getString(errorStringTable, expiredOnSubmitMsgKey);
                        Log.customer.debug(sClass + " user selected value that is not a valid- returning false");
                        return false;
                    }
                    else if (sReqStatus.equalsIgnoreCase("Submitted"))
                    {
                        sErrorMessage = ResourceService.getString(errorStringTable, expiredOnApproveMsgKey);
                        Log.customer.debug(sClass + " user selected value that is not a valid- returning false");
                        return false;
                    }
                    return true;
                }
            }
        }
    }

    private AQLResultCollection getResults(ReqLineItem reqLI)
    {
        if (reqLI != null)
        {
            /**************************************************************************************************
            * CSPL-1890 
            * Use supplier uniquename to return BuysenseContracts instead of using supplierlocation uniquename
            ****************************************************************************************************/
            Supplier loSupplier = reqLI.getSupplier();
            if (loSupplier != null)
            {
                String supplierId = loSupplier.getUniqueName();

                    queryString = "SELECT s FROM ariba.core.BuysenseContracts s where s.SupplierID in ('" + supplierId
                            + "','ALL')";
                    Requisition req = (Requisition) reqLI.getLineItemCollection();
                    User requester = req.getRequester();
                    ariba.common.core.User partitionedUser = ariba.common.core.User.getPartitionedUser(requester, req
                            .getPartition());
                    String sClinetUniqueName = (String) partitionedUser.getDottedFieldValue("ClientName.ClientName");
                    sClinetUniqueName = sClinetUniqueName.trim().substring(0, 4);
                    if (!StringUtil.nullOrEmptyString(sClinetUniqueName))
                    {
                        queryString += " AND s.AuthorizedAgency  in ('" + sClinetUniqueName + "','ALL')";
                    }
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    df.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
                    String lsDateTime = df.format(Calendar.getInstance().getTime());
                    lsDateTime = lsDateTime +" 00:00:00";
                    queryString += " AND s.EffectiveBeginDate <= CurrentDate()";
                    queryString += " AND s.EffectiveEndDate >= CalendarDate('" + lsDateTime + "')";
                    Log.customer.debug(sClass + " query: " + queryString);
                    AQLQuery aqlQuery = AQLQuery.parseQuery(queryString);
                    AQLOptions aqlOptions = new AQLOptions(req.getPartition());
                    AQLResultCollection results = Base.getService().executeQuery(aqlQuery, aqlOptions);
                    return results;
                }

        }
        return null;
    }

}
