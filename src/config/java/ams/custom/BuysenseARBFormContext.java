package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.BaseObject;
import ariba.htmlui.eform.wizards.ARBFormContext;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseARBFormContext extends ARBFormContext
{

    public String getApprovalFlowHint()
    {
        Log.customer.debug("Inside BuysenseARBFormContext");
        String sHint = "@ariba.commonui.hint/ApprovalFlowHint";
        Approvable oEform = getApprovable();
        if (((BaseObject) oEform).instanceOf("ariba.core.BuysDynamicEform"))
        {
            Object oCustomDynamicHint = oEform.getDottedFieldValue("EformChooser.ApprovalWarningMessage");
            if (oCustomDynamicHint != null && !StringUtil.nullOrEmptyOrBlankString((String) oCustomDynamicHint))
            {
                sHint = "<font face=Arial size=2 color=#FF0000><b>" + (String) oCustomDynamicHint + "</b></font>";
            }
        }
        return sHint;
    }

    public String getFormFieldsHint()
    {
        String sFormHint = "@buysense.DynamicEform/FieldsPageHint";
        Log.customer.debug("Inside getFormFieldsHint");
        Approvable oEform = getApprovable();
        if (((BaseObject) oEform).instanceOf("ariba.core.BuysDynamicEform"))
        {
            Object oCustomDynamicHint = oEform.getDottedFieldValue("EformChooser.EformWarningMessage");
            if (oCustomDynamicHint != null && !StringUtil.nullOrEmptyOrBlankString((String) oCustomDynamicHint))
            {
                sFormHint = "<font face=Arial size=2 color=#FF0000><b>" + (String) oCustomDynamicHint + "</b></font>";
            }
        }
        return sFormHint;
    }

    public String getReviewHint()
    {
        String sFormSaveHint = "@ariba.commonui.hint/FormSaveReviewHint";
        String sFormReviewHint = "@ariba.commonui.hint/FormReviewHint";
        Log.customer.debug("Inside getReviewHint");
        if (getWasCheckedOut())
        {
           return sFormSaveHint;
        }
        else
        {
            Approvable oEform = getApprovable();
            if (((BaseObject) oEform).instanceOf("ariba.core.BuysDynamicEform"))
            {
                Object oCustomDynamicHint = oEform.getDottedFieldValue("EformChooser.SummaryWarningMessage");
                if (oCustomDynamicHint != null && !StringUtil.nullOrEmptyOrBlankString((String) oCustomDynamicHint))
                {
                    sFormReviewHint = "<font face=Arial size=2 color=#FF0000><b>" + (String) oCustomDynamicHint + "</b></font>";
                    return sFormReviewHint;
                }
                else
                {
                    return sFormSaveHint;
                }
            }
        return sFormReviewHint;
        }
    }
}
