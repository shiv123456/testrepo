/************************************************************************************
 * Author:  Richard Lee
 * Date:    September 22, 2006
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 09/22/2006        Richard Lee            Prod 1029 - SWAM not initialized for non-catalog 
 *                                                      ReqLineItems
 *
 * @(#)SetSWAMOnCopy.java     1.0 9/22/2006
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/

/****************************
 * This trigger copies the SWAM fields on the Line Item Supplier to the Line Item Description
 * in order to have the most recent Supplier SWAM status on the copied Requisition.
 *
*****************************/
package config.java.ams.custom;

import ariba.purchasing.core.ReqLineItem;
import ariba.base.fields.*;
import ariba.util.core.PropertyTable;
import java.util.List;
import ariba.util.log.Log;
import ariba.purchasing.core.Requisition;

//81->822 changed Vector to List
public class SetSWAMOnCopy extends Action
{
    public void fire(ValueSource object, PropertyTable params)
    {
        Requisition loCopiedReq=(Requisition)object;
        List bvLineItems = (List) loCopiedReq.getFieldValue("LineItems");
        ReqLineItem loLineItem = null;
        Boolean lbSupplierSmall = new Boolean("false");
        Boolean lbSupplierWoman = new Boolean("false");
        Boolean lbSupplierMinority = new Boolean("false");
        Boolean lbSupplierMicroBusiness = new Boolean("false");
        
        try
        {
            for (int i=0; i<(bvLineItems.size()); i++)
            {
                loLineItem = (ReqLineItem)bvLineItems.get(i);
                lbSupplierSmall = (Boolean) loLineItem.getDottedFieldValue("Supplier.SmallBusiness");
                lbSupplierWoman = (Boolean) loLineItem.getDottedFieldValue("Supplier.WomanOwnedBusiness");
                lbSupplierMinority = (Boolean) loLineItem.getDottedFieldValue("Supplier.MinorityOwnedBusiness");
                lbSupplierMicroBusiness = (Boolean) loLineItem.getDottedFieldValue("Supplier.MicroBusiness");

                loLineItem.setDottedFieldValue("Description.SmallBusiness", lbSupplierSmall);
                loLineItem.setDottedFieldValue("Description.WomanOwnedBusiness", lbSupplierWoman);
                loLineItem.setDottedFieldValue("Description.MinorityOwnedBusiness", lbSupplierMinority);
                loLineItem.setDottedFieldValue("Description.MicroBusiness", lbSupplierMicroBusiness);
            }
        }
        catch (Exception ex)
        {
            String lsMsg = ex.getMessage();
            Log.customer.debug("Error exception while setting LineItems vector SWAM fields: " + lsMsg);
        }
    }
}
