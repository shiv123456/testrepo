// Anup - Can now use buysense as a logging category

package config.java.ams.custom ;

import ariba.util.log.Logger;

public class Logs extends ariba.util.log.Log
{
    public static final String ClassName = "config.java.ams.custom.Logs";

    //public static final Logger buysense =
        //new Logger("buysense");
    public static final Logger buysense = (Logger) Logger.getLogger("buysense");
}

