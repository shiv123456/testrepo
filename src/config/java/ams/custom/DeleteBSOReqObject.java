package config.java.ams.custom;

import java.util.List;

import ariba.approvable.core.Approvable;
import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.FieldProperties;
import ariba.base.fields.FieldPropertiesLibrary;
import ariba.base.fields.Fields;
import ariba.base.fields.ValueSource;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.util.core.Date;
import ariba.util.core.Fmt;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

/** 
* @author Manoj Gaur
* @version 1 DEV
* @reference CSPL-593
* @see Date 08-12-2008
* @Explanation Getting fields from different groups and nullify those objects if they have ERPValue is n/a.
* */
public class DeleteBSOReqObject extends Action {

	public void fire(ValueSource anchor, PropertyTable propertytable) throws ActionExecutionException
	{
		Requisition objRequisition = null;
		if (anchor != null && anchor instanceof Requisition)
			objRequisition = (Requisition) anchor;				
		if(objRequisition != null)
		{
			Log.customer.debug(" Req Object: %s",objRequisition);
			nullifyObj("HeaderDetailsEditable",FieldListContainer.getHeaderFieldValues(),objRequisition);
			nullifyObj("LineItemGeneralFields",FieldListContainer.getLineFieldValues(),objRequisition);
			nullifyObj("ProcureAccountingFields",FieldListContainer.getAcctgFieldValues(),objRequisition);
		}
	}
	
	public void nullifyObj(String Group,List fields,Approvable objAppr)
	{
		Log.customer.debug("Entered in nullifyObj method for "+Group+" and Approvable "+objAppr);
		if(objAppr!=null && objAppr instanceof Requisition)
		{	
			Requisition objReq = (Requisition) objAppr;
			List fieldNames = fields;
			
			String sFieldName;
			String sFullFieldName="";		
			
			for (int i = 0; i < fieldNames.size(); i++)
			{
				sFieldName = (String) fieldNames.get(i);
				Log.customer.debug("Looping through "+Group+" for field "+sFieldName);
				if(Group.equals("LineItemGeneralFields"))
				{
					List lineItems = objReq.getLineItems();	
					for(int j=0; j<lineItems.size();j++)
					{
						sFullFieldName = Fmt.S("%s.%s","LineItems["+new Integer(j).toString()+"]",sFieldName);						
						setNull(objAppr,sFullFieldName);
					}
					Log.customer.debug("Process complete for"+sFullFieldName+" under "+Group);
				}
				else if (Group.equals("ProcureAccountingFields"))
				{
					Log.customer.debug("Coming inside Group: "+Group);
					List lineItems = objReq.getLineItems();	
					for(int j=0; j<lineItems.size();j++)
					{
						ReqLineItem objReqLineItem = (ReqLineItem)lineItems.get(j);
						List accountings = objReqLineItem.getAccountings().getAllSplitAccountings();
						for(int k=0; k<accountings.size();k++)
						{
							sFullFieldName = Fmt.S("%s.%s.%s","LineItems["+new Integer(j).toString()+"]","Accountings.SplitAccountings["+new Integer(k).toString()+"]",sFieldName);							
							setNull(objAppr,sFullFieldName);								
						}
					}
					Log.customer.debug("Process complete for"+sFullFieldName+" under "+Group);
				}
				else 
				{
					sFullFieldName = sFieldName;
					setNull(objAppr,sFullFieldName);					
				}				
			}		
		}
		else
		{
			Log.customer.debug("Object is not instance of Requisition");
		}
	}	
	public static void setNull(Approvable appr,String fieldName)
	{
		try
		{
			Object obj = appr!=null? appr.getDottedFieldValue(fieldName) : null;
			ClusterRoot objBuysenseFDT;
			if(obj instanceof ClusterRoot)
			{
				objBuysenseFDT = (ClusterRoot) obj;
				String bERPVal = (String)objBuysenseFDT.getDottedFieldValue("FieldTable.ERPValue");
				if(bERPVal!= null && bERPVal.equals("n/a"))
					appr.setDottedFieldValue(fieldName, null);
			}
		}
		catch(Exception e)
		{
			Log.customer.debug(" Exception from DeleteBSOReqObject.setNull()");
			e.printStackTrace();
		}
	}
	public static List getFieldNames(String className, String Group)
	{
		FieldPropertiesLibrary fpl = Fields.getService().getFpl(className,Base.getSession().getVariant());
		return FieldProperties.getFieldsInGroup(Group, fpl);
	}
	
}