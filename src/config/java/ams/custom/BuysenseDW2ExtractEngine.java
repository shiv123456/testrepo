package config.java.ams.custom;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.StringTokenizer;

import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseSession;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseDW2ExtractEngine
{
    private Map<String, BuysenseDWFieldDetails[]>    aribaFieldDetailsMap           = new HashMap<String, BuysenseDWFieldDetails[]>();
    private Stack<BuysenseDWHierarchyDetailsObj>     aribaObjStack                  = new Stack();
    private BaseObject                               currentBaseObj;
    private BaseObject                               newBaseObject;
    private BaseObject                               newBaseObjectRecur;
    private String                                   msCN                           = "BuysenseDW2ExtractEngine";
    private JsonGenerator                            jsonGenerator;
    private ArrayList<BuysenseDWHierarchyDetailsObj> aribaHierachyObjList;
    BuysenseDW2ExtractRecursiveElements              recurElements                  = new BuysenseDW2ExtractRecursiveElements();

    private Stack<String>                            aribaObjStackForAllowedInVals  = new Stack();
    static int                                       BRACETYPE_WRITEOBJFIELDSTART   = 1;
    static int                                       BRACETYPE_WRITESTARTOBJ        = 2;
    static int                                       BRACETYPE_WRITEARRAYFIELDSTART = 3;

    // for method invocation ... all reflection invokable methods use a single parameter ... BaseObject
    private Class[]                                  moMethodParmTypes              = null;

    public String extractObj(String inputObjJson, String inputFieldJson, BaseObject docBaseObj) throws Exception
    {

        Type typefield = new TypeToken<Map<String, ArrayList<BuysenseDWFieldDetailsObj>>>()
        {
        }.getType();
        Map<String, ArrayList<BuysenseDWFieldDetailsObj>> aribaFields = new Gson().fromJson(inputFieldJson, typefield);
        Iterator it = aribaFields.entrySet().iterator();
        Map.Entry fieldDetailLvl1;
        ObjectMapper mapper = null;
        JsonNode rootNode = null;
        JsonNode rootNodeEmptyVal = null;
        String sHierarchyObjName = "";

        while (it.hasNext())
        {
            fieldDetailLvl1 = (Map.Entry) it.next();
            Log.customer.debug("*****%s***** found key/value key:" + fieldDetailLvl1.getKey() + " val:" + fieldDetailLvl1.getValue() + "\n", msCN);
            ArrayList<BuysenseDWFieldDetailsObj> fieldObjs = (ArrayList<BuysenseDWFieldDetailsObj>) fieldDetailLvl1.getValue();
            for (int i = 0; i < fieldObjs.size(); i++)
            {
                Log.customer.debug("*****%s***** found hierarchy fieldobj:" + fieldObjs.get(i).getObjectHierarchyName() + "\n", msCN);
                aribaFieldDetailsMap.put(fieldObjs.get(i).getObjectHierarchyName(), fieldObjs.get(i).getBuysenseFieldDetails());
            }
        }

        Type typehierarchy = new TypeToken<Map<String, ArrayList<BuysenseDWHierarchyDetailsObj>>>()
        {
        }.getType();

        Map<String, ArrayList<BuysenseDWHierarchyDetailsObj>> aribaHierarchyObjMap = new Gson().fromJson(inputObjJson, typehierarchy);
        Iterator it2 = aribaHierarchyObjMap.entrySet().iterator();

        while (it2.hasNext())
        {
            Map.Entry pairs = (Map.Entry) it2.next();
            Log.customer.debug("*****%s***** HHH found key/value key:" + pairs.getKey() + " val:" + pairs.getValue() + "\n", msCN);
            aribaHierachyObjList = (ArrayList<BuysenseDWHierarchyDetailsObj>) pairs.getValue();
            Log.customer.debug("*****%s***** HHH details list: " + aribaHierachyObjList + "details list size:" + aribaHierachyObjList.size() + "\n", msCN);

            Log.customer.debug("********** HHHHHHHH class:" + aribaHierachyObjList.get(0).getClass());

            for (int i = 0; i < aribaHierachyObjList.size(); i++)
            {
                Log.customer.debug("*****%s***** HHH found fieldobj:" + aribaHierachyObjList.get(i).getBuysenseAribaObjHierarchy() + "\n", msCN);
                // process the XML here
            }
        }
        if (aribaHierachyObjList == null)
        {
            return "";
        }
        ByteArrayOutputStream bOutputJson = new ByteArrayOutputStream();

        try
        {
            jsonGenerator = (new JsonFactory()).createJsonGenerator(bOutputJson,JsonEncoding.UTF8);
            // for pretty printing - not needed as we want compact view
            //jsonGenerator.setPrettyPrinter(new DefaultPrettyPrinter());
            int aribaObjCount = 0;
            // you have every list populated
            jsonGenerator.writeStartObject(); // starting root object
            aribaObjCount++;
            // clean stack before processing
            if (aribaObjStack != null && !aribaObjStack.isEmpty())
            {
                aribaObjStack.clear();
            }
            Log.customer.debug("*****%s***** >>>> calling populateJsonObj ", msCN);
            populateJsonObj(docBaseObj, 0, false, false, 0, false, true); // first one is false
            Log.customer.debug("*****%s***** >>>>  DONE calling populateJsonObj", msCN);
            jsonGenerator.writeEndObject(); // closing root object
            jsonGenerator.flush();
            jsonGenerator.close();
        } // Don't catch the exception. Parent class will handle it
        finally
        {
            try
            {
                jsonGenerator.flush();
                jsonGenerator.close();
            }
            catch (Exception ex)
            {
            }
        }

        // JB - start allowedinvalues
        // now that we have all the data, filter it based on 'allowedinvalues'
        // need to re-process
        try
        {
            mapper = new ObjectMapper();
            rootNode = mapper.readTree(new String( bOutputJson.toByteArray(),"UTF-8"));
            sHierarchyObjName = aribaHierachyObjList.get(0).getBuysenseAribaObjHierarchy().getObjectHierarchyName();
            boolean validObj = checkAllowedInValues(rootNode, sHierarchyObjName); // process using aribaHierachyObjList and aribaFieldDetailsMap
           Log.customer.debug(msCN + "::rootNode after allowedinvalues are processed:" + rootNode.toString());
            // remove empty records from a vector
            removeNullNodesInJSON(rootNode);

        }
        finally
        {
        }
        return mapper.writeValueAsString(rootNode);
        // return bOutputJson.toString()
    }

    // startNextObj is where to begin looping
    // Example: Supplier object could have
    // 0 - Supplier
    // 1 - Supplier:CommonSupplier
    // 2 - Supplier:CommonSupplier:OrganizationID
    // 3 - Supplier:CommonSupplier:OrganizationID:Ids and then
    // 4 - Supplier:Locations
    // currObj - 0 and startNextObj - 0
    // currObj - 0 and startNextObj - 4

    private void populateJsonObj(BaseObject poObj, int currObj, boolean isObjLevelVector, boolean isObjLevelRecursive, int startNextObj, boolean bLastIteration, boolean bPushToStack) throws Exception
    {
        BuysenseDWHierarchyDetailsObj popDWHierarchyDetails;
        ObjectMapper mapperRecur = null;
        JsonNode rootNodeRecur = null;
        int nextObjVal = 0;
        BaseObject temp3BaseObject = null;

        try
        {
            if (aribaHierachyObjList == null || aribaHierachyObjList.size() == 0 || aribaHierachyObjList.size() < (currObj + 1))
            {
                Log.customer.debug("*****%s***** emptying list", msCN);

                while (!aribaObjStack.empty())
                {
                    popDWHierarchyDetails = aribaObjStack.pop();
                    while (!popDWHierarchyDetails.getBuysenseAribaObjHierarchy().isBracesEmpty())
                    {
                        addClosingBraces(popDWHierarchyDetails);
                    }
                }
                return;
            }

            // push the current level aribaHierachyObjList[currObj].getLevel and obj onto stack
            // PurchaseOrder:LineItems
            // PurchaseOrder
            aribaHierachyObjList.get(currObj).getBuysenseAribaObjHierarchy().setAribaBaseObject(poObj);
            // Skip Pushing to the stack for vector iterations
            Log.customer.debug("*****%s***** bPushToStack: " + bPushToStack, msCN);
            if (bPushToStack)
            {
                aribaObjStack.push(aribaHierachyObjList.get(currObj));
            }

            Log.customer.debug("*****%s***** check 1 stack", msCN);
            printAribaObjStack(aribaObjStack);

            Log.customer.debug("*****%s***** pushing on stack level: %s, currobj num:%s   :startNextObj:%s", msCN, aribaHierachyObjList.get(currObj).getBuysenseAribaObjHierarchy()
                    .getObjectHierarchyName(), (new Integer(currObj)).toString(), (new Integer(startNextObj)).toString());

            if ((currObj == 0) && (currObj != startNextObj)) // / should happen only at the begining
                                                             // where both have value '0'
            {
                ; // do nothing
            }
            else
            {
                // assuming it is not a vector for the first time?? verify
                if (!isObjLevelVector)
                {
                    jsonGenerator.writeObjectFieldStart(aribaHierachyObjList.get(currObj).getBuysenseAribaObjHierarchy().getDWOutputStr()); // starting
                                                                                                                                            // current
                                                                                                                                            // object
                    aribaObjStack.peek().getBuysenseAribaObjHierarchy().pushBracesStack(BRACETYPE_WRITEOBJFIELDSTART);
                    Log.customer.debug("*****%s***** !isObjLevelVector writeObjectFieldStart JNNNNNNNNNNN-{", msCN);
                }

                if (poObj != null)
                {
                    BuysenseDW2ExtractUtils.getFieldsForObjectLevel(jsonGenerator, aribaFieldDetailsMap.get(aribaHierachyObjList.get(currObj).getBuysenseAribaObjHierarchy().getObjectHierarchyName()),
                            poObj, aribaHierachyObjList.get(currObj));
                }
            }

            Log.customer.debug(msCN + "::DONE - getFieldsForObjectLevel()");
            Log.customer.debug("*****%s***** DONE - getFieldsForObjectLevel()", msCN);

            // if ((aribaHierachyObjList.size() == (currObj + 1)) || (aribaHierachyObjList.size() ==
            // (startNextObj + 1))) // done processing all the objects
            if (aribaHierachyObjList.size() == (currObj + 1)) // done processing all the objects
            {
                // pop the stack and finish processing
                // aribaObjStack.pop();
                Log.customer.debug("*****%s***** done processing all the objects", msCN);
                /*
                 * if (aribaObjStack != null && !aribaObjStack.isEmpty()) { while
                 * (!aribaObjStack.empty()) { popDWHierarchyDetails = aribaObjStack.pop();; while
                 * (!popDWHierarchyDetails.getBuysenseAribaObjHierarchy().isBracesEmpty()) {
                 * addClosingBraces(popDWHierarchyDetails); } }
                 * 
                 * aribaObjStack.clear(); // clear the stack }
                 */
                return; // fields are fetched
            }

            // next one exists
            // check if next one is inside current one or not
            int nextObj = 0;
            if ((currObj == 0) && (startNextObj == 0)) // / should happen only at the begining where
                                                       // both have value '0'
            {
                nextObj = currObj + 1;
            }
            else if ((currObj == 0) && (startNextObj != 0))
            {
                nextObj = startNextObj;
            }
            else
            // if recursive, don't go to the next level
            {
                nextObj = currObj + 1;
            }
            String tempNextLevel = aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getObjectHierarchyName();
            String tempCurrentLevel = aribaHierachyObjList.get(currObj).getBuysenseAribaObjHierarchy().getObjectHierarchyName();
            boolean isCurrVector = aribaHierachyObjList.get(currObj).getBuysenseAribaObjHierarchy().getIsVector();
            Log.customer.debug("*****%s***** isCurrVector: " + isCurrVector, msCN);
            BaseObject tempBaseObject = null;
            BuysenseDWHierarchyDetails tempDWHierarchyDetails = null;
            BuysenseDWHierarchyDetailsObj tempDWHierarchyDetailsObj = null;

            Log.customer.debug("*****%s***** nextObj chosen", msCN);
            String[] tempNextLevelTok = tempNextLevel.split(":");
            String tempNextLevelStart = tempNextLevel.substring(0, tempNextLevel.length() - (tempNextLevelTok[tempNextLevelTok.length - 1].length() + 1));

            Log.customer.debug(msCN + "::tempNextLevel:" + tempNextLevel + "; tempCurrentLevel: " + tempCurrentLevel + "; tempNextLevelStart:" + tempNextLevelStart);

            // need to find exact match. Example: PO.Supplier, PO.SupplierLocation
            if (StringUtil.equalsIgnoreCase(tempNextLevelStart, tempCurrentLevel))
            {
                currentBaseObj = poObj;
                // close braces though
            }
            else
            {
                // using same array in 2 diff format??
                StringTokenizer st2 = new StringTokenizer(tempNextLevel, ":");
                boolean isCurrVector2 = false;

                // need to find exact match. Example: PO.Supplier, PO.SupplierLocation
                if (!StringUtil.equalsIgnoreCase(tempNextLevelStart, tempCurrentLevel) && st2 != null && (st2.countTokens() == 2))
                {
                    Log.customer.debug("*****%s***** returning 2222", msCN);
                    return; // return at this point
                    // tempNextLevel => 'Supplier:Locations'
                    // tempCurrentLevel => 'Supplier:CommonSupplier'
                }

                Log.customer.debug("*****%s***** bLastIteration: " + bLastIteration, msCN);
                if (isCurrVector && bLastIteration) // if current is a vector then just return
                                                    // instead of popping the objects!
                {
                    // addOneClosingBrace(aribaHierachyObjList.get(currObj));
                    // return;
                    Log.customer.debug("*****%s***** poping the stack instead of  returning on isCurrVector", msCN);
                    tempDWHierarchyDetailsObj = aribaObjStack.pop();
                    Log.customer.debug("*****%s***** tempDWHierarchyDetailsObj: " + tempDWHierarchyDetailsObj, msCN);
                    Log.customer.debug("*****%s***** aribaHierachyObjList.get(currObj): " + aribaHierachyObjList.get(currObj), msCN);
                    addClosingBraces(tempDWHierarchyDetailsObj);
                }

                while (!StringUtil.equalsIgnoreCase(tempNextLevelStart, tempCurrentLevel))
                {
                    printAribaObjStack(aribaObjStack);
                    tempDWHierarchyDetailsObj = aribaObjStack.peek();
                    Log.customer.debug("*****%s*****  next level to be processed aaa tempDWHierarchyDetailsObj: " + tempDWHierarchyDetailsObj.toString(), msCN);
                    tempDWHierarchyDetails = tempDWHierarchyDetailsObj.getBuysenseAribaObjHierarchy();
                    Log.customer.debug("*****%s*****  next level to be processed aaa tempDWHierarchyDetails: " + tempDWHierarchyDetails, msCN);
                    tempBaseObject = tempDWHierarchyDetails.getAribaBaseObject();
                    Log.customer.debug("*****%s*****  next level to be processed aaa tempBaseObject: " + tempBaseObject, msCN);
                    tempCurrentLevel = tempDWHierarchyDetails.getObjectHierarchyName();

                    isCurrVector2 = tempDWHierarchyDetailsObj.getBuysenseAribaObjHierarchy().getIsVector();

                    Log.customer.debug("*****%s*****  next level to be processed aaa isCurrVector2: " + isCurrVector2, msCN);
                    Log.customer.debug("*****%s*****  next level to be processed aaa tempNextLevelStart: " + tempNextLevelStart + ", tempCurrentLevel: " + tempCurrentLevel, msCN);

                    if (!StringUtil.equalsIgnoreCase(tempNextLevelStart, tempCurrentLevel))
                    {
                        // if it doesn't match then remove it. Otherwise keep it on the stack!

                        if (isCurrVector2)
                        {
                            Log.customer.debug("*****%s*****  breaking the loop since isCurrVector2 is true.", msCN);

                            bPushToStack = false;
                            return;
                        }
                        else
                        {
                            printAribaObjStack(aribaObjStack);
                            Log.customer.debug("*****%s***** poping next level", msCN);
                            tempDWHierarchyDetailsObj = aribaObjStack.pop();
                            Log.customer.debug("*****%s*****  popped tempDWHierarchyDetailsObj: " + tempDWHierarchyDetailsObj.toString(), msCN);
                            addClosingBraces(tempDWHierarchyDetailsObj);
                            Log.customer.debug("*****%s*****  calling addClosingBraces 1:", msCN);
                        }
                    }
                    /*
                     * else //StringUtil.equalsIgnoreCase(tempNextLevelStart, tempCurrentLevel) {
                     * int braceType; while
                     * (!tempDWHierarchyDetailsObj.getBuysenseAribaObjHierarchy().isBracesEmpty()) {
                     * braceType =
                     * aribaHierachyObjList.get(currObj).getBuysenseAribaObjHierarchy().popBracesStack
                     * (); if (braceType == BRACETYPE_WRITESTARTOBJ) {
                     * jsonGenerator.writeEndObject();
                     * Log.customer.debug("*****%s***** writeEndObject JNNNNNNNNNNN-}", msCN); }
                     * else if (braceType == BRACETYPE_WRITEOBJFIELDSTART) {
                     * jsonGenerator.writeEndObject();
                     * Log.customer.debug("*****%s***** writeEndObject JNNNNNNNNNNN-}", msCN); }
                     * else if (braceType == BRACETYPE_WRITEARRAYFIELDSTART) {
                     * jsonGenerator.writeEndArray();
                     * Log.customer.debug("*****%s***** writeEndObject JNNNNNNNNNNN-]", msCN); } } }
                     */
                    Log.customer.debug(msCN + "XXXXX: tempNextLevelStart" + tempNextLevelStart);
                }

                if (aribaObjStack.empty())
                {
                    currentBaseObj = tempBaseObject;
                }
                else
                { // probably don't need this condition at all -JB
                    currentBaseObj = aribaObjStack.peek().getBuysenseAribaObjHierarchy().getAribaBaseObject();
                }
                // keep removing till you match re
            }

            Log.customer.debug("*****%s***** check 2 stack", msCN);
            printAribaObjStack(aribaObjStack);

            boolean b = aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getIsVector();
            boolean isRecursive = aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getIsRecursive();

            Log.customer.debug(msCN + "::tempNextLevel:" + b);

            if (isRecursive)
            {
                // retrieve it in flat structure --TBD
                // Dependencies{
                // DependenciesVECTOR[
                // {
                // APPROVEDBYID:
                // APPROVEDBYNAME:
                // }
                // {
                // APPROVEDBYID:
                // APPROVEDBYNAME:
                // }
                // {
                // APPROVEDBYID:
                // APPROVEDBYNAME:
                // }
                // ]
                // }
                // aribaFieldDetailsMap
                BuysenseDWFieldDetails[] recurFieldList = aribaFieldDetailsMap.get(aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getObjectHierarchyName());
                String recursiveJSONStr = recurElements.getRecursiveJSON(currentBaseObj, aribaHierachyObjList.get(nextObj), recurFieldList);

                if (!StringUtil.nullOrEmptyOrBlankString(recursiveJSONStr))
                {
                    mapperRecur = new ObjectMapper();
                    rootNodeRecur = mapperRecur.readTree(recursiveJSONStr);
                    String nodeNameRec = aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getDWOutputStr();
                    JsonNode recurDataNode = rootNodeRecur.get(nodeNameRec);
                    Log.customer.debug(msCN + ": recurring obj:" + recurDataNode.toString());
                    jsonGenerator.writeObjectFieldStart(nodeNameRec);
                    if (recurDataNode != null)
                    {
                        String recurDataNodeStr = recurDataNode.toString();
                        int indexOfOpenBracket = recurDataNodeStr.indexOf("{");
                        int indexOfLastBracket = recurDataNodeStr.lastIndexOf("}");
                        Log.customer.debug(msCN + ": recurring vec obj:" + recurDataNodeStr);
                        String recurData = recurDataNodeStr.substring(indexOfOpenBracket + 1, indexOfLastBracket);
                        Log.customer.debug(msCN + ": recurring data:" + recurData);

                        // writeRaw throws an exception if special characters are found in JSON
                        // one way to prevent is to replace all special characters with '?'. We do not want to replace at this point
                        // START CODING
                        //CharsetDecoder utf8Decoder = Charset.forName("UTF-8").newDecoder();
                        //utf8Decoder.onMalformedInput(CodingErrorAction.REPLACE);
                        //utf8Decoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
                        //utf8Decoder.replaceWith("?");
                        // UTF-8 decoding
                        //CharBuffer outputUTF8Json = utf8Decoder.decode(ByteBuffer.wrap(recurData.getBytes()));
                        // jsonGenerator.writeRaw(outputUTF8Json.toString());
                        //END CODING
                        jsonGenerator.writeRaw(recurData);
                        
                    }
                    jsonGenerator.writeEndObject();
                }
            }
            else if (b) // don't process recursive items in vector sector
            {
                // loop through all the values
                // peek and check the top value PO.LI is in PO, PO.LI.Acc is in PO.LI and so on
                java.util.List lvRecordsVector;
                int liRecordIndex = 0;
                Log.customer.debug(msCN + ": current obj:" + currentBaseObj);
                Log.customer.debug("*****%s*****  next level to be processed %s:" + " obj name:" + aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getAribaObjname(), msCN,
                        tempCurrentLevel);

                if (currentBaseObj == null)
                {

                    Log.customer.debug(msCN + ": currentBaseObj is null. Processing next level with dummy object. START");

                    jsonGenerator.writeObjectFieldStart(aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getDWOutputStr());

                    aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().pushBracesStack(BRACETYPE_WRITEOBJFIELDSTART);
                    jsonGenerator.writeArrayFieldStart(aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getDWOutputStr() + "VECTOR");
                    aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().pushBracesStack(BRACETYPE_WRITEARRAYFIELDSTART);

                    Log.customer.debug("*****%s*****  next level to be processed %s:" + " obj name:" + aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getAribaObjname(), msCN,
                            tempCurrentLevel);

                    aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().printBracesStack();
                    jsonGenerator.writeStartObject();
                    aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().pushBracesStack(BRACETYPE_WRITESTARTOBJ);

                    /*
                     * boolean lastIteration2 = true, bPushToStack1= true; Partition partition =
                     * Base.getService().getPartition("pcsv"); BaseObject oDummyBaseObject =
                     * (BaseObject) BaseObject.create("ariba.core.Product", partition);
                     * Log.customer.debug(msCN + ": oDummyBaseObject: "+oDummyBaseObject);
                     * 
                     * populateJsonObj(oDummyBaseObject, nextObj,
                     * aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy
                     * ().getIsVector(),
                     * aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy
                     * ().getIsRecursive(), nextObj, lastIteration2, bPushToStack1);
                     * 
                     * if(lastIteration2) { Log.customer.debug(msCN + ": addOneClosingBrace()");
                     * addOneClosingBrace(aribaHierachyObjList.get(nextObj)); }
                     */
                    Log.customer.debug(msCN + ": currentBaseObj is null. Processing next level with dummy object. END");
                }
                else
                {
                    lvRecordsVector = (java.util.List) currentBaseObj.getDottedFieldValue(aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getAribaObjname());
                    jsonGenerator.writeObjectFieldStart(aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getDWOutputStr()); // starting
                                                                                                                                            // current
                                                                                                                                            // object
                    aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().pushBracesStack(BRACETYPE_WRITEOBJFIELDSTART);
                    // aribaObjStack.peek().getBuysenseAribaObjHierarchy().pushBracesStack(BRACETYPE_WRITEOBJFIELDSTART);
                    Log.customer.debug("*****%s***** writeObjectFieldStart" + aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getDWOutputStr() + " JNNNNNNNNNNN-{", msCN);

                    jsonGenerator.writeArrayFieldStart(aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getDWOutputStr() + "VECTOR"); // don't
                                                                                                                                                      // hardcode
                                                                                                                                                      // vector
                                                                                                                                                      // here
                    // aribaObjStack.peek().getBuysenseAribaObjHierarchy().pushBracesStack(BRACETYPE_WRITEARRAYFIELDSTART);
                    aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().pushBracesStack(BRACETYPE_WRITEARRAYFIELDSTART);

                    Log.customer.debug("*****%s***** writeArrayFieldStart" + aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getDWOutputStr() + "JNNNNNNNNNNN-[", msCN);

                    Log.customer.debug(msCN + ": current obj:" + currentBaseObj);
                    // Log.customer.debug("*****%s***** next level to be processed %s:" +
                    // " obj name:" + aribaHierachyObjList.get(currObj +
                    // 1).getBuysenseAribaObjHierarchy().getAribaObjname(), msCN, tempCurrentLevel);
                    Log.customer.debug("*****%s*****  next level to be processed %s:" + " obj name:" + aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getAribaObjname(), msCN,
                            tempCurrentLevel);
                    aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().printBracesStack();

                    if (lvRecordsVector == null)
                    {
                        Log.customer.debug(msCN + ": lvRecordsVector is NULL. DOING NOTHING");
                    }
                    else
                    {

                        for (liRecordIndex = 0; liRecordIndex < lvRecordsVector.size(); liRecordIndex++)
                        {
                            Log.customer.debug(msCN + ": liRecordIndex:" + liRecordIndex);
                            jsonGenerator.writeStartObject();
                            aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().pushBracesStack(BRACETYPE_WRITESTARTOBJ);
                            // aribaObjStack.peek().getBuysenseAribaObjHierarchy().pushBracesStack(BRACETYPE_WRITESTARTOBJ);
                            Log.customer.debug("*****%s***** writeStartObject " + ":" + aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getDWOutputStr() + ":" + "JNNNNNNNNNNN-{",
                                    msCN);
                            Log.customer.debug(msCN + ": size:" + (new Integer(lvRecordsVector.size())).toString());
                            Log.customer.debug(msCN + ": vec:" + lvRecordsVector);
                            /*
                             * BaseVector xx = null; newBaseObject = (BaseObject) xx.get(0);
                             * ComplexTypeImpl comImpl = new ComplexTypeImpl(); List <AttributeImpl>
                             * attrImpl = comImpl.getAttributes(); newBaseObject = (BaseObject)
                             * attrImpl.get(0);
                             */
                            Log.customer.debug(msCN + ": class:" + lvRecordsVector.get(liRecordIndex).getClass());
                            if (lvRecordsVector.get(liRecordIndex) instanceof BaseId)
                            {
                                BaseId refBaseId = (BaseId) lvRecordsVector.get(liRecordIndex);
                                BaseSession baseSession = Base.getSession();
                                newBaseObject = baseSession.objectForRead(refBaseId);
                            }
                            else
                            // not of type BaseId. So assuming it is of type BaseObjects
                            {
                                newBaseObject = (BaseObject) lvRecordsVector.get(liRecordIndex);
                            }
                            // getFieldsForObjectLevel(newBaseObject,
                            // aribaHierachyObjList.get(currObj));

                            boolean lastIteration1 = false;
                            if (liRecordIndex == (lvRecordsVector.size() - 1))
                            {
                                lastIteration1 = true;
                            }

                            Log.customer.debug(msCN + ": calling populateJsonObj - newBaseObject:" + newBaseObject + ", nextObj:" + nextObj + ",getIsVector()"
                                    + aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getIsVector());
                            Log.customer.debug(msCN + ": calling populateJsonObj - getIsRecursive():" + aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getIsRecursive());
                            Log.customer.debug(msCN + ": lastIteration1:" + lastIteration1);
                            if (liRecordIndex != 0 && !lastIteration1)
                            {
                                bPushToStack = false;
                            }
                            Log.customer.debug(msCN + ": bPushToStack:" + bPushToStack);
                            populateJsonObj(newBaseObject, nextObj, aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getIsVector(), aribaHierachyObjList.get(nextObj)
                                    .getBuysenseAribaObjHierarchy().getIsRecursive(), nextObj, lastIteration1, bPushToStack);
                            Log.customer.debug(msCN + ": vector after populatejson:");
                            // just pop one and leave the rest
                            // if (aribaHierachyObjList.size() >= (nextObj + 1)) // Don't add
                            // closing brace if this is the last hierarchy
                            // {
                            Log.customer.debug(msCN + ": aribaHierachyObjList.get(nextObj): " + aribaHierachyObjList.get(nextObj));
                            Log.customer.debug(msCN + ": aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().isBracesEmpty(): "
                                    + aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().isBracesEmpty());
                            Log.customer.debug(msCN + ": lastIteration1:" + lastIteration1);
                            if (lastIteration1)
                            {
                                Log.customer.debug(msCN + ": addOneClosingBrace()");
                                addOneClosingBrace(aribaHierachyObjList.get(nextObj));
                            }
                            else
                            {
                                Log.customer.debug(msCN + ": writeEndObject:");
                                jsonGenerator.writeEndObject();
                                aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().popBracesStack();

                                Log.customer.debug(msCN + ": printBracesStack() in the loop for index: " + liRecordIndex);
                                aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().printBracesStack();
                            }

                            // }
                            // just pop if antyhing in there?
                            // jsonGenerator.writeEndObject();
                        }
                        // moving it from here to down
                    }
                }
            }
            else
            {
                // next object not a vector
                // don't loop - get object
                Log.customer.debug("*****%s*****  printBracesStack22222s", msCN);
                aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().printBracesStack();

                if (currentBaseObj != null)
                {
                    newBaseObject = (BaseObject) currentBaseObj.getDottedFieldValue(aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getAribaObjname());
                }
                else
                {
                    newBaseObject = null;
                }
                // continue with rest of the functionality
                populateJsonObj(newBaseObject, nextObj, aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getIsVector(), aribaHierachyObjList.get(nextObj)
                        .getBuysenseAribaObjHierarchy().getIsRecursive(), nextObj, false, true);

                if ((currObj == 0) && (currObj != startNextObj))
                {
                    ; // do nothing
                }
                else
                // / should happen only at the begining where both have value '0'
                {
                    // jsonGenerator.writeEndObject(); --jbtemp1
                }

                Log.customer.debug("*****%s*****  DONE processing first level", msCN);
            }

            // next level processing START

            // Log.customer.debug("*****%s***** DONE processing first level: (currObj + 2):" + (new
            // Integer(currObj + 2)).toString(aribaHierachyObjList.size()) + " " +(new
            // Integer(aribaHierachyObjList.size())).toString(), msCN);

            // Example: Supplier object could have
            // Supplier
            // Supplier:CommonSupplier
            // Supplier:CommonSupplier:OrganizationID
            // Supplier:CommonSupplier:OrganizationID:Ids and then
            // Supplier:Locations

            // go to next 2 tokens hierarchy
            // go to next 2 level token value from top

            // String curr3Level = aribaHierachyObjList.get(currObj +
            // 1).getBuysenseAribaObjHierarchy().getObjectHierarchyName();
            Log.customer.debug("*****%s*****  printBracesStack3333", msCN);
            aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().printBracesStack();
            String curr3Level = aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getObjectHierarchyName();
            StringTokenizer st2 = null;
            st2 = new StringTokenizer(curr3Level, ":");

            // proceed only if curr3Level is 2 objects --Supplier:CommonSupplier
            if (st2 != null && (st2.countTokens() > 2))
            {
                Log.customer.debug("*****%s*****  proceed only if curr3Level", msCN);
                addClosingBraces(aribaHierachyObjList.get(nextObj));
                return;
            }

            StringTokenizer stNextLvl2 = null;

            // String[] nxtLevel2Tok = null;
            String nxt3Level = null;
            String nxtLevel2Start = null;

            // before you find next level 2 obj, close braces for current object except the last one
            addClosingBraces(aribaHierachyObjList.get(nextObj));

            // go through all level 2 objects and choose the next one to process
            for (int i = nextObj + 1; i < aribaHierachyObjList.size(); i++)
            {
                nxt3Level = aribaHierachyObjList.get(i).getBuysenseAribaObjHierarchy().getObjectHierarchyName();

                stNextLvl2 = new StringTokenizer(nxt3Level, ":");

                Log.customer.debug("*****%s*****  choose the next level2 object to process from hierarchy list nxt3Level:" + nxt3Level + "; curr3Level:" + curr3Level + "; nxtLevel2Start"
                        + nxtLevel2Start, msCN);
                if (stNextLvl2 != null && (stNextLvl2.countTokens() == 2))
                {
                    Log.customer.debug("*****%s*****  DONE choose the next level2 object to process from hierarchy list", msCN);
                    nextObjVal = i;
                    break; // this is the next object that needs to be processed
                }
                // what if it is 3 token or 4 token next level object?
            }

            // currObj + 1 => Supplier:CommonSupplier
            // currObj + 2 => Supplier:Locations

            if (nextObjVal == 0)// just pop items and close braces
            {

                // do it for nextObj first and then pop up rest
                aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().printBracesStack();
                addClosingBraces(aribaHierachyObjList.get(nextObj));

                // json finish object
                // int braceType;
                while (!aribaObjStack.empty())
                {
                    addClosingBraces(aribaObjStack.pop());
                }
            }
            // if (nxt3Level != null)
            else if (nextObjVal != 0) // found the next lvl2 obj
            {
                // 'Supplier:CommonSupplier'
                // 'Supplier:Locations'

                st2 = new StringTokenizer(nxt3Level, ":");
                String[] next3LevelTok = nxt3Level.split(":");
                String nxt3LevelStart = nxt3Level.substring(0, nxt3Level.length() - (next3LevelTok[next3LevelTok.length - 1].length() + 1));

                Log.customer.debug("*****%s*****  round2 processing for level 2 objs curr3Level:" + curr3Level + " nxt3Level:" + nxt3Level + " currObj:", msCN);
                Log.customer.debug("*****%s*****  round2 processing nxt3LevelStart:" + nxt3LevelStart, msCN);

                if (st2 != null && (st2.countTokens() == 2))
                {
                    Log.customer.debug("*****%s*****  round2 processing for level 2 objs final val curr3Level:" + curr3Level + " nxt3Level:" + nxt3Level, msCN);

                    // need to get to correct object
                    // assuming one level up!
                    // 'Supplier:CommonSupplier'

                    // keep popping from the stack till you get the right object
                    String temp3CurrentLevel = aribaHierachyObjList.get(nextObj).getBuysenseAribaObjHierarchy().getObjectHierarchyName();

                    BuysenseDWHierarchyDetails temp3DWHierarchyDetails = null;
                    BuysenseDWHierarchyDetailsObj temp3DWHierarchyDetailsObj = null;

                    // PO:Supplier
                    // PO:SupplierLocation
                    // 'PO:SupplierLocation' starts with 'PO:Supplier' but that's not correct

                    // check next level without last token
                    // for PO:SupplierLocation, you want PO

                    while (!StringUtil.equalsIgnoreCase(temp3CurrentLevel, nxt3LevelStart))
                    {
                        temp3DWHierarchyDetailsObj = aribaObjStack.pop();
                        temp3DWHierarchyDetails = temp3DWHierarchyDetailsObj.getBuysenseAribaObjHierarchy();
                        temp3BaseObject = temp3DWHierarchyDetails.getAribaBaseObject();
                        temp3CurrentLevel = temp3DWHierarchyDetails.getObjectHierarchyName();
                        Log.customer.debug("*****%s*****  loop2 next level to be processed %s:", msCN, temp3CurrentLevel);
                        // json finish object
                        // /--jbtemp2
                        // close braces if it isn't level 2
                        if (!StringUtil.equalsIgnoreCase(temp3CurrentLevel, nxt3LevelStart))
                        {
                            addClosingBraces(temp3DWHierarchyDetailsObj); // --jbtemp2
                        }
                        // addOneClosingBrace(temp3DWHierarchyDetailsObj);
                    }
                    Log.customer.debug("*****%s*****  process next level from base obj %s:" + " obj name:" + aribaHierachyObjList.get(nextObjVal).getBuysenseAribaObjHierarchy().getAribaObjname(),
                            msCN, temp3BaseObject);
                    populateJsonObj(temp3BaseObject, 0, aribaHierachyObjList.get(nextObjVal).getBuysenseAribaObjHierarchy().getIsVector(), aribaHierachyObjList.get(nextObjVal)
                            .getBuysenseAribaObjHierarchy().getIsRecursive(), nextObjVal, false, true);
                }
            }
            // next level processing END
            // next level matches previous one close the json object or open new one
        }
        catch (Exception ex)
        {
            // print exception and throw the same
            String curLevel = "";
            if (currObj >= 0 && aribaHierachyObjList.size() > currObj)
            {
                curLevel = aribaHierachyObjList.get(currObj).getBuysenseAribaObjHierarchy().getAribaObjname();
            }
            if (temp3BaseObject != null)
            {
                Log.customer.error(7000, msCN + "::Error processing document from base obj:" + temp3BaseObject + " level:" + curLevel);
            }
            throw ex;
        }
    }

    private static void removeNullNodesInJSON(JsonNode jsNode) throws Exception
    {
        List<Integer> arrayItemsToRemove = new ArrayList<Integer>();
        boolean removeArrayItem = false;

        try
        {
            Iterator<Map.Entry<String, JsonNode>> nodeIterator = jsNode.getFields();
            while (nodeIterator.hasNext())
            {
                Map.Entry<String, JsonNode> entry = (Map.Entry<String, JsonNode>) nodeIterator.next();

                if (entry.getValue().isObject())
                {
                    if (entry.getValue().getElements().hasNext())
                    {
                        removeNullNodesInJSON(entry.getValue());
                    }
                }
                else if (entry.getValue().isArray())
                {
                    arrayItemsToRemove.clear();
                    int i = 0;

                    for (JsonNode tempNode : entry.getValue())
                    {
                        if (tempNode.toString().equalsIgnoreCase("{}"))
                        {
                            removeArrayItem = true;
                        }
                        else
                        {
                            removeNullNodesInJSON(tempNode);
                        }
                        if (removeArrayItem)
                        {
                            arrayItemsToRemove.add(new Integer(i));
                        }
                        removeArrayItem = false;
                        i++;
                    }
                    int j;
                    int k=0;
                    for (Iterator<Integer> iter = arrayItemsToRemove.iterator(); iter.hasNext();)
                    {
                        j = (Integer) iter.next().intValue();
                        // need k because array changes as we remove elements. Need to adjust for it -JB
                        ((ArrayNode) entry.getValue()).remove(j-k);
                        k++;
                    }
                }
            }
        }
        finally
        {
            arrayItemsToRemove.clear();
        }

    }

    private boolean checkAllowedInValues(JsonNode rootNode, String objHierarchyName) throws Exception
    {

        try
        {
            // aribaFieldDetailsMap has all the objects/fields for a particular obj hierarchy
            String sNodeName = null;
            String sNodeValue = null;
            String sAllowedInValues = null;
            String sDWOutputStr = null;
            boolean objAllowed = true;

            if (StringUtil.nullOrEmptyOrBlankString(objHierarchyName))
            {
                Log.customer.debug(msCN + "::empty objHierarchyName");
            }
            else
            {
                Log.customer.debug(msCN + "::entering ------------:" + objHierarchyName);
                Log.customer.debug(msCN + "::entering rootNode.toString() ------------:" + rootNode.toString());
                
            }

            Iterator<Map.Entry<String, JsonNode>> nodeIterator = rootNode.getFields();
            while (nodeIterator.hasNext())
            {

                Map.Entry<String, JsonNode> entry = (Map.Entry<String, JsonNode>) nodeIterator.next();

                Log.customer.debug(msCN + "::key --> " + entry.getKey() + " value-->" + entry.getValue());
                sNodeName = entry.getKey();
                sNodeValue = entry.getValue().toString();

                if (entry.getValue().isObject())
                {
                    Log.customer.debug(msCN + "::is an object");
                    String newObjHierarchyName = null;
                    // push the item
                    if (aribaObjStackForAllowedInVals.isEmpty())
                    {
                        newObjHierarchyName = objHierarchyName;
                        aribaObjStackForAllowedInVals.push(objHierarchyName);
                    }
                    else
                    {
                        // circle through all the list and find the one that begins with 'objHierarchyName:'
                        // AND has the JSON tag in it (ATTRIBUTES)
                        newObjHierarchyName = getHierarchyNameForJSONTag(aribaObjStackForAllowedInVals.peek(), entry.getKey());
                        aribaObjStackForAllowedInVals.push(newObjHierarchyName);
                    }
                    // how to put ATTRIBUTES on top?
                    // Log.customer.debug(msCN + "::newObjHierarchyName:" + newObjHierarchyName);

                    objAllowed = checkAllowedInValues(entry.getValue(), newObjHierarchyName);
                    if (!objAllowed)
                    {
                        Log.customer.debug(msCN + "::removing node:" + entry.getValue());
                        ((ObjectNode) entry.getValue()).removeAll();
                        objAllowed = true; // set it to true as obj removed at array level. don't want to remove obj - change1
                    }
                    // pop the item
                    aribaObjStackForAllowedInVals.pop();
                }
                else if (entry.getValue().isArray())
                {
                    Log.customer.debug(msCN + "::is an array");
                    for (JsonNode tempNode : entry.getValue())
                    {
                        // objAllowed = objAllowed && checkAllowedInValues(tempNode, objHierarchyName);
                        // ANDing is throwing off the logic
                        objAllowed = checkAllowedInValues(tempNode, objHierarchyName);
                        if (!objAllowed)
                        {
                            Log.customer.debug(msCN + "::removing node in an array:" + tempNode);
                            ((ObjectNode) tempNode).removeAll();
                            objAllowed = true; // set it to true as obj removed at array level. don't want to remove obj - change1
                        }

                    }
                }
                else
                {
                    // if (entry.getValue().isTextual() || entry.getValue().isBoolean() || entry.getValue().isInt() || entry.getValue().isBigDecimal() || entry.getValue().isDouble() )
                    Log.customer.debug(msCN + "::is a textual node or other");
                    // get corresponding allowedinvalues
                    BuysenseDWFieldDetails[] fieldList = aribaFieldDetailsMap.get(objHierarchyName);

                    if (fieldList == null)
                    {
                        // print and do nothing as you need to process other nodes
                        Log.customer.debug(msCN + "::field list is null");
                    }

                    Log.customer.debug(msCN + "::field list is not null");

                    if (fieldList != null)
                    {
                        for (int i = 0; i < fieldList.length; i++)
                        {
                            // we care only for those values that have allowedinvalues filled in
                            sDWOutputStr = fieldList[i].getDwOutputStr();
                            if (StringUtil.nullOrEmptyOrBlankString(sDWOutputStr) || StringUtil.nullOrEmptyOrBlankString(sNodeName))
                            {
                                continue;
                            }

                            if (sDWOutputStr.equals(sNodeName))
                            {
                                sAllowedInValues = fieldList[i].getAllowedInValues();
                                if (!StringUtil.nullOrEmptyOrBlankString(sAllowedInValues))
                                {
                                    Log.customer.debug(msCN + ":: sNode sAllowedInValues :" + sAllowedInValues);

                                    if (!StringUtil.nullOrEmptyOrBlankString(sNodeValue))
                                    {
                                        Log.customer.debug(msCN + ":: sNode value:" + sNodeValue);
                                        String tokStr = ",";
                                        String[] tokens = sAllowedInValues.split(tokStr);
                                        objAllowed = false;

                                        if (sNodeValue.startsWith("\"")) // it's a string. remove the first char and the trailing character
                                        {
                                            sNodeValue = sNodeValue.substring(1, sNodeValue.length() - 1);
                                        }
                                        // Log.customer.debug(msCN + ":: sNode value: removing quotes:" + sNodeValue);
                                        for (int j = 0; j < tokens.length; j++)
                                        {
                                            Log.customer.debug(msCN + ":: token val:" + tokens[j]);
                                            if (sNodeValue.equals(tokens[j]))
                                            {
                                                // Log.customer.debug(msCN + ":: sNode value allowed:" + tokens[j]);
                                                objAllowed = true;
                                            }
                                        }
                                        // compare and return false if condition not satisfied
                                    }
                                }
                            }
                        }
                    }
                    if (!objAllowed) // allowed in value condition not satisfied. No point going through rest of the objects
                    {
                        return objAllowed;
                    }
                }

            }
            return objAllowed;
        }
        finally
        {
        }
    }

    private String getHierarchyNameForJSONTag(String parentHierarchyName, String currDWOutTag)
    {
        // parentHierarchyName - ComplexTypeImpl
        // currDWOutTag - ATTRIBUTES
        // we need to make sure that ATTRIBUTES exists in hierarchy with name 'ComplexTypeImpl:XYZ'
        // go through the list

        // String sObjHierarchyName;
        String[] tokens = null;
        String sHierarchyName = null;
        BuysenseDWHierarchyDetails hierarchyDetails = null;

        if (StringUtil.nullOrEmptyOrBlankString(parentHierarchyName) || StringUtil.nullOrEmptyOrBlankString(currDWOutTag))
        {
            return ""; // return blank
        }

        for (int i = 0; i < aribaHierachyObjList.size(); i++)
        {
            hierarchyDetails = aribaHierachyObjList.get(i).getBuysenseAribaObjHierarchy();
            // chop last part and then compare - ComplexTypeImpl:XYZ
            tokens = hierarchyDetails.getObjectHierarchyName().split(":");
            Log.customer.debug("*****%s***** hierarchy name before change:" + hierarchyDetails.getObjectHierarchyName(), msCN);
            if (tokens.length <= 1)
            {
                Log.customer.debug("*****%s***** hierarchy at topmost level", msCN);
                sHierarchyName = hierarchyDetails.getObjectHierarchyName();
            }
            else
            {
                sHierarchyName = hierarchyDetails.getObjectHierarchyName().substring(0, (hierarchyDetails.getObjectHierarchyName().length() - ((tokens[tokens.length - 1]).length() + 1))); // remove last token + :
            }

            // Log.customer.debug("*****%s***** hierarchy name:" + sHierarchyName, msCN);
            if (StringUtil.equalsIgnoreCase(parentHierarchyName, sHierarchyName))
            {
                // we got the hierarchy name. see if DW output tag matches
                if (hierarchyDetails.getDWOutputStr().equals(currDWOutTag))
                {
                    Log.customer.debug("*****%s***** parent hierarchy found. sending :" + hierarchyDetails.getObjectHierarchyName(), msCN);
                    return hierarchyDetails.getObjectHierarchyName();
                }
            }
        }
        return "";
    }

    private String getStackMessg(Exception ex)
    {
        StackTraceElement[] arr = ex.getStackTrace();
        String stackMessg = ex.getMessage() + "\n";
        if (ex.getMessage() != null)
        {
            stackMessg = stackMessg + ex.getMessage() + "\n";
        }

        for (int i = 0; i < arr.length; i++)
        {
            stackMessg = stackMessg + (arr[i].toString()) + "\n";
        }
        return stackMessg;
    }

    private void printAribaObjStack(Stack<BuysenseDWHierarchyDetailsObj> ObjStack)
    {
        Log.customer.debug("*****%s***** obj stack =================================", msCN);
        if (ObjStack == null)
        {
            Log.customer.debug("*****%s***** obj stack is null:", msCN);
        }
        Iterator<BuysenseDWHierarchyDetailsObj> it = ObjStack.iterator();
        BuysenseDWHierarchyDetailsObj ObjDetails = null;
        while (it.hasNext())
        {
            ObjDetails = it.next();
            Log.customer.debug("*****%s***** obj:" + ObjDetails.getBuysenseAribaObjHierarchy().getObjectHierarchyName(), msCN);
        }
        Log.customer.debug("*****%s***** end obj stack =================================", msCN);
    }

    private void addClosingBraces(BuysenseDWHierarchyDetailsObj HierarchyDetailsObj) throws Exception
    {
        int braceType;
        Log.customer.debug("*****%s*****  In HierarchyDetailsObj:", msCN);
        while (!HierarchyDetailsObj.getBuysenseAribaObjHierarchy().isBracesEmpty())
        {
            Log.customer.debug("*****%s*****  HierarchyDetailsObj pop :", msCN);
            braceType = HierarchyDetailsObj.getBuysenseAribaObjHierarchy().popBracesStack();
            if (braceType == BRACETYPE_WRITESTARTOBJ)
            {
                jsonGenerator.writeEndObject();
                Log.customer.debug("*****%s***** writeEndObject JNNNNNNNNNNN-}", msCN);
            }
            else if (braceType == BRACETYPE_WRITEOBJFIELDSTART)
            {
                jsonGenerator.writeEndObject();
                Log.customer.debug("*****%s***** writeEndObject JNNNNNNNNNNN-}", msCN);
            }
            else if (braceType == BRACETYPE_WRITEARRAYFIELDSTART)
            {
                jsonGenerator.writeEndArray();
                Log.customer.debug("*****%s***** writeEndArray JNNNNNNNNNNN-]", msCN);
            }
        }

    }

    private void addOneClosingBrace(BuysenseDWHierarchyDetailsObj HierarchyDetailsObj) throws Exception
    {
        int braceType;
        Log.customer.debug("*****%s*****  In addOneClosingBrace:", msCN);
        if (!HierarchyDetailsObj.getBuysenseAribaObjHierarchy().isBracesEmpty())
        {
            Log.customer.debug("*****%s*****  HierarchyDetailsObj pop one :", msCN);
            braceType = HierarchyDetailsObj.getBuysenseAribaObjHierarchy().popBracesStack();
            if (braceType == BRACETYPE_WRITESTARTOBJ)
            {
                jsonGenerator.writeEndObject();
                Log.customer.debug("*****%s***** writeEndObject JNNNNNNNNNNN-}", msCN);
            }
            else if (braceType == BRACETYPE_WRITEOBJFIELDSTART)
            {
                jsonGenerator.writeEndObject();
                Log.customer.debug("*****%s***** writeEndObject JNNNNNNNNNNN-}", msCN);
            }
            else if (braceType == BRACETYPE_WRITEARRAYFIELDSTART)
            {
                jsonGenerator.writeEndArray();
                Log.customer.debug("*****%s***** writeEndArray JNNNNNNNNNNN-]", msCN);
            }
        }

    }

}
