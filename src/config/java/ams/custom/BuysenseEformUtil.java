package config.java.ams.custom;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import ariba.approvable.core.Approvable;
import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.common.core.User;
import ariba.htmlui.fieldsui.Log;
import ariba.procure.core.SimpleProcureRecord;
import ariba.purchasing.core.Requisition;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;

public class BuysenseEformUtil {
	
	protected String sResourceFile = "buysense.DynamicEform";

	static String findFieldValue(String sFieldName, BaseObject baseObj,
			String appType) {
		String fieldValue = new String();
		String field = sFieldName.substring(5);
		Log.customer.debug("In findFieldValue: field: %s ", field);

		if (appType.equals("BuysDynamicEform")) {
			fieldValue = (String) baseObj.getDottedFieldValue(field);
			return fieldValue;
		}
		return fieldValue;
	}
	
	static public ClusterRoot findField(Partition foPart,
			String sClientID,String sProfName, String sFieldName) {

		String targetFieldName = sFieldName;
		String UniqueName = (sClientID+":"+sProfName+":"+targetFieldName);

		Log.customer.debug("*** UniqueName: " + UniqueName);

		ClusterRoot fieldTable = Base.getService().objectMatchingUniqueName(
				"ariba.core.BuysEformFieldTable", foPart, UniqueName);
		Log.visibility.debug("Found this fieldTable object: %s", fieldTable);
		return fieldTable;
	}

	static public String findFieldLabel(Partition foPart,
			String sClientID,String sProfName,String sFieldName) {
		ClusterRoot fieldTable = findField(foPart, sClientID, sProfName, sFieldName);
		String lsEmptyLabel = "";
		Log.customer.debug("findFieldLabel: Found this fieldTable object: %s",
				fieldTable);
		if (fieldTable != null) {
			Boolean lbVisible = ((Boolean)fieldTable.getFieldValue("VisibleOnEform")).booleanValue();
			if(lbVisible != null && lbVisible.toString().trim().equals("true"))
			{
				String sLabel = (String) fieldTable.getFieldValue("FieldLabel");
				Log.customer.debug("findFieldLabel: Label for fieldname "
						+ sFieldName + " is " + sLabel);
				return sLabel;				
			}
			else{
				return lsEmptyLabel;
			}
		}
		else{
			return lsEmptyLabel;	
		}
		
	}

	static public ClusterRoot findField(String sClientID,String sProfName,
			String sFieldName) {

		String targetFieldName = sFieldName;
		String UniqueName = (sClientID+":"+sProfName+":"+targetFieldName);
		Log.visibility.debug("The uniquename to be searched is :"+UniqueName);
		ClusterRoot fieldTable = Base.getService().objectMatchingUniqueName(
				"ariba.core.BuysEformFieldTable",
				Base.getSession().getPartition(), UniqueName);
		Log.visibility.debug("Found this fieldTable object: %s", fieldTable);
		return fieldTable;
	}

	static public String findFieldLabel(String sProfUniqueName,
			String sFieldName) {
		String sClientID = StringUtil.substring(sProfUniqueName,0, sProfUniqueName.indexOf(":"));
		String lsEmptyLabel = "";
		Log.customer.debug("The ClientID is :"+sClientID);
		String sProfName = StringUtil.substring(sProfUniqueName, sProfUniqueName.indexOf(":")+1, sProfUniqueName.length());
		Log.customer.debug("The profName is :"+sProfName);
		ClusterRoot fieldTable = findField(sClientID,sProfName, sFieldName);
		Log.customer.debug("findFieldLabel: Found this fieldTable object: %s",
				fieldTable);
		if (fieldTable != null) {
			Boolean lbVisible = ((Boolean)fieldTable.getFieldValue("VisibleOnEform")).booleanValue();
			if(lbVisible != null && lbVisible.toString().trim().equals("true"))
			{
				String sLabel = (String) fieldTable.getFieldValue("FieldLabel");
				Log.customer.debug("findFieldLabel: Label for fieldname "
						+ sFieldName + " is " + sLabel);
				return sLabel;				
			}
			else{
				return lsEmptyLabel;
			}
		}
		else{
			return lsEmptyLabel;	
		}
	}


	@SuppressWarnings("unchecked")
	public static void createHistory(BaseObject foApprovableObject,
			String fsUniqueName, String fsRecordType, String fsMessage,
			User foUser) {
		BaseObject po_hist = null;
		if(foApprovableObject instanceof Requisition)
		{
			po_hist = (ariba.procure.core.SimpleProcureRecord) BaseObject
				.create("ariba.procure.core.SimpleProcureRecord",
						foApprovableObject.getPartition());
		}
		else
		{
			po_hist = (ariba.procure.core.SimpleProcureRecord) BaseObject
					.create("ariba.procure.core.SimpleProcureRecord",
							foApprovableObject.getPartition());		
		}
		po_hist.setFieldValue("Approvable", foApprovableObject);
		po_hist.setFieldValue("ApprovableUniqueName", fsUniqueName);

		ariba.util.core.Date loDate = new ariba.util.core.Date();
		po_hist.setFieldValue("Date", loDate);

		po_hist.setFieldValue("User", foUser.getUser());
		po_hist.setFieldValue("RecordType", fsRecordType);
		if(fsRecordType.equalsIgnoreCase("CopyEformRecord"))
		{
			String copyRecordString = Fmt.Sil(Base.getSession().getLocale(),
					"buysense.DynamicEform", "EformCopiedText",foApprovableObject.getFieldValue("UniqueName"));
			List inDetail = ListUtil.list();
			inDetail.add(copyRecordString);
			Log.customer.debug("Setting the type value for Eform and requisition");
			po_hist.setFieldValue("Type", "Copied");
			po_hist.setFieldValue("Details", inDetail);
			Log.customer.debug("The set type value for eform is :"+po_hist.getFieldValue("Type"));
		}
		else if(fsRecordType.equalsIgnoreCase("ReqCreated"))
		{
			po_hist.setFieldValue("Type", "ReqCreated");
		}

		if (fsMessage != null && ((fsRecordType.equalsIgnoreCase("ReqCreated"))||(fsRecordType.equalsIgnoreCase("CreateReqRecord")))) {
			((List<String>) po_hist.getFieldValue("Details")).add(fsMessage);
		}

		// Now add it to the approvable
		((List<BaseObject>) foApprovableObject.getFieldValue("Records")).add(po_hist);
	}

	public static void showCallerInfo(Throwable t, String classname, Object obj) {
		StackTraceElement[] elements = t.getStackTrace();

		StringBuffer x = new StringBuffer(getTimeStamp());
		x.append(": Method ").append(elements[0].getMethodName());
		x.append(" in class ").append(classname);
		x.append(" was invoked by method ").append(elements[1].getMethodName());
		x.append(" in class ").append(elements[1].getClassName());
		x.append(", file name ").append(elements[1].getFileName());
		x.append(", line nbr ").append(elements[1].getLineNumber());

		Log.customer.debug(x.toString());
	}

	public static String getTimeStamp() {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(
					"yyyy.MM.dd hh:mm:ss.S");
			Date currentTime_1 = new Date(System.currentTimeMillis());
			return (formatter.format(currentTime_1));
		} catch (Exception e) {
			return "";
		}
	}	

}
