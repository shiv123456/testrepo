
/************************************************************************************
 * Author:  Richard Lee
 * Date:    September, 2007
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 09/19/2007        Richard Lee            CSPL-142: Change quantities received (negative receiving)
 *
 * @(#)AddCommentForDataRetention.java     1.0 09/11/2007
 *  ValidateNegativeReceiptItem.java    1.0 9/19/2007
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 */

package config.java.ams.custom;
import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.core.BaseObject;
import java.math.BigDecimal;
import java.util.List;
import ariba.util.core.ListUtil;
import ariba.util.core.Constants;

//81->822 changed ConditionValueInfo to ValueInfo
public class ValidateReceiptDate extends Condition
{
    private static final ValueInfo parameterInfo[];
    private static final List    NO_ERROR_RESULT          = ListUtil.list(Constants.getInteger(0));
    String message = null;

    public boolean evaluate(Object value, PropertyTable params)
    {
        Logs.customer.debug("Calling ValidateReceiptDate");
        BigDecimal loNumPrevAcc = new BigDecimal(0);
        BigDecimal loNumAcc = new BigDecimal(0);
        BigDecimal loNumRej = new BigDecimal(0);
        BaseObject obj   = (BaseObject)params.getPropertyForKey("ReceiptItem");
        if (obj.instanceOf("ariba.receiving.core.ReceiptItem"))
        {
            loNumAcc = (BigDecimal) obj.getFieldValue("NumberAccepted");
            Logs.customer.debug("loNumAcc = "+loNumAcc);
            loNumRej = (BigDecimal) obj.getFieldValue("NumberRejected");
            Logs.customer.debug("loNumAcc = "+loNumRej);
            loNumPrevAcc = (BigDecimal) obj.getFieldValue("NumberPreviouslyAccepted");
            Logs.customer.debug("loNumPrevAcc = "+loNumPrevAcc);
            
            if ((loNumAcc.doubleValue()) == 0 && loNumRej.doubleValue() == 0 && loNumPrevAcc.doubleValue() == 0)
            {
            	return ListUtil.list(Constants.getInteger(1), "Receive date was updated for lines x, y, z but no items have " +
            			"been received. Please review and make necessary adjustments");
                   
            }
        }
    }

    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    static
    {
        parameterInfo = (new ValueInfo[]{ new ValueInfo("ReceiptItem", 0)});
    }
}
