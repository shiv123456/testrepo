/*
    Copyright (c) 1996-2001 AMS Inc.
    All rights reserved. Patents pending.

   Anup - March 2001
*/

// 08/07/2003: Updates for Ariba 8.0 (David Chamberlain)
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.receiving.core.Receipt;
import ariba.approvable.core.Approvable;
import java.util.List;
import ariba.base.core.*;
// Ariba 8.0: Replaced deprecated ApprovableHook
import ariba.approvable.core.ApprovableHook;
//import ariba.approvable.core.ApprovableHook;
import ariba.util.log.Log;
// Ariba 8.0: Deprecations, methods have changed packages.
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;


//81->822 changed Vector to List
public class AMSReceiptApproveHook implements ApprovableHook
{
    // Ariba 8.0: Removed Util.integer() and replaced it with Util.getInteger()
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0)); 
    
    public List run (Approvable approvable)
    {
        Log.customer.debug("Calling AMSReceiptApproveHook");
        if (approvable.instanceOf("ariba.receiving.core.Receipt"))
        {
            Boolean orderStatus = new Boolean(false);
            String receiptNum = (String)approvable.getFieldValue("UniqueName"); 
            List receipts = (List)approvable.getDottedFieldValue("Order.Receipts");
            for (int i=0; i<receipts.size(); i++)
            {
                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                BaseId id = (BaseId)receipts.get(i);
                Receipt receipt = (Receipt)id.get();
                if (!(receiptNum.equals((String)receipt.getFieldValue("UniqueName")))) 
                {
                    orderStatus = (Boolean)receipt.getFieldValue("CloseOrder");
                    String receiptStatus = (String)receipt.getFieldValue("StatusString");
                    String receiptName = (String)receipt.getFieldValue("UniqueName"); 
                    if (orderStatus.booleanValue())
                    {
                        if (receiptStatus.equals("Approved"))
                            return ListUtil.list(Constants.getInteger(-1),
                                                     "No more receipts may be submitted as the order has been closed.");       
                        else
                            return ListUtil.list(Constants.getInteger(-1),
                                                     "This Receipt can not be Submitted/Approved"+
                                                     " because Receipt # " + receiptName + " which is in 'Composing' state " +
                                                     "proposes closing the order.");       
                    }
                }
            }
            
            //Anup - Generic invocation of dynamically determined Client/Agency edits. - Dev SPL #173 
            List result = EditClassGenerator.invokeHookEdits(approvable,
                                                               this.getClass().getName());
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            int errorCode = ((Integer)result.get(0)).intValue();
            Log.customer.debug("Validations in " + this.getClass().getName() + ", error Code --> %s",errorCode); 
            if(errorCode<0)
            {
                Log.customer.debug("Validations Failed in " + this.getClass().getName() + " - returning error to UI"); 
                return result;
            }
            
        }
        
        Log.customer.debug("Done with AMSReceiptApproveHook");
        return NoErrorResult;
    }
}
