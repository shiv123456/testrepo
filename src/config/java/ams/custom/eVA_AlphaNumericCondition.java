package config.java.ams.custom;

import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ConditionResult;
import ariba.util.core.PropertyTable;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;

public class eVA_AlphaNumericCondition extends Condition
{
	private static String sErrorMessage = "";
	private static final String ERROR_FILE = "aml.Collaboration"; // Specify the Resource file
	private static final String ERROR_MESSAGE1 = "ErrorContractorID"; // Specify the resource key for error msg
	
	public boolean evaluate(Object obj, PropertyTable propertytable)throws ConditionEvaluationException 
	{
		return evaluateImplementation(obj, propertytable);
	}
	private boolean evaluateImplementation(Object obj, PropertyTable propertytable)
	{
		String toCheck = (String) obj;
		boolean bIsAlphaNumeric;
		bIsAlphaNumeric = isAlphaNumeric(toCheck);
		if(bIsAlphaNumeric)
		return true;
		else
		{
			sErrorMessage = ResourceService.getString(ERROR_FILE, ERROR_MESSAGE1);
			return false;
		}
	}
	private boolean isAlphaNumeric(String s)
	{
		if(!StringUtil.nullOrEmptyOrBlankString(s))
		{
		  final char[] chars = s.toCharArray();
		  for (int i = 0; i < chars.length; i++)
		  {      
		    final char c = chars[i];
		    if ((c >= 'a') && (c <= 'z')) continue; // lowercase
		    if ((c >= 'A') && (c <= 'Z')) continue; // uppercase
		    if ((c >= '0') && (c <= '9')) continue; // numeric
		    return false;
		  }  
		  return true;
		}else
		  return false;
	}

	public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
	{

        if (!evaluateImplementation(value, params))
        {
			if (sErrorMessage != null)
			{
			    return new ConditionResult (sErrorMessage);
			}
		}
		return null;
	}

}
