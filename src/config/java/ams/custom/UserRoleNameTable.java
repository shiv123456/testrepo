/*
    Responsible: Anup - Rewrote the class.
    Feb 2001
 
    labraham, 08/26/2004 - Ariba 8.1 Dev SPL # 93 - In the 8.1 upgrade, Ariba introduced a new class Approver that has 3 subclasses
                           (1) ariba.user.core.User, (2) ariba.user.core.Role, (3) ariba.user.core.Group (new 8.1 class).
                           All customer extrinsic approver fields were modified to be of type Approver.   For eVA, we have 
                           implemented Groups to have a 1 to 1 relationship with Roles, meaning, for every Role in the system, 
                           there will be a corresponding Group.  So Ariba is returning User, Groups and Roles for any Approver
                           fields which is confusing to the User since the Roles and Groups have the same name, the User sees it as 
                           duplicate data and can not distinguish which Name is a Group or Role.  To alieviate this sitution, we  
                           have rewritten this class to only get valid Approvers of type 
                           ariba.user.core.User and ariba.user.core.Group.  

                           Currently, this class is used as the NameTable for:
                           (1) BuysenseOrg Approver fields, (2) UserProfile Expenditure_Limit_Exceeded_Approver field, and
                           (3) Requisition Ad-hoc Approver.
*/

package config.java.ams.custom;

import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLQuery;
// Ariba 8.1:  Added the AQLClassUnion class.
import ariba.base.core.aql.AQLClassUnion;
/* Ariba 8.0: Replaced ariba.common.core.nametable.NamedObjectNameTable) */
import ariba.base.core.aql.AQLNameTable;
import ariba.base.fields.ValueSource;
import ariba.util.log.Log;

//81->822 changed Vector to List
public class UserRoleNameTable extends AQLNameTable
{

    public static final String ClassName="config.java.ams.custom.UserRoleNameTable";
    public static final String FieldName="Approver";

    
    /* Ariba 8.1 - As part of rewriting the class to only return Approvers that are of type ariba.user.core.User or
                   ariba.user.core.Group, replaced the matchPattern() and addQueryConstraints() methods with buildQuery().  */
    public AQLQuery buildQuery (String field, String pattern)
    {
        Log.customer.debug( "Inside UserRoleNameTable") ;
        AQLQuery userQuery = new AQLQuery("ariba.user.core.User");
        userQuery = super.buildQuery(userQuery, field, pattern, null);
        AQLQuery groupQuery = new AQLQuery("ariba.user.core.Group");
        groupQuery = super.buildQuery(groupQuery, field, pattern, null);
        String lsAgency = null;
        AQLQuery approverQuery = null;
        
        String lsDeactivated = "(Deactivated)%";
    	ValueSource context = getValueSourceContext();
        if (context != null && context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.BuysenseUserProfileRequest"))
        {
        	lsAgency = (String)context.getFieldValue("Agency");
        	if(lsAgency == null)
        	{
                userQuery.andNotLike("Name.PrimaryString", lsDeactivated);
                ariba.base.core.aql.AQLClassExpression groupAndUserQueries = new AQLClassUnion(AQLQuery.buildSubquery(groupQuery), 1,AQLQuery.buildSubquery(userQuery));
                approverQuery = new AQLQuery(groupAndUserQueries);
                Log.customer.debug( "GroupAnduserQuery(1): " + approverQuery) ;
                return approverQuery;
        	}
        	else
        	{
        		String lsAgencyName = lsAgency.substring(0, 4);
        		
        		String lsquery = "Select usr, usr.Name from ariba.common.core.User cusr JOIN ariba.user.core.User usr USING cusr.User where cusr.ClientName.ClientName like '" +lsAgencyName + "%' and cusr.UniqueName = usr.UniqueName and usr.Creator IS NULL";
        		userQuery = AQLQuery.parseQuery(lsquery);
        		groupQuery.andLike("UniqueName", lsAgencyName + "%");
        		if(pattern == "*")
        		{    
                    userQuery.andNotLike("Name.PrimaryString", lsDeactivated);
        	        ariba.base.core.aql.AQLClassExpression groupAndUserQueries = new AQLClassUnion(AQLQuery.buildSubquery(groupQuery), 1, AQLQuery.buildSubquery(userQuery));
        	    
                    approverQuery = new AQLQuery(groupAndUserQueries);
                    
                    Log.customer.debug( "GroupAnduserQuery(2): " + approverQuery) ;
        		}
        		else
        		{
        			userQuery.andLike("User.Name.PrimaryString",pattern);
                    userQuery.andNotLike("Name.PrimaryString", lsDeactivated);
            	    ariba.base.core.aql.AQLClassExpression groupAndUserQueries = new AQLClassUnion(AQLQuery.buildSubquery(groupQuery), 1, AQLQuery.buildSubquery(userQuery));
                
                    approverQuery = new AQLQuery(groupAndUserQueries);
                    Log.customer.debug( "GroupAnduserQuery(2): " + approverQuery) ;       			
        		}                
                return approverQuery;    		
        	}
        }
        else
        {
        	Log.customer.debug( "Inside UserRoleNameTable: Context is not 'ariba.core.BuysenseUserProfileRequest'");
    		String lsquery = "Select usr, usr.Name,cusr.ClientName.ClientName,usr.EmailAddress from ariba.common.core.User cusr JOIN ariba.user.core.User usr USING cusr.User where cusr.UniqueName = usr.UniqueName and usr.Creator IS NULL";
    		userQuery = AQLQuery.parseQuery(lsquery);
    		String lsQuery1 = "Select grp, grp.Name,grp.SearchUniqueName,grp.SearchEmailAddress from ariba.user.core.Group grp where grp.Creator IS NULL";
    		groupQuery = AQLQuery.parseQuery(lsQuery1);        	
    		if(pattern == "*")
    		{    
            userQuery.andNotLike("Name.PrimaryString", lsDeactivated);
            ariba.base.core.aql.AQLClassExpression groupAndUserQueries = new AQLClassUnion(AQLQuery.buildSubquery(groupQuery), 1, AQLQuery.buildSubquery(userQuery));
            approverQuery = new AQLQuery(groupAndUserQueries);
            Log.customer.debug( "GroupAnduserQuery(3): " + approverQuery) ;
    		}
    		else
    		{
    			Log.customer.debug( "The field being searched for is:" +field) ;
    			if(field!=null && field.equalsIgnoreCase("Name"))
    			{
    				userQuery.andLike("User.Name.PrimaryString", pattern);
    				groupQuery.andLike("Name.PrimaryString", pattern);
    			}
    			else if((field!=null && field.equalsIgnoreCase("SearchUniqueName"))||(field!=null && field.equalsIgnoreCase("ClientId")))
    			{
    				userQuery.andLike("ClientName.ClientName", pattern);
    				groupQuery.andLike("SearchUniqueName", pattern);
    			}
    			else if(field!=null && field.equalsIgnoreCase("SearchEmailAddress"))
    			{
    				userQuery.andLike("User.EmailAddress", pattern);
    				groupQuery.andLike("SearchEmailAddress", pattern);
    			}
    			userQuery.andNotLike("Name.PrimaryString", lsDeactivated);   			
    			ariba.base.core.aql.AQLClassExpression groupAndUserQueries = new AQLClassUnion(AQLQuery.buildSubquery(groupQuery), 1,
                        AQLQuery.buildSubquery(userQuery));
        		Log.customer.debug( "User query when pattern is" +pattern+ " is: " + userQuery) ;
        		Log.customer.debug( "Group query when pattern is" +pattern+ " is: " + groupQuery) ;
        		Log.customer.debug( "Group query when pattern is" +pattern+ " is: " + AQLQuery.buildSubquery(groupQuery)) ;
                approverQuery = new AQLQuery(groupAndUserQueries);
                Log.customer.debug( "GroupAnduserQuery(3): " + approverQuery) ;       			
    		}                
            return approverQuery;
        }
    }

    /* Ariba 8.1 - As part of rewriting the class to only return Approvers that are of type ariba.user.core.User or
                   ariba.user.core.Group, commented out the matchPattern() and addQueryConstraints() methods.
    public List matchPattern (String field, String pattern)
    {
       setClassIsLeaf(false);
       List results = super.matchPattern(field, pattern);
       return results;
       
    }

    public void addQueryConstraints (AQLQuery query, String field, String pattern)
    {
        setClassIsLeaf(false);
        super.addQueryConstraints(query, field, pattern);
        beginSystemQueryConstraints(query);
        
        String dummyString = "%%";
        
        //Ariba 8.1: Approver fields changed from Type "Role" to Type "Approver" which does not have a UniqueName field, so 
        //Changed AQL to get the Name field from the Approver object instead of the UniqueName.
        AQLCondition defaultCond = AQLCondition.buildLike(query.buildField("Name"),dummyString);

        query.and(defaultCond);
       
        endSystemQueryConstraints(query);
    }
    */

    /* Ariba 8.0: Since NamedObjectNameTable was deprecated by AQLNameTable
       the constraints method is no longer available. If constraints are needed
       see the migration pdf for 7.1a
    public List constraints (String field, String pattern)
    {
        List constraints = super.constraints(field, pattern);
        return constraints;
    }
    */
}
