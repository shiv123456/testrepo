package config.java.ams.custom;

import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.util.core.MapUtil;
import ariba.util.log.Log;
import ariba.util.scheduler.ScheduledTask;
import ariba.util.scheduler.Scheduler;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

public class SetPortalSystemID extends ScheduledTask
{
	private final String cn = this.getClass().getName();
	
	private final static String sSystemIDSelectQuery = "SELECT SystemID FROM ariba.common.core.CommonSupplier "
	                                                    + "INCLUDE INACTIVE WHERE LOWER(OrganizationID.Ids.Domain) = 'internalsupplierid' and "
	                                                    + "OrganizationID.Ids.Value = :JoinToken";
	
	private String sPortalSelectQuery = "SELECT SYSTEMID SYSTEMID, (CASE WHEN LENGTH(SUBSTR(JOINTOKEN, 1, INSTR(JOINTOKEN, ':') - 1)) >= 9 "
		                                + "THEN SUBSTR(JOINTOKEN, 1, INSTR(JOINTOKEN, ':') - 1) ELSE JOINTOKEN END) JOINTOKEN "
		                                + "FROM COMMONSUPPLIERIDMAPHEADER";
	

	private AQLOptions aqlOptions = new AQLOptions(Partition.None);
	private Map moParams = MapUtil.map();
	AQLResultCollection results = null;
	java.sql.Statement moPortalStatement = null;	
	private Connection moJDBCConn = null;
	
	private String msDBDriver = null;
	private String msDBPassword = null;
	private String msDBUser = null;
	private String msDBType = null;
	private String msDBURL = null;

    public void init (Scheduler scheduler, String scheduledTaskName, Map parameters)
    {
    	super.init(scheduler, scheduledTaskName, parameters);
        String lsKey = null;

        for(java.util.Iterator e = parameters.keySet().iterator(); e.hasNext();)
        {
        	lsKey = (String)e.next();
        	if (lsKey.equals("DBType")) msDBType = (String)parameters.get(lsKey);
        	else if (lsKey.equals("DBDriver")) msDBDriver = (String)parameters.get(lsKey);
        	else if (lsKey.equals("DBPassword")) msDBPassword = (String)parameters.get(lsKey);
        	else if (lsKey.equals("DBURL")) msDBURL = (String)parameters.get(lsKey);
        	else if ( lsKey.equals("DBUser")) msDBUser = (String)parameters.get( lsKey );
        }
    }

    public void run ()
    {
        if (!OpenTXNConn())
        {
            Log.customer.debug("*****%s*****run***Could not open jdbc connection.  Terminating...", cn);
            return;
        }
        try 
        {
        	Log.customer.debug("*****%s*****run***Selecting from BSADMIN", cn);
        	ResultSet loPortalResults = getPortalResults();

            while (loPortalResults != null && loPortalResults.next()) 
            {
            	String sJoinToken = loPortalResults.getString("JOINTOKEN");
            	Log.customer.debug("*****%s*****run***JoinToken from Portal %s", cn, sJoinToken);
            	if(sJoinToken != null)
            	{
            	   updatePortalSystemID(loPortalResults, sJoinToken);
            	}
            }
        }
        catch (Exception loEx2)
        {
        	Log.customer.debug("*****%s*****run***** Exception: %s", cn, loEx2.getMessage());
        }
        finally
        {
          try
          {
          	moJDBCConn.commit();
        	moPortalStatement.close();
            moJDBCConn.close();
          }
          catch (SQLException loEx1)
          {
              Log.customer.debug("*****%s*****run***finally***** SQLException: %s", cn, loEx1.getMessage());
          }
          catch (Exception loEx2)
          {
              Log.customer.debug("*****%s*****run***finally***** Exception: %s", cn, loEx2.getMessage());
          }
        }
    }
    
    public ResultSet getPortalResults()
    {
 	   ResultSet loPortalResults = null;
       try
        {
    	   moPortalStatement = moJDBCConn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
    	   loPortalResults = moPortalStatement.executeQuery(sPortalSelectQuery);
        }
       catch (SQLException loEx1)
       {
           Log.customer.debug("*****%s*****getPortalResults****** SQLException: %s", cn, loEx1.getMessage());
       }
       catch (Exception loEx2)
       {
           Log.customer.debug("*****%s*****getPortalResults****** Exception: %s", cn, loEx2.getMessage());
       }
       return loPortalResults;
    }
    
    public void updatePortalSystemID(ResultSet loPortalResults, String joinToken)
    {
        try
        {
           String lsSystemID = getSystemID(joinToken);
           if(lsSystemID != null)
           {
        	   loPortalResults.updateString("SYSTEMID",lsSystemID);
        	   loPortalResults.updateRow();
        	   Log.customer.debug("*****%s*****Successfully updated portal row of SystemID %s ", cn, lsSystemID);
           }
        }
        catch (SQLException loEx1)
        {
            Log.customer.debug("*****%s*****Error ***** SQLException: updatePortalSystemID %s", cn, loEx1.getMessage());
        }
        catch (Exception loEx2)
        {
            Log.customer.debug("*****%s*****Error ***** Exception: updatePortalSystemID %s", cn, loEx2.getMessage());
        }
    }

    public String getSystemID(String joinToken)
    {
    	String lsSystemID = null;
    	moParams.clear();
    	moParams.put("JoinToken", joinToken);
    	aqlOptions.setActualParameters(moParams);
    	results = Base.getService().executeQuery(sSystemIDSelectQuery, aqlOptions);
        while (results.next()) 
        {
        	lsSystemID = results.getString(0);
        	Log.customer.debug("*****%s*****getSystemID****** lsSystemID %s ", cn, lsSystemID);
        }
        return lsSystemID;
    }
    
    private boolean OpenTXNConn()
    {
       try
       {
          Class.forName(msDBDriver);
          moJDBCConn = (Connection) DriverManager.getConnection(msDBURL, msDBUser, msDBPassword);
          moJDBCConn.setAutoCommit(false);
       }
       catch (SQLException loEx1)
       {
           Log.customer.debug("*****%s*****Error OpenTXNConn***** SQLException: %s", cn, loEx1.getMessage());
           return false;
       }
       catch (Exception loEx2)
       {
           Log.customer.debug("*****%s*****Error OpenTXNConn***** Exception: %s", cn, loEx2.getMessage());
           return false;
       }
       return true;
    }
}