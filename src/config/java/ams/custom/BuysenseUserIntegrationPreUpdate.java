package config.java.ams.custom;

import java.util.List;
import java.util.Map;

import ariba.base.core.ClusterRoot;
import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import ariba.common.core.User;
import ariba.util.core.ListUtil;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseUserIntegrationPreUpdate extends Action
{

    public void fire(ValueSource object, PropertyTable params)
    {
        try
        {
            Map<?, ?> loUserInfo = (Map<?, ?>) params.getPropertyForKey("UserInfo");
            String lsTopicName = null;
            if (loUserInfo != null)
            {
                lsTopicName = (String) loUserInfo.get("TopicName");
            }

            if (object instanceof ariba.common.core.User && lsTopicName != null && lsTopicName.trim().equals("BuysenseEformProfileMap_CSV"))
            {
                List<?> loEfromProfiles = ListUtil.list();
                ClusterRoot loObject = (ClusterRoot) object;
                User loPartUser = (User) loObject;
                loEfromProfiles = (List<?>) loPartUser.getFieldValue("BuysEformProfiles");
                loEfromProfiles.clear();
            }

        }
        catch (Exception e)
        {
            Log.customer.warning(8000, "Exception in BuysenseUserIntegrationPreUpdate action: " + e.getMessage());
        }

    }

}
