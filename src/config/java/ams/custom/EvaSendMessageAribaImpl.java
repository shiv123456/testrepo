package config.java.ams.custom;

import com.cgi.evamq.common.EvaQueueMessage;
import com.cgi.evamq.producer.EvaSendMessageImpl;

import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.util.core.StringUtil;
import ariba.util.log.*;

public abstract class EvaSendMessageAribaImpl extends EvaSendMessageImpl
{
    private static String msCN = "EvaSendMessageAribaImpl";
    private static String brokerUrl = null;
    private EvaQueueMessage mqMessg = null;

    protected void printDebugMsg(String msg)
    {
        // ariba specific
        Log.customer.warning(7100,msCN + " msg:" + msg);
        Log.customer.debug(msCN + ": " + msg);
    }

    // This can be overridden by Portal or other applications
    protected String getEndpoint() throws Exception
    {
        if (!StringUtil.nullOrEmptyOrBlankString(brokerUrl))
        {
            return brokerUrl;
        }

        // for ariba get the Endpoint from P.table
        Log.customer.debug(msCN + ": fetching Endpoint from P.table");
        Partition moPart = null;
        moPart = Base.getService().getPartition("pcsv");
        if (moPart == null)
        {
            moPart = Base.getSession().getPartition();
        }

        brokerUrl = (String) Base.getService().getParameter(moPart, "Application.AMSParameters.EvaMessageQueueEndpoint");
        return brokerUrl;
    }

    public void setMQMessg(EvaQueueMessage messg)
    {
        mqMessg = messg;
    }

    public EvaQueueMessage getMQMessg()
    {
        return mqMessg;
    }

    protected abstract String getQueueName() throws Exception;
}
