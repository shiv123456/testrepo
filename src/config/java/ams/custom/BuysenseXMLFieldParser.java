/*
 * @(#)BuysenseXMLFieldParser.java     1.0 05/14/2001
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 * 
 * PVCS Modification Log:
 * 
 * $Log:   R:\Software\development\archives\Ariba 8.x Customizations\app\buyer\Server\classes\extensions\config\java\ams\custom\BuysenseXMLFieldParser.java-arc  $
 * 
 *    Rev 1.1   01 Mar 2004 10:35:18   jnamada
 * Ariba 8.x Dev SPL #31 - Globally change Log.util.debug to Log.customer.debug
 * 
 *    Rev 1.0   21 Jan 2004 15:12:30   cm
 * Initial revision.
 * 
 *    Rev 1.1   Aug 01 2001 15:48:30   ghodum
 * Added method to reset a field value
 * 
 *    Rev 1.0   Jul 18 2001 12:11:52   smokkapa
 * Initial revision.
 * 
 */
/* 09/09/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom ;

import java.text.* ;
import ariba.base.core.*;
import ariba.util.log.Log;
// Ariba 8.0: This class no longer exists and isn't needed.
//import ariba.server.objectserver.core.AdapterError;
import ariba.base.core.BaseObject;
// Ariba 8.0: This class no longer exist in Ariba 8.0. It wasn't being used in this module anyhow.
//import ariba.server.ormsserver.ApprovableOnServer;
import ariba.util.formatter.Formatter;

/**
 * The BuysenseXMLFieldParser class provides helper methods to parse
 * given XML field string values into the corresponding object
 *
 * @author Greg Hodum
 *
 */
public abstract class BuysenseXMLFieldParser
{
    /**
     * Given a field value and the type of the field, this method will try to
     * return a corresponding new object that contains the value given.
     *
     * @param fsFieldValue the value for the object as a String
     * @param fsFieldType the type of the object to create
     * @return Object the new object created, or null if none could be created
     */
    public static Object parseFieldValue( String fsFieldValue,
                                         String fsFieldType )
    {
        try
        {
            return ((Object)Formatter.parseString( fsFieldValue,
                                                  fsFieldType ) ) ;
        }
        catch ( ParseException loParseExcep )
        {
            return null ;
        }
    }
    
    
    /**
     * This method will try to retrieve (lookup) an existing object in
     * the Ariba system and will return this object to the caller, if found.
     *
     * @param fsFieldValue the value for the lookup key as a String
     * @param fsFieldType the type of the object to get
     * @param foPartition the Ariba partition to use to perform the lookup in
     * @return BaseObject the object retrieved
     */
    public static BaseObject lookupFieldValue( String fsFieldValue, 
                                              String fsFieldType, 
                                              Partition foPartition  )
    {
        return (BaseObject)(Base.getService().objectMatchingUniqueName( fsFieldType,
                                                                       foPartition,
                                                                       fsFieldValue ) ) ;
    }
    
    /**
     * This method will try to reset the given field back to a null
     * value to indicate that the field was not set.
     *
     * @param foBaseObject the BaseObject the field is contained
     * @param fsFieldName the name of the field to reset
     * @param fsFieldType the type of the field to reset
     * @return boolean the success of this operation
     */
    public static boolean resetFieldValue( BaseObject foBaseObject,
                                          String fsFieldName,
                                          String fsFieldType )
    {
        try
        {
            if ( fsFieldType.equals( "java.lang.Boolean" ) )
            {
                foBaseObject.setDottedFieldValue( fsFieldName,
                                                 new Boolean( false) ) ;
            }
            else if ( fsFieldType.equals( "java.lang.Integer" ) ) 
            {
                foBaseObject.setDottedFieldValue( fsFieldName,
                                                 new Integer( 0 ) ) ;
            }
            else
            {
                foBaseObject.setDottedFieldValue( fsFieldName, null ) ;
            }
            
            return true ;    
        }
        catch ( Exception loExcep )
        {
            Log.customer.debug( "Error: unable to reset field " + fsFieldName ) ;
            Log.customer.debug( loExcep.getMessage() ) ;
            return false ;
        }
    }
}
