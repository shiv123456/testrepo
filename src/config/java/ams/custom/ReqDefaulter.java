// 10/10/2003: Updates for Ariba 8.1 (Richard Lee)
// Ariba 8.1 Replaced all the elementAt with get
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/

package config.java.ams.custom;

import ariba.base.fields.Action;
import ariba.base.fields.ValueInfo;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import java.util.List;
import ariba.base.core.ClusterRoot;
import ariba.base.core.BaseObject;
import ariba.base.core.Base;
import ariba.base.fields.FieldProperties;
import ariba.approvable.core.Approvable;
//import ariba.htmlui.fieldsui.Log;
import ariba.util.log.Log;


/**
    An action that updates the dates of the entries, based on
    the time card's EndingDate.
*/

//81->822 changed Vector to List
//81->822 changed ConditionValueInfo to ValueInfo
public class ReqDefaulter extends Action
{
    static final String Value = "Value";
    public static final String ClientName="ClientName";
    private static final ValueInfo[] parameterInfo =
        {
        new ValueInfo(TargetParam, true, IsScalar, "java.lang.String")
    };


    private static final String[] requiredParameterNames =
        {
        TargetParam
    };

    // allowed types for the value
    //private static final ValueInfo valueInfo =
    //new ValueInfo(IsScalar, "ariba.common.core.Accounting");

    private static final ValueInfo valueInfo =
        new ValueInfo(IsScalar, "ariba.base.core.BaseObject");

    private boolean paymentTxn=false;

    /**
    Executes the given action.

    @param object The object receiving the action.
    @param params Parameters used to perform the action.
       */
    public void fire (ValueSource object, PropertyTable params)
    {
        paymentTxn=false;
        String clientUniqueName =null;
        Log.customer.debug("Custom Action ReqDefaulter is called");

        BaseObject bo = (BaseObject)object;
        Log.customer.debug("This is the passed in object %s ", bo);

        Log.customer.debug("Class name of the object %s", bo.getClassName());

        if (bo == null) return;
        //BaseObject accounting = (BaseObject)bo.getDottedFieldValue("Accountings.SplitAccountings[0].Accounting");

        // FieldProperties cp = accounting.getFieldProperties("FieldDefault1");


        if (bo.getClassName().equals("ariba.common.core.SplitAccounting"))
        {
            Log.customer.debug("In the Accounting section");
            List fieldsVector = FieldListContainer.getAcctgFields();
            List fieldValuesVector = FieldListContainer. getAcctgFieldValues();
            clientUniqueName = (String)bo.getDottedFieldValue("ClientName.UniqueName");
            Log.customer.debug("This is the client UniqueName : %s ",
                                 clientUniqueName);
            if (clientUniqueName == null)
            {
                return;
            };
            setFieldLabels(fieldsVector,fieldValuesVector,bo,clientUniqueName);
        }
        if (bo.getClassName().equals("ariba.purchasing.core.ReqLineItem"))
        {
            Log.customer.debug("In the ReqLine section");
            List fieldsVector = FieldListContainer.getLineFields();
            List fieldValuesVector = FieldListContainer. getLineFieldValues();
            clientUniqueName = (String)bo.getDottedFieldValue("ClientName.UniqueName");
            Log.customer.debug("This is the client UniqueName: %s",
                                 clientUniqueName);
            if (clientUniqueName == null)
            {
                return;
            };
            setFieldLabels(fieldsVector,fieldValuesVector,bo,clientUniqueName);
        }
        if (bo.getClassName().equals("ariba.purchasing.core.Requisition"))
        {
            Log.customer.debug("In the ReqHeader section");
            List fieldsVector = FieldListContainer.getHeaderFields();
            List fieldValuesVector = FieldListContainer. getHeaderFieldValues();
            ariba.purchasing.core.Requisition req = ( ariba.purchasing.core.Requisition)bo;
            Log.customer.debug("Got this req1: %s",req);

            //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
            //Partitioned User to obtain the extrinsic BuysenseOrg field value.
            ariba.user.core.User RequesterUser = (ariba.user.core.User)req.getDottedFieldValue("Requester");
            //rgiesen dev SPL #39
            //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
            ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,req.getPartition());
            clientUniqueName = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.UniqueName");
            Log.customer.debug("This is the Client UniqueName: %s",
                                 clientUniqueName);
            if (clientUniqueName == null)
            {
                return;
            };
            setFieldLabels(fieldsVector,fieldValuesVector,bo,clientUniqueName);
        }

        //This is for PaymentLine Accounting
        if (bo.getClassName().equals("ariba.common.core.Accounting"))
        {
            Log.customer.debug("In the Accounting section");
            List fieldsVector = FieldListContainer.getAcctgFields();
            List fieldValuesVector = FieldListContainer. getAcctgFieldValues();
            clientUniqueName = (String)bo.getDottedFieldValue("ClientName.UniqueName");
            Log.customer.debug("This is the client UniqueName : %s ",
                                 clientUniqueName);
            if (clientUniqueName == null)
            {
                return;
            };
            paymentTxn=true;
            setFieldLabels(fieldsVector,fieldValuesVector,bo,clientUniqueName);
        }

    }
    public void setAccounting (ValueSource object, PropertyTable params)
    {

        BaseObject bo = (BaseObject)object;
        if(!(bo.getTypeName().equals("ariba.common.core.SplitAccounting")))
            bo =  (BaseObject)object.getDottedFieldValue("Accountings.SplitAccountings[0]");
        Log.customer.debug("This is the passed in object %s ", bo);

        Log.customer.debug("Class name of the object %s", bo.getClassName());

        if (bo == null) return;
        //BaseObject accounting = (BaseObject)bo.getDottedFieldValue("Accountings.SplitAccountings[0].Accounting");

        // FieldProperties cp = accounting.getFieldProperties("FieldDefault1");


        if (bo.getClassName().equals("ariba.common.core.SplitAccounting"))
        {
            Log.customer.debug("In the Accounting section");
            List fieldsVector = FieldListContainer.getAcctgFields();
            List fieldValuesVector = FieldListContainer. getAcctgFieldValues();
            String clientUniqueName = (String)bo.getDottedFieldValue("ClientName.UniqueName");
            Log.customer.debug("This is the client UniqueName : %s ",
                                 clientUniqueName);
            if (clientUniqueName == null)
            {
                return;
            };
            setFieldLabels(fieldsVector,fieldValuesVector,bo,clientUniqueName);
        }



    }
    public void setPaymentSummaryLabels (ValueSource object,
                                         PropertyTable params)
    {

        String clientUniqueName =null;
        Log.customer.debug("Custom Action setPaymentSummaryLabels in ReqDefaulter is called");

        BaseObject bo = (BaseObject)object;
        Log.customer.debug("This is the passed in object %s ", bo);

        Log.customer.debug("Class name of the object %s", bo.getClassName());

        if (bo == null) return;
        //This is for the PaymentSummary ReferenceAccounting
        if (!(bo.getClassName()).equals("ariba.base.core.DynamicBaseObject"))
        {
            if (((Approvable)bo).instanceOf("ariba.core.PaymentEform"))
            {
                Log.customer.debug("In the PaymentEform section");
                List fieldsVector = FieldListContainer.getRefAcctgFields();
                List fieldValuesVector = FieldListContainer.getRefAcctgFields();
                clientUniqueName = (String)bo.getDottedFieldValue("PaymentItems[0].ClientName.UniqueName");
                Log.customer.debug("This is the client UniqueName : %s ",
                                     clientUniqueName);
                if (clientUniqueName == null)
                {
                    return;
                };
                List refAcctgVector = (List)bo.getFieldValue("PaymentAcctgSummaryItems");
                BaseObject refAcctg = (BaseObject)refAcctgVector.get(0);
                paymentTxn=true;
                setFieldLabels(fieldsVector,
                               fieldValuesVector,refAcctg,clientUniqueName);
            }
        }
    }
    private void setFieldLabels( List fieldsVector,
                                List fieldValuesVector,
                                BaseObject bo,String clientUniqueName)
    {
        String tempfield = null;
        String label = null;

        String UniqueName = null;
        String targetFieldName = null;

        int fieldsCount = fieldsVector.size();


        for(int j=0; j< fieldsCount ; j++)
        {
            //tempfield = "FieldDefault" + j;
            tempfield = (String)fieldValuesVector.get(j);
            Log.customer.debug("Working on %s", tempfield);
            FieldProperties fp = bo.getFieldProperties(tempfield);
            if (fp == null)
            {
                Log.customer.debug("Got a null FieldProperties");
            }

            targetFieldName = (String)fieldsVector.get(j);
            UniqueName = (clientUniqueName+":"+targetFieldName);
            ClusterRoot fieldTable = Base.getService().objectMatchingUniqueName
                ("ariba.core.BuysenseFieldTable",
                 bo.getPartition(), UniqueName);

            if(fieldTable != null)
            {
                label = (String)fieldTable.getFieldValue("ERPValue");
                Log.customer.debug("This is the label: %s",tempfield,label);
            }
            if (label == null)
            {
                Log.customer.debug("Got a null FieldLabel");
            }
            Log.customer.debug("Setting %s with %s",tempfield,label);
            // Sunil -  this log prints too much information hense commenting
            // Log.customer.debug("FP is %s",fp);
            if (fp != null && label != null)
            {
                // Sunil -
                Log.customer.debug("Setting %s with %s",tempfield,label);
                fp.setPropertyForKey("Hidden",new Boolean(false));
                // Sunil - added the folowing line to deafult the field to editable and the logic following will
                // set it to the appropriate boolean value.
                fp.setPropertyForKey("Editable",new Boolean(true));
                fp.setPropertyForKey("Label",label);
            }
            if (label.equals("n/a"))
            {
                Log.customer.debug("Hit n/a section for this field %s",
                                     (String)fieldValuesVector.get(j));
                fp.setPropertyForKey("Hidden",new Boolean(true));
            }

            //VisibleOnRequisition and EditableOnRequisition
            //Read VisibleOnRequisition and EditableOnRequisition and set Hidden and Editable properties accordingly
            //Double negation was necessary for Editable property to change it from its default value which is true
            if (!((Boolean)fieldTable.getFieldValue("VisibleOnReq")).booleanValue())
            {
                Log.customer.debug("Hit VisibleOnReq = false section for this field %s",(String)fieldValuesVector.get(j));
                fp.setPropertyForKey("Hidden",new Boolean(true));
            }
            if (!((Boolean)fieldTable.getFieldValue("EditableOnReq")).booleanValue())
            {
                Log.customer.debug("Hit EditableOnReq = false section for this field %s",(String)fieldValuesVector.get(j));
                fp.setPropertyForKey("Editable",new Boolean(false));
            }
        }

    }

    /**
        Return the list of valid value types.
    */
    protected ValueInfo getValueInfo ()
    {
        return valueInfo;
    }

    /**
        Return the parameter names.
    */
    protected ValueInfo[] getParameterInfo ()
    {
        return parameterInfo;
    }

    /**
        Return the required parameter names.
    */
    protected String[] getRequiredParameterNames ()
    {
        return requiredParameterNames;
    }
}
