package config.java.ams.custom;

import ariba.base.core.BaseObject;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
import ariba.common.core.UserProfile;
/**
 *  Date : 24-Aug-2009
 *  ACP Requirement: Condition to set field editabiliy/Visibility for UserProfileDetails object while 
 *  while submitting a User Profile.
 */
public class BuysenseContractorUserProfileFieldsCondition extends Condition
{	
	private static final ValueInfo parameterInfo[];
	public boolean evaluate(Object obj, PropertyTable params) throws ConditionEvaluationException
	{		
		BaseObject baseobj  = (BaseObject)params.getPropertyForKey("SourceObject");	
		Log.customer.debug("Inside BuysenseContractorUserProfileFieldsCondition"+baseobj);
		if(baseobj.instanceOf("ariba.common.core.UserProfileDetails"))
		{						
			UserProfile userprof = (UserProfile)baseobj.getClusterRoot();						
			ariba.common.core.User partUser = (ariba.common.core.User)userprof.getDottedFieldValue("User");			
			ariba.user.core.User commonUser = (ariba.user.core.User)partUser.getUser();
			Log.customer.debug("BuysenseContractorUserProfileFieldsCondition commonUser "+commonUser);
			Boolean isContractor = (Boolean)commonUser.getFieldValue("IsContractor");
			if(isContractor!=null && isContractor.booleanValue())
	        {	        
				Log.customer.debug("Inside BuysenseContractorUserProfileFieldsCondition"+obj);
				Log.customer.debug("Inside BuysenseContractorUserProfileFieldsCondition returining false as user is a contractor"+obj);
	        	return false;
	        }
	        else
	        {
	        	Log.customer.debug("Inside BuysenseContractorUserProfileFieldsCondition returining true as user is a contractor"+obj);
	        	return true;
	        }
	    }
		else
		{
			Log.customer.debug("Inside BuysenseContractorUserProfileFieldsCondition returining false as not instance"+obj);
			return false;
		}		
	}
	 protected ValueInfo[] getParameterInfo()
	 {
		 return parameterInfo;
	 }
	 static
     {
		 parameterInfo = (new ValueInfo[] {
         new ValueInfo("SourceObject", 0)
         });
	 }
}
