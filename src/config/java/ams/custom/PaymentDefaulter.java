// WA ST SPL # 475/Baseline Dev SPL # 229 - PaymentAccountings --> Accountings.PaymentAccountings
// 10/10/2003: Updates for Ariba 8.1 (Richard Lee)
// Ariba 8.1 replaced all the elementAt with get

package config.java.ams.custom;

import ariba.base.fields.Action;
import ariba.base.fields.ValueInfo;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import java.util.List;
import ariba.base.core.ClusterRoot;
import ariba.base.core.BaseObject;
import ariba.base.core.Base;
import ariba.base.fields.FieldProperties;
import ariba.approvable.core.Approvable;
import ariba.htmlui.fieldsui.Log;
/*
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method

*/

//81->822 changed Vector to List
//81->822 changed ConditionValueInfo to ValueInfo
public class PaymentDefaulter extends Action
{
    static final String Value = "Value";
    public static final String ClientName="ClientName";
    private static final ValueInfo[] parameterInfo =
        {
        new ValueInfo(TargetParam, true, IsScalar, "java.lang.String")
    };


    private static final String[] requiredParameterNames =
        {
        TargetParam
    };

    // allowed types for the value
    //private static final ValueInfo valueInfo =
    //new ValueInfo(IsScalar, "ariba.common.core.Accounting");

    private static final ValueInfo valueInfo =
        new ValueInfo(IsScalar, "ariba.base.core.BaseObject");



    /**
    Executes the given action.

    @param object The object receiving the action.
    @param params Parameters used to perform the action.
       */
    public void fire (ValueSource object, PropertyTable params)
    {
        String clientUniqueName =null;
        Log.visibility.debug("Custom Action PaymentDefaulter is called");

        //BaseObject bo = (BaseObject)object;
        BaseObject bo = (BaseObject)object.getDottedFieldValue("PaymentItems[0]");

        if (bo == null) return;

        if (!(bo.getClassName()).equals("ariba.base.core.DynamicBaseObject"))
        {
            if (((Approvable)bo).instanceOf("ariba.core.PaymentEform"))
            {
                Log.visibility.debug("In the PaymentEformHeader section");
                //Approvable vs = (Approvable)getValueSourceContext();
                //Log.visibility.debug( "This is the approvable through value source: %s", vs);
                List fieldsVector = FieldListContainer.getPaymentHeaderFields();
                List fieldValuesVector = FieldListContainer. getPaymentHeaderFieldValues();
                ariba.approvable.core.Approvable apr = ( ariba.approvable.core.Approvable)bo;
                Log.visibility.debug("Got this Approvable %s",apr);
                //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
                //Partitioned User to obtain the extrinsic BuysenseOrg field value.
                ariba.user.core.User RequesterUser = (ariba.user.core.User)apr.getDottedFieldValue("Requester");
                //rgiesen dev SPL #39
                //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
                ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,apr.getPartition());
                clientUniqueName = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.UniqueName");
                Log.visibility.debug("This is the Client UniqueName: %s",
                                     clientUniqueName);
                if (clientUniqueName == null)
                {
                    return;
                };
                setFieldLabels(fieldsVector,
                               fieldValuesVector,bo,clientUniqueName);
            }
        }

        else
        {
            Log.visibility.debug("In the PaymentLine section");
            List fieldsVector = FieldListContainer.getPaymentLineFields();
            List fieldValuesVector = FieldListContainer. getPaymentLineFieldValues();
            clientUniqueName = (String)bo.getDottedFieldValue("ClientName.UniqueName");
            Log.visibility.debug("This is the client UniqueName: %s",
                                 clientUniqueName);
            if (clientUniqueName == null)
            {
                return;
            };
            setFieldLabels(fieldsVector,fieldValuesVector,bo,clientUniqueName);
        }

    }

    public void setAccounting (ValueSource object, PropertyTable params)
    {
        BaseObject bo =  (BaseObject)object.getDottedFieldValue("PaymentItems[0].Accountings.PaymentAccountings[0]");
        if (bo == null)
        {
            Log.visibility.debug("Received a null Accounting object in the PaymentDefaulter"); return;
        }

        if (bo.getTypeName().equals("ariba.core.PaymentAccounting"))
        {
            Log.visibility.debug("In the Accounting section");
            List fieldsVector = FieldListContainer.getAcctgFields();
            List fieldValuesVector = FieldListContainer. getAcctgFieldValues();
            String clientUniqueName = (String)bo.getDottedFieldValue("ClientName.UniqueName");
            Log.visibility.debug("This is the client UniqueName : %s ",
                                 clientUniqueName);
            if (clientUniqueName == null)
            {
                return;
            };
            setFieldLabels(fieldsVector,fieldValuesVector,bo,clientUniqueName);
        }

    }

    public void setRefAccounting (ValueSource object, PropertyTable params)
    {
        BaseObject bo = (BaseObject) object;
        if (bo == null)
        {
            Log.visibility.debug("Received a null PaymentEform object in the PaymentDefaulter"); return;
        }
        if (bo.getTypeName().equals("ariba.core.PaymentEform"))
        {
            List fieldsVector = FieldListContainer.getRefAcctgFields();
            List fieldValuesVector = FieldListContainer.getRefAcctgFields();
            String clientUniqueName = (String)bo.getDottedFieldValue("PaymentItems[0].ClientName.UniqueName");
            Log.visibility.debug("This is the client UniqueName : %s ",
                                 clientUniqueName);
            if (clientUniqueName == null)
            {
                return;
            };
            List refAcctgVector = (List)bo.getFieldValue("PaymentAcctgSummaryItems");
            if (!refAcctgVector.isEmpty())
            {
                BaseObject refAcctg = (BaseObject)refAcctgVector.get(0);
                setFieldLabels(fieldsVector,
                               fieldValuesVector,refAcctg,clientUniqueName);
            }
        }
    }

    private void setFieldLabels( List fieldsVector,
                                List fieldValuesVector,
                                BaseObject bo,String clientUniqueName)
    {
        String tempfield = null;
        String label = null;

        String UniqueName = null;
        String targetFieldName = null;

        int fieldsCount = fieldsVector.size();


        for(int j=0; j< fieldsCount ; j++)
        {
            //tempfield = "FieldDefault" + j;
            tempfield = (String)fieldValuesVector.get(j);
            Log.visibility.debug("Working on %s", tempfield);
            FieldProperties fp = bo.getFieldProperties(tempfield);
            if (fp == null)
            {
                Log.visibility.debug("Got a null FieldProperties");
            }

            targetFieldName = (String)fieldsVector.get(j);
            UniqueName = (clientUniqueName+":"+targetFieldName);
            ClusterRoot fieldTable = Base.getService().objectMatchingUniqueName
                ("ariba.core.BuysenseFieldTable",
                 bo.getPartition(), UniqueName);

            if(fieldTable != null)
            {
                label = (String)fieldTable.getFieldValue("ERPValue");
                Log.visibility.debug("This is the label: %s",tempfield,label);
            }
            if (label == null)
            {
                Log.visibility.debug("Got a null FieldLabel");
            }
            Log.visibility.debug("Setting %s with %s",tempfield,label);
            if (fp != null && label != null)
            {
                fp.setPropertyForKey("Hidden",new Boolean(false));
                fp.setPropertyForKey("Editable",new Boolean(true));
                fp.setPropertyForKey("Label",label);
            }
            if (label.equals("n/a"))
            {
                Log.visibility.debug("Hit n/a section for this field %s",
                                     (String)fieldValuesVector.get(j));
                fp.setPropertyForKey("Hidden",new Boolean(true));
            }
            //VisibleOnPayment and EditableOnPayment
            //Read VisibleOnPayment and EditableOnPayment and set Hidden and Editable properties accordingly
            //Double negation was necessary for Editable property to change it from its default value which is true
            if (!((Boolean)fieldTable.getFieldValue("VisibleOnPayment")).booleanValue())
            {
                Log.visibility.debug("Hit VisibleOnPayment = false section for this field %s",(String)fieldValuesVector.get(j));
                fp.setPropertyForKey("Hidden",new Boolean(true));
            }
            if (!((Boolean)fieldTable.getFieldValue("EditableOnPayment")).booleanValue())
            {
                Log.visibility.debug("Hit EditableOnPayment = false section for this field %s",(String)fieldValuesVector.get(j));
                fp.setPropertyForKey("Editable",new Boolean(false));
            }
        }
    }

    /**
        Return the list of valid value types.
    */
    protected ValueInfo getValueInfo ()
    {
        return valueInfo;
    }

    /**
        Return the parameter names.
    */
    protected ValueInfo[] getParameterInfo ()
    {
        return parameterInfo;
    }

    /**
        Return the required parameter names.
    */
    protected String[] getRequiredParameterNames ()
    {
        return requiredParameterNames;
    }
}
