package config.java.ams.custom;

import java.util.List;

import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.purchasing.core.PurchaseOrder;
import ariba.util.core.ListUtil;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
import ariba.workforce.core.Contractor;
import ariba.workforce.core.LaborLineItemDetails;
import ariba.workforce.core.TimeSheet;

public class BuysenseSetOrderForTimeSheet extends Action
{

	public void fire(ValueSource obj, PropertyTable params) throws ActionExecutionException 
	{
		TimeSheet ts=(TimeSheet)obj;
		Log.customer.debug("BuysenseSetOrderForTimeSheet Clusterroot returned  = " + ts);
		PurchaseOrder initialtimesheetOrder =(PurchaseOrder)ts.getOrder();
		Log.customer.debug("BuysenseSetOrderForTimeSheet Purchase Order is = " + initialtimesheetOrder);
		if(initialtimesheetOrder!=null)
		{
			if(initialtimesheetOrder!=null && !(((initialtimesheetOrder.getStatusString()).equalsIgnoreCase("Ordered"))||((initialtimesheetOrder.getStatusString()).equalsIgnoreCase("Receiving"))))
			{
				List llidList = Contractor.getCurrentLaborLineItemDetails(ts.getRequester(), ts.getPartition());			
				if(!ListUtil.nullOrEmptyList(llidList) && llidList.size() >= 1)
		        {
		        	boolean orderflag=true;
		        	for(int i=0;i<llidList.size();i++)
		        	{
		        		LaborLineItemDetails llid = (LaborLineItemDetails)llidList.get(i);
		                if(llid.getLineItem() != null && llid.getLineItem().getLineItemCollection() != null)
		                {
		                    PurchaseOrder po = (PurchaseOrder)llid.getLineItem().getLineItemCollection();
		                    if((po.getFieldValue("StatusString").equals("Received")))
		                    {
		                    	continue;
		                    }
		                    else if((po.getFieldValue("StatusString").equals("Ordered"))||(po.getFieldValue("StatusString").equals("Receiving")))
		                    {
		                    	ts.setOrder(po);
		                    	orderflag=false;
		                    	break;
		                    }
		                }
		        	}     
		            if(orderflag)
		            {
		            	LaborLineItemDetails llid = (LaborLineItemDetails)llidList.get(0);
		            	if(llid.getLineItem() != null && llid.getLineItem().getLineItemCollection() != null)
		                {
		            		PurchaseOrder po = (PurchaseOrder)llid.getLineItem().getLineItemCollection();
		            		ts.setOrder(po);
		                }
		            
		        	}            
		        }
				else
				{
					ts.setOrder(null);		        	
				}
			}
			
		}
		
		
		
	}
	

}
