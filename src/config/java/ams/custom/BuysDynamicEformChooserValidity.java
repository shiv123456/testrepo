package config.java.ams.custom;


import java.util.List;

import ariba.approvable.core.Approvable;
import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.core.Partition;
import ariba.htmlui.fieldsui.Log;
import ariba.util.core.ListUtil;

public class BuysDynamicEformChooserValidity extends Condition
{
	private static final List    NoErrorResult = ListUtil.list(Constants.getInteger(0));
	protected String             sResourceFile = "buysense.DynamicEform";

    String msMessage = null;
    
    public List valVector;
    public static final String TargetValue1Param = "TargetValue1";
    private static final ValueInfo parameterInfo[];
    private static final String requiredParameterNames[] = 
        {
        "TargetValue1"
    };

    public ConditionResult evaluateAndExplain(Object object, PropertyTable params)
    {
        Log.customer.debug("Calling BuysDynamicEformChooserValidity for new Eform Profile validation.");
        if(!evaluate(object, params))
        {
            String retMsg = null;
            if(!StringUtil.nullOrEmptyOrBlankString(msMessage))
            {
                retMsg = msMessage;
                msMessage = null;
                return new ConditionResult(retMsg);
            }
            else
                return null;
        }
        return null;
    }
    
    public boolean evaluate(Object object, PropertyTable params)
    {
    	String sProfName = null;

		BaseObject baseobj  = (BaseObject)params.getPropertyForKey("SourceObject");	
		BaseObject oProfile = (BaseObject)params.getPropertyForKey("TargetValue1");
		Log.customer.debug("Inside BuysDynamicEformChooserValidity "+baseobj);

		
		if(((Approvable)baseobj).instanceOf("ariba.core.BuysDynamicEform"))
		{
     		Log.customer.debug("Inside BuysDynamicEformChooserValidity : Begin to validate the EformChooser field");
     		
     		if((ariba.user.core.User)baseobj.getDottedFieldValue("Requester") != null)
     		{
            ariba.user.core.User RequesterUser = (ariba.user.core.User)baseobj.getDottedFieldValue("Requester");

    	    Partition appPartition = baseobj.getPartition();

    	    ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,appPartition);
    	    BaseVector lvProfiles = (BaseVector)RequesterPartitionUser.getFieldValue("BuysEformProfiles");
    	    
    	    
            if(oProfile != null && oProfile.instanceOf("ariba.core.BuysEformProf"))
            {
            	sProfName = (String)oProfile.getFieldValue("ProfileName");
            }
            
            if(sProfName == null)
            {
            	msMessage = Fmt.Sil(Base.getSession().getLocale(),sResourceFile, "NoProfileError");
            	return false;
            }
            else if(lvProfiles == null)
            {
            	msMessage = Fmt.Sil(Base.getSession().getLocale(),sResourceFile, "NoUserProfileError");
            	return false;           	
            }
            else 
            {
            	return validateFields(sProfName,lvProfiles,baseobj);
            }
     		}
     		else
     		{
     			return false;
     		}
		}
		else
			return true;
    }    
    
    protected boolean validateFields(String lsProfName,BaseVector lvProfiles, BaseObject baseobj)
    {
    	List llProfileList = ListUtil.list();


    	int liSize = lvProfiles.size();
      	Log.customer.debug("The size of the list is :"+liSize);
        for(int i=0;i<liSize;i++)
    	   {
    	  		BaseObject oProfile1 = (BaseObject)Base.getSession().objectFromId((BaseId) lvProfiles.get(i));
    	   		if(oProfile1!=null)
    	   		{
         			Log.customer.debug("The object picked is :"+oProfile1+" and the profile name is"+oProfile1.getFieldValue("ProfileName"));
    	  			llProfileList.add((String)oProfile1.getFieldValue("ProfileName"));
    	   		}
        	}

        if(!(llProfileList.contains(lsProfName)))
           {
   		    msMessage = Fmt.Sil(Base.getSession().getLocale(),sResourceFile, "ProfileMismatchError");
   		    return false;    	    	
	       }
        else
    	    return true;
    }
   
    
    
    private List getTargetValues(PropertyTable params)
    {
        // Ariba 8.1: List constructor is deprecated by ListUtil.newVector() 
        List<Object> targetValues = ListUtil.list();
        Object targetValue = params.getPropertyForKey("TargetValue1");
        if(targetValue != null)
            targetValues.add(targetValue);
        return targetValues;
    }
    
    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }
    
    protected String[] getRequiredParameterNames()
    {
        return requiredParameterNames;
    }
    
    public BuysDynamicEformChooserValidity()
    {
    }
    
	 static
     {
		 parameterInfo = (new ValueInfo[] {
         new ValueInfo("SourceObject", 0),
         new ValueInfo("TargetValue1", 0)
         });
	 }
}
