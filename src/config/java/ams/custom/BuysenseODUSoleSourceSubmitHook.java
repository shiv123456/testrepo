//package config.java.ams.custom;
package config.java.ams.custom;

import java.util.List;

import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableHook;
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseODUSoleSourceSubmitHook implements ApprovableHook
{
  private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));
  public List run(Approvable approvable)
  {
	  Approvable loAppr = approvable;
	  
	  Log.customer.debug("Inside BuysenseODUSoleSourceSubmitHook");
	  
	  Log.customer.debug("Setting the BuysenseReFireWorkFlow to false");
	  loAppr.setFieldValue("BuysenseReFireWorkflow", new Boolean("false"));
	  
	  String lsVendor = (String)approvable.getFieldValue("Vendor");
	  String lsOnlyMeetDeptNeedVal = (String)approvable.getFieldValue("OnlyMeetDeptNeedVal");
	  String lsOnlySourceVal = (String)approvable.getFieldValue("OnlySourceVal");
	  
	  if(StringUtil.nullOrEmptyOrBlankString(lsVendor) || StringUtil.nullOrEmptyOrBlankString(lsOnlyMeetDeptNeedVal) || StringUtil.nullOrEmptyOrBlankString(lsOnlySourceVal))
	  {
		  Log.customer.debug("The values for Department, Vendor , OnlyMeetDeptNeed and OnlySource are :"+lsVendor +","+lsOnlyMeetDeptNeedVal+","+lsOnlySourceVal);
		  return ListUtil.list(Constants.getInteger(-1),
                  "The values for the mandatory field(s) must be set.");
	  }
      else
      {
          return NoErrorResult;
      }
  }
}
