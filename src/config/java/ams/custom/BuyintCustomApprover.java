/************************************************************************************
 * Author:  Richard Lee
 * Date:    July 30, 2004
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 07/30/2004        Richard Lee            Integration Interface
 * 10/14/2004        Richard Lee            UAT SPL 26 - Errors from PreEncumbrance logic
 * 6/6/2007          Richard Lee            ST SPL 1562 - INT Duplicate Ariba Req TXNs
 *
 * @(#)BuyintCustomApprover.java     1.0 08/24/2004
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/

/*
 * rlee, 10/14/2004: UAT SPL 26 and 36 - Errors from PreEncumbrance logic not handling null values from Ariba 7.1
 *
 * rlee, August 2004: lntegration Interface for PreEncumbrance
 *
 * This program is called when Ariba notifyApprovalRequired is issued for the Custom Approver to approve a Requisition.
 * This program will send out this Requisition to ERP for PreEncumbrance.
 * @param approvalRequest - The request which needs to be approved
 * @param token - The token granting permission for approval
 * @param originalSubmission - False if this is a resubmission.
 */

package config.java.ams.custom;
import ariba.approvable.core.ApprovalRequest;
import ariba.approvable.core.CustomApproverDelegateAdapter;
import ariba.util.log.Log;
import ariba.base.core.Partition;
import ariba.base.core.*;
import ariba.common.core.*;
import ariba.purchasing.core.Requisition;
import ariba.util.core.StringUtil;
import ariba.util.formatter.BooleanFormatter;

public class BuyintCustomApprover extends CustomApproverDelegateAdapter implements BuyintConstants
{

    public void notifyApprovalRequired (ApprovalRequest approvalRequest,
                                        String token,
                                        boolean originalSubmission)
    {
        Log.customer.debug("Calling BuysenseCustomApprover.");
        String lsToken = token.trim();
        Requisition loReq = (Requisition)approvalRequest.getApprovable();
        TransientData td = Base.getSession().getTransientData();
        String lsTransToken = (String) td.get("CGICustomToken");

        if( !(StringUtil.nullOrEmptyOrBlankString(lsTransToken)))
        {
            Log.customer.debug("This Requisition has already been sent to ERP.");
            UpdateReq(loReq, lsToken);
            return;
        }

        if (loReq ==null)
        {
           Log.customer.debug("BuysenseCustomApprover: the req is null");
        }
        else
        {
            // Check if req has already been send to ERP
            String lsPECheck = (String) loReq.getDottedFieldValue("PreEncumbranceStatus");
            Boolean lbPreEncumbered = (Boolean) loReq.getFieldValue("PreEncumbered");

            if( StringUtil.nullOrEmptyOrBlankString(lsPECheck))
            {
                loReq.setFieldValue("PreEncumbranceStatus", STATUS_ERP_READY);
                lsPECheck = STATUS_ERP_READY;
            }

            if(StringUtil.nullOrEmptyOrBlankString(BooleanFormatter.getStringValue(lbPreEncumbered)))
            {
                loReq.setFieldValue("PreEncumbered", new Boolean(false));
                lbPreEncumbered = new Boolean(false);
            }
            if (!(
                 (lsPECheck.equalsIgnoreCase(STATUS_ERP_INPROGRESS))||
                 (lbPreEncumbered.booleanValue())
                ))
            {
                td.put("CGICustomToken", lsToken);
                BuyintXMLFactory.submit(TXNTYPE_SEND, loReq);
                UpdateReq(loReq, lsToken);
                Log.customer.debug("CustomApprover: After setting PreEncumbranceStatus to: " + loReq.getFieldValue("PreEncumbranceStatus"));
            }
            else
            {
                Log.customer.debug("Error: Inside BuyintCustomApprove. This Requisition is not READY for ERP.");
            }
        }
    }
    private void UpdateReq( Requisition loReq, String lsToken)
    {
        BaseObject loReqObject = (BaseObject) loReq;
        Partition loPartition = loReq.getPartition();
        String lsRecordType = "PreEncIRecord";
        String lsReqUN = (String) loReq.getDottedFieldValue("UniqueName");
        ariba.common.core.User loAribaSystemUser = Core.getService().getAribaSystemUser(loPartition);
        BuysenseUtil.createHistory(loReqObject,lsReqUN,lsRecordType,null,loAribaSystemUser);
        loReq.setFieldValue("CustomApproverToken",lsToken);
        loReq.setFieldValue("PreEncumbranceStatus", STATUS_ERP_INPROGRESS);
    }

}
