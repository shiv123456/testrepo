package config.java.ams.custom;

import ariba.approvable.core.condition.BoundedMoney;
import ariba.base.core.Base;
import ariba.base.fields.ConditionResult;
import ariba.base.fields.ValueInfo;
import ariba.procure.core.LineItemProductDescription;
import ariba.procure.core.ProductDescription;
import ariba.util.core.Constants;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

/**
 * CSPL-8060: 9r2: Unable to enter a negative amount for a line item
 * 
 * @author Pavan Aluri
 * @Date 23-Mar-2017
 * @version 1.0
 * @see ariba.procure.core.condition.ValidLineItemPrice
 * @Explanation During run time, ValidLineItemPrice class defaults Min param to 0. Hence validation fails for negative amounts.
 * So created this class bypassing setting the Min param and keeping all other validation logic same as ValidLineItemPrice class 
 * Note: The reference OOTB class is decompiled from 9r2 SP14 base
 */

public class BuysenseValidLineItemPrice extends BoundedMoney
{

    public BuysenseValidLineItemPrice()
    {
        parameterInfo = (new ValueInfo[] {
            new ValueInfo(ProductDescriptionParam, 0, "ariba.procure.core.ProductDescription")
        });
        requiredParameterNames = (new String[] {
            ProductDescriptionParam
        });
    }

    public boolean evaluate(Object value, PropertyTable params)
    {
        setConditionParams(params);
        return super.evaluate(value, params);
    }

    public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
    {
        setConditionParams(params);
        return super.evaluateAndExplain(value, params);
    }

    private boolean allowZeroItemPrice(ProductDescription pd)
    {
        return Base.getService().getBooleanParameter(pd.getPartition(), "Application.Procure.AllowZeroItemPrice");
    }

    private void setConditionParams(PropertyTable params)
    {
        Log.customer.debug("in BuysenseValidLineItemPrice::setConditionParams() ");
        LineItemProductDescription pd = (LineItemProductDescription)params.getPropertyForKey(ProductDescriptionParam);
        boolean allowZero = allowZeroItemPrice(pd);
        /*
         * commented below line to skip setting Min param to 0 to bypass minimum value validation 
         * in order to allow negative values for Price 
         */
        //params.setPropertyForKey("Min", "0");
        if(allowZero)
            params.setPropertyForKey("RangeInclusive", Constants.getBoolean(true));
        else
            params.setPropertyForKey("RangeInclusive", Constants.getBoolean(false));
        params.setPropertyForKey("CompatibleCurrency", pd.getFieldValue("LineItem.LineItemCollection.ReportingCurrency"));
        params.setPropertyForKey("InProgress", pd.getFieldValue("LineItem.InProgress"));
    }

    protected ValueInfo[] getParameterInfo()
    {
        ValueInfo parentInfo[] = super.getParameterInfo();
        return ValueInfo.concatInfoArrays(parentInfo, parameterInfo);
    }

    protected String[] getRequiredParameterNames()
    {
        String parentReqs[] = super.getRequiredParameterNames();
        return ValueInfo.concatStringArrays(parentReqs, requiredParameterNames);
    }

    public static final String ParameterAllowZeroItemPrice = "Application.Procure.AllowZeroItemPrice";
    public static String ProductDescriptionParam = "ProductDescription";
    private ValueInfo parameterInfo[];
    private String requiredParameterNames[];

}
