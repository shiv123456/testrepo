/*
    Copyright (c) 1996-2000 Ariba, Inc.
    All rights reserved. Patents pending.

    $Id: //ariba/release/7.0/current/code/sample/procure/misc/java/procure/server/TestPrintHTMLApprovableHook.java#2 $

    5/31/01 Adding the rest of the CommodityCodeApprovers/Watchers. RLee
*/

/* 08/13/2003: Updates for Ariba 8.0 (David Chamberlain) */
// 10/10/2003: Updates for Ariba 8.1 (Richard Lee)
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.base.core.ClusterRoot;
// Ariba 8.0: Commented out BaseObjectOnServer and ApprovableOnServer; they are no longer needed.
//import ariba.base.server.BaseObjectOnServer;
//import ariba.server.ormsserver.ApprovableOnServer;
import ariba.approvable.core.Approvable;
// Ariba 8.1 commented this out import ariba.util.core.Util;
import ariba.util.log.Log;
import java.util.Hashtable;
import java.util.Enumeration;


public class BuysenseOrgEformPrint
{
    static Hashtable hyHdrParams = new Hashtable();
    static Hashtable hoHdrParams = new Hashtable();
    static Hashtable hnHdrParams = new Hashtable();
    static Enumeration eParams = null, eoParams = null, eyParams = null;
    static String sSubString = "",
        sTag = null, snTag = null, soTag = null, syTag = null;
    static String sApprovers ="";
    static String sTotalApprovers="";
    static String SomeHead = "";
    static Object oValue = null, oNValue = null, yValue = null;
    static String sOldValue = "", sNewValue = "", sYesValue ="";
    static String soTagValue = "", snTagValue = "", sTagValue = "";
    static boolean matchOld = false, matchNew = false;
    
    /****************************************************
    * Create Hashtable for the Old Eform values
    *
    *****************************************************/
    
    static public String Eform(String sHdr,
                               String sReqType,
                               Approvable approvable,
                               Enumeration enParams,
                               Hashtable hnHdrParams,
                               String snTag, boolean bNewEform)
    {
        String [] DefaultName = 
            {
            
            "LineAcctText1Value",
                "FieldDefault7.Name","FieldDefault6.Name","FieldDefault5.Name",
                "FieldDefault4.Name","FieldDefault3.Name","FieldDefault2.Name",
                "FieldDefault1.Name","ReqLineCB1Value","ReqLineText1Value",
                "ReqHeadText1Value",
                "ReqHeadFieldDefault6.Name","ReqHeadFieldDefault5.Name",
                "ReqHeadFieldDefault4.Name","ReqHeadFieldDefault3.Name",
                "ReqHeadFieldDefault2.Name","ReqHeadFieldDefault1.Name"
        };
        
        Log.customer.debug("String [] DefaultName: " + DefaultName[0]);
        
        String clientUniqueName = "";
        String soSubString = "", sySubString = "";
        String syValue = "";
        String label = "";
        String sReqHeader1 = "";
        String nameStr = ".Name";
        String OldHead = "<!--Old.", YesHead = "<!--Yes.";
        ClusterRoot fieldTableCR;
        ClusterRoot oldBSOrg = null;
        int count = 0;
        int NumElement = DefaultName.length;
        int ioTag = 0, iyTag = 0;
        int ioTagEnd = -1, iyTagEnd = -1;
        int index = -1;
        
        ioTag = sHdr.lastIndexOf("<!--Old.");
        hoHdrParams = CreateHashTable(sHdr, ioTag, ioTagEnd, OldHead);
        
        if (!bNewEform)
        {
            iyTag = sHdr.lastIndexOf("<!--Yes.");
            hyHdrParams = CreateHashTable(sHdr, iyTag, iyTagEnd, YesHead);
            
            /****************************************************************
            * get a handle on the instance of the Eform being created
            * e.g. ariba.core.BuysenseOrg 114473gh3.2f,
            * which still has the old values we want until Eform is approved
            ****************************************************************/
            oldBSOrg = (ClusterRoot) approvable.getDottedFieldValue("KeyName");
            Log.customer.debug("line 101: After ClusterRoot oldBSOrg");
        }
        /****************************************************
        * Search Label Name for the Title column
        *
        *****************************************************/
        
        //gets e.g. A151DOA, which doesn't change with old and new
        clientUniqueName = (String)approvable.getDottedFieldValue("ClientName.UniqueName");
        Log.customer.debug("line 110, clientUniqueName: " + clientUniqueName);
        
        /*****************************************************
        * Step thru each key in the Old Hashtable
        *
        ******************************************************/
        // Put field values into the Hashtables
        //eoParams gets all the keys from left side of hash table,e.g.uniqueName
        eoParams = hoHdrParams.keys();
        while (eoParams.hasMoreElements())
        {
            NumElement = DefaultName.length;
            //put the next key in soTag as string
            
            soTag = eoParams.nextElement().toString();
            Log.customer.debug("line 127: soTag = " + soTag);
            soSubString = soTag.substring(4); //Ignore the first 4 char (old.)of string.
            //sLookupName = soTag.substring(4);
            
            /*******************************************************
            * For each hash key, cycle DefaultName [] to see if any match.
            ********************************************************/
            
            for (int i=0; i<NumElement; i++)
            {
                Log.customer.debug("line 137: count inside while: " + i);
                if (soSubString.equals(DefaultName[i]))
                {
                    Log.customer.debug("line 141: DefaultName inside while: " + DefaultName[i]);
                    count = i;
                    break;
                }
            }
            
            
            /********************************************************************
            * compare hash key with DefaultName[]
            * if match, do 3 lookups (label, old, new values), else do 2 lookups
            *********************************************************************/
            
            if (soSubString.equals(DefaultName[count]))
            {
                Log.customer.debug("line 157, Just inside if: count = " + count);
                //Go thru the BSFieldTable and find the pointer that matches DefaultName
                // e.g.A151DOA ReqHeadFieldDefault1 field, in order to find the label e.g.Entity or Agency
                //fieldTableCR will have the pointer
                
                index = DefaultName[count].indexOf(nameStr);
                
                if (index > 0)
                    DefaultName[count] = DefaultName[count].substring(0, index);
                Log.customer.debug("line 166: DefaultName[]: " + DefaultName[count]);
                
                fieldTableCR = BuysenseUtil.findField(clientUniqueName,
                                                      DefaultName[count]);
                Log.customer.debug("line 169: count++: " + count );
                
                /*****************************************
                * if handle is null, error in BuysenseFieldTable
                ********************************************/
                if (fieldTableCR==null)
                {
                    Log.customer.debug("line 176: Error: BuysenseFieldTable has a null field");
                    //Don't try to get the handle of DefaultName[count]
                }
                else
                {
                    
                    Log.customer.debug("line 182, count++: " + count );
                    /****************************************************
                    * Gets the value for the label. e.g. Entity
                    ****************************************************/
                    label = (String)fieldTableCR.getFieldValue("ERPValue");
                    Log.customer.debug("line 188, String label: " + label);
                    
                    /*************************************************
                    * if label is not used, do not show row on printout
                    *****************************************************/
                    if (label.equals("n/a"))
                    {
                        hoHdrParams.put(soTag, "");
                        Log.customer.debug("line 196, hoHdrParams");
                    }
                    else
                    {
                        sNewValue = getNewValue (approvable, soSubString);
                        Log.customer.debug("line 201, sNewValue: " + sNewValue);
                        
                        if(!bNewEform)
                        {
                            // call the getOldValue
                            sOldValue = getOldValue (oldBSOrg, soSubString);
                            Log.customer.debug("line 208, sOldValue:" + sOldValue);
                            
                            syValue = YesNo (sOldValue, sNewValue);
                            
                            sReqHeader1 = "<tr ALIGN=LEFT><td WIDTH=\"30%\"><Font Size=2>" + label + "</Font></td><td WIDTH=\"30%\"><Font Size=2>" + sOldValue + "</Font></td><td WIDTH=\"30%\"><Font Size=2>" + sNewValue + "</Font></td><td WIDTH=\"10%\"><Font Size=2>" + syValue + "</Font></td></tr>";
                            Log.customer.debug("line 214, sReqHeader1: " + sReqHeader1 );
                            //put the string value on the right side of hash table corresponding to the soTag key
                        }
                        else
                        {
                            sReqHeader1 = "<tr ALIGN=LEFT><td WIDTH=\"30%\"><Font Size=2>" + label + "</Font></td><td WIDTH=\"70%\"><Font Size=2>" + sNewValue + "</Font></td></tr>";
                            Log.customer.debug("line 222, sReqHeader1: " + sReqHeader1 );
                            //put the string value on the right side of hash table corresponding to the soTag key
                        }
                        
                        hoHdrParams.put(soTag, sReqHeader1);
                        Log.customer.debug("line 228, sreqheader1: ");
                        
                    }//if (label.equals("n/a"))
                    
                }//if (fieldTableCR==null), don't try to get the pointer
                
            }//if (soSubString.equals(DefaultName[count])), need to lookup the label
            
            //else just do two lookup (old and new field values, no need to look up field label name.)
            else
            {
                // call the getOldValue, e.g. SoSubString = CommCodeCatAppr40.Name.PrimaryString
                if(!bNewEform)
                {
                    sOldValue = getOldValue (oldBSOrg, soSubString);
                    hoHdrParams.put(soTag, sOldValue);
                    Log.customer.debug("line 248, not new eform" );
                }
                else
                {
                    sNewValue = getNewValue (approvable, soSubString);
                    hoHdrParams.put(soTag, sNewValue);
                    Log.customer.debug("line 253, new eform" );
                }
                
            }//else, no need to look up field name.
            
        }//while (eoParams.hasMoreElements())
        
        if(!bNewEform)
        {
            /***********************************************************
            * Yes Hashtable
            ***********************************************************/
            
            //Go thru Yes Hashtable to mark "Yes" for changes in each field
            eyParams = hyHdrParams.keys();
            while (eyParams.hasMoreElements())
            {
                
                //put the next key in syTag as string
                syTag = eyParams.nextElement().toString();
                sySubString=syTag.substring(4); //Ignore the first 4 char (old.)of string.
                Log.customer.debug("line 273, inside eyParams.hasMoreElements: sySubString = " + sySubString );
                
                soTagValue = TagValue(hoHdrParams, sySubString);
                Log.customer.debug("line 276 soTagValue = " + soTagValue);
                snTagValue = TagValue(hnHdrParams, sySubString);
                Log.customer.debug("line 278, snTagValue = " + snTagValue);
                Log.customer.debug("line 279 soTagValue = " + soTagValue);
                
                String soTV = (String) soTagValue;
                String snTV = (String) snTagValue;
                syValue = YesNo (soTV, snTV);
                Log.customer.debug("line 282, syValue = " + syValue);
                hyHdrParams.put(syTag, syValue);
                
                syValue = "";
                matchOld = false;
                matchNew = false;
            }//while (eyParams.hasMoreElements())
            
            sHdr = PSGFunctions.ReplaceHtml(sHdr, hyHdrParams);
            Log.customer.debug("ReplaceHtml for hyHdrParams" );
        }//if(!bNewEform)
        
        Log.customer.debug("line 295, before return to Hook.java");
        
        sHdr = PSGFunctions.ReplaceHtml(sHdr, hoHdrParams);
        Log.customer.debug("ReplaceHtml for hoHdrParams" );
        
        
        return sHdr;
    }
    
    /*** getOldValue ***************************************************/
    static String getOldValue (ClusterRoot oldBSOrg, String soSubString)
    {
        //Get the Before value of this field from BSOrg
        //Convert old key names (ApproverAmount is the key name used in the BuysenseOrg,
        //   but ApproverAmt is used in the BuysenseOrgEform.)
        //
        String comcode = "CommCode";
        String watchcom = "WatchCom";
        
        String incomecode = soSubString.substring(0,8);
        Log.customer.debug("line 313, incomecode: " + incomecode);
        
        
        if(soSubString.equals("ApproverAmt"))
            soSubString = "ApproverAmount";
        
        else if (soSubString.equals("ProcurementOfficerAmt"))
            soSubString = "ProcurementOfficerAmount";
        
        else if (soSubString.equals("FinOfficerAmt"))
            soSubString = "FinancialOfficerAmount";
        
        else if (soSubString.equals("FinOfficer.Name.PrimaryString"))
            soSubString = "FinancialOfficer.Name.PrimaryString";
        
        else if ((comcode.equals(incomecode)) || (watchcom.equals(incomecode)))
        {
            soSubString = convertlabel(soSubString);
            Log.customer.debug("line 329, soSubString: " + soSubString);
        }
        
        
        oValue = oldBSOrg.getDottedFieldValue(soSubString);
        Log.customer.debug("line 307: oValue " );
        if (oValue == null)
        {
            sOldValue = "null";
            Log.customer.debug("line 310: if (oValue == null):");
        }
        else
        {
            sOldValue = String.valueOf(oValue);
            Log.customer.debug("line 315: if (oValue == null) else: ");
        }
        
        if (sOldValue.equals(""))
        {
            sOldValue = "null";
            Log.customer.debug("line 326");
        }
        return sOldValue;
    }
    
    /*************************************************************************
    * Convert old field name used in BuysenseOrg to a common field name used in
    * in BuysenseOrgEform.
    ***************************************************************************/
    
    static String convertlabel (String soSubString)
    {
        
        int FindAppr = -1;
        int FindWatchC = -1;
        int FindWatcher = -1;
        String ApprNum = "", CodeNum = "", WatchNum = "", WatcherNum = "";
        FindAppr = soSubString.indexOf("Appr");
        FindWatchC = soSubString.indexOf("WatchC");
        FindWatcher = soSubString.indexOf("Watcher");
        
        int stringsize = soSubString.length(); //17 or 18 for ccc, 35 or 36 for ccca
        Log.customer.debug("Aline 406, stringsize: " + stringsize);
        // Is this a CommCodeCatAppr?
        if (FindAppr > 0)
        {
            ApprNum = soSubString.substring(15, (stringsize - 19));
            Log.customer.debug("Aline 412: ApprNum: " + ApprNum);
            soSubString = ("CommodityCodeCatApprover" + ApprNum + ".Name.PrimaryString");
            Log.customer.debug("Aline 414, soSubString: " + soSubString);
        }
        //Is this a WatchCommCode?
        else if (FindWatchC > 0)
        {
            WatchNum = soSubString.substring(16, (stringsize - 5));
            Log.customer.debug("Aline 412: WatchNum: " + WatchNum);
            soSubString = ("WatchCommodityCodeCat" + WatchNum + ".Name");
            Log.customer.debug("Aline 415, soSubString: " + soSubString);
        }
        //Is this a CommCodeCatWatcher?
        else if (FindWatcher > 0)
        {
            WatcherNum = soSubString.substring(18, (stringsize - 19));
            Log.customer.debug("Aline 412: WatcherNum: " + WatcherNum);
            soSubString = ("CommodityCodeCatWatcher" + WatcherNum + ".Name.PrimaryString");
            Log.customer.debug("Aline 417, soSubString: " + soSubString);
        }
        
        // This must be a CommCodeCat.
        else
        {
            CodeNum = soSubString.substring(11, (stringsize - 5));
            Log.customer.debug("Aline 418: CodeNum: " + CodeNum);
            soSubString = ("CommodityCodeCat" + CodeNum + ".Name");
            Log.customer.debug("Aline 420, soSubString: " + soSubString);
        }
        
        return soSubString;
        
    }
    
    
    /*** getNewValue *****************************************************/
    static String getNewValue (Approvable approvable, String soSubString)
    {
        oNValue = approvable.getDottedFieldValue(soSubString);
        Log.customer.debug("line 337: oNValue: ");
        if (oNValue == null)
            sNewValue = "null";
        else
            sNewValue = String.valueOf(oNValue);
        
        
        if (sNewValue.equals(""))
            sNewValue = "null";
        
        return sNewValue;
    }
    
    
    /***  Create Hashtables of the fields we need from the document ***/
    static Hashtable CreateHashTable (String sHdr,
                                      int iTag, int iTagEnd, String SomeHead)
    {
        Hashtable hHdrParams = new Hashtable();
        while (iTag >= 0)
        {
            iTagEnd = sHdr.indexOf("-->", iTag + 1);
            Log.customer.debug("line 363 : iTagEnd: " + iTagEnd);
            sTag = sHdr.substring(iTag + 4, iTagEnd);
            Log.customer.debug("line 365 : sTag: " + sTag);
            
            hHdrParams.put(sTag, "[?]");
            Log.customer.debug("line 368 hHdrParms: ");
            
            iTag = sHdr.lastIndexOf(SomeHead, iTag - 1);
            Log.customer.debug("line 371, iTag: " + iTag);
            
        }//while
        
        return hHdrParams;
    }
    
    /************************************************************/
    static String TagValue (Hashtable hHdrParams, String sySubString)
    {
        String sTagValue = "";
        boolean match = false;
        eParams = hHdrParams.keys();
        while (eParams.hasMoreElements())
        {
            
            sTag = eParams.nextElement().toString();
            Log.customer.debug("line 381: In TagValue, snTag: " + sTag);
            sSubString=sTag.substring(4); //Ignore the first 4 char (Hdr.)of string.
            Log.customer.debug("line 383, In TagValue, snSubString: " + sSubString );
            //CommCodeCatAppr1.Name.PrimaryString
            
            if (sySubString.equals(sSubString))
            {
                match = true;
                Log.customer.debug("line 399: sySubString: " + sySubString);
                Log.customer.debug("line 400: sSubString: " + sSubString);
                Log.customer.debug("line 401: snTag: " + sTag);
                break;
            }
        }//while (enParams.hasMoreElements())
        
        if (match)
        {
            match = false;
            
            
            sTagValue = (String) hHdrParams.get(sTag);
            Log.customer.debug("line 404: Inside matchOld, soTagValue = " + sTagValue);
            
        }
        else
        {
            Log.customer.debug("line 408: Did not find a match with Old Hashtable");
        }
        Log.customer.debug("line 434: sTagValue = " + sTagValue);
        return sTagValue;
        
    }
    
    
    /*** Changed Status: yes or no **********************************/
    
    static String YesNo(String OldValue, String NewValue)
    {
        String syValue = "";
        String OV = (String) OldValue;
        String NV = (String)NewValue;
        if (OV.equals(NV))
        {
            syValue = " ";
            Log.customer.debug("line 423: No change ");
        }
        else
        {
            syValue = "Yes";
            Log.customer.debug("line 428: Yes change ");
        }
        
        return syValue;
    }
    
    /**********************************************************************
    * End of Misc Functions
    ***********************************************************************/
    
}//end of Public Class BuysenseOrgEformPrint

