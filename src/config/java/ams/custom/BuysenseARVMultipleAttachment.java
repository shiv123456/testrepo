package config.java.ams.custom;

import ariba.htmlui.approvableui.fields.*;

/*
Purpose  : Custom controller of Multiple attachment
*/

public class BuysenseARVMultipleAttachment extends ARVAttachment
{
	public static final String ClassName = "config.java.ams.custom.BuysenseARVMultipleAttachment";
}
