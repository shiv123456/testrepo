package config.java.ams.custom;

import java.util.List;

import ariba.approvable.core.Approvable;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.ClusterRoot;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseSetNIGPDescription extends Action
{
    private static String msCN   = "BuysenseSetNIGPDescription";

    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        Log.customer.debug("***%s***Called fire:: valuesource obj %s", msCN, valuesource);

        if (valuesource != null && ((BaseObject) valuesource).instanceOf("ariba.core.BuysenseExemptionRequest"))
        {
            setNIGPDescription(valuesource, "NIGPCommodityCodes", "NIGPDescription");
        }
        if (valuesource != null && ((BaseObject) valuesource).instanceOf("ariba.core.BuysDynamicEform"))
        {
            setNIGPDescription(valuesource, "NIGPCommodityCodes", "NIGPDescription");
            setNIGPDescription(valuesource, "EformAcctNIGPCommodityCodes", "EformAcctNIGPDescription");
        }
    }

    @SuppressWarnings("unchecked")
    private void setNIGPDescription(ValueSource valuesource, String getterFieldName, String setterFieldName)
    {

        ClusterRoot loNIGPCode = null;
        String lsNIGPDescr = null;
        String lsNIGPFinalDescr = "";
        BaseId loNIGPbid = null;
        Approvable loAppr = (Approvable) valuesource;
        
        List<BaseId> loNIGPCodes = (List<BaseId>) loAppr.getFieldValue(getterFieldName);
        if (loNIGPCodes != null && loNIGPCodes.size() > 0)
        {
            boolean lbFirst = true;
            for (int k = 0; k < loNIGPCodes.size(); k++)
            {
                loNIGPbid = (BaseId) loNIGPCodes.get(k);
                loNIGPCode = (ClusterRoot) Base.getSession().objectFromId(loNIGPbid);
                lsNIGPDescr = (String) loNIGPCode.getFieldValue("NIGPDescription");

                if (lsNIGPDescr != null && lbFirst)
                {
                    lsNIGPFinalDescr = lsNIGPDescr;
                    lbFirst = false;
                }
                else if (lsNIGPFinalDescr != null && !lbFirst)
                {
                    lsNIGPFinalDescr = lsNIGPFinalDescr + "," + "  " + lsNIGPDescr;
                }
            }
        }

        if (!StringUtil.nullOrEmptyOrBlankString(lsNIGPFinalDescr))
        {
            Log.customer.debug("***%s***fire:: setting %s = %s", msCN, setterFieldName, lsNIGPFinalDescr);
            loAppr.setFieldValue(setterFieldName, lsNIGPFinalDescr);
        }
    }
}
