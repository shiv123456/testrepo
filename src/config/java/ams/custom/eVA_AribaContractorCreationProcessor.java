package config.java.ams.custom;
//import java.util.Map;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.MultiLingualString;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLResultCollection;
import ariba.procure.core.ProcureLineItem;
import ariba.purchasing.core.POLineItem;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.util.core.Fmt;
import ariba.util.core.FormatBuffer;
import ariba.util.core.StringUtil;
//import ariba.util.core.MapUtil;
import ariba.workforce.core.AribaContractorCreationProcessor;
import ariba.workforce.core.Contractor;
import ariba.workforce.core.ContractorCandidate;
import ariba.workforce.core.LaborLineItemDetails;
import ariba.workforce.core.Log;

/**
 *  Date : 24-Aug-2009
 *  ACP Requirement: The OOTB class AribaContractorCreationProcessor has been customized 
 *  in the process of contractor creation with Driver Licence and Client combination as 
 *  uniqueName. Driver Licence and Client name are being set while creating a new contractor.
**/

public class eVA_AribaContractorCreationProcessor extends AribaContractorCreationProcessor
{
	//public static Map mContractorMapTable = MapUtil.map();
	public Contractor findContractorForLineItem(LaborLineItemDetails llid)
    {
		Log.contractor.debug("Calling eVA_AribaContractorCreationProcessor.java"+llid);
		Contractor contractor = null;
        //ContractorCandidate candidate = llid.getSelectedCandidate();
		ContractorCandidate candidate = (ContractorCandidate) llid.getSelectedCandidate();
        if(candidate != null && candidate.getName() != null)
        {
            MultiLingualString name = candidate.getName();
            Log.contractor.debug("Inside If candidate is not null"+name);
            String partialGlobalID = candidate.getPartialGlobalID();
            if(name.getPrimaryString() != null && partialGlobalID != null)
            {            	
            	String  sClientUniqueName = (String) llid.getDottedFieldValue("WorkLocation.ClientName.UniqueName");
            	Log.contractor.debug("Calling contractorMatchingNameAndID"+sClientUniqueName);            	
            	String  sDriversLicenseStateNumber = (String) candidate.getDottedFieldValue("DriversLicenseStateNumber");
            	contractor = contractorMatchingNameAndID(sDriversLicenseStateNumber, llid.getPartition(),sClientUniqueName);
            }

        } else
        {
            Log.contractor.debug("skip finding contractor because no candidate info available: %s", llid);
        }
        /*
         * Here if 'contractor' is not null, means proposed candidate object already exists in Ariba system (could be inactive) 
         * If 'contractor' is null, system (ContrctorUtil) creates Contractor(using processor.createContractorForLineItem(llid)) Object and sets that to line item.
         * Send a notification, if 'contractor' is not null.
         * At this point,  re-activation notification is sent for development purpose.
         */
        if(contractor != null)
        {
        	Log.contractor.debug("eVA_AribaContractorCreationProcessor.findContractorForLineItem-contractor is not null");
        	ProcureLineItem li = llid.getLineItem();
        	if(li != null && li instanceof POLineItem)
        	{
        		POLineItem poline = (POLineItem) li;
        		Requisition req = (Requisition) poline.getRequisition();
        		ReqLineItem reqLineItem = (ReqLineItem) req.getLineItem(poline.getNumberOnReq());
            	BuysenseEmailNotifications nofication = new BuysenseEmailNotifications();
            	Log.contractor.debug("eVA_AribaContractorCreationProcessor.findContractorForLineItem- setting contractor "+contractor);
            	nofication.setContractor(contractor);
            	nofication.notifyUser(reqLineItem, true);
        	}
        }
        return contractor;
    }
	
	//CSPL-1690: changed the permission for this method to access from BuysenseEmailNotifications.java 
	 static final Contractor contractorMatchingNameAndID(String sDriversLicenseStateNumber, Partition partition, String sClientUniqueName)
	    {
		    Log.contractor.debug("Inside contractorMatchingNameAndID");		    	        
	        FormatBuffer aqlFmt = new FormatBuffer();
	        Contractor contractor = null;
	        //Fmt.B(aqlFmt, "SELECT t FROM ariba.workforce.core.Contractor t WHERE t.DriversLicenseStateNumber = '%s' AND t.ClientName.UniqueName = '%s'", escapeForAQLStringLiteral(sDriversLicenseStateNumber),escapeForAQLStringLiteral(sClientUniqueName));
	        Fmt.B(aqlFmt, "SELECT t FROM ariba.workforce.core.Contractor t WHERE t.DriversLicenseStateNumber = '%s' AND t.ClientName.UniqueName = '%s'", StringUtil.replaceCharByString(sDriversLicenseStateNumber, '\'', "''"),StringUtil.replaceCharByString(sClientUniqueName, '\'', "''"));	        
	        Log.contractor.debug("Querry Being Executed"+aqlFmt.toString());
	        AQLOptions opt = new AQLOptions();
	        opt.setRowLimit(0);
	        opt.setPartition(partition);
	        opt.setUseCache(false);
	        AQLResultCollection results = Base.getService().executeQuery(aqlFmt.toString(), opt);
	        if(!results.isEmpty())
	            while(results.next())
	            {
	                Object current[] = results.getCurrent();
	                contractor = (Contractor)((BaseId)current[0]).getIfAny();
	                try
	                {
	                	Log.contractor.debug("eVA_AribaContractorCreationProcessor::Before sleeping");
	                	Thread.sleep(2000);
	                	Log.contractor.debug("eVA_AribaContractorCreationProcessor::After sleeping");
	                }
	                catch(Exception e)
	                {
	                	Log.contractor.debug("eVA_AribaContractorCreationProcessor::Caught exception while sleeping " +e.getMessage());
	                }	                
	            }
	        Log.contractor.debug("Returning contractor"+contractor);
	        return contractor;
	    }

	 public Contractor createContractorForLineItem(LaborLineItemDetails llid)
	    {
		    Log.contractor.debug("Inside createContractorForLineItem");
		 	Contractor contractor = new Contractor(llid.getPartition());
	        contractor.save();
	        //ContractorCandidate candidate = llid.getSelectedCandidate();
	        ContractorCandidate candidate = (ContractorCandidate) llid.getSelectedCandidate();
	        BaseObject Client = (BaseObject) llid.getDottedFieldValue("WorkLocation.ClientName");
	        if(candidate != null && candidate.getName() != null)
	        {
	        	Log.contractor.debug("Inside createContractorForLineItem-if");
	            contractor.setName(candidate.getName());
	            contractor.setPartialGlobalID(candidate.getPartialGlobalID());
	            contractor.setFieldValue("ClientName", Client);
	            contractor.setFieldValue("DriversLicenseStateNumber", (String)candidate.getDottedFieldValue("DriversLicenseStateNumber"));
	        } else
	        {
	        	Log.contractor.debug("Inside createContractorForLineItem-else");
	            MultiLingualString name = new MultiLingualString(Partition.None);
	            String nameStr = llid.getContractorID();
	            name.setString(Base.getService().getDefaultLanguage(Partition.None), nameStr);
	            contractor.setName(name);
	            contractor.setFieldValue("ClientName", Client);
	            if(candidate != null)
	                contractor.setPartialGlobalID(candidate.getPartialGlobalID());
	            else
	                contractor.setPartialGlobalID(null);
	        }	        
	        return contractor;
	    }
	 public ariba.user.core.User createUserForContractor(Contractor contractor, LaborLineItemDetails llid)
	 {
		 Log.contractor.debug("Inside createUserForContractor");
		 ariba.user.core.User loContractorUser = super.createUserForContractor(contractor, llid);
		 Boolean Btrue= Boolean.valueOf(true);
		 loContractorUser.setFieldValue("IsContractor", Btrue);
		 Log.contractor.debug("Inside createUserForContractor returning user "+loContractorUser);
		 loContractorUser.save();
		 return loContractorUser;
	 }
}