package config.java.ams.custom;

import ariba.htmlui.eform.EformRegistration;
import ariba.htmlui.fieldsui.FieldsUIApplication;
import ariba.htmlui.orms.Application;

public class BuysenseEformDirectAccessRegistration implements EformRegistration
{

    @SuppressWarnings("static-access")
    public void setupWellKnownPages(FieldsUIApplication application)
    {
        ((Application) application).putWellKnownPage("CreateDynamicEform", "ariba.core.BuysDynamicEform");
        ((Application) application).putWellKnownPage("createbuyseform", "ariba.core.BuysenseOrgEform");
        ((Application) application).putWellKnownPage("createevaeueform", "ariba.core.BuysenseUserProfileRequest");
        ((Application) application).putWellKnownPage("createvmiameform", "ariba.core.BuysenseVMIAssetTracking");
        ((Application) application).putWellKnownPage("createdpexeform", "ariba.core.BuysenseExemptionRequest");
        ((Application) application).putWellKnownPage("createoduemeform", "ariba.core.BuysenseODUEmergencyProcurement");
        ((Application) application).putWellKnownPage("createodusseform", "ariba.core.BuysenseODUSoleSourceRequest");
        ((Application) application).putWellKnownPage("createdgssseform", "ariba.core.BuysenseSoleSourceRequest");
        ((Application) application).putWellKnownPage("createReqLineItem", "ariba.purchasing.core.ReqLineItem");
    }

    @SuppressWarnings("static-access")
    public void setupWellKnownPageAliases(FieldsUIApplication application)
    {
        ((Application) application).putWellKnownPageAlias("CreateDynamicEform", "ariba.core.BuysDynamicEform");
        ((Application) application).putWellKnownPageAlias("createbuyseform", "ariba.core.BuysenseOrgEform");
        ((Application) application).putWellKnownPageAlias("createevaeueform", "ariba.core.BuysenseUserProfileRequest");
        ((Application) application).putWellKnownPageAlias("createvmiameform", "ariba.core.BuysenseVMIAssetTracking");
        ((Application) application).putWellKnownPageAlias("createdpexeform", "ariba.core.BuysenseExemptionRequest");
        ((Application) application).putWellKnownPageAlias("createoduemeform", "ariba.core.BuysenseODUEmergencyProcurement");
        ((Application) application).putWellKnownPageAlias("createodusseform", "ariba.core.BuysenseODUSoleSourceRequest");
        ((Application) application).putWellKnownPageAlias("createdgssseform", "ariba.core.BuysenseSoleSourceRequest");
        ((Application) application).putWellKnownPage("createReqLineItem", "ariba.purchasing.core.ReqLineItem");
    }

    public static final String ClassName = "config.java.ams.custom.DynamicEformDirectAccessRegistration";

}
