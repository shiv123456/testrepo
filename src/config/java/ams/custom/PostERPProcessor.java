/*
    Copyright (c) 1996-1999 Ariba/Buysense, Inc.
    imohideen March 2000
    ---------------------------------------------------------------------------------------------------
   WA ST SPL # 475/Baseline Dev SPL # 229 - PaymentAccountings --> Accountings.PaymentAccountings

*/

/* 09/10/2003: Updates for Ariba 8.0 (David Chamberlain) */
// 10/10/2003: Updates for Ariba 8.1 (Richard Lee)
/*
03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/04/2004: Ariba 8.1 rgiesen dev SPL #38 - replace getLeadCurrency with Currency.getDefaultCurrency(partition)
03/11/2004: Ariba 8.1 rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
03/22/2004 - Ariba 8.1 Dev SPL #41 rgiesen - Created a custom object called BuysenseTXNStatus which is used instead of
	AdapterError (7.1) or IntegrationError (8.1).  This object is populated with the BuyINT record
	which has been updated by Advantage.
*/

package config.java.ams.custom;

import ariba.purchasing.core.PurchaseOrder;
import java.util.List;
import java.math.*;
import ariba.base.core.*;
import ariba.common.core.*;
import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.base.core.BaseObject;
// Ariba 8.0: IntegrationPostUpdateERPOrder.class has moved.
import ariba.purchasing.core.action.IntegrationPostUpdateERPOrder;
import ariba.purchasing.core.action.IntegrationPostLoadPOError;

import java.io.File;
import ariba.util.core.Assert;

import config.java.ams.custom.ResultStringParser;
// Ariba 8.1: added Vectorutil
import ariba.util.core.ListUtil;
// Ariba 8.1 added Money and Currency
import ariba.basic.core.Money;
import ariba.basic.core.Currency;

//81->822 changed Vector to List
public class PostERPProcessor
{
    //Ariba 8.1 Dev SPL #41 - instead of AdapterError or IntegrationError object, use the created
    // BuysenseTXNStatus object, which has been specially created for this process

    //IntegrationError adapterErr;
    ClusterRoot crBuysenseTXNStatus;

    String resultString;
    String txnType = null;
    ariba.purchasing.core.ERPOrder erpOrder ;
    ariba.purchasing.core.ERPOrder urOrder;
    ariba.purchasing.core.Requisition req;

    BaseObject pv;
    User currentUser;
    boolean erpProcessStatus = true;
    boolean result;
    PropertyTable params;
    String urUniqueName;
    String poUniqueName;
    final String LINEID = "LINEID";
    final String SUBLINEID = "SUBLINEID";
    final String ORMSFIELDNAME = "ORMSFIELDNAME";
    final String ORMSFIELDVALUE = "ORMSFIELDVALUE";
    final String ORMSFIELDMETHOD = "ORMSFIELDMETHOD";
    final String BODYTEXT = "Please refer to the history for more information. You may submit a new Payment.";
    final int RSLENGTH = 4000;
    String LINEITEMS = "LineItems";
    String ACCTGFIELD = "Accountings.SplitAccountings";

	//Ariba 8.1 Dev SPL #41 - instead of AdapterError or IntegrationError object, use the created
	// BuysenseTXNStatus object, which has been specially created for this process
    public void processPO(ClusterRoot pBuysenseTXNStatus,ariba.purchasing.core.ERPOrder po,
                          ariba.purchasing.core.Requisition requisition,
                          User aribaSystemUser,PropertyTable aeParams)
    {
		Log.customer.debug("PostERPProcessor:processPO(): pBuysenseTXNStatus: " + pBuysenseTXNStatus + " po: " + po + " requisition: " + requisition + " aribaSystemUser: " + aribaSystemUser + " aeParams: " + aeParams);

        crBuysenseTXNStatus = pBuysenseTXNStatus;
        erpOrder = po;
        req = requisition;
        ResultStringParser rsParser=null;
        currentUser = aribaSystemUser;

        txnType = "PC";
        //String resultStr = (String)ae.getFieldValue("resultstring");
        resultString = getResultString();
		Log.customer.debug("PostERPProcessor:processPO(): resultString: " + resultString);
        params = aeParams;

        if (resultString != null)
        {
            rsParser = getResultStringParser(req.getPartition());
            // VEPI PROD #552: Debugs for pullback issue
            debug("ResultStringParser object ID: " + rsParser.toString()); 

            //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
            //Partitioned User to obtain the extrinsic BuysenseOrg field value.
            ariba.user.core.User RequesterUser = (ariba.user.core.User)req.getDottedFieldValue("Requester");
            //rgiesen Ariba 8.1 dev SPL #39
            //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
            ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,req.getPartition());
            String clientName = (String)(RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName"));
            String clientID = (String)(RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientID"));
            String XMLClientTag = clientID.trim() + "_" + clientName.trim() ;
            Log.customer.debug("PostERPProcessor:processPO():ClientName: %s",XMLClientTag);
            rsParser.txnInit( XMLClientTag, txnType);
            rsParser.initData( resultString);

            //cast the req to a baseobject as createHistroy takes only BaseObject
            BaseObject reqObject = (BaseObject) req;
            createHistory(rsParser,resultString,reqObject,getPORecordType());
            BaseObject poObject = (BaseObject) erpOrder;
            createHistory(rsParser,resultString,poObject,getPORecordType());
            updateApprovables(rsParser,resultString,"PO");
            poUniqueName = (String) crBuysenseTXNStatus.getFieldValue("ORMSUniqueName");

            String ERPNUMBER = (String) crBuysenseTXNStatus.getFieldValue("erpnumber");

            erpOrder.setFieldValue("ERPPONumber",ERPNUMBER);
            //set the POERPStatus to Encumbered  - The ResultString process will set this now
            //erpOrder.setFieldValue("POERPStatus","Encumbered");

            if (erpProcessStatus) postPOprocess();
            else  failedPOProcess();
        }
    }

	//Ariba 8.1 Dev SPL #41 - instead of AdapterError or IntegrationError object, use the created
	// BuysenseTXNStatus object, which has been specially created for this process
    public void processReq(ClusterRoot pBuysenseTXNStatus,
                           ariba.purchasing.core.Requisition requisition,
                           User aribaSystemUser,PropertyTable aeParams)
    {
        crBuysenseTXNStatus = pBuysenseTXNStatus;
        req = requisition;
        txnType = "RX";
        currentUser = aribaSystemUser;
        Log.customer.debug("Begin processing the PO for the adapter Error object: %s",crBuysenseTXNStatus);

        resultString = getResultString();
        params = aeParams;
        if (resultString != null)
        {
            ResultStringParser rsParser = getResultStringParser(req.getPartition());
            // VEPI PROD #552: Debugs for pullback issue
            debug("ResultStringParser object ID: " + rsParser.toString()); 

            //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
            //Partitioned User to obtain the extrinsic BuysenseOrg field value.
            ariba.user.core.User RequesterUser = (ariba.user.core.User)req.getDottedFieldValue("Requester");
            //Ariba 8.1 rgiesen dev SPL #39
            //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
            ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,req.getPartition());
            String clientName = (String)(RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName"));
            String clientID = (String)(RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientID"));
            String XMLClientTag = clientID.trim() + "_" + clientName.trim() ;
            Log.customer.debug("PostERPProcessor:processReq():ClientName: %s",XMLClientTag);
            rsParser.txnInit( XMLClientTag, txnType);
            rsParser.initData( resultString);
            //cast the req to a baseobject as createHistroy takes only BaseObject
            BaseObject reqObject = (BaseObject) req;
            createHistory(rsParser,resultString,reqObject,getReqRecordType());
            updateApprovables(rsParser,resultString,"Req");
        }
    }
	//Ariba 8.1 Dev SPL #41 - instead of AdapterError or IntegrationError object, use the created
	// BuysenseTXNStatus object, which has been specially created for this process
    public void processPV(ClusterRoot pBuysenseTXNStatus,
                          BaseObject pvObject,
                          User aribaSystemUser,PropertyTable aeParams)
    {
        crBuysenseTXNStatus = pBuysenseTXNStatus;
        pv = pvObject;
        txnType = "PV";
        currentUser = aribaSystemUser;
        Log.customer.debug("Begin processing the PV for the adapter Error object: %s",crBuysenseTXNStatus);

        resultString = getResultString();
        params = aeParams;

        if (resultString != null)
        {
            ResultStringParser rsParser = getResultStringParser(pv.getPartition());
            // VEPI PROD #552: Debugs for pullback issue
            debug("ResultStringParser object ID: " + rsParser.toString()); 

            //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
            //Partitioned User to obtain the extrinsic BuysenseOrg field value.
            ariba.user.core.User RequesterUser = (ariba.user.core.User)pv.getDottedFieldValue("Requester");
            //Ariba 8.1 rgiesen dev SPL #39
            //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
            ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,pv.getPartition());
            String clientName = (String)(RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName"));
            String clientID = (String)(RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientID"));
            String XMLClientTag = clientID.trim() + "_" + clientName.trim() ;
            Log.customer.debug("PostERPProcessor:processPV():ClientName: %s",XMLClientTag);
            rsParser.txnInit( XMLClientTag, txnType);
            rsParser.initData( resultString);
            createHistory(rsParser,resultString,pvObject,getPVRecordType());
            updateApprovables(rsParser,resultString,"PV");
            //set the POERPStatus to Paid
            pv.setDottedFieldValue("Order.POERPStatus","Paid");
            Log.customer.debug("PostERPProcessor:processPV():erpProcessStatus: %s",
                           new Boolean(erpProcessStatus));
            if (erpProcessStatus)
            {
                Log.customer.debug("PostERPProcessor:processPV():About to call sendLiquidation");
                sendLiquidation(pvObject);
            }

            else
            {
                processFailedPV(pvObject);
            }
        }
    }

    private void processFailedPV(BaseObject pvObject)
    {
        ClusterRoot order = (ClusterRoot)pvObject.getFieldValue("Order");
        BaseId refBaseId = (BaseId)order.getBaseId();
        BaseSession baseSession = Base.getSession();
        order = baseSession.objectForWrite(refBaseId);

        //Get the order and payment lines.
        BaseVector polines = (BaseVector)order.getFieldValue("LineItems");
        BaseVector pvlines = (BaseVector)pvObject.getFieldValue("PaymentItems");

        //Substract the order lines NumberPaid by PayingQuantity from payment

        for(int i=0;i<polines.size();i++)
        {
            // Ariba 8.1: rlee, BaseObject item = (BaseObject)polines.elementAt(i);
            BaseObject item = (BaseObject)polines.get(i);
            // rlee, Ariba 8.1 BaseObject paymentItem = (BaseObject)pvlines.elementAt(i);
            BaseObject paymentItem = (BaseObject)pvlines.get(i);
            BigDecimal payingQty = (BigDecimal)paymentItem.getFieldValue("CurPayingQuantity");
            //BigDecimal inPorcessQty = (BigDecimal)item.getFieldValue("NumberInPaymentProcess");
            BigDecimal numberPaid = (BigDecimal)item.getFieldValue("NumberPaid");
            //BigDecimal numberAccepted = (BigDecimal)item.getFieldValue("NumberAccepted");

            //inPorcessQty=inPorcessQty.subtract(payingQty);
            numberPaid=numberPaid.subtract(payingQty);
            Log.customer.debug("PostERPProcessor:processFailedPV(): Updating the NumberPaid with %s",numberPaid);
            //item.setFieldValue("NumberInPaymentProcess",inPorcessQty);
            item.setFieldValue("NumberPaid",numberPaid);
        }
        order.save();

        notifyRequester(pvObject);
    }

    //e2e Start
	//Ariba 8.1 Dev SPL #41 - instead of AdapterError or IntegrationError object, use the created
	// BuysenseTXNStatus object, which has been specially created for this process
    public void processUR (ClusterRoot pBuysenseTXNStatus,
                           ariba.purchasing.core.ERPOrder ur,
                           ariba.purchasing.core.Requisition requisition,
                           User aribaSystemUser,
                           PropertyTable aeParams)
    {

        crBuysenseTXNStatus = pBuysenseTXNStatus;
        urOrder = ur;
        req = requisition;
        ResultStringParser rsParser = null;
        currentUser = aribaSystemUser;
        Log.customer.debug("Begin processing the UR for the adapter Error object: %s", crBuysenseTXNStatus);
        txnType = "UR";
        resultString = getResultString();
        params = aeParams;

        if (resultString != null)
        {


            rsParser = getResultStringParser(ur.getPartition());
            // VEPI PROD #552: Debugs for pullback issue
            debug("ResultStringParser object ID: " + rsParser.toString()); 

            //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
            //Partitioned User to obtain the extrinsic BuysenseOrg field value.
            ariba.user.core.User RequesterUser = (ariba.user.core.User)ur.getDottedFieldValue("Requester");
            //Ariba 8.1 rgiesen dev SPL #39
            //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
            ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,ur.getPartition());
            String clientName = (String)(RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName"));
            String clientID = (String)(RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientID"));
            String XMLClientTag = clientID.trim() + "_" + clientName.trim();
            Log.customer.debug("PostERPProcessor:processUR():ClientName: %s",XMLClientTag);
            rsParser.txnInit(XMLClientTag, txnType);
            rsParser.initData(resultString);

            urUniqueName = (String) crBuysenseTXNStatus.getFieldValue("ORMSUniqueName");
            Log.customer.debug("PostERPProcessor:processUR: IXM UR : %s " , urUniqueName );
            //String ERPNUMBER = (String) ae.getFieldValue("erpnumber");
            //Log.customer.debug("%s: IXM Setting the ERPPONumber", ERPNUMBER);
            //erpOrder.setFieldValue("ERPPONumber",ERPNUMBER);
            //erpOrder.save();

            BaseVector urLines = (BaseVector) urOrder.getFieldValue("LineItems");
            for(int i = 0; i < urLines.size(); i++)
            {
                // rlee Ariba 8.1 BaseObject item = (BaseObject) urLines.elementAt(i);
                BaseObject item = (BaseObject) urLines.get(i);
                item.setFieldValue("Quantity", new BigDecimal("0"));
            }

            urOrder.save();


            //cast the req to a baseobject as createHistroy takes only BaseObject
            BaseObject reqObject = (BaseObject) req;
            createHistory(rsParser,resultString,reqObject,getPORecordType());
            BaseObject urObject = (BaseObject) urOrder;
            createHistory(rsParser, resultString, urObject, getPORecordType());
            updateApprovables(rsParser, resultString, "UR");

        }

        Log.customer.debug("PostERPProcessor:processUR: erpProcessStatus: " + erpProcessStatus);
        if (erpProcessStatus) postURprocess();
        else  failedURProcess();
    }
    //e2e End

    private void createHistory ( ResultStringParser rsParser,
                                String aeResultString,
                                BaseObject approvableObject,String recordType)
    {
        //Gets all the messages from the result string, using the parser

        java.util.List messageVector = rsParser.getAllMessages();

        BaseObject po_hist = (ariba.procure.core.SimpleProcureRecord) BaseObject.create("ariba.procure.core.SimpleProcureRecord",crBuysenseTXNStatus.getPartition());
        po_hist.setFieldValue("Approvable",approvableObject);
        po_hist.setFieldValue("ApprovableUniqueName",
                              approvableObject.getFieldValue("UniqueName"));
        ariba.util.core.Date  poDate =  new ariba.util.core.Date();
        po_hist.setFieldValue("Date", poDate);

        //Ariba 8.1 DEV SPL #41 - The currentUser is the partitioned aribasystem user.
        //However, it needs the shared user
	    //po_hist.setFieldValue("User", currentUser);
	    po_hist.setFieldValue("User", currentUser.getUser());
        po_hist.setFieldValue("RecordType",recordType);

        int mvSize = messageVector.size();
        for (int i=0;i<mvSize; i++)
        {
            // rlee Ariba 8.1 Log.customer.debug("Message %s from the MessageVector: %s",new Integer(i),(String)messageVector.elementAt(i));
            Log.customer.debug("PostERPProcessor:createHistory(): Message %s from the MessageVector: %s",
                           new Integer(i),(String)messageVector.get(i));
            // rlee Ariba 8.1 ((List)po_hist.getFieldValue("Details")).addElement((String)messageVector.elementAt(i));
            ((List)po_hist.getFieldValue("Details")).add((String)messageVector.get(i));

        }
        //Now add it to the approvable
        // Ariba 8.1 changed addElement to add
        ((List)approvableObject.getFieldValue("Records")).add(po_hist) ;

    }


    private String getPORecordType()
    {
        String recordType;
        String SEVERITY  = (String)crBuysenseTXNStatus.getFieldValue("ErrorColumn");
        if ( SEVERITY.equals("OK") ||       SEVERITY.equals("WARN") )
        {
            erpProcessStatus = true;
            recordType= "ESRecord";
        }
        else
        {
            erpProcessStatus = false;
            recordType= "EFRecord";
            result= true;
        }
        return  recordType  ;
    }

    private String getReqRecordType()
    {
        String recordType;
        String SEVERITY  = (String)crBuysenseTXNStatus.getFieldValue("ErrorColumn");
        if ( SEVERITY.equals("OK") ||       SEVERITY.equals("WARN") )
        {
            recordType= "PESRecord";
            req.setFieldValue("RequisitionStatus","PreEncumbered");
        }
        else
        {
            recordType= "PEFRecord";
            req.setFieldValue("RequisitionStatus","PreEncumberError");
        }
        return  recordType  ;
    }

    private String getPVRecordType()
    {
        String recordType;
        String SEVERITY  = (String)crBuysenseTXNStatus.getFieldValue("ErrorColumn");
        if ( SEVERITY.equals("OK") ||       SEVERITY.equals("WARN") )
        {
            erpProcessStatus = true;
            recordType= "PVSRecord";
        }
        else
        {
            erpProcessStatus = false;
            recordType= "PVFRecord";
        }
        return  recordType  ;
    }

    //e2e Start
    private String getURRecordType()
    {
        String recordType;
        String SEVERITY  = (String)crBuysenseTXNStatus.getFieldValue("ErrorColumn");
        if ( SEVERITY.equals("OK") || SEVERITY.equals("WARN") )
        {
            erpProcessStatus = true;
            recordType= "URSRecord";
        }
        else
        {
            erpProcessStatus = false;
            recordType= "URFRecord";
            result= true;
        }
        return  recordType;
    }

    private void postURprocess()
    {
        try
        {
            // Ariba 8.0: IntegrationPostUpdateERPOrder has moved.  See above import.
            // ariba.procure.server.action.IntegrationPostUpdateERPOrder postUpdateOrder
            // = new ariba.procure.server.action.IntegrationPostUpdateERPOrder();
            IntegrationPostUpdateERPOrder postUpdateOrder
                = new IntegrationPostUpdateERPOrder();
            postUpdateOrder.fire(urOrder,params) ;
        }
        catch (Exception e)
        {
            Log.customer.debug("Error occured while calling the fire method of IntegrationPostUpdateERPOrder for UR Order");
        }
    }

    private void failedURProcess()
    {
        Log.customer.debug("Hit the UR ERP error section");
        ClusterRoot poe = (ClusterRoot) ClusterRoot.create("ariba.integration.core.PurchaseOrderError", crBuysenseTXNStatus.getPartition());
        Log.customer.debug("This is the poe: %s",poe);
        poe.setFieldValue("AddedToReq",new Boolean(false));
        Log.customer.debug("Setting the id to %s",urUniqueName);
        poe.setFieldValue("Id",urUniqueName);
        Log.customer.debug("set the ponumber successfully");
        poe.setFieldValue("Type","AFRS Error");
        poe.setFieldValue("Date",new ariba.util.core.Date());

        BaseObject iei = (BaseObject)BaseObject.create("ariba.integration.core.IntegrationErrorItem",crBuysenseTXNStatus.getPartition());
        Log.customer.debug("This is the Integration Error Item: %s",iei);
        iei.setDottedFieldValue("ItemId","1");
        iei.setDottedFieldValue("ErrorMessage",
                                "Please refer to the history for details");
        List itemVector = (List)poe.getDottedFieldValue("Items");
        Log.customer.debug("This is the itemVector: %s",itemVector);
        // Ariba 8.1 itemVector.addElement(iei);
        itemVector.add(iei);

        poe.save();
        Log.customer.debug("Successfully saved poe");

        // Ariba 8.0: IntegrationPostLoadPOError has moved: see above import statement.
        /* ariba.procure.server.action.IntegrationPostLoadPOError poError =
                                        new ariba.procure.server.action.IntegrationPostLoadPOError();*/
        IntegrationPostLoadPOError poError = new IntegrationPostLoadPOError();
        Log.customer.debug("Before firing poError method: %s",poError);
        try
        {
            poError.fire((ariba.integration.core.PurchaseOrderError)poe,params);
        }
        catch (Exception e)
        {
            Log.customer.debug("Error occured while calling the fire method of PurchaseOrderError");
        }
    }
    //e2e End

    private void postPOprocess()
    {
        try
        {
            // Ariba 8.0: IntegrationPostUpdateERPOrder has moved.  See above import.
            //ariba.procure.server.action.IntegrationPostUpdateERPOrder postUpdateOrder
            // = new ariba.procure.server.action.IntegrationPostUpdateERPOrder();
            IntegrationPostUpdateERPOrder postUpdateOrder
                = new IntegrationPostUpdateERPOrder();
            postUpdateOrder.fire(erpOrder,params) ;
        }
        catch (Exception e)
        {
            Log.customer.debug("Error occured while calling the fire method of IntegrationPostUpdateERPOrder");
        }
    }

    private void failedPOProcess()
    {
        Log.customer.debug("Hit the ERP error section");

        ClusterRoot    poe = (ClusterRoot) ClusterRoot.create("ariba.integration.core.PurchaseOrderError", crBuysenseTXNStatus.getPartition());
        Log.customer.debug("This is the poe: %s",poe);
        poe.setFieldValue("AddedToReq",new Boolean(false));
        Log.customer.debug("Setting the id to %s",poUniqueName);
        poe.setFieldValue("Id",poUniqueName);
        Log.customer.debug("set the ponumber successfully");
        poe.setFieldValue("Type","AFRS Error");
        poe.setFieldValue("Date",new ariba.util.core.Date());

        BaseObject iei = (BaseObject)BaseObject.create("ariba.integration.core.IntegrationErrorItem",crBuysenseTXNStatus.getPartition());
        Log.customer.debug("This is the Integration Error Item: %s",iei);
        iei.setDottedFieldValue("ItemId","1");
        iei.setDottedFieldValue("ErrorMessage",
                                "Please refer to the history for details");
        List itemVector = (List)poe.getDottedFieldValue("Items");
        Log.customer.debug("This is the itemVector: %s",itemVector);
        // Ariba 8.1 changed to add itemVector.addElement(iei);
        itemVector.add(iei);

        poe.save();
        Log.customer.debug("Successfully saved poe");

        // Ariba 8.0: IntegrationPostLoadPOError has moved: see above import statement.
        /*ariba.procure.server.action.IntegrationPostLoadPOError poError =
                                          new ariba.procure.server.action.IntegrationPostLoadPOError();*/
        IntegrationPostLoadPOError poError = new IntegrationPostLoadPOError();
        Log.customer.debug("Before firing poError method: %s",poError);
        try
        {
            poError.fire((ariba.integration.core.PurchaseOrderError)poe,params);
        }
        catch (Exception e)
        {
            Log.customer.debug("Error occured while calling the fire method of PurchaseOrderError");
        }

    }

    private void updateApprovables(ResultStringParser rsParser,
                                   String resultString,String appType)
    {
        Log.customer.debug("In  updateApprovables");
        java.util.List actonVector = rsParser.getAllActions();
        Log.customer.debug("This is the action vector:%s ",actonVector);
        int actionVectorSize = actonVector.size();
        Log.customer.debug("Size of the vector: %s", new Integer(actionVectorSize));
        for(int j=0; j<actionVectorSize; j++)
        {
            // rlee Ariba 8.1 String actionElement = (String) actonVector.elementAt(j);
            String actionElement = (String) actonVector.get(j);
            int lineID = Integer.parseInt(rsParser.getActionValue(LINEID,
                                                                  actionElement).trim());
            int sublineID = Integer.parseInt(rsParser.getActionValue(SUBLINEID,
                                                                     actionElement).trim());
            String fieldName = rsParser.getActionValue(ORMSFIELDNAME,
                                                       actionElement).trim();
            String fieldValue = rsParser.getActionValue(ORMSFIELDVALUE,
                                                        actionElement).trim();
            String method = rsParser.getActionValue(ORMSFIELDMETHOD,
                                                    actionElement).trim();
            Log.customer.debug("Got this action from the result string");
            Log.customer.debug("LINEID:%s SUBLINEID:%s ORMSFIELDNAME:%s ORMSFIELDVALUE:%s ORMSFIELDMETHOD:%s",new Integer(lineID),
                           new Integer(sublineID),fieldName,fieldValue,method);

            if (appType.equals("PO"))
            {
                updatePOFields(lineID,sublineID,fieldName,fieldValue,method);
                updateReqFields(lineID,sublineID,fieldName,fieldValue,method);
            }
            if (appType.equals("Req"))
            {
                BaseObject reqObject = (BaseObject) req;
                updateFields(reqObject,
                             lineID,sublineID,fieldName,fieldValue,method);
            }
            if (appType.equals("PV"))
            {
                LINEITEMS = "PaymentItems";
                ACCTGFIELD = "Accountings.PaymentAccountings";
                BaseObject pvObject = (BaseObject) pv;
                updateFields(pvObject,
                             lineID,sublineID,fieldName,fieldValue,method);
            }

        }

    }


    private void updateReqFields(int lineID,
                                 int sublineID,
                                 String fieldName,
                                 String fieldValue, String method)
    {

        //As BuysenseIntegrator sets the line number based on the PO
        //we need to find out the corresponding req line and pass it on for
        //the updateFields while updating the Req.

        BaseObject bo = (BaseObject)req;
        //we want to do this only when we are handling the line level actions
        if (lineID > 0)
        {
            List templineitems = (List)erpOrder.getDottedFieldValue("LineItems");
            // rlee Ariba 8.1 BaseObject tempLI = (BaseObject)templineitems.elementAt(lineID-1);
            BaseObject tempLI = (BaseObject)templineitems.get(lineID-1);
            Log.customer.debug("POLine Item: %s ",tempLI);
            int reqLineID = ((Integer) tempLI.getDottedFieldValue("NumberOnReq")).intValue();
            Log.customer.debug("This is the number on Req: %s",
                           new Integer(reqLineID));
            updateFields(bo,reqLineID,sublineID,fieldName,fieldValue,method);
        }
        //This will take care of updating the header
        if (lineID == 0)
        {
            updateFields(bo,lineID,sublineID,fieldName,fieldValue,method);
        }
    }

    private void updatePOFields(int lineID,
                                int sublineID,
                                String fieldName,
                                String fieldValue, String method)
    {
        BaseObject bo = (BaseObject)erpOrder;
        updateFields(bo,lineID,sublineID,fieldName,fieldValue,method);
    }

    private void updateFields(BaseObject approvableObj,
                              int lineID,
                              int sublineID,
                              String fieldName,String fieldValue, String method)
    {
        if (lineID == 0)
        {
            Log.customer.debug("Calling updateField in the header");
            updateField(approvableObj,fieldName,fieldValue,method);
        }
        else if (lineID > 0)
        {
            if  (sublineID == 0)
            {
                List lineItems = (List)approvableObj.getDottedFieldValue(LINEITEMS);
                // rlee Ariba 8.1 BaseObject li = (BaseObject)lineItems.elementAt(lineID - 1);
                BaseObject li = (BaseObject)lineItems.get(lineID - 1);
                Log.customer.debug("Calling updateField in the line");
                updateField(li,fieldName,fieldValue,method);
            }
            else
            {
                List lineItems = (List)approvableObj.getDottedFieldValue(LINEITEMS);
                // rlee Ariba 8.1 BaseObject li = (BaseObject)lineItems.elementAt(lineID - 1);
                BaseObject li = (BaseObject)lineItems.get(lineID - 1);
                List actgs = (List)li.getDottedFieldValue(ACCTGFIELD);
                // rlee Ariba 8.1 BaseObject actgbo = (BaseObject)actgs.elementAt(sublineID - 1);
                BaseObject actgbo = (BaseObject)actgs.get(sublineID - 1);
                Log.customer.debug("Calling updateField in the in the subline");
                updateField(actgbo,fieldName,fieldValue,method);
            }
        }
    }


    private void updateField(BaseObject bo,
                             String fieldName,String fieldValue, String method)
    {
        try
        {
            Log.customer.debug("Setting %s with %s in %s. type: %s",
                           fieldName,fieldValue,bo,method);
            if (method.equals("String"))
            {
                bo.setDottedFieldValue(fieldName,fieldValue);
            }
            else if (method.equals("Boolean"))
            {
                bo.setDottedFieldValue(fieldName,new Boolean(fieldValue));
            }
            else if (method.equals("Object"))
            {
                //if it is an object type, we need to do a object matching
                //to get a handle on the object that matches the unique name

                if (fieldValue.equals("null"))
                {
                    Object tempFieldData = null ;
                    bo.setDottedFieldValue(fieldName,tempFieldData);
                }
                else
                {
                    ClusterRoot fieldDataTable = Base.getService().
                        objectMatchingUniqueName("ariba.core.BuysenseFieldDataTable", crBuysenseTXNStatus.getPartition(), fieldValue);
                    if (fieldDataTable == null)
                    {
                        Log.customer.debug("Did not find a matching object for UniqueName: %s",fieldValue);
                    }
                    else
                    {
                        Log.customer.debug("Setting: %s with %s",
                                       fieldName,fieldDataTable);
                        bo.setDottedFieldValue(fieldName,fieldDataTable);
                    }
                }
            }
            else if (method.equals("Money"))
            {
                BuysenseUtil bu = new BuysenseUtil();
                Log.customer.debug("Created new BuysenseUtil instance, and about to call BuysenseUtil.getBD");
                BigDecimal bd = BuysenseUtil.getBD(fieldValue, fieldName);
                Log.customer.debug("BuysenseUtil returned: " + bd.toString());
                //Ariba 8.1 rgiesen dev SPL #38 - replace getLeadCurrency with Currency.getDefaultCurrency(partition)
                bo.setDottedFieldValue(fieldName,
                                       new Money(bd,
                                                 Currency.getDefaultCurrency(bo.getPartition())));
                Log.customer.debug("successfully set Money field");
            }
            else
            {
                Log.customer.debug("Got a unknown method type: %s",method);
            }
        }
        catch (Exception e)
        {
            Log.customer.debug("Field Received for update does not exist in the approvable");
            // Ariba 8.0: Replaced deprecated Util.assertNonFatal with Assert.assertNonFatal.
            Assert.assertNonFatal(false,
                                  "Field Received for update does not exist in the approvable");
        }
    }

    private ResultStringParser getResultStringParser(Partition part)
    {
        String XMLFile = Base.getService().getParameter(part,
                                                        "Application.AMSParameters.ResultXMLFile");
        Log.customer.debug("getResultStringParser():Result XMLFile from AMSParameters:%s",XMLFile);

        //Create the file object
        File resultXML = new File(XMLFile);

        ResultStringParser rsp = new ResultStringParser(resultXML);
        return rsp;
    }

    private String getResultString()
    {
        String temprs = new String();
        String rsVariable = new String();
        String rsStr = (String)crBuysenseTXNStatus.getFieldValue("resultstring");
        for(int rsExtents=1; rsExtents<=9; rsExtents++)
        {
            rsVariable = "RESULTSEXTN" + rsExtents;
            temprs = (String)crBuysenseTXNStatus.getFieldValue(rsVariable);
            if (temprs != null && temprs.trim().length() != 0 )
            {
                while (temprs.length() < RSLENGTH) temprs += ' ';
                rsStr += temprs;
            }
        }
        Log.customer.debug("In getResultString():%s",rsStr.trim());
        return (rsStr.trim());
    }


    private void notifyRequester(BaseObject appObject)
    {
        //This method sends an email to the Requester about the ERPfailure.

        BaseId requesterBaseId = ((BaseObject)appObject.getDottedFieldValue("Requester")).getBaseId();
        Log.customer.debug("Requester's BaseId: %s",requesterBaseId);
        // Ariba 8.1 List recipients = new List();
        List recipients = ListUtil.list();

        // Ariba 8.1 changed to add recipients.addElement(requesterBaseId);
        recipients.add(requesterBaseId);

        String subject =  "Payment "+
            (String)appObject.getDottedFieldValue("UniqueName") + ":" +
            (String)appObject.getDottedFieldValue("Name")       + " - "+
            "Failed in ERP Processing";

        String body    = "Payment "+
            (String)appObject.getDottedFieldValue("UniqueName") + ":" +
            (String)appObject.getDottedFieldValue("Name")       + " - "+
            "Submitted by " ;

        String requester =    (String)appObject.getDottedFieldValue("Requester.Name.PrimaryString") ;
        Log.customer.debug("Requester: %s",requester);
        body = body + requester + " has failed in ERP Processing." + BODYTEXT ;
        // Ariba 8.1 BuysenseEmailAdapter.sendMail(recipients,subject,body);
        BuysenseEmailAdapter.sendMail(recipients,subject,body);


    }

    public void sendLiquidation(BaseObject paymentObject)
    {
        ClusterRoot cr = (ClusterRoot)paymentObject;
        BaseVector pvlines = (BaseVector)cr.getFieldValue("PaymentItems");
        ClusterRoot order = (ClusterRoot)cr.getFieldValue("Order");
        //Create the liquidation transaction if the order that is being paid for is an ERPOrder
        //i.e enter the quantity fields on the order advRefAcctg vector on the ERPOrder and use it to create the liquidation txn.
        //These fields will be used by the buysense integrator for liquidation.

        if(order instanceof ariba.purchasing.core.ERPOrder)
        {
            boolean encumbered = ((Boolean)order.getFieldValue("Encumbered")).booleanValue();
            Log.customer.debug("The encumbered flag is %b",new Boolean(encumbered));
            if(encumbered)
            {
                //Generate the Liquidation transcation by calling AMSLiquidationGenerator
                Log.customer.debug("About to call the liquidationGenerator...");
                //Create the array of quantities
                //BigDecimal q[]={new BigDecimal(1)};
                BigDecimal quantities[]=new BigDecimal[pvlines.size()];
                for(int i=0;i<pvlines.size();i++)
                {
                    // rlee Ariba 8.1 BaseObject paymentItem = (BaseObject)pvlines.elementAt(i);
                    BaseObject paymentItem = (BaseObject)pvlines.get(i);
                    BigDecimal payingQty = (BigDecimal)paymentItem.getFieldValue("CurPayingQuantity");
                    quantities[i]=payingQty;
                }
                AMSLiquidationGenerator liquidationGenerator= new AMSLiquidationGenerator(cr,(PurchaseOrder)order,quantities);
                liquidationGenerator.generateLiquidation("PVX");
                Log.customer.debug("After calling the liquidationGenerator...");
            }
        }
    }
 
    public void debug(String fsMsg)
    {
        Log.customer.debug( "PostERPProcessor: " + fsMsg) ;
    }

}
