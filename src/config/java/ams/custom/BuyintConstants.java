/*
 * Name:             BuyintConstants
 * Description: Constants Interface for Integration.
 * Author:  David Chamberlain
 * Date:    July 30, 2004
 * Revision History:
 *
 *  Date              Responsible            Description
 *  ----------------------------------------------------------------------------------
 *  07/30/2004        David Chamberlain      Initial Version. Ariba 8.1 Upgrade
 *                                           Dev SPL's 88 and 89.
 * PVCS Modification :
 * $Log:   R:\Software\development\archives\Ariba 8.x Customizations\app\buyer\Server\classes\extensions\config\java\ams\custom\BuyintConstants.java-arc  $
 * 
 *    Rev 1.8   04 Jul 2006 09:09:36   ngrao
 * VEPI Dev SPL #670: INT - Updates to fix due to functional clarifications. 
 * 
 *    Rev 1.7   06 Mar 2006 13:53:14   ngrao
 * VEPI Dev SPL #670: INT - new cancel transaction to differentiate between pre-encumbrance negation due to order cancel vs. change order. 
 * 
 *    Rev 1.6   02 Mar 2006 13:58:10   rlee
 * Dev SPL 732 - INT - New Flag for ByPass Integration.
 *
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information"). You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *
 * DETAILED DESCRIPTION:
 * This is the constants interface for Integration. This interface will be used by all
 * other classes that need access to shared constants. Any constants that are used by
 * more than one integration class will be a member of this interface. To use this
 * interface, the other integration classes will implement this interface.
*/

package config.java.ams.custom;

public interface BuyintConstants
{
    //Transaction Statuses
    public final String STATUS_NEW = "NEW";
    public final String STATUS_READY = "READY";
    public final String STATUS_INPROGRESS = "INPROGRESS";
    public final String STATUS_COMPLETED = "COMPLETE";
    public final String STATUS_FAILED = "FAILED";
    public final String STATUS_ALL = "READY, INPROGRESS, COMPLETE, FAILED";

    //ERP Statuses
    public final String STATUS_ERP_READY = "NONE";
    public final String STATUS_ERP_NOT_ENABLED = "NOT ENABLED";
    public final String STATUS_ERP_BELOW_THRESHOLD = "BELOW THRESHOLD";
    public final String STATUS_ERP_USER_BYPASSED = "USER BYPASSED";
    public final String STATUS_ERP_EXTERNAL_ORDER = "EXTERNAL ORDER";
    public final String STATUS_ERP_INPROGRESS = "INPROGRESS";
    public final String STATUS_ERP_APPROVE = "APPROVE";
    public final String STATUS_ERP_DENY = "DENY";
    public final String STATUS_ERP_CANCELLED = "CANCELLED";
    public final String STATUS_ERP_SENT_TO_EPRO = "SENT TO EPRO";

    //Encumbrance Constants
    public static final String RECORD_ENCUMBERING = "EncRecord";
    public static final String RECORD_ENCUMBER_SUCCESS = "EncSRecord";
    public static final String RECORD_ENCUMBER_FAIL = "EncFRecord";
    public static final String RECORD_ENC_CANCEL = "EncCRecord";

        //Transaction Types
        public static final String TXNTYPE_SEND   = "SEND";
        public static final String TXNTYPE_CANCEL = "CNCL";
        public static final String TXNTYPE_ENC_CANCEL_ON_CHG = "CCNCL";
        public static final String TXNTYPE_PREENC_CANCEL = "PCNCL";
        public static final String TXNTYPE_RESPONSE = "RESP";
        public static final String TXNTYPE_DONE = "DONE";

        // Transaction Header Attlist
        public static final String ERP_MESSAGE = "erp_message";
        public static final String RESPONSE_TYPE = "response_type";
        public static final String INTEGRATION_SIGNER_RULE = "integration_signer_rule";
        public static final String PO_UNIQUE_NAME = "po_unique_name";

        public final static int RETRY_DEFAULT = 3 ;
        public final static int MAX_TRANSACTIONS_DEFAULT = 50 ;

        // BuysenseXMLImportException Error Codes
        public final int ERR_CODE_DEFAULT = 0 ;
        public final int ERR_CODE_SKIP_RETRY = 1 ;
}
