

/* 09/10/2003: Updates for Ariba 8.0 (Richard Lee) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/

package config.java.ams.custom;
import ariba.util.log.Log;
//Ariba 8.0: deprecated SimpleScheduledTask
//import ariba.server.objectserver.SimpleScheduledTask;
//import ariba.server.objectserver.core.WorkflowState;
//import ariba.server.ormsserver.ApprovableOnServer;
import ariba.base.core.Partition;
import java.util.Map;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Base;
import ariba.purchasing.core.PurchaseOrder;
import java.util.Vector;

//Ariba 8.0: added import scheduler
import ariba.util.scheduler.*;

//81->822 changed Hashtable to Map
public class CustomTxnPush extends ScheduledTask
//Ariba 8.0: deprecated public class CustomTxnPush extends SimpleScheduledTask
{
    String approvable = null;
    String approvableType = null;
    String fileName = null;
    Partition part ;

    public CustomTxnPush()
    {
    }

    /** We just want to persist the partition for the run method to use */
    public void init(Scheduler scheduler,
                     String scheduledTaskName, Map arguments)
    //Ariba 8.0: deprecated public void init(Partition partition, String scheduledTaskName, Map arguments)
    {

		//rgiesen dev SPL #39
        //part =  Base.getSession().getPartition();
        part =  Base.getService().getPartition();
        //Ariba 8.0: part =  partition;
        fileName = (String)arguments.get("FileName");
    }
    /**
    */
    public void run() throws ScheduledTaskException
    {
        Log.customer.debug("CustomTxnPush.run()  was called...");
        Log.customer.debug("Transaction File : %s",fileName);
        //call the CSV reader to get the list of approvables to push
        CSVReader csvr = new CSVReader();
        Vector appVector = csvr.readCSV(fileName);
        ClusterRoot cr = null;
        int appSize = appVector.size();

        for (int j = 0; j < appSize; j++)
        {
            Vector fieldsVector = (Vector)appVector.elementAt(j);
            approvableType = (String)fieldsVector.elementAt(0);
            approvable = (String)fieldsVector.elementAt(1);
            Log.customer.debug("Trying to get: %s of type : %s",
                           approvable,approvableType);
            cr = Base.getService().objectMatchingUniqueName
                (approvableType,part,approvable);

            Log.customer.debug("Got this approvable object: %s",cr);

            if (approvableType.equals("ariba.core.PaymentEform"))
                pushPayment(cr);
            if (approvableType.equals("ariba.core.Liquidation"))
                pushLiquidation(cr);
            //We sleep one minute before getting back to the next transaction
            try
            {
                Thread.sleep(60000);
            }
            catch(Exception e)
            {
                //Log.customer.debug("Problem sleeping");
            }
        }
    }
    //Code here is very similar to or a copy of how we push payments elsewhere
    public void pushPayment(ClusterRoot cr)
    {
        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        ariba.user.core.User RequesterUser = (ariba.user.core.User)cr.getDottedFieldValue("Requester");
        //rgiesen dev SPL #39
        //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,cr.getPartition());
        String clientName = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName");
        String clientID = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientID");
        String XMLClientTag = clientID.trim() + "_" + clientName.trim() ;

        AMSTXN.initMappingData();
        int rt=AMSTXN.push(XMLClientTag,
                           "PV", (String)cr.getFieldValue("UniqueName"),
                           "NEW",
                           "RDY",
                           "A",
                           "0",
                           "",
                           cr,
                           "PaymentItems",
                           "PaymentAccountings", cr.getPartition());
        Log.customer.debug("value of rt %s",rt);
    }

    //Code here is very similar to or a copy of how we push Liquidation elsewhere.
    //one big difference is
    //we figure out the txntype by inspecting the Liquidation object
    public void pushLiquidation(ClusterRoot cr)
    {
        String txntype;
        ClusterRoot refTran = (ClusterRoot)cr.getDottedFieldValue("RefTranObject");

        String classType = refTran.getTypeName();
        Log.customer.debug("This is the refTransObject class type: %s",classType);
        if (classType.equals("ariba.core.PaymentEform")) txntype ="PVX";
        else txntype = "PCX";

        PurchaseOrder order = (PurchaseOrder)cr.getDottedFieldValue("Order");
        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        ariba.user.core.User poRequesterUser = (ariba.user.core.User)order.getDottedFieldValue("LineItems[0].Requisition.Requester");
        //rgiesen dev SPL #39
        //ariba.common.core.User poRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(poRequesterUser,Base.getSession().getPartition());
        ariba.common.core.User poRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(poRequesterUser,order.getPartition());
        String clientName = (String)poRequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName");
        String clientID = (String)poRequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientID");

        String XMLClientTag = clientID.trim() + "_" + clientName.trim() ;
        Log.customer.debug("XMLClientTag %s",XMLClientTag);
        AMSTXN.initMappingData();
        Log.customer.debug("XMLClientTag %s",XMLClientTag);

        int rt=AMSTXN.push(XMLClientTag,
                           txntype, (String)cr.getFieldValue("UniqueName"),
                           "NEW",
                           "RDY",
                           "A",
                           "0",
                           "",
                           cr,
                           "LiquidationItems",
                           "LiquidationAccountings", cr.getPartition());

        try
        {
            Thread.sleep(60000);
        }
        catch(Exception e)
        {
            Log.customer.debug("Problem sleeping");
        }

    }

}
