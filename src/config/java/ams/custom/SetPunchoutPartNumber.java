
package config.java.ams.custom;

import ariba.common.core.punchout.PunchOut;
import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
import ariba.procure.core.*;
import ariba.util.core.*;

/*
*  CSPL-390 Add edit to require SupplierPartNumber for catalog and punchout items
*  This class is called by the "SetPunchoutPartNumber" trigger in ReqLineItemEditabilityFields.aml
*  It will set the SupplierPartNumber field on the LineItem.Description with a value "Missing Part #",
*  if the punchout items is null for this field.   
*
*/

public class SetPunchoutPartNumber extends Action
{
    private final String MISSINGPARTNUMBER = "Missing Part #";
    public void fire (ValueSource object, PropertyTable params)
    {
        try
        {
            Log.customer.debug("Calling SetPunchoutPartNumber");
            LineItemProductDescription loDescription = null;
            String lsPartNumber;
            PunchOut loPunchOut = null;

            if(object == null)
            {
                return;
            }
            if(object instanceof LineItemProductDescription)
            {
                loDescription = (LineItemProductDescription) object;
                lsPartNumber = (String) loDescription.getFieldValue("SupplierPartNumber");
                loPunchOut = (PunchOut) loDescription.getDottedFieldValue("LineItem.PunchOut");

                if ((StringUtil.nullOrEmptyOrBlankString(lsPartNumber)) && (loPunchOut != null))
                {
                    loDescription.setFieldValue("SupplierPartNumber", MISSINGPARTNUMBER);
                    Log.customer.debug("Setting SupplierPartNumber with Missing Part #. ");
                }
            }
        }
        catch(Exception loExcep)
        {
            Log.customer.debug("Exception while setting SupplierPartNumber on Punchout items.");
            loExcep.printStackTrace() ;
        }
    }
}
