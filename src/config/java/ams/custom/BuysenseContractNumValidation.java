package config.java.ams.custom;

import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ConditionResult;
import ariba.procure.core.LineItemProductDescription;
import ariba.purchasing.core.ReqLineItem;
import ariba.util.core.PropertyTable;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

/**
 * CER-23:End engagement email populated with null value
 * 
 * @author Pavan Aluri
 * @Date 25-May-2010
 * @version 1.0
 * @Explanation Validates Contract# field for Non-catalog items and it is required, when user 
 * selects NotInList for Contract Num List
 * 
 */

public class BuysenseContractNumValidation extends Condition
{
    private String        sClass                         = "BCNumValidation";
    private String        errorStringTable               = "ariba.procure.core";
    private String        sNotInListUniqueNameKey        = "NotInListUniqueName";
    private String        sContractNumRequiredValidation = "ContractNumRequiredValidation";
    private static String sErrorMessage                  = null;

    public boolean evaluate(Object obj, PropertyTable propertytable) throws ConditionEvaluationException
    {
        if (obj instanceof LineItemProductDescription)
        {
            LineItemProductDescription lipd = (LineItemProductDescription) obj;
            if (lipd != null && (lipd.getLineItem() instanceof ReqLineItem))
            {
                ReqLineItem reqLI = (ReqLineItem) lipd.getLineItem();
                return evaluateImplementation(reqLI, propertytable);
            }
        }
        return true;
    }

    public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
    {
        Log.customer.debug(sClass + ":evaluateAndExplain " + " - start");
        if (value instanceof LineItemProductDescription)
        {
            LineItemProductDescription lipd = (LineItemProductDescription) value;
            if (lipd != null && (lipd.getLineItem() instanceof ReqLineItem))
            {
                if (!evaluateImplementation((ReqLineItem) lipd.getLineItem(), params))
                {
                    if (sErrorMessage != null)
                    {
                        Log.customer.debug(sClass + " evaluateAndExplain-sErrorMessage:" + sErrorMessage);
                        return new ConditionResult(sErrorMessage);
                    }
                }
            }
        }
        return null;
    }

    private boolean evaluateImplementation(ReqLineItem reqLI, PropertyTable propertytable)
    {
        String sNotInListUniqueName = ResourceService.getString(errorStringTable, sNotInListUniqueNameKey);
        String sContractNumListUN = (String) reqLI.getDottedFieldValue("ContractNumList.UniqueName");
        String sDescContractNum = (String) reqLI.getDottedFieldValue("Description.ContractNum");
        if (reqLI.getIsAdHoc() && !StringUtil.nullOrEmptyOrBlankString(sContractNumListUN))
        {
            Log.customer.debug(sClass + " IsAdHoc:");
            if (sContractNumListUN.equals(sNotInListUniqueName)
                    && StringUtil.nullOrEmptyOrBlankString(sDescContractNum))
            {
                sErrorMessage = ResourceService.getString(errorStringTable, sContractNumRequiredValidation);
                Log.customer.debug(sClass + " returning false,  Contract# cant be null");
                return false;
            }
        }
        return true;
    }

}
