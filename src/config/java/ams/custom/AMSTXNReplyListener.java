//***************************************************************************/
//  Copyright (c) 1996-2000 Ariba Technologies, Inc.
//  All rights reserved. Patents pending.
//
//  Description:  Implementation of TXN reply listener
//
//  Change History:
//  04-09-01 KTL - Created Class
//
//***************************************************************************/
/*

08/13/2003: Updates for Ariba 8.0 (David Chamberlain)
09/09/2003: Updates for Ariba 8.0 (Richard Lee)
rgiesen 02/19/2004 Dev SPL #19 - Cleaned up imports
*/
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */


package config.java.ams.custom;

import java.util.List;
import ariba.base.core.*;
//import ariba.server.objectserver.ObjectServer;
import ariba.app.server.ObjectServer;
import ariba.util.log.Log;
// Ariba 8.1: Permission has moved to ariba.use...
import ariba.user.core.Permission;
import ariba.util.messaging.ReplyCallback;
import ariba.util.messaging.MessagingException;

//81->822 changed Vector to List
public class AMSTXNReplyListener implements ReplyCallback
// Ariba 8.0: IntegrationReplyListener deprecated: public class AMSTXNReplyListener extends IntegrationReplyListener
{

    private BaseId m_TXNBaseId;
    private ObjectServer m_objectServer;
    private boolean actionInvoked;
    private String failedTXNPermission = "FailedTXN";

    public AMSTXNReplyListener(BaseId receiptId, ObjectServer objectServer)
    {
        actionInvoked = false;
        m_TXNBaseId = receiptId;
        m_objectServer = objectServer;
    }

    //Ariba 8.0: public void action(IntegrationEventDataInterface eventData)
    public void onReply(Object response)
    {
        if(actionInvoked)
        {
            Log.customer.debug("AMSTXNReplyListener action was invoked twice");
            return;
        }
        else
        {
            actionInvoked = true;
            //Ariba 8.0: if(m_errorContext != null)
            //Ariba 8.0: moved to onException below: SendNotification();
            return;
        }
    }

    //Ariba 8.0: add void onException
    public void onException(MessagingException exception)
    {
        if(actionInvoked)
        {
            Log.customer.debug("AMSTXNReplyListener action was invoked twice");
            return;
        }
        else
        {
            actionInvoked = true;
            SendNotification();
            return;
        }
    }

    public void SendNotification()
    {
        ClusterRoot curTXN = (ClusterRoot) Base.getSession().objectFromId(m_TXNBaseId);

        if (curTXN == null)
        {
            Log.customer.debug("Could not retrieve TXN object with baseid " + m_TXNBaseId + ".");
            return;
        }

        Permission pa = Permission.getPermission(failedTXNPermission);

        if (pa == null)
        {
            Log.customer.debug("Could not retrieve permission " + failedTXNPermission + ".");
            return;
        }

        // Ariba 8.1: List::users() has been deprecated by List::getAllUsers()
        //List users = pa.users(curTXN.getPartition());
        List users = pa.getAllUsers();
        if(users == null || users.isEmpty())
        {
            Log.customer.debug("No users found that has the " + failedTXNPermission + " permission.");
            return;
        }
        else
        {
            String subject = "Approvable " + (String) curTXN.getFieldValue("ERPNUMBER") + " failed in transaction push.";
            String body = "The integration event to push transaction " +
                (String) curTXN.getFieldValue("TIN") +
                ", type " +
                (String) curTXN.getFieldValue("TXNTYPE") +
                ", Approvable UniqueName " +
                (String) curTXN.getFieldValue("ERPNUMBER") +
                " to the TXN integration tables timed out.";


            BuysenseEmailAdapter.sendMail(users, subject, body);
        }
    }
}
