package config.java.ams.custom;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.Comment;
import ariba.approvable.core.PrintApprovableHook;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseSession;
import ariba.base.core.LongString;
import ariba.approvable.core.ApprovalRequest;
import ariba.basic.core.Money;
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;
import ariba.util.formatter.BigDecimalFormatter;
import ariba.util.log.Log;
import ariba.util.net.HTMLPrintWriter;
import ariba.base.core.BaseVector;

public class BuysenseUserProfileRequestPrintHook implements PrintApprovableHook
{
    private static final List NoErrorResult = ListUtil.list(Constants.getInteger(NoError));
    Hashtable hHdrParams = new Hashtable();
        
    public List run(Approvable approvable, PrintWriter out, Locale locale, boolean printForEmail)
    {            
         HTMLPrintWriter loHout = new HTMLPrintWriter(out);
         String lsHtmlPage=null;
         String lsReqType=approvable.getTypeName();
         if (printForEmail)
         {
            Log.customer.debug("PrintForEmail is true");
            return NoErrorResult;
         }
         try
         {           
            if (lsReqType.equals("ariba.core.BuysenseUserProfileRequest"))
            {    
               int liTag = 0, liTagEnd = -1;
               String lsTag = null;
                 lsHtmlPage = PSGFunctions.ReadFile("config/htmlui/resource/ariba.core.BuysenseUserProfileRequest.html"); 
              
              liTag = lsHtmlPage.lastIndexOf("<!--Hdr.");
              liTagEnd = -1;
              Object loFieldVal=null;
              while (liTag >= 0)
              {
                liTagEnd = lsHtmlPage.indexOf("-->", liTag + 1);
                lsTag = lsHtmlPage.substring(liTag + 4, liTagEnd);
                Log.customer.debug("HdrKey: " + lsTag);
                hHdrParams.put(lsTag, "[?]");
                liTag = lsHtmlPage.lastIndexOf("<!--Hdr.", liTag - 1);
              }
              Enumeration loEnumParams = hHdrParams.keys();
              while (loEnumParams.hasMoreElements())
              {                  
                  lsTag = loEnumParams.nextElement().toString();
                  String sFieldName = lsTag.substring(4);

                 if((sFieldName.equals("AribaDelegatedPurchaseAuthorityAmt") && approvable.getDottedFieldValue(sFieldName) != null)||
                		 (sFieldName.equals("AribaExpenditureLimitAmt") && approvable.getDottedFieldValue(sFieldName) != null))
                       {
                         Money loProcMoney=(Money)approvable.getDottedFieldValue(sFieldName);                             
                         loProcMoney = new Money(BigDecimalFormatter.round(loProcMoney.getAmount(), 2), loProcMoney.getCurrency());
                         loFieldVal= (loProcMoney.asString()).substring(0, (loProcMoney.asString().indexOf(".")+3)).concat(" ").concat(loProcMoney.getCurrency().getSuffix());   
                         Log.customer.debug("Value of AribaDelegatedPurchaseAuthorityAmt or AribaExpenditureLimitAmt field  : "+loFieldVal);
                       }
                 else if(sFieldName.equals("username"))
                       {
                         String loStatus=(String)approvable.getDottedFieldValue("StatusString"); 
                         if(loStatus.equals("Denied"))
                         {
                            loFieldVal="Denied by:";
                         }
                         else
                         {
                           loFieldVal="Approved by:";                                         
                         }                                           
                       }
                 else if(sFieldName.equals("date"))
                       {
                         String loStatus=(String)approvable.getDottedFieldValue("StatusString"); 
                         if(loStatus.equals("Denied"))
                         {
                           loFieldVal="Denied Date:";
                         }
                        else
                        {
                           loFieldVal="Approved Date:";                                              
                        }                                               
                      }
                 else if(sFieldName.contains("Preparer"))
                     {
                        String lsObjval=sFieldName.substring(0,sFieldName.indexOf("@"));
                        Log.customer.debug("Object value: "+lsObjval);
                        String lsFieldVal=sFieldName.substring(sFieldName.indexOf("@")+1);
                        Log.customer.debug("field value: "+lsFieldVal);
                        loFieldVal=getReqValue(lsObjval,lsFieldVal,approvable);
                     }
                 else if(sFieldName.contains("$"))
                 {   
                     int liFirstIndex=sFieldName.indexOf('$');
                     int liLastIndex=sFieldName.indexOf('$', liFirstIndex+1);
                     String lsFieldName=sFieldName.substring(liLastIndex+2);  
                     String lsStringObj=sFieldName.substring(liFirstIndex+1,liLastIndex);                      
                     loFieldVal = getFieldValue(lsFieldName,lsStringObj,approvable);
                     Log.customer.debug("field value "+loFieldVal);
                 }
                 else if(sFieldName.contains("eMall"))
                 {
                	 loFieldVal = null;
                     Boolean lbeMall = (approvable.getFieldValue("eMall") == null)? false:(Boolean) approvable.getFieldValue("eMall");
                     Boolean lbOther = (approvable.getFieldValue("Other") == null)? false:(Boolean) approvable.getFieldValue("Other");
                     Boolean lbLogiXML = (approvable.getFieldValue("LogiXML") == null)? false:(Boolean) approvable.getFieldValue("LogiXML");
                     Boolean lbQuickQuote = (approvable.getFieldValue("QuickQuote") == null)? false:(Boolean) approvable.getFieldValue("QuickQuote");
                     Boolean lbVBOBuyer = (approvable.getFieldValue("VBOBuyer") == null)? false:(Boolean) approvable.getFieldValue("VBOBuyer");
                     Boolean lbElectronicForms = (approvable.getFieldValue("ElectronicForms") == null)? false:(Boolean) approvable.getFieldValue("ElectronicForms");
                     if(lbeMall)
                     {
                    	 if(loFieldVal == null)
                    	 {
                    		 loFieldVal = "eMall/eForms";
                    	 }
                    	 else
                    	 {
                    		 loFieldVal = loFieldVal + "," + "eMall/eForms";
                    	 }
                     }
                     if(lbElectronicForms)
                     {
                         if(loFieldVal == null)
                         {
                             loFieldVal = "eForms Only";
                         }
                         else
                         {
                             loFieldVal = loFieldVal + "," + "eForms Only";
                    	 }
                     }
                     if(lbLogiXML)
                     {
                    	 if(loFieldVal == null)
                    	 {
                    		 loFieldVal = "Logi Reporting";
                    	 }
                    	 else
                    	 {
                    		 loFieldVal = loFieldVal + "," + "Logi Reporting";
                    	 }
                     }
                     if(lbQuickQuote)
                     {
                    	 if(loFieldVal == null)
                    	 {
                    		 loFieldVal = "Quick Quote";
                    	 }
                    	 else
                    	 {
                    		 loFieldVal = loFieldVal + "," + "Quick Quote";
                    	 }
                     }
                     if(lbVBOBuyer)
                     {
                    	 if(loFieldVal == null)
                    	 {
                    		 loFieldVal = "Virginia Business Opportunities (Basic VBO)";
                    	 }
                    	 else
                    	 {
                    		 loFieldVal = loFieldVal + "," + "Virginia Business Opportunities (Basic VBO)";
                    	 }
                     }
                     if(lbOther)
                     {
                    	 if(loFieldVal == null)
                    	 {
                    		 loFieldVal = "Other";
                    	 }
                    	 else
                    	 {
                    		 loFieldVal = loFieldVal + "," + "Other";
                    	 }
                     }
                 }
                 else 
                     {   
                        loFieldVal = approvable.getDottedFieldValue(sFieldName);                        
                        if((sFieldName.equals("AcceptableAcknowledgementForm") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                        		(sFieldName.equals("QQReverseAuctionAccess") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                        		//(sFieldName.equals("OtherDataManagement") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                        		//(sFieldName.equals("OtherUserManagement") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                        		(sFieldName.equals("OthereProcurementVBO") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                        		(sFieldName.equals("OtherVSSAdminSetup") && loFieldVal != null && loFieldVal.toString().trim().equals("true")))
                        {
                          loFieldVal="Yes";
                        }
                        else if(sFieldName.equals("AcceptableAcknowledgementForm") && loFieldVal != null && loFieldVal.toString().trim().equals("false")||
                        		(sFieldName.equals("QQReverseAuctionAccess") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                        		//(sFieldName.equals("OtherDataManagement") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                        		//(sFieldName.equals("OtherUserManagement") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                        		(sFieldName.equals("OthereProcurementVBO") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                        		(sFieldName.equals("OtherVSSAdminSetup") && loFieldVal != null && loFieldVal.toString().trim().equals("false")))
                        {
                           loFieldVal="No";
                        }
                     }                                       
                if (loFieldVal == null)
                {
                   loFieldVal = "";
                }              
                 hHdrParams.put(lsTag, String.valueOf(loFieldVal));
             }
            lsHtmlPage = PSGFunctions.ReplaceHtml(lsHtmlPage, hHdrParams);            
            loHout.println(lsHtmlPage);  
          }
            
        }
        catch (Exception e) {
            Log.customer.debug(e);
          
          }
         return NoErrorResult;
     }
    
    public String getFieldValue(String fieldName,String stringObj,Approvable approvable)
    {
        Log.customer.debug("Inside getFieldValue method, fieldName :"+fieldName);        
        String lsResultValue="";           
        List loObjlist = (List)approvable.getFieldValue(stringObj);     
        Log.customer.debug("list:" +loObjlist);       
        BaseSession loSession = Base.getSession();       
            for (int i = 0; i < loObjlist.size(); i++)
            {             
                if(i>0)
                {
                    if(fieldName.equals("Text"))
                    {
                     lsResultValue=lsResultValue+"<br>";
                    }
                    else
                    {
                    lsResultValue=lsResultValue+", ";
                    }
                }                
                Object obj = loObjlist.get(i);                             
                BaseObject loObjectName = null;
                try
                {
                    if(obj instanceof Comment)
                    {
                       Comment loCmt = (Comment)loObjlist.get(i);   
                       loObjectName = loCmt;
                    }
                    else if(obj instanceof ApprovalRequest)
                        {
                          ApprovalRequest loAppReq=(ApprovalRequest)loObjlist.get(i);
                          loObjectName = loAppReq;
                        }
                        else
                        {    
                          BaseId loId = (BaseId)loObjlist.get(i);
                          loObjectName = loSession.objectFromId(loId);
                        }                                                   
                    Log.customer.debug("Inside for of getFieldValue, object:"+loObjectName);
                    Object loObjectValue=null;
                    if(fieldName.equals("Text"))
                    {                                    
                        LongString llsText = (LongString) loObjectName.getDottedFieldValue(fieldName);
                        if(llsText != null)
                        {
                           loObjectValue = (String) llsText.string();
                        }                    
                    }
                    else
                    {
                        loObjectValue = loObjectName.getDottedFieldValue(fieldName);
                    } 
                    Log.customer.debug("field name " +loObjectValue);                   
                    if (loObjectValue == null)
                        loObjectValue = ""; // replace NullValue with blank.
                    lsResultValue=lsResultValue+String.valueOf(loObjectValue);                    
                }catch(Exception e)
                {
                    Log.customer.debug("Excception: "+e.getMessage());
                    lsResultValue = "EXCEPTION";
                }
                                                                
             }           
            return lsResultValue;
     }
    public String getReqValue(String objName,String fieldName,Approvable approvable)
    {
        ariba.user.core.User loUser = (ariba.user.core.User)approvable.getDottedFieldValue(objName);
        ariba.common.core.User loPartitionUser = ariba.common.core.User.getPartitionedUser(loUser,approvable.getPartition());
        Object loFieldValue = loPartitionUser.getDottedFieldValue(fieldName);
        if (loFieldValue == null)
        {
            loFieldValue = "";
        }
        Log.customer.debug("Object value in getReqValue mathod: " +loFieldValue);
        String lsResultValue=loFieldValue.toString(); 
        return lsResultValue;
    } 


}
