package config.java.ams.custom;

import java.util.List;

import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableHook;
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;
import ariba.util.log.Log;
import ariba.workforce.core.TimeSheet;

public class BuysenseTimeSheetSubmitHook implements ApprovableHook
{
    public List retVector;
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));

    public List run(Approvable approvable)
    {
        TimeSheet timeSheetCreated = (TimeSheet) approvable;
        Log.customer.debug("BuysenseTimeSheetSubmitHook.java called");
        if (timeSheetCreated == null)
        {
            return ListUtil.list(Constants.getInteger(-1), "Cannot submit timesheet without selecting a order");
        }
        else
        {
            if(timeSheetCreated.getOrder().getStatusString().equals("Received"))
            {
                return ListUtil.list(Constants.getInteger(-2), "Cannot submit timesheet against fully received Order");
            }
        }
        return NoErrorResult;
    }
}
