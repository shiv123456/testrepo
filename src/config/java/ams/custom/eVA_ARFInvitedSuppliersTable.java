package config.java.ams.custom;

/*
Purpose          : Custom controller of Invited Suppliers
Change Log	  :
*/

import ariba.base.fields.ValueSource;
import ariba.htmlui.procure.fields.*;
import ariba.util.log.Log;

public class eVA_ARFInvitedSuppliersTable extends ARFInvitedSuppliersTable
{
	public static final String ClassName = "config.java.ams.custom.eVA_ARVInvitedSuppliersTable";

    public String outputView(String group, ValueSource context)
    {
    	Log.customer.debug("inside eVA_ARFInvitedSuppliersTable.outputView()");
        return "config.java.ams.custom.eVA_ARVInvitedSuppliersTable";
    }

    public String inputView(String group, ValueSource context)
    {
        return outputView(group, context);
    }

}
