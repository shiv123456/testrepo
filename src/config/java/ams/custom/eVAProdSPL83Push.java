/**
This batch program is designed to push specific Approvables or Buysenense custom objects
to the BuysenseIntegrator.
To use this program, you need to know the class name and the UniqueName of the
objects that need to be pushed.

The csv file name is a parameter for the Scheduled task that runs this program.
It is set to .../config/java/ams/custom/txn.csv

This program is designed to run on demand.

The CSV file should conform to the following format.

approvabletype,unquename  for example ariba.core.PaymentEform,PV435

####  Note 1:  There should not be any headers for this file.
####  Note 2:  There should not be " " aroud the data.
####  Note 3:  Should follow the order specified above meaning approvable type should come first and the UniqueName.


imohideen, AMS, July 2001
*/
/* 09/11/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 09/24/2003: Updates for Ariba 8.0 (Richard Lee) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/

package config.java.ams.custom;
import ariba.util.log.Log;
// Ariba 8.0: SimpleSecheduleTask has been deprecated by Scheduled task.
//import ariba.server.objectserver.SimpleScheduledTask;
import ariba.util.scheduler.*;
// Ariba 8.0: Not using WorkflowState; also, it has been taken out of Ariba 8.0.
//import ariba.server.objectserver.core.WorkflowState;
// Ariba 8.0: Not using ApprovableOnServer in this module; also, it has been taken out in Ariba 8.0.
//import ariba.server.ormsserver.ApprovableOnServer;
import ariba.base.core.Partition;
import java.util.Map;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Base;
import ariba.purchasing.core.PurchaseOrder;
import java.util.Vector;
import ariba.base.core.*;
//import ariba.core.BuysenseOrg;
//import ariba.core.BuysenseCatalogController;
//import ariba.core.BuysenseClient;
//import ariba.server.objectserver.workflow.ActionInterface;
//import config.java.ams.custom.AMSBuysenseOrgApproved2;

// Ariba 8.0: SimpleScheduledTask has been deprectated by ScheduledTask.
//public class eVAProdSPL83Push extends SimpleScheduledTask
//81->822 changed Hashtable to Map
public class eVAProdSPL83Push extends ScheduledTask
{

    String approvable = null;
    String approvableType = null;
    String fileName = null;
    Partition part;

    public eVAProdSPL83Push()
    {
        Log.customer.debug("Line 48...");
    }

    /** We just want to persist the partition for the run method to use */
    public void init(Partition partition,
                     String scheduledTaskName, Map arguments)
    {
        Log.customer.debug("Line 54...");
        part =  partition;
        fileName = (String)arguments.get("FileName");
        Log.customer.debug("Line 57...");
    }

    /**
    */
    public void run() throws ScheduledTaskException
    {
        Log.customer.debug("CustomTxnPush.run()  was called...");
        Log.customer.debug("Transaction File : %s",fileName);
        //call the CSV reader to get the list of approvables to push
        CSVReader csvr = new CSVReader();
        Vector appVector = csvr.readCSV(fileName);
        ClusterRoot cr = null;
        int appSize = appVector.size();

        for (int j = 0; j < appSize; j++)
        {
            Vector fieldsVector = (Vector)appVector.elementAt(j);

            approvableType = (String)fieldsVector.elementAt(0);

            approvable = (String)fieldsVector.elementAt(1);
            Log.customer.debug("Trying to get: %s of type : %s",
                           approvable,approvableType);

            cr = Base.getService().objectMatchingUniqueName
                (approvableType,part,approvable);

            Log.customer.debug("Got this approvable object: %s",cr);

            if (approvableType.equals("ariba.core.PaymentEform"))
                pushPayment(cr);
            if (approvableType.equals("ariba.core.Liquidation"))
                pushLiquidation(cr);
            if (approvableType.equals("ariba.common.core.UserProfile"))
                pushUserProfile(cr);
            if (approvableType.equals("ariba.core.BuysenseOrgEform"))
                localAMSBuysenseOrgApproved(cr);

            //We sleep one minute before getting back to the next transaction
            try
            {
                Thread.sleep(60000);
            }
            catch(Exception e)
            {
                Log.customer.debug("Problem sleeping");
            }
        }
    }
    //Code here is very similar to or a copy of how we push payments elsewhere
    public void pushPayment(ClusterRoot cr)
    {
        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        ariba.user.core.User RequesterUser = (ariba.user.core.User)cr.getDottedFieldValue("Requester");
        //rgiesen dev SPL #39
        //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,cr.getPartition());
        String clientName = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName");
        String clientID = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientID");
        String XMLClientTag = clientID.trim() + "_" + clientName.trim() ;

        AMSTXN.initMappingData();
        int rt=AMSTXN.push(XMLClientTag,
                           "PV",
                           (String)cr.getFieldValue("UniqueName"),
                           "NEW",
                           "RDY",
                           "A",
                           "0",
                           "",
                           cr,
                           "PaymentItems",
                           "PaymentAccountings", cr.getPartition());
        Log.customer.debug("value of rt %s",rt);

    }

    //Code here is very similar to or a copy of how we push Liquidation elsewhere. one big difference is
    //we figure out the txntype by inspecting the Liquidation object
    public void pushLiquidation(ClusterRoot cr)
    {
        String txntype;
        ClusterRoot refTran = (ClusterRoot)cr.getDottedFieldValue("RefTranObject");

        String classType = refTran.getTypeName();
        Log.customer.debug("This is the refTransObject class type: %s",classType);
        if (classType.equals("ariba.core.PaymentEform")) txntype ="PVX";
        else txntype = "PCX";

        PurchaseOrder order = (PurchaseOrder)cr.getDottedFieldValue("Order");

        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        ariba.user.core.User poRequesterUser = (ariba.user.core.User)order.getDottedFieldValue("LineItems[0].Requisition.Requester");
        //rgiesen dev SPL #39
        //ariba.common.core.User poRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(poRequesterUser,Base.getSession().getPartition());
        ariba.common.core.User poRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(poRequesterUser,order.getPartition());
        String clientName = (String)poRequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName");
        String clientID = (String)poRequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientID");
        String XMLClientTag = clientID.trim() + "_" + clientName.trim() ;
        Log.customer.debug("XMLClientTag %s",XMLClientTag);
        AMSTXN.initMappingData();
        Log.customer.debug("XMLClientTag %s",XMLClientTag);

        int rt=AMSTXN.push(XMLClientTag,
                           txntype,
                           (String)cr.getFieldValue("UniqueName"),
                           "NEW",
                           "RDY",
                           "A",
                           "0",
                           "",
                           cr,
                           "LiquidationItems",
                           "LiquidationAccountings", cr.getPartition());

        try
        {
            Thread.sleep(60000);
        }
        catch(Exception e)
        {
            Log.customer.debug("Problem sleeping");
        }

    }

    /* pushUserProfile
       cr is the UserProfile object being pass from the csv file.
       user is the object we are changing and pushing based on the fields in UserProfile.Details.
    */

    public void pushUserProfile(ClusterRoot cr)
    {

        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        ariba.user.core.User RequesterUser = (ariba.user.core.User)cr.getDottedFieldValue("Requester");
        //rgiesen dev SPL #39
        Partition crPartition = cr.getPartition();
        //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,crPartition);
        String clientID = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientID");
        // clientID like EVA001
        //rgiesen dev SPL #39
        //String BSOrgName = (String)Base.getService().getParameter(Base.getSession().getPartition(),
        String BSOrgName = (String)Base.getService().getParameter(crPartition,
                                                                  "Application.AMSParameters.BSORGClient");
        Log.customer.debug("BSOrgName in UserProfile: " + BSOrgName);
        String XMLClientTag = clientID.trim() + "_" + BSOrgName;
        Log.customer.debug("XMLClientTag: " + XMLClientTag);
        ClusterRoot user = (ClusterRoot)cr.getFieldValue("User");
        String strUN = (String) user.getDottedFieldValue("UniqueName");
        Log.customer.debug("The User is: %s", strUN);

        // Updating the User object from the UserProfile.Details

        Log.customer.debug(strUN + " OLD DefaultingSetBits: " + user.getDottedFieldValue("DefaultingSetBits"));
        user.setFieldValue("DefaultingSetBits",
                           cr.getDottedFieldValue("Details.DefaultingSetBits"));
        Log.customer.debug(strUN + " NEW DefaultingSetBits: " + user.getDottedFieldValue("DefaultingSetBits"));
        /*  ClassProperties is not being used by eVA
                        Log.customer.debug(strUN + " OLD ClassProperties: " + (boolean)user.getDottedFieldValue("ClassProperties"));
                        Log.customer.debug(strUN + " NEW ClassProperties: " + (boolean) cr.getDottedFieldValue("Details.ClassProperties"));
                user.setFieldValue("ClassProperties",(boolean)cr.getDottedFieldValue("Details.ClassProperties"));
        */
        Log.customer.debug(strUN + " OLD Name: " + user.getDottedFieldValue("Name"));
        user.setFieldValue("Name",cr.getDottedFieldValue("Details.Name"));
        Log.customer.debug(strUN + " NEW Name: " + user.getDottedFieldValue("Name"));

        Log.customer.debug(strUN + " OLD EmailAddress: " + user.getDottedFieldValue("EmailAddress"));
        user.setFieldValue("EmailAddress",
                           cr.getDottedFieldValue("Details.EmailAddress"));
        Log.customer.debug(strUN + " NEW EmailAddress: " + user.getDottedFieldValue("EmailAddress"));

        Log.customer.debug(strUN + " OLD EmployeeNumber: " + user.getDottedFieldValue("EmployeeNumber"));
        user.setFieldValue("EmployeeNumber",
                           cr.getDottedFieldValue("Details.EmployeeNumber"));
        Log.customer.debug(strUN + " NEW EmployeeNumber: " + user.getDottedFieldValue("EmployeeNumber"));

        Log.customer.debug(strUN + " OLD Accounting: " + user.getDottedFieldValue("Accounting"));
        user.setFieldValue("Accounting",
                           cr.getDottedFieldValue("Details.Accounting"));
        Log.customer.debug(strUN + " NEW Accounting: " + user.getDottedFieldValue("Accounting"));

        Log.customer.debug(strUN + " OLD BillingAddress: " + user.getDottedFieldValue("BillingAddress"));
        user.setFieldValue("BillingAddress",
                           cr.getDottedFieldValue("Details.BillingAddress"));
        Log.customer.debug(strUN + " NEW BillingAddress: " + user.getDottedFieldValue("BillingAddress"));

        Log.customer.debug(strUN + " OLD Roles: " + user.getDottedFieldValue("Roles"));
        Log.customer.debug(strUN + " NEW Roles: " + (BaseVector)cr.getDottedFieldValue("Details.Roles"));
        user.setFieldValue("Roles",
                           (BaseVector)cr.getDottedFieldValue("Details.Roles"));
        Log.customer.debug(strUN + " Updated Roles: " + user.getDottedFieldValue("Roles"));

        Log.customer.debug(strUN + " OLD ChargeDelegation: " + user.getDottedFieldValue("ChargeDelegation"));
        Log.customer.debug(strUN + " NEW ChargeDelegation: " + (BaseVector)cr.getDottedFieldValue("Details.ChargeDelegation"));
        user.setFieldValue("ChargeDelegation",
                           (BaseVector)cr.getDottedFieldValue("Details.ChargeDelegation"));
        Log.customer.debug(strUN + " Updated ChargeDelegation: " + user.getDottedFieldValue("ChargeDelegation"));

        Log.customer.debug(strUN + " OLD Permissions: " + user.getDottedFieldValue("Permissions"));
        Log.customer.debug(strUN + " NEW Permissions: " + (BaseVector)cr.getDottedFieldValue("Details.Permissions"));
        user.setFieldValue("Permissions",
                           (BaseVector)cr.getDottedFieldValue("Details.Permissions"));
        Log.customer.debug(strUN + " Updated Permissions: " + user.getDottedFieldValue("Permissions"));

        Log.customer.debug(strUN + " OLD Delegation: " + user.getDottedFieldValue("Delegation"));
        user.setFieldValue("Delegation",
                           cr.getDottedFieldValue("Details.Delegation"));
        Log.customer.debug(strUN + " NEW Delegation: " + user.getDottedFieldValue("Delegation"));

        Log.customer.debug(strUN + " OLD Supervisor: " + user.getDottedFieldValue("Supervisor"));
        user.setFieldValue("Supervisor",
                           cr.getDottedFieldValue("Details.Supervisor"));
        Log.customer.debug(strUN + " NEW Supervisor: " + user.getDottedFieldValue("Supervisor"));

        Log.customer.debug(strUN + " OLD ShipTo: " + user.getDottedFieldValue("ShipTo"));
        user.setFieldValue("ShipTo",cr.getDottedFieldValue("Details.ShipTo"));
        Log.customer.debug(strUN + " NEW ShipTo: " + user.getDottedFieldValue("ShipTo"));

        Log.customer.debug(strUN + " OLD DeliverTo: " + user.getDottedFieldValue("DeliverTo"));
        user.setFieldValue("DeliverTo",
                           cr.getDottedFieldValue("Details.DeliverTo"));
        Log.customer.debug(strUN + " NEW DeliverTo: " + user.getDottedFieldValue("DeliverTo"));

        Log.customer.debug(strUN + " OLD BuysenseOrg: " + user.getDottedFieldValue("BuysenseOrg"));
        user.setFieldValue("BuysenseOrg",
                           cr.getDottedFieldValue("Details.BuysenseOrg"));
        Log.customer.debug(strUN + " NEW BuysenseOrg: " + user.getDottedFieldValue("BuysenseOrg"));

        Log.customer.debug(strUN + " OLD ClientName: " + user.getDottedFieldValue("ClientName"));
        user.setFieldValue("ClientName",
                           cr.getDottedFieldValue("Details.ClientName"));
        Log.customer.debug(strUN + " NEW ClientName: " + user.getDottedFieldValue("ClientName"));

        Log.customer.debug(strUN + " OLD BuysenseCatalogController: " + user.getDottedFieldValue("BuysenseCatalogController"));
        user.setFieldValue("BuysenseCatalogController",
                           cr.getDottedFieldValue("Details.BuysenseCatalogController"));
        Log.customer.debug(strUN + " NEW BuysenseCatalogController: " + user.getDottedFieldValue("BuysenseCatalogController"));

        Log.customer.debug(strUN + " OLD Phone: " + user.getDottedFieldValue("Phone"));
        user.setFieldValue("Phone",cr.getDottedFieldValue("Details.Phone"));
        Log.customer.debug(strUN + " NEW Phone: " + user.getDottedFieldValue("Phone"));

        Log.customer.debug(strUN + " OLD BuysenseEmployeeNumber: " + user.getDottedFieldValue("BuysenseEmployeeNumber"));
        user.setFieldValue("BuysenseEmployeeNumber",
                           cr.getDottedFieldValue("Details.BuysenseEmployeeNumber"));
        Log.customer.debug(strUN + " NEW BuysenseEmployeeNumber: " + user.getDottedFieldValue("BuysenseEmployeeNumber"));

        Log.customer.debug(strUN + " OLD Expenditure_Limit: " + user.getDottedFieldValue("Expenditure_Limit"));
        user.setFieldValue("Expenditure_Limit",
                           cr.getDottedFieldValue("Details.Expenditure_Limit"));
        Log.customer.debug(strUN + " NEW Expenditure_Limit: " + user.getDottedFieldValue("Expenditure_Limit"));

        Log.customer.debug(strUN + " OLD Delegated_Purchasing_Authority: " + user.getDottedFieldValue("Delegated_Purchasing_Authority"));
        user.setFieldValue("Delegated_Purchasing_Authority",
                           cr.getDottedFieldValue("Details.Delegated_Purchasing_Authority"));
        Log.customer.debug(strUN + " NEW Delegated_Purchasing_Authority: " + user.getDottedFieldValue("Delegated_Purchasing_Authority"));

        Log.customer.debug(strUN + " OLD Expenditure_Limit_Exceeded_Approver: " + user.getDottedFieldValue("Expenditure_Limit_Exceeded_Approver"));
        user.setFieldValue("Expenditure_Limit_Exceeded_Approver",
                           cr.getDottedFieldValue("Details.Expenditure_Limit_Exceeded_Approver"));
        Log.customer.debug(strUN + " NEW Expenditure_Limit_Exceeded_Approver: " + user.getDottedFieldValue("Expenditure_Limit_Exceeded_Approver"));

        // Push the new User object
        AMSTXN.initMappingData();

        int rt=AMSTXN.push(XMLClientTag,
                           "UP",
                           (String)cr.getFieldValue("UniqueName"),
                           "NEW",
                           "RDY",
                           "A",
                           "0",
                           "", user, "Roles","Permissions", cr.getPartition());
        Log.customer.debug("value of rt %s",rt);

    }


    public void localAMSBuysenseOrgApproved(ClusterRoot cr)
    {
        ClusterRoot BOrg;

        Log.customer.debug("AMSBuysenseOrgApproved.execute called...");
        //Log.customer.debug("workflowState: %s", workflowState.toString());
        Log.customer.debug("cr: %s", cr.toString());

        Boolean B=(Boolean)cr.getFieldValue("NewBuysenseOrg");
        boolean NBOrg=B.booleanValue();
        Log.customer.debug("NBOrg: %s",new Boolean(NBOrg));

        if (NBOrg==false)
        {
            Log.customer.debug("Modify the existing BuysenseOrg");

            String UniqueName = (String)cr.getDottedFieldValue("KeyName.UniqueName");
            Log.customer.debug("The unique name for this req is: %s", UniqueName);

            BOrg = Base.getService().objectMatchingUniqueName("ariba.core.BuysenseOrg", cr.getPartition(), UniqueName);
            Log.customer.debug("ClusterRoot : %s", BOrg.toString());

            String b1=(String)BOrg.getDottedFieldValue("ClientName.Name");
            String b2=b1.toString();
            Log.customer.debug("Value of ClientName is: %s", b2);

            BOrg.setFieldValue("ClientName", cr.getFieldValue("ClientName"));

            BOrg.setFieldValue("ClientID",
                               cr.getDottedFieldValue("ClientName.UniqueName"));

            String b3=(String)BOrg.getDottedFieldValue("ClientName.Name");
            String b4=b3.toString();
            Log.customer.debug("Value of ClientName is: %s",b4);

        }
        else
        {
            Log.customer.debug("In the 2nd loop");
            Log.customer.debug("Create a new BuysenseOrg Object");

            BOrg=(ClusterRoot)ClusterRoot.create("ariba.core.BuysenseOrg",
                                                 cr.getPartition());
            Log.customer.debug("cr: "+cr.toString());
            Log.customer.debug("ClusterRoot : "+BOrg.toString());

            BOrg.setFieldValue("UniqueName", cr.getFieldValue("KeyID"));
            BOrg.setFieldValue("Name", cr.getFieldValue("KeyID"));
            BOrg.setFieldValue("ClientName", cr.getFieldValue("ClientName"));
            BOrg.setFieldValue("ClientID",
                               cr.getDottedFieldValue("ClientName.UniqueName"));

            BOrg.save();
        }

        // Initialize the BuysenseOrg Object with values from the BuysenseOrgEform Object
        {
            BOrg.setFieldValue("PreEncumbranceLimit",
                               cr.getFieldValue("PreEncumbranceLimit"));
            BOrg.setFieldValue("EncumbranceLimit",
                               cr.getFieldValue("EncumbranceLimit"));

            // Initialize the 25 Chooser Accounting fields Data values
            BOrg.setFieldValue("FieldDefault1",
                               cr.getFieldValue("FieldDefault1"));
            BOrg.setFieldValue("FieldDefault2",
                               cr.getFieldValue("FieldDefault2"));
            BOrg.setFieldValue("FieldDefault3",
                               cr.getFieldValue("FieldDefault3"));
            BOrg.setFieldValue("FieldDefault4",
                               cr.getFieldValue("FieldDefault4"));
            BOrg.setFieldValue("FieldDefault5",
                               cr.getFieldValue("FieldDefault5"));
            BOrg.setFieldValue("FieldDefault6",
                               cr.getFieldValue("FieldDefault6"));
            BOrg.setFieldValue("FieldDefault7",
                               cr.getFieldValue("FieldDefault7"));
            BOrg.setFieldValue("FieldDefault8",
                               cr.getFieldValue("FieldDefault8"));
            BOrg.setFieldValue("FieldDefault9",
                               cr.getFieldValue("FieldDefault9"));
            BOrg.setFieldValue("FieldDefault10",
                               cr.getFieldValue("FieldDefault10"));
            BOrg.setFieldValue("FieldDefault11",
                               cr.getFieldValue("FieldDefault11"));
            BOrg.setFieldValue("FieldDefault12",
                               cr.getFieldValue("FieldDefault12"));
            BOrg.setFieldValue("FieldDefault13",
                               cr.getFieldValue("FieldDefault13"));
            BOrg.setFieldValue("FieldDefault14",
                               cr.getFieldValue("FieldDefault14"));
            BOrg.setFieldValue("FieldDefault15",
                               cr.getFieldValue("FieldDefault15"));
            BOrg.setFieldValue("FieldDefault16",
                               cr.getFieldValue("FieldDefault16"));
            BOrg.setFieldValue("FieldDefault17",
                               cr.getFieldValue("FieldDefault17"));
            BOrg.setFieldValue("FieldDefault18",
                               cr.getFieldValue("FieldDefault18"));
            BOrg.setFieldValue("FieldDefault19",
                               cr.getFieldValue("FieldDefault19"));
            BOrg.setFieldValue("FieldDefault20",
                               cr.getFieldValue("FieldDefault20"));
            BOrg.setFieldValue("FieldDefault21",
                               cr.getFieldValue("FieldDefault21"));
            BOrg.setFieldValue("FieldDefault22",
                               cr.getFieldValue("FieldDefault22"));
            BOrg.setFieldValue("FieldDefault23",
                               cr.getFieldValue("FieldDefault23"));
            BOrg.setFieldValue("FieldDefault24",
                               cr.getFieldValue("FieldDefault24"));
            BOrg.setFieldValue("FieldDefault25",
                               cr.getFieldValue("FieldDefault25"));

            // Initialize the 10 Text Accounting fields Data
            BOrg.setFieldValue("LineAcctText1Value",
                               cr.getFieldValue("LineAcctText1Value"));
            BOrg.setFieldValue("LineAcctText2Value",
                               cr.getFieldValue("LineAcctText2Value"));
            BOrg.setFieldValue("LineAcctText3Value",
                               cr.getFieldValue("LineAcctText3Value"));
            BOrg.setFieldValue("LineAcctText4Value",
                               cr.getFieldValue("LineAcctText4Value"));
            BOrg.setFieldValue("LineAcctText5Value",
                               cr.getFieldValue("LineAcctText5Value"));
            BOrg.setFieldValue("LineAcctText6Value",
                               cr.getFieldValue("LineAcctText6Value"));
            BOrg.setFieldValue("LineAcctText7Value",
                               cr.getFieldValue("LineAcctText7Value"));
            BOrg.setFieldValue("LineAcctText8Value",
                               cr.getFieldValue("LineAcctText8Value"));
            BOrg.setFieldValue("LineAcctText9Value",
                               cr.getFieldValue("LineAcctText9Value"));
            BOrg.setFieldValue("LineAcctText10Value",
                               cr.getFieldValue("LineAcctText10Value"));

            // Initialize the 5 CB Accounting fields Data
            if (cr.getFieldValue("LineAcctCB1Value") != null)
            {
                BOrg.setFieldValue("LineAcctCB1Value",
                                   cr.getFieldValue("LineAcctCB1Value"));
            }
            else
            {
                BOrg.setFieldValue("LineAcctCB1Value",new Boolean("false"));
            }
            if (cr.getFieldValue("LineAcctCB2Value") != null)
            {
                BOrg.setFieldValue("LineAcctCB2Value",
                                   cr.getFieldValue("LineAcctCB2Value"));
            }
            else
            {
                BOrg.setFieldValue("LineAcctCB2Value",new Boolean("false"));
            }
            if (cr.getFieldValue("LineAcctCB3Value") != null)
            {
                BOrg.setFieldValue("LineAcctCB3Value",
                                   cr.getFieldValue("LineAcctCB3Value"));
            }
            else
            {
                BOrg.setFieldValue("LineAcctCB3Value",new Boolean("false"));
            }
            if (cr.getFieldValue("LineAcctCB4Value") != null)
            {
                BOrg.setFieldValue("LineAcctCB4Value",
                                   cr.getFieldValue("LineAcctCB4Value"));
            }
            else
            {
                BOrg.setFieldValue("LineAcctCB4Value",new Boolean("false"));
            }
            if (cr.getFieldValue("LineAcctCB5Value") != null)
            {
                BOrg.setFieldValue("LineAcctCB5Value",
                                   cr.getFieldValue("LineAcctCB5Value"));
            }
            else
            {
                BOrg.setFieldValue("LineAcctCB5Value",new Boolean("false"));
            }

            // Initialize the 4 Chooser Req Header fields Data
            BOrg.setFieldValue("ReqHeadFieldDefault1",
                               cr.getFieldValue("ReqHeadFieldDefault1"));
            BOrg.setFieldValue("ReqHeadFieldDefault2",
                               cr.getFieldValue("ReqHeadFieldDefault2"));
            BOrg.setFieldValue("ReqHeadFieldDefault3",
                               cr.getFieldValue("ReqHeadFieldDefault3"));
            BOrg.setFieldValue("ReqHeadFieldDefault4",
                               cr.getFieldValue("ReqHeadFieldDefault4"));
            BOrg.setFieldValue("ReqHeadFieldDefault5",
                               cr.getFieldValue("ReqHeadFieldDefault5"));
            BOrg.setFieldValue("ReqHeadFieldDefault6",
                               cr.getFieldValue("ReqHeadFieldDefault6"));

            // Initialize the 4 Text Req Header fields Data
            BOrg.setFieldValue("ReqHeadText1Value",
                               cr.getFieldValue("ReqHeadText1Value"));
            BOrg.setFieldValue("ReqHeadText2Value",
                               cr.getFieldValue("ReqHeadText2Value"));
            BOrg.setFieldValue("ReqHeadText3Value",
                               cr.getFieldValue("ReqHeadText3Value"));
            BOrg.setFieldValue("ReqHeadText4Value",
                               cr.getFieldValue("ReqHeadText4Value"));

            // Initialize the 3 CB Req Header fields Data
            if (cr.getFieldValue("ReqHeadCB1Value") != null)
            {
                BOrg.setFieldValue("ReqHeadCB1Value",
                                   cr.getFieldValue("ReqHeadCB1Value"));
            }
            else
            {
                BOrg.setFieldValue("ReqHeadCB1Value",new Boolean("false"));
            }
            if (cr.getFieldValue("ReqHeadCB2Value") != null)
            {
                BOrg.setFieldValue("ReqHeadCB2Value",
                                   cr.getFieldValue("ReqHeadCB2Value"));
            }
            else
            {
                BOrg.setFieldValue("ReqHeadCB2Value",new Boolean("false"));
            }

            if (cr.getFieldValue("ReqHeadCB3Value") != null)
            {
                BOrg.setFieldValue("ReqHeadCB3Value",
                                   cr.getFieldValue("ReqHeadCB3Value"));
            }
            else
            {
                BOrg.setFieldValue("ReqHeadCB3Value",new Boolean("false"));
            }
            // CSPLU-5 add 2 new ReqHeadCB fields
            if (cr.getFieldValue("ReqHeadCB4Value") != null)
            {
                BOrg.setFieldValue("ReqHeadCB4Value",
                                   cr.getFieldValue("ReqHeadCB4Value"));
            }
            else
            {
                BOrg.setFieldValue("ReqHeadCB4Value",new Boolean("false"));
            }

            if (cr.getFieldValue("ReqHeadCB5Value") != null)
            {
                BOrg.setFieldValue("ReqHeadCB5Value",
                                   cr.getFieldValue("ReqHeadCB5Value"));
            }
            else
            {
                BOrg.setFieldValue("ReqHeadCB5Value",new Boolean("false"));
            }

            // Initialize the 4 Chooser Req Line fields Data
            BOrg.setFieldValue("ReqLineFieldDefault1",
                               cr.getFieldValue("ReqLineFieldDefault1"));
            BOrg.setFieldValue("ReqLineFieldDefault2",
                               cr.getFieldValue("ReqLineFieldDefault2"));
            BOrg.setFieldValue("ReqLineFieldDefault3",
                               cr.getFieldValue("ReqLineFieldDefault3"));
            BOrg.setFieldValue("ReqLineFieldDefault4",
                               cr.getFieldValue("ReqLineFieldDefault4"));

            // Initialize the 4 Text Req Line fields Data
            BOrg.setFieldValue("ReqLineText1Value",
                               cr.getFieldValue("ReqLineText1Value"));
            BOrg.setFieldValue("ReqLineText2Value",
                               cr.getFieldValue("ReqLineText2Value"));
            BOrg.setFieldValue("ReqLineText3Value",
                               cr.getFieldValue("ReqLineText3Value"));
            BOrg.setFieldValue("ReqLineText4Value",
                               cr.getFieldValue("ReqLineText4Value"));

            // Initialize the 2 CB Req Line fields Data
            if (cr.getFieldValue("ReqLineCB1Value") != null)
            {
                BOrg.setFieldValue("ReqLineCB1Value",
                                   cr.getFieldValue("ReqLineCB1Value"));
            }
            else
            {
                BOrg.setFieldValue("ReqLineCB1Value",new Boolean("false"));
            }
            if (cr.getFieldValue("ReqLineCB2Value") != null)
            {
                BOrg.setFieldValue("ReqLineCB2Value",
                                   cr.getFieldValue("ReqLineCB2Value"));
            }
            else
            {
                BOrg.setFieldValue("ReqLineCB2Value",new Boolean("false"));
            }

            // Initialize the 4 Chooser Payment Header fields Data
            BOrg.setFieldValue("PaymentHeadFieldDefault1",
                               cr.getFieldValue("PaymentHeadFieldDefault1"));
            BOrg.setFieldValue("PaymentHeadFieldDefault2",
                               cr.getFieldValue("PaymentHeadFieldDefault2"));
            BOrg.setFieldValue("PaymentHeadFieldDefault3",
                               cr.getFieldValue("PaymentHeadFieldDefault3"));
            BOrg.setFieldValue("PaymentHeadFieldDefault4",
                               cr.getFieldValue("PaymentHeadFieldDefault4"));

            // Initialize the 4 Text Payment Header fields Data
            BOrg.setFieldValue("PaymentHeadText1Value",
                               cr.getFieldValue("PaymentHeadText1Value"));
            BOrg.setFieldValue("PaymentHeadText2Value",
                               cr.getFieldValue("PaymentHeadText2Value"));
            BOrg.setFieldValue("PaymentHeadText3Value",
                               cr.getFieldValue("PaymentHeadText3Value"));
            BOrg.setFieldValue("PaymentHeadText4Value",
                               cr.getFieldValue("PaymentHeadText4Value"));
            BOrg.setFieldValue("PaymentHeadText5Value",
                               cr.getFieldValue("PaymentHeadText5Value"));
            BOrg.setFieldValue("PaymentHeadText6Value",
                               cr.getFieldValue("PaymentHeadText6Value"));

            // Initialize the 2 CB Payment Header fields Data
            if (cr.getFieldValue("PaymentHeadCB1Value") != null)
            {
                BOrg.setFieldValue("PaymentHeadCB1Value",
                                   cr.getFieldValue("PaymentHeadCB1Value"));
            }
            else
            {
                BOrg.setFieldValue("PaymentHeadCB1Value",new Boolean("false"));
            }
            if (cr.getFieldValue("PaymentHeadCB2Value") != null)
            {
                BOrg.setFieldValue("PaymentHeadCB2Value",
                                   cr.getFieldValue("PaymentHeadCB2Value"));
            }
            else
            {
                BOrg.setFieldValue("PaymentHeadCB2Value",new Boolean("false"));
            }

            // Initialize the 4 Chooser Payment Line fields Data
            BOrg.setFieldValue("PaymentLineFieldDefault1",
                               cr.getFieldValue("PaymentLineFieldDefault1"));
            BOrg.setFieldValue("PaymentLineFieldDefault2",
                               cr.getFieldValue("PaymentLineFieldDefault2"));
            BOrg.setFieldValue("PaymentLineFieldDefault3",
                               cr.getFieldValue("PaymentLineFieldDefault3"));
            BOrg.setFieldValue("PaymentLineFieldDefault4",
                               cr.getFieldValue("PaymentLineFieldDefault4"));

            // Initialize the 4 Text Payment Line fields Data
            BOrg.setFieldValue("PaymentLineText1Value",
                               cr.getFieldValue("PaymentLineText1Value"));
            BOrg.setFieldValue("PaymentLineText2Value",
                               cr.getFieldValue("PaymentLineText2Value"));
            BOrg.setFieldValue("PaymentLineText3Value",
                               cr.getFieldValue("PaymentLineText3Value"));
            BOrg.setFieldValue("PaymentLineText4Value",
                               cr.getFieldValue("PaymentLineText4Value"));

            // Initialize the 2 CB Payment Line fields Data
            if (cr.getFieldValue("PaymentLineCB1Value") != null)
            {
                BOrg.setFieldValue("PaymentLineCB1Value",
                                   cr.getFieldValue("PaymentLineCB1Value"));
            }
            else
            {
                BOrg.setFieldValue("PaymentLineCB1Value",new Boolean("false"));
            }
            if (cr.getFieldValue("PaymentLineCB2Value") != null)
            {
                BOrg.setFieldValue("PaymentLineCB2Value",
                                   cr.getFieldValue("PaymentLineCB2Value"));
            }
            else
            {
                BOrg.setFieldValue("PaymentLineCB2Value",new Boolean("false"));
            }

            // Initialize the various approver and watcher fields

            if (cr.getFieldValue("SupervisorFirstApprover") != null)
            {
                BOrg.setFieldValue("SupervisorFirstApprover",
                                   cr.getFieldValue("SupervisorFirstApprover"));
            }
            else
            {
                BOrg.setFieldValue("SupervisorFirstApprover",
                                   new Boolean("false"));
            }

            // General financial approvers
            BOrg.setFieldValue("PaymentInitiator",
                               cr.getFieldValue("PaymentInitiator"));
            BOrg.setFieldValue("Approver",cr.getFieldValue("Approver"));
            BOrg.setFieldValue("ApproverAmount",
                               cr.getFieldValue("ApproverAmt"));
            BOrg.setFieldValue("ProcurementOfficer",
                               cr.getFieldValue("ProcurementOfficer"));
            BOrg.setFieldValue("ProcurementOfficerAmount",
                               cr.getFieldValue("ProcurementOfficerAmt"));
            BOrg.setFieldValue("FinancialOfficer",
                               cr.getFieldValue("FinOfficer"));
            BOrg.setFieldValue("FinancialOfficerAmount",
                               cr.getFieldValue("FinOfficerAmt"));

            // Receipt approvers
            BOrg.setFieldValue("ReceiptApprover1",
                               cr.getFieldValue("ReceiptApprover1"));
            BOrg.setFieldValue("ReceiptApprover2",
                               cr.getFieldValue("ReceiptApprover2"));
            BOrg.setFieldValue("ReceiptApprover3",
                               cr.getFieldValue("ReceiptApprover3"));

            // Payment approvers
            BOrg.setFieldValue("PaymentInitiator",
                               cr.getFieldValue("PaymentInitiator"));
            BOrg.setFieldValue("PaymentApprover",
                               cr.getFieldValue("PaymentApprover"));

            // Non-Catalog Approver, Non-enabled Approver & Out-of-Tolerance Approver
            BOrg.setFieldValue("NonCatalogApprover",
                               cr.getFieldValue("NonCatalogApprover"));
            BOrg.setFieldValue("NonCatalogProcessor",
                               cr.getFieldValue("NonCatalogProcessor"));
            BOrg.setFieldValue("OOTPaymentApprover",
                               cr.getFieldValue("OOTPaymentApprover"));
            BOrg.setFieldValue("OOTAmount",cr.getFieldValue("OOTAmount"));
            BOrg.setFieldValue("OOTPercent",cr.getFieldValue("OOTPercent"));

            // Commodity Code & Category Approvers
            BOrg.setFieldValue("CommodityCodeCat1",
                               cr.getFieldValue("CommCodeCat1"));
            BOrg.setFieldValue("CommodityCodeCatApprover1",
                               cr.getFieldValue("CommCodeCatAppr1"));
            BOrg.setFieldValue("CommodityCodeCat2",
                               cr.getFieldValue("CommCodeCat2"));
            BOrg.setFieldValue("CommodityCodeCatApprover2",
                               cr.getFieldValue("CommCodeCatAppr2"));
            BOrg.setFieldValue("CommodityCodeCat3",
                               cr.getFieldValue("CommCodeCat3"));
            BOrg.setFieldValue("CommodityCodeCatApprover3",
                               cr.getFieldValue("CommCodeCatAppr3"));
            BOrg.setFieldValue("CommodityCodeCat4",
                               cr.getFieldValue("CommCodeCat4"));
            BOrg.setFieldValue("CommodityCodeCatApprover4",
                               cr.getFieldValue("CommCodeCatAppr4"));
            BOrg.setFieldValue("CommodityCodeCat5",
                               cr.getFieldValue("CommCodeCat5"));
            BOrg.setFieldValue("CommodityCodeCatApprover5",
                               cr.getFieldValue("CommCodeCatAppr5"));
            BOrg.setFieldValue("CommodityCodeCat6",
                               cr.getFieldValue("CommCodeCat6"));
            BOrg.setFieldValue("CommodityCodeCatApprover6",
                               cr.getFieldValue("CommCodeCatAppr6"));
            BOrg.setFieldValue("CommodityCodeCat7",
                               cr.getFieldValue("CommCodeCat7"));
            BOrg.setFieldValue("CommodityCodeCatApprover7",
                               cr.getFieldValue("CommCodeCatAppr7"));
            BOrg.setFieldValue("CommodityCodeCat8",
                               cr.getFieldValue("CommCodeCat8"));
            BOrg.setFieldValue("CommodityCodeCatApprover8",
                               cr.getFieldValue("CommCodeCatAppr8"));
            BOrg.setFieldValue("CommodityCodeCat9",
                               cr.getFieldValue("CommCodeCat9"));
            BOrg.setFieldValue("CommodityCodeCatApprover9",
                               cr.getFieldValue("CommCodeCatAppr9"));
            BOrg.setFieldValue("CommodityCodeCat10",
                               cr.getFieldValue("CommCodeCat10"));
            BOrg.setFieldValue("CommodityCodeCatApprover10",
                               cr.getFieldValue("CommCodeCatAppr10"));
            BOrg.setFieldValue("CommodityCodeCat11",
                               cr.getFieldValue("CommCodeCat11"));
            BOrg.setFieldValue("CommodityCodeCatApprover11",
                               cr.getFieldValue("CommCodeCatAppr11"));
            BOrg.setFieldValue("CommodityCodeCat12",
                               cr.getFieldValue("CommCodeCat12"));
            BOrg.setFieldValue("CommodityCodeCatApprover12",
                               cr.getFieldValue("CommCodeCatAppr12"));
            BOrg.setFieldValue("CommodityCodeCat13",
                               cr.getFieldValue("CommCodeCat13"));
            BOrg.setFieldValue("CommodityCodeCatApprover13",
                               cr.getFieldValue("CommCodeCatAppr13"));
            BOrg.setFieldValue("CommodityCodeCat14",
                               cr.getFieldValue("CommCodeCat14"));
            BOrg.setFieldValue("CommodityCodeCatApprover14",
                               cr.getFieldValue("CommCodeCatAppr14"));
            BOrg.setFieldValue("CommodityCodeCat15",
                               cr.getFieldValue("CommCodeCat15"));
            BOrg.setFieldValue("CommodityCodeCatApprover15",
                               cr.getFieldValue("CommCodeCatAppr15"));
            BOrg.setFieldValue("CommodityCodeCat16",
                               cr.getFieldValue("CommCodeCat16"));
            BOrg.setFieldValue("CommodityCodeCatApprover16",
                               cr.getFieldValue("CommCodeCatAppr16"));
            BOrg.setFieldValue("CommodityCodeCat17",
                               cr.getFieldValue("CommCodeCat17"));
            BOrg.setFieldValue("CommodityCodeCatApprover17",
                               cr.getFieldValue("CommCodeCatAppr17"));
            BOrg.setFieldValue("CommodityCodeCat18",
                               cr.getFieldValue("CommCodeCat18"));
            BOrg.setFieldValue("CommodityCodeCatApprover18",
                               cr.getFieldValue("CommCodeCatAppr18"));
            BOrg.setFieldValue("CommodityCodeCat19",
                               cr.getFieldValue("CommCodeCat19"));
            BOrg.setFieldValue("CommodityCodeCatApprover19",
                               cr.getFieldValue("CommCodeCatAppr19"));
            BOrg.setFieldValue("CommodityCodeCat20",
                               cr.getFieldValue("CommCodeCat20"));
            BOrg.setFieldValue("CommodityCodeCatApprover20",
                               cr.getFieldValue("CommCodeCatAppr20"));
            BOrg.setFieldValue("CommodityCodeCat21",
                               cr.getFieldValue("CommCodeCat21"));
            BOrg.setFieldValue("CommodityCodeCatApprover21",
                               cr.getFieldValue("CommCodeCatAppr21"));
            BOrg.setFieldValue("CommodityCodeCat22",
                               cr.getFieldValue("CommCodeCat22"));
            BOrg.setFieldValue("CommodityCodeCatApprover22",
                               cr.getFieldValue("CommCodeCatAppr22"));
            BOrg.setFieldValue("CommodityCodeCat23",
                               cr.getFieldValue("CommCodeCat23"));
            BOrg.setFieldValue("CommodityCodeCatApprover23",
                               cr.getFieldValue("CommCodeCatAppr23"));
            BOrg.setFieldValue("CommodityCodeCat24",
                               cr.getFieldValue("CommCodeCat24"));
            BOrg.setFieldValue("CommodityCodeCatApprover24",
                               cr.getFieldValue("CommCodeCatAppr24"));
            BOrg.setFieldValue("CommodityCodeCat25",
                               cr.getFieldValue("CommCodeCat25"));
            BOrg.setFieldValue("CommodityCodeCatApprover25",
                               cr.getFieldValue("CommCodeCatAppr25"));
            BOrg.setFieldValue("CommodityCodeCat26",
                               cr.getFieldValue("CommCodeCat26"));
            BOrg.setFieldValue("CommodityCodeCatApprover26",
                               cr.getFieldValue("CommCodeCatAppr26"));
            BOrg.setFieldValue("CommodityCodeCat27",
                               cr.getFieldValue("CommCodeCat27"));
            BOrg.setFieldValue("CommodityCodeCatApprover27",
                               cr.getFieldValue("CommCodeCatAppr27"));
            BOrg.setFieldValue("CommodityCodeCat28",
                               cr.getFieldValue("CommCodeCat28"));
            BOrg.setFieldValue("CommodityCodeCatApprover28",
                               cr.getFieldValue("CommCodeCatAppr28"));
            BOrg.setFieldValue("CommodityCodeCat29",
                               cr.getFieldValue("CommCodeCat29"));
            BOrg.setFieldValue("CommodityCodeCatApprover29",
                               cr.getFieldValue("CommCodeCatAppr29"));
            BOrg.setFieldValue("CommodityCodeCat30",
                               cr.getFieldValue("CommCodeCat30"));
            BOrg.setFieldValue("CommodityCodeCatApprover30",
                               cr.getFieldValue("CommCodeCatAppr30"));
            BOrg.setFieldValue("CommodityCodeCat31",
                               cr.getFieldValue("CommCodeCat31"));
            BOrg.setFieldValue("CommodityCodeCatApprover31",
                               cr.getFieldValue("CommCodeCatAppr31"));
            BOrg.setFieldValue("CommodityCodeCat32",
                               cr.getFieldValue("CommCodeCat32"));
            BOrg.setFieldValue("CommodityCodeCatApprover32",
                               cr.getFieldValue("CommCodeCatAppr32"));
            BOrg.setFieldValue("CommodityCodeCat33",
                               cr.getFieldValue("CommCodeCat33"));
            BOrg.setFieldValue("CommodityCodeCatApprover33",
                               cr.getFieldValue("CommCodeCatAppr33"));
            BOrg.setFieldValue("CommodityCodeCat34",
                               cr.getFieldValue("CommCodeCat34"));
            BOrg.setFieldValue("CommodityCodeCatApprover34",
                               cr.getFieldValue("CommCodeCatAppr34"));
            BOrg.setFieldValue("CommodityCodeCat35",
                               cr.getFieldValue("CommCodeCat35"));
            BOrg.setFieldValue("CommodityCodeCatApprover35",
                               cr.getFieldValue("CommCodeCatAppr35"));
            BOrg.setFieldValue("CommodityCodeCat36",
                               cr.getFieldValue("CommCodeCat36"));
            BOrg.setFieldValue("CommodityCodeCatApprover36",
                               cr.getFieldValue("CommCodeCatAppr36"));
            BOrg.setFieldValue("CommodityCodeCat37",
                               cr.getFieldValue("CommCodeCat37"));
            BOrg.setFieldValue("CommodityCodeCatApprover37",
                               cr.getFieldValue("CommCodeCatAppr37"));
            BOrg.setFieldValue("CommodityCodeCat38",
                               cr.getFieldValue("CommCodeCat38"));
            BOrg.setFieldValue("CommodityCodeCatApprover38",
                               cr.getFieldValue("CommCodeCatAppr38"));
            BOrg.setFieldValue("CommodityCodeCat39",
                               cr.getFieldValue("CommCodeCat39"));
            BOrg.setFieldValue("CommodityCodeCatApprover39",
                               cr.getFieldValue("CommCodeCatAppr39"));
            BOrg.setFieldValue("CommodityCodeCat40",
                               cr.getFieldValue("CommCodeCat40"));
            BOrg.setFieldValue("CommodityCodeCatApprover40",
                               cr.getFieldValue("CommCodeCatAppr40"));

            BOrg.setFieldValue("WatchCommodityCodeCat1",
                               cr.getFieldValue("WatchCommCodeCat1"));
            BOrg.setFieldValue("CommodityCodeCatWatcher1",
                               cr.getFieldValue("CommCodeCatWatcher1"));
            BOrg.setFieldValue("WatchCommodityCodeCat2",
                               cr.getFieldValue("WatchCommCodeCat2"));
            BOrg.setFieldValue("CommodityCodeCatWatcher2",
                               cr.getFieldValue("CommCodeCatWatcher2"));
            BOrg.setFieldValue("WatchCommodityCodeCat3",
                               cr.getFieldValue("WatchCommCodeCat3"));
            BOrg.setFieldValue("CommodityCodeCatWatcher3",
                               cr.getFieldValue("CommCodeCatWatcher3"));
            BOrg.setFieldValue("WatchCommodityCodeCat4",
                               cr.getFieldValue("WatchCommCodeCat4"));
            BOrg.setFieldValue("CommodityCodeCatWatcher4",
                               cr.getFieldValue("CommCodeCatWatcher4"));
            BOrg.setFieldValue("WatchCommodityCodeCat5",
                               cr.getFieldValue("WatchCommCodeCat5"));
            BOrg.setFieldValue("CommodityCodeCatWatcher5",
                               cr.getFieldValue("CommCodeCatWatcher5"));
            BOrg.setFieldValue("WatchCommodityCodeCat6",
                               cr.getFieldValue("WatchCommCodeCat6"));
            BOrg.setFieldValue("CommodityCodeCatWatcher6",
                               cr.getFieldValue("CommCodeCatWatcher6"));
            BOrg.setFieldValue("WatchCommodityCodeCat7",
                               cr.getFieldValue("WatchCommCodeCat7"));
            BOrg.setFieldValue("CommodityCodeCatWatcher7",
                               cr.getFieldValue("CommCodeCatWatcher7"));
            BOrg.setFieldValue("WatchCommodityCodeCat8",
                               cr.getFieldValue("WatchCommCodeCat8"));
            BOrg.setFieldValue("CommodityCodeCatWatcher8",
                               cr.getFieldValue("CommCodeCatWatcher8"));
            BOrg.setFieldValue("WatchCommodityCodeCat9",
                               cr.getFieldValue("WatchCommCodeCat9"));
            BOrg.setFieldValue("CommodityCodeCatWatcher9",
                               cr.getFieldValue("CommCodeCatWatcher9"));
            BOrg.setFieldValue("WatchCommodityCodeCat10",
                               cr.getFieldValue("WatchCommCodeCat10"));
            BOrg.setFieldValue("CommodityCodeCatWatcher10",
                               cr.getFieldValue("CommCodeCatWatcher10"));

            BOrg.setFieldValue("BillingAddress",
                               cr.getFieldValue("BillingAddress"));
        }


        // Add Push followed immediately by the approve transaction to update the buysenseorg tables. BGN SPL#193

        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        ariba.user.core.User RequesterUser = (ariba.user.core.User)cr.getDottedFieldValue("Requester");
        //rgiesen dev SPL #39
        //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,cr.getPartition());
        String clientName = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName");
        String clientID = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientID");
        //String BSOrgName = (String)Base.getService().getParameter(Base.getSession().getPartition(),
        String BSOrgName = (String)Base.getService().getParameter(cr.getPartition(),
                                                                  "Application.AMSParameters.BSORGClient");
        String XMLClientTag = clientID.trim() + "_" + BSOrgName;
        String updateFlag = null;
        String nullString = null;
        if (NBOrg==true)
        {
            updateFlag = "NEW";
        }
        else
        {
            updateFlag = "UPP";
        }

        AMSTXN.initMappingData();
        int rt1=AMSTXN.push(XMLClientTag,
                            "BO01",
                            (String)cr.getFieldValue("UniqueName"),
                            updateFlag,
                            "RDY",
                            "A",
                            "0",
                            "",
                            BOrg, nullString, nullString, cr.getPartition());
        Log.customer.debug("value of rt1 %s",rt1);
        int rt2=AMSTXN.push(XMLClientTag,
                            "BO02",
                            (String)cr.getFieldValue("UniqueName"),
                            updateFlag,
                            "RDY",
                            "A",
                            "0",
                            "", BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt2 %s",rt2);
        int rt3=AMSTXN.push(XMLClientTag,
                            "BO03",
                            (String)cr.getFieldValue("UniqueName"),
                            updateFlag,
                            "RDY",
                            "A",
                            "0",
                            "", BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt3 %s",rt3);
        int rt4=AMSTXN.push(XMLClientTag,
                            "BO04",
                            (String)cr.getFieldValue("UniqueName"),
                            updateFlag,
                            "RDY",
                            "A",
                            "0",
                            "", BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt4 %s",rt4);
        int rt5=AMSTXN.push(XMLClientTag,
                            "BO05",
                            (String)cr.getFieldValue("UniqueName"),
                            updateFlag,
                            "RDY",
                            "A",
                            "0",
                            "", BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt5 %s",rt5);
        int rt6=AMSTXN.push(XMLClientTag,
                            "BO06",
                            (String)cr.getFieldValue("UniqueName"),
                            updateFlag,
                            "RDY",
                            "A",
                            "0",
                            "", BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt6 %s",rt6);
        int rt7=AMSTXN.push(XMLClientTag,
                            "BO07",
                            (String)cr.getFieldValue("UniqueName"),
                            updateFlag,
                            "RDY",
                            "A",
                            "0",
                            "", BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt7 %s",rt7);
        int rt8=AMSTXN.push(XMLClientTag,
                            "BO08",
                            (String)cr.getFieldValue("UniqueName"),
                            updateFlag,
                            "RDY",
                            "A",
                            "0",
                            "", BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt8 %s",rt8);
        int rt9=AMSTXN.push(XMLClientTag,
                            "BO09",
                            (String)cr.getFieldValue("UniqueName"),
                            updateFlag,
                            "RDY",
                            "A",
                            "0",
                            "", BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt9 %s",rt9);
        int rt10=AMSTXN.push(XMLClientTag,
                             "BO10",
                             (String)cr.getFieldValue("UniqueName"),
                             updateFlag,
                             "RDY",
                             "A",
                             "0",
                             "",
                             BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt10 %s",rt10);
        int rt11=AMSTXN.push(XMLClientTag,
                             "BO11",
                             (String)cr.getFieldValue("UniqueName"),
                             updateFlag,
                             "RDY",
                             "A",
                             "0",
                             "",
                             BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt11 %s",rt11);
        int rt12=AMSTXN.push(XMLClientTag,
                             "BO12",
                             (String)cr.getFieldValue("UniqueName"),
                             updateFlag,
                             "RDY",
                             "A",
                             "0",
                             "",
                             BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt12 %s",rt12);
        int rt13=AMSTXN.push(XMLClientTag,
                             "BO13",
                             (String)cr.getFieldValue("UniqueName"),
                             updateFlag,
                             "RDY",
                             "A",
                             "0",
                             "",
                             BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt13 %s",rt13);
        int rt14=AMSTXN.push(XMLClientTag,
                             "BO14",
                             (String)cr.getFieldValue("UniqueName"),
                             updateFlag,
                             "RDY",
                             "A",
                             "0",
                             "",
                             BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt14 %s",rt14);
        int rt15=AMSTXN.push(XMLClientTag,
                             "BO15",
                             (String)cr.getFieldValue("UniqueName"),
                             updateFlag,
                             "RDY",
                             "A",
                             "0",
                             "",
                             BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt15 %s",rt15);
        int rt16=AMSTXN.push(XMLClientTag,
                             "BO16",
                             (String)cr.getFieldValue("UniqueName"),
                             updateFlag,
                             "RDY",
                             "A",
                             "0",
                             "",
                             BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt16 %s",rt16);
        int rt17=AMSTXN.push(XMLClientTag,
                             "BO17",
                             (String)cr.getFieldValue("UniqueName"),
                             updateFlag,
                             "RDY",
                             "A",
                             "0",
                             "",
                             BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt17 %s",rt17);
        int rt18=AMSTXN.push(XMLClientTag,
                             "BO18",
                             (String)cr.getFieldValue("UniqueName"),
                             updateFlag,
                             "RDY",
                             "A",
                             "0",
                             "",
                             BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt18 %s",rt18);
        int rt19=AMSTXN.push(XMLClientTag,
                             "BO19",
                             (String)cr.getFieldValue("UniqueName"),
                             updateFlag,
                             "RDY",
                             "A",
                             "0",
                             "",
                             BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt19 %s",rt19);
        int rt20=AMSTXN.push(XMLClientTag,
                             "BO20",
                             (String)cr.getFieldValue("UniqueName"),
                             updateFlag,
                             "RDY",
                             "A",
                             "0",
                             "",
                             BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt20   %s",rt20);
        int rt21=AMSTXN.push(XMLClientTag,
                             "BO21",
                             (String)cr.getFieldValue("UniqueName"),
                             updateFlag,
                             "RDY",
                             "A",
                             "0",
                             "",
                             BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt21 %s",rt21);
        int rt22=AMSTXN.push(XMLClientTag,
                             "BO22",
                             (String)cr.getFieldValue("UniqueName"),
                             updateFlag,
                             "RDY",
                             "A",
                             "0",
                             "",
                             BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt22 %s",rt22);
        int rt23=AMSTXN.push(XMLClientTag,
                             "BO23",
                             (String)cr.getFieldValue("UniqueName"),
                             updateFlag,
                             "RDY",
                             "A",
                             "0",
                             "",
                             BOrg, nullString,nullString, cr.getPartition());
        Log.customer.debug("value of rt23 %s",rt23);

        // END SPL#193


    }




}
