package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.MultiLingualString;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseSetODUEPJTitle extends Action
{
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        Log.customer.debug("fire method called");
        Approvable loAppr = null;
        if(valuesource!= null && valuesource instanceof Approvable)
        {
            loAppr = (Approvable)valuesource;
            setCustomTitle(loAppr);
        }
    }

    public void setCustomTitle(Approvable appr)
    {
    	ariba.user.core.User loRequesterUser = (ariba.user.core.User)appr.getRequester();
    	if(loRequesterUser != null)
    	{
    		Log.customer.debug("The requester is :"+loRequesterUser);
    		ariba.common.core.User loRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(loRequesterUser,appr.getPartition());
            String lsClientName = (String)loRequesterPartitionUser.getDottedFieldValue("ClientName.ClientName");
            Log.customer.debug("ClientName  "+lsClientName);
            lsClientName = StringUtil.substring(lsClientName, 0, 4);
            String lsRequesterName = (String)loRequesterUser.getDottedFieldValue("Name.PrimaryString");
            Log.customer.debug("The Requester Name is:"+lsRequesterName);
            if(lsClientName != null)
            {
            	Log.customer.debug("The Title is about to be set");
                appr.setName(lsClientName+ " - Emergency Procurement Justification - " +lsRequesterName);
                Log.customer.debug("The Title is :"+appr.getName());
            }            
    	}

    }
}
