package config.java.ams.custom;

import ariba.base.core.BaseVector;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.procure.core.CategoryLineItemDetails;
import ariba.procure.core.RankedSupplier;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
import ariba.collaboration.core.*;
import ariba.common.core.Supplier;
import ariba.common.core.SupplierLocation;


public class SetCollaborativeRankedSupplierLocation extends Action
{
	public void fire(ValueSource object, PropertyTable params) throws ActionExecutionException
	{
		Log.customer.debug("Calling SetRankedSupplierLocation.java"+object);
		CollaborationRequest collaborationReq =  (CollaborationRequest)object;
	    BaseVector collaborationLineItems = collaborationReq.getLineItems();
	    int lineItemsize = collaborationLineItems.size();
	    for(int i=0;i<lineItemsize;i++)
	    {
	    	Log.customer.debug("inside for");
	    	CollaborationLineItem collaborativeLineItem = (CollaborationLineItem)collaborationLineItems.get(i);
	    	Supplier linesupplier = (Supplier)collaborativeLineItem.getSupplier();
	    	CategoryLineItemDetails  categoryLineItem = (CategoryLineItemDetails) collaborativeLineItem.getFieldValue("CategoryLineItemDetails");
	    	BaseVector invitedlsupplierList = categoryLineItem.getInvitedSuppliers();
	    	if(invitedlsupplierList.size()>=1)
	    	{
	    		Log.customer.debug("inside if");
	    		SupplierLocation location = getRankedSupplierLocation(linesupplier,invitedlsupplierList);
	    		if(location!=null)
	    	    {
	    			collaborativeLineItem.setFieldValue("SupplierLocation", location);
	    	    }
	    		else return;
	    	}
	    	else return;
	    }
	}

	public static SupplierLocation  getRankedSupplierLocation(Supplier linesupplier, BaseVector invitedlsupplierList)
	{
		Log.customer.debug("inside getRankedSupplierLocation");
		int supplierlistsize = invitedlsupplierList.size();
		String supplieruniquename = linesupplier.getUniqueName();
		for (int i=0;i<supplierlistsize;i++)
		{
			Log.customer.debug("inside getRankedSupplierLocation- in side if");
			ariba.procure.core.RankedSupplier ranksupplier = (RankedSupplier) invitedlsupplierList.get(i);
			Supplier partsupplier = ranksupplier.getPartitionedSupplier();
			String sPartsupplierUniqueName = partsupplier.getUniqueName();
			if(sPartsupplierUniqueName.equals(supplieruniquename))
			{
				return (SupplierLocation) ranksupplier.getFieldValue("SupplierLocation");
			}
		}
		return null;

	}
}
