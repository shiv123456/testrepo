package config.java.ams.custom ;

import java.util.Collections;
import java.util.List;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableUtil;
import ariba.approvable.core.ApprovalRequest;
import ariba.approvable.core.Record;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.base.fields.ConditionResult;
import ariba.base.fields.FieldProperties;
import ariba.base.fields.ValueSourceUtil;
import ariba.purchasing.core.PurchaseOrder;
import ariba.receiving.core.Receipt;
import ariba.receiving.core.ReceiptItem;
import ariba.user.core.Group;
import ariba.user.core.Role;
import ariba.util.core.Date;
import ariba.util.core.ListUtil;
import ariba.util.log.Log;
import ariba.util.core.StringUtil;

public class BuysenseERPReceiptImport extends BuysenseXMLImport implements BuyintConstants
{
	private static String msCN = "BuysenseERPReceiptImport";
	protected static final String PO_LINE_NUMBER = "po_line_number";
	protected static final String AUTO_APPROVE = "auto_approve";
	protected static final String RECEIVER_LOGIN = "receiver_login";
	protected static final String RECEIVE_DATE = "receive_date";
	
	public static Receipt moComposingReceipt = null;
	public static boolean mbHeader = false;
	public static boolean mbDetail = false;
	public static String msRCNumber = null;
	public static String msPONumber = null;
	
	String moReceiverLoginMessage = null;
	String msField = null;
	boolean moIsValidReceiver = true;
	ariba.user.core.User moReceiver = null;
	
	public BuysenseERPReceiptImport()
    {
        super();
        moPartition = Base.getSession().getPartition() ;
    }

    public BuysenseERPReceiptImport( Partition foPartition )
    {
        super();
        moPartition = foPartition;
    }
    
   /**
    * 
    * @param orderUniqueName
    * @return PurchaseOrder
    */
    public PurchaseOrder findReceiptOrder(String orderUniqueName)
    {
    	PurchaseOrder loOrder = null;
    	moPartition = Base.getSession().getPartition();
    	loOrder = (PurchaseOrder) Base.getService().objectMatchingUniqueName("ariba.purchasing.core.PurchaseOrder", moPartition, orderUniqueName);
    	if(loOrder != null)
    	{
    		loOrder = (PurchaseOrder) loOrder.getLatestVersion();
    		msPONumber = (String) loOrder.getUniqueName();
    	}
    	return loOrder;
    }
    
    public boolean orderGoodForReceiving(PurchaseOrder order)
    {
    	if(order.getStatusString().equals("Ordered") || order.getStatusString().equals("Receiving"))
    	{
    		return true;
    	}
    	return false;
    }
    
    public boolean orderClosedForReceiving(PurchaseOrder order)
    {
    	if(order.getClosed() == 3 || order.getClosed() == 5)
    	{
    		return true;
    	}
    	return false;
    }

    public boolean hasComposingReceipts(PurchaseOrder order)
    {
    	boolean lbHasComposingReceipt = false;
		List lreceipts = ListUtil.list();
		lreceipts = (List)order.getReceipts();
		for(int i=0;i<lreceipts.size(); i++)
		{
			Receipt receipt;
			BaseId receiptId = (BaseId) lreceipts.get(i);
			receipt = (Receipt)Base.getSession().objectFromId(receiptId);
			if(receipt.getStatusString().equals("Composing"))
			{
				moComposingReceipt = receipt;
				msRCNumber = (String) moComposingReceipt.getUniqueName();
				lbHasComposingReceipt = true;
				break;
			}
		}
		return lbHasComposingReceipt;	    	
    }

    public boolean validReceivingDate(String receiveDate)
    {
    	boolean lbValidReceiveDate = true;
    	int result = 0;
    	Object loValueObj = BuysenseXMLFieldParser.parseFieldValue(receiveDate, "ariba.util.core.Date");
    	if(loValueObj instanceof ariba.util.core.Date)
    	{
    		Date loReceiveDate = (Date) loValueObj;
    		result = Date.getNow().compareTo(loReceiveDate);
    		if(result < 0)
    		{
    			lbValidReceiveDate = false;
    		}
    	}
    	return lbValidReceiveDate;
    }
    
    public boolean receiptEditedManually()
    {
    	boolean lbReceiptManualEdit = false;
    	List lreceiptLines = (List) moComposingReceipt.getReceiptItems();
		for(int i=0;i<lreceiptLines.size(); i++)
		{
			ReceiptItem recpItem = (ReceiptItem)lreceiptLines.get(i);
			if(recpItem.getNumberAccepted().intValue() > 0 || recpItem.getNumberRejected().intValue() > 0)
			{
				lbReceiptManualEdit = true;
				break;
			}
		}
		return lbReceiptManualEdit;
    }
    
    public boolean isValidReceiverLoginId(String receiverLoginId)
    {
    	moReceiver = (ariba.user.core.User) Base.getService().objectMatchingUniqueName("ariba.user.core.User", Base.getService().getPartition("None"), receiverLoginId);
    	if(moReceiver == null)
    	{
    		moReceiverLoginMessage = "Receiver Login ID doesn't match in emall";
    		return false;
    	}
    	moReceiverLoginMessage = checkForValidReceiver(moReceiver, moReceiverLoginMessage);
    	if(moReceiverLoginMessage == null)
    	{
    		return true;
    	}
    	return false;
     }
    
    public String checkForValidReceiver(ariba.user.core.User receiver, String receiverLoginMessage)
    {
    	java.util.List lvAllApprovalRequest = getAllApprovalRequests();
    	ApprovalRequest loApprovalRequest = null;
    	receiverLoginMessage = null;
    	boolean lbValidReceiver = false;
        for (int i = 0; i < lvAllApprovalRequest.size(); i ++)
        {
        	loApprovalRequest = (ApprovalRequest) lvAllApprovalRequest.get(i);
        	if(loApprovalRequest.getApprovalRequired() && loApprovalRequest.getDerivedApprovalState().equals("Ready") &&
        			loApprovalRequest.getDerivedApprovalRequired().equals("Required"))
        	{
        		lbValidReceiver = isValidReceiver(receiver, loApprovalRequest, lbValidReceiver);
        		Log.customer.debug("***%s***::checkForValidReceiver::lbValidReceiver %s", msCN, lbValidReceiver);
        		if(lbValidReceiver)
        		{
        			break;
        		}
        		if(!lbValidReceiver && loApprovalRequest.getManuallyAdded())
        		{
        			receiverLoginMessage = "Receiver Login ID is not valid and it is manually added in emall";
        		}
        		else if(!lbValidReceiver && !loApprovalRequest.getManuallyAdded())
        		{
        			receiverLoginMessage = "Receiver Login ID is not valid in emall";
        		}
        	}
        }
        return receiverLoginMessage;
    }
    
    public boolean  isValidReceiver(ariba.user.core.User receiver, ApprovalRequest approvalRequest, boolean validReceiver)
    {
    	validReceiver = false;
    	if(approvalRequest.getApprover() != null)
    	{
    		if(approvalRequest.getApprover() instanceof Role)
    		{
    			//validReceiver = ((Role)approvalRequest.getApprover()).getAllUsers().contains(receiver);
    			validReceiver = receiver.hasRole((Role)approvalRequest.getApprover());
    		}
    		else if(approvalRequest.getApprover() instanceof Group)
    		{
    			//validReceiver = ((Group)approvalRequest.getApprover()).getAllUsers().contains(receiver);
    			validReceiver = receiver.hasGroup((Group)approvalRequest.getApprover());
    		}
    		else if(approvalRequest.getApprover() instanceof ariba.user.core.User)
    		{
    			validReceiver = ((ariba.user.core.User)approvalRequest.getApprover()).getUniqueName().equals(receiver.getUniqueName());
    		}
    	}
    	return validReceiver;
    }
    
    public List getAllApprovalRequests()
    {
    	List lvApprovalRequestsVector;
        List lvecApprovalRequests;
        ApprovalRequest loApprovalRequest;
        
    	lvApprovalRequestsVector = (List)moComposingReceipt.getFieldValue("ApprovalRequests");
    	lvecApprovalRequests = ListUtil.cloneList(lvApprovalRequestsVector);
        for (int i = 0; i < lvecApprovalRequests.size(); i ++)
        {
        	loApprovalRequest = (ApprovalRequest) lvecApprovalRequests.get(i);
        	lvApprovalRequestsVector = getDependencies(lvecApprovalRequests, loApprovalRequest);
        }
        return lvecApprovalRequests;
    }
    
    public List getDependencies(List lvec, BaseObject appReq)
    {
        List vecNewDependencies = ListUtil.list();
        ApprovalRequest newDependency = new ApprovalRequest();
        vecNewDependencies = (List)appReq.getFieldValue("Dependencies");
        int size = vecNewDependencies.size();

        for (int i = 0; i < size; i ++)
        {
            newDependency = (ApprovalRequest) vecNewDependencies.get(i);
            if (lvec.contains(newDependency))
            {
                continue;
            }
            else
            {
                lvec.add(newDependency);
                getDependencies(lvec, newDependency);
            }
        }
        return lvec;
    }

    
    /**
     * This method processes the start of the header section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void startHeader( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        try
        {
            PurchaseOrder loOrder = null ;
            String      lsAribaType   = foMyContext.getAttrValue(ARIBA_TYPE);
            String      lsPOUniqueName  = foMyContext.getAttrValue(PO_UNIQUE_NAME);
            String      lsReceiverLoginId  = foMyContext.getAttrValue(RECEIVER_LOGIN);
            
            lsAribaType = (String) BuyintXWalk.XWalk.changeOldToNew(lsAribaType);
            
            moComposingReceipt = null;
            msRCNumber = null;
            msPONumber = null;
            mbHeader = false;
            mbDetail = false;

            /* Find if Purchase Order with the UniqueName exists 
             * If exists then always get latest version*/
            loOrder = findReceiptOrder(lsPOUniqueName) ;
            
            if(!lsAribaType.equals("ariba.receiving.core.Receipt"))
            {
            	Log.customer.debug("***%s***::startHeader::Type is not Receipt", msCN);
                throw new BuysenseXMLImportException(
                                                     "Error:Type is not Receipt", ERR_CODE_SKIP_RETRY);
            }
            else if (loOrder == null)
            {
            	Log.customer.debug("***%s***::startHeader::Requested Order for Receipt Import not found", msCN);
                throw new BuysenseXMLImportException(
                                                     "Error: Receipt Transaction cannot be processed as a valid Ariba order " +lsPOUniqueName+ " doesn't exist.", ERR_CODE_SKIP_RETRY );
            }
            else if(!loOrder.getActive())
            {
            	Log.customer.debug("***%s***::startHeader::Requested Order for Receipt Import is not Active", msCN);
                throw new BuysenseXMLImportException(
                                                     "Error: Receipt Transaction cannot be processed because the Order is in Inactive status.", ERR_CODE_SKIP_RETRY);
            }
            else if(!orderGoodForReceiving(loOrder))
            {
            	Log.customer.debug("***%s***::startHeader::Order status is %s", msCN, loOrder.getStatusString());
                throw new BuysenseXMLImportException(
                                                     "Error: Receipt Transaction cannot be processed as a Order is in " +loOrder.getStatusString()+ " status", ERR_CODE_SKIP_RETRY);
            }
            else if(orderClosedForReceiving(loOrder))
            {
            	Log.customer.debug("***%s***::startHeader::Requested Order is closed for Receiving", msCN);
                throw new BuysenseXMLImportException(
                                                     "Error: Receipt Transaction cannot be processed as order is closed for receiving.", ERR_CODE_SKIP_RETRY);
            }
            else if(!hasComposingReceipts(loOrder))
            {
            	Log.customer.debug("***%s***::startHeader::Requested Order doesnt have Composing Receipt to Import", msCN);
                throw new BuysenseXMLImportException(
                                                     "Error: Receipt Transaction cannot be processed because the Order does not have any composing receipts.", ERR_CODE_SKIP_RETRY);
            }
            else if(receiptEditedManually())
            {
            	Log.customer.debug("***%s***::startHeader::Requested Order Receipt is edited Manually", msCN);
                throw new BuysenseXMLImportException(
                                                     "Error: Receipt Transaction cannot be processed as the receipt in eMall  has Non Zero values currently.", ERR_CODE_SKIP_RETRY);
            }
            else if(!isValidReceiverLoginId(lsReceiverLoginId))
            {
            	Log.customer.debug("***%s***::startHeader::Invalid Receiver Login ID %s", msCN, moReceiverLoginMessage);
                throw new BuysenseXMLImportException(
                                                     "Error: Receipt Transaction cannot be processed as the user login ID specified as the receiver is not a valid receiver for the eMall Order.", ERR_CODE_SKIP_RETRY);
            }
            else
            {
            	/* This order passed all header validations and ready for Line Validations */
            	Log.customer.debug("***%s***::startHeader::Order passed all header validations and ready for Line Validations", msCN);
            }
            mbHeader = true;
            //msRCNumber = (String) moComposingReceipt.getUniqueName();
            //msPONumber = (String) loOrder.getUniqueName();
            moHeader = (ClusterRoot) moComposingReceipt;
            foMyContext.setObject((ClusterRoot) moComposingReceipt);
        }
        catch (BuysenseXMLImportException loBXMLIE)
        {
        	throw loBXMLIE;
        }
        catch ( Exception loException )
        {
        	throw new BuysenseXMLImportException(
                                                 "Error: Exception while creating header: " +
                                                 loException.getMessage() ) ;
        }
    }

    /**
     * This method processes the start of the details section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void startDetails( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
        /* Hold Receipt Line Items in this list for processing */
    	try
    	{
	    	if(moComposingReceipt == null)
	    	{
	    		moComposingReceipt = (Receipt) getHeaderObject();
	    		if(moComposingReceipt == null)
	    		{
	            	Log.customer.debug("***%s***::startDetails::Not able to retreive header", msCN);
	                throw new BuysenseXMLImportException(
	                                                     "Error:Not able to retreive header", ERR_CODE_DEFAULT);
	    		}
	    	}
	    	else
	    	{
		    	foMyContext.setObject((List)moComposingReceipt.getReceiptItems()) ;	    		
	    	}
    	}
        catch (BuysenseXMLImportException loBXMLIE)
        {
        	throw loBXMLIE;
        }
        catch (Exception loException)
        {
        	throw new BuysenseXMLImportException(
                                                 "Error: Exception while creating details: " +
                                                 loException.getMessage()) ;
        }
    }
    
    /**
     * This method processes the start of a detail line item.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void startDetail( ContextObject foMyContext )
        throws BuysenseXMLImportException
    {
    	String lsReceiptLine = null;
        BaseObject loBaseObj = null ;
        
    	try
        {
            lsReceiptLine   = foMyContext.getAttrValue(PO_LINE_NUMBER);
            
            if(moComposingReceipt == null)
	    	{
	    		moComposingReceipt = (Receipt) getHeaderObject();
	    		if(moComposingReceipt == null)
	    		{
	            	Log.customer.debug("***%s***::startDetail::Not able to retreive header", msCN);
	                throw new BuysenseXMLImportException(
	                                                     "Error: Not able to retreive header", ERR_CODE_DEFAULT);
	    		}
	    	}
	    	else
	    	{
	    		try
	    		{
	    			loBaseObj = (BaseObject)moComposingReceipt.getReceiptItem(Integer.parseInt(lsReceiptLine.trim()));
	    			if(loBaseObj == null)
	    			{
		            	Log.customer.debug("***%s***::startDetail::Not a valid Receipt Line", msCN);
	    				throw new BuysenseXMLImportException(
                                "Error: Not a valid Receipt Line", ERR_CODE_SKIP_RETRY);
	    			}
		    		else
		    		{
		    			/* Order also passed all line validations and ready for import */
		    			Log.customer.debug("***%s***::startDetail::Order also passed all line validations and READY for IMPORT", msCN);
		    		}
	    		}
	            catch (NumberFormatException loNumberFormatException)
	            {
	                Log.customer.debug("Could not resolve : " + lsReceiptLine + "as int.") ;
	                throw new Exception("Cannot process detail line ... \"" + lsReceiptLine + "\" is not a valid line number");
	            }
	    	}
	    	mbDetail = true;
            moDetail = loBaseObj;
            foMyContext.setObject(loBaseObj);
        }
        catch (BuysenseXMLImportException loBXMLIE)
        {
            Log.customer.debug("***%s***::startDetail:: Caught with BuysenseXMLImportException", msCN);
            throw loBXMLIE;
        }
    	catch (Exception loException)
        {
            Log.customer.debug("***%s***::startDetail:: Caught with Exception", msCN);
    		throw new BuysenseXMLImportException(
                                                 "Error:Exception while creating detail: " +
                                                 loException.getMessage() ) ;
        }
    }
    
    /**
     * This method processes the end of the details section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void finishDetails(ContextObject foMyContext)
        throws BuysenseXMLImportException
    {
    	Log.customer.debug("***%s***::finishDetails::Called", msCN);
    	/* Just override super to make it silent 
         * Dont think we need any special processing for Receipts*/
    }

    /**
     * This method processes the end of the detail section.
     *
     * @param foAttrList The XML tag attributes parsed
     * @exception BuysenseXMLImportException
     */
    protected void finishDetail(ContextObject foMyContext)
        throws BuysenseXMLImportException
    {
    	Log.customer.debug("***%s***::finishDetail::Calling validation check for %s", msCN, moDetail);
    	String lsErrorMsg = null;
    	if(moDetail == null || !(moDetail instanceof ReceiptItem))
    	{
			Log.customer.debug("***%s***::finishDetail::Not able to retreive detail", msCN);
            throw new BuysenseXMLImportException(
                                                 "Error:Not able to retreive detail", ERR_CODE_DEFAULT);
    	}
    	else if(moDetail != null && moDetail instanceof ReceiptItem)
        {
    		ConditionResult errorMsgs = checkValidationErrors(); 
	        if(errorMsgs != null && errorMsgs.getErrorCount() > 0)
	        {
	        	lsErrorMsg = getFinalErrorMsg(errorMsgs);
	        	Log.customer.debug("***%s***::finishDetail::Failed with validation error during submission, error msg %s", msCN, lsErrorMsg);
	            throw new BuysenseXMLImportException(
	                                                 "Error: " +lsErrorMsg, ERR_CODE_SKIP_RETRY);
	        }
        }
    	else
    	{
    		Log.customer.debug("***%s***::finishDetail::Successfully passed finished detail", msCN);
    	}
    	Log.customer.debug("***%s***::finishDetail::Finished Calling validation check %s", msCN, lsErrorMsg);
    }

    protected void finishHeader(ContextObject foMyContext)
        throws BuysenseXMLImportException
    {
        try
        {
        	this.checkFieldParsingErrors();
        	String lsAutoApprove = foMyContext.getAttrValue(AUTO_APPROVE);   
        	if(moComposingReceipt == null)
	    	{
	    		moComposingReceipt = (Receipt)getHeaderObject();
	    	}
	    	else if(moComposingReceipt == null)
	    	{
	    		moComposingReceipt = (Receipt)foMyContext.getObject();	
	    	}
	    	else if(moComposingReceipt == null)
	    	{
    			Log.customer.debug("***%s***::finishHeader::Not able to retreive header", msCN);
                throw new BuysenseXMLImportException(
                                                     "Error:Not able to retreive header", ERR_CODE_DEFAULT);
	    	}

        	Base.getSession().setEffectiveUser(moReceiver.getBaseId());
        	/* Right now eVA doesn't have Receipt Submit or Approve hook check, added this for safer side */
        	/* Starting submit hook check */
        	List loHookCheckEW;
        	loHookCheckEW = ApprovableUtil.getService().getApprovableServer().checkSubmit((Approvable)moComposingReceipt);
        	Log.customer.debug("***%s***::finishHeader::Finished Calling submit check %s", msCN, loHookCheckEW);
            if(loHookCheckEW == null || !(ListUtil.firstElement(loHookCheckEW) instanceof Integer))
            {
               throw new BuysenseXMLImportException("Error: Receipt: " + moComposingReceipt.getUniqueName() +
                                                 " failed submit hook validation.");
            }
            else if(((Integer) loHookCheckEW.get(0)).intValue() == -1)
            {
    			Log.customer.debug("***%s***::finishHeader::Caught with submit hook error %s", msCN, (String) loHookCheckEW.get(1));
                throw new BuysenseXMLImportException(
                                                     "Error: " +(String) loHookCheckEW.get(1), ERR_CODE_SKIP_RETRY);
            }
            /* Ended submit hook check */
            
        	/* Starting approve hook check */
        	loHookCheckEW = ApprovableUtil.getService().getApprovableServer().checkApprove((Approvable)moComposingReceipt);
        	Log.customer.debug("***%s***::finishHeader::Finished Calling approve check %s", msCN, loHookCheckEW);
            if(loHookCheckEW == null || !(ListUtil.firstElement(loHookCheckEW) instanceof Integer))
            {
               throw new BuysenseXMLImportException("Error: Receipt: " + moComposingReceipt.getUniqueName() +
                                                 " failed approve hook validation.");
            }
            else if(((Integer) loHookCheckEW.get(0)).intValue() == -1)
            {
    			Log.customer.debug("***%s***::finishHeader::Caught with approve hook error %s", msCN, (String) loHookCheckEW.get(1));
                throw new BuysenseXMLImportException(
                                                     "Error: " +(String) loHookCheckEW.get(1), ERR_CODE_SKIP_RETRY);
            }
            /* Ended approve hook check */            
        	Base.getSession().setEffectiveUser(null);

        	/* Set AutoApprove after finishing all validations */
            moComposingReceipt.setFieldValue("AutoApprove", (Boolean.valueOf(lsAutoApprove)).booleanValue());
            
            List loApprovalRequests = this.getAllApprovalRequests();
            Collections.reverse(loApprovalRequests);  
            /* Start Auto Approval Process */
            if (((Boolean.valueOf(lsAutoApprove)).booleanValue()) && moComposingReceipt != null)
            {
            	Log.customer.debug("***%s***::finishHeader::Auto Approving Receipt", msCN);
                for (int i = 0; i < loApprovalRequests.size(); i ++)
                {
                	ApprovalRequest loApprovalRequest = (ApprovalRequest) loApprovalRequests.get(i);
                	Log.customer.debug("***%s***::finishHeader::Auto approving with approver %s", msCN, loApprovalRequest.getApprover().getUniqueName());
                	if(loApprovalRequest.getApprovalRequired() && loApprovalRequest.getDerivedApprovalState().equals("Ready") &&
                			loApprovalRequest.getDerivedApprovalRequired().equals("Required") && loApprovalRequest.getApprover() != null)
                	{
                		moComposingReceipt.approve(loApprovalRequest.getApprover(), moReceiver, null, null);
                	}
                	else if(loApprovalRequest.getApprovalRequired() && loApprovalRequest.getApprover() != null && 
                			!loApprovalRequest.isApproved() && loApprovalRequest.getState() == 2)
                	{
                		/* This is Ready for Approval */
                		moComposingReceipt.approve(loApprovalRequest.getApprover(), ariba.user.core.User.getAribaSystemUser());
                	}
                	else if(loApprovalRequest.getApprovalRequired() && loApprovalRequest.getApprover() != null && 
                			!loApprovalRequest.isApproved() && loApprovalRequest.getState() == 1)
                	{
                		/* Force it to get Ready for Approval */
                		loApprovalRequest.setState(2);
                		Log.customer.debug("***%s***::finishHeader::Second else If State %s", msCN, loApprovalRequest.getState());
                		moComposingReceipt.approve(loApprovalRequest.getApprover(), ariba.user.core.User.getAribaSystemUser());
                	}
                	else if(loApprovalRequest.getApprovalRequired() && loApprovalRequest.getApprover() != null && 
                			!loApprovalRequest.isApproved())
                	{
                		/* Force it to get Ready for Approval in any of the state */
                		loApprovalRequest.setState(2);
                		Log.customer.debug("***%s***::finishHeader::Third else If State %s", msCN, loApprovalRequest.getState());
                		moComposingReceipt.approve(loApprovalRequest.getApprover(), ariba.user.core.User.getAribaSystemUser());
                	}
                	else
                	{
                		Log.customer.debug("***%s***::finishHeader::Not able to approve and state is %s", msCN, loApprovalRequest.getState());
                	}
                }
            }
            else
            {
            	/* Start Auto Submit Process */
            	Log.customer.debug("***%s***::finishHeader::Auto Submitting Receipt with user %s", msCN, moReceiver);
                for (int i = 0; i < loApprovalRequests.size(); i ++)
                {
                	ApprovalRequest loApprovalRequest = (ApprovalRequest) loApprovalRequests.get(i);
                	if(loApprovalRequest.getApprovalRequired() && loApprovalRequest.getDerivedApprovalState().equals("Ready") &&
                			loApprovalRequest.getDerivedApprovalRequired().equals("Required") && loApprovalRequest.getApprover() != null)
                	{
                    	moComposingReceipt.approve(loApprovalRequest.getApprover(), moReceiver, null, null);
                    	break;
                	}
                }
            }
            /* Update all records to ariba system */
            updateRecordRealUser();
            moComposingReceipt.save();
            Base.getSession().sessionCommit();
         }
        catch (BuysenseXMLImportException loBuyException)
        {
        	throw loBuyException ;
        }
        catch (Exception loException)
        {
        	throw new BuysenseXMLImportException(
                                                 "Error: Exception while finishing header: " +
                                                 loException.getMessage() ) ;
        }
    }
    
    public void updateRecordRealUser()
    {
    	List loRecords = (List) moComposingReceipt.getRecords();
        for (int i = 0; i < loRecords.size(); i ++)
        {
        	Record loRecord = (Record) loRecords.get(i);
        	if(loRecord.getFieldValue("RecordType") != null && loRecord.getUser() != null &&
        			((String) loRecord.getFieldValue("RecordType")).trim().equals("ApproveRecord")        			)
        	{
        		Log.customer.debug("***%s***::updateRecordRealUser::Setting Real User %s", msCN, loRecord.getUser().getName().getPrimaryString());
        		loRecord.setRealUser("ReceiptImport");
        	}
        }
    }
    
    public void checkFieldParsingErrors()
    		throws BuysenseXMLImportException
    {
    	try
    	{
	    	List tempVectorOfErrors = super.getErrorList();
	        StringBuffer lsbErr = new StringBuffer();
	        String lsSeverity = null;
	
	        for (int i = 0; i<tempVectorOfErrors.size(); i++)
	        {
	            if(i == 0)
	            {
	            	/* Skip Severity for Receiving import. Throw exception for any parsing errors */
	                lsSeverity = ( String ) tempVectorOfErrors.get(0) ;
	            }
	            else
	            {
	                /* Accumulate Errors */
	                String tempString = (String) tempVectorOfErrors.get(i);
	                lsbErr.append(tempString);
	                lsbErr.append(";") ;
	            }
	        }
	        if(lsbErr.length() > 0)
	        {
	           throw new BuysenseXMLImportException(lsbErr.toString(), ERR_CODE_SKIP_RETRY);
	        }
    	}
        catch (BuysenseXMLImportException loBuyException)
        {
        	throw loBuyException ;
        }
        catch (Exception loException)
        {
        	throw new BuysenseXMLImportException("Error: Exception while parsing errors: " +
                                                 loException.getMessage()) ;
        }

    }
    
    public ConditionResult checkValidationErrors()
    {
    	msField = null;
    	ConditionResult loCondResult = null;
    	String[] lsCheckFields = {"Date", "NumberAccepted", "NumberRejected", "NumberPreviouslyAccepted", "AmountAccepted"};
        
    	for (int i = 0; i < lsCheckFields.length; i++)
        {
    		loCondResult = ValueSourceUtil.evaluateAndExplainConstraints((ReceiptItem)moDetail, lsCheckFields[i], "__validity", ((ReceiptItem)moDetail).getObjectValidationGroup(), null, null, null);
    		if(loCondResult != null)
    		{
    			msField = lsCheckFields[i];
    			return loCondResult;
    		}
        }
    	return loCondResult;
    }
    
    public String getFinalErrorMsg(ConditionResult condResult)
    {
    	String lsErrorMsg = "";
    	String lsErrorStartMsg = "Cannot process Receipt Line #";
    	String lsErrorEndMsg = " because ";
    	
    	List lsErrorMsgs = ListUtil.arrayToList(condResult.getErrors());
    	for (int i=0; i < lsErrorMsgs.size() ; i++)
    	{
    		lsErrorMsg = lsErrorMsg + (String) lsErrorMsgs.get(i);
    	}

    	if(!StringUtil.nullOrEmptyOrBlankString(lsErrorMsg))
    	{
    		FieldProperties loFieldProps = (FieldProperties)((ReceiptItem)moDetail).getFieldProperties(msField);
    		lsErrorMsg = lsErrorStartMsg + ((ReceiptItem)moDetail).getNumberInCollection() + lsErrorEndMsg + lsErrorMsg.replaceFirst("Value", loFieldProps.getLabel());
    	}
    	return lsErrorMsg;
    }
}
