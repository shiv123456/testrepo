package config.java.ams.custom;

import ariba.approvable.core.Folder;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseVector;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import ariba.purchasing.core.POLineItem;
import ariba.purchasing.core.PurchaseOrder;
import ariba.purchasing.core.Requisition;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

/**
 * @author Jackie Bhadange
 * @version 1 DEV
 * @reference CSPL-5836
 * @see Date 02-02-2014
 * @Explanation Added trigger to copy labels from req to order when order is generated (status changed to ordering). It is called from POExtrinsicFields.aml
 * 
 */
public class BuysenseSetLabelsOnOrder extends Action
{

    public void fire(ValueSource object, PropertyTable params)
    {
        Log.customer.debug("BuysenseSetLabelsOnOrder::passed object %s ", object);
        BaseVector lvPOLineItems = null;
        POLineItem loPOLineItem = null;
        Requisition loReq = null;
        PurchaseOrder loPO = null;

        try
        {

            if (object != null && object instanceof PurchaseOrder)
            {
                loPO = (PurchaseOrder) object;
                lvPOLineItems = (BaseVector) loPO.getFieldValue("LineItems");

                // retrieve requisition
                if (lvPOLineItems.size() > 0)
                {
                    loPOLineItem = (POLineItem) lvPOLineItems.get(0);
                    loReq = (Requisition) loPOLineItem.getFieldValue("Requisition");
                }
                if (loReq != null) // fetch labels and apply to the order
                {
                    String sQuery = "select Folder from ariba.approvable.core.Folder JOIN ariba.approvable.core.FolderItem AS FolItem USING Folder.Items JOIN ariba.approvable.core.Approvable AS Appr USING FolItem.Item where Appr.UniqueName=\'" + loReq.getUniqueName() + "\' AND Folder.\"Type\"=1";

                    Log.customer.debug("BuysenseSetLabelsOnOrder::  req = %s, previous sql = %s", sQuery, loReq);

                    AQLQuery aqlQuery = AQLQuery.parseQuery(sQuery);
                    Partition partition = Base.getService().getPartition("pcsv");
                    AQLOptions aqlOptions = new AQLOptions(partition);
                    AQLResultCollection aqlResults = Base.getService().executeQuery(aqlQuery, aqlOptions);

                    if (aqlResults.getErrors() == null) // no errors
                    {
                        while (aqlResults.next())
                        {
                            BaseId baseIdFolder = aqlResults.getBaseId(0);
                            Folder folderObj = (Folder) Base.getSession().objectFromId(baseIdFolder);

                            // check if label is already not applied to this req.
                            // No need to check that in this case as we are applying labels as soon as the order is generated (ordering status)
                            folderObj.addItem(loPO.getBaseId());
                            folderObj.save();
                            Log.customer.debug("BuysenseSetLabelOnChangeReq::  newly added folder owner= %s, app= %s", folderObj.getOwner(), loPO.getUniqueName());
                        }
                    } // end of no errors in aql results
                }// end of req!=null
            } // end of obj not null
        } // end of try
        catch (Exception e)
        {
            Log.customer.debug("BuysenseSetLabelOnChangeReq::  exception in processing %s", e.getMessage());
        }
    }
}
