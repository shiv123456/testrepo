package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.htmlui.eform.wizards.ARBFormController;
import ariba.htmlui.fieldsui.wizards.ARBWizardContext;
import ariba.htmlui.fieldsui.wizards.ARBWizardFrame;
import ariba.ui.aribaweb.core.AWRequest;
import ariba.ui.aribaweb.util.Log;
import ariba.user.core.User;
import ariba.util.core.StringUtil;

public class BuysenseARBFormController extends ARBFormController
{

    public ARBWizardFrame getFirstFrame()
    {

        ARBWizardContext context = getContext();
        if (context.getSession() != null)
        {

            AWRequest request = context.getSession().request();
            String sLinkProfileID = request.formValueForKey("profid");
            Log.customer.debug( "BuysenseARBFormController.getFirstFrame() sLinkProfileID: " + sLinkProfileID);

            User user = (User) Base.getSession().getEffectiveUser();
            Log.customer.debug( "BuysenseARBFormController.getFirstFrame() user: " + user);

            BuysenseEformDirectAccessUtil loUserProfObj = BuysenseEformDirectAccessUtil.getInstance();
            if (!StringUtil.nullOrEmptyOrBlankString(sLinkProfileID))
                loUserProfObj.putProfId(user.getUniqueName(), sLinkProfileID);
            
        }
        return super.getFirstFrame();
    }

}
