/*







    Responsible: ryeliset



*/







/* 08/11/2003: Updates for Ariba 8.0 (Jeff Namadan) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.base.core.Base;

/* Ariba 8.0: Replaced ariba.common.core.nametable.NamedObjectNameTable) */
import ariba.base.core.aql.AQLNameTable;

/* Ariba 8.0: Replaced ariba.util.core.Util */
import ariba.util.log.Log;

import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLCondition;




/* Ariba 8.0: Replaced NamedObjectNameTable */
public class COVAField4NameTable extends AQLNameTable

{
    
    public void addQueryConstraints (AQLQuery query,
                                     String field, String pattern)
    
    
    
    {
        
        Log.customer.debug("COVAField4NameTable: Entered the addQueryConstraints");
        
        Log.customer.debug("The field : %s ",field);
        // The Query right now has a list of all Buysensefieldtabledata objects.
        // set new AQL condition, FieldNumber = Field4
        
        super.addQueryConstraints(query, field, pattern);
        
        beginSystemQueryConstraints(query);
        
        AQLCondition  fieldNumberCond =
            AQLCondition.buildEqual(query.buildField("FieldNumber"),"Field4");
        
        query.and(fieldNumberCond);
        
        // get User
        //Ariba 8.1: Modified logic to get the effectiveUser as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        ariba.user.core.User effectiveUser = (ariba.user.core.User)Base.getSession().getEffectiveUser();
        ariba.common.core.User user = ariba.common.core.User.getPartitionedUser(effectiveUser,Base.getSession().getPartition());
        
        Log.customer.debug("This is the user: %s ", user);
        
        // get User's Client Name
        
        String clientName = (String)user.getDottedFieldValue("ClientName.ClientName");
        Log.customer.debug("This is the client name : %s ",clientName); 
        
        // set new AQL condition, ClientName's ClientName
        AQLCondition clientNameCond = 
            AQLCondition.buildEqual(query.buildField("ClientName"),clientName);
        
        query.and(clientNameCond);
        
        endSystemQueryConstraints(query);
        
    }
    
    
    
}



