/************************************************************************************
 * Author:  Richard Lee
 * Date:    January 10, 2005
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 4/6/2005         Richard Lee            ER 116 - Disallow ASCII special characters
 *						     						except hyphen and dash
 *                                                  when creating new BuysenseOrg
 *
 * @(#)UserInputValidation.java     1.0 01/10/2005
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/

package config.java.ams.custom;

//import ariba.basic.util.Log;
import ariba.util.log.Log;
import ariba.base.core.Base;
import ariba.base.fields.*;
import ariba.util.core.*;

//81->822 changed ConditionValueInfo to ValueInfo
public class UserInputValidation extends Condition
{
    private static final ValueInfo parameterInfo[];
    String msMessage = null;

    public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
    {
        Log.customer.debug("Calling UserInputValidation for new BuysenseOrg name.");
        if(!evaluate(value, params))
        {
            String retMsg = null;
            if(!StringUtil.nullOrEmptyOrBlankString(msMessage))
            {
                retMsg = msMessage;
                msMessage = null;
                return new ConditionResult(retMsg);
            }
            else
                return new ConditionResult(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SLDefaultMessage"));
        }
        return null;
    }

    public boolean evaluate(Object value, PropertyTable params)
    {
       String lsUserInput = (String)value;
       String lsFieldName = params.stringPropertyForKey("TargetValue");
       return validateFields(lsUserInput, lsFieldName);
    }

    protected boolean validateFields(String lsUserInput, String lsFieldName)
    {
	if(lsFieldName.equals("KeyID"))
	{
		if(StringUtil.nullOrEmptyOrBlankString(lsUserInput))
		{
			msMessage = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SLDefaultMessage");
  			return false;
 		}

                // 30 ASCII Special characters not allowed
		if(StringUtil.contains(lsUserInput, ".") ||
			StringUtil.contains(lsUserInput, ",") ||
			StringUtil.contains(lsUserInput, "!") ||
			StringUtil.contains(lsUserInput, "~") ||
			StringUtil.contains(lsUserInput, "`") ||
			StringUtil.contains(lsUserInput, "@") ||
			StringUtil.contains(lsUserInput, "#") ||
			StringUtil.contains(lsUserInput, "$") ||
			StringUtil.contains(lsUserInput, "%") ||
			StringUtil.contains(lsUserInput, "^") ||
			StringUtil.contains(lsUserInput, "&") ||
			StringUtil.contains(lsUserInput, "*") ||
			StringUtil.contains(lsUserInput, "(") ||
			StringUtil.contains(lsUserInput, ")") ||
			StringUtil.contains(lsUserInput, "+") ||
			StringUtil.contains(lsUserInput, "=") ||
			StringUtil.contains(lsUserInput, "{") ||
			StringUtil.contains(lsUserInput, "}") ||
			StringUtil.contains(lsUserInput, "|") ||
			StringUtil.contains(lsUserInput, "[") ||
			StringUtil.contains(lsUserInput, "]") ||
			StringUtil.contains(lsUserInput, "\\") ||
			StringUtil.contains(lsUserInput, ":") ||
			StringUtil.contains(lsUserInput, ";") ||
			StringUtil.contains(lsUserInput, "\"") ||
			StringUtil.contains(lsUserInput, "'") ||
			StringUtil.contains(lsUserInput, "<") ||
			StringUtil.contains(lsUserInput, ">") ||
			StringUtil.contains(lsUserInput, "?") ||
			StringUtil.contains(lsUserInput, "/"))
		{
			msMessage = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","BSODot");
	        	return false;
		}
	 }
         return true;
    }

    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    static
    {
        parameterInfo = (new ValueInfo[] {
            new ValueInfo("TargetValue", 0),
            new ValueInfo("Message", 0, Behavior.StringClass)
        });
    }
}

