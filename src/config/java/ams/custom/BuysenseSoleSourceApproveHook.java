package config.java.ams.custom;

import java.util.List;

import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableHook;
import ariba.approvable.core.ApprovalRequest;
import ariba.approvable.core.Comment;
import ariba.base.core.Base;
import ariba.util.core.Constants;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;
import ariba.util.log.Log;

public class BuysenseSoleSourceApproveHook implements ApprovableHook
{
    private static final List<Integer> NO_ERROR_RESULT = ListUtil.list(Constants.getInteger(0));

    public List<?> run(Approvable approvable)
    {
        Log.customer.debug("The BuysenseSoleSourceApproveHook.java called");
        boolean isActiveApprover = false;
        int iApprovedCount = 0;
        int iApprovalRequiredCount = 0;
        int iRequiredApprovalCount = 0;
        ariba.user.core.User loRequester = null;
        List<ApprovalRequest> vAllApprovalRequests = ListUtil.list();

        List approvalRequestsList = approvable.getApprovalRequests();
        
        for (int i = 0; i < (approvalRequestsList.size()); i++)
        {

            ApprovalRequest ar = (ApprovalRequest) approvalRequestsList.get(i);
            vAllApprovalRequests.add(ar);
            getAllDependencies(ar, vAllApprovalRequests);
        }

        for (int j = 0; j < vAllApprovalRequests.size(); j++)
        {
            ApprovalRequest ar = vAllApprovalRequests.get(j);
            if (ar.getApprovalRequired())
            {
                iApprovalRequiredCount = iApprovalRequiredCount + 1;
                Log.customer.debug("The count of iApprovalRequiredCount" + iApprovalRequiredCount);
            }
            
            if (ar.isApproved())
            {
                iApprovedCount = iApprovedCount + 1;
            }
            
            if (ar.isActive())
            {
                isActiveApprover = true;
            }
        }

        iRequiredApprovalCount = iApprovalRequiredCount - iApprovedCount;
        if (isActiveApprover && iRequiredApprovalCount == 1)
        {
            Comment loComment = new Comment(approvable.getPartition());
            loRequester = (ariba.user.core.User) approvable.getRequester();
            loComment.setBody(Fmt.Sil(Base.getSession().getLocale(), "buysense.eform", "BuysenseSoleSourceApproveAlertMessage"));
            loComment.setType(Comment.TypeApprove);
            loComment.setUser(loRequester);
            loComment.setDate(new ariba.util.core.Date());
            approvable.addComment(loComment);
            approvable.save();
        }

        return NO_ERROR_RESULT;
    }

    public static List<ApprovalRequest> getAllDependencies(ApprovalRequest approvalRequest, List<ApprovalRequest> vDependencies)
    {
        List vDependencies1 = (List) approvalRequest.getDependencies();
        if (vDependencies1 != null)
        {
            ApprovalRequest dependency;
            for (int i = 0; i < vDependencies1.size(); i++)
            {
                dependency = (ApprovalRequest) vDependencies1.get(i);
                if (!(vDependencies.contains(dependency) == true))
                {
                    vDependencies.add(dependency);
                }
                getAllDependencies(dependency, vDependencies);
            }
        }
        return vDependencies;
    }

}
