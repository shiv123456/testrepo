/*
    Copyright (c) 1996-2001 Buysense.
    Anup - June 2001
    ---------------------------------------------------------------------------------------------------

*/

/* 09/05/2003: Updates for Ariba 8.0 (David Chamberlain) */

/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom.HookEdits;

import ariba.approvable.core.Approvable;
import java.util.List;
// Ariba 8.0: Replaced deprecated ApprovableHook
import ariba.approvable.core.ApprovableHook;
import ariba.common.core.*;
import ariba.util.log.Log;
// Ariba 8.0: Deprecations, methods have changed packages.
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;


public class eVA_S000EVALITE_AMSReqSubmitHook implements ApprovableHook
{
    // Ariba 8.0: Replaced deprecated method
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));
    
    public List run (Approvable approvable)
    {
        Log.customer.debug("In eVA_S000EVALITE_AMSReqSubmitHook.java ");
        
        //Requester must always equal Preparer for the S000EVALITE Agency
        
        Object uPreparer = (User)(approvable.getFieldValue("Preparer"));
        Log.customer.debug("uPreparer= " + uPreparer);
        
        Object uRequester = (User)(approvable.getFieldValue("Requester"));
        Log.customer.debug("uRequester= " + uRequester);
        
        if (uPreparer == uRequester)
        {
            Log.customer.debug("In eVA_S000EVALITE_AMSReqSubmitHook.java Requester == Preparer ");
            return NoErrorResult;
        }
        else
        {
            Log.customer.debug("In eVA_S000EVALITE_AMSReqSubmitHook.java Requester != Preparer ");
            // Ariba 8.0: Replaced deprecated method
            return ListUtil.list(Constants.getInteger(-1),
                                     "Preparer and On Behalf Of must be the same.");
        }
        
        
    }
}
