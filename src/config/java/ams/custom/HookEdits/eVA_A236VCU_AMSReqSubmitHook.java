package config.java.ams.custom.HookEdits;

import ariba.approvable.core.ApprovableHook;
import ariba.approvable.core.Approvable;
import ariba.util.core.Vector;
import ariba.util.log.Log;
import ariba.util.core.ListUtil;
import ariba.util.core.Constants;
import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.purchasing.core.Requisition;
import ariba.purchasing.core.ReqLineItem;
import java.util.List;

/*
 * CSPL-540 Enable Foreign Currency for users in A236VCU
 *
 * Get the user ClientName and the currency code from the line item to build the UniqueName to
 * perform a look up in the BuysenseFieldDataTable. 
 * If a record match is found, it means this client is data setup for Foreign currency and the following 
 * is performed on each line item: 
 * 1. Get the Req.lineitem.Amount.currency.UniqueName and put it in LineItem.ReqLineFieldDefault3
 * 2. Get the Req.LineItem.Amount.Amount and put it in LineItem.ReqLineText2
 *
 */

public class eVA_A236VCU_AMSReqSubmitHook implements ApprovableHook
{
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));

    public List run (Approvable approvable)
    {
        Log.customer.debug("Calling eVA_A236VCU_AMSReqSubmitHook.java ");
        ReqLineItem loItem = null;
        String lsCurrencyCode = null;
        String lsUniqueName = null;
        String lsCurrencyAmt = null;
        ClusterRoot loFieldDataTable = null;
        java.math.BigDecimal loAmount = null;
        String lsClientUniqueName = null;
        Partition  loPartition = Base.getSession().getPartition();
        ariba.user.core.User RequesterUser = (ariba.user.core.User)approvable.getDottedFieldValue("Requester");
        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,approvable.getPartition());
        lsClientUniqueName = (String)RequesterPartitionUser.getDottedFieldValue("ClientName.UniqueName");
        Requisition loReq = (Requisition) approvable;
        String REQLINEFIELD3 = lsClientUniqueName + ":ReqLineField3:"; 
        Vector lvItems = (Vector) loReq.getLineItems();
        int liSize = lvItems.size();
        try
        {
            for(int j = 0; j < liSize; j++)
            {
                loItem = (ReqLineItem)lvItems.elementAt(j);
                lsCurrencyCode = (String) loItem.getDottedFieldValue("Amount.Currency.UniqueName");
                lsUniqueName = REQLINEFIELD3 + lsCurrencyCode;
                loFieldDataTable = Base.getService().objectMatchingUniqueName("ariba.core.BuysenseFieldDataTable", loPartition, lsUniqueName);
                if(loFieldDataTable != null);
                {
                    loItem.setFieldValue("ReqLineFieldDefault3", loFieldDataTable);
                    loAmount = (java.math.BigDecimal)loItem.getDottedFieldValue("Amount.Amount");
                    lsCurrencyAmt = (String)loAmount.toString();
                    loItem.setFieldValue("ReqLineText2Value", lsCurrencyAmt);
                }
            }
        }
        catch (Exception loEx)
        {
            return ListUtil.list(Constants.getInteger(-1), "Error setting Foreign Currency: " +
                            loEx.getMessage());

        }
        return NoErrorResult;
    }
}
