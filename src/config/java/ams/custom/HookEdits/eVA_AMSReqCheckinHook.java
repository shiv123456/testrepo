/*
    Copyright (c) 1996-2001 Buysense.
    Anup - June 2001
    ---------------------------------------------------------------------------------------------------

*/

/* 09/05/2003: Updates for Ariba 8.0 (David Chamberlain) */

/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom.HookEdits;

import ariba.approvable.core.Approvable;
import java.util.List;
import config.java.ams.custom.AMSReqSubmitHook;
// Ariba 8.0: Replaced deprecated ApprovableHook
import ariba.approvable.core.ApprovableHook;
import ariba.base.core.Base;
import ariba.util.log.Log;
// Ariba 8.0: Deprecations, methods have changed packages.
import ariba.util.core.Constants;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;


public class eVA_AMSReqCheckinHook implements ApprovableHook
{
    // Ariba 8.0: Replaced deprecated method
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));
    //e2e
    private static final boolean DEBUG = true ;
    // Ariba 8.0: Replaced deprecated method
    private static final List  NO_ERROR_RESULT = ListUtil.list( Constants.getInteger( 0 ) ) ;
    //e2e
    public List run (Approvable foApprovable)
    {
        Log.customer.debug("In the eVA_AMSReqCheckinHook.java");
        
        /*
        eVA001 eVAVal = new eVA001();
            List results = eVAVal.requisitionValidations(approvable);
            // Ariba 8.1: Replaced deprecated method
            int eVAerrorCode = ((Integer)results.get(0)).intValue();
            if(eVAerrorCode<0)
            {   //in the case of error return vector.
                 return results;
        }
 
        return NoErrorResult;
        */
        
        //e2e
        List loRetVector = NO_ERROR_RESULT ;
        
        if ( DEBUG )
        {
            Log.customer.debug( "Starting AMSReqCheckinHook..." ) ;
        }
        
        /* Change Order functionality is invoked here
         * If no error is encountered, e2e logic follows preceded by 
         * resetting of the return error List
         */
        ChangeOrderProcessor loCOP = new ChangeOrderProcessor() ;
        loRetVector = loCOP.run( foApprovable ) ;
        // Ariba 8.1: Replaced deprecated method
        int errorCode = ((Integer)loRetVector.get(0)).intValue();
        if( errorCode<0 )
        {
            // Ariba 8.1: Replaced deprecated method
            Log.customer.debug( "Change Order logic Failed with error: " + loRetVector.get(1) ) ;
            return loRetVector ;
        }
        else
        {
            loRetVector = NO_ERROR_RESULT ;
        }
        
        loRetVector = eVA_AMSReqSubmitHook.validateReq( foApprovable ) ;
        
        //Added by srini for CSPL-963 for multiple warning messages.
        /**
        *There is a limitation in ariba that we cannot return more than one warning message at a time. For example If there are two
        *warning messages need to return, system returns first warning message successfully and if users hits on submit,
        *system allows users to submit the REQ successfully without showing up the second warning message.
        *To overcome this issue and after long analysis of lengthy codes came to know we cannot insert checkAdHocPCO() inside validateReq()
        *method because there are numerous conditions for each warning message and each message is returning independently.
        *So that we came to decision before returning a existing warning message we are checking for any warning message exist already
        *and we are appending checkAdHocPCO() to that message if checkAdHocPCO() satisfies else it will return existing message.
		*Future warning messages will work as below code. Developer just need to check his condition in else if and need to append his warning
		*message to existing warning message as like we are doing now
        */
       	int liNumbering = 0;
       	String lsFormattedString = null;
       	String lsSpace = "    ";
       	
        String lsCheckSensitiveData = null;
		try {
			lsCheckSensitiveData = AMSReqSubmitHook.checkSensitiveDataPattern(foApprovable);
			Log.customer.debug("The message to print is "+lsCheckSensitiveData);
		} catch (Exception e) {
			Log.customer.debug(e.getMessage());
			e.printStackTrace();
		}
        if(((Integer)loRetVector.get(0)).intValue() < 0)
        {
        	return loRetVector ;
        }
        else if(eVA_AMSReqSubmitHook.checkAdHocPCO(foApprovable) || eVA_AMSReqSubmitHook.getAttachmentsSize(foApprovable) || 
               (!loRetVector.isEmpty() && ((Integer)loRetVector.get(0)).intValue() == 1) || lsCheckSensitiveData !=null)
        {
        	StringBuffer lsbTempStr = new StringBuffer();
        	//Added by Srini for CSPL-1033
        	if(eVA_AMSReqSubmitHook.getAttachmentsSize(foApprovable))
        	{
        		liNumbering = liNumbering + 1;
       			lsFormattedString = eVA_AMSReqSubmitHook.getFormattedNumberString(liNumbering);
       			lsbTempStr.append(lsFormattedString);
       			lsbTempStr.append(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","AttachmentBeginMessage"));
       			lsbTempStr.append(eVA_AMSReqSubmitHook.getAttachmentWarningMSG(foApprovable));
       			lsbTempStr.append(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","AttachmentEndMessage"));
        	}
        	if(lsCheckSensitiveData !=null)
        	{
            	liNumbering = liNumbering + 1;
            	lsFormattedString = eVA_AMSReqSubmitHook.getFormattedNumberString(liNumbering);
            	lsbTempStr.append(lsFormattedString);
            	lsbTempStr.append(lsCheckSensitiveData);
            }
        	if(eVA_AMSReqSubmitHook.checkAdHocPCO(foApprovable))
            {
        		liNumbering = liNumbering + 1;
        		lsFormattedString = eVA_AMSReqSubmitHook.getFormattedNumberString(liNumbering);
        		lsbTempStr.append(lsFormattedString);
        		lsbTempStr.append(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","CheckAdHocPCOMessage"));
            }  
        	if(!loRetVector.isEmpty() && ((Integer)loRetVector.get(0)).intValue() == 1)
        	{
        		liNumbering = liNumbering + 1;
        		lsFormattedString = eVA_AMSReqSubmitHook.getFormattedNumberString(liNumbering);
        		if(lsbTempStr != null)
        		{
        			lsbTempStr.append(lsSpace);
        		}
        		lsbTempStr.append(lsFormattedString);
        		lsbTempStr.append((String) loRetVector.get(1));
        	}
            loRetVector = ListUtil.list(Constants.getInteger(1), lsbTempStr.toString());
        }
        else
        {
        	loRetVector = NO_ERROR_RESULT ;
        }
        
        
        if ( DEBUG )
        {
            Log.customer.debug( "Finished AMSReqCheckinHook." ) ;
        }
        //Added by Srini for CSPL-1033
        eVA_AMSReqSubmitHook.addHeaderComment(foApprovable);
        
        return loRetVector ;
        //e2e
        
    }
}
