package config.java.ams.custom.HookEdits;

import java.util.List;

import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableHook;
import ariba.base.core.BaseId;
import ariba.common.core.UserProfile;
import ariba.common.core.UserProfileDetails;
import ariba.user.core.Group;
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;
import ariba.util.log.Log;

public class BuysenseUPSubmitHook implements ApprovableHook
{
    String sClassName = "BuysenseUPSubmitHook";
    String sErrorMessage = "The eVA Receive Role cannot be removed.";
    List<String> loResults = ListUtil.list();
    public List<?> run(Approvable approvable)
    {
        if(approvable !=null && approvable instanceof UserProfile)
        {
            UserProfile up = (UserProfile) approvable;
            UserProfileDetails upDetails = up.getDetails();
            ariba.user.core.UserProfileDetails userUPDetails = upDetails.getUserProfileDetails();
            List<?> loGroups = userUPDetails.getGroups();
            if(haseVAReceiveGroup(loGroups))
            {
                return ListUtil.list(Constants.getInteger(0));
            }
            else
            {
                Log.customer.debug(sClassName+" ERRORS in UP submit hook");
                return ListUtil.list(Constants.getInteger(-1),sErrorMessage);
            }
        }
        return loResults;
    }

    private boolean haseVAReceiveGroup(List<?> loGroups)
    {
        String seVAReceiveGroupName = "eVA-Receive";
        for(int i=0;i<loGroups.size();i++)
        {
            Group group = (Group) ((BaseId)loGroups.get(i)).get();
            if(group.getUniqueName().equalsIgnoreCase(seVAReceiveGroupName))
            {
             return true;
            }
        }
        return false;    
    }

}
