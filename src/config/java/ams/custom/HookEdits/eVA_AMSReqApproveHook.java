/*
    Copyright (c) 1996-1999 Ariba, Inc.
    All rights reserved. Patents pending.

   Falahyar March 2000
*/

// rlee, 10/24/03 ER18 and Prod 216 - Copying and using expired pcards and PCard related issues

// jnamadan, 10/29/03: eProcurement CR 38 - Modified to check the PrePrintedText table for the ALL clientname. Issue
// an error if it is not present

// rlee, 12/24/03: Prod SPL 247 - No edit to check if user is disassociated with the pcard.  Added check to see if PCardUsed is
// still associated with the Requester.

/* 01/16/2004: Updates for Ariba 8.1 (Jeff Namadan)
   Replaced all Util.vector()'s with ListUtil.vector()
   Replaced all Util.getInteger()'s with Constants.getInteger()
*/

/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/

package config.java.ams.custom.HookEdits;

// Ariba 8.1: Deprecations, methods have changed packages.
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.util.core.Constants;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;
// Ariba 8.1: Replaced deprecated ApprovableHook
import ariba.approvable.core.ApprovableHook;
import java.util.List;
import config.java.ams.custom.AMSReqSubmitHook;
import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.common.core.PCard;
import ariba.approvable.core.Approvable;
import ariba.base.core.Base;

public class eVA_AMSReqApproveHook implements ApprovableHook
{
    private static final List  NO_ERROR_RESULT = ListUtil.list( Constants.getInteger( 0 ) ) ;
    private static final String ST_EDIT_APPROVABLE_PERMISSION = "EditApprovable";
    private static final boolean DEBUG = true ;

    public List run (Approvable foApprovable)
    {
        List loRetVector = NO_ERROR_RESULT ;

        Log.customer.debug("In the eVA_AMSApproveHook.java ");

        loRetVector = approverReqCheck( foApprovable ) ;
        //Added by srini for CSPL-1033.
        int liNumbering = 0;
       	String lsFormattedString = null;
       	String lsSpace = "    ";

        if(((Integer)loRetVector.get(0)).intValue() < 0)
        {
        	return loRetVector ;
        }
        else if(eVA_AMSReqSubmitHook.getAttachmentsSize(foApprovable) || checkRegistrationTypeMatch((Requisition) foApprovable) != null ||
        		(!loRetVector.isEmpty() && ((Integer)loRetVector.get(0)).intValue() == 1))
        {
        	StringBuffer lsbTempStr = new StringBuffer();
        	//Added by Srini for CSPL-1033
        	if(eVA_AMSReqSubmitHook.getAttachmentsSize(foApprovable))
        	{
        		liNumbering = liNumbering + 1;
       			lsFormattedString = eVA_AMSReqSubmitHook.getFormattedNumberString(liNumbering);
       			lsbTempStr.append(lsFormattedString);
       			lsbTempStr.append(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","AttachmentBeginMessage"));
       			lsbTempStr.append(eVA_AMSReqSubmitHook.getAttachmentWarningMSG(foApprovable));
       			lsbTempStr.append(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","AttachmentEndMessage"));
        	}
            //SRINI: START: SEV changes for SupplierLocation
        	String lsRegistrationTypeMatch = checkRegistrationTypeMatch((Requisition) foApprovable);
        	if(lsRegistrationTypeMatch != null)
        	{
        		liNumbering = liNumbering + 1;
       			lsFormattedString = eVA_AMSReqSubmitHook.getFormattedNumberString(liNumbering);
       			lsbTempStr.append(lsFormattedString);
       			lsbTempStr.append(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","RegTypeMismatch"));
       			lsbTempStr.append(lsRegistrationTypeMatch);
        	}
            //SRINI: END: SEV changes for SupplierLocation
        	if(!loRetVector.isEmpty() && ((Integer)loRetVector.get(0)).intValue() == 1)
        	{
        		liNumbering = liNumbering + 1;
        		lsFormattedString = eVA_AMSReqSubmitHook.getFormattedNumberString(liNumbering);
        		if(lsbTempStr != null)
        		{
        			lsbTempStr.append(lsSpace);
        		}
        		lsbTempStr.append(lsFormattedString);
        		lsbTempStr.append((String) loRetVector.get(1));
        	}
            loRetVector = ListUtil.list(Constants.getInteger(1), lsbTempStr.toString());        	
        }
        else
        {
        	loRetVector = NO_ERROR_RESULT ;
        }
        
        if ( DEBUG )
        {
            Log.customer.debug( "Finished eVA_AMSReqApproveHook" ) ;
        }

        //Added by Srini for CSPL-1033
        eVA_AMSReqSubmitHook.addHeaderComment(foApprovable);
        
        return loRetVector ;
    }

    public static List approverReqCheck( Approvable foApprovable )
    {

        List loRetVector = NO_ERROR_RESULT ;
        // start rlee Check PCard expiration, PCard association, and Approver edit permission
        loRetVector = PCardExpirationEdit(foApprovable);
        // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
        if ( ((Integer)loRetVector.get( 0 ) ).intValue() == -1 )
        {
            return loRetVector ;
        }
        // end rlee Check PCard expiration date, PCard association, and Approver edit permission

        // Check to see if clientname ALL entry exists on the PrePrintedText table
        loRetVector = eVA_AMSReqSubmitHook.checkPrePrintedText(foApprovable);
        // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
        if ( ((Integer)loRetVector.get( 0 ) ).intValue() == -1 )
        {
            return loRetVector ;
        }

        return loRetVector;
    }
    /* rlee, PCardExpirationEdit validates PCard being used for this Requisition.
    If the UsePCardBool check box has not been checked meaning NOT using PCard for this Requisition, return no error
    Else check if PCard use exist; if exist, check each of the Requester's PCards by a for loop.
    If at least one of the PCards matches the Pcard Used, set lbDeleted flag to FALSE.
    Return NO ERROR if the PCard Used exist AND Has not expired AND is still associated with Requester.

    If the PCard Used is not valid for any reasons, different error message will appear depending
    on whether the Approver has Edit Approable Permission and whether the Requester has any other
    valid PCards.
    */
    /* paluri, CSPL-2433, 12-Jan-2011. References of CardNumber filed is changed to UniqueName, as CardNumber is Encrypted in 9r1,
     * causing class cast exception.
     */
    private static List PCardExpirationEdit ( Approvable foApprovable)
    {
        boolean lbDeleted = true;
        boolean lbEditFlag = false;
        boolean lbUserHasPCard = false;
        Date ldPcardDate;
        Boolean lbUsePCardBool = (Boolean)foApprovable.getDottedFieldValue("UsePCardBool");
        //Ariba 8.1: Added class definition to decipher between ariba.common.core.User class and new ariba.user.core.User class.
        ariba.user.core.User loApprover = (ariba.user.core.User)Base.getSession().getEffectiveUser();
        PCard loUsingPCard = (PCard)foApprovable.getDottedFieldValue("PCardToUse");
        ariba.user.core.User loRequester = (ariba.user.core.User)foApprovable.getDottedFieldValue("Requester");
        ariba.common.core.User loCommRequester = ariba.common.core.User.getPartitionedUser(loRequester,Base.getSession().getPartition());
        List lvRequesterPCards = loCommRequester.getPCardsVector();
        int liPCardSize = lvRequesterPCards.size();
        PCard loPCardCheck = null;


        if(!(lbUsePCardBool.booleanValue()))
        {
          return NO_ERROR_RESULT;
        }
        else
        {
        	
            if (loUsingPCard != null)
            {
                String lsPCardNum = (String)loUsingPCard.getFieldValue("UniqueName");
                if (liPCardSize>0)
                {
                    for(int i=0;i<liPCardSize;i++)
                    {
                        loPCardCheck = (PCard)lvRequesterPCards.get(i);
                        //SRINI: Added null check for PCard unique name
                        if (lsPCardNum !=null && loPCardCheck.getUniqueName() != null && lsPCardNum.equals((String)loPCardCheck.getFieldValue("UniqueName")))
                        {
                        	// Sarath: CSPL-1109 -- Added condition to check the PCard being selected in Req is active or not.
                        	if(loPCardCheck.getActive())
                            {
                         	   lbDeleted = false;                    	   
                            }                            
                        }
                    }
                }
                ldPcardDate=loUsingPCard.getExpirationDate();
                if (ldPcardDate!=null)
                {
                    int result=0;
                    result = Date.getNow().compareTo(ldPcardDate);

                    if ((result<=0) && (lbDeleted == false))
                    {
                        return NO_ERROR_RESULT;
                    }
                    Log.customer.debug("The PCard used is either expired or disassociated with the Requester." );
                }
                else
                {
                    /*if Pcard expiration date is null, assume NO expiration date.
                    Return no error if PCard used is still associated with Requester.
                    */
                    if(lbDeleted == false)
                    {
                        return NO_ERROR_RESULT;
                    }
                }
            }
        }

        // If above PCard has expired or disassociated from Requester, check if approver has Edit Approvable role
        if (loApprover.hasPermission(ST_EDIT_APPROVABLE_PERMISSION))
        {
            // Approver has edit approvable:
            lbEditFlag = true;
        }
        else
        {
            return ListUtil.list( Constants.getInteger( -1 ),
                  (Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","ApproverCannotEdit")));
        }
        //Since Approver has Edit Approvable role, check Requester's pcard for a valid pcard.

        // check if requester has at least one valid PCard
        lbUserHasPCard = checkRequesterHasPCard(foApprovable);

        // If approver has Edit Approvable role and Requester has other valid Pcard,
        // ask the Approver to select a valid PCard
        if ((lbUserHasPCard) && (lbEditFlag))
        {
         return ListUtil.list( Constants.getInteger( -1 ),
             (Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","MakeAnotherSelection")));
        }
        else
        {
         return ListUtil.list( Constants.getInteger( -1 ),
             (Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","ApproverDeselectOrDeny")));
        }
    }

    private static boolean checkRequesterHasPCard (Approvable foApprovable)
    {
        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        ariba.user.core.User RequesterUser = (ariba.user.core.User)foApprovable.getDottedFieldValue("Requester");
        //rgiesen dev SPL #39
        //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,foApprovable.getPartition());
        List lvRequesterPCards = RequesterPartitionUser.getPCardsVector();
        int liPCardSize = lvRequesterPCards.size();
        PCard loPCardCheck = null;
        Date ldPcardDate=null;

        // Check if Requester has any other valid PCard, the one being used has expired
        if (liPCardSize>0)
        {
            //If the current user has one (or more) valid PCards assigned to them, return true, which makes the field visible
            for(int i=0;i<liPCardSize;i++)
            {
                //Get the individual PCard
                // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                loPCardCheck = (PCard)lvRequesterPCards.get(i);

                //Check to see if at least one of their PCards is of type 2
                // rlee SPL 1057, added CardType 3 allowed
                if ((loPCardCheck.getCardType()==2)|| (loPCardCheck.getCardType()==3))
                {
                    //Check if the expiration date is past today's date
                    //If it is past today's date, it is not a valid PCard
                    ldPcardDate=loPCardCheck.getExpirationDate();

                    if (ldPcardDate!=null)
                    {
                        int result=0;
                        result = Date.getNow().compareTo(ldPcardDate);

                        //result <0 if the current date is less than expDate
                        if (result<=0)
                        {
                          // Requester has at least one valid PCard
                          return true;
                        }
                    }
                }
            }//for
        }
        Log.customer.debug("rlee 188 return false in the eVA_AMSApproveHook.java ");
        return false;
    }// end boolean checkRequesterHasPCard

    //SRINI: START: SEV changes for SupplierLocation
    public String checkRegistrationTypeMatch(Requisition req)
    {
    	List loReqLines = req.getLineItems();
    	String lsLineNumbers = null;
    	for (int i = 0; i < loReqLines.size(); i++)
        {
    		ReqLineItem loReqLine = (ReqLineItem) loReqLines.get(i);
        	if((AMSReqSubmitHook.isLocationChanged(loReqLine) || loReqLine.getOldValues() == null) && (loReqLine.getFieldValue("RgstTypeCd") != null &&
        	   loReqLine.getDottedFieldValue("SupplierLocation.RgstTypeCd") != null &&
        	   !((String)loReqLine.getDottedFieldValue("SupplierLocation.RgstTypeCd")).equals((String) loReqLine.getDottedFieldValue("RgstTypeCd"))))
        	{
        		lsLineNumbers = getFormattedLineNumber(loReqLine.getNumberInCollection(), lsLineNumbers);
        	}
        }
    	return lsLineNumbers;
    }

    public String getFormattedLineNumber(int lineNumber, String lineNumbers)
    {
    	StringBuffer lsbTemp = new StringBuffer();
    	if(lineNumbers != null)
    	{
    		lsbTemp.append(lineNumbers + "," + lineNumber);
    	}
    	else
    	{
    		lsbTemp.append(lineNumber);
    	}
    	return lsbTemp.toString();
    }
    //SRINI: END: SEV changes for SupplierLocation

}
