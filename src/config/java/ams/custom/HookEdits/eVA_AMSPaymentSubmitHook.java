/*
    Copyright (c) 1996-2001 Buysense.
    Anup - June 2001
    ---------------------------------------------------------------------------------------------------
WA ST SPL # 475/Baseline Dev SPL # 229 - Changed references to PaymentAccountings to Accountings.PaymentAccountings.

*/

/* 09/05/2003: Updates for Ariba 8.0 (David Chamberlain) */

/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom.HookEdits;

import ariba.approvable.core.Approvable;
import java.util.List;
import config.java.ams.custom.*;
import ariba.base.core.*;
import ariba.util.log.Log;
// Ariba 8.0: Replaced deprecated ApprovableHook
import ariba.approvable.core.ApprovableHook;
//import ariba.approvable.core.ApprovableHook;
// Ariba 8.0: Deprecations, methods have changed packages.
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;


public class eVA_AMSPaymentSubmitHook implements ApprovableHook
{
    // Ariba 8.0: Replaced deprecated method
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));
    public List run (Approvable approvable)
    {
        Log.customer.debug("In the WA payment Submit Hook. ");
        
        //Call the Business Rule engine to validate all chooser field selections
        //if we get a -1 that means we failed the validation. so we return without proceeding
        
        List retVector = BuysenseRulesEngine.runPaymentFilteringRules(approvable);
        // Ariba 8.1: Replaced deprecated method
        if (((Integer)retVector.get(0)).intValue() == -1)
            return retVector;
        
        BaseVector pvlines = (BaseVector)approvable.getFieldValue("PaymentItems");    
        
        // Also set ActgLineNumber for each accounting line. - Dev SPL #165 
        for(int i=0;i<pvlines.size();i++)
        {
            Log.customer.debug("About to do the accounting edit for payment line %s",
                           i);
            // Ariba 8.1: Replaced deprecated method
            BaseObject paymentItem = (BaseObject)pvlines.get(i);
            List paymentAccountings = (List)paymentItem.getDottedFieldValue("Accountings.PaymentAccountings");
            setActgLineNumber(paymentAccountings);  
        }
        
        // Invoice Number field cannot be blank - ST SPL # 118
        String invoiceNumberValue = (String)(approvable.getFieldValue("InvoiceNumber"));
        Log.customer.debug("Hello washington - in invoice # validation --> %s",
                       invoiceNumberValue);
        // Ariba 8.0: Replaced deprecated method
        if ((invoiceNumberValue == null) 
                || (invoiceNumberValue.trim().equals("")))
            return ListUtil.list(Constants.getInteger(-1),
                                     "Invoice Number field cannot be blank.");
        
        // Set Payment Eform Title - ST SPL # 118
        String EformTitle = (String)(approvable.getFieldValue("Name"));
        if (EformTitle.trim().equals("Untitled Payment Eform"))
        {
            String purchaseOrderNumber = (String)(approvable.getDottedFieldValue("Order.UniqueName"));
            String supplierName = (String)(approvable.getDottedFieldValue("Supplier.Name"));     
            String invoiceNumber = (String)(approvable.getFieldValue("InvoiceNumber"));
            approvable.setFieldValue("Name",
                                     (String)(purchaseOrderNumber + supplierName + invoiceNumber));
            
        }
        
        return NoErrorResult;
        
    }
    
    
    private void setActgLineNumber(List paymentAccountings)
    {
        for(int i=0;i<paymentAccountings.size();i++)
        {
            // Ariba 8.1: Replaced deprecated method
            BaseObject paymentAcctg = (BaseObject)paymentAccountings.get(i);
            paymentAcctg.setFieldValue("ActgLineNumber", new Integer(i+1));
        }
    }
    
    
}
