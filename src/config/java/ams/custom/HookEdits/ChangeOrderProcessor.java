/************************************************************************************/
// Name:		     ChangeOrderProcessor
// Description:
// Author:	David Leonard, Ariba, Inc.
// Date:	Sept. 4, 2002
// Revision History:
//
//	Date	          Responsible		     Description
//	-------------------------------------------------------------------------------
//   09/4/2002        David Leonard, Ariba   SubmitHook to allow req submission/order
//                                             creation when only comments change.
//   12/17/03      Anup Hiranandani       ST SPL # 698 - Determine solicitation Line from flag.
//
// Copyright of Ariba, Inc., 1996-2002	     All Rights Reserved
/*************************************************************************************/

/*
   rlee, 3/3/2003, eProc ST SPL 895 - Order does not "version" if Pcard is changed
   Added Pcard fields to the FieldListContainer to check for PCard fields change as well
*/ 
/* 09/03/2003: Updates for Ariba 8.0 (David Chamberlain) */

/* 07/01/2013, Pavan Aluri: CSPL-5309. Modified getHeaderFields() method to exclude ReqHeadCB2Value header field for comparison.
   Also, added extra argument to setHeaderFieldsHaveChangedField() method to pass DummyCommentField or InternalChangeOrderFlag field name based on
   external change order or internal change order respectively. */

package config.java.ams.custom.HookEdits;

import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.core.ClusterRoot;

import ariba.approvable.core.Approvable;
import ariba.approvable.core.AttachmentWrapper;
import ariba.app.util.Attachment;
import ariba.approvable.core.Comment;

import ariba.procure.core.ProcureLineItem;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;

// Ariba 8.0: Replaced deprecated ApprovableHook
import ariba.approvable.core.ApprovableHook;
//import ariba.approvable.core.ApprovableHook;

// Ariba 8.0: Deprecations, methods have changed packages.
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;

/* Ariba 8.0: Replaced ariba.util.core.Util */
//import ariba.util.core.Util;
import java.util.List;

import ariba.util.log.Log;

import config.java.ams.custom.FieldListContainer;

import java.text.SimpleDateFormat;

import java.util.Calendar;

public class ChangeOrderProcessor implements ApprovableHook
{
    public static final String ClassName = "config.java.ams.ChangeOrderProcessor";
    // rlee ST SPL 895 modified FieldListContainer to include PCard fields
    public static final List mvHeaderFields = FieldListContainer.getHeaderPCardFields() ;
    //iib
    // Ariba 8.0: Replaced deprecated method
    private final List NoErrorResult = ListUtil.list(Constants.getInteger(NoError));
    /*
        This submit hook sets a ProcureLineItem extrinsic if comments or attachments
            have been changed via a change order.
        Out of the box Buyer does not allow reqs to be submitted if only the comments have changed.
        This submit hook remedies that limitation.
     */
    public List run (Approvable approvable) {

        Log.customer.debug("Calling " + ClassName);

        if (!(approvable instanceof Requisition)) {            
            return NoErrorResult;
        }

    	Requisition newRequisition = (Requisition)approvable;

    	// If Req has no previous version, then it is not a change scenario.  Do not continue processing.
        ClusterRoot loCluster = newRequisition.getPreviousVersion();
        if (loCluster == null) 
        {
            Log.customer.debug("Req has no previous version.  Returning without setting extrinsics.");
            return NoErrorResult;
        }
        else
        {
            String lsStatus = (String)loCluster.getFieldValue("StatusString");
            if (lsStatus.equals("Composing"))
            {
                Log.customer.debug("Previous version in composing - not a change order. Returning without setting extrinsics.");
                return NoErrorResult;
            }
        }
        
        Log.customer.debug("Requisition does have a previous version...");

        Requisition oldRequisition = (Requisition)newRequisition.getPreviousVersion();
        
        if ( haveHeaderFieldsChanged( newRequisition, oldRequisition ) )
        {
            Log.customer.debug("Hdr Fields Have Changed...");
            
            //this triggers internal change order
            setHeaderFieldsHaveChangedField( newRequisition, "InternalChangeOrderFlag" ) ;
        }
        
        if(isExtChangeOrderFieldChanged(newRequisition, oldRequisition ))
        {
            //this triggers external change order
            setHeaderFieldsHaveChangedField( newRequisition, "DummyCommentField" ) ;
        }
        
        BaseVector oldComments = oldRequisition.getComments();
        BaseVector newComments = newRequisition.getComments();

        // Inspect the comment body (text) for each and find matches.
        // If new comment has match, then inspect the attachments for each and find matches.
        // If a new comment has no match for text or attachments, then comments have changed.
        // Set line item extrinsic that pertains to that comment.
        boolean foundIt = true;

        // The following logic identifies all added and changed comments
        for (int i = 0; i < newComments.size(); i++) {
            // Ariba 8.1: Replaced deprecated method
            Log.customer.debug("Search comment " + i + " is: " + ((Comment)newComments.get(i)).getBody());
            // Process comment only if it is intended as a PO comment
            // Ariba 8.1: Replaced deprecated method
            if (((Comment)newComments.get(i)).getExternalComment()) {
                Log.customer.debug("Comment is external, processing.");
                foundIt = false;
                for (int j = 0; j < oldComments.size(); j++ ) {
                    // Ariba 8.1: Replaced deprecated method
                    Log.customer.debug("Compare comment " + j + " is: " + ((Comment)oldComments.get(j)).getBody());
                    // Ariba 8.1: Replaced deprecated method
                    if (((Comment)newComments.get(i)).getBody().equals(((Comment)oldComments.get(j)).getBody())) {
                        Log.customer.debug("Found text match for this comment.");
                        // Ariba 8.1: Replaced deprecated method
                        if (!haveAttachmentsChanged((Comment)newComments.get(i),(Comment)oldComments.get(j))) {
                            Log.customer.debug("Found attachment match for this comment.");
                            foundIt = true;
                            break;
                        }
                    }
                }
                // Once foundIt is false for a new comment, set extrinsic for this comment.
                if (!foundIt) {
                    Log.customer.debug("Found no match for comment " + i);
                    // Ariba 8.1: Replaced deprecated method
                    setCommentsChangeField((Comment)newComments.get(i));
                }
            }
            else {
                Log.customer.debug("Comment is internal, skipping.");
            }
        }

        // The following logic identifies all deleted comments
        Log.customer.debug("Doing reverse check for deleted comments");

        for (int ii = 0; ii < oldComments.size(); ii++) {
            Log.customer.debug("Old comment " + ii + " is: " + ((Comment)oldComments.get(ii)).getBody());
            // Process comment only if it is intended as a PO comment
            if (((Comment)oldComments.get(ii)).getExternalComment()) {
                Log.customer.debug("Old comment is external, processing.");
                foundIt = false;
                for (int jj = 0; jj < newComments.size(); jj++ ) {
                    Log.customer.debug("Compare new comment " + jj + " is: " + ((Comment)newComments.get(jj)).getBody());
                    if (((Comment)oldComments.get(ii)).getBody().equals(((Comment)newComments.get(jj)).getBody())) {
                        Log.customer.debug("Found match for old comment " + ii + " in new comment " + jj);
                        foundIt = true;
                        break;
                    }
                }
                // Once foundIt is false for an old comment, set extrinsic for all lines in the new req
                if (!foundIt) {
                    if (((Comment)oldComments.get(ii)).getLineItem() == null)
                    {
                       Log.customer.debug("Old header comment " + ii + " not found ... will set extrinsic on all lines");
                       setAllCommentsChangeFields(newRequisition);
                       break;
                    }
                    else
                    {
                       Log.customer.debug("Old comment " + ii + " is a line comment ... will ignore extrinsic setting");
                    }
                }
            }
            else {
                Log.customer.debug("Old comment is internal, skipping.");
            }
        }
        //(Sarath) CSPL-2617 -The following method takes care of Changing Order on addition or deletion of an Attachemnt in new 9r1 Attachemnt structure. 
        have9r1AttachmentsChanged(oldRequisition,newRequisition);
        return NoErrorResult;
    }

    /*
        Comment text may remain the same while attachment(s) have changed.
        If this is the case, return "true" to indicate that attachments have changed.
     */
    private boolean haveAttachmentsChanged(Comment newComment, Comment oldComment) {

        BaseVector oldAttachments = newComment.getAttachments();
        BaseVector newAttachments = oldComment.getAttachments();

        // Inspect the unique stored filename for each attachment and find matches.
        // If a new attachment has no match, then attachments have changed.
        // Return true in order to set line item extrinsic that pertains to that comment.
        boolean foundIt = true;

        for (int i = 0; i < newAttachments.size(); i++) {
            // Ariba 8.1: Replaced deprecated method
            Log.customer.debug("Search attachment " + i + " is: " + ((Attachment)newAttachments.get(i)).getStoredFilename());
            foundIt = false;
            for (int j = 0; j < oldAttachments.size(); j++ ) {
                // Ariba 8.1: Replaced deprecated method
                Log.customer.debug("Compare attachment " + j + " is: " + ((Attachment)oldAttachments.get(j)).getStoredFilename());
                // Ariba 8.1: Replaced deprecated method
                if (((Attachment)newAttachments.get(i)).getStoredFilename().equals(((Attachment)oldAttachments.get(j)).getStoredFilename())) {
                    Log.customer.debug("Found match for this attachment.");
                    foundIt = true;
                    break;
                }
            }
            // Once foundIt is false for a new attachment, return true for the the newComment
            if (!foundIt) {
                Log.customer.debug("Attachments have changed for newComment");
                return true;
            }
        }
        // If it has made it this far without returning true, then attachments have not changed for the newComment
        Log.customer.debug("Attachments have not changed.");
        return false;
    }
    /* The following code is to handle Attachments with new Architecture in 9r1.
     * This code will set the extrinsic filed if any attachment is newly added or deleted. 
     */
    private void have9r1AttachmentsChanged (Requisition oldRequisition, Requisition newRequisition)
    {
        boolean foundIt = true;
        BaseVector oldAttachmentWrapper = oldRequisition.getAttachments();
        BaseVector newAttachmentWrapper = newRequisition.getAttachments();
        
        for (int i = 0; i < newAttachmentWrapper.size(); i++) 
        {            
            Log.customer.debug("Search attachment " + i + " is: " + ((Attachment)((AttachmentWrapper)newAttachmentWrapper.get(i)).getAttachments().get(0)).getStoredFilename());
            if (((AttachmentWrapper)newAttachmentWrapper.get(i)).getExternalAttachment()) 
            {
                foundIt = false;           
                for (int j = 0; j < oldAttachmentWrapper.size(); j++ )
                {                    
                    Log.customer.debug("Compare attachment " + j + " is: " + ((Attachment)((AttachmentWrapper)oldAttachmentWrapper.get(j)).getAttachments().get(0)).getStoredFilename());                    
                    if (((Attachment)((AttachmentWrapper)newAttachmentWrapper.get(i)).getAttachments().get(0)).getStoredFilename().equals(((Attachment)((AttachmentWrapper)oldAttachmentWrapper.get(j)).getAttachments().get(0)).getStoredFilename())) {
                        Log.customer.debug("Found match for this attachment.");
                        foundIt = true;
                        break;
                    }
                }
                // Once foundIt is false for a new attachment, return true for the the newComment
                if (!foundIt)
                {
                    Log.customer.debug("Found no match for Attachment " + i);                    
                    setAttachmentChangeField((AttachmentWrapper)newAttachmentWrapper.get(i));
                }
            }
            else
            {
                Log.customer.debug("New Attachment is internal, skipping.");
            }
        }
        
        Log.customer.debug("Doing reverse check for deleted Attachments"+oldAttachmentWrapper.size());        
        for (int p = 0; p < oldAttachmentWrapper.size(); p++) 
        {                        
            Log.customer.debug("Search attachment " + p + " is: " + ((Attachment)((AttachmentWrapper)oldAttachmentWrapper.get(p)).getAttachments().get(0)).getStoredFilename());
            if (((AttachmentWrapper)oldAttachmentWrapper.get(p)).getExternalAttachment()) 
            {
                foundIt = false;           
                for (int k = 0; k < newAttachmentWrapper.size(); k++ )
                {                    
                    Log.customer.debug("Compare attachment " + k + " is: " + ((Attachment)((AttachmentWrapper)oldAttachmentWrapper.get(k)).getAttachments().get(0)).getStoredFilename());                    
                    if (((Attachment)((AttachmentWrapper)oldAttachmentWrapper.get(p)).getAttachments().get(0)).getStoredFilename().equals(((Attachment)((AttachmentWrapper)newAttachmentWrapper.get(k)).getAttachments().get(0)).getStoredFilename())) {
                        Log.customer.debug("Found match for this attachment.");
                        foundIt = true;
                        break;
                    }
                }
                // Once foundIt is false for a new attachment, return true for the the newComment
                if (!foundIt)
                {
                    Log.customer.debug("Found no match for Attachment " + p);                    
                    setAllAttachmentsChangeFields(newRequisition);
                }
            }
            else
            {
                Log.customer.debug("Attachment is internal, skipping.");
            }
        }
        return;
    }
    
    private void setAttachmentChangeField(AttachmentWrapper attachmentWrapper) {

        // Note space at beginning and end of SetFieldValue string
        // "Line" or "Header" will be placed at front of string depending on the type of comment
        // Timestamp will be placed at end of string
        String SetFieldValue = " Attachments were changed for this order on ";
        String SetFieldName = "DummyCommentField";       

        // Get requisition
        BaseObject bo = attachmentWrapper.getLineItemCollection();
        if (!(bo instanceof Requisition)) {
            return;
        }
        Requisition requisition = (Requisition)bo;

        // If comment is for a line item, set extrinsic for only that line item
        if (attachmentWrapper.getFirstLineItem() != null) {
            Log.customer.debug("Setting extrinsic to: " + "Line" + SetFieldValue + getDateTime());
            attachmentWrapper.getFirstLineItem().setFieldValue(SetFieldName, "Line" + SetFieldValue + getDateTime());
        }
        // If Attachment is for requisition header, then set extrinsic for all requisition line
        //  items (across orders) so that all orders are versioned
        else {
            Log.customer.debug("Attachment line item is null.  Must be a header comment.");
            setAllAttachmentsChangeFields(requisition);
        }
    }

    private void setAllAttachmentsChangeFields(Requisition foReq)
    {
        String SetFieldValue = " Attachments were changed for this order on ";
        String SetFieldName = "DummyCommentField";       

        //  items (across orders) so that all orders are versioned
        BaseVector lineItems = foReq.getLineItems();

        for (int i = 0; i < lineItems.size(); i++) 
        {
            Log.customer.debug("Setting extrinsic to: " + "Header" + SetFieldValue + getDateTime());            
            ((ProcureLineItem)lineItems.get(i)).setFieldValue(SetFieldName,"Header" + SetFieldValue + getDateTime());
        }
        
    }
    /*
        Extrinsic will be set to the following:
            "[Line||Header] Comments were changed by submit hook on [date/time stamp]"
        Date/time stamp format example is: "Thu, Sep 5, 2002 at 02:24:48 EDT"
        Use of timestamp will ensure that hook will always provide a unique value to the field, thus allowing
            the req to submit and forcing the order upon approval.
        If a comment is for an individual line item, set the extrinsic for only that line item.
        If a comment is for the requisition header, then set the extrinsic for all line items to the same value.
            This will ensure that all orders generated from req will have the new header comment.
     */
    private void setCommentsChangeField(Comment comment) {

        // Note space at beginning and end of SetFieldValue string
        // "Line" or "Header" will be placed at front of string depending on the type of comment
        // Timestamp will be placed at end of string
        String SetFieldValue = " Comments were changed for this order on ";
        String SetFieldName = "DummyCommentField";       

        // Get requisition
        BaseObject bo = comment.getParent();
        if (!(bo instanceof Requisition)) {
            return;
        }
        Requisition requisition = (Requisition)bo;

        // If comment is for a line item, set extrinsic for only that line item
        if (comment.getLineItem() != null) {
            Log.customer.debug("Setting extrinsic to: " + "Line" + SetFieldValue + getDateTime());
            comment.getLineItem().setFieldValue(SetFieldName, "Line" + SetFieldValue + getDateTime());
        }
        // If comment is for requisition header, then set extrinsic for all requisition line
        //  items (across orders) so that all orders are versioned
        else {
            Log.customer.debug("Commment line item is null.  Must be a header comment.");
            setAllCommentsChangeFields(requisition);
        }
    }
    
    /*
        Extrinsic will be set to the following:
            "Header Comments were changed by submit hook on [date/time stamp]"
        Date/time stamp format example is: "Thu, Sep 5, 2002 at 02:24:48 EDT"
        Use of timestamp will ensure that hook will always provide a unique value to the field, thus allowing
            the req to submit and forcing the order upon approval.
        Set the extrinsic for all line items to the same value.
        This will ensure that all orders generated from req will have the new header comment.
     */
    private void setAllCommentsChangeFields(Requisition foReq) {

        // Note space at beginning and end of SetFieldValue string
        // "Header" will be placed at front of string 
        // Timestamp will be placed at end of string
        String SetFieldValue = " Comments were changed for this order on ";
        String SetFieldName = "DummyCommentField";       

        //  items (across orders) so that all orders are versioned
        BaseVector lineItems = foReq.getLineItems();

        for (int i = 0; i < lineItems.size(); i++) 
        {
            Log.customer.debug("Setting extrinsic to: " + "Header" + SetFieldValue + getDateTime());
            // Ariba 8.1: Replaced deprecated method
            ((ProcureLineItem)lineItems.get(i)).setFieldValue(SetFieldName,"Header" + SetFieldValue + getDateTime());
        }
    }
    
    private boolean haveHeaderFieldsChanged( Requisition foNewRequisition, Requisition foOldRequisition )
    {
        
        java.util.List lvNewHeaderFields = new java.util.LinkedList() ;
        java.util.List lvOldHeaderFields = new java.util.LinkedList() ;
        
        lvNewHeaderFields = getHeaderFields( foNewRequisition ) ;
        lvOldHeaderFields = getHeaderFields( foOldRequisition ) ;
        
        for ( int i=0; i<lvNewHeaderFields.size(); i++ )
        {
            // Ariba 8.1: Replaced deprecated method
            Log.customer.debug( "New header fields: " + lvNewHeaderFields.get( i ) ) ;
        }
        for ( int i=0; i<lvOldHeaderFields.size(); i++ )
        {
            // Ariba 8.1: Replaced deprecated method
            Log.customer.debug( "Old header fields: " + lvOldHeaderFields.get( i ) ) ;
        }
        
        if ( ! lvNewHeaderFields.equals( lvOldHeaderFields ) )
        {
            Log.customer.debug( "Header fields have changed." ) ;
            return true ;
        }
        else
        {
            Log.customer.debug( "Header fields have not changed." ) ;
            return false ;
        }
    }
    
    private java.util.List getHeaderFields( Requisition foRequisition )
    {

        java.util.List lvHeaderFields = new java.util.LinkedList() ;
        //excluding the External Change Order field from the header field list
        mvHeaderFields.remove("ReqHeadCB2Value");
        
        for ( int i=0; i<mvHeaderFields.size(); i++ )
        {                                  
            
            // Ariba 8.1: Replaced deprecated method
            String lsHeaderField      = (String)mvHeaderFields.get( i ) ;
            String lsHeaderFieldValue = null ;
            
            Log.customer.debug("Got the " + i + "th element from mvHeaderFields: " + lsHeaderField ) ;
            
            if ( lsHeaderField.indexOf("ReqHeadFieldDefault") != -1 )
            {
                // Chooser field, so get the Name (i.e. the value)
                Log.customer.debug("Got the Chooser field " + lsHeaderField + " so look for Name..." ) ;
                lsHeaderFieldValue = (String)( foRequisition.getDottedFieldValue( lsHeaderField + ".Name" ) );
            }
            else if ( lsHeaderField.indexOf( "ReqHeadText" ) != -1 )
            {
                // Text field, so get the UniqueName
                lsHeaderFieldValue = (String)foRequisition.getFieldValue( lsHeaderField ) ;
            }
            else if ( lsHeaderField.indexOf( "ReqHeadCB" ) != -1 )
            {
            	// (Manoj) 12/19/2008 Checking the null value before converting from Boolean to String value
                // Boolean field, so get the String value of the Boolean
                lsHeaderFieldValue = foRequisition.getFieldValue( lsHeaderField )!= null? ( (Boolean)foRequisition.getFieldValue( lsHeaderField ) ).toString() : null ;                
            }                       
            else if ( lsHeaderField.indexOf( "UsePCardBool" ) != -1 )
            {
            	// (Manoj) 12/19/2008 Checking the null value before converting from Boolean to String value
            	// rlee PCard field           	
                lsHeaderFieldValue = foRequisition.getFieldValue( lsHeaderField )!= null? ( (Boolean)foRequisition.getFieldValue( lsHeaderField ) ).toString() : null ;                
            }
            else if ( lsHeaderField.indexOf("PCardToUse") != -1 )
            {
                // rlee PCard object
                Log.customer.debug("rlee 284 PCardToUse " + lsHeaderField + " so look for Alias..." ) ;
                lsHeaderFieldValue = (String)( foRequisition.getDottedFieldValue( lsHeaderField + ".Alias" ) );
            }
                        
            if ( lsHeaderFieldValue != null )
            {
                lvHeaderFields.add( lsHeaderFieldValue ) ;
            }
            else
            {
                lvHeaderFields.add( "null" ) ;
                Log.customer.debug("Got the null value, adding string null... " ) ;
            }
            
            Log.customer.debug("Got the value for " + lsHeaderField + "value is: " + lsHeaderFieldValue ) ;                                   
        }

        return lvHeaderFields ;
    }
    
    /**     
     * This method sets the dummy line field to force Ariba to generate a new version
     * of Order.  The only exception to this logic are lines that have been sent
     * for solicitation.
     */
    private void setHeaderFieldsHaveChangedField( Requisition foNewRequisition, String lsFieldNameToSet )
    {        
        ReqLineItem loReqLineItem     = null ;
        List      lvReqLineItems    = null ;        
        String      lsFieldValueToSet = " Header fields were changed for this order on " ;        
        
        lvReqLineItems = (List)foNewRequisition.getLineItems() ;
        
        for ( int i=0; i<lvReqLineItems.size(); i++ )
        {
            // Ariba 8.1: Replaced deprecated method
            loReqLineItem = (ReqLineItem)lvReqLineItems.get( i ) ;
            
            if ( (Boolean)loReqLineItem.getFieldValue("SolicitationLine") == null ||
                ! ((Boolean)loReqLineItem.getFieldValue("SolicitationLine")).booleanValue() )
            {
                Log.customer.debug( "Setting the dummy field for header changed fields..." ) ;
                loReqLineItem.setFieldValue( lsFieldNameToSet, lsFieldValueToSet + getDateTime() ) ;
            }            
        }
    }
    
    private boolean isExtChangeOrderFieldChanged(Requisition foNewRequisition, Requisition oldRequisition)
    {
        String sExtChgOrderField = "ReqHeadCB2Value";
        String sNewReqHeaderFieldValue = foNewRequisition.getFieldValue( sExtChgOrderField )!= null? ( (Boolean)foNewRequisition.getFieldValue( sExtChgOrderField) ).toString() : "null" ;
        String sOldReqHeaderFieldValue = oldRequisition.getFieldValue( sExtChgOrderField)!= null? ( (Boolean)oldRequisition.getFieldValue( sExtChgOrderField ) ).toString() : "null" ;
        if(!sOldReqHeaderFieldValue.equals(sNewReqHeaderFieldValue))
        {
            //field values has changed
            return true;
        }else
        {
            //field values not changed
            return false;
        }
    }

    private String getDateTime()
    {
        SimpleDateFormat loSimpleDateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'at' hh:mm:ss z") ;
        String           lsDateTime = loSimpleDateFormat.format(Calendar.getInstance().getTime()) ;
        
        return lsDateTime ;
    }  
}
