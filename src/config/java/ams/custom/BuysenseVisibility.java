/**
Version :  1.2

Baseline DEV SPL 233 - Added a log message for missing clientName.
                       Took out the NonFatalexception as it puts out a stack trace.

*/
// 10/10/2003: Updates Ariba 8.1 (Richard Lee)
// Ariba 8.1 Replaced addElement with add

package config.java.ams.custom;

import java.util.List;
import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.core.TransientData;
import ariba.htmlui.fieldsui.Log;

//81->822 changed Vector to List
//81->822 changed ConditionValueInfo to ValueInfo
public class BuysenseVisibility extends Condition
{
    
    public static final String TargetValue1Param = "TargetValue1";
    public static final String TargetValue2Param = "TargetValue2";
    public static final String TargetValue3Param = "TargetValue3";
    private static final ValueInfo parameterInfo[];
    private static final String requiredParameterNames[] = 
        {
        "TargetValue"
    };
    private static final String EqualToMsg1 = "EqualToMsg1";
    
    public boolean evaluate(Object value, PropertyTable params)
    
    {
        Log.visibility.debug("Buysense: This is the object we get in the evaluate: %s ", value);
        return testValue(value, params);
    }
    
    protected boolean testValue(Object value, PropertyTable params)
    {
        String clientUniqueName = (String)params.getPropertyForKey("TargetValue");
        String fieldName        = (String)params.getPropertyForKey("TargetValue1");
        BaseObject bo   = (BaseObject)params.getPropertyForKey("TargetValue2");
        String appType          = (String)params.getPropertyForKey("TargetValue3");
        
        
        Log.visibility.debug("ClientUN: %s fieldName: %s BaseObject: %s",
                             clientUniqueName,fieldName,bo);
        /*
         * Added by SRINI for 9r1 field label issue
         */
        TransientData td =  Base.getSession().getTransientData();
        String lsClientUniqueNameSession = (String)td.get("AMSLabel");
        /*
         * Pavan Aluri - CSPL-8834: Updated the logic to set ClientName when On Behalf of user is changed
         */
        /*if(lsClientUniqueNameSession == null && clientUniqueName != null)
        {
            td.put("AMSLabel", clientUniqueName);
        }*/
        Log.customer.debug("lsClientUniqueNameSession = "+lsClientUniqueNameSession +", clientUniqueName = "+clientUniqueName);
        if (lsClientUniqueNameSession == null)
        {
            if (clientUniqueName != null)
            {
                td.put("AMSLabel", clientUniqueName);
            }

        } else
        {
            if (clientUniqueName != null && !lsClientUniqueNameSession.equalsIgnoreCase(clientUniqueName))
            {
                td.put("AMSLabel", clientUniqueName);
            }
        }
        
        //Baseline DEV SPL 233
        if (clientUniqueName == null)  
        {
            Log.customer.info(8888,"BuysenseVisibility");
            // Util.assertNonFatal(false,"Did not receive the Client Name");
            return false;
        }
        return BuysenseUIConditions.isFieldVisible(clientUniqueName ,
                                                   fieldName,bo,appType);
    }
    
    public ConditionResult evaluateAndExplain(Object value,
                                              PropertyTable params)
    {
        
        if(!testValue(value, params))
        {
            String msg = params.stringPropertyForKey("Message");
            String subject = subjectForMessages(params);
            if(msg != null)
                return new ConditionResult(Fmt.Si(msg, subject));
            else
                return new ConditionResult(Fmt.Sil("ariba.common.core.condition",
                                                   "EqualToMsg1", subject));
        }
        else
        {
            return null;
        }
    }
    
    private List getTargetValues(PropertyTable params)
    {
        Log.visibility.debug("In target values");
        // Ariba 8.1 List targetValues = new List();
        List targetValues = ListUtil.list();
        Object targetValue = params.getPropertyForKey("TargetValue");
        if(targetValue != null)
            // Ariba 8.1 replaced addElement with add
            targetValues.add(targetValue);
        targetValue = params.getPropertyForKey("TargetValue1");
        if(targetValue != null)
            targetValues.add(targetValue);
        targetValue = params.getPropertyForKey("TargetValue2");
        if(targetValue != null)
            targetValues.add(targetValue);
        targetValue = params.getPropertyForKey("TargetValue3");
        if(targetValue != null)
            targetValues.add(targetValue);
        return targetValues;
    }
    
    /* protected String[] getTypesForParsingStaticParameter(String paramName, Type valueType, PropertyTable dynParamTypes, PropertyTable outParamTypes)
     {
         Log.visibility.debug("In getTypesForParsingStaticParameter");
         if(paramName.equals("TargetValue") || paramName.equals("TargetValue1") || paramName.equals("TargetValue2") || paramName.equals("TargetValue3"))
         {
             String result[] = new String[1];
             result[0] = valueType.getClassName();
             return result;
         }
         else
         {
             return super.getTypesForParsingStaticParameter(paramName, valueType, dynParamTypes, outParamTypes);
         }
     }
     */
    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }
    
    protected String[] getRequiredParameterNames()
    {
        return requiredParameterNames;
    }
    
    public BuysenseVisibility()
    {
    }
    
    static
        {
        parameterInfo = (new ValueInfo[] 
                         {
                new ValueInfo("TargetValue",
                                       0),
                    new ValueInfo("TargetValue1",
                                           0),
                    new ValueInfo("TargetValue2",
                                           0),
                    new ValueInfo("TargetValue3",
                                           0),
                    new ValueInfo("Message", 0, Behavior.StringClass)
            }
        );
    }
}
