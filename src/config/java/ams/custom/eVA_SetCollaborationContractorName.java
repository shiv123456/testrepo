package config.java.ams.custom;

import java.util.Locale;

import ariba.base.core.Base;
import ariba.base.core.MultiLingualString;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;
import ariba.workforce.core.ContractorCandidate;

/*
 * This class is called from trigger in  ariba.workforce.core.ContractorCandidate class
 * this sets Name field as Name = LastName, FirstName.
 * Also sets DriversLicenseStateNumber=DriversLicenseState + DriverLicenseNumber (PartialGlobalID)
 */
public class eVA_SetCollaborationContractorName extends Action
{

	public void fire(ValueSource valuesource, PropertyTable propertytable)
			throws ActionExecutionException
	{
		Log.customer.debug(" Executing trigger eVA_SetCollaborationContractorName.fire() the object is: " + valuesource);
		if (valuesource instanceof ContractorCandidate)
		{
			ContractorCandidate cnadidate = (ContractorCandidate) valuesource;
			setName(cnadidate);
			setDriversLicenseStateNumber(cnadidate);
		}
	}

	private void setName(ContractorCandidate cnadidate) 
	{
		String contractorName = " ";
		String firstName = (String)cnadidate.getDottedFieldValue("ContractorFirstName");
		Log.customer.debug(" eVA_SetCollaborationContractorName ContractorFirstName : "+firstName);
		String lastName = (String)cnadidate.getDottedFieldValue("ContractorLastName");
		Log.customer.debug(" eVA_SetCollaborationContractorName ContractorFirstName : "+lastName);
		if(!StringUtil.nullOrEmptyOrBlankString(firstName) && !StringUtil.nullOrEmptyOrBlankString(lastName))				
		contractorName = lastName+", "+firstName;
		
		Locale locale = Base.getSession().getLocale();
		MultiLingualString mls = new MultiLingualString(Base.getSession().getPartition());
		mls.setString(locale, contractorName);
		Log.customer.debug(" eVA_SetCollaborationContractorName Contractor Name : "+mls);			
		cnadidate.setName(mls);
	}

	private String getStringFromMLS(Object mlsObj)
	{
		String strValue = "";
		if (mlsObj instanceof MultiLingualString)
		{
			strValue =((MultiLingualString)mlsObj).getString(Base.getSession().getLocale());
		}
		return strValue;
	}
	
	private void setDriversLicenseStateNumber(ContractorCandidate cnadidate)
	{
		String sDLState = (String)cnadidate.getDottedFieldValue("DriversLicenseState.UniqueName"); // Drivers LicenseState Number (2-Letter)
		String sDLNumber = (String)cnadidate.getDottedFieldValue("PartialGlobalID"); // Drivers License Number
		Log.customer.debug(" eVA_SetCollaborationContractorName setting DriversLicenseStateNumber to : "+sDLState+sDLNumber);
		cnadidate.setDottedFieldValue("DriversLicenseStateNumber", sDLState+sDLNumber);
	}
}
