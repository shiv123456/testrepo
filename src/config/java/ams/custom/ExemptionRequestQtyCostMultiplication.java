package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.base.fields.Action;
import ariba.base.fields.Log;
import ariba.base.fields.ValueInfo;
import ariba.base.fields.ValueSource;
import ariba.basic.core.Currency;
import ariba.basic.core.Money;
import ariba.util.core.PropertyTable;

public class ExemptionRequestQtyCostMultiplication extends Action
{
    private static final ValueInfo parameterInfo[];

    public void fire(ValueSource object, PropertyTable params)
    {
        try
        {
            Partition partition = Base.getService().getPartition("pcsv");
            Approvable loAppr = (Approvable) object;
            String lsRowQuantityField = (String) params.getPropertyForKey("QtyFieldName");
            String lsRowAmountField = (String) params.getPropertyForKey("AmtFieldName");
            String lsEstAmountField = (String) params.getPropertyForKey("TotalCostFieldName");
            Integer loRowQuantity = (Integer) loAppr.getDottedFieldValue(lsRowQuantityField);
            Money loRowAmount = (Money) loAppr.getDottedFieldValue(lsRowAmountField);
            if (loRowQuantity == null && loRowAmount == null)
            {
                return;
            }
            else
            {
                if (loRowQuantity == null)
                {
                    loRowQuantity = Integer.valueOf(0);
                }
                if (loRowAmount == null)
                {
                    loRowAmount = new Money(0.0, Currency.getDefaultCurrency(partition));
                    loAppr.setDottedFieldValue(lsRowAmountField, loRowAmount);
                }
                loAppr.setDottedFieldValue(lsEstAmountField, loRowAmount.multiply(loRowQuantity.doubleValue()));
            }

        }
        catch (Exception e)
        {
            Log.customer.debug("BuysenseEformQtyCostMultiplication:: exception" + e.getMessage());
        }
    }

    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    static
    {
        parameterInfo = (new ValueInfo[] { new ValueInfo("QtyFieldName", 0), new ValueInfo("AmtFieldName", 0),
            new ValueInfo("TotalCostFieldName", 0) });
    }
}