package config.java.ams.custom;

import java.io.*;
import java.math.*;
import java.sql.*;
import java.util.*;
import oracle.sql.*;
import ariba.util.log.*;

/** Wrapper for BUYINT Database Record */
public class BuyintDBRec extends Properties //implements BuyintConstants
{
    public static final String VAR_DELIM = "%%";

    protected BuyintDBIO moDBIO = null;
    protected String msTableName = null;
    protected Connection moConn = null;
    protected static PreparedStatement moPrep = null;
    protected int miCol;
    protected long mlSeq;

    protected static final String COL_TIN              = "TIN";
    protected static final String COL_DOCUMENT_TYPE    = "DOCUMENT_TYPE";
    protected static final String COL_DOCUMENT_ID      = "DOCUMENT_ID";
    protected static final String COL_TRANSACTION_TYPE = "TRANSACTION_TYPE";
    protected static final String COL_STATUS           = "STATUS";
    protected static final String COL_TRANSACTION_DATA = "TRANSACTION_DATA";
    protected static final String COL_ERROR_MESSAGE    = "ERROR_MESSAGE";
    protected static final String COL_RETRY_COUNT      = "RETRY_COUNT";
    protected static final String COL_CREATION_DATE    = "CREATION_DATE";
    protected static final String COL_PROCESS_DATE     = "PROCESS_DATE";
    protected static final String IMPOSSIBLE_STRING    = "impossible" + String.valueOf(System.currentTimeMillis());

    protected static final String COL_IMPORT_DATA 	   = "IMPORT_DATA";
    protected static final String COL_ATTEMPTS		   = "ATTEMPTS";
    protected static final String COL_ERRORS    	   = "ERRORS";
    protected static final String COL_PO_NUMBER    	   = "PO_NUMBER";
    protected static final String COL_RC_NUMBER    	   = "RC_NUMBER";
    protected static final String COL_TID_NUMBER       = "TID";
    protected static final String COL_PROCD_PO_NUMBER  = "PROCESSED_PO_NUMBER";
    public BuyintDBRec(BuyintDBIO foDBIO, String fsTableName)
    {
        moDBIO = foDBIO;
        moConn = foDBIO.getConnection();
        msTableName = fsTableName;
    }

    public BuyintDBRec(BuyintDBIO foDBIO, Connection foConn, String fsTableName)
    {
        moDBIO = foDBIO;
        moConn = foConn;
        msTableName = fsTableName;
    }

    public void setConnection(Connection foConn)
    {
        moConn = foConn;
    }

    public void destroy()
    {
        if (moDBIO != null)
        {
            if (moConn != null)
            {
               moDBIO.closeConnection(moConn);
            }
            moConn = null;
            msTableName = null;;
            moDBIO = null;
        }
    }

    public void clear()
    {
        // empty out all current properties
        super.clear();
        setTIN(0L);
    }

    public void checkConnection()
        throws SQLException
    {
        try
        {
            if (moConn == null || moConn.isClosed())
            {
                moConn = moDBIO.getConnection();
            }
        }
        catch(SQLException loEx)
        {
            moConn = moDBIO.getConnection();
        }
    }

    protected String getSelectString(String fsTableName)
    {
        return "SELECT TIN, DOCUMENT_TYPE, DOCUMENT_ID, TRANSACTION_TYPE, STATUS, "
               + " TRANSACTION_DATA, ERROR_MESSAGE, RETRY_COUNT, CREATION_DATE, "
               + " PROCESS_DATE "
               + " FROM " + fsTableName + " ";
    }

    protected String getSelectByKey(String fsTableName)
    {
        return "SELECT TIN, DOCUMENT_TYPE, DOCUMENT_ID, TRANSACTION_TYPE, STATUS, "
               + " TRANSACTION_DATA, ERROR_MESSAGE, RETRY_COUNT, CREATION_DATE, "
               + " PROCESS_DATE "
               + " FROM " + fsTableName
               + " WHERE DOCUMENT_TYPE = ? AND DOCUMENT_ID = ? AND TRANSACTION_TYPE = ? "
               + " AND (STATUS = ? OR STATUS = ? OR STATUS = ? OR STATUS = ? OR STATUS = ?)"
               + " ORDER BY TIN";
    }

    protected String getSelectByStatus(String fsTableName)
    {
        return "SELECT TIN, DOCUMENT_TYPE, DOCUMENT_ID, TRANSACTION_TYPE, STATUS, "
               + " TRANSACTION_DATA, ERROR_MESSAGE, RETRY_COUNT, CREATION_DATE, "
               + " PROCESS_DATE "
               + " FROM " + fsTableName
               + " WHERE (STATUS = ? OR STATUS = ? OR STATUS = ? OR STATUS = ? OR STATUS = ?)";
    }

    protected String getSelectByTin(String fsTableName, long flTIN)
    {
        return "SELECT TIN, DOCUMENT_TYPE, DOCUMENT_ID, TRANSACTION_TYPE, STATUS, "
               + " TRANSACTION_DATA, ERROR_MESSAGE, RETRY_COUNT, CREATION_DATE, "
               + " PROCESS_DATE "
               + " FROM " + fsTableName
               + " WHERE TIN = " + String.valueOf(flTIN);
    }

    protected static String getSelectForUpdate(String fsTableName, long flTIN)
    {
        return "SELECT TRANSACTION_DATA "
               + " FROM " + fsTableName
               + " WHERE TIN = " + String.valueOf(flTIN)
               + " FOR UPDATE";
    }

    protected String getUpdateByTin(String fsTableName)
    {
        return "UPDATE " + fsTableName + " SET "
               + " STATUS = ?, ERROR_MESSAGE = ?, RETRY_COUNT = ?, PROCESS_DATE = ? "
               + " WHERE TIN = ?";

    }

    protected String getUpdateQuery(String fsTableName)
    {
    	debug("getUpdateQuery::Called");
    	return "UPDATE " + fsTableName + " SET "
               + " STATUS = ?, RC_NUMBER = ?, PROCESSED_PO_NUMBER = ?, ERRORS = ?, ATTEMPTS = ?, PROCESS_DATE = ? "
               + " WHERE TIN = ?";
    }
    
    protected String getInsert(String fsTableName)
    {
        return "INSERT INTO " + fsTableName
               + "(TIN, DOCUMENT_TYPE, DOCUMENT_ID, TRANSACTION_TYPE, STATUS, "
               + " TRANSACTION_DATA, ERROR_MESSAGE, RETRY_COUNT, CREATION_DATE, "
               + " PROCESS_DATE)"
               + " VALUES (?, ?, ?, ?, ?, EMPTY_CLOB(), ?, ?, ?, ?)";
    }

    protected static String getSeqSQL()
    {
        return "SELECT SEQ_EXPORT_DATA_TIN.nextval from DUAL";
    }

    protected String getDuplicateSQL()
    {
    	return "SELECT DOCUMENT_ID FROM EXPORT_DATA WHERE TRANSACTION_TYPE='SEND' AND STATUS='NEW' AND DOCUMENT_ID='"+this.getDocumentID()+"'";
    }

    protected String getSysDateSQL()
    {
    	return "SELECT TO_CHAR(SYSDATE, 'MM-DD-YYYY HH24:MI:SS') SYS_DATE FROM DUAL";
    }
    
    protected String getDeleteSQL(String docID)
    {
    	return "DELETE FROM EXPORT_DATA WHERE TRANSACTION_TYPE='SEND' AND STATUS='NEW' AND length(TRANSACTION_DATA) = 0 " +
    			"AND DOCUMENT_ID='"+docID+"'"; 
    }
 
    protected boolean foundDuplicate(Connection conn)
    throws SQLException
    {
    	Log.customer.debug("BuyintDBRec::Incoming table name " +this.msTableName);
    	if(!this.msTableName.equals("EXPORT_DATA"))
    	{
    		return false;
    	}
    	
    	Log.customer.debug("BuyintDBRec::Duplicate check query " +this.getDuplicateSQL());
    	Statement stmt = conn.createStatement();
	    ResultSet rs   = stmt.executeQuery(getDuplicateSQL());
	    if(rs.next())
	    {
	    	Log.customer.warning(10003, this.getDocumentID());
	    	return true;	
	    }
	    stmt.close();
	    rs.close();
	    return false;
    }
    
    protected void printSysDate(Connection conn)
    throws SQLException
    {
    	Statement stmt = conn.createStatement();
	    ResultSet rs   = stmt.executeQuery(getSysDateSQL());
	    if(rs.next())
	    {
	    	Log.customer.debug("BuyintDBRec::SysDate " +rs.getString("SYS_DATE"));
	    }
	    stmt.close();
	    rs.close();
    }
    
   /**
    * Gets next sequence from database
    */
    protected long getNextSeqVal (Connection conn)
        throws SQLException
    {
        Statement stmt = conn.createStatement();
        ResultSet rs   = stmt.executeQuery(getSeqSQL());
        rs.next();
        long id = rs.getLong(1);
        stmt.close();
        rs.close();
        return id;
    }

    /**
     * Deletes the record from Export_Data table with null transaction data 
     * before writing to Exception_Recovery table.
     *
     */
    public boolean deleteNullTransRecord(String docID)
    throws SQLException
    {
      int liResult = 0;
      Log.customer.debug("BuyintDBRec::getDeleteSQL " +this.getDeleteSQL(docID));
      this.checkConnection();
      moPrep = moConn.prepareStatement(getDeleteSQL(docID));
      liResult = moPrep.executeUpdate();
      moPrep.close();
      return liResult > 0;
    }
      
   /**
    * Retrieves a single record based on where clause passed.
    *
    */
    public boolean getRec(String fsWhereClause)
    {
        return getRec(fsWhereClause, "TIN");
    }

   /**
    * Retrieves a single record based on where clause passed.
    *
    */
    public boolean getRec(String fsWhereClause, String fsOrderBy)
    {
        boolean lboolSuccess = false;
        Statement loStmt = null;
        ResultSet loResult = null;
        String lsQuery = null;
        try
        {
            this.checkConnection();
            this.clear();
            lsQuery = getSelectString(msTableName) + fsWhereClause;
            if (fsOrderBy != null)
            {
               lsQuery += " order by " + fsOrderBy;
            }

            loStmt = moConn.createStatement();
            loResult = loStmt.executeQuery(lsQuery);
            if (loResult.next())
            {
                loadFromRow(loResult);
                lboolSuccess = true;
            }
            else
            {
                setStatus(null);
                setTIN(0L);
            }
        }
        catch(SQLException loEx1)
        {
            loEx1.printStackTrace();
            // no point throwing exception or calling ExceptionHandler at this time
            // BuyintException loEx1a = new BuyintException("Error during getRec(" + fsWhereClause + "," + fsOrderBy + ")", BuyintException.EXCPT_DBIO_ERROR);
            lboolSuccess = false;
        }
        finally
        {
           try
           {
               if (loStmt != null)
               {
                  loStmt.close();
               }
               if (loResult != null)
               {
                  loResult.close();
               }
           }
           catch (SQLException loEx2)
           {
              // nothing to do
           }
        }
        return lboolSuccess;
    }

    /**
     * Saves the current record.
     *
     */
     public boolean saveRecord(String table)
         throws BuyintException
     {
         int liResult = 0;
         miCol = 0;
         try
          {
              //Set Update Columns
        	  debug("saveRecord::Called");
        	  msTableName = table;
        	  moConn.setAutoCommit(false);
        	  //this.checkConnection();
        	  this.setCommitColumns();
              liResult = moPrep.executeUpdate();
              moPrep.close();
              moConn.commit();
              moConn.setAutoCommit(true);
              debug("saveRecord::Finished Committing");
          }
          catch(SQLException loEx1)
          {
        	  debug("saveRecord::SQLException");
        	  loEx1.printStackTrace();
              throw new BuyintException("Error during update", BuyintException.EXCPT_DBIO_ERROR);
          }
          catch(Exception loEx2)
          {
        	  debug("saveRecord::Exception");
              loEx2.printStackTrace();
              throw new BuyintException("Error during update", BuyintException.EXCPT_DBIO_ERROR);
          }
         return liResult > 0;
     }
    
   /**
    * Saves the current record.
    *
    */
    public synchronized boolean save()
        throws BuyintException
    {
        BuysenseUtil.showCallerInfo(new Throwable(), this.getClass().getName(), this);

        int liResult = 0;
        miCol = 0;

        if (getTIN() == 0L)
        {
            try
            {
                this.checkConnection();
                this.printSysDate(moConn);
                if(!foundDuplicate(moConn))
                {
	                
                	// this is an insert
	                mlSeq = getNextSeqVal(moConn);
	                this.setInsertColumns();
	                liResult = moPrep.executeUpdate();
	                setTIN(mlSeq);
	                moPrep.close();
	                // now update the CLOB
	                Log.customer.debug("BuyintDBRec::AutoCommit Status before saveTransactionData " +moConn.getAutoCommit());
	                saveTransactionData();
	                Log.customer.debug("BuyintDBRec::AutoCommit Status after saveTransactionData " +moConn.getAutoCommit());
	                Log.customer.warning(10004, this.getDocumentID());
                }
            }
            catch(SQLException loEx1)
            {
                loEx1.printStackTrace();
                throw new BuyintException("Error during insert", BuyintException.EXCPT_DBIO_ERROR);
            }
            catch(IOException loEx2)
            {
                loEx2.printStackTrace();
                throw new BuyintException("Error during insert", BuyintException.EXCPT_DBIO_ERROR);
            }
        }
        else
        {
            try
            {
                // this is an update
                this.setUpdateColumns();

                liResult = moPrep.executeUpdate();
                moPrep.close();
                // now update the CLOB
                saveTransactionData();
            }
            catch(SQLException loEx1)
            {
                loEx1.printStackTrace();
                throw new BuyintException("Error during update", BuyintException.EXCPT_DBIO_ERROR);
            }
            catch(Exception loEx2)
            {
                loEx2.printStackTrace();
                throw new BuyintException("Error during update", BuyintException.EXCPT_DBIO_ERROR);
            }
        }
        return liResult > 0;
    }

   /** Update TransactionData CLOB column with value set in current record.
    *
    */
    public void saveTransactionData()
        throws SQLException, IOException
    {
        byte [] lbBytes = null;
        CLOB loCLOB = null;
        OutputStream loCLOBOutputStream = null;
        Statement loStmt = null;
        ResultSet loResult = null ;
        String lsData = getTransactionData();

        if (lsData != null && lsData.length() > 0)
        {
            lbBytes = lsData.getBytes();
            moConn.setAutoCommit(false);
            loStmt = moConn.createStatement();
            loResult = loStmt.executeQuery(getSelectForUpdate(msTableName, getTIN()));

            loResult.next();
            loCLOB = (CLOB)loResult.getClob( 1 );
            loCLOBOutputStream = loCLOB.getAsciiOutputStream();
            loCLOBOutputStream.write( lbBytes );
            loCLOBOutputStream.flush();

            loCLOBOutputStream.close();
            loStmt.close();
            loResult.close();
            moConn.commit();
            moConn.setAutoCommit(true);
        }
    }
   /**
    * Returns the current row of the ResultSet into the current BuyintDBRec.
    *
    * @param results ResultSet to be converted
    *
    */
    public void loadFromRow(ResultSet results)
        throws SQLException
    {
        ResultSetMetaData rsMeta = results.getMetaData();
        int cols = rsMeta.getColumnCount();          // initialize nbr of columns
        String colsValue, colsName;

        for (int j=1; j <= cols; j++)
        {   // database column identifier is 1-based
            colsName = rsMeta.getColumnLabel(j);
            colsValue = getColValueAsString(results, rsMeta, j);
            setValue(colsName,colsValue);
        }
    }

    /**
      * Returns the column value as a String.
      * @return String
      */
    public String getColValueAsString(ResultSet foResult, ResultSetMetaData foMeta, int fiCol)
        throws SQLException
    {
        String lsColValue = null;
        CLOB loClob = null;
        int liClobLength = 0;
        int liEffectiveLength = 0;

        int liColType = foMeta.getColumnType(fiCol);
        if (liColType == java.sql.Types.BIGINT)
        {
            lsColValue = String.valueOf(foResult.getLong(fiCol));
        }
        else if (liColType == java.sql.Types.CLOB)
        {
            debug("Starting CLOB column processing ... ");
            loClob = (CLOB)foResult.getClob(fiCol);
            liClobLength = (new Long(loClob.length())).intValue();
            debug("CLOB.length() = " + liClobLength);
            if (loClob == null || liClobLength == 0)
            {
                lsColValue = null;
            }
            else
            {
                lsColValue = getStreamAsString(loClob.getAsciiStream());
                debug("String length = " + lsColValue.length());
                // Look for null (ascii 0)
                if (lsColValue.indexOf(0) > 0)
                {
                    debug("Data contains nulls ... will use to figure out length");
                    liEffectiveLength = lsColValue.indexOf(0);
                }
                else
                {
                    debug("Data contains no nulls ... will use CLOB length");
                    liEffectiveLength = liClobLength;
                }
                debug("Effective length of CLOB column=" + liEffectiveLength);
                if (liEffectiveLength < lsColValue.length())
                {
                    lsColValue = lsColValue.substring(0, liEffectiveLength);
                }
            }
        }
        else
        {
            lsColValue = foResult.getString(fiCol);
        }

        if (lsColValue == null || lsColValue == "null")
        {
             lsColValue = "";
        }

        return lsColValue;
    }

    /**
      * Loads properties
      * @return void
      */
    public void loadFromString(String foString)
    {
        try
        {
            super.load(getStringAsStream(foString));
        }
        catch (Exception loEx)
        {
            loEx.printStackTrace();
        }

        // Convert propertynames to uppercase
        for (Enumeration ee = this.propertyNames(); ee.hasMoreElements();) {
            String k =(String)ee.nextElement();
            String v = (String)this.remove(k);     // "remove" method returns original value
            this.setValue(k,v);
        }
    }

    /**
      * Loads properties from file name passed
      * @return void
      */

    public void loadFromFile (String inFile)
    {
        this.loadFromString(getFileAsString(inFile));
    }

   /** Returns the current TIN.
    * @return TIN.
    */
    protected long getTIN()
    {
        return getLongValue(COL_TIN, 0L);
    }

    /** Returns the current TIN.
     * @return TIN.
     */
     protected String getTINString()
     {
         return getValue(COL_TIN);
     }
     /** Returns the current TIN.
      * @return TIN.
      */
      protected String getPONumber()
      {
          return getValue(COL_PO_NUMBER);
      }
   /** Returns the Document Type of the current record.
    * @return Document Type.
    */
    protected String getDocumentType()
    {
        return getValue(COL_DOCUMENT_TYPE);
    }

   /** Returns the Document ID on the current record.
    * @return Document ID.
    */
    protected String getDocumentID()
    {
        return getValue(COL_DOCUMENT_ID);
    }

   /** Returns the current TIN
    *
    */
    protected String getTransactionType()
    {
        return getValue(COL_TRANSACTION_TYPE);
    }

   /** Returns the status of the current record.
    * @return Record status.
    */
    protected String getStatus()
    {
        return getValue(COL_STATUS);
    }

   /** Returns the data in the Clob as a String.
    * @return Transaction (XML) Data.
    */
    protected String getTransactionData()
    {
        return getValue(COL_TRANSACTION_DATA,"");
    }

    /** Returns the data in the Clob as a String.
     * @return Transaction (XML) Data.
     */
     protected String getImportData()
     {
         return getValue(COL_IMPORT_DATA,"");
     }
   /** Returns Error Message
    * @return Error Message on the current record.
    */
    protected String getErrorMessage()
    {
        return getValue(COL_ERROR_MESSAGE);
    }

    /** Returns Error Message
     * @return Error Message on the current record.
     */
     protected String getErrors()
     {
         return getValue(COL_ERRORS);
     }

     /** Returns receipt number for update.
      * @return receipt number for update.
      */
      protected String getReceiptNumber()
      {
          return getValue(COL_RC_NUMBER);
      }

      /** Returns processed PO Number for update.
       * @return processed PO Number for update.
       */
       protected String getProcessedPONumber()
       {
           return getValue(COL_PROCD_PO_NUMBER);
       }
     
   /** Returns the retry count.
    * @return Retry count of the current record.
    */
    protected int getRetryCount()
    {
        return getIntValue(COL_RETRY_COUNT,0);
    }

    /** Returns the retry count.
     * @return Retry count of the current record.
     */
     protected int getAttempts()
     {
         return getIntValue(COL_ATTEMPTS,0);
     }

   /** Returns a String representation of the Creation Date.
    *  @return Creation Date
    */
    protected String getCreationDate()
    {
        return getValue(COL_CREATION_DATE);
    }

   /** Returns a String representation of the Process Date.
    *  @return Process Date
    */
    protected String getProcessDate()
    {
        return getValue(COL_PROCESS_DATE);
    }

   /**
      * Returns the parameter value.
      * @return String
      */
    public String getValue (String fsName, String fsDefault) {
        String lsVal = getValue(fsName);
        if (lsVal == null)
        {
            lsVal = fsDefault;
        }
        return lsVal;
    }

   /**
      * Returns the parameter value, substituting for variables if needed
      * @return String
      */
   public String getValue (String inString) {
      String s = super.getProperty(inString.toUpperCase());
      if (s != null)
      {
          if (s.indexOf(VAR_DELIM) >= 0)
          {
              if (s.indexOf(VAR_DELIM+inString+VAR_DELIM) >= 0)
              {
                  s = null;      // circular reference
              }
              else
              {
                  s = subVars(s);
              }
          }
      }
      return s;
   }

    /**
      * Gets the value as an int
      */
    public int getIntValue(String fsName, int fiDefault)
    {
        int liVal = fiDefault;
        String lsVal = getValue(fsName);
        if (lsVal != null)
        {
            try
            {
                liVal = Integer.parseInt(lsVal);
            }
            catch (NumberFormatException loEx1)
            {
                // default goes thru
            }
        }

        return liVal;
    }

   /**
    * getBooleanValue allows retrieving values from the BuyintDBRec object
    * that are known to be boolean. Anything other than "true" (case insensitive)
    * returns "false".
    *
    * @param key String value to key on
    */
    public boolean getBooleanValue(String fsName, boolean fboolDefault) {
        boolean lboolResult = fboolDefault;
        String lsVal = getValue(fsName);
        if (lsVal != null)
        {
            if (lsVal.equalsIgnoreCase("TRUE"))
            {
                lboolResult = true;
            }
            else
            {
                lboolResult = false;
            }
        }
        return lboolResult;
    }

   /**
      * Gets the value as a long.
      */
    public long getLongValue(String fsName, long flDefault)
    {
        long llVal = flDefault;
        String lsVal = getValue(fsName);
        if (lsVal != null)
        {
            try
            {
                llVal = Long.parseLong(lsVal);
            }
            catch (NumberFormatException loEx1)
            {
                // default goes thru
            }
        }

        return llVal;
    }

   /**
      * Gets the value as a long.
      */
    public BigDecimal getBigDecimalValue(String fsName, BigDecimal foDefault)
    {
        BigDecimal loVal = foDefault;
        String lsVal = getValue(fsName);
        if (lsVal != null)
        {
            try
            {
                loVal = new BigDecimal(lsVal);
            }
            catch (NumberFormatException loEx1)
            {
                // default goes thru
            }
        }

        return loVal;
    }

   /**
      * Sets the property value. If being set to null, removes
      * the property.
      */
    public void setValue (String inProp, String inString)
    {
        if (inString == null)
        {
            super.remove(inProp);
        }
        else
        {
            super.setProperty(inProp.toUpperCase(), inString);
        }
    }

   /** Sets the current TIN.
    * @param flTIN The current TIN as a long value.
    */
    protected void setTIN(long flTIN)
    {
        setValue(COL_TIN, String.valueOf(flTIN));
    }

   /** Sets the current DocumentType.
    * @param fsDocType The current Doc Type.
    */
    protected void setDocumentType(String fsDocType)
    {
        setValue(COL_DOCUMENT_TYPE, fsDocType);
    }

   /** Sets the current Document ID.
    * @param fsDocID The current Doc ID.
    */
    protected void setDocumentID(String fsDocID)
    {
        setValue(COL_DOCUMENT_ID, fsDocID);
    }

   /** Sets the current Transaction Type.
    * @param fsTransType The current Transaction Type.
    */
    protected void setTransactionType(String fsTranType)
    {
        setValue(COL_TRANSACTION_TYPE, fsTranType);
    }

   /** Sets the receipt number for update.
    * @param fsReceiptNumber for update.
    */
    protected void setReceiptNumber(String fsReceiptNumber)
    {
        setValue(COL_RC_NUMBER, fsReceiptNumber);
    }

    /** Sets processed PO Number for update.
     * @return processed PO Number for update.
     */
     protected void setProcessedPONumber(String fsPOProcessedNumber)
     {
         setValue(COL_PROCD_PO_NUMBER, fsPOProcessedNumber);
     }

   /** Sets the status of the current record .
    * @param fsStatus The current status.
    */
    protected void setStatus(String fsStatus)
    {
        setValue(COL_STATUS, fsStatus);
    }

   /** Sets the Transaction Data.
    * @param fsData The current Transaction data.
    */
    protected void setTransactionData(String fsData)
    {
        setValue(COL_TRANSACTION_DATA, fsData);
    }

   /** Sets the Error Message for the current record.
    * @param fsMessage Error Message.
    */
    protected void setErrorMessage(String fsMessage)
    {
        setValue(COL_ERROR_MESSAGE, fsMessage);
    }

    /** Sets the Error Message for the current record.
     * @param fsMessage Error Message.
     */
     protected void setErrors(String fsMessage)
     {
         setValue(COL_ERRORS, fsMessage);
     }
    
   /** Sets the retry count on the record.
    * @param fiRetry New retry count for record.
    */
    protected void setRetryCount(int fiRetry)
    {
        setValue(COL_RETRY_COUNT,String.valueOf(fiRetry));
    }

    /** Sets the retry count on the record.
     * @param fiRetry New retry count for record.
     */
     protected void setAttempts(int fiRetry)
     {
         setValue(COL_ATTEMPTS,String.valueOf(fiRetry));
     }

   /**
     * Substitutes INI param variables in the passed string
     * @return String
     */
    public String subVars (String inString) {
        StringBuffer sb = new StringBuffer(inString);
        int i = 0;
        int j = 0;
        while ((i = sb.toString().indexOf(VAR_DELIM)) >= 0)
        {
            if ((j = sb.substring(i+2).toString().indexOf(VAR_DELIM)) > 0)
            {
                String var = sb.substring(i+2,i+2+j).toString();
                sb = sb.replace(i,i+2+j+2,this.getValue(var));
            }
            else
            {
                break;             // don't interpret odd number of delimiters
            }
        }
        return sb.toString();
    }

   /**
     * Returns the contents of a file as linefeed separated string
     */
    public static String getFileAsString(String fileName) {
        String filestring=null;
        File _file=new File(fileName);
        FileInputStream in;
        ByteArrayOutputStream out;

        try
        {
            in=new FileInputStream(_file);
            byte[] buffer=new byte[4096];
            out = new ByteArrayOutputStream();

            int bytes_read;
            while ((bytes_read = in.read(buffer)) != -1) {
                out.write(buffer, 0, bytes_read);
                //String(byte[] bytes, int offset, int length) method can also be used
                filestring = out.toString();
            }
            in.close();
            out.close();
         }
         catch (Exception e) {
             e.printStackTrace();
         }
         return filestring;
     }

    /**
      * Returns the passed String as an InputStream
      */
    public static ByteArrayInputStream getStringAsStream(String inString) {
        byte [] buf1 = null;
        buf1 = inString.getBytes();
        return  new ByteArrayInputStream (buf1);
    }


    public static String getStreamAsString(InputStream foIS)
    {
        // close passed input stream by default
        return getStreamAsString(foIS, true);
    }

    /**
      * Returns the passed InputStream as String
      *
      */
    public static String getStreamAsString(InputStream foIS, boolean fboolClose)
    {
        int liBufSize = 4096;
        if (foIS == null)
        {
            return null;
        }

        StringBuffer loSB = new StringBuffer();
        BufferedReader loBR = null;
        int liCharsRead;
        char [] lcBuffer = new char[liBufSize];

        try
        {
            loBR = new BufferedReader(new InputStreamReader(foIS));
            while ((liCharsRead = loBR.read(lcBuffer, 0, liBufSize)) != -1)
            {
               loSB.append(lcBuffer,0,liCharsRead);
               lcBuffer = new char[liBufSize];
            }

            loBR.close();

            if (fboolClose)
            {
               foIS.close();
            }
        }
        catch (IOException loEx)
        {
            loEx.printStackTrace();
        }
        return loSB.toString();
    }

    /**
      * Returns the contents of a file as an InputStream
      */
    public static ByteArrayInputStream getFileAsStream(String inString) {
        String fileInfo = getFileAsString(inString);
        return getStringAsStream(fileInfo);
    }

    /**
      * Sets the prepared statment with the columns to be inserted
      */
    protected void setInsertColumns() throws BuyintException
    {
		try
        {
            moPrep = moConn.prepareStatement(getInsert(msTableName));

            moPrep.setLong(++miCol, mlSeq);
            moPrep.setString(++miCol, getDocumentType());
            moPrep.setString(++miCol, getDocumentID());
            moPrep.setString(++miCol, getTransactionType());
            moPrep.setString(++miCol, getStatus());
            // TRANSACTION_DATA is set to EMPTY_CLOB() in INSERT_SQL
            moPrep.setString(++miCol, getErrorMessage());
            moPrep.setInt(++miCol, getRetryCount());
            moPrep.setTimestamp(++miCol, new Timestamp(System.currentTimeMillis()));
            moPrep.setNull(++miCol, java.sql.Types.TIMESTAMP);   // process date to be set to null
        }
        catch(SQLException loEx1)
        {
		    loEx1.printStackTrace();
		    throw new BuyintException("Error during insert", BuyintException.EXCPT_DBIO_ERROR);
        }
    }

    /**
      * Sets the prepared statment with the columns to be updated
      */
    protected void setUpdateColumns() throws BuyintException
    {
	    try
	    {
            String lsSQL = getUpdateByTin(msTableName);
            moPrep = moConn.prepareStatement(lsSQL);

            // Key fields are never updated
            moPrep.setString(++miCol, getStatus());
            // TRANSACTION_DATA is not updated this way
            moPrep.setString(++miCol, getErrorMessage());
            moPrep.setInt(++miCol, getRetryCount());
            // CREATION_DATE is never updated
            moPrep.setTimestamp(++miCol, new Timestamp(System.currentTimeMillis()));
            moPrep.setLong(++miCol, getTIN());
        }
        catch(SQLException loEx1)
        {
		    loEx1.printStackTrace();
		    throw new BuyintException("Error during insert", BuyintException.EXCPT_DBIO_ERROR);
        }
    }

    /**
     * Sets the prepared statment with the columns to be updated
     */
   protected void setCommitColumns() throws BuyintException
   {
	    try
	    {
	       debug("setCommitColumns::Called::msTableName " +msTableName);
	       
	       String lsSQL = getUpdateQuery(msTableName);

	       debug("setCommitColumns:: lsSQL" +lsSQL);
	       debug("setCommitColumns:: getStatus()" +getStatus());
	       debug("setCommitColumns:: getReceiptNumber()" +getReceiptNumber());
	       debug("setCommitColumns:: getProcessedPONumber()" +getProcessedPONumber());
	       debug("setCommitColumns:: getErrors()" +getErrors());
	       debug("setCommitColumns:: getAttempts()" +getAttempts());
	       debug("setCommitColumns:: getTINString()" +getTINString());
	       
           moPrep = moConn.prepareStatement(lsSQL);
           moPrep.setString(++miCol, getStatus());
           moPrep.setString(++miCol, getReceiptNumber());
           moPrep.setString(++miCol, getProcessedPONumber());
           moPrep.setString(++miCol, getErrors());
           moPrep.setInt(++miCol, getAttempts());
           moPrep.setTimestamp(++miCol, new Timestamp(System.currentTimeMillis()));
           moPrep.setString(++miCol, getTINString());
       }
       catch(SQLException loEx1)
       {
		    loEx1.printStackTrace();
		    throw new BuyintException("Error during insert", BuyintException.EXCPT_DBIO_ERROR);
       }
   }
    
    public void debug(String fsMsg)
    {
        Log.customer.debug("BuyintDBRec: " + fsMsg);
    }

}
