package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionResult;
import ariba.base.fields.ValueInfo;
import ariba.purchasing.core.ReqLineItem;
import ariba.util.core.Fmt;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class CheckBuysenseReceivingMethod extends Condition
{
  
    private static final ValueInfo parameterInfo[];
    String message = null;
    @Override
    public boolean evaluate(Object obj, PropertyTable params) 
    {
        int iReceivingType;
        // TODO Auto-generated method stub
        BaseObject obj1   = (BaseObject)params.getPropertyForKey("ReqLineItem");
        Log.customer.debug("The class CheckBuysenseReceivingMethod.java called");
        if (obj1 instanceof ReqLineItem)
        {
            Object oEnableReceivingType =  obj1.getDottedFieldValue("ClientName.EnableReceivingType");
            
            if(oEnableReceivingType != null )
            {
                Boolean isReceivingTypeEnabled = (Boolean)oEnableReceivingType;
                if(!isReceivingTypeEnabled)
                {
                    return true;
                }
            }
        java.math.BigDecimal loMoney = (java.math.BigDecimal)(obj1.getDottedFieldValue("Amount.Amount"));
        iReceivingType = ((Integer) obj1.getFieldValue("BuysenseReceivingType")).intValue();

        if(loMoney.doubleValue() < 0 && iReceivingType == 3)
        {
            message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","NegativeAmountLineSubmitError");
            return false;
        }
    }
        return true;
    }
    
    public ConditionResult evaluateAndExplain(Object obj, PropertyTable params)
    {         
         if (!evaluate(obj, params))
           {
              if(!StringUtil.nullOrEmptyOrBlankString(message));
              {
                  return new ConditionResult(message);
              }           
           }
           else
           {
              return null;
           }     
    }    
    
    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    public CheckBuysenseReceivingMethod()
    {
    }

    static
    {
        parameterInfo = (new ValueInfo[]{ new ValueInfo("ReqLineItem", 0)});
    }
}