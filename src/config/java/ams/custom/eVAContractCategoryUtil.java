/*
   Author           : Daniel Charlton / Ariba
   Date of creation : 08-19-2009
   Purpose          : Custom controller of Invited Suppliers
   Change Log	  :

Ver   Date        Modified By       Description
------------------------------------------------------------------------------------
1.0   09/09/09    Daniel Charlton   complete logic to filter invited suppliers.
1.1   09/29/09    Daniel Charlton   add logic to allow positive and negative logic in override.
*/

package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.NamedPair;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLResultCollection;
import ariba.common.core.CommonSupplier;
import ariba.contract.core.category.ContractCategoryUtil;
import ariba.procure.core.CategoryDefinition;
import ariba.procure.core.CategoryLineItemDetails;
import ariba.procure.core.LineItemProductDescription;
import ariba.procure.core.Log;
import ariba.procure.core.MappableProperties;
import ariba.procure.core.ProcureLineItem;
import ariba.procure.core.RankedSupplier;
import ariba.user.core.OrganizationID;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;
import java.util.Iterator;
import java.util.List;
import ariba.procure.core.CategoryItem;
import ariba.purchasing.core.ReqLineItem;

public class eVAContractCategoryUtil extends ContractCategoryUtil
{
    public static boolean Debug = true; // for debugging, set to false in Prod
    public static final String ClassName = "config.java.COVALaborProcureCategoryUtil";
    public static final String Duns_Domain = "duns";
    public static final String KeyCommonSupplierID =  "CommonSupplierID";
    public static final String KeyExperienceLevel =  "yearsexperience";
    public static final String KeyQueryParameter = "Application.CategoryProcurement.SupplierRankingOverrideQuery";
    public static final String KeyRegion = "zone";
    public static final String KeyAllowed = "Allowed";
    public static final String SupplierRankingOverrideQuery = "SELECT " + KeyCommonSupplierID + " FROM ariba.core.BuysenseSupplierRankingOverride WHERE UPPER(CategoryDefinitionName) = UPPER('%s') AND (UPPER(\"Zone\") = '%s' OR UPPER(\"Zone\") = 'ALL') AND (UPPER(ExperienceLevel) = UPPER('%s') OR UPPER(ExperienceLevel) = 'ALL') AND (UPPER(ItemID) = UPPER('%s') OR UPPER(ItemID) = 'ALL') AND UPPER(" + KeyAllowed + ") = 'TRUE'"  + " AND "   + KeyCommonSupplierID + " NOT IN (" + "SELECT " + KeyCommonSupplierID + " FROM ariba.core.BuysenseSupplierRankingOverride WHERE UPPER(CategoryDefinitionName) = UPPER('%s') AND (UPPER(\"Zone\") = UPPER('%s') OR UPPER(\"Zone\") = 'ALL') AND (UPPER(ExperienceLevel) = UPPER('%s') OR UPPER(ExperienceLevel) = 'ALL') AND (UPPER(ItemID) = UPPER('%s') OR UPPER(ItemID) = 'ALL') AND UPPER(" + KeyAllowed + ") = 'FALSE'" + ")";

    public static String Query;
    public static AQLOptions Options = new AQLOptions();

    public List getSuppliersForItem(ProcureLineItem pli, boolean requiredSuppliersOnly)
    {
		List supplierList = super.getSuppliersForItem(pli, requiredSuppliersOnly);

		//Filter out suppliers based on logic
		try
		{
			supplierList = removeSuppliersNotValidForGivenAttributes(supplierList, getSuppliersValidForGivenAttributes(pli));
		}
		catch (Exception e)
		{
   	    	log(" caught exception " + e, true);
		}
		finally
		{
   	    	log(" returning " + supplierList);
   	    	return supplierList;
		}
	}

	// given the item attributes entered by the user, generate a list of suppliers which are valid for that item and those attributes
	public List getSuppliersValidForGivenAttributes(ProcureLineItem lineItem)
	{
		AQLResultCollection results;
        Options.setRowLimit(0);
        Options.setPartition(lineItem.getPartition());  // set partition so that the search is multi-variant safe
		Options.setUseCache(!Debug);

		// get and execute query to find valid suppliers based on item attributes
        results = Base.getService().executeQuery(getQueryForValidSuppliersGivenAttributes(lineItem), Options);

        // build list of suppliers from query results
        List validSuppliers = ListUtil.list();
        if (results.getErrors() == null && results.getSize()>0)
        {
			while (results.next())
			{
				validSuppliers.add(results.getString(KeyCommonSupplierID));
			}
		}

		// return list of valid suppliers for this line item, given the attributes selected by the user
		log(" getSuppliersValidForGivenAttributes() returning " + validSuppliers);
		return validSuppliers;
	}

	// get query for BuysenseSupplierRankingOverride objects, and interpolate the line item attribute values
	public String getQueryForValidSuppliersGivenAttributes(ProcureLineItem pli)
	{
		log("getQueryForValidSuppliersGivenAttributes(pli " + pli + ", getSuppliersValidForGivenAttributes())");

// get Category Definition
String categoryDefinitionName = (String)pli.getDottedFieldValue("CategoryLineItemDetailsVector[0].CategoryDefinition.Name");
// get category item id for this procurement line item
String itemID = (String)pli.getDottedFieldValue("CategoryLineItemDetailsVector[0].CategoryItem.ItemId");

		// get category information from line item
		if (pli instanceof ReqLineItem)
		{
log("pli " + pli +  " is a ReqLineItem.");
			CategoryLineItemDetails categoryLineItemDetails = ((ReqLineItem)pli).getCategoryLineItemDetails();
log("categoryLineItemDetails: " + categoryLineItemDetails);
			if (null != categoryLineItemDetails)
			{
				// get category definition
				CategoryDefinition categoryDefinition = categoryLineItemDetails.getCategoryDefinition();
log("categoryDefinition: " + categoryDefinition );
				if (null == categoryDefinition)
				{
					categoryDefinitionName = categoryDefinition.getUniqueName();
					log("got categoryDefinitionName through accessors " + categoryDefinitionName);
				}

				// item get item id
				CategoryItem categoryItem = categoryLineItemDetails.getCategoryItem();
log("categoryItem: " + categoryItem);
				if (null != categoryItem)
				{
					itemID = categoryItem.getItemId();
				}
			}
		}else{log("pli " + pli +  " is not a ReqLineItem.");}

		// get zone attribute value for this procurement line item
		String zone = getValueForAttribute(pli, KeyRegion);
		// get experience level attribute value for this procurement line item
		String explevel = getValueForAttribute(pli, KeyExperienceLevel);

		log(" categoryDefinitionName "+categoryDefinitionName);
		log(" ItemID: " + itemID);
		log(" zone: " + zone);
		log(" explevel: " + explevel);

		// interpolate attribute values into query
/*
		List argumentValues = ListUtil.list();
		argumentValues.add(categoryDefinitionName);
		argumentValues.add(zone);
		argumentValues.add(explevel);
		argumentValues.add(itemID);
		argumentValues.add(categoryDefinitionName);
		argumentValues.add(zone);
		argumentValues.add(explevel);
		argumentValues.add(itemID);
*/
		String[] argumentValues = new String[8];
		argumentValues[0] = categoryDefinitionName;
		argumentValues[1] = zone;
		argumentValues[2] = explevel;
		argumentValues[3] = itemID;
		argumentValues[4] = categoryDefinitionName;
		argumentValues[5] = zone;
		argumentValues[6] = explevel;
		argumentValues[7] = itemID;

		String query = Fmt.S(getQuery(pli.getPartition()), argumentValues);
		log(" resulting query is: " + query);

		return query;
	}

	// get generic query for BuysenseSupplierRankingOverride objects
	public String getQuery(Partition partition)
	{
		log("getQuery(Partition "+partition+")");

		// get query from ReportQuery

		// if query is null, set it to the value from the Parameters.table file
		log("query is " + Query);
		if (null == Query)
		{
			// get value from Parameters.table
			log("query is null getting from Parameters.table" );
			Query = Base.getService().getParameter(partition,KeyQueryParameter);
			log("got query from Parameters.table " + Query);
			// if the value from the parameters.table is null, use this hardcoded default
			if (null == Query)
			{
				log("query from Parameters.table was null, using default" );
			// can we use baseid lookup instead of category name string lookup for speed?
				Query = SupplierRankingOverrideQuery;
			}
		}
		log("query is " + Query);
		return Query;
	}

	public List removeSuppliersNotValidForGivenAttributes(List supplierList, List validSuppliers)
	{
   	    log("removeSuppliersNotValidForGivenAttributes(supplierList " + supplierList + ", validSuppliers " + validSuppliers + ")");

   	    log("validSuppliers: " + validSuppliers);
 		for (int i=validSuppliers.size()-1; i>=0; i--)
		{
			String supplierID = (String)validSuppliers.get(i);
			log(" validSupplier: " + supplierID );
		}

		log(" filtering supplierList: " + supplierList );

 		RankedSupplier rankedSupplier;
 		for (int i=supplierList.size()-1; i>=0; i--)
		{
			rankedSupplier = (RankedSupplier)supplierList.get(i);
			if (rankedSupplier != null)
			{
				// get common supplier from ranked supplier
				CommonSupplier commonSupplier = rankedSupplier.getSupplier();
				log(" looking for commonSupplier "+commonSupplier+" in supplierList: " + supplierList );
				if (commonSupplier != null)
				{
					// get organization id for common supplier for given domain
					OrganizationID commonSupplierIDs = commonSupplier.getOrganizationID();
					String commonSupplierID = commonSupplierIDs.getValueForDomain(Duns_Domain);
					// does this organization id exist in the list of valid suppliers?
					log(" looking for commonSupplierID "+commonSupplierID +" in supplierList: " + supplierList );
					if (!validSuppliers.contains(commonSupplierID))
					{
						// if not, remove it
						log(" removing record from InvitedSuppliers" );
						supplierList.remove(rankedSupplier);
					}
				}
			}
		}
		log(" supplierList is now: " + supplierList );
        return supplierList;
	}

	String getValueForAttribute (ProcureLineItem pli, String attributeName)
	{
		// get category line attributes selected by user (these attributes must be required)
		
		//START: Deleted, Added and modified by SRINI for CSPL-2788. See P4 for detailed changes.
		String lsAttrValue = "";
		CategoryLineItemDetails loCategoryLineItemDetails = ((ReqLineItem)pli).getCategoryLineItemDetails();
		if(loCategoryLineItemDetails != null)
		{
			MappableProperties loMappableProperties = (MappableProperties) loCategoryLineItemDetails.getMappableProperties();
			if(loMappableProperties != null)
			{
				lsAttrValue = (String) loMappableProperties.getFieldValue(attributeName);
			}
		}
		Log.customer.debug(ClassName + " returning lsAttrValue" +lsAttrValue);
		return lsAttrValue;
		//END: Deleted, Added and modified by SRINI for CSPL-2788. See P4 for detailed changes.
	}

	void log(String message)
	{
		log(ClassName + " " + message, Debug);
	}

	void log(String message, boolean shouldWrite)
	{
		if (shouldWrite)
		{
			Log.categoryProcurement.debug(ClassName + " " + message);
		}
	}
}
