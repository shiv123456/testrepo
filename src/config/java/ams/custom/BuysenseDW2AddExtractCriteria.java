package config.java.ams.custom;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLFieldExpression;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.collaboration.core.CollaborationRequest;
import ariba.collaboration.core.CollaborationThread;
import ariba.collaboration.core.CounterProposal;
import ariba.collaboration.core.Proposal;
import ariba.purchasing.core.Requisition;
import ariba.util.core.ListUtil;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

import com.buysense.dw.BuysenseDW2Export;

public class BuysenseDW2AddExtractCriteria
{
    private static final String ClassName   = "BuysenseDW2AddExtractCriteria";
    private static Partition    moPartition = null;
    private static int          iMaxINcount = BuysenseDW2Export.iMaxINcount;
    
    
    public static String getCATATTRIBUTES_CATEGORYChangeCriterea(String fromDate, String toDate, ArrayList<BaseId> loBaseIds)
    {
        Log.customer.debug(ClassName + "::getCATATTRIBUTES_CATEGORYChangeCriterea  fromDate:" + fromDate + " toDate:" +  toDate);
        String sReturnQuery ="";
        if(loBaseIds!= null && !loBaseIds.isEmpty())
        {
            sReturnQuery = " this IN (";
            for (int i = 0; i < loBaseIds.size(); i++)
            {
                sReturnQuery = sReturnQuery +" BaseId('"+loBaseIds.get(i).toBase36String()+"'),";
            }
            sReturnQuery = sReturnQuery.substring(0, sReturnQuery.length()-1)+") ";
        }
        AQLResultCollection loBuysenseClientResultSet;
        AQLQuery loWhereQuery = AQLQuery.parseQuery("select Name,IsLatest from ariba.catalog.base.core.ComplexTypeImpl where Name IN ('medicallabor', 'generallabor') AND IsLatest = True AND TimeUpdated >=" + fromDate + " AND TimeUpdated <  " + toDate);
        // check if we can check on count(*) result set - JB
        AQLOptions loOptions = new AQLOptions();
        moPartition = Base.getService().getPartition("pcsv");
        loOptions.setUserLocale(Base.getSession().getLocale());
        loOptions.setUserPartition(moPartition);
        String sCatDriverName = "";
        String sInClause = "";
        loBuysenseClientResultSet = (AQLResultCollection) Base.getService().executeQuery(loWhereQuery, loOptions);

        if (loBuysenseClientResultSet.getFirstError() != null)
        {            
            Log.customer.warning(8888, ClassName + "::run() AQL execution error: " + loBuysenseClientResultSet.getFirstError().toString());
            return "";
        }
        while(loBuysenseClientResultSet.next())
        {
            sCatDriverName = loBuysenseClientResultSet.getString("Name");
            sInClause = sInClause+"'_"+sCatDriverName+"'"+",";
        }
        if(!StringUtil.nullOrEmptyOrBlankString(sInClause))
        {
            Log.customer.debug(ClassName + ":: sInClause: "+sInClause);
            if(!StringUtil.nullOrEmptyOrBlankString(sReturnQuery)) // add OR only if sReturnQuery is non-null
            {
            	sReturnQuery = sReturnQuery + " OR ";
            }
            sReturnQuery = sReturnQuery + " (HeaderCategoryDriverName  IN ("+sInClause.substring(0, sInClause.length()-1)+") )";
        }

        Log.customer.debug(ClassName + ":: did not find any records");
        return sReturnQuery;

    }

    public static String getCATATTRIBUTES_ATTRIBUTESChangeCriterea(String fromDate, String toDate, ArrayList<BaseId> loBaseIds)
    {
        Log.customer.debug(ClassName + "::getCATATTRIBUTES_ATTRIBUTESChangeCriterea  fromDate:" + fromDate + " toDate:" +  toDate);
        String sReturnQuery ="";
        if(loBaseIds!= null && !loBaseIds.isEmpty())
        {
            sReturnQuery = " this IN (";
            for (int i = 0; i < loBaseIds.size(); i++)
            {
                sReturnQuery = sReturnQuery +" BaseId('"+loBaseIds.get(i).toBase36String()+"'),";
            }
            sReturnQuery = sReturnQuery.substring(0, sReturnQuery.length()-1)+") ";
        }
        AQLResultCollection loBuysenseClientResultSet;
        AQLQuery loWhereQuery = AQLQuery.parseQuery("select HeaderCategoryDriverName,UniqueName,Name from ariba.procure.core.CategoryDefinition where  HeaderCategoryDriverName IN ('_medicallabor','_generallabor') AND TimeUpdated >=" + fromDate + " AND TimeUpdated <  " + toDate);
        // check if we can check on count(*) result set - JB
        AQLOptions loOptions = new AQLOptions();
        moPartition = Base.getService().getPartition("pcsv");
        loOptions.setUserLocale(Base.getSession().getLocale());
        loOptions.setUserPartition(moPartition);
        String sCatDriverName = "";
        String sInClause = "";
        loBuysenseClientResultSet = (AQLResultCollection) Base.getService().executeQuery(loWhereQuery, loOptions);

        if (loBuysenseClientResultSet.getFirstError() != null)
        {            
            Log.customer.warning(8888, ClassName + "::run() AQL execution error: " + loBuysenseClientResultSet.getFirstError().toString());
            return "";
        }
        while(loBuysenseClientResultSet.next())
        {
            sCatDriverName = loBuysenseClientResultSet.getString("HeaderCategoryDriverName");
            sInClause = sInClause+"'"+sCatDriverName+"'"+",";
        }
        
        if(!StringUtil.nullOrEmptyOrBlankString(sInClause))
        {
            Log.customer.debug(ClassName + ":: sInClause: "+sInClause);
            if(!StringUtil.nullOrEmptyOrBlankString(sReturnQuery)) // add OR only if sReturnQuery is non-null
            {
            	sReturnQuery = sReturnQuery + " OR ";
            }
            sReturnQuery = sReturnQuery+ " ('_'||Name IN ("+sInClause.substring(0, sInClause.length()-1)+") AND IsLatest = True)";
        }

        Log.customer.debug(ClassName + ":: did not find any records");
        return sReturnQuery;

    }

    public static String getAPPLNS_POChangeCriterea(String fromDate, String toDate, ArrayList<BaseId> loBaseIds)
    {
        if(loBaseIds == null || (loBaseIds!= null && loBaseIds.isEmpty()))
        {
            return "";
        }

        AQLResultCollection loREQsResultSet;
        AQLQuery loWhereQuery = AQLQuery.parseQuery("Select po.LineItems.Requisition.UniqueName AS UniqueName from ariba.purchasing.core.PurchaseOrder po include inactive");
        AQLOptions loOptions = new AQLOptions();
        moPartition = Base.getService().getPartition("pcsv");
        loOptions.setUserLocale(Base.getSession().getLocale());
        loOptions.setUserPartition(moPartition);
        //loWhereQuery.andIn("this", loBaseIds);
        
        Log.customer.debug(ClassName + "::building IN clause - list: " + loBaseIds.size() + "; iMaxINcount: " + iMaxINcount);
        for (int i = 0; i < loBaseIds.size(); i += iMaxINcount)
        {
            List<BaseId> tempINList;
            tempINList = loBaseIds.subList(i, Math.min(loBaseIds.size(), i + iMaxINcount));
            if (i == 0)
            {
                loWhereQuery.andIn("this", tempINList);
            }
            else
            {
                loWhereQuery.or(AQLCondition.buildIn(loWhereQuery.buildField("this"), tempINList));
            }
        }
        
        Log.customer.debug(ClassName + "::getAPPLNS_POChangeCriterea  loWhereQuery:" +loWhereQuery);
        

        loREQsResultSet = (AQLResultCollection) Base.getService().executeQuery(loWhereQuery, loOptions);
        List<String> loReqUNList = ListUtil.list();
        if (loREQsResultSet.getFirstError() != null)
        {            
            return "";
        }
        while(loREQsResultSet.next())
        {
            String sReqUN = loREQsResultSet.getString("UniqueName");
            loReqUNList.add(sReqUN);
        }
        
        List<AQLCondition> conditionList = ListUtil.list();
        
        if(loReqUNList!= null && !loReqUNList.isEmpty())
        {  
            
            for (int i = 0; i < loReqUNList.size(); i += iMaxINcount)
            {
                List<String> tempINList;
                tempINList = loReqUNList.subList(i, Math.min(loReqUNList.size(), i + iMaxINcount));
                conditionList.add(AQLCondition.buildIn(new AQLFieldExpression("UniqueName"), tempINList));
            }
            
            AQLCondition condition = AQLCondition.buildOr(conditionList);
           // AQLCondition condition =  AQLCondition.buildIn(new AQLFieldExpression("UniqueName"), loReqUNList);
            Log.customer.debug(ClassName + "::getAPPLNS_POChangeCriterea() condition: "+condition.toString());
            return condition.toString();
        }

        Log.customer.debug(ClassName + ":: did not find any records returning dummy where clause");
        //Return dummy where clause when no records found to avoid where clause less search (which will return all Req objects)
        return " ";

    }
    public static String getPRP_ChangeCriterea(String fromDate, String toDate, ArrayList<BaseId> loBaseIds)
    {
        List<Requisition> loReqList = ListUtil.list();
        List<String> loPropUNList = ListUtil.list();
        List<AQLCondition> conditionList = ListUtil.list();

        for(int i=0; i<loBaseIds.size();i++)
        {
            Requisition loReq = (Requisition) loBaseIds.get(i).get();
            loReqList.add(loReq);
        }
        
        if(loReqList!= null && !loReqList.isEmpty())
        {

            for (int i = 0; i < loReqList.size(); i++)
            {
                Requisition loReq = loReqList.get(i); 
                Log.customer.debug(ClassName + "::getPRP_ChangeCriterea() loReq: " + loReq);
                for (Iterator loCollbReqs = loReq.getCollaborationRequestsIterator(); loCollbReqs.hasNext();)
                {
                    BaseId bi = (BaseId) loCollbReqs.next();              
                    CollaborationRequest loCollbReq = (CollaborationRequest) bi.get();
                    if (loCollbReq != null)
                    {
                        for (Iterator loCollabThreads = loCollbReq.getCollaborationThreadsIterator(); loCollabThreads.hasNext();)
                        {
                            BaseId biThread = (BaseId) loCollabThreads.next();              
                            CollaborationThread loCollabThread = (CollaborationThread) biThread.get();
                            for (Iterator loCollbThreadDocs = loCollabThread.getDocumentsIterator(); loCollbThreadDocs.hasNext();)
                            {
                                BaseId biThreadDoc = (BaseId) loCollbThreadDocs.next();
                                Object oDoc = biThreadDoc.get();
                                if(oDoc instanceof Proposal)
                                {
                                    Proposal loProp = (Proposal) oDoc;
                                    if (loProp != null)
                                    {
                                        Log.customer.debug(ClassName + "::getPRP_ChangeCriterea() loProp: " + loProp);
                                        int iThreadState = loProp.getThreadState().getState();
                                        Log.customer.debug(ClassName + "::getPRP_ChangeCriterea() iThreadState: " + iThreadState);
                                        if (iThreadState != 1 || iThreadState != 32)
                                        {
                                            loPropUNList.add(loProp.getUniqueName());
                                        }
                                    }
                                }
                            }
                        }
                    }

                }

            }

            if (loPropUNList != null && !loPropUNList.isEmpty())
            {
                conditionList.add(AQLCondition.buildIn(new AQLFieldExpression("UniqueName"), loPropUNList));
            }else
            {
                conditionList.add(AQLCondition.parseCondition("1=2"));
            }

            AQLCondition condition = AQLCondition.buildOr(conditionList);
            Log.customer.debug(ClassName + "::getPRP_ChangeCriterea() condition: " + condition.toString());
            return condition.toString();
        }

        Log.customer.debug(ClassName + ":: did not find any records returning dummy where clause");
        return " ";

    }
    public static String getCP_ChangeCriterea(String fromDate, String toDate, ArrayList<BaseId> loBaseIds)
    {
        List<Requisition> loReqList = ListUtil.list();
        List<String> loCpUNList = ListUtil.list();
        List<AQLCondition> conditionList = ListUtil.list();

        for(int i=0; i<loBaseIds.size();i++)
        {
            Requisition loReq = (Requisition) loBaseIds.get(i).get();
            loReqList.add(loReq);
        }
        
        if(loReqList!= null && !loReqList.isEmpty())
        {

            for (int i = 0; i < loReqList.size(); i++)
            {
                Requisition loReq = loReqList.get(i); 
                Log.customer.debug(ClassName + "::getCP_ChangeCriterea() loReq: " + loReq);
                for (Iterator loCollbReqs = loReq.getCollaborationRequestsIterator(); loCollbReqs.hasNext();)
                {
                    BaseId bi = (BaseId) loCollbReqs.next();              
                    CollaborationRequest loCollbReq = (CollaborationRequest) bi.get();
                    if (loCollbReq != null)
                    {
                        for (Iterator loCollabThreads = loCollbReq.getCollaborationThreadsIterator(); loCollabThreads.hasNext();)
                        {
                            BaseId biThread = (BaseId) loCollabThreads.next();              
                            CollaborationThread loCollabThread = (CollaborationThread) biThread.get();
                            for (Iterator loCollbThreadDocs = loCollabThread.getDocumentsIterator(); loCollbThreadDocs.hasNext();)
                            {
                                BaseId biThreadDoc = (BaseId) loCollbThreadDocs.next();
                                Object oDoc = biThreadDoc.get();
                                if(oDoc instanceof CounterProposal)
                                {
                                    CounterProposal loCp = (CounterProposal) oDoc;
                                    if (loCp != null)
                                    {
                                        int iThreadState = loCp.getThreadState().getState();
                                        Log.customer.debug(ClassName + "::getCP_ChangeCriterea() iThreadState: " + iThreadState);
                                        if (iThreadState != 1)
                                        {
                                            loCpUNList.add(loCp.getUniqueName());
                                        }
                                    }
                                }
                            }
                        }
                    }

                }

            }

            if (loCpUNList != null && !loCpUNList.isEmpty())
            {
                conditionList.add(AQLCondition.buildIn(new AQLFieldExpression("UniqueName"), loCpUNList));
            }else
            {
                conditionList.add(AQLCondition.parseCondition("1=2"));
            }

            AQLCondition condition = AQLCondition.buildOr(conditionList);
            Log.customer.debug(ClassName + "::getCP_ChangeCriterea() condition: " + condition.toString());
            return condition.toString();
        }

        Log.customer.debug(ClassName + ":: did not find any records returning dummy where clause");
        return " ";

    }
    public static String getCR_ChangeCriterea(String fromDate, String toDate, ArrayList<BaseId> loBaseIds)
    {
        List<Requisition> loReqList = ListUtil.list();
        List<String> loCrUNList = ListUtil.list();
        List<AQLCondition> conditionList = ListUtil.list();

        for(int i=0; i<loBaseIds.size();i++)
        {
            Requisition loReq = (Requisition) loBaseIds.get(i).get();
            loReqList.add(loReq);
        }
        
        if(loReqList!= null && !loReqList.isEmpty())
        {

            for (int i = 0; i < loReqList.size(); i++)
            {
                Requisition loReq = loReqList.get(i); 
                Log.customer.debug(ClassName + "::getCR_ChangeCriterea() loReq: " + loReq);
                for (Iterator loCollbReqs = loReq.getCollaborationRequestsIterator(); loCollbReqs.hasNext();)
                {
                    BaseId bi = (BaseId) loCollbReqs.next();              
                    CollaborationRequest loCollbReq = (CollaborationRequest) bi.get();
                    if (loCollbReq != null)
                    {
                        for (Iterator loCollabThreads = loCollbReq.getCollaborationThreadsIterator(); loCollabThreads.hasNext();)
                        {
                            BaseId biThread = (BaseId) loCollabThreads.next();              
                            CollaborationThread loCollabThread = (CollaborationThread) biThread.get();
                            CollaborationRequest loCr = loCollabThread.getCollaborationRequest();
                                    if (loCr != null)
                                    {
                                        int iThreadState = loCr.getThreadState().getState();
                                        Log.customer.debug(ClassName + "::getCR_ChangeCriterea() iThreadState: " + iThreadState);
                                        if (iThreadState != 1)
                                        {
                                            loCrUNList.add(loCr.getUniqueName());
                                        }
                                    }
                                }
                            }
                    }

            }

            if (loCrUNList != null && !loCrUNList.isEmpty())
            {
                conditionList.add(AQLCondition.buildIn(new AQLFieldExpression("UniqueName"), loCrUNList));
            }else
            {
                conditionList.add(AQLCondition.parseCondition("1=2"));
            }

            AQLCondition condition = AQLCondition.buildOr(conditionList);
            Log.customer.debug(ClassName + "::getCR_ChangeCriterea() condition: " + condition.toString());
            return condition.toString();
        }

        Log.customer.debug(ClassName + ":: did not find any records returning dummy where clause");
        return " ";

    }
}
