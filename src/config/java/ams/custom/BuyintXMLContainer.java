/*
 * @(#)BuyintXMLContainer.java      2004/09/03
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information"). You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 * PVCS Modification :
 *
 * $Log:   R:\Software\development\archives\Ariba 8.x Customizations\app\buyer\Server\classes\extensions\config\java\ams\custom\BuyintXMLContainer.java-arc  $
 * 
 *    Rev 1.1   08 Sep 2004 11:12:24   nrao
 * A8 Dev SPL #86: Changed to use BuyintDBRec subclass. Also, changed to use Log.customer.debug rather than System.err.println.
 *
 */
package config.java.ams.custom;

import org.w3c.dom.*;
import java.util.*;
import ariba.util.log.Log;

/**
 * <code>BuyintXMLContainer</code> is a wrapper for a DOM Parser. It stores a pre-parsed
 * DOM document for use by the <code>BuyintXMLWriter</code> class in rendering XML.
 *
 * This class was developed for the Ariba 8.1 Upgrade.
 *
 * @author Simha Rao
 * @version 1.0
 */
public class BuyintXMLContainer implements BuyintConstants
{
    public static final String FIELD = "field";
    public static final String TEXT_DATA = "@TEXT_DATA@";

    private java.util.Hashtable moAttsHash;
    private java.util.Hashtable moFieldAttsHash;

    private Document moDoc;

    public BuyintXMLContainer(Document foDoc)
    {
        setDoc(foDoc);
        moAttsHash = new java.util.Hashtable(3);
        moFieldAttsHash = new java.util.Hashtable(3);

        Element loDocElement = moDoc.getDocumentElement();
        String lsDocElementName = loDocElement.getTagName();
        saveAtts(lsDocElementName, loDocElement);
        traverseDOM(loDocElement);
    }

    private void setDoc(Document foDoc)
    {
        moDoc = foDoc;
    }

    public Document getDoc()
    {
        return moDoc;
    }

    public void print()
    {
        String key = null;
        for (Enumeration ee = moAttsHash.keys(); ee.hasMoreElements() ;)
        {
            key = ee.nextElement().toString();
            debug("Printing " + key + " level atts ........");
            printAtts((Properties)moAttsHash.get(key));
        }

        debug("********************************************************");

        for (Enumeration ff = moFieldAttsHash.keys(); ff.hasMoreElements() ;)
        {
            key = ff.nextElement().toString();
            debug("Printing " + key + " level field atts ........");
            Vector aa = (Vector)moFieldAttsHash.get(key);
            for (int i = 0; i < aa.size(); i++)
            {
                printAtts((Properties)aa.elementAt(i));
            }
        }
    }

    public Properties getAtts(String fsLevel)
    {
        Properties loProps = null;
        if (moAttsHash.containsKey(fsLevel))
        {
            loProps = (Properties)moAttsHash.get(fsLevel);
        }
        return loProps;
    }

    public Vector getFieldAtts(String fsLevel)
    {
        Vector lvFldAtts = new Vector();
        if (moFieldAttsHash.containsKey(fsLevel))
        {
            lvFldAtts = (Vector)moFieldAttsHash.get(fsLevel);
        }
        return lvFldAtts;
    }

    public void printAtts(Properties foProps)
    {
        String lsKey = null;
        String lsValue = null;

        for (Enumeration loEnu = foProps.propertyNames(); loEnu.hasMoreElements();)
        {
            lsKey = loEnu.nextElement().toString();
            lsValue = foProps.getProperty(lsKey);
            debug("\t" + lsKey + "=" + lsValue);
        }
    }

    public void printFieldAtts(Vector fvPropsVector)
    {
        for (int lix = 0; lix < fvPropsVector.size(); lix++)
        {
            Properties loProps = (Properties)fvPropsVector.elementAt(lix);
            printAtts(loProps);
            debug("--------------");
        }
    }

    /**
     */
    private void traverseDOM(Node foCurNode)
    {
        NodeList loNodes = null;
        Element loElement = null;
        String lsElementName = null;
        int liNumNodes = 0;

        if (foCurNode == null)
        {
            return;
        }
        loNodes = foCurNode.getChildNodes();
        if (loNodes == null)
        {
            return;
        }
        liNumNodes = loNodes.getLength();

        for (int liNode = 0; liNode < liNumNodes; liNode++)
        {
            Node loNode = loNodes.item(liNode);
            if (loNode.getNodeType() == Node.ELEMENT_NODE)
            {
                loElement = (Element) loNode;
                lsElementName = loElement.getTagName();
                if (loNode.hasChildNodes())
                {
                    saveAtts(lsElementName, loNode);
                    saveFieldAtts(lsElementName, loNode);
                }
            }
            traverseDOM(loNode);
        }
    }

    private void saveAtts(String fsLevel, Node foNode)
    {
        Properties loProps = getAtts(foNode);
        if (loProps != null)       // we will save as place holder even if empty 
        {
            moAttsHash.put(fsLevel, loProps);
        }
    }

    private void saveFieldAtts(String fsLevel, Node foNode)
    {
        Element loElement = null;
        String lsElementName = null;
        Vector lvAll = new Vector();
        Properties loProps = null;
        Node loNode = null;
        NodeList loNL = foNode.getChildNodes();
        for (int lix = 0; lix < loNL.getLength(); lix++)
        {
            loNode = loNL.item(lix);
            if (loNode.getNodeType() == Node.ELEMENT_NODE)
            {
                loElement = (Element) loNode;
                lsElementName = loElement.getTagName();
                if (lsElementName.equals(FIELD))
                {
                   loProps = getAtts(loNode);
                   if (loProps.size() > 0)
                   {
                      lvAll.addElement(loProps);
                   }
                }
            }
        }
        if (lvAll.size() > 0)
        {
            moFieldAttsHash.put(fsLevel, lvAll);
        }
    }

    private static Properties getAtts(Node foNode)
    {
        Properties loProps = new Properties();
        String lsText = null;

        // get attributes and assign to actual names
        if (foNode.getNodeType() == Node.ELEMENT_NODE ) {
            NamedNodeMap loNodeMap = foNode.getAttributes();
            for(int i=0;i < loNodeMap.getLength();i++)
            {
                Node loAttsNode = loNodeMap.item(i);
                loProps.setProperty(loAttsNode.getNodeName(),loAttsNode.getNodeValue());
            }
            // get text data and assign to TEXT_DATA attribute
            lsText = getTextData(foNode);
            if (lsText != null && lsText.trim().length() > 0)
            {
                loProps.setProperty(TEXT_DATA, lsText.trim());
            }
        }
        return loProps;

    }

    private Vector getFieldAtts(Node foNode)
    {
        Vector lvAll = new Vector();
        Node loNode = null;
        NodeList loNL = foNode.getChildNodes();
        for (int lix = 0; lix < loNL.getLength(); lix++)
        {
            loNode = loNL.item(lix);
            lvAll.addElement(getAtts(loNode));
        }
        return lvAll;
    }

    private static String getTextData(Node foNode)
    {
        Text loText = (Text)foNode.getFirstChild();
        String lsData = null;
        if (loText != null)
        {
            lsData = loText.getData();
        }
        return lsData;
    }

   /**
    * Convenience method for debug message output.
    *
    * @param  fsMsg The text of the message to be printed.
    *
    */
    public static void debug(String fsMsg)
    {
        Log.customer.debug("BuyintXMLContainer: " + fsMsg);     // ONLINE
    }
}
