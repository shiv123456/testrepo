/*
    eVA Enh #8 - New class to address requirements
*/
/* 09/03/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 10/08/2003: Updates for Ariba 8.1 (Richard Lee) */


package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import java.util.List;
// Ariba 8.1: Deprecated Util
//import ariba.util.core.Util;
// Ariba 8.1: Deprecated Money
// import ariba.common.core.Money;
// Ariba 8.0: Deprecations, methods have changed packages.
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;

//81->822 changed Vector to List
public class eVA001
{
    // Ariba 8.0: Replaced deprecated method
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));
    
    public List requisitionValidations(Approvable approvable)
    {
        return NoErrorResult;
    }
    
    //*************************************************
    // IMPORTANT NOTE:
    //
    //      All code below was commented out for SPL # 52.
    //
    //*************************************************
        /*  if (approvable instanceof ariba.purchasing.core.Requisition)
        {
                        //Rob Giesen, Jan 2001, Added check for expiration/effective dates
                        BaseVector bvLineItems = (BaseVector)approvable.getFieldValue("LineItems");
                        for (int i=0; i<(bvLineItems.count()); i++)
                        {
                                ReqLineItem li =(ReqLineItem)bvLineItems.elementAt(i);

                                Boolean liIsAdHoc = (Boolean)li.getFieldValue("IsAdHoc");

                                if (liIsAdHoc.booleanValue()==false)
                                {
                                        Date liExpDate=(Date)li.getDottedFieldValue("Description.ExpirationDate");
                                        Date liEffDate=(Date)li.getDottedFieldValue("Description.EffectiveDate");


                                        if (liExpDate!=null)
                                        {
                                                int result=0;
                                                result = Date.getNow().compareTo(liExpDate);
                                                //result <0 if the current date is less than expDate
                                                if (result>0)
                                                {
                                                        String liDescription=(String)li.getDottedFieldValue("Description.ShortName");
                                                        int iLineNumber=i;
                                                        iLineNumber++;
                                                        // Ariba 8.0: Replaced deprecated method
                                                        return ListUtil.vector(Constants.getInteger(-1), "For item: " + iLineNumber+ " : "+liDescription+", this item has expired.");
                                                }
                                        }
                                        if (liEffDate!=null)
                                        {
                                                int result=0;
                                                result =Date.getNow().compareTo(liEffDate);
                                                //result <0 if the current date is less than expDate
                                                if (result<0)
                                                {
                                                        String liDescription=(String)li.getDottedFieldValue("Description.ShortName");
                                                        int iLineNumber=i;
                                                        iLineNumber++;
                                                        // Ariba 8.0: Replaced deprecated method 
                                                        return ListUtil.vector(Constants.getInteger(-1), "For item: " + iLineNumber+ " : "+liDescription+", this item is not in effect yet.");
                                                }
                                        }

                                }


                        }
*/
    
    
    //  All agency specific validations for required fields were removed.  07/16/01 J.Perry
    //      The Validations are now contained in the BuysenseValidationEngine class.
    
    //      return NoErrorResult;
    //      }
    
}




