/*
 * @(#)SetVendorPayDefault.java     1.0 06/26/2008
 *
 * Copyright 2008 by CGI
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of CGI ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with CGI
 */

/**               
 * CSPL #       1462-End engagement notification not sent when change order used to reduce hours and close order
 * @author      Pavan Aluri
 * Date         03-Feb-2010
 * @version     1.2
 * Explanation  Added a method setNotifyAgencySecOfficer(Requisition req) to set NotifyAgencySecOfficer field,
 *              when a Requisition is copied.  
 */
package config.java.ams.custom;

import java.util.List;

import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import ariba.procure.core.CategoryLineItemDetails;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
import ariba.workforce.core.LaborLineItemDetails;


public class SetVendorPayDefault extends Action
{
    public void fire (ValueSource object, PropertyTable params)
    {
        Log.customer.debug("Calling SetVendorPayDefault");
        Requisition loReq = (Requisition) object;
        try
        {
	    if(object == null)
	    {
	        return;
	    }
	    loReq.setFieldValue("VendorPay", null);
	    loReq.setFieldValue("VendorPayDWString", null);
	    setNotifyAgencySecOfficer(loReq);
	    return;
	}
	catch(Exception loExcep)
	{
	    Log.customer.debug("Exception while setting VendorPay to null.");
            loExcep.printStackTrace() ;
        }
    }

	private void setNotifyAgencySecOfficer(Requisition req)
	{
		CategoryLineItemDetails catLID = null;
		LaborLineItemDetails laborLine = null;
		Boolean bCollaborate = Boolean.TRUE;
		List reqLIs = req.getLineItems();
		
		for(int i = 0; i< reqLIs.size(); i++)
		{
			ReqLineItem reqLI = (ReqLineItem) reqLIs.get(i);
			catLID = reqLI.getCategoryLineItemDetails();
			if(catLID != null && catLID instanceof LaborLineItemDetails)
			{
				laborLine = (LaborLineItemDetails) catLID;
				bCollaborate = (Boolean)laborLine.getFieldValue("Collaborate");
			}						
			// Set NotifyAgencySecOfficer to TRUE, if Collaboration is false.
			if( ! bCollaborate.booleanValue() )
			{
				req.setDottedFieldValue("NotifyAgencySecOfficer", Boolean.TRUE);
			    break;
			}
		}
	}
    
}
