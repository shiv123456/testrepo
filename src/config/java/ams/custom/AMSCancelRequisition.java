
/* 08/13/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 09/23/2003: Updates for Ariba 8.0 (Richard Lee) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.fields.*;
import ariba.util.core.*;
// Ariba 8.0: Commented out the workflow and core imports; they aren't needed.
//import ariba.server.objectserver.workflow.*;
//import ariba.server.objectserver.core.*;
import ariba.base.core.*;
import ariba.util.log.Log;

// Ariba 8.0: added the 5 import below
import ariba.base.fields.Action;

/* Ariba 8.0: Action replaced ActionInterface, which can no longer be used
public class AMSCancelRequisition implements ActionInterface
{
     public boolean execute( WorkflowState workflowState,
                            ClusterRoot cr,
                            WorkflowParameter[] parameters)
                            throws Wc class AMSReceiptApproved extends Action
{
*/

public class AMSCancelRequisition extends Action
{
    // Ariba 8.0: replaced public boolean execute
    public void fire (ValueSource object,
                      PropertyTable parameters)
        throws ActionExecutionException
    {
        ClusterRoot cr = (ClusterRoot)object;
        System.out.println("In Fire of AMSCancelRequisition"+cr);
        
        //Anup - Generic invocation of dynamically determined Client/Agency edits. - Dev SPL #173 
        Boolean result = EditClassGenerator.invokeStatusEdits((Approvable)cr,
                                                              this.getClass().getName());
        if(!result.booleanValue())
        {
            Log.customer.debug("Validations Failed in " + this.getClass().getName()); 
            // Ariba 8.0: fire can not have return false
            //return false;
            return;
        }
        
        // Ariba 8.0: fire can not have return false
        //return true;
        return;
    }
}
