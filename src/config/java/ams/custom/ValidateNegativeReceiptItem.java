
/************************************************************************************
 * Author:  Richard Lee
 * Date:    September, 2007
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 09/19/2007        Richard Lee            CSPL-142: Change quantities received (negative receiving)
 *
 * @(#)AddCommentForDataRetention.java     1.0 09/11/2007
 *  ValidateNegativeReceiptItem.java    1.0 9/19/2007
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 */

package config.java.ams.custom;
import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.core.BaseObject;
import java.math.BigDecimal;

//81->822 changed ConditionValueInfo to ValueInfo
public class ValidateNegativeReceiptItem extends Condition
{
    private static final ValueInfo parameterInfo[];
    String message = null;

    public boolean evaluate(Object value, PropertyTable params)
    {
        Logs.customer.debug("Calling ValidateNegativeReceiptItem");
        BigDecimal loNumPrevAcc = new BigDecimal(0);
        BigDecimal loNumAcc = new BigDecimal(0);
        BaseObject obj   = (BaseObject)params.getPropertyForKey("ReceiptItem");
        if (obj.instanceOf("ariba.receiving.core.ReceiptItem"))
        {
            loNumAcc = (BigDecimal) obj.getFieldValue("NumberAccepted");
            Logs.customer.debug("loNumAcc = "+loNumAcc);

            if ((loNumAcc.doubleValue()) < 0)
            {
                loNumPrevAcc = (BigDecimal) obj.getFieldValue("NumberPreviouslyAccepted");
                   Logs.customer.debug("loNumPrevAcc = "+loNumPrevAcc);
                if((loNumPrevAcc.doubleValue() + loNumAcc.doubleValue()) < 0 )
                {
                   Logs.customer.debug("warning: negative number entered greater than positive number.");
                   return false;
                }
            }
        }
        return true;
    }

    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    static
    {
        parameterInfo = (new ValueInfo[]{ new ValueInfo("ReceiptItem", 0)});
    }
}
