package config.java.ams.custom;

import java.util.List;

import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.htmlui.baseui.Log;
import ariba.util.core.ListUtil;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;

public class BuysenseEformHeadGrp2RadioValSigner extends Action
{
    String sClassName = "BuysenseEformHeadGrp2RadioValSginer";

    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        if (valuesource != null)
        {
            ClusterRoot oEform = (ClusterRoot) valuesource;

            String sRadioVal = (String) oEform.getDottedFieldValue("EformHeadGrp2RadioVal");
            if (StringUtil.nullOrEmptyOrBlankString(sRadioVal))
            {
                return;
            }
            List<String> loRadioValues = getRadioFieldVAlues(oEform);
            int iSelectedIndex = -1;

            if (loRadioValues != null && !loRadioValues.isEmpty())
            {
                iSelectedIndex = loRadioValues.indexOf(sRadioVal);
            }

            oEform.setDottedFieldValue("EformHeadGrp1Radio2ValSigner", (iSelectedIndex + 1));

        }

    }

    private List<String> getRadioFieldVAlues(ClusterRoot cobj)
    {
        List<String> lsFieldValues = ListUtil.list();

        String sClientID = (String) cobj.getDottedFieldValue("EformChooser.ClientID");
        String sProfName = (String) cobj.getDottedFieldValue("EformChooser.ProfileName");
        String sFieldName = "EformHeadGrp2Radio";

        String queryText = "Select Description, FieldName, UniqueName from ariba.core.BuysEformFieldDataTable where " + "ClientID = '" + sClientID
                + "' AND ProfileName='" + sProfName + "' " + "AND FieldName like '" + sFieldName + "%' Order by UniqueName ";

        AQLQuery query = AQLQuery.parseQuery(queryText);
        Log.customer.debug(sClassName + " query: " + query);
        Partition pcsv = Base.getService().getPartition("pcsv");
        AQLResultCollection rc = Base.getService().executeQuery(query, new AQLOptions(pcsv));

        if (rc != null && rc.getFirstError() == null)
        {
            while (rc.next())
            {
                String s = rc.getString("Description");
                Log.customer.debug(sClassName + " Name: " + s);
                lsFieldValues.add(s);
            }

        }

        return lsFieldValues;
    }

}
