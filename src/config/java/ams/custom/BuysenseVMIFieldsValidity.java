package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ConditionResult;
import ariba.base.fields.ValueInfo;
import ariba.util.core.Fmt;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.approvable.core.Approvable;
import ariba.util.core.ResourceService;
import ariba.util.log.Log;
import ariba.base.core.*;

public class BuysenseVMIFieldsValidity extends Condition{
	
	  private static final ValueInfo parameterInfo[];
	  String msMessage = null;
      private static String lsErrorMessage = "";
	  private static final String ERROR_FILE = "buysense.eform"; // Specify the Resource file
	  private static final String ERROR_MESSAGE1 = "DuplicateDepartmentMessage";	  
	  
 
		public boolean evaluate(Object object, PropertyTable params)
		{
			BaseObject baseobj  = (BaseObject)params.getPropertyForKey("SourceObject");	
			Log.customer.debug("Inside BuysenseVMIFieldValidity "+baseobj);
			
			String lsSubDept = null;
			String lsDept = null;
			Boolean lbreturnvalue = true;

			if(((Approvable)baseobj).instanceOf("ariba.core.BuysenseVMIAssetTracking"))
			{
				Log.customer.debug("Checking for Dept is's");
   		   	    lsSubDept =  (String)baseobj.getDottedFieldValue("Sub1dept.UniqueName");
   		   	    lsDept = (String)baseobj.getDottedFieldValue("Department.UniqueName");

   		   	    if(lsSubDept != null && lsDept != null && lsSubDept.equalsIgnoreCase(lsDept))
   		   	    {
   		   		  Log.customer.debug("Getting each of the department id's Department is: "+lsDept + "SubDept is:"+lsSubDept);
 		   		  
   		   		  Log.customer.debug("The receiving department is same as the originating department");
   		   		  Log.customer.debug("The error message to be displayed is :"+lsErrorMessage);
   		   	      lbreturnvalue = false;
   		   		     		   	    	
   		   	    }
   		   	    else
   		   	    {
   		   	    	Log.customer.debug("The department codes don't match or are not filled");
   		   	        lbreturnvalue = true;
   		   	    }

				if(lbreturnvalue == false)
				{
					    Log.customer.debug("The department codes match");
					    msMessage = Fmt.Sil(Base.getSession().getLocale(),ERROR_FILE, ERROR_MESSAGE1);
						return false;
				}
				else
				{
					return true;
				}
		    }
		
		return false;

	}	  
	    public ConditionResult evaluateAndExplain(Object object, PropertyTable params)
	    {
	        Log.customer.debug("Calling BuysenseVMIFieldsValidity for Receiving Dept Validation.");
	        if(!evaluate(object, params))
	        {
	            String retMsg = null;
	            if(!StringUtil.nullOrEmptyOrBlankString(msMessage))
	            {
	                retMsg = msMessage;
	                msMessage = null;
	                return new ConditionResult(retMsg);
	            }
	            else
	                return null;
	        }
	        return null;
	    }
		
	  protected ValueInfo[] getParameterInfo()
	  {
	        return parameterInfo;
	  }
	
	  static
    {
        parameterInfo = (new ValueInfo[] {
           new ValueInfo("SourceObject", 0)
          });
     }

}
