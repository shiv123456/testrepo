package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.user.core.User;
import ariba.util.core.PropertyTable;
import ariba.util.core.ResourceService;
import ariba.util.log.Log;
import ariba.util.parameters.Parameters;

public class BuysenseDynamicEformSetImage extends Action {
	protected String sResourceFile = "buysense.DynamicEform";

	public void fire(ValueSource valuesource, PropertyTable propertytable)
			throws ActionExecutionException {
		Log.customer.debug("fire method called");
		Approvable loAppr = null;
		if (valuesource != null && valuesource instanceof Approvable) {
			loAppr = (Approvable) valuesource;
			Log.customer.debug("The approvable object is :"
					+ loAppr.getUniqueName());
			setCustomImage(loAppr);
            setCustomFormIdentifier(loAppr);
            setEformLink(loAppr);
            setCustomTitle(loAppr);
		}
	}

    public void setCustomImage(Approvable appr)
    {

        String sImage = (String) appr.getDottedFieldValue("EformChooser.LogoFile");

        String sHost = (String) Parameters.getInstance().getParameter("System.Base.Host");
        String sPath = ResourceService.getString(sResourceFile, "Path");
        String sCompletePath = "https://" + sHost + sPath + sImage;
        Log.customer.debug("The Complete Image Path is :" + sCompletePath);
        appr.setFieldValue("EformImage", sCompletePath);

    }
    public void setCustomFormIdentifier(Approvable appr)
    {

        String lsEformChooser = (String) appr.getDottedFieldValue("EformChooser.ProfileName");
        
        User loPreparer;
        if(appr.getPreparer() != null)
        {
            loPreparer = appr.getPreparer();
        }else
        {
            loPreparer =  (User) Base.getSession().getEffectiveUser();
            Log.customer.debug("BuysenseDynamicEformSetImage  appr.getPreparer() is null using Session.EffectiveUser: " + loPreparer);
        }
        ariba.common.core.User loPartUser = ariba.common.core.User.getPartitionedUser(loPreparer,
                appr.getPartition());
        String lsPreparerAgency = (String) loPartUser.getDottedFieldValue("ClientName.ClientName");
        appr.setFieldValue("FormIdentifier", lsEformChooser + "-" + lsPreparerAgency);
    }
    public void setCustomTitle(Approvable appr)
    {

        String lsEformChooser = (String) appr.getDottedFieldValue("EformChooser.ProfileName");
        String lsEformTitle = (String)appr.getFieldValue("Name");
        User loPreparer;
        if(appr.getPreparer() != null)
        {
            loPreparer = appr.getPreparer();
        }else
        {
            loPreparer =  (User) Base.getSession().getEffectiveUser();
            Log.customer.debug("BuysenseDynamicEformSetImage  appr.getPreparer() is null using Session.EffectiveUser: " + loPreparer);
        }
        ariba.common.core.User loPartUser = ariba.common.core.User.getPartitionedUser(loPreparer,
                appr.getPartition());
        String lsPreparerAgency = (String) loPartUser.getDottedFieldValue("ClientName.ClientName");
        
        if(lsEformChooser != null)
        {
        String lsTitlePrefix = lsPreparerAgency + " - " +lsEformChooser;
        
        if(!(lsEformTitle.contains(lsTitlePrefix)))
        {
                Log.customer.debug("Not finding the exact prefix not '-' and '_' exist: so adding the prefix once again");
                
                String lsTitle = lsTitlePrefix + "_";
                appr.setFieldValue("Name", lsTitle);              
            }
        }
    }    
    public void setEformLink(Approvable appr)
    {
        String lsEformChooser = (String) appr.getDottedFieldValue("EformChooser.UniqueName");
        String lsFieldName = "EformLink1";
        String sClientID = (String) appr.getDottedFieldValue("EformChooser.ClientID");
        String sProfName = (String) appr.getDottedFieldValue("EformChooser.ProfileName");
        String lsFieldLabel = BuysenseEformUtil.findFieldLabel(lsEformChooser, lsFieldName);
        String sLinkURL = null;
        String sAQL = "Select Name, UniqueName from ariba.core.BuysEformFieldDataTable where " +
        "ClientID = '" + sClientID + "' AND ProfileName='"+ sProfName + "' " +
        "AND FieldName = '" + lsFieldName + "' Order by UniqueName ";
        AQLQuery query = AQLQuery.parseQuery(sAQL);
        Log.customer.debug(ClassName + " query: " + query);
        Partition pcsv = Base.getService().getPartition("pcsv");
        AQLResultCollection slink = Base.getService().executeQuery(query, new AQLOptions(pcsv));
        if (slink != null && slink.getFirstError() == null)
        {
            while (slink.next())
            {
                sLinkURL = slink.getString("Name");
                Log.customer.debug(ClassName + " sLinkURL: " + sLinkURL);
            }

       }
        appr.setFieldValue("EformLink1", sLinkURL);
    }
}
