//
//***************************************************************************/
//  Anup - Aug 2003 - Determine editability of SupplierPartNumber 
//                     and SupplierPartAuxiliaryID 
//
//***************************************************************************/

/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.core.*;
import ariba.util.log.Log;
import ariba.procure.core.*;
import ariba.procure.core.condition.*;

//81->822 changed ConditionValueInfo to ValueInfo
public class BuysenseSupplierPartsEditability extends CatalogItemEditable
{

    private static final ValueInfo parameterInfo[];

    public boolean evaluate(Object value, PropertyTable params)
    {
       BaseObject Obj = (BaseObject)params.getPropertyForKey("LineItem");
       Log.customer.debug("Current BaseObject is: " + Obj);
       
       if (Obj.instanceOf("ariba.procure.core.ProcureLineItem"))
       {
          ProcureLineItem line = (ProcureLineItem)Obj;
          if (line.getIsAdHoc()) 
          {
             // For non-catalog item, allow baseline editability logic to kick in 
             Log.customer.debug("BuysenseSupplierPartsEditability : Ad Hoc Item ... allowing super to evaluate editability");
             return super.evaluate(value, params);
          }
          else
          {
             // Catalog Item must not be editable
             Log.customer.debug("BuysenseSupplierPartsEditability : Catalog item ... making field uneditable");
             return false;
          }
       }

       Log.customer.debug("BuysenseSupplierPartsEditability : Not a ProcureLineItem ... returning true");
       return true;
    }
   
    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    static
    {
        parameterInfo = (new ValueInfo[] {
            new ValueInfo("LineItem", 0),
            new ValueInfo("Message", 0, Behavior.StringClass)
        });
    }

}

