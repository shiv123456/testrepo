/* 08/11/2003: Updates for Ariba 8.0 (Jeff Namadan) */
/* 09/05/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLQuery;
import ariba.common.core.PCard;
import ariba.base.fields.*;
/* Ariba 8.0: Replaced ariba.common.core.nametable.NamedObjectNameTable) */
import ariba.base.core.aql.AQLNameTable;
/* Ariba 8.0: Replaced ariba.util.core.Util */
import java.util.List;
// Ariba 8.1: Various methods have changed from the java.util.List package to the ariba.util.core.ListUtil package.
import ariba.util.core.ListUtil;
import ariba.util.log.Log;
import ariba.util.core.Date;
/***************
Rob Giesen
December, 2000
BuysenseUserPCardNameTable is used to retrieve only the PCards that a user has.

On the main requisition header, there is a new field added called "PCardToUse", which will store the PCard that the
user wishes to use for the purchase.  On the Req header, there is a chooser (drop-down combobox) of all the user's
PCards.  A NameTable is needed because without it, the chooser would have all the PCards that are in the system, instead
of just the users.

Within the addQueryContraints() function (which is basically a way of buiding WHERE clauses), the current user is first
retrieved and from that, a list of all the PCards that are assigned to that person is generated.  This list is then
added to the query to filter on.  The final SQL code will look something like:

Select * from PCard where (PCard.uniqueName = "<currentuser>.PCards.pcard1uniqueName") AND
    (PCard.uniqueName = "<currentuser>.PCards.pcard2uniqueName") AND
    (PCard.uniqueName = "<currentuser>.PCards.pcard3uniqueName") ...

The actual return is done in matchPattern(), and should return a List of PCards.

There is a special check to see if the user has any PCards assigned to them at all.  If they don't, then treat differently.

January 31, 2000 - Added a check for expiration date.  If it is null, or the expiration date has
passed, then it is not a valid PCard.

rlee, 8/7/03: ST SPL 1057 - CardType 3 is not seen by User. Mod to include Type 3

rlee, 11/2003: Enh 18 and Prod 218 - PCard Expiration and PCard fields visibility check for Requesters and Approvers.

rgiesen 01/20/2004 - Merged Ariba 8.1 changes with rlee's 11/2003 changes
*******************/

/* Ariba 8.0: Replaced NamedObjectNameTable */
//81->822 changed Vector to List
public class BuysenseUserPCardNameTable extends AQLNameTable
{

    public List matchPattern (String field, String pattern)
    {
        Log.customer.debug("rlee 63, in matchPatter of BuysenseUserPCardNameTable");
        setClassIsLeaf(false);

        ValueSource context = getValueSourceContext();
        //rgiesen 01/20/2004 Ariba 8.1
        //User luRequester = (User)context.getDottedFieldValue("Requester");
        ariba.user.core.User luRequester = (ariba.user.core.User)context.getDottedFieldValue("Requester");

        // if Requester has no valid pcard, return null
        if (findValidPCards(luRequester).isEmpty())
        {
            Log.customer.debug("rlee 70 find valid pcards is empty return null ");
            return null;
        }

        //if findValidPCards is not empty, retrieve all the valid PCards of the Requester
        List results = super.matchPattern(field, pattern);
        setClassIsLeaf(false);

        /* rlee, not needed
        if (numPCards<1)
        {
            //If the user doesn't have any PCards, then return null (no PCards)
            return null;
        }
        */
        //If the user has PCards, the return the results of the Query (Just the PCards that the user has)

        return results;
    }

    public void addQueryConstraints (AQLQuery query, String field, String pattern)
    {
        ValueSource context = getValueSourceContext();

        //rgiesen Ariba 8.1
        //User luRequester = (User)context.getDottedFieldValue("Requester");
        ariba.user.core.User luRequester = (ariba.user.core.User)context.getDottedFieldValue("Requester");

        // Ariba 8.1: Replaced deprecated new List() Constructor with ListUtil.newVector();
        List lvSearchVector= ListUtil.list();

        lvSearchVector = (List)findValidPCards(luRequester) ;

        setClassIsLeaf(false);
        super.addQueryConstraints(query, field, pattern);
        beginSystemQueryConstraints(query);

        AQLCondition defaultCond1 = AQLCondition.buildIn(query.buildField("UniqueName"),lvSearchVector);

        //Add this condition to the overall query
        query.and(defaultCond1);
        query.setDistinct(true);
        endSystemQueryConstraints(query);
    }

    // return all the User's valid PCards
    private List findValidPCards (ariba.user.core.User luRequester)
    {
        // Ariba 8.1: Replaced deprecated new List() Constructor with ListUtil.newVector();
        List lvSearchVector= ListUtil.list();

		//rgiesen Ariba 8.1
        //List lvUserPCards =luRequester.getPCardsVector();
		List lvUserPCards = ariba.common.core.User.getPartitionedUser(luRequester,Base.getSession().getPartition()).getPCardsVector();

        PCard lpPCard;
        String lsPCardName;
        Date ldPCardExpDate;
        int liNumPCards = 0;

        if(lvUserPCards != null)
            liNumPCards=lvUserPCards.size();

        if (liNumPCards>0)
        {
        //lvSearchVector will contain a List of Strings of the PCards UniqueName (usually the PCard number)

            for(int i=0;i<liNumPCards;i++)
            {
                //Get the individual PCard
                // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                lpPCard = (PCard)lvUserPCards.get(i);
                //Check to see if PCard is of type 2
                // rlee added CardType ==3
                if ((lpPCard.getCardType()==2) || (lpPCard.getCardType()==3))
                {
                    //Check if the expiration date is past today's date
                    //If it is past today's date, it is not a valid PCard

                    ldPCardExpDate=lpPCard.getExpirationDate();

                    //Get the PCard UniqueName
                    lsPCardName = (String) lpPCard.getUniqueName();

                    if (ldPCardExpDate!=null)
                    {
                        int result=0;
                        result = Date.getNow().compareTo(ldPCardExpDate);
                        //result <0 if the current date is less than expDate
                        if (result<=0)
                        {
                            //Add the UniqueName to the lvSearchVector
                            // Ariba 8.1: Replaced deprecated addElement() method with add() method.
                            lvSearchVector.add(lsPCardName);
                            //set
                        } //end of result
                    }//end of PCard date null
                }//End of type 2 or 3
            }//end of PCard for loop
        }//if liNumPCards
        return lvSearchVector;
    }

    /* Ariba 8.0: NamedObjectNameTable was deprecated by AQLNameTable.
       This class was changed to derive from AQLNameTable and the
       method constraints() is not in this class and isn't needed.
    public List constraints (String field, String pattern)
    {
        List constraints = super.constraints(field, pattern);
        return constraints;
    } */
}
