package config.java.ams.custom;

import ariba.base.core.BaseId;
import ariba.cxml.PunchOutSession;
import ariba.htmlui.orms.SupplierPunchInRequestHandler;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

import java.util.Map;

/**
* CSPL:2674 - Custom punchin handler compatibility/changes for 9r1
* 
* @author Pavan Aluri
* @Date 09-Mar-2011
* @version 1.1
* @Explanation Modified getDirectActionClassName() method to return CollaborationPunchInDirectAction class basically which is responsible for punchin.
*/

public class eVA_CollaborationPunchInRequestHandler extends SupplierPunchInRequestHandler
{

    private static final String lsSuccessCode = "200";

    public Map handle(PunchOutSession punchOutSession, Map setupRequest)
    {
        Log.customer.debug(className + " handle method");
        Map lmHandle = super.handle(punchOutSession, setupRequest);
        Log.customer.debug(className + " handle:lmHandle-" + lmHandle);
        if (lmHandle.get("statusCode").toString().equalsIgnoreCase(lsSuccessCode))
        {
            lmHandle = getCustomHandle(punchOutSession, lmHandle);
        }
        Log.customer.debug(className + " handle:returning-" + lmHandle);
        return lmHandle;
    }

    private Map getCustomHandle(PunchOutSession superPunchOutSession, Map customHandle)
    {
        Log.customer.debug(className + " getCustomHandle");

        String lsAppData = superPunchOutSession.getPersistableAppData();
        String objectIds[] = StringUtil.delimitedStringToArray(lsAppData, ',');
        BaseId userId = BaseId.parse(objectIds[0]);
        ariba.user.core.User user = (ariba.user.core.User) userId.get();
        String lsNewStartPageURL = (String) customHandle.get("startPageURL") + "&acptoken="
                + encodeCredentials(objectIds[0].toString(), user.getUniqueName());
        customHandle.put("startPageURL", lsNewStartPageURL);
        return customHandle;
    }

    private String encodeCredentials(String baseIDStr, String userUniqueName)
    {
        StringBuffer lsCredentials = new StringBuffer(userUniqueName + "+" + baseIDStr);
        return lsCredentials.reverse().toString();
    }

    protected String getDirectActionClassName()
    {
        Log.customer.debug(className + " getDirectActionClassName:returning-" + directActionClassName);
        return directActionClassName;
    }

    public static String className             = "eVA_CollaborationPunchInRequestHandler";
    public static String directActionClassName = "CollaborationPunchInDirectAction";
}