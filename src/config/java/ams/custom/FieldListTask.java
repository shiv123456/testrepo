
/**
This batch program is designed to query all receipt of the Ariba OM to a specific agency,
which not yet been paid to create payment documents in an ERP system.

This program is designed to run in a nightly cycle or on demand.
imohideen, AMS, August 2000

Anup - DEV SPL # 11 - Defined constant ClassName and called super.init() for 7.1a sync up.
*/

/* 08/13/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 09/08/2003: Updates for Ariba 8.0 (Richard Lee) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.util.log.Log;
// Ariba 8.0: ScheduledTask has moved from ariba.server.objectserver to ariba.util.objectserver
// So changed the import statement to reflect that. 
import ariba.util.scheduler.*;
// Ariba 8.0: Removed 'import ariba.server.objectserver.core.WorkflowState;'
// Ariba 8.0: deleted 'import ariba.server.ormsserver.ApprovableOnServer;' 
import java.util.Map;
// Ariba 8.0: Changed FieldListTask to extend from ScheduledTask instead of SimpleScheduledTask
// Since SimpleScheduledTask is deprecated and ScheduledTask is offered as the alternative.

//81->822 changed Hashtable to Map
public class FieldListTask extends ScheduledTask
{
    public FieldListTask()
    {
    }
    
    /**
    This init function get automatoically invoked by Ariba on system start-up time.
    */
    public void init(Scheduler scheduler,
                     String scheduledTaskName, Map arguments)
    {
        Log.customer.debug("FieldListTask init was called...");
        
        super.init( scheduler, scheduledTaskName, arguments ) ;
        
        FieldListContainer.loadList();
    }
    
    public void run() throws ScheduledTaskException
    {
        Log.customer.debug("FieldListTask run was called...");
        FieldListContainer.loadList();
        
    }
    
}
