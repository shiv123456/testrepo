package config.java.ams.custom;


import ariba.approvable.core.Approvable;
import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.basic.core.Currency;
import ariba.basic.core.Money;
import ariba.util.core.PropertyTable;

public class ExemptionRequestSetTotalCost extends Action
{
    String msClassName = "ExemptionRequestSetTotalCost";

    public void fire(ValueSource valuesource, PropertyTable param) throws ActionExecutionException
    {
        Approvable loAppr = (Approvable) valuesource;
        Partition partition = Base.getService().getPartition("pcsv");
        Money loTotalCost = new Money(0.0,Currency.getDefaultCurrency(partition));
        
           //String lsFieldName = "Table01Field5";
        	//Table01Field5, Table01Field51,Table01Field511,Table01Field5111,Table01Field51111
        	
            Money loRowAmount = (Money) loAppr.getDottedFieldValue("Table01Field5");
            if(loRowAmount != null)
            {
                loTotalCost=loTotalCost.add(loRowAmount);
            }
            Money loRowAmount1 = (Money) loAppr.getDottedFieldValue("Table01Field51");
            if(loRowAmount1 != null)
            {
                loTotalCost=loTotalCost.add(loRowAmount1);
            }
            Money loRowAmount11 = (Money) loAppr.getDottedFieldValue("Table01Field511");
            if(loRowAmount11 != null)
            {
                loTotalCost=loTotalCost.add(loRowAmount11);
            }
            Money loRowAmount111 = (Money) loAppr.getDottedFieldValue("Table01Field5111");
            if(loRowAmount111 != null)
            {
                loTotalCost=loTotalCost.add(loRowAmount111);
            }
            Money loRowAmount1111 = (Money) loAppr.getDottedFieldValue("Table01Field51111");
            if(loRowAmount1111 != null)
            {
                loTotalCost=loTotalCost.add(loRowAmount1111);
            }
        loAppr.setFieldValue("TotalCost", loTotalCost);
    }
}
