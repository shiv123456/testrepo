package config.java.ams.custom;

import java.util.List;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.Comment;
import ariba.base.core.Base;
import ariba.base.core.BaseVector;
import ariba.base.core.LongStringElement;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.Fmt;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
import ariba.util.core.ResourceService;

public class BuysenseAddConfirmationOrderComment extends Action
{
    private static String messageStringTable = "ariba.procure.core";
    private static String sCommentData       = "BuysenseOrderConfComment";
    Comment       loComment               = null;
    String        lsCommentString         = null;
    private static         String        orderConfComment        = "BuysenseOrderConfComment";
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        Approvable loAppr = null;
        String sConfirmCommentMessage = ResourceService.getString(messageStringTable, sCommentData);

        Log.customer.debug("BuysenseAddConfirmationOrderComment... ");

        if (valuesource != null && valuesource instanceof Approvable)
        {
            loAppr = (Approvable) valuesource;
            // Check if BuysenseOrderConfirm is set to true
            Boolean bBuysenseOrderConfirmVal = (Boolean) loAppr.getDottedFieldValue("BuysenseOrderConfirm");
            BaseVector lvComments = (BaseVector) loAppr.getFieldValue("Comments");

            if (bBuysenseOrderConfirmVal.booleanValue())
            {
                // Add header comment
                Log.customer.debug("BuysenseAddConfirmationOrderComment: adding a comment (started)");
                for (int i = 0; i < lvComments.size(); i++)
                {
                    loComment = (Comment) lvComments.get(i);
                    lsCommentString = getCommentString(loComment);
                    
                    if((sConfirmCommentMessage.trim()).equals(lsCommentString.trim()))
                    {
                        Log.customer.debug("BuysenseAddConfirmationOrderComment: not adding a comment as already there is comment on Req");
                      return;
                    }
                
                }  
                Comment loComment = new Comment(loAppr.getPartition());
                if (loAppr.getPreparer() == null)
                {
                    Log.customer.debug("BuysenseAddConfirmationOrderComment: Preparer is null");
                    loComment.setUser((ariba.user.core.User) Base.getSession().getEffectiveUser());
                }
                else
                {
                    Log.customer.debug("BuysenseAddConfirmationOrderComment: Preparer is not null");
                    loComment.setUser(loAppr.getPreparer()); // add preparer
                }

                Log.customer.debug("BuysenseAddConfirmationOrderComment: sConfirmCommentMessage-", sConfirmCommentMessage);                
                
                loComment.setTitle(sConfirmCommentMessage);
                loComment.setBody(Fmt.Sil(Base.getSession().getLocale(), "ariba.procure.core", sCommentData));
                loComment.setType(Comment.TypeSubmit); // is the type correct?
                loComment.setDate(new ariba.util.core.Date());
                loComment.setExternalComment(true);
                loAppr.addComment(loComment);
                loAppr.save();
                Log.customer.debug("BuysenseAddConfirmationOrderComment: adding a comment (done)");
            }
            else if(!bBuysenseOrderConfirmVal.booleanValue())
            {
                Log.customer.debug("BuysenseAddConfirmationOrderComment:: Inside else if");
                for (int i = 0; i < lvComments.size(); i++)
                {
                    Log.customer.debug("BuysenseAddConfirmationOrderComment: deleting a comment (done)");
                    String lsOrderConfComment = ResourceService.getString(messageStringTable, orderConfComment);
                    String lsOrderConfPattern = ResourceService.getString(messageStringTable, lsOrderConfComment);
                    Log.customer.debug("Inside for if:: BuysenseOrderConfirm one comment");
                    loComment = (Comment) lvComments.get(i);
                    lsCommentString = getCommentString(loComment);
                    if ((lsOrderConfPattern.trim()).equals(lsCommentString.trim()))
                    {
                        loAppr.removeComment(loComment);
                        loAppr.save();
                    }
                } 
            }
            Log.customer.debug("BuysenseAddConfirmationOrderComment: not adding a comment as BuysenseOrderConfirm is not set to true");
        }
    }
    public static String getCommentString(Comment cmtStr)
    {
        String sLongString = "";
     
        List vCmt = cmtStr.getText().getStrings();
        LongStringElement sLongElement=null;
                            
        for(int iCmtText = 0; iCmtText < vCmt.size(); iCmtText++)
           {
                sLongElement = (LongStringElement) vCmt.get(iCmtText);
                sLongString += sLongElement.getString();
                Log.customer.debug("The value of sLongString is :"+sLongString);
           }
        Log.customer.debug("The value os sLongString is :"+sLongString);
        return sLongString;
  
    }
}
