package config.java.ams.custom;

import ariba.approvable.core.Folder;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import ariba.purchasing.core.Requisition;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

/*
 This class gets called from a trigger in ReqExtrinsicFields.aml.
 */

/**
 * @author Jackie Bhadange
 * @version 1
 * @reference CSPL-5836
 * @see Date 02-02-2014
 * @Explanation Added trigger to set apply labels to a change requisition from previous version
 * 
 */
public class BuysenseSetLabelsOnChangeReq extends Action
{

    public void fire(ValueSource object, PropertyTable params)
    {        
        try
        {
            if (object != null && object instanceof Requisition)
            {
                ClusterRoot loCurrentVersion = (ClusterRoot) object;
                ClusterRoot loPreviousVersion = (ClusterRoot) loCurrentVersion.getDottedFieldValue("PreviousVersion");

                // if previous version exists then copy the labels from it to newly created req
                if (loPreviousVersion != null)
                {
                    Log.customer.debug("BuysenseSetLabelOnChangeReq:: PreviousReq = %s", loPreviousVersion);
                    // Old value from previous req
                    String sQuery = "select Folder from ariba.approvable.core.Folder JOIN ariba.approvable.core.FolderItem AS FolItem USING Folder.Items JOIN ariba.approvable.core.Approvable AS Appr USING FolItem.Item where Appr.UniqueName=\'" + loPreviousVersion.getUniqueName() + "\' AND Folder.\"Type\"=1";

                    Log.customer.debug("BuysenseSetLabelOnChangeReq::  previousreq = %s, previous sql = %s", sQuery, loPreviousVersion);

                    AQLQuery aqlQuery = AQLQuery.parseQuery(sQuery);
                    Partition partition = Base.getService().getPartition("pcsv");
                    AQLOptions aqlOptions = new AQLOptions(partition);
                    AQLResultCollection aqlResults = Base.getService().executeQuery(aqlQuery, aqlOptions);

                    if (aqlResults.getErrors() == null) // no errors
                    {
                        while (aqlResults.next())
                        {
                            BaseId baseIdFolder = aqlResults.getBaseId(0);
                            Folder folderObj = (Folder) Base.getSession().objectFromId(baseIdFolder);

                            // check if label is already not applied to this req.
                            // No need to check that in this case as we are applying labels as soon as the req is created
                            folderObj.addItem(loCurrentVersion.getBaseId());
                            folderObj.save();
                            Log.customer.debug("BuysenseSetLabelOnChangeReq::  newly added folder owner= %s, app= %s", folderObj.getOwner(), loCurrentVersion.getUniqueName());
                        }
                    } // end of no errors in aql results
                }// end of previous version not null
            } // end of obj not null
        } // end of try
        catch (Exception e)
        {
            Log.customer.debug("BuysenseSetLabelOnChangeReq::  exception in processing %s", e.getMessage());
        }
    }
}
