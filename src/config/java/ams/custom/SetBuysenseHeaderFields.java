/* Responsible rlee */
/* 10/08/2003: Updates fo Ariba 8.1 (Richard Lee) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import java.util.List;
import ariba.base.core.ClusterRoot;
import ariba.base.core.BaseObject;
import ariba.util.log.Log;

/*
    This class gets called from a trigger in ReqExtrinsicFields.aml.
    rlee: eProcurement ST SPL # 596 - PR V2 Header Fields blanked out (not getting copied to Change Order) -->
    rlee: Added trigger to copy Header fields from PreviousVersion of Requisition when creating a Change Order -->
*/

//81->822 changed Vector to List
/**
* @author Manoj Gaur
* @version 1 DEV
* @reference CSPL-593
* @see Date 08-12-2008
* @Explanation Set the HeaderField null, if field has n/a value in it's ERP Value.
* 
*/
public class SetBuysenseHeaderFields extends Action
{
    
    public void fire (ValueSource object, PropertyTable params)
    {
        //Get the Requisition Header fields
        List headfieldvect = FieldListContainer.getHeaderFieldValuesOnly();
        BaseObject bo = (BaseObject)object;
        Log.customer.debug("rlee In SetBuysenseHeaderFields, This is the passed in object %s ", bo);
        ariba.purchasing.core.Requisition req = (ariba.purchasing.core.Requisition) bo;
        
        ClusterRoot previousV = (ClusterRoot) req.getDottedFieldValue("PreviousVersion");
        Log.customer.debug("rl20, previousV = %s", previousV);
        
        if ( previousV == null )
        {
            return;
        }
        else
        {
            //Copy Header field information from previous version of Requisition.
            BaseObject PreviousReq = (BaseObject) req.getPreviousVersion();
            Log.customer.debug("rl40, PreviousReq = %s", PreviousReq);
            int FieldSize = headfieldvect.size();
            Log.customer.debug("rl60, FieldSize = %s", FieldSize);
            
            for (int liCount = 0 ;liCount < FieldSize;liCount++) 
            {
                // Ariba 8.1: rlee, changed elementAt to get
                //String HeadField = (String)headfieldvect.elementAt(liCount);
                String HeadField = (String)headfieldvect.get(liCount);
                Log.customer.debug("rl80, HeadField = %s", HeadField);
                req.setDottedFieldValue(HeadField,
                                        PreviousReq.getDottedFieldValue(HeadField));
                // If field has ERP value n/a then set null.
                DeleteBSOReqObject.setNull(req, HeadField);
            }
        }
    }
}
