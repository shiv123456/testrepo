/************************************************************************************
 * Author:  Richard Lee
 * Date:    July 30, 2004
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 07/30/2004        Richard Lee            Integration Interface
 * 9/20/2004         Richard Lee            Dev SPL 112 - Response Transaction Selection
 *
 *
 * @(#)BuyintIntegrationXMLReader.java     1.0 08/24/2004
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/

/**
 * @author manoj.gaur
 * @Date 9/30/2008
 * @CSPL-592
 * @Description Checking the response from External ERP and set the status.
 */
package config.java.ams.custom;

import java.util.Map;
import java.util.List;
import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.user.core.*;
import ariba.util.log.Log;
import ariba.util.scheduler.ScheduledTask;
import ariba.util.scheduler.Scheduler;
import java.io.*;
import java.lang.Integer;
import java.util.Iterator;
import javax.xml.parsers.*;

import org.apache.log4j.Level;
import org.w3c.dom.*;

//81->822 changed Vector to List
//81->822 changed Hashtable to Map
public class BuyintERPResponseReader extends ScheduledTask implements BuyintConstants
{
    private BuyintDBRecExportData moRecords[] = null;
    private BuyintDBRecExportData moDbRec = null;
    private String msDBPassword = "";
    private String msDBURL = "";
    private String msDBUser = "";
    private static Partition moPart = Base.getSession().getPartition();
    private BuyintERPResponseImport moXMLImporter = new BuyintERPResponseImport();
    private int miRetryThreshold = RETRY_DEFAULT ;
    private int miMaxRecords = MAX_TRANSACTIONS_DEFAULT ;
    private String msFailedTXNPermission = "";
    private static DocumentBuilderFactory moDOMFactory;
    public void run()
    {
        Level currLevel = Log.customer.getLevel(); 
        //Logs.buysense.setDebugOn();
        Log.customer.setLevel(Level.DEBUG);

        Log.customer.debug("Calling BuyintERPResponseReader.");

        try{
            //Build the sql string and get response for processing. 
            String lsSQL = "WHERE TRANSACTION_TYPE = '" + TXNTYPE_RESPONSE + "' AND STATUS = '" + STATUS_NEW + "' AND RETRY_COUNT <= " + miRetryThreshold;
            moRecords = moDbRec.getRecs(lsSQL , miMaxRecords);
            String lsTIN = null;
            String lsXML = new String();
            InputStream tempIS = null;

            if (moRecords == null)
            {
                return;
            }

            //Process all records
            for (int i = 0; i < moRecords.length; i++)
            {
                try
                {
                    lsXML = moRecords[i].getTransactionData();
                    if (lsXML == null)
                    {
                       throw new BuysenseXMLImportException("Response is missing the XML.");
                    }
                    else if (lsXML.trim().length() < 1)
                    {
                       throw new BuysenseXMLImportException("Response is missing the XML.");
                    }
                    else
                    {
                       lsXML.trim();
                    }
                    byte[] moBytes = (byte[]) lsXML.getBytes();
                    tempIS = new ByteArrayInputStream(moBytes);

                    Log.customer.debug("BuyintERPResponseReader:: before calling moXMLImporter.importFromXML");
                    moXMLImporter.importFromXML(tempIS); 
                    Log.customer.debug("BuyintERPResponseReader:: after calling moXMLImporter.importFromXML");
                    // CSPL-592 (Manoj)checking for RESP is blank or null
                    if(!BuyintERPResponseImport.bCorrectRESP)
                    {
                    	throw new BuysenseXMLImportException("RESP is blank string",1);
                    }
                    Log.customer.debug("BuyintERPResponseReader:: before setting DB records");
                    moRecords[i].setStatus(TXNTYPE_DONE);                    
                    moRecords[i].setRetryCount(moRecords[i].getRetryCount());
                    moRecords[i].save();
                    Log.customer.debug("BuyintERPResponseReader:: after setting DB records");
                } 
                catch (BuysenseXMLImportException ex)
                {
                    Log.customer.debug("Inside for loop: BuysenseXMLImportException: " + ex.getMessage());
                    try {
                        updateFailedStatus( moRecords[i], ex);
                        Log.customer.debug("BuyintERPResponseReader:: exception 333");
                    }
                    catch ( Exception loEx)
                    {
                        Log.customer.debug("BuyintERPResponseReader:: exception 222"+loEx.getMessage());
                        if (lsTIN.length() > 0)
                        {
                            lsTIN += (", " + moRecords[i].getTIN());
                        }
                        else
                        {
                            lsTIN = "" + moRecords[i].getTIN();
                        }
                    }
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                    Log.customer.debug("Inside for loop: Exception: " + ex.getMessage());
                    try {
                        Log.customer.debug("BuyintERPResponseReader:: exception 555");
                        updateFailedStatus( moRecords[i], ex);
                    }
                    catch ( Exception loEx)
                    { 
                        // do nothing; Tin will be updated.
                        Log.customer.debug("BuyintERPResponseReader:: exception 444"+loEx.getMessage());
                    }
                    if (lsTIN.length() > 0)
                    {
                        lsTIN += (", " + moRecords[i].getTIN());
                    }
                    else
                    {
                        lsTIN = "" + moRecords[i].getTIN();
                    }
                }
                finally
                {
                    Log.customer.setLevel(currLevel);
                }

            }
            if (lsTIN != null)
            {
               if (lsTIN.length() > 0)
               {
                   logError ("Buyint:error: Failed to import the ERP response for the following TINs: " + lsTIN);
               }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Log.customer.debug("Outside for loop: BuyintResponseXMLReader::processException " + ex.getMessage());
        }
        finally
        {
            Log.customer.setLevel(currLevel);
        }

        Log.customer.setLevel(currLevel);

    }

    private void logError(String fsMessage)
    {
        Logs.buysense.setLevel(Level.DEBUG);
        Logs.buysense.debug(fsMessage);
        Logs.buysense.setLevel(Level.DEBUG.OFF);
    }

    private void updateFailedStatus ( BuyintDBRecExportData foRecord, Exception loEx) throws Exception
    {
        try
        {
            int liRetryCnt = foRecord.getRetryCount();
            Log.customer.debug("inside updateFailedStatus. liRetryCnt = " + liRetryCnt);

            String lsErrorMessage = (loEx.getMessage()).trim();
            boolean lboolSkipRetry = false;

            if (loEx instanceof BuysenseXMLImportException)
            {
               if (((BuysenseXMLImportException)loEx).getErrorCode() == ERR_CODE_SKIP_RETRY)
               {
                  lboolSkipRetry = true;
               }
            }
    
            if(liRetryCnt == 0)
            {
                foRecord.setErrorMessage(lsErrorMessage);
            }
            else
            {
                foRecord.setErrorMessage(foRecord.getErrorMessage()+ "; " + lsErrorMessage);
            }

            if ( (liRetryCnt + 1) >= miRetryThreshold || lboolSkipRetry )
            {
                Log.customer.debug("TIN : " + foRecord.getTIN() + " failed after max retry attempts.");
                foRecord.setStatus("ERR");
                String lsEmailSubject = "BuyintERPResponseReader task failed to process TIN: " + foRecord.getTIN();
                String lsEmailBody = "Response to Document ID: " + foRecord.getDocumentID() + " could not be retrieved. "
                                     + "Please resubmit.";
                sendEmail(lsEmailBody, lsEmailSubject);
            }
            ++liRetryCnt;
            foRecord.setRetryCount(liRetryCnt);
            foRecord.save();

        }
        catch ( Exception ex)
        {
            throw ex;
        }
    }

    /* Send email on error */
    private void sendEmail(String fsEmailBody, String fsEmailSubject)
    {
        Permission pa = Permission.getPermission(msFailedTXNPermission);
        if (pa == null)
        {
            Log.customer.debug("Could not retrieve permission " + msFailedTXNPermission + ".");
            return;
        }

        // Ariba 8.1: List::users() has been deprecated by List::getUsers()
        // VEPI Dev #541: getUsers() does not work in 8.1 ... using getAllUsers() instead.
        List users = pa.getAllUsers();
        if(users == null || users.isEmpty())
        {
            Log.customer.debug("No users having " + msFailedTXNPermission + " permission were found.");
            return;
        }

        Log.customer.debug("Will be sending emails to " + users.size() + " users.");
        BuysenseEmailAdapter.sendMail(users, fsEmailSubject, fsEmailBody);
    }

    //Extend the init method to get parameters from the scheduled task .table file
    public void init(Scheduler scheduler, String scheduledTaskName, Map arguments)
    {
        super.init(scheduler, scheduledTaskName, arguments);
        String lsKey = null;

        if (moPart == null)
        {
            moPart = Base.getService().getPartition("pcsv");
        }

        //Get Database connection information from Parameters.table file
        msDBURL = Base.getService().getParameter(moPart,"Application.AMSParameters.BuyintDBURL");
        msDBUser = Base.getService().getParameter(moPart,"Application.AMSParameters.BuyintDBUser");
        msDBPassword = Base.getService().getParameter(moPart,"Application.AMSParameters.BuyintDBPassword");

        for (Iterator loItr = arguments.keySet().iterator(); loItr.hasNext();)
        {
           lsKey = (String)loItr.next() ;

           if(lsKey.equals("MaxTransactions"))
           {
              String lsMax = (String)arguments.get(lsKey);
              miMaxRecords = Integer.parseInt(lsMax);
           }
           else if ( lsKey.equals( "RetryThreshold" ) )
           {
              String lsRetryValue = (String)arguments.get( lsKey ) ;
              try
                {
                    miRetryThreshold = Integer.parseInt( lsRetryValue ) ;
                }
                catch ( NumberFormatException loNumExcep )
                {
                    Log.customer.debug( "Invalid retry threshold value specified, resetting to default value." ) ;
                    miRetryThreshold = BuyintERPResponseReader.RETRY_DEFAULT ;
                }
                Log.customer.debug( "Retry Threshold = " + miRetryThreshold + "." ) ;
            }

            else if ( lsKey.equals( "MaxTransactions" ) )
            {
                String lsMaxTransactionsValue = (String)arguments.get( lsKey ) ;
                try
                {
                    miMaxRecords = Integer.parseInt( lsMaxTransactionsValue ) ;
                }
                catch ( NumberFormatException loNumExcep )
                {
                    Log.customer.debug( "Invalid max transactions value specified, resetting to default value." ) ;
                    miMaxRecords = BuyintERPResponseReader.MAX_TRANSACTIONS_DEFAULT ;
                }
                Log.customer.debug( "Max Records = " + miMaxRecords + "." ) ;
            }
            else if(lsKey.equals("EmailPermission"))
            {
                msFailedTXNPermission = (String) arguments.get(lsKey);
                Log.customer.debug("Email permission parameter specified as " + msFailedTXNPermission + ".");
            }

        }//end for
        try
        {
            //Instantiating new objects
            BuyintDBIO moDbIo = new BuyintDBIO(msDBURL,msDBUser,msDBPassword);
            moDbRec = new BuyintDBRecExportData(moDbIo);

            moDOMFactory = DocumentBuilderFactory.newInstance();
            moDOMFactory.setValidating(true);
            Log.customer.debug("About to create new DocumentBuilder ... ");
            moDOMFactory.newDocumentBuilder();
        }
        catch (ParserConfigurationException ex)
        {
            String lsErrorMessage = ex.getMessage();
            Log.customer.debug("ParserConfigurationException Error: " + lsErrorMessage);
        }
    }//end init


}
