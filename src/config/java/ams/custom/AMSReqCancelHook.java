/*
    Copyright (c) 1996-2001 Ariba/Buysense, Inc.
    Anup - June 2001
    ---------------------------------------------------------------------------------------------------
    RLee, 7/30/02, eVA Dev SPL 64, should only allow cancel of requisition
    when req is in "Ordered" status. This should be a baseline function.

*/

/* 09/05/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;


import ariba.approvable.core.Approvable;
import java.util.List;
// Ariba 8.0: Replaced deprecated ApprovableHook
import ariba.approvable.core.ApprovableHook;
//import ariba.approvable.core.ApprovableHook;
import ariba.util.log.Log;
// Ariba 8.0: Deprecations, methods have changed packages.
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;

//81->822 changed Vector to List
public class AMSReqCancelHook implements ApprovableHook
{
    // Ariba 8.0: Replaced deprecated method
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));
    
    public List run (Approvable approvable)
    {
        Log.customer.debug("ReqCancel Hook got called");
        
        // RLee - SPL 64
        String status = (String)(approvable.getFieldValue("StatusString"));
        if (status.trim().equals("Ordering"))
        {
            // Ariba 8.0: Replaced deprecated method
            return ListUtil.list(Constants.getInteger(-1),
                                     "This Requisition is in Ordering status.  A Requisition cannot be cancelled until it has reached Ordered status.");
        }
        
        
        //Anup - Generic invocation of dynamically determined Client/Agency edits. - Dev SPL #173
        List result = EditClassGenerator.invokeHookEdits(approvable,
                                                           this.getClass().getName());
        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
        int errorCode = ((Integer)result.get(0)).intValue();
        Log.customer.debug("Validations in " + this.getClass().getName() + ", error Code --> %s",errorCode);
        if(errorCode<0)
        {
            Log.customer.debug("Validations Failed in " + this.getClass().getName() + " - returning error to UI");
            return result;
        }
        
        return NoErrorResult;
    }
}
