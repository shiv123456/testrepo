/*
    Copyright (c) 1996-2000 Ariba, Inc.
    All rights reserved. Patents pending.

    $Id$

    Responsible: imohideen

    CHANGES:

    DATE            DEVEL OPER          COMMENTS
    11/10/2000      rohit               At the end of processing, push a new transaction to the Integration Database
                                        that will suggest the integrator to update the status of this current transaction
                                        to be completed (CMP)
    STSPL 442       imohideen           Added code to handle the Liquidation pull back
    05/16/2001      iberaha             Baseline Dev SPL #210 - TXN Push and AdapterError Enhancements
*/

/* 08/13/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 09/10/2003: Updates for Ariba 8.0 (David Chamberlain) */
// 10/10/2003: Updates for Ariba 8.1 (Richard Lee)
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)

03/22/2004 - Ariba 8.1 Dev SPL #41 rgiesen - Created a custom object called BuysenseTXNStatus which is used instead of
	AdapterError (7.1) or IntegrationError (8.1).  This object is populated with the BuyINT record
	which has been updated by Advantage.
*/

package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.base.core.*;
// Ariba 8.1 import ariba.common.core.Permission;
import ariba.user.core.Permission;
import ariba.common.core.User;
import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
/* 09/09/2003: Updates for Ariba 8.0 (David Chamberlain) */
//import ariba.procure.server.OrderRecipientOnServer;
import ariba.util.core.PropertyTable;
import java.util.List;
// Ariba 8.1 import ariba.common.core.Money;
import ariba.basic.core.Money;
import ariba.common.core.Core;
// Ariba 8.0: IntegrationPostLoadPOError moved so changed import statement.
//import ariba.procure.server.action.IntegrationPostLoadPOError;
import ariba.util.core.Date;
// Ariba 8.0: imported Assert.class for change of deprecated Util.assertNonFatal().
import ariba.util.core.Assert;

import config.java.ams.custom.PostERPProcessor;


/**
    @aribaapi internal
*/
//81->822 changed Vector to List
public class BuysenseIntPostLoad extends Action
{

    protected static final String SubmitParam = "Submit";

    private static final String[] requiredParameterNames =
        {
        SubmitParam
    };
    private String emailBody = "";


    public void fire (ValueSource object, PropertyTable params)
    {
        Log.customer.debug("BuysenseIntPostLoad: fire() - Received this object %s", object);

        //Ariba 8.1 Dev SPL #41 - instead of AdapterError or IntegrationError object, use the created
        // BuysenseTXNStatus object, which has been specially created for this process
        ClusterRoot vsBuysenseTXNStatus = (ClusterRoot)object;

        //Get the partition
        Partition part = vsBuysenseTXNStatus.getPartition();

		//Get the aribasystem user at the partition level - caution - you might need the shared user.  Use getUser()
        User aribaSystemUser = Core.getService().getAribaSystemUser(part);

        String aeUniqueName = (String) vsBuysenseTXNStatus.getFieldValue("UniqueName");

        //Ariba 8.1 Dev SPL #41 - instead of AdapterError or IntegrationError object, use the created
        // BuysenseTXNStatus object, which has been specially created for this process
        if  ((ValueSource)Base.getService().objectMatchingUniqueName
                ("ariba.core.BuysenseTXNStatus",
                 part,aeUniqueName) != null )
        {
            Log.customer.debug("BuysenseIntPostLoad: This BuysenseTXNStatus object has been pulled already.  %s",aeUniqueName);
            return ;
        }

        Log.customer.debug("BuysenseIntPostLoad Got past the BuysenseTXNStatus creation : %s" , vsBuysenseTXNStatus.toString());

		//Ariba 8.1 Dev SPL #41 - using the BuysenseTXNStatus, instead of AdapterError
        String errorId = (String)vsBuysenseTXNStatus.getFieldValue("TIN");
        String type = (String)vsBuysenseTXNStatus.getFieldValue("Type");
        String errorERP = (String)vsBuysenseTXNStatus.getFieldValue("ERP");

        String recordType=null;
        String ORMSSTATUS  = (String)vsBuysenseTXNStatus.getFieldValue("ErrorValue");
        String SEVERITY     = (String)vsBuysenseTXNStatus.getFieldValue("ErrorColumn");
        //    e2e Shakedown SPL #77 - stop using BuysenseTXNStatus.ErrorCode to pull back UniqueName; use BuysenseTXNStatus.ORMSUniqueName instead
        String ORMS_UNIQUE_NAME  = (String)vsBuysenseTXNStatus.getFieldValue("ORMSUniqueName");
        String TXNTYPE = (String)vsBuysenseTXNStatus.getFieldValue("txntype");
        String ERPNUMBER = (String) vsBuysenseTXNStatus.getFieldValue("erpnumber");
        ClusterRoot po=null;
        ClusterRoot req=null;
        Log.customer.debug("BuysenseIntPostLoad: IXM Got past getting the values with ORMS unique Name : " + ORMS_UNIQUE_NAME);

        try
        {
            if ( TXNTYPE.equals("RX"))
            {
                Log.customer.debug("BuysenseIntPostLoad: Pulled back a Requisition");
                String reqUniqueName = (String) vsBuysenseTXNStatus.getFieldValue("ORMSUniqueName");
                req = (ariba.purchasing.core.Requisition)Base.getService().objectMatchingUniqueName( "ariba.purchasing.core.Requisition",part,reqUniqueName);

                String reqName = (String) req.getFieldValue("Name");

                Log.customer.debug("BuysenseIntPostLoad: %s: This is the Name of the Requisition",reqName);
                // pb050200 modificaqtion for cancel
                // in the push, we save total amount and unique name in case later a re-preencumbrance occurs
                req.setFieldValue("OriginalTotalAmount",new Money((Money)req.getFieldValue("TotalCost")));

                String UniqueName = (String)req.getFieldValue("UniqueName");
                req.setFieldValue("OriginalUniqueName", new String(UniqueName));
                req.setFieldValue("LastPreEncumberDate",new Date(System.currentTimeMillis()));
                // mod end

                //We are using the PostERPProcessor now
                PostERPProcessor pep = new PostERPProcessor();
                Log.customer.debug("BuysenseIntPostLoad: calling processReq");
                pep.processReq(vsBuysenseTXNStatus,
                               (ariba.purchasing.core.Requisition)req,
                               aribaSystemUser, params);
            }

            if ( TXNTYPE.equals("RXC"))
            {
                Log.customer.debug("BuysenseIntPostLoad: Pulled back a Requisition Cancel");
                String reqUniqueName = (String) vsBuysenseTXNStatus.getFieldValue("ORMSUniqueName");
                req = (ariba.purchasing.core.Requisition)Base.getService().objectMatchingUniqueName( "ariba.purchasing.core.Requisition",part,reqUniqueName);
                String reqName = (String) req.getFieldValue("Name");

                Log.customer.debug("BuysenseIntPostLoad: %s: This is the Name of the Requisition",reqName);
                // pb050200 modificaqtion for cancel
                // in the push, we save total amount and unique name in case later a re-preencumbrance occurs
                req.setFieldValue("OriginalTotalAmount",
                                  new Money((Money)req.getFieldValue("TotalCost")));
                String UniqueName = (String)req.getFieldValue("UniqueName");
                req.setFieldValue("OriginalUniqueName", new String(UniqueName));
                req.setFieldValue("LastPreEncumberDate",
                                  new Date(System.currentTimeMillis()));
                // mod end
                if ( SEVERITY.equals("OK") || SEVERITY.equals("WARN") )
                {
                    recordType= "CancelRecord";
                    req.setFieldValue("RequisitionStatus","New");
                }
                else
                {
                    recordType= "CancelFailedRecord";
                    req.setFieldValue("RequisitionStatus","WithdrawError");
                }

                //Create the history object

                BaseObject hist = (ariba.procure.core.SimpleProcureRecord) BaseObject.create("ariba.procure.core.SimpleProcureRecord",part);

                hist.setFieldValue("Approvable",req);
                hist.setFieldValue("ApprovableUniqueName", req.getFieldValue("UniqueName"));
                ariba.util.core.Date  testDate =  new ariba.util.core.Date();
                hist.setFieldValue("Date", testDate);

                //Ariba 8.1 Dev SPL #41 use the shared user
                //hist.setFieldValue("User", aribaSystemUser);
                hist.setFieldValue("User", aribaSystemUser.getUser());
                hist.setFieldValue("RecordType",recordType);
                // Ariba 8.1 changd addElement to add
                ((List)hist.getFieldValue("Details")).add(vsBuysenseTXNStatus.getFieldValue("resultstring"));

                //Now add this history record to the Req.
                // Ariba 8.1 changed addElement to add
                ((List)req.getFieldValue("Records")).add(hist);

                String b1=reqUniqueName;
                //String b1=((String)approvable.getFieldValue("UniqueName"));
                int i1 = b1.indexOf("v");
                if (i1!=-1)
                {
                    String st1=b1.substring(i1+1);
                    String st2=b1.substring(0, (i1+1));
                    Integer I=Integer.valueOf(st1);
                    int i=I.intValue();
                    System.out.println(i);
                    i++;
                    System.out.println(i);
                    Integer I2=new Integer(i);
                    String b4=I2.toString();
                    b1=st2+b4;
                    System.out.println(b1);
                    req.setFieldValue("UniqueName", b1);
                    String b5=(String)(req.getFieldValue("UniqueName"));
                    Log.customer.debug("BuysenseIntPostLoad UniqueName is: " + b5);
                }
                else
                {
                    int VNum=0;
                    VNum=VNum+1;
                    Integer i=new Integer(VNum);
                    String b2=i.toString();
                    String UName=b1+"v"+b2;
                    req.setFieldValue("UniqueName", UName);
                    String b3=(String)(req.getFieldValue("UniqueName"));
                    Log.customer.debug("BuysenseIntPostLoad UniqueName is: " + b3);
                }
            }

            else if ( TXNTYPE.equals("PC"))
            {
                Log.customer.debug("BuysenseIntPostLoad: Pulled back a Purchase Order");

                String poUniqueName = (String) vsBuysenseTXNStatus.getFieldValue("ORMSUniqueName");

                po = (ariba.purchasing.core.ERPOrder)Base.getService().objectMatchingUniqueName( "ariba.purchasing.core.ERPOrder",part,poUniqueName);

                List tempVector = (List) po.getFieldValue("LineItems") ;
                // Ariba 8.1 changed ElementAt to get
                ariba.purchasing.core.POLineItem line = (ariba.purchasing.core.POLineItem)tempVector.get(0);

                req = (ariba.purchasing.core.Requisition) line.getFieldValue("Requisition");
                PostERPProcessor pep = new PostERPProcessor();

                Log.customer.debug("BuysenseIntPostLoad: calling processPO");
                pep.processPO(vsBuysenseTXNStatus,
                              (ariba.purchasing.core.ERPOrder)po,
                              (ariba.purchasing.core.Requisition)req,
                              aribaSystemUser, params);
            }
            else if ( TXNTYPE.equals("PV"))
            {
                Log.customer.debug("BuysenseIntPostLoad: Trying to pull back a PV ");
                String pvUniqueName = (String) vsBuysenseTXNStatus.getFieldValue("ORMSUniqueName");
                BaseObject pvObject  = (BaseObject)( Base.getService().objectMatchingUniqueName ("ariba.core.PaymentEform",part,pvUniqueName));
                //Log.customer.debug("This is the UniqueName of the Payment: %s", (String) pv.getFieldValue("Name"));
                PostERPProcessor pep = new PostERPProcessor();

                Log.customer.debug("BuysenseIntPostLoad: calling processPV");
                pep.processPV(vsBuysenseTXNStatus,pvObject,aribaSystemUser, params);
            }

            //STSPL 442 - Implements the Liquidation pull back
            else if ( TXNTYPE.equals("PVX")|| TXNTYPE.equals("PCX"))
            {
                Log.customer.debug("BuysenseIntPostLoad: Trying to pull back a PVX");
                String liqUniqueName = (String) vsBuysenseTXNStatus.getFieldValue("ORMSUniqueName");
                BaseObject liqObject  = (BaseObject)( Base.getService().objectMatchingUniqueName ("ariba.core.Liquidation",part,liqUniqueName));
                Log.customer.debug("BuysenseIntPostLoad: This is the UniqueName of the Liquidation: %s",liqUniqueName);
                PostERPProcessor pep = new PostERPProcessor();
            }

            //e2e
            else if ( TXNTYPE.equals("UR"))
            {

                String urUniqueName = (String) vsBuysenseTXNStatus.getFieldValue("ORMSUniqueName");
                Log.customer.debug("BuysenseIntPostLoad: Trying to pull back UR: %s",urUniqueName);

                if ( urUniqueName != null )
                {
                    ariba.purchasing.core.ERPOrder ur = (ariba.purchasing.core.ERPOrder)Base.getService().objectMatchingUniqueName("ariba.purchasing.core.ERPOrder", part, urUniqueName);

                    List tempVector = (List) ur.getFieldValue("LineItems");
                    // Ariba 8.1 changed ElementAt to get
                    ariba.purchasing.core.POLineItem urLine = (ariba.purchasing.core.POLineItem)tempVector.get(0);
                    ariba.purchasing.core.Requisition reqToUpdate = (ariba.purchasing.core.Requisition) urLine.getFieldValue("Requisition");

                    PostERPProcessor pep = new PostERPProcessor();
                    Log.customer.debug("BuysenseIntPostLoad: calling processUR");
                    pep.processUR(vsBuysenseTXNStatus, ur, reqToUpdate, aribaSystemUser, params);
                }
            }
            //e2e

            //At the end of processing, push a new transaction to the Integration Database that will suggest the integrator to
            //update the status of this current transaction to be completed (CMP)

			Log.customer.debug("BuysenseIntPostLoad: Before AMSTXN.push aeUniqueName: " + aeUniqueName);
            int ret = AMSTXN.push("UPD","RDY",aeUniqueName,part);
        }
        catch (NullPointerException e)
        {
            Log.customer.debug("BuysenseIntPostLoad: NullPointerException");
            //Ariba 8.1 Dev SPL #41 - display, in the log file, the stack trace for the NullPointerException
            Log.customer.debug(SystemUtil.stackTrace(e));
        }
        catch (Exception e)
        {
            Log.customer.debug("BuysenseIntPostLoad: Exception");
            //Ariba 8.1 Dev SPL #41 - display, in the log file, the stack trace for the NullPointerException
            Log.customer.debug(SystemUtil.stackTrace(e));

            // Ariba 8.0: Util.assertNonFatal has been deprecated by Assert.assertNonFatal.
            Assert.assertNonFatal(false,"BuysenseIntPostLoad Error Catch");

            Log.customer.debug("BuysenseIntPostLoad: About to delete the BuysenseTXNStatus object");
            try
            {
				//Ariba 8.1 Dev SPL #41 - instead of AdapterError or IntegrationError object, use the created
				// BuysenseTXNStatus object, which has been specially created for this process

                String tempAeUniqueName = (String)vsBuysenseTXNStatus.getFieldValue("UniqueName");
                String tempAeErpnumber = (String)vsBuysenseTXNStatus.getFieldValue("erpnumber");
                vsBuysenseTXNStatus.delete();
                //Send an e-mail to notify the OPS BuysenseTXNStatus got deleted
                emailBody = "";
                emailBody = "Deleting BuysenseTXNStatus object: " + tempAeUniqueName +
                    ", ERP Number: " + tempAeErpnumber + "\n";
                sendSummary();
            }
            catch (Exception exE)
            {
                Log.customer.debug("Error occured while trying to delete the BuysenseTXNStatus object");
				//Ariba 8.1 Dev SPL #41 - display, in the log file, the stack trace for the NullPointerException
	            Log.customer.debug(SystemUtil.stackTrace(exE));
            }
        }

    }
    //SendSummary method sends an e-mail using buysense e-mail wrapper
    private void sendSummary()
    {
        String failedTXNPermission = "FailedTXN";
        Permission pa = Permission.getPermission(failedTXNPermission);
        if (pa == null)
        {
            Log.customer.debug("Could not retrieve permission " + failedTXNPermission + ".");
            return;
        }
        // Ariba 8.1: Permission::users() is deprecated by Permission::getAllUsers()
        List users = pa.getAllUsers();
        if(users == null || users.isEmpty())
        {
            Log.customer.debug("No users found that has the " + failedTXNPermission + " permission.");
            return;
        }
        String subject = "FailedTXN scheduled task republished the following transactions.";
        BuysenseEmailAdapter.sendMail(users, subject, emailBody);
    }
}

