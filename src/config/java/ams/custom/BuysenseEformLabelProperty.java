package config.java.ams.custom;

import ariba.base.core.*;
import ariba.base.fields.*;
import ariba.htmlui.fieldsui.Log;

public class BuysenseEformLabelProperty implements FieldPropertiesFilter
{
public Object getPropertyForKey(String sPropName, String sFieldName, FieldProperties fp)
{
    TransientData td =  Base.getSession().getTransientData();

	String sProfileUniqueName = (String)td.get("EformLabel");
	Log.customer.debug("Retrieve client Name "+sProfileUniqueName);
    if (sProfileUniqueName == null)
    {
        Log.customer.debug("Found this profile: %s"+sProfileUniqueName);
    	return fp.getPropertyForKey("Label");
	}

    return BuysenseEformUtil.findFieldLabel(sProfileUniqueName, sFieldName);
}
}
