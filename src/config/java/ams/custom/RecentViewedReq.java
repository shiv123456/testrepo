/**
3/10/2004 Shane Liu

Baseline DEV SPL 10 - Store clientUniqueName from Requester of the Req that's being viewed.
					The string is stored using TransientData, from BaseSession.

*/

package config.java.ams.custom;

import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.htmlui.fieldsui.Log;
import ariba.base.core.*;

//81->822 changed ConditionValueInfo to ValueInfo
/**
* @author Manoj Gaur
* @version 2 DEV
* @reference CSPL-604
* @see Date 08-12-2008
* @Explanation Added ActiveClusterRoot validity with this java.
* */
public class RecentViewedReq extends Condition
{
    private static final ValueInfo parameterInfo[];
    private static final String requiredParameterNames[] =
        {
        "TargetValue"
    };

	public boolean evaluate (Object value, PropertyTable params)
	{
            Log.customer.debug("rlee value = " + value);
		String clientUniqueName = (String)params.getPropertyForKey("TargetValue");

		if (clientUniqueName != null)
		{
			Log.customer.debug("RecentViewedReq:  storing " + clientUniqueName);
			TransientData td =  Base.getSession().getTransientData();
	        td.put("AMSLabel", clientUniqueName);
		}
		else
			Log.customer.debug("RecentViewedReq:  clientName is null");

       if(value instanceof ClusterRoot)
           	return ((ClusterRoot)value).getActive();

       // return true, if object is not a Cluster Root object
		return true;

	}


    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    protected String[] getRequiredParameterNames()
    {
        return requiredParameterNames;
    }

    public RecentViewedReq()
    {
    }

    static
        {
        parameterInfo = (new ValueInfo[]
                         {
                new ValueInfo("TargetValue",
                                       0),
            }
        );
    }
}
