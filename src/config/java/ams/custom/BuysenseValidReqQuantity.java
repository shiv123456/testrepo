package config.java.ams.custom;

import ariba.approvable.core.LineItemCollection;
import ariba.procure.core.ProcureLineItem;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.condition.ValidReqQuantity;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

/**
 * CSPL-6151: Ariba 9r1 - Allow zero Quantity on Change Order - post SP30.
 * 
 * @author Pavan Aluri
 * @Date 25-Mar-2014
 * @version 1.0
 * @Explanation isInclusive method of ValidReqQuantity (SP30) is overridden to include min value(0) for Change Order in order to allow zero $ value.
 * Note: For Quantity field, min property is not defined and system takes it as 0.0D
 * 
 */

public class BuysenseValidReqQuantity extends ValidReqQuantity
{
    private String sClass = "BuysenseValidReqQuantity";


    /*/
     * For reference, SP30 OOTB version of isInclusive() method is commented as below.
     */
    /*
    protected boolean isInclusive(PropertyTable params)
    {
        Object paramValue = params.getPropertyForKey("Inclusive");
        if(paramValue != null)
            return params.booleanPropertyForKey("Inclusive");
        ProcureLineItem lineItem = (ProcureLineItem)params.getPropertyForKey(ProcureLineItemParam);
        if(lineItem instanceof ReqLineItem)
            return false;
        else
            return super.isInclusive(params);
    }
     */
    protected boolean isInclusive(PropertyTable params)
    {
        Object paramValue = params.getPropertyForKey("Inclusive");
        if(paramValue != null)
            return params.booleanPropertyForKey("Inclusive");
        ProcureLineItem lineItem = (ProcureLineItem) params.getPropertyForKey(ProcureLineItemParam);
        if (lineItem != null && lineItem instanceof ReqLineItem)
        {
            ReqLineItem rli = (ReqLineItem) lineItem;
            LineItemCollection lic = rli.getLineItemCollection();
            if (lic != null && lic.getPreviousVersion() != null)
            {
                Log.customer.debug(sClass + ":isInclusive returning true");
                return true;
            }
        }
            return super.isInclusive(params);
    }

}
