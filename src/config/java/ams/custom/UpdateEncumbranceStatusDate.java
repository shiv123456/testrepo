/************************************************************************************
 * Author:  Richard Lee
 * Date:    January 31, 2006
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 01/31/2006        Richard Lee            Set EncumbranceStatusChangeDate
 *
 * @(#)UpdateEncumbranceStatusDate.java     1.0 01/31/2006
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/

/* 
 * rlee, 1/31/2006:  Dev SPL 719 - INT Set EncumbranceStatusChangeDate with this trigger
 *
 */
package config.java.ams.custom;

import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.purchasing.core.PurchaseOrder;

public class UpdateEncumbranceStatusDate extends Action
{
    public void fire (ValueSource object, PropertyTable parameters)
        throws ActionExecutionException
    {
        Log.customer.debug("Calling UpdateEncumbranceStatusDate");
        PurchaseOrder loPO = (PurchaseOrder) object;
	loPO.setFieldValue("EncumbranceStatusChangeDate", new Date());
	return;
    }
}
