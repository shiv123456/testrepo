package config.java.ams.custom;
import ariba.approvable.core.Approvable;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseSetSSTitle extends Action
{
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        Log.customer.debug("fire method called");
        Approvable loAppr = null;
        String lsAgencyNumber = null;
        if(valuesource!= null && valuesource instanceof Approvable)
        {
            loAppr = (Approvable)valuesource;
            lsAgencyNumber = (String) loAppr.getFieldValue("AgencyRequestNum");
            Log.customer.debug("AgencyRequestNumber "+lsAgencyNumber);
            setCustomTitle(loAppr, lsAgencyNumber);
        }
    }

    public void setCustomTitle(Approvable appr, String lsAgencyNumber)
    {
        ariba.user.core.User loRequesterUser = (ariba.user.core.User)appr.getRequester();
        if(loRequesterUser != null && lsAgencyNumber !=null)
        {
            ariba.common.core.User loRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(loRequesterUser,appr.getPartition());
            String lsClientName = (String)loRequesterPartitionUser.getDottedFieldValue("ClientName.ClientName");
            Log.customer.debug("ClientName  "+lsClientName);
            if(lsClientName != null)
            {
                appr.setName(lsClientName+ " - Sole Source - " +lsAgencyNumber);
            }
        }
    }
}
