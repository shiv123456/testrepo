//************************************************************************************
// Name:             BuyintExceptionHandler
// Description: Handler class for exceptions thrown during integration processing.
// Author:  David Chamberlain
// Date:    July 27, 2004
// Revision History:
//
//  Date              Responsible            Description
//  ----------------------------------------------------------------------------------
//  07/27/2004        David Chamberlain      Initial Version. Ariba 8.1 Upgrade
//											 Dev SPL 88.
//
//
// Copyright 2001 by American Management Systems, Inc.,
// 4050 Legato Road, Fairfax, Virginia, U.S.A.
// All rights reserved.
//
// This software is the confidential and proprietary information
// of American Management Systems, Inc. ("Confidential Information"). You
// shall not disclose such Confidential Information and shall use
// it only in accordance with the terms of the license agreement
// you entered into with American Management Systems, Inc.
//*************************************************************************************

/* DETAILED DESCRIPTION:
   The BuyintExceptionHandler is concerned with the integration infrastructure and not
   general Ariba processing errors.  Exception handling api's will be provided to all
   subcomponents of the integration system.  That is, if a given module encounters an
   exception, it will pass the XML, the exception type, and the exception detail to the
   exception handler as parameters through the api.  The exception handler will then
   determine if the exception is recoverable and persist the exception to a database table
   for later recovery.  If the exception is such that the operation is not recoverable,
   the exception detail will be written to a log file for analysis.
*/

package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.user.core.*;
import org.apache.log4j.Level;

import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;

public class BuyintExceptionHandler implements BuyintConstants
{
	private final int MAX_RETRY_COUNT = 1;

	private String msDBURL;
	private String msDBUser;
	private String msDBPassword;
	private BuyintException moException;
	private String msTransactionType;
	private ClusterRoot moAribaObject;
	private String msErrorMessage;
	private String msXML;
	private BuyintDBRecExceptionRecovery moDbRec;
    private static Partition moPart = Base.getSession().getPartition();

	//Constructor
	public BuyintExceptionHandler()
	{
		if(moPart == null)
		{
			moPart = Base.getService().getPartition("pcsv");
        }

        //Get Database connection information from Parameters.table file
        msDBURL = Base.getService().getParameter(moPart,"Application.AMSParameters.IntegrationDBURL");
        msDBUser = Base.getService().getParameter(moPart,"Application.AMSParameters.IntegrationDBUser");
        msDBPassword = Base.getService().getParameter(moPart,"Application.AMSParameters.IntegrationDBPassword");

		//Create the DBIO object
		BuyintDBIO moDbIo = new BuyintDBIO(msDBURL,msDBUser,msDBPassword);

		//Create the DBRec object
        moDbRec = new BuyintDBRecExceptionRecovery(moDbIo);
	}

	//This api is used to throw an integration processing exception.
	public void handleException(BuyintException foException, String fsTransactionType, ClusterRoot foAribaObject,
															String fsXML, String fsErrorMessage)
	{
		Log.customer.debug("*** In BuyintExceptionHandler::handleException ***");

                foException.printStackTrace();

		this.moException = foException;
		this.msTransactionType = fsTransactionType;
		this.moAribaObject = foAribaObject;
		this.msXML = fsXML;
		this.msErrorMessage = fsErrorMessage;

		String lsDocId = null;
		String lsDocType = null;

		try
		{
			//From the clusterRoot object, get the doc type and docId
			lsDocId = moAribaObject.getUniqueName();
			lsDocType = (String)moAribaObject.getTypeName();

			Log.customer.debug("* lsDocType: " + lsDocType);
			Log.customer.debug("* lsDocId: " + lsDocId);
			Log.customer.debug("* msTransactionType: " + msTransactionType);

			//Deletes the record from Export_Data table with null transaction data 
		    //before writing to Exception_Recovery table.
			boolean lbDeleted = moDbRec.deleteNullTransRecord(lsDocId);
			Log.customer.debug("* returned value for lbDeleted " +lbDeleted);
			
			//Try and retrieve an exception for this document
			boolean lbSuccess = moDbRec.getRec("WHERE DOCUMENT_TYPE = '" + lsDocType + "' " +
											   "AND DOCUMENT_ID = '" + lsDocId + "' " +
											   "AND TRANSACTION_TYPE = '" + msTransactionType + "' " +
											   "AND STATUS = '" + STATUS_INPROGRESS + "'");

			if(lbSuccess = false)
			{
				Log.customer.debug("BuyintExceptionHandler::handle() failed on call to BuyintDBRecExceptionRecovery::getRec().");
				return;
			}

			Log.customer.debug("* moDbRec.docid: " + moDbRec.getDocumentID());
			Log.customer.debug("* moDbRec.Status: " + moDbRec.getStatus());
			Log.customer.debug("* moDbRec.doctype: " + moDbRec.getDocumentType());
			Log.customer.debug("* moDbRec.TransType: " + moDbRec.getTransactionType());

			//If the Status is null then there was no exception and this is a new exception insert
			if(moDbRec.getStatus() == null)
			{
				Log.customer.debug("New exception");

				//Set the fields for the exception insert
				if(lbDeleted)
				{
					moDbRec.setStatus(STATUS_FAILED);
					moDbRec.setErrorMessage(" Exception Type: Null Transaction Data");
				}
				else
				{
					moDbRec.setStatus(STATUS_READY);
					moDbRec.setErrorMessage(" Exception Type: " + moException.getType() +
							" Error Message: " + this.msErrorMessage);				
				}
				moDbRec.setRetryCount(0);
				moDbRec.setTransactionType(msTransactionType);
				moDbRec.setDocumentID(lsDocId);
				moDbRec.setDocumentType(lsDocType);
				moDbRec.setTransactionData(msXML);
				
				//Save the record
				this.moDbRec.save();
			}
			else  //If we are here, then this is a retry of an existing exception
			{
				Log.customer.debug("Retry Existing exception");

				//Increment the retry count to see if this is last retry
				moDbRec.setRetryCount(moDbRec.getRetryCount() + 1);

				//If the retry count isn't maxed out then set the record up for retry, else set it to failed
				if(moDbRec.getRetryCount() >= MAX_RETRY_COUNT)
				{
					//Last retry so set status to failed
					moDbRec.setStatus(STATUS_FAILED);

					//Set error message
					String lsErrorMessage = "Buyint:error: Transaction for Approvable = " + lsDocId + " Failed due to maximum retry exceeded.";

					//Notify the Administrator that the transaction has failed
					this.logError(lsErrorMessage);
				}
				else
				{
					//retry again
					moDbRec.setStatus(STATUS_READY);
				}

				//Set the fields for the exception update
				moDbRec.setTransactionType(msTransactionType);
				moDbRec.setErrorMessage(moDbRec.getErrorMessage() + "; Exception Type: " +
										moException.getType() + " Error Message: " + this.msErrorMessage);

				//Save the record
				moDbRec.save();

			}//end if/else
		}//end try
		catch(Exception e)
		{
			Log.customer.debug("BuyintExceptionHandler::handleException() failed with an exception.");
			e.printStackTrace();

			//Set error message
                        String lsErrorMessage = "Buyint:error: Transaction for Approvable = " + lsDocId + " Failed due to exception.";


                        //Notify the Administrator that the transaction has failed
                        this.logError(lsErrorMessage);
		}//end catch


		Log.customer.debug("*** Leaving BuyintExceptionHandler::handleException ***");
	}

	private void logError(String fsMessage)
	{
	    Logs.buysense.setLevel(Level.DEBUG);
		Logs.buysense.debug(fsMessage);
		Logs.buysense.setLevel(Level.DEBUG.OFF);
	}

}
