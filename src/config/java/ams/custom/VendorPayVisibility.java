/*
 * @(#) VendorPayVisibility.java     1.0 06/26/2008
 *
 * Copyright 2008 by CGI
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of CGI ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with CGI
 */

package config.java.ams.custom;

import ariba.base.core.BaseVector;
import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.htmlui.fieldsui.Log;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;

public class VendorPayVisibility extends Condition
{
    private static final ValueInfo parameterInfo[];
    private static final String requiredParameterNames[] = {"TargetValue"};

    public boolean evaluate(Object value, PropertyTable params)
    {
        Log.customer.debug("Calling VendorPayVisibility evaluate: %s ", value);
        return testValue(value, params);
    }
    protected boolean testValue(Object value, PropertyTable params)
    {
        Requisition loReq   = (Requisition)params.getPropertyForKey("TargetValue");
	BaseVector lvLineItems = (BaseVector)loReq.getLineItems();
	int liSize = lvLineItems.size();
	ReqLineItem loItem = null;
	for(int i=0; i<liSize; i++)
	{
	    loItem = (ReqLineItem) lvLineItems.get(i);
	    String lsAdapterSource = (String) loItem.getDottedFieldValue("SupplierLocation.AdapterSource");
	    if( StringUtil.nullOrEmptyOrBlankString(lsAdapterSource))
	    {
		return true;
	    }
	}
	return false;
    }

    public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
    {
	return null;
    }

    protected ValueInfo[] getParameterInfo()
    {
 	return parameterInfo;
    }
    protected String[] getRequiredParameterNames()
    {
  	return requiredParameterNames;
    }
    public VendorPayVisibility()
    {
    }

    static {parameterInfo = (new ValueInfo[]{new ValueInfo("TargetValue", 0),
			 new ValueInfo("Message", 0, Behavior.StringClass)});
    }
}
