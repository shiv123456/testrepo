/************************************************************************************
 * Author:  Ivan Beraha
 * Date:    Nov 30, 2004
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 11/30/2003        Ivan Beraha            ER 17
 *
 * @(#)CommonSupplierSWAMPostLoad.java     1.0 11/30/2004
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/

package config.java.ams.custom;

import ariba.base.core.*;
import ariba.util.log.Log;
import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.common.core.Supplier;
import ariba.common.core.CommonSupplier;

public class CommonSupplierSWAMPostLoad extends Action
{
    public void fire ( ValueSource object, PropertyTable params )
    {
        Boolean lbSmallBusiness = new Boolean( "false" ) ;
        Boolean lbMinorityOwnedBusiness = new Boolean( "false" ) ;
        Boolean lbWomanOwnedBusiness = new Boolean( "false" ) ;
        Boolean lbMicroBusiness = new Boolean( "false" ) ;

        try
        {
           ClusterRoot loObject = (ClusterRoot) object ;
           Supplier loPartSupplier = (Supplier) loObject ;
           CommonSupplier loCommonSupplier = (CommonSupplier) loPartSupplier.getFieldValue( "CommonSupplier" ) ;
           if ( loCommonSupplier != null )
           {
               lbSmallBusiness = (Boolean) loPartSupplier.getFieldValue("SmallBusiness") ;
               lbMinorityOwnedBusiness = (Boolean) loPartSupplier.getFieldValue("MinorityOwnedBusiness") ;
               lbWomanOwnedBusiness = (Boolean) loPartSupplier.getFieldValue("WomanOwnedBusiness") ;
               lbMicroBusiness = (Boolean) loPartSupplier.getFieldValue("MicroBusiness") ;

               //Log.customer.debug( "SmallBusiness = %s, MinorityOwnedBusiness = %s, WomanOwnedBusiness = %s ",
               //                    lbSmallBusiness, lbMinorityOwnedBusiness, lbWomanOwnedBusiness ) ;
           }

           if( lbSmallBusiness != null )
           {
               loCommonSupplier.setDottedFieldValue( "SmallBusiness", lbSmallBusiness ) ;
           }
           if( lbMinorityOwnedBusiness != null )
           {
               loCommonSupplier.setDottedFieldValue( "MinorityOwnedBusiness", lbMinorityOwnedBusiness ) ;
           }
           if( lbWomanOwnedBusiness != null )
           {
               loCommonSupplier.setDottedFieldValue( "WomanOwnedBusiness", lbWomanOwnedBusiness ) ;
           }
           if( lbMicroBusiness != null )
           {
               loCommonSupplier.setDottedFieldValue( "MicroBusiness", lbMicroBusiness ) ;
           }

        }
        catch ( Exception ex)
        {
           String lsMsg = ex.getMessage();
           Log.customer.debug( "CommonSupplierSWAMPostLoad error message =  " + lsMsg ) ;
        }
    }
}
