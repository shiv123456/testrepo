package config.java.ams.custom;

import java.util.Iterator;
import java.util.Stack;

import ariba.base.core.BaseObject;
import ariba.util.log.Log;

public class BuysenseDWHierarchyDetails
{

    private String         objecthierarchyname;
    private String         aribaobjname;
    private String         aribaobjtype;
    private String         dwoutputstr;
    private boolean        isvector;
    private boolean        isrecursive;
    private BaseObject     baseObj;
    private Stack<Integer> bracesStack;
    private String         msCN = "BuysenseDWHierarchyDetails";

    BuysenseDWHierarchyDetails()
    {
        isvector = false;
        isrecursive = false;
        bracesStack = new Stack();
    }

    public BaseObject getAribaBaseObject()
    {
        return baseObj;
    }

    public void setAribaBaseObject(BaseObject obj)
    {
        baseObj = obj;
    }

    public void pushBracesStack(int braceType)
    {
        bracesStack.push(new Integer(braceType));
    }

    public int peekBracesStack()
    {
        return bracesStack.peek().intValue();
    }

    public int popBracesStack()
    {
        return bracesStack.pop().intValue();
    }

    public void printBracesStack()
    {
        Log.customer.debug("*****%s***** braces stack =================================", msCN);
        Log.customer.debug("*****%s***** braces stack =================:" + objecthierarchyname, msCN);
        if (bracesStack == null)
        {
            Log.customer.debug("*****%s***** braces stack  is null:", msCN);
        }
        Iterator<Integer> it = bracesStack.iterator();

        while (it.hasNext())
        {
            Log.customer.debug("*****%s***** braces stack val:" + ((Integer) it.next()).toString(), msCN);
        }
        Log.customer.debug("*****%s***** end obj stack =================================", msCN);

    }

    public boolean isBracesEmpty()
    {
        return bracesStack.isEmpty();
    }

    public String getObjectHierarchyName()
    {
        return objecthierarchyname;
    }

    public String getAribaObjname()
    {
        return aribaobjname;
    }

    public String getAribaObjType()
    {
        return aribaobjtype;
    }

    public String getDWOutputStr()
    {
        return dwoutputstr;
    }

    public boolean getIsVector()
    {
        return isvector;
    }

    public boolean getIsRecursive()
    {
        return isrecursive;
    }

}
