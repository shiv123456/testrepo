package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.ClusterRoot;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseResetProposedProcurement extends Action
{
	private static String msCN = "BuysenseResetProposedProcurement";
	private static String msSubmitNote = "The following must be submitted and attached to this request: \n" +
                                "- Solicitation, including all addenda and award documents.\n" +
                                   "- Documentation of the solicitation advertisement in VBO or evidence that Virginia vendors received an award.\n" +
                                   "- A memorandum that addresses the following four points:\n" +
                                   "    1. Specifically how does the Scope of Work and pricing of this Cooperative Contract provide for the goods or services you seek to purchase? \n" +  
                                   "    2. Provide evidence the vendor is registered in eVA. \n" + 
                                   "    3. Explain why the use of this cooperative contract is the best option for the Commonwealth, including why the prices offered in the contract are considered fair and reasonable. \n" +
                                   "    4. Provide verification that no state contract exists to satisfy the requirement.";
	
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
    	Log.customer.debug("***%s***Called fire:: valuesource obj %s", msCN, valuesource);
    	Approvable loAppr = null;
        String lsProposedProcurement = null;
        
        if(valuesource!= null && valuesource instanceof Approvable)
        {
           loAppr = (Approvable)valuesource;
           lsProposedProcurement = (String) loAppr.getFieldValue("ProposedProcurement");
           setCustomTitle(loAppr, lsProposedProcurement);
           
           Boolean bCooperativeContract = (Boolean) loAppr.getDottedFieldValue("CooperativeContractCB");
           
           if(bCooperativeContract != null && bCooperativeContract)
           {
        	   Log.customer.debug("***%s***fire::Setting submit note %s", msCN, msSubmitNote);
        	   loAppr.setFieldValue("SubmitNote", msSubmitNote);
        	   loAppr.setFieldValue("ContactEx", "For Example: Contact (firstname lastname, phone, email)");
        	   loAppr.setFieldValue("OtherProcurement", null);
           }
           else
           {
        	   Log.customer.debug("***%s***fire::Re-Setting values to null", msCN);
        	   loAppr.setFieldValue("SubmitNote", null);
        	   loAppr.setFieldValue("ContactEx", null);
        	   loAppr.setFieldValue("Contact", null);
        	   loAppr.setFieldValue("Issuedby", null);
        	   loAppr.setFieldValue("CooperativeContract", null);
        	   loAppr.setFieldValue("OtherProcurement", null);
           }
        }
    }
    
    public void setCustomTitle(Approvable appr, String proposedProcurement)
    {
        ariba.user.core.User loRequesterUser = (ariba.user.core.User)appr.getRequester();
        if(loRequesterUser != null && proposedProcurement !=null)
        {
        	ariba.common.core.User loRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(loRequesterUser,appr.getPartition());
        	String lsClientName = (String)loRequesterPartitionUser.getDottedFieldValue("ClientName.ClientName");
        	if(lsClientName != null)
        	{
        		appr.setName(lsClientName+ " - " +proposedProcurement);
        	}
        }
    }
}
