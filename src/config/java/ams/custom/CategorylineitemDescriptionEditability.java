package config.java.ams.custom;

import ariba.base.core.BaseObject;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.procure.core.LineItemProductDescription;
import ariba.procure.core.ProcureLineItem;
import ariba.purchasing.core.ReqLineItem;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
/**
 * CSPL #: CSPL-1309
 * Author: Sarath Babu Garre
 * Date:   21/10/2009
 * Explanation: The Description field at edit line item is editable only for non-catalog line
 *              items and it will not be editable for catalog item or category line item, 
 *              So as the requirement is to make it editable for category line item, 
 *              we have add a custom condition "CategorylineitemDescriptionEditability" which 
 *              will check if the item being selected is category item or not and will return 
 *              true if the line item is category item and will return false for all other kind of line items. 
 */
public class CategorylineitemDescriptionEditability extends Condition
{
	private static final ValueInfo parameterInfo[];
	public boolean evaluate(Object value, PropertyTable params) throws ConditionEvaluationException 
	{
		Log.customer.debug("Inside CategorylineitemDescriptionEditability");
		BaseObject obj   = (BaseObject)params.getPropertyForKey("SourceObject");
		 if (obj.instanceOf("ariba.procure.core.LineItemProductDescription"))
		 {			 
			 ProcureLineItem lineItem =(ProcureLineItem)((LineItemProductDescription)obj).getLineItem();			 
			 if(lineItem instanceof ReqLineItem)
			 {
				 ReqLineItem reqLine = (ReqLineItem)lineItem;
				 if(reqLine!=null)
				 {
					 if(reqLine.getCategoryLineItemDetails()!=null)
					 {					 
						 return true;
					 }				 
				 }				 
			 }			 
		 }		 
		 return false;
	}	
    protected ValueInfo[] getParameterInfo()
    {
       return parameterInfo;
    }
    static
    {
       parameterInfo = (new ValueInfo[] {
          new ValueInfo("SourceObject", 0)
         });
    }

}
