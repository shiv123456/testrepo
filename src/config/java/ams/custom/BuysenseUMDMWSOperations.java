package config.java.ams.custom;

import java.util.List;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseUMDMWSOperations
{
    private static String sClassName = "BuysenseUMDMWSOperations";

    // This method return operation in ariba
    // customOperation is operation that <custom WS object>.Operation has. Example: BuysenseWSAddress.Operation
    public static String getAribaOperation(String customOperation)
    {
        if (customOperation == null) // null check
        {
            return "Error";
        }

        if (StringUtil.equalsIgnoreCase(customOperation.trim(), "ADD"))
        {
            return "Create";
        }
        if (StringUtil.equalsIgnoreCase(customOperation.trim(), "UPDATE"))
        {
            return "Update";
        }
        if (StringUtil.equalsIgnoreCase(customOperation.trim(), "UPDATE"))
        {
            return "Delete";
        }
        return "Error";
    }

    public static boolean isAddOperation(String customOperation)
    {
        if (customOperation != null && StringUtil.equalsIgnoreCase(customOperation.trim(), "ADD"))
        {
            return true;
        }
        return false;
    }

    public static boolean isDeleteOperation(String customOperation)
    {
        if (customOperation != null && StringUtil.equalsIgnoreCase(customOperation.trim(), "DELETE"))
        {
            return true;
        }
        return false;
    }

    public static boolean isUpdateOperation(String customOperation)
    {
        if (customOperation != null && StringUtil.equalsIgnoreCase(customOperation.trim(), "UPDATE"))
        {
            return true;
        }
        return false;
    }

    public static boolean isValidCustomOperation(String customOperation)
    {
        // is it a valid custom operation allowed in web service call. This isn't specific to any particular web service
        if (isAddOperation(customOperation) || isDeleteOperation(customOperation) || isUpdateOperation(customOperation))
        {
            return true;
        }

        return false;
    }

    // check if it's valid operation for a particular service
    @SuppressWarnings("rawtypes")
    public static boolean isValidCustomOperationForService(List lCustomOperations, String sCustomOperation)
    {

        if (lCustomOperations == null || lCustomOperations == null || lCustomOperations.isEmpty())
        {
            Log.customer.debug(sClassName + " custom operation list is blank or operation in WS is null");
            return false;
        }

        for (int i = 0; i < lCustomOperations.size(); i++)
        {
            // If condition matched, return true
            if (StringUtil.equalsIgnoreCase(((String) lCustomOperations.get(i)).trim(), sCustomOperation.trim()))
            {
                return true;
            }
        }
        return false;
    }

}
