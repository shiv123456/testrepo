/**
*/
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */
/* Anup - 09/20/2004 - ST SPL 172 - Replaced method add() by addAll() */


package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.app.util.Attachment;
import ariba.approvable.core.AttachmentWrapper;
import ariba.approvable.core.Comment;
import ariba.util.core.Date;
import java.util.List;
import ariba.util.log.Log;
// Ariba 8.1: Need ListUtil
import ariba.util.core.ListUtil;

//81->822 changed Vector to List
//81->822 changed Hashtable to Map
public class BuysenseAttachmentFetcher
{
   public final static String STATUS_NEW   = "NEW";
   public final static String STATUS_ERROR = "ERR";
   public final static String STATUS_DONE  = "DONE";
   private Approvable foApprovable = null;
   // Ariba 8.1: List constructor is deprecated by ListUtil.newVector()
   private List vAttachmentCommentArrays = ListUtil.list();
   public BuysenseAttachmentFetcher()
   {
      Log.customer.debug("BuysenseAttachmetFetcher Constructor called");
   }

   public List processAttachments(Approvable inApprovable)
   {
      foApprovable = inApprovable;

      if (foundAttachments())
      {
         openConnection();
         pushAttachments();
         closeConnection();
         emailMethods();
      }
      return vAttachmentCommentArrays;
   }

   public boolean foundAttachments()
   {
      // Ariba 8.1: List constructor is deprecated by ListUtil.newVector()
      List lvComments          = ListUtil.list();
      Comment loComment          = null;
      boolean lbFoundAttachments = false;
      // Ariba 8.1: List constructor is deprecated by ListUtil.newVector()
      List lvAttLevel = ListUtil.list();
      String lsLineID = null;
      String lsATTID = null;
      String lsTemp = null;
      boolean lbExistingLevel = false;
      int liAttNum = 0;
      int liExistingLinePos = 0;
      lvComments = foApprovable.getComments();
      List lvAttachemntWraper = (List) foApprovable.getFieldValue("Attachments");
      if ( ! lvComments.isEmpty() )
      {
         Log.customer.debug("Comments total: " + lvComments.size());
         for (int i = 0; i < lvComments.size(); i++)
         {
            // Ariba 8.1: List::elementAt() is deprecated by List::get()
            loComment = (Comment)lvComments.get(i);
            List lvCommentAttachments = ListUtil.list();
            // Ariba 8.1: List::addElement() is deprecated by List::add()
            lvCommentAttachments.addAll(loComment.getAttachments());

            if ( ! lvCommentAttachments.isEmpty() )
            {
               Log.customer.debug("Attachment was found on Comment # " + i);
               if (loComment.getDottedFieldValue("LineItem.NumberInCollection") == null)
               {
                  lsLineID = "0";
                  Log.customer.debug("Header level att.");
               }
               else
               {
                  lsLineID = loComment.getDottedFieldValue("LineItem.NumberInCollection") + "";
                  Log.customer.debug("Line level att. " + lsLineID);
               }

               lbExistingLevel = false;
               // Have we already dealt with attachments for Line ID under process
               for (int k=0; k < lvAttLevel.size(); k = k + 2)
               {
                  // Ariba 8.1: List::elementAt() is deprecated by List::get()
                  lsTemp = (String)lvAttLevel.get(k);
                  if (lsLineID.equals(lsTemp))
                  {
                     // Ariba 8.1: List::elementAt() is deprecated by List::get()
                     lsATTID = (String)lvAttLevel.get(k+1);
                     lbExistingLevel = true;
                     liExistingLinePos = k;
                     break;
                  }
               }
               for (int j = 0; j < lvCommentAttachments.size(); j++)
               {
                  Log.customer.debug("Populate the attachmentCommentArray");
                  Object[] attachmentCommentArray = new Object[9];

                  if(lbExistingLevel)
                  {
                     try
                     {
                        liAttNum = Integer.parseInt(lsATTID) + j + 1;
                        lsATTID = liAttNum + "";
                     } 
                     catch (NumberFormatException loNFE)
                     {
                        //We should never get here.
                        Log.customer.debug("Error while parsing attachment #. " + loNFE.getMessage());
                     }
                  }
                  else
                  {
                     lsATTID = j + "";
                  }
                  attachmentCommentArray[0] = lsLineID;
                  attachmentCommentArray[1] = lsATTID;
                  attachmentCommentArray[2] = "NEW";
                  attachmentCommentArray[3] = (String) loComment.getDottedFieldValue("Parent.UniqueName");
                  attachmentCommentArray[4] = (Date)   loComment.getDottedFieldValue("Date");
                  attachmentCommentArray[5] = (String) loComment.getDottedFieldValue("User.UniqueName");
                  // Ariba 8.1: List::elementAt() is deprecated by List::get()
                  attachmentCommentArray[6] = (String) ((Attachment) lvCommentAttachments.get(j)).getDottedFieldValue("StoredFilename");
                  // Ariba 8.1: List::elementAt() is deprecated by List::get()
                  attachmentCommentArray[7] = (String) ((Attachment) lvCommentAttachments.get(j)).getDottedFieldValue("Filename");
                  boolean lboolPandC = false;
                  Object loPandC = loComment.getDottedFieldValue("ProprietaryAndConfidential");
                  if (loPandC != null) 
                  {
                     Log.customer.debug("PAndC flag on Comment contains \"" + loPandC.toString() + "\"");
                     lboolPandC = ((Boolean)loPandC).booleanValue(); 
                  }
                  if (lboolPandC)
                  { 
                     Log.customer.debug("PandC being set to true ... ");
                     attachmentCommentArray[8] = new Integer(1);
                  }
                  else
                  {
                     Log.customer.debug("PandC being set to false ... ");
                     attachmentCommentArray[8] = new Integer(0);
                  }
                  Log.customer.debug("Content of Array: " + attachmentCommentArray.toString());
                  // Ariba 8.1: List::addElement() is deprecated by List::add()
                  vAttachmentCommentArrays.add(attachmentCommentArray);
                  if ( (j + 1) == lvCommentAttachments.size())
                  {
                     // We are at the end of vector, store Line ID and Last Attachment #
                     if (lbExistingLevel)
                     {
                	//jackie - need to ask Ariba consultant or in house ariba experts. is this okay?
                	// 8.1 -> 822
                        // lvAttLevel.replaceElementAt(liExistingLinePos + 1, lsATTID);
                	 ListUtil.replace(lvAttLevel,lvAttLevel.get(liExistingLinePos + 1),lsATTID);                        
                     }
                     else
                     {
                        // Ariba 8.1: List::addElement() is deprecated by List::add()
                        lvAttLevel.add(lsLineID);
                        lvAttLevel.add(lsATTID);
                     }

                  }
               }// end for loop lvCommentAttachments
            } // end if (lvCommentAttachments.isEmpty())
         } // end for loop for Comments
      }//end if!lvComments.isEmpty()
      if(! lvAttachemntWraper.isEmpty())
      {
          Log.customer.debug("Total Attachemnts: " + lvAttachemntWraper.size());        
          for (int p = 0; p < lvAttachemntWraper.size(); p++)
          {              
              AttachmentWrapper loAttachmentWraper = (AttachmentWrapper)lvAttachemntWraper.get(p);              
              if (loAttachmentWraper.getDottedFieldValue("FirstLineItem.NumberInCollection") == null)
              {
                  lsLineID = "0";
                  Log.customer.debug("Header level att.");
              }
              else
              {
                  lsLineID = loAttachmentWraper.getDottedFieldValue("FirstLineItem.NumberInCollection") + "";
                  Log.customer.debug("Line level att. " + lsLineID);
              }
              lbExistingLevel = false;             
              for (int k=0; k < lvAttLevel.size(); k = k + 2)
              {                  
                  lsTemp = (String)lvAttLevel.get(k);
                  if (lsLineID.equals(lsTemp))
                  {                      
                      lsATTID = (String)lvAttLevel.get(k+1);
                      lbExistingLevel = true;
                      //liExistingLinePos = k;
                      break;
                  }
              }
              Log.customer.debug("Populate the attachmentCommentArray");
              Object[] attachmentCommentArray = new Object[9];
              Log.customer.debug("After attachmentCommentArray creation");
              if(lbExistingLevel)
              {
                  Log.customer.debug("Inside lbExistingLevel");
                  try
                  {
                      liAttNum = Integer.parseInt(lsATTID) + 1;
                      lsATTID = liAttNum + "";
                      Log.customer.debug("exiting lbExistingLevel");
                  } 
                  catch (NumberFormatException loNFE)
                  {                     
                      Log.customer.debug("Error while parsing attachment #. " + loNFE.getMessage());
                  }
              }
              else
              {
                  lsATTID = 0 + "";
              }
              Log.customer.debug("After lbExistingLevel");
              attachmentCommentArray[0] = lsLineID;
              attachmentCommentArray[1] = lsATTID;
              attachmentCommentArray[2] = "NEW";
              attachmentCommentArray[3] = (String) loAttachmentWraper.getDottedFieldValue("LineItemCollection.UniqueName");
              attachmentCommentArray[4] = (Date)   loAttachmentWraper.getDottedFieldValue("Date");
              attachmentCommentArray[5] = (String) loAttachmentWraper.getDottedFieldValue("User.UniqueName");            
              attachmentCommentArray[6] = (String) ((Attachment) loAttachmentWraper.getAttachments().get(0)).getFieldValue("StoredFilename");
              attachmentCommentArray[7] = (String) ((Attachment) loAttachmentWraper.getAttachments().get(0)).getFieldValue("Filename");
              boolean lboolPandC = false;
              Object loPandC = loAttachmentWraper.getDottedFieldValue("ProprietaryAndConfidential");
              if (loPandC != null) 
              {
                  Log.customer.debug("PAndC flag on Comment contains \"" + loPandC.toString() + "\"");
                  lboolPandC = ((Boolean)loPandC).booleanValue(); 
              }
              if (lboolPandC)
              { 
                  Log.customer.debug("PandC being set to true ... ");
                  attachmentCommentArray[8] = new Integer(1);
              }
              else
              {
                  Log.customer.debug("PandC being set to false ... ");
                  attachmentCommentArray[8] = new Integer(0);
              }
              Log.customer.debug("Content of Array: " + attachmentCommentArray.toString());              
              vAttachmentCommentArrays.add(attachmentCommentArray);              
              /*if (lbExistingLevel)
              {                  
                  ListUtil.replace(lvAttLevel,lvAttLevel.get(liExistingLinePos + 1),lsATTID);                        
              }
              else
              {*/                 
              lvAttLevel.clear();
                  lvAttLevel.add(lsLineID);
                  lvAttLevel.add(lsATTID);
              //}
          }//end for looplvAttachemntWraper.size
      }// end if !lvAttachemntWraper.isEmpty()        
         if ( ! vAttachmentCommentArrays.isEmpty() )
         {
            lbFoundAttachments = true;
            return lbFoundAttachments;
         }
         else
         {
            Log.customer.debug("No attachments were found on Approvable");
            return lbFoundAttachments;
         }
      }
   
   public void openConnection()
   {
      Log.customer.debug("openConnection called");
   }   public void pushAttachments()
   {
      Log.customer.debug("pushAttachments called");
   }   public void closeConnection()
   {
      Log.customer.debug("closeConnection called");
   }   public static void emailMethods()
   {
      Log.customer.debug("emailMethods called");
   }
}
