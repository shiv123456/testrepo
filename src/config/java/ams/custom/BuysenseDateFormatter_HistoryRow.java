package config.java.ams.custom;

 import ariba.base.core.Base;
 import ariba.util.core.Date;
 import ariba.util.core.Fmt;
 import ariba.util.formatter.DateFormatter;
 import ariba.util.log.Log;
 import java.util.Locale;
 import java.util.TimeZone;
 
 
public class BuysenseDateFormatter_HistoryRow extends DateFormatter{
	   protected String formatObject(Object object, Locale locale)
	   {
		     TimeZone timezone = Base.getSession().getTimezone();
	    	 String lsLongDate = toFullDayDateMonthYearString((Date)object,locale);
	    	 int liFirst = lsLongDate.indexOf("at") + 2;
	    	 int liSecond = lsLongDate.lastIndexOf("M") + 1;
	    	 String lsTime = lsLongDate.substring(liFirst, liSecond);
	    	 String lsPreciseDate = toDayDateMonthYearString ((Date)object,locale,timezone);
	    	 lsPreciseDate = lsPreciseDate + ", " + lsTime; 
	    	 
	    	 Log.customer.debug("Date format is Prod_Like:" +lsPreciseDate.toString());
	    	 
 	    	 return Fmt.S("%s", lsPreciseDate.toString());
	   }

}
