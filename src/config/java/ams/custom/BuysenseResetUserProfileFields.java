package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.BaseVector;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseResetUserProfileFields extends Action
{
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
    	Log.customer.debug("***%s***Called fire BuysenseResetUserProfileFields:: valuesource obj %s", valuesource);
    	Approvable loAppr = null;
        Boolean lbeMall = null;
        Boolean lbOther = null;
        Boolean lbLogiXML = null;
        Boolean lbQuickQuote = null;
        Boolean lbVBOBuyer = null;
        Boolean lbElectronicEforms = null;
        if(valuesource!= null && valuesource instanceof Approvable)
        {
           loAppr = (Approvable)valuesource;
           lbeMall = (Boolean) loAppr.getFieldValue("eMall");
           lbOther = (Boolean) loAppr.getFieldValue("Other");
           lbLogiXML = (Boolean) loAppr.getFieldValue("LogiXML");
           lbQuickQuote = (Boolean) loAppr.getFieldValue("QuickQuote");
           lbVBOBuyer = (Boolean) loAppr.getFieldValue("VBOBuyer");
           lbElectronicEforms = (Boolean) loAppr.getFieldValue("ElectronicForms");
           String lsRadioButton = (String) loAppr.getDottedFieldValue("RadioButtons");
           if(lbeMall != null && lbeMall == false)
           {
        	   loAppr.setFieldValue("AribaBuysenseOrg", null);
        	   loAppr.setFieldValue("AribaCatalogController", null);
        	   loAppr.setFieldValue("AribaDeliverToName", null);
        	   loAppr.setFieldValue("AribaEmployeeNum", null);
        	   loAppr.setFieldValue("AribaExpenditureLimitAmt", null);
        	   loAppr.setFieldValue("AribaExpenditureLimitType", null);
        	   loAppr.setFieldValue("AribaExpenditureLimitApprover", null);
        	   BaseVector bvAribaStdRoles = (BaseVector)loAppr.getFieldValue("AribaStandardRoles");
   			   bvAribaStdRoles.clear();
        	   BaseVector bvAribaAddRoles = (BaseVector)loAppr.getFieldValue("AribaAdditionalRoles");
        	   bvAribaAddRoles.clear();        	   
        	   BaseVector bvAribaAddAgencyRoles = (BaseVector)loAppr.getFieldValue("AribaAdditionalAgencyRoles");
        	   bvAribaAddAgencyRoles.clear();
        	   loAppr.setFieldValue("AribaShipToAddress", null);
               loAppr.setFieldValue("AribaBillToAddress", null);
        	   loAppr.setFieldValue("AribaSupervisor", null);
           }
           if(lsRadioButton == null || lsRadioButton.equalsIgnoreCase("New"))
           {
               
               loAppr.setFieldValue("CustodialCare", null);
               loAppr.setFieldValue("Custodian", null);
           }
           if(lbElectronicEforms != null && lbElectronicEforms == false)
           {
               BaseVector bveFormProfiles = (BaseVector)loAppr.getFieldValue("eFormProfiles");
               bveFormProfiles.clear();               
           }
           if(lbLogiXML != null && lbLogiXML == false)
           {
        	   loAppr.setFieldValue("LogiEntityAccess", null);
        	   loAppr.setFieldValue("LogiReportThresholdLimit", null);
        	   BaseVector bvLogiStdReportNeeds = (BaseVector)loAppr.getFieldValue("LogiStandardReportNeeds");
        	   bvLogiStdReportNeeds.clear();
        	   BaseVector bvLogiAdvReportNeeds = (BaseVector)loAppr.getFieldValue("LogiAdvancedReportNeeds");
        	   bvLogiAdvReportNeeds.clear();        	   
           } 
           if(lbQuickQuote != null && lbQuickQuote == false)
           {
        	   BaseVector bvQQRoles = (BaseVector)loAppr.getFieldValue("QQRoles");
        	   bvQQRoles.clear();
        	   BaseVector bvQQAgencyBSOAccess = (BaseVector)loAppr.getFieldValue("QQAgencyBSOAccess");
        	   bvQQAgencyBSOAccess.clear();
        	   loAppr.setFieldValue("QQReverseAuctionAccess", null);
           } 
           if(lbVBOBuyer != null && lbVBOBuyer == false)
           {
        	   loAppr.setFieldValue("VBOUnit", null);
        	   loAppr.setFieldValue("VBOFaxNumber", null);
           }   
           if(lbOther != null && lbOther == false)
           {
        	  // loAppr.setFieldValue("OtherDataManagement", null);
        	   //loAppr.setFieldValue("OtherUserManagement", null);
        	   loAppr.setFieldValue("OthereProcurementVBO", null);
        	   BaseVector bvAdvancedVBORoles = (BaseVector)loAppr.getFieldValue("AdvancedVBORoles");
        	   bvAdvancedVBORoles.clear();
        	   loAppr.setFieldValue("OtherVSSAdminSetup", null);
           }            
        }
    }

}
