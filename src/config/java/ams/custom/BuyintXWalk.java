/* 
 * @(#)BuyintXWalk.java     1.0 06/26/2008
 *
 * Copyright 2008 by CGI 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of CGI ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with CGI 
 */

package config.java.ams.custom ;

import ariba.util.log.*; 
import java.util.*;

/**
 * @version 1.0
 * ABR CSPLU-34 
 * Buyer 8.2.2 involved package renames for some packages that are XML attributes in transactions
 * imported from external systems.  Created this class to store mappings to avoid changes to external systems. 
 */

/**
 * CSPL:2721 - 9r1 - Order import Attachments not working
 * 
 * @author Pavan Aluri
 * @Date 15-Mar-2011
 * @version 1.1
 * @Explanation Changed attachment class name to mathc with External system so as to get rid of exception 
 * "Error: exception while creating attachment: No BaseMeta for ariba.common.core.Attachment" while importing a Req with attachment.
 */
public class BuyintXWalk
{
    private static final int miHashSize = 10; 
    public static Hashtable moOldToNew = new Hashtable(miHashSize); 
    public static Hashtable moNewToOld = new Hashtable(miHashSize); 
     

    static
    {
        Log.customer.debug("in static initializer for BuyintXWalk");

        // Populate Old to New mappings
        moOldToNew.put( "ariba.procure.core.Requisition",   "ariba.purchasing.core.Requisition");
        moOldToNew.put( "ariba.procure.core.ReqLineItem",   "ariba.purchasing.core.ReqLineItem");
        moOldToNew.put( "ariba.procure.core.ERPOrder",      "ariba.purchasing.core.ERPOrder");
        moOldToNew.put( "ariba.procure.core.PCardOrder" ,   "ariba.pcard.core.PCardOrder");
        moOldToNew.put( "ariba.common.core.CommodityCode" , "ariba.common.core.PartitionedCommodityCode");
        moOldToNew.put( "ariba.common.core.UnitOfMeasure",  "ariba.basic.core.UnitOfMeasure");
        moOldToNew.put( "ariba.common.core.Comment",        "ariba.approvable.core.Comment");
        moOldToNew.put( "ariba.common.core.Attachment",     "ariba.app.util.Attachment");

        // Populate New to Old mappings
        moNewToOld.put( "ariba.purchasing.core.Requisition",          "ariba.procure.core.Requisition");
        moNewToOld.put( "ariba.purchasing.core.ReqLineItem",          "ariba.procure.core.ReqLineItem");
        moNewToOld.put( "ariba.purchasing.core.ERPOrder",             "ariba.procure.core.ERPOrder");
        moNewToOld.put( "ariba.pcard.core.PCardOrder",           "ariba.procure.core.PCardOrder"); 
        moNewToOld.put( "ariba.common.core.PartitionedCommodityCode", "ariba.common.core.CommodityCode");
        moNewToOld.put( "ariba.basic.core.UnitOfMeasure",             "ariba.common.core.UnitOfMeasure");
        moNewToOld.put( "ariba.approvable.core.Comment",              "ariba.common.core.Comment");
        moNewToOld.put( "ariba.app.util.Attachment",           "ariba.common.core.Attachment");
    }

    public static class XWalk
    {
        public static String changeOldToNew(String fsFromVal)
        {
            String lsToVal = (String)moOldToNew.get(fsFromVal); 

            if ( lsToVal == null )
            {
                lsToVal = fsFromVal; 
            }

            return lsToVal;
        }

        public static String changeNewToOld(String fsFromVal)
        {
            String lsToVal = (String)moNewToOld.get(fsFromVal); 

            if ( lsToVal == null )
            {
                lsToVal = fsFromVal; 
            }

            return lsToVal;
        }
    }
}
