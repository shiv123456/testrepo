// Version 1.23

/*
    Copyright (c) 1996-1999 Ariba/Buysense, Inc.
    Sunil Aug 2000
    ---------------------------------------------------------------------------------------------------

    Dev SPL # 198 - Enhanced discount percent edit error message. Deleted 'discount' for accounting line message.
                    Also percentage values recognize decimal values.

    Dev SPL 232 - Removed superfluous code. Added line # to Quantity Edit.

    Release 1.5: UAT SPL #15 Pay In Process updating incorrectly. RLee

    Release 1.5 UAT SPL # 16: PV Line Numbers out of order. RLee 2/27/02

    eVA Dev SPL # 4 - Get #Accepted from NumberAccepted instead of BuysenseNumberAccepted on poline.
*/

/* 09/02/2003: Updates for Ariba 8.0 (David Chamberlain)
   Changed all Util.vector() to ListUtil.vector
   Changed all Util.getInteger() to Constants.getInteger()  */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/

package config.java.ams.custom;
// Ariba 8.0: Replaced deprecated ApprovableHook
import ariba.approvable.core.ApprovableHook;
//import ariba.approvable.core.ApprovableHook;
//import ariba.util.core.Util;
import java.util.List;
import ariba.approvable.core.Approvable;
import ariba.base.core.BaseVector;
import ariba.purchasing.core.POLineItem;
import ariba.base.core.BaseObject;
import java.math.BigDecimal;
import ariba.base.core.BaseId;
import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.util.log.Log;
import ariba.base.core.BaseSession;
// Ariba 8.0: Deprecations, methods have changed packages.
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;


public class AMSPaymentSubmitHook implements ApprovableHook
{
    // Ariba 8.0: Replaced deprecated method
    // 81->822 changed Vector to List
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));

    public List run (Approvable approvable)
    {
        Log.customer.debug("In the payment Submit Hook");

        //Call CalculateTotals and PaymentSummary
        CalculateTotals ct = new CalculateTotals();
        ct.fire(approvable,null);
        PaymentSummary ps =  new PaymentSummary();
        ps.fire(approvable,null);

        //Anup - Perform ALL Buysense edits.
        List retVector = performBuysenseEdits(approvable);
        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
        int errorCode = ((Integer)retVector.get(0)).intValue();
        Log.customer.debug("Validations in " + this.getClass().getName() + ", error Code --> %s",errorCode);
        if(errorCode<0)
        {
            Log.customer.debug("Validations Failed in " + this.getClass().getName() + " - returning error to UI");
            return retVector;
        }

        approvable.save();

        return NoErrorResult;
    }

    //Anup - Method to perform ALL edits. Baseline edits are performed in this method.
    //Generic Client/Agency level edits are invoked from this method.
    private List performBuysenseEdits(Approvable approvable)
    {
        // Withdraw/Submit instead of Resubmit - ST SPL # 427
        String status = (String)approvable.getFieldValue("StatusString");
        // Ariba 8.0: Replaced deprecated method
        if (status.equals("Denied"))
            return ListUtil.list(Constants.getInteger(-1),
                                     "Please 'Withdraw' this payment and then 'Submit'.");

        // Supplier/SupplierLocation validation - ST SPL # 324
        ClusterRoot supplierObject = (ClusterRoot)approvable.getFieldValue("Supplier");
        ClusterRoot supplierLocationObject = (ClusterRoot)approvable.getFieldValue("SupplierLocation");
        // Ariba 8.0: Replaced deprecated method
        if ((supplierObject == null) || (supplierLocationObject == null))
            return ListUtil.list(Constants.getInteger(-1),
                                     "Cannot Submit Payment because the Supplier/Address is not valid.");


        String supplierID1 = (String)approvable.getDottedFieldValue("Supplier.SupplierIDValue");
        String supplierID2 = (String)approvable.getDottedFieldValue("SupplierLocation.Supplier.SupplierIDValue");
        // Ariba 8.0: Replaced deprecated method
        if (!supplierID1.equals(supplierID2))
            return ListUtil.list(Constants.getInteger(-1),
                                     "Cannot Submit Payment because the Supplier/Address are not related.");

        // Lock the order that is being paid
        Log.customer.debug("Lock the order whose baseid is %s",
                       approvable.getFieldValue("Order"));
        ClusterRoot order=(ClusterRoot)approvable.getFieldValue("Order");
        BaseId refBaseId = (BaseId)order.getBaseId();
        BaseSession baseSession = Base.getSession();
        order = baseSession.objectForWrite(refBaseId);
        Log.customer.debug("Finished locking the order");

        // Get the Line items vector on the locked order and on the payment
        BaseVector polines = (BaseVector)order.getFieldValue("LineItems");
        BaseVector pvlines = (BaseVector)approvable.getFieldValue("PaymentItems");
        Log.customer.debug("After getting the order and payment lines");

        // ButsenseOrg/Client Name validations - Enhancement Request #111
        boolean activeFlag;

        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        //ClusterRoot bso = (ClusterRoot)approvable.getDottedFieldValue("Requester.BuysenseOrg");
        ariba.user.core.User RequesterUser = (ariba.user.core.User)approvable.getDottedFieldValue("Requester");
        //rgiesen dev SPL #39
        //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,approvable.getPartition());
        ClusterRoot bso = (ClusterRoot)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg");

        // Ariba 8.0: Replaced deprecated method
        if (bso == null)
            return ListUtil.list(Constants.getInteger(-1),
                                     "BuysenseOrg for a payment must not be blank.");

        activeFlag = bso.getActive();
        // Ariba 8.0: Replaced deprecated method
        if (!activeFlag)
            return ListUtil.list(Constants.getInteger(-1),
                                     "BuysenseOrg for a payment must be valid.");

        String clientUniqueName1 = (String)bso.getDottedFieldValue("ClientName.UniqueName");
        // Ariba 8.0: Replaced deprecated method
        if (clientUniqueName1 == null)
            return ListUtil.list(Constants.getInteger(-1),
                                     "ClientName/UniqueName for a BuysenseOrg must not be blank.");

        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic ClientName field value.
        ClusterRoot clientName = (ClusterRoot)RequesterPartitionUser.getDottedFieldValue("ClientName");
        // Ariba 8.0: Replaced deprecated method
        if (clientName == null)
            return ListUtil.list(Constants.getInteger(-1),
                                     "ClientName for a payment must not be blank.");

        activeFlag = clientName.getActive();
        // Ariba 8.0: Replaced deprecated method
        if (!activeFlag)
            return ListUtil.list(Constants.getInteger(-1),
                                     "ClientName for a payment must be valid.");

        String clientUniqueName2 = (String)clientName.getFieldValue("UniqueName");
        // Ariba 8.0: Replaced deprecated method
        if (clientUniqueName2 == null)
            return ListUtil.list(Constants.getInteger(-1),
                                     "ClientName/UniqueName for a payment must not be blank.");

        // Ariba 8.0: Replaced deprecated method
        if (!clientUniqueName1.equals(clientUniqueName2))
            return ListUtil.list(Constants.getInteger(-1),
                                     "BuysenseOrg/Client Name objects for the requester do not match.");


        // Current Total Paying Quantity cannot be zero/negative - ST SPL # 208
        BigDecimal pmtQuantity = new BigDecimal(0);
        BigDecimal pmtQuantityTotal = new BigDecimal(0);
        for(int i=0;i<pvlines.size();i++)
        {
            // Check if payment line numbers and order line numbers are not crossed. RLee
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            BaseObject paymentItem = (BaseObject)pvlines.get(i);
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            BaseObject OrderItem = (BaseObject)polines.get(i);
            int PaymentLiNum = Integer.parseInt(((String) paymentItem.getFieldValue("LineNumber")).trim());
            int OrderLiNum = ((Integer)(OrderItem.getFieldValue("NumberInCollection"))).intValue();
            if ( !((PaymentLiNum - 1) == i) || !(OrderLiNum  == PaymentLiNum))
            {
                Log.customer.debug("Inside if payment line number is messed up.");
                // Ariba 8.0: Replaced deprecated method
                return ListUtil.list(Constants.getInteger(-1),
                                         "Error encountered in payment line sequence. Please delete this PV and create a new one. To delete, select Home and then Delete. If problem persist, please contact your System Administrator.");
            }


            pmtQuantity = (BigDecimal)(paymentItem.getFieldValue("CurPayingQuantity"));
            pmtQuantityTotal = pmtQuantityTotal.add(pmtQuantity);
        }

        // Ariba 8.0: Replaced deprecated method
        if (pmtQuantityTotal.doubleValue() <= 0.0)
            return ListUtil.list(Constants.getInteger(-1),
                                     "Total payment quantity cannot be zero/negative.");

        // Discount Percent and Accounting Line percentage cannot be greater than 100 - ST SPL #243
        BigDecimal hdrDiscountPercent = (BigDecimal)approvable.getFieldValue("DiscountPercent");
        // Ariba 8.0: Replaced deprecated method
        if(hdrDiscountPercent.compareTo(new BigDecimal(100)) > 0)
            return ListUtil.list(Constants.getInteger(-1),
                                     "Discount Percent cannot be greater than 100.");

        for(int i=0;i<pvlines.size();i++)
        {
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            BaseObject paymentItem = (BaseObject)pvlines.get(i);
            List paymentAcctgs = (List)paymentItem.getDottedFieldValue("Accountings.PaymentAccountings");

            for(int j=0;j<paymentAcctgs.size();j++)
            {
                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                BaseObject paymentAcctg = (BaseObject)paymentAcctgs.get(j);
                BigDecimal acctPercentage = (BigDecimal)paymentAcctg.getFieldValue("PaymentAmountPercent");
                // Ariba 8.0: Replaced deprecated method
                if (acctPercentage.compareTo(new BigDecimal(100)) > 0)
                    return ListUtil.list(Constants.getInteger(-1),
                                             "Payment Line # " + (i+1) +
                                             " : Accounting Line # " + (j+1) + " Percent cannot be greater than 100.");
            }
        }

        //Now perform Client/Agency edits.
        //Anup - Generic invocation of dynamically determined Client/Agency edits. - Dev SPL #173
        List result = EditClassGenerator.invokeHookEdits(approvable,
                                                           this.getClass().getName());
        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
        int errorCode = ((Integer)result.get(0)).intValue();
        Log.customer.debug("Validations in " + this.getClass().getName() + ", error Code --> %s",errorCode);
        if(errorCode<0)
        {
            Log.customer.debug("Validations Failed in " + this.getClass().getName() + " - returning error to UI");
            return result;
        }

        /* UAT SPL #15: moved to bottom
        // SPL # 83 - added following loop to update NumberInPaymentProcess,
            // NumberPaid on every payment line from the order line - Sunil.
        for(int i=0;i<polines.size();i++)
            {
                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                POLineItem item = (POLineItem)polines.get(i);
                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                BaseObject paymentItem = (BaseObject)pvlines.get(i);
                BigDecimal payingQty = (BigDecimal)paymentItem.getFieldValue("CurPayingQuantity");
                BigDecimal inPorcessQty = (BigDecimal)item.getFieldValue("NumberInPaymentProcess");
                BigDecimal numberPaid = (BigDecimal)item.getFieldValue("NumberPaid");
                BigDecimal numberAccepted = (BigDecimal)item.getFieldValue("NumberAccepted");
                //BigDecimal numberAccepted = (BigDecimal)item.getFieldValue("BuysenseNumberAccepted");
                paymentItem.setFieldValue("NumberInPaymentProcess",inPorcessQty);
                paymentItem.setFieldValue("NumberPaid",numberPaid);
                paymentItem.setFieldValue("NumberAccepted",numberAccepted);
            }
            */

        //Quantity Edit
        /*for(int i=0;i<polines.size();i++)
            {
                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                POLineItem item = (POLineItem)polines.get(i);
                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                BaseObject paymentItem = (BaseObject)pvlines.get(i);
                BigDecimal payingQty = (BigDecimal)paymentItem.getFieldValue("CurPayingQuantity");
                BigDecimal inPorcessQty = (BigDecimal)item.getFieldValue("NumberInPaymentProcess");
                BigDecimal numberPaid = (BigDecimal)item.getFieldValue("NumberPaid");
                BigDecimal numberAccepted = (BigDecimal)item.getFieldValue("NumberAccepted");
                //BigDecimal numberAccepted = (BigDecimal)item.getFieldValue("BuysenseNumberAccepted");
                if (numberAccepted == null)
                    numberAccepted = new BigDecimal(0);

                // Ariba 8.0: Replaced deprecated method
                if(payingQty.compareTo(numberAccepted.subtract(numberPaid).subtract(inPorcessQty))==1)
                {
                    return ListUtil.vector(Constants.getInteger(-1), "Cannot Submit Payment because paying quantity is too high");
                }
            }
            */
        // For every payment line update the corresponding order line field --> "NumberInPaymentProcess"

        for(int i=0;i<polines.size();i++)
        {
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            POLineItem item = (POLineItem)polines.get(i);
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            BaseObject paymentItem = (BaseObject)pvlines.get(i);
            BigDecimal payingQty = (BigDecimal)paymentItem.getFieldValue("CurPayingQuantity");
            BigDecimal inPorcessQty = (BigDecimal)item.getFieldValue("NumberInPaymentProcess");
            BigDecimal numberPaid = (BigDecimal)item.getFieldValue("NumberPaid");
            BigDecimal numberAccepted = (BigDecimal)item.getFieldValue("NumberAccepted");
            //BigDecimal numberAccepted = (BigDecimal)item.getFieldValue("BuysenseNumberAccepted");
            if (numberAccepted == null)
                numberAccepted = new BigDecimal(0);

            // Ariba 8.0: Replaced deprecated method
            if(payingQty.compareTo(numberAccepted.subtract(numberPaid).subtract(inPorcessQty))==1)
            {
                return ListUtil.list(Constants.getInteger(-1),
                                         "Line # : " + (i+1) +
                                         " - Cannot Submit Payment because paying quantity is too high");
            }

            /* UAT SPL 15: moved. adding too early to item.NumberInPaymenProcess. RLee
    inPorcessQty=inPorcessQty.add(payingQty);
    Log.customer.debug("The new Inprocess qty: %s",inPorcessQty);
    item.setFieldValue("NumberInPaymentProcess",inPorcessQty);
    */

        }

        // UAT SPL #15: moved here from above
        // SPL # 83 - added following loop to update NumberInPaymentProcess,
        // NumberPaid on every payment line from the order line - Sunil.
        for(int i=0;i<polines.size();i++)
        {
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            POLineItem item = (POLineItem)polines.get(i);
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            BaseObject paymentItem = (BaseObject)pvlines.get(i);
            BigDecimal payingQty = (BigDecimal)paymentItem.getFieldValue("CurPayingQuantity");
            BigDecimal inPorcessQty = (BigDecimal)item.getFieldValue("NumberInPaymentProcess");
            BigDecimal numberPaid = (BigDecimal)item.getFieldValue("NumberPaid");
            BigDecimal numberAccepted = (BigDecimal)item.getFieldValue("NumberAccepted");
            //BigDecimal numberAccepted = (BigDecimal)item.getFieldValue("BuysenseNumberAccepted");
            paymentItem.setFieldValue("NumberInPaymentProcess",inPorcessQty);
            paymentItem.setFieldValue("NumberPaid",numberPaid);
            paymentItem.setFieldValue("NumberAccepted",numberAccepted);

            // UAT SPL 15: moved here from above.
            inPorcessQty=inPorcessQty.add(payingQty);
            Log.customer.debug("The new Inprocess qty: %s",inPorcessQty);
            item.setFieldValue("NumberInPaymentProcess",inPorcessQty);

        }


        order.save();
        Log.customer.debug("After updating the locked order");
        return NoErrorResult;

    }

}
