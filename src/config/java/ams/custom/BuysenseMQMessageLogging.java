package config.java.ams.custom;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;

import ariba.util.core.StringUtil;
import ariba.util.log.Log;
import ariba.util.parameters.Parameters;

public class BuysenseMQMessageLogging
{
    /*
     *  DBDriver = "oracle.jdbc.driver.OracleDriver()";
        DBPassword = pwd;
        DBType = Oracle;
        DBURL = "jdbc:oracle:thin:@162.70.7.xx:1526:xxARBA";
        DBUser = ariba;
        
        AribaDBCharset = UTF8;
        AribaDBHostname = 162.70.7.xx;
        AribaDBJDBCDriverType = OracleType4;
        AribaDBPassword = "{DESede}ENCRYPTED==";
        AribaDBPort = 1526;
        AribaDBServer = xxarba;
        AribaDBType = oracle;
        AribaDBUsername = ariba;
     */
    private String     sClassName   = "BuysenseMQMessageLogging";
    private Connection moConnection;
    //private String    msDBCharset  = null;
    private String     msDBHostname = null;
    //private String    msDBType     = null;
    private String     msDBDriver   = null;
    private String     msDBServer   = null;
    private Integer    msDBPort     = null;
    private String     msDBUsername = null;
    private String     msDBPassword = null;
    private String     msDBURL      = null;
    private String     sLogKeyWord  = "E2QQ Send Req to QQ ERROR";

    BuysenseMQMessageLogging()
    {
        Parameters paramInstance = Parameters.getInstance();
        this.msDBDriver = "oracle.jdbc.driver.OracleDriver()";
        //this.msDBCharset = (String) paramInstance.getParameter("System.DatabaseSchemas.Transaction.Schema1.AribaDBCharset");
        this.msDBHostname = (String) paramInstance.getParameter("System.DatabaseSchemas.Transaction.Schema1.AribaDBHostname");
        this.msDBPort = (Integer) paramInstance.getParameter("System.DatabaseSchemas.Transaction.Schema1.AribaDBPort");
        this.msDBServer = (String) paramInstance.getParameter("System.DatabaseSchemas.Transaction.Schema1.AribaDBServer");
        this.msDBUsername = (String) paramInstance.getParameter("System.DatabaseSchemas.Transaction.Schema1.AribaDBUsername");
        this.msDBPassword = (String) paramInstance.getParameter("System.DatabaseSchemas.Transaction.Schema1.AribaDBPassword"); // this method gives decrypted pwd
        //this.msDBType = (String) paramInstance.getParameter("System.DatabaseSchemas.Transaction.Schema1.AribaDBType");
        this.msDBURL = "jdbc:oracle:thin:@" + msDBHostname + ":" + msDBPort.toString() + ":" + msDBServer;

    }

    public boolean logMQMessagetoDB(String sObjectID, String sAppName, String sMQMsg)
    {
        Log.customer.debug(sClassName + "::logMQMessagetoDB() start msDBURL: "+msDBURL);

        PreparedStatement loInsertStatement = null;

        if (StringUtil.nullOrEmptyOrBlankString(sObjectID) || StringUtil.nullOrEmptyOrBlankString(sAppName))
        {
            Log.customer.error(8888, sClassName + "::logMQMessagetoDB() DOC_ID or APP_NAME is null or blank. Can not write MQ message to DB");
            return false;
        }

        try
        {

            if (initConnection())
            {

                String sInsertSQL = "INSERT INTO COVA_MQ_MSG_LOGS (DOC_ID, APP_NAME, MESSAGE, CREATE_DATE) VALUES " + "(?,?,?,?)";
                loInsertStatement = moConnection.prepareStatement(sInsertSQL);
                loInsertStatement.setString(1, sObjectID);
                loInsertStatement.setString(2, sAppName);
                InputStream is = new ByteArrayInputStream(sMQMsg.getBytes());
                loInsertStatement.setAsciiStream(3, is, is.available());
                loInsertStatement.setTimestamp(4, new java.sql.Timestamp(Calendar.getInstance().getTimeInMillis()));
                //Log.customer.debug(sClassName + "::logMQMessage() sInsertSQL: " + loInsertStatement.toString());
                int iInserRowCount = loInsertStatement.executeUpdate();
                Log.customer.warning(8888, sClassName + "::logMQMessagetoDB() iInserRowCount "+iInserRowCount);
                if (iInserRowCount <= 0)
                {
                    Log.customer.warning(8888, sClassName + "::logMQMessagetoDB() failed to insert ");
                    return false;
                }
            }
        }
        catch (Exception loEx)
        {
            Log.customer.error(8888, sClassName + "::logMQMessagetoDB()::"+sLogKeyWord+" for "+sObjectID+" in writing to MQ Log table; message: " + loEx.getMessage());
            return false;
        }
        finally
        {
            try
            {
                if (loInsertStatement != null)
                {
                    loInsertStatement.close();
                }
                if (moConnection != null)
                {
                    moConnection.commit();
                }
                Log.customer.warning(8888, sClassName + "::logMQMessagetoDB() closed loInsertStatement and committed moConnection");
            }
            catch (Exception loEx)
            {
                Log.customer.error(8888, sClassName + "::logMQMessagetoDB() failed to close loInsertStatement or commit");
                Log.customer.error(8888, sClassName + "::logMQMessagetoDB() exception message: " + loEx.getMessage());

            }
        }
        Log.customer.debug(sClassName + "::logMQMessagetoDB() end - returning success");
        return true;
    }

    private boolean initConnection()
    {
        Log.customer.debug(sClassName + "::initConnection() start");
        try
        {
            if (moConnection == null || moConnection.isClosed())
            {
                Log.customer.debug(sClassName + "::initConnection() is null/closed so get one");
                try
                {

                    String lsDriver = msDBDriver;
                    String lsUrl = msDBURL;
                    String lsUser = msDBUsername;
                    String lsPassword = msDBPassword;

                    Log.customer.debug(sClassName + "Driver = " + lsDriver + ", URL = " + lsUrl + ", User = " + lsUser + " password = "
                            + lsPassword);
                    moConnection = (Connection) DriverManager.getConnection(lsUrl, lsUser, lsPassword);

                    Log.customer.debug(sClassName + "::initConnection() connected. now setAutoCommit to false.");
                    moConnection.setAutoCommit(false);

                }
                catch (SQLException loExp)
                {
                    Log.customer.error(8888, sClassName + "::initConnection() SQL Exception in connection - " + loExp.getMessage());
                    Log.customer.error(8888, sClassName + "::initConnection() exception in creating connection; message: " + loExp.getMessage());
                    return false;
                }
                Log.customer.debug(sClassName + "::initConnection() new connection created");
                return true;
            }
            else
            {
                Log.customer.debug(sClassName + "::initConnection() using existing connection");
                return true;
            }
        }
        catch (Exception loEx)
        {
            Log.customer.error(8888, sClassName + "::initConnection() "+sLogKeyWord+" exception - " + loEx.getMessage());
            Log.customer.error(8888, sClassName + "::initConnection() exception in connection closing; message: " + loEx.getMessage());
            return false;
        }
    }

}
