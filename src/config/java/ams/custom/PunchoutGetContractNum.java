/************************************************************************************
 * Author:  Richard Lee
 * Date:    June 2005
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 06/08/2005        Richard Lee            CR# 22 - Punchout get Contract Number
 *
 *
 * @(#)PunchoutGetContractNum.java     1.0 06/08/2005
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/


package config.java.ams.custom;

import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import ariba.base.core.LongString;
import java.io.*;
import ariba.purchasing.core.ReqLineItem;
import ariba.common.core.punchout.PunchOut;

public class PunchoutGetContractNum extends Action
{
	public void fire (ValueSource object, PropertyTable params)
	{
		String lsString = new String();
		LongString llsSPAID = null;
		ByteArrayInputStream larrayXML = null;
		if(object == null)
		{
			return;
		}
		if(!(object instanceof ReqLineItem))
		{	
			return;
		}
		ReqLineItem loReqLineItem = (ReqLineItem) object;

		Log.customer.debug("Calling PunchoutGetContractNum for LineItem: " + loReqLineItem);

		try
		{
			PunchOut loPunchOut = (PunchOut) loReqLineItem.getFieldValue("PunchOut");
			if(loPunchOut != null)
			{
				llsSPAID= (LongString) loPunchOut.getFieldValue("SupplierPartAuxId");
			}
			else
			{
				Log.customer.debug("Punchout is null. Get out.");
				return;
			}


			if (llsSPAID != null)
			{
				lsString = llsSPAID.string();
			}
			else
			{
				return;
			}
			// if not in XML format, get out of trigger
			if ( (lsString.indexOf("<field")) > 0 ) 
			{
				// Converse the long string to input stream
				byte[] moBytes = (byte[]) lsString.getBytes();
				larrayXML = new ByteArrayInputStream(moBytes);

				ParsePunchoutContractNum loPunchoutObject = new ParsePunchoutContractNum();
				loPunchoutObject.parseAndSetValue(loReqLineItem, larrayXML, lsString);
			}
			else
			{
				return;
			}
		}
		catch ( Exception loExcep )
		{
			Log.customer.debug( "exception while getting XML parser" ) ;
			loExcep.printStackTrace() ;
		}
	}
	private static ByteArrayInputStream getStringAsStream(String inString)
	{
		byte [] buf1 = null;
		buf1 = inString.getBytes();
		return  new ByteArrayInputStream (buf1);
	}
}
