package config.java.ams.custom;

import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLNameTable;
import ariba.base.core.aql.SearchTermQuery;
import ariba.base.fields.ValueSource;
import ariba.util.log.Log;

public class BuysenseAdditionalRoleNameTable extends AQLNameTable{
	
    public static final String ClassName="config.java.ams.custom.BuysenseAdditionalRoleNameTable";
    public static final String FieldName="AribaAdditionalRoles";

    
    /* Ariba 8.1 - As part of rewriting the class to only return Approvers that are of type ariba.user.core.User or
                   ariba.user.core.Group, replaced the matchPattern() and addQueryConstraints() methods with buildQuery().  */
    public void addQueryConstraints(AQLQuery query, String field, String pattern, SearchTermQuery searchTermQuery)
    {
    	String lsRole1 = "eVA-CreateRequisition";
    	String lsRole2 = "eVA-Rpt-Hier";
        Log.customer.debug( "Inside BuysenseAdditionalRoleNameTable") ;
        String lsWhereCLause = "eVA";
        ValueSource valueSourceCurrent = getValueSourceContext();
        super.addQueryConstraints(query, field, pattern, searchTermQuery);
    	if (valueSourceCurrent instanceof ariba.approvable.core.Approvable && valueSourceCurrent.getTypeName().equals("ariba.core.BuysenseUserProfileRequest"))
        { 
            	query.and(AQLCondition.parseCondition("  UniqueName LIKE '" + lsWhereCLause + "%' and UniqueName NOT IN ('" + lsRole1 + "','" + lsRole2 + "')"));
                Log.customer.debug("The final query is"+query);
        }
}
}
