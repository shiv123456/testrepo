/**
This program is designed to run in a nightly cycle or on demand.
iberaha, AMS, October 2001
*/
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import java.io.*;
import java.util.*;
import ariba.base.core.*;
import ariba.util.log.Log;

public class ContentTypeContainer
{
    
    private static Hashtable mhContentType;
    
    /**
     This method gets called from the FieldListTask on server start up.
    */
    
    public static void loadContentType(Partition foPartition)    
    {
        mhContentType = new Hashtable();        
        
        //Load ContentType.csv file
        BufferedReader  loInput = null;
        
        String lsFilename = Base.getService().getParameter(foPartition,
                                                           "Application.AMSParameters.ContentTypeFile");        
        
        try
        {
            loInput = new BufferedReader(new FileReader(lsFilename));            
            String lsFileLine = null;
            
            while ((lsFileLine = loInput.readLine()) != null)
            {
                StringTokenizer loTokenizer = new StringTokenizer(lsFileLine,
                                                                  ",");                
                int count = loTokenizer.countTokens();
                
                String lsHashKey   = null;
                String lsHashValue = null;
                
                for (int i=0; i<count; i++)
                {
                    if (i == 0)
                    {
                        lsHashKey = loTokenizer.nextToken();
                    }
                    else if (i == 1)
                    {
                        lsHashValue = loTokenizer.nextToken();
                    }
                }
                
                mhContentType.put(lsHashKey, lsHashValue);
                
            }
            loInput.close();
        }
        catch (Exception e)
        {
            Log.customer.debug("Could not find ContentType.csv file or problem reading the file. " + 
                           "Exception: " + e.getMessage());
        }
    }
    
    public static Hashtable getContentType()
    {
        return mhContentType;
    }
    
}
