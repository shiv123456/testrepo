package config.java.ams.custom;

import java.util.List;
import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.common.core.PCard;
import ariba.util.core.Date;
import ariba.purchasing.core.Requisition;
import ariba.approvable.core.Record;
/**************************
Rob Giesen
December 2000
This condition is called by both the two new fields, UsePCardBool and PCardToUse, to set them non-visible in the case
where the user doesn't have any P-Cards assigned to them.
There is a parameter that is sent, which is the requester.  Normally this is the same as the preparer, but in the case
where the On Behalf Of field is set, use that users PCards and not the preparer's.

January 2001
Added a check for the expiration date.  If the date is null, or the date has passed, then it is
not a valid PCard

rlee, 8/7/03:  ST SPL 1057 - PCardType 3 is not seen by user.
rlee 11/4/03:  Enh 18 and Prod 218, allow PCardBool checkbox and PCardToUse to be visible to user if Requisition is in Submitted or Denied status

rgiesen 01/16/2004 - Merged rlee 11/4/03 with changes necessary for Ariba 8.1
***********************/
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/
//81->822 changed Vector to List
//81->822 changed ConditionValueInfo to ValueInfo
public class BuysensePCardCondition extends Condition
{
    public static final String TargetRequesterParam = "TargetRequester";
    private static final ValueInfo parameterInfo[];
    private static final boolean DEBUG = false;

    public boolean evaluate(Object value, PropertyTable params)
    {
        Log.customer.debug("rlee 35 inside BuysensePCardCondition");
        Requisition lrReq = (Requisition)params.getPropertyForKey("TargetRequester");
		if (lrReq == null)
			return false;

        //Ariba 8.1 - get the PCardsVector a different way
        //User luRequester = (User)lrReq.getFieldValue("Requester");
        //List lvRequesterPCards =luRequester.getPCardsVector();

        ariba.user.core.User luRequester = (ariba.user.core.User)lrReq.getRequester();
		//Null check - Production # 195
		if (luRequester == null)
			return false;
		//rgiesen dev SPL #39
		//List lvRequesterPCards = ariba.common.core.User.getPartitionedUser(luRequester,Base.getSession().getPartition()).getPCardsVector();
		List lvRequesterPCards = ariba.common.core.User.getPartitionedUser(luRequester,lrReq.getPartition()).getPCardsVector();

        //Check the various condition where the PCard checkbox and PCardToUse fields will be visible to user
        //if at least one PCard is valid, it will be always be visible regardless of the condition
        //else no PCard is valid, visibility depends on Requisition Status and Record Type ..denied, submitted, withdraw, PCardBool checked ..
        if(checkRequesterPCardValidity(lvRequesterPCards))
        {
            if(DEBUG)
            Log.customer.debug("rlee bpc 20 at least one valid pcard");
            return true;
        }
        else
        {
            if(DEBUG)
            Log.customer.debug("rlee bpc 30 no valid pcard but check visible exception");
            return PCardVisibilityCondition(lrReq);
        }
    }

    // Return true if at least one of the PCards is valid
    private static boolean checkRequesterPCardValidity (List lvRequesterPCards)
    {
        int liNumPCards = lvRequesterPCards.size();
        PCard lpPCard = null;

        // Check if Requester has any other valid PCard, the one being used has expired
        if (liNumPCards>0)
        {
            //If the current user has one (or more) valid PCards assigned to them, return true, which makes the field visible
            for(int i=0;i<liNumPCards;i++)
            {
                //Get each of the Requester's PCard
                //lpPCard = (PCard)lvRequesterPCards.elementAt(i);
                // Ariba 8.1: List::elementAt() is deprecated by List::get()
               	lpPCard = (PCard)lvRequesterPCards.get(i);

                //Check to see if at least one of their PCards is of type 2
                // rlee SPL 1057, added CardType 3 allowed
                if ((lpPCard.getCardType()==2)|| (lpPCard.getCardType()==3))
                {
                    //Check if the expiration date is past today's date
                    //If it is past today's date, it is not a valid PCard
                    Date ldPCardExpDate=lpPCard.getExpirationDate();

                    if (ldPCardExpDate!=null)
                    {
                        int result=0;
                        result = Date.getNow().compareTo(ldPCardExpDate);
                        //result <0 if the current date is less than expDate
                        if (result<=0)
                        {
                            // Requester has at least one valid PCard
                            return true;
                        }
                    }
                }
            }//for
        }//if liNumPCards is greater than zero
        return false;
    }

    // If no valid PCard exist, the PCard Number and PCard checkbox can still be visible is they meet
    // certain condition.
    private boolean PCardVisibilityCondition(Requisition lrReq)
    {
        List lvRecordDeny = (List) lrReq.getFieldValue("Records");
        String lsRecordType;
        String lsStatusString = (String)lrReq.getFieldValue("StatusString");
        Record lrLastElement;
        Boolean lbUsePCardBool = (Boolean)lrReq.getDottedFieldValue("UsePCardBool");
        boolean lbUsePCard = lbUsePCardBool.booleanValue();
      //jackie - need to ask Ariba consultant or in house ariba experts. just need to confirm. should be fine
        lrLastElement = (Record)ListUtil.lastElement(lvRecordDeny);
        //lrLastElement = (Record) lvRecordDeny.get(lvRecordDeny.size()-1);
        if(DEBUG) {
        Log.customer.debug("rlee bpc 2 lvRecordDeny is %s", lvRecordDeny);
        Log.customer.debug("rlee bpc 5 lrLastElement is %s", lrLastElement);
        }

        // if the return lrLastElement or lsRecordType is null, set Record Type to NonExist.
        // meaning this requisition is not in a Withdraw status
        if(lrLastElement == null)
        {
            lsRecordType = "NonExist";
        }
        else
        {
            lsRecordType = (String)lrLastElement.getFieldValue("RecordType");
            if(lsRecordType == null)
            {
                lsRecordType = "NonExist1";
                if(DEBUG)
                Log.customer.debug("rlee bpc 10 lsRecord type is %s", lsRecordType);
            }
        }
            if(DEBUG){
            Log.customer.debug("rlee 102 lsStatusString is %s", lsStatusString);
            Log.customer.debug("rlee 104 lsRecordType is %s", lsRecordType);
            }

        // PCard Number and PCard checkbox are visible if any of the following three if's are true.
        if (lsStatusString.equalsIgnoreCase("Denied") || lsStatusString.equalsIgnoreCase("Submitted"))
        {
            //They do not have a PCard of type 2, or the expiration date is not valid
            if(DEBUG)
            Log.customer.debug("rlee 108 return true");
            return true;
        }
        else if ((lsStatusString.equalsIgnoreCase("Composing")) && (lsRecordType.equalsIgnoreCase("WithdrawRecord")))
        {
            //visible for non-requesters
            if(DEBUG)
            Log.customer.debug("rlee 112 return true");
            return true;
        }
        else if ((lsStatusString.equalsIgnoreCase("Composing")) && (lbUsePCard))
        {
            // visible if this is a change order and UsePcardBool box is checked and in composing
            if(DEBUG)
            Log.customer.debug("rlee 118 return true");
            return true;
        }
        else
        {
            if(DEBUG)
            Log.customer.debug("rlee 119 return false");
            return false;
        }
    }// end: private boolean PCardVisibilityCondition(Requisition lrReq)

    private List getTargetValues(PropertyTable params)
    {

        List targetValues = ListUtil.list();
        Object targetValue = params.getPropertyForKey("TargetRequester");

        if(targetValue != null)
            targetValues.add(targetValue);
        return targetValues;
    }

    protected ValueInfo[] getParameterInfo()
    {

        return parameterInfo;
    }

    public BuysensePCardCondition()
    {

    }

    static
    {
        parameterInfo = (new ValueInfo[] {
        new ValueInfo("TargetRequester", 0)});
    }
}
