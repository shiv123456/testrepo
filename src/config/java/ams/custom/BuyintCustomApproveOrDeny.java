/************************************************************************************
 * Author:  Richard Lee
 * Date:    July 30, 2004
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 07/30/2004        Richard Lee            Integration Interface
 * 10/05/2004        Richard Lee	    ST SPL 215 - INT Custom Approver remains 
 *                                                       active approver
 * 3/7/2005          Richard Lee	    Dev SPL 525 - INT Req Integration Update
 *
 *
 * @(#)BuyinCustomApproveOrDeny.java      2.0 2004/08/12
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/

package config.java.ams.custom;

import ariba.approvable.core.CustomApprover;
import ariba.common.core.*;
import ariba.common.core.User;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;
import ariba.purchasing.core.Requisition;
import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.base.core.BaseObject;
import ariba.base.core.Partition;
import java.util.Map;

/*
 * ScheduledTask will pickup and parse reply message from ERP
 * and will call this module to approve or deny the PreEncumbrance
 */
//81->822 changed Hashtable to Map
public class BuyintCustomApproveOrDeny implements BuyintConstants
{
    public void fire(ClusterRoot foObject, Map foparams)
    {
        Log.customer.debug("Calling BuyintCustomApproveOrDeny.");

        Requisition loReq=(Requisition) foObject;
        Partition loPartition = loReq.getPartition();
        BaseObject loReqObject = (BaseObject) loReq;
        String lsReqUN = (String) loReq.getDottedFieldValue("UniqueName");
        String lsStatusString = (String) loReq.getDottedFieldValue("StatusString");
        User loAribaSystemUser = Core.getService().getAribaSystemUser(loPartition);
        String lsRecordType = null;
        /*
         * CSPL-1137, passing null for ERP_MESSAGE if it is Blank or Empty.
         */
        String lsMessage = (String)foparams.get(ERP_MESSAGE);
    	if(StringUtil.nullOrEmptyOrBlankString(lsMessage))
        		lsMessage = null;
        String lsResponseType = (String)foparams.get(RESPONSE_TYPE);
        String lsISRule = (String)foparams.get(INTEGRATION_SIGNER_RULE);
        String lsMsg = null;
        CustomApprover loBCA = null;
        String lsBCAID = null;

        loBCA =(CustomApprover)(Base.getService().objectMatchingUniqueName
            ("ariba.approvable.core.CustomApprover",
            loPartition,"Requisition Custom Approver"));

        if ( loBCA != null)
        {
           lsBCAID = loBCA.getBaseId().toDBString();
        }
        String lsReqID = loReq.getBaseId().toDBString();
        String lsCustomApproverToken = (String) loReq.getDottedFieldValue("CustomApproverToken");

        if ((lsCustomApproverToken == null) || !(lsCustomApproverToken.length() > 0))
        {
            Log.customer.debug("CustomApprover Token is null.");
            return;
        }
        else if(!(lsStatusString.equalsIgnoreCase("Submitted")))
        {
            lsMsg = "Requisition status = " + loReq.getStatusString() + ".";
            lsRecordType = "PreEncXRecord";
            BuysenseUtil.createHistory(loReqObject, lsReqUN,  lsRecordType, lsMsg, loAribaSystemUser);
            Log.customer.debug("StatusString is not in Submitted.");
            return;
       }


        if((lsResponseType.trim().equalsIgnoreCase(STATUS_ERP_APPROVE)))
        {
           //loBCA.approve(lsReqID, lsBCAID, lsCustomApproverToken);
           int liApproveCode = loBCA.approve(lsReqID, lsBCAID, lsCustomApproverToken, null);
           Log.customer.debug("Custom approve return code: "+liApproveCode);
           lsRecordType = "PreEncSRecord";
           loReq.setDottedFieldValue("PreEncumbered", new Boolean(true));
           loReq.setDottedFieldValue("PreEncumbranceStatus", STATUS_ERP_APPROVE);
           loReq.setDottedFieldValue("IntegrationSignerRule", "null");
        }
        else
        {
           int liDenyCode = loBCA.deny(lsReqID, lsBCAID, lsCustomApproverToken);
           Log.customer.debug("Custom deny return code: "+liDenyCode);
           lsRecordType = "PreEncFRecord";
           loReq.setDottedFieldValue("PreEncumbered", new Boolean(false));
           loReq.setDottedFieldValue("PreEncumbranceStatus", STATUS_ERP_DENY);
           loReq.setDottedFieldValue("IntegrationSignerRule", lsISRule);
        }

        //Update History tab and PreEncumbranceStatus if approved or denied without error
        BuysenseUtil.createHistory(loReqObject, lsReqUN,  lsRecordType, lsMessage, loAribaSystemUser);

        return;
    }
}
