/**
 * @(#)BuyintPushPCO.java      2005/02/22
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information"). You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 * PVCS Modification :
 *
 * $Log:   R:\Software\development\archives\Ariba 8.x Customizations\app\buyer\Server\classes\extensions\config\java\ams\custom\BuyintPushPCO.java-arc  $
 * 
 *    Rev 1.4   17 Apr 2006 11:23:00   rlee
 * Dev SPL 732 - INT - New Flag for ByPass Integration.
 * 
 *    Rev 1.3   01 Feb 2006 16:42:44   rlee
 * Dev SPL 719 - INT - Set EncumbranceStatusChangeDate with trigger.
 * 
 *    Rev 1.2   21 Apr 2005 09:10:16   ahiranan
 * ST SPL 1501 - Set custom date field when EncumbranceStatus is changed for DW.
 * 
 *    Rev 1.1   12 Apr 2005 15:39:16   nrao
 * VEPI ST #1503: Added code to create history records when pushing encumbrances. 
 * 
 *    Rev 1.0   10 Mar 2005 13:12:30   cm
 * Initial revision.
 *
*/

package config.java.ams.custom;

import ariba.purchasing.core.Requisition;
import java.util.Map;
import ariba.pcard.core.PCardOrder;
import java.util.List;

import ariba.purchasing.core.POLineItem;
import ariba.base.core.*;
import ariba.base.core.aql.*;
import ariba.common.core.*;
import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.util.scheduler.*;
import java.util.Iterator;

import org.apache.log4j.Level;

//81->822 changed Vector to List
//81->822 changed Hashtable to Map
public class BuyintPushPCO extends ScheduledTask implements BuyintConstants 
{
    /* The partition */
    private Partition moPart = null;

    /* aribasystem user object */
    private ariba.common.core.User moAribaSystemUser = null;

    /* Max number of transactions of each kind to run in one run of scheduled task */
    private int miMaxTransactions = 5;

    /* Max Age of transactions to include ... 0 implies only today's transactions */
    private int miMaxTxnAge = 0;

    /* Set of apost-enclosed, commma separated values of StatusString enclosed in parentheses to identify orders for SEND */
    private String msSendStatusSet = "('Ordered','Received','Receiving')";

    /* Set of apost-enclosed, commma separated values of StatusString enclosed in parentheses to identify orders for CANCEL */
    private String msCancelStatusSet = "('Canceled')";

    /* Parameters Map for AQL options */
    private Map moParams = MapUtil.map();

    public void run()
    {
       debug("Entered run ...");

       // Get Today's date as object for use in AQL
       Date loDate = new Date();
       Date.setTimeToMidnight(loDate);
       // Subtract max transaction age from current date for inclusion in set 
       Date.addDays(loDate, (0 - miMaxTxnAge));

       // Save data into parameter Map
       moParams.clear();
       moParams.put("Today", loDate);

       // Process SEND transactions     
       // moParams.put("StatusSet",msSendStatusSet);
       moParams.put("EncStatus",BuyintConstants.STATUS_ERP_READY);
       processOrders(BuyintConstants.TXNTYPE_SEND);

       // Process CNCL transactions     
       // moParams.put("StatusSet",msCancelStatusSet);
       moParams.put("EncStatus",BuyintConstants.STATUS_ERP_APPROVE);
       processOrders(BuyintConstants.TXNTYPE_CANCEL);

       debug("Done with run ...");
    }

   private void processOrders(String foTxnType) 
   {
       AQLOptions loOptions;
       AQLQuery loQuery;
       AQLResultCollection loResults;

       BaseId loBid; 
       PCardOrder loPCO;
       ariba.user.core.User loRequester;
       ariba.common.core.User loPartitionedUser;

       int liNumOfTxn = 0;
       int liTxnCounter = 0;

       // Prep for running AQL
       loOptions = new AQLOptions(moPart);
       loOptions.setActualParameters(moParams);

       /* ----------------------------------------------------------------------- */
       // Construct and run AQL for PCardOrders to push
       loQuery = AQLQuery.parseQuery(constructAQL(foTxnType));
       loResults = Base.getService().executeQuery(loQuery, loOptions);

       if (loResults.getFirstError() == null) 
       {
          liNumOfTxn = loResults.getSize();
          debug("processOrders returned " + liNumOfTxn + " PCardOrders ready for " + foTxnType);
   
          if (liNumOfTxn > 0)
          {
             if (liNumOfTxn > miMaxTransactions) 
             {
                Logs.buysense.setLevel(Level.DEBUG);
                Logs.buysense.debug("BuyintPushPCO: Number of eligible PCardOrders (" + liNumOfTxn + ")" + 
                                    " exceeds Max Transactions limit (" + miMaxTransactions + ") ..." + 
                                    " will skip " + (liNumOfTxn - miMaxTransactions) + " orders in this run");
                Logs.buysense.setLevel(Level.DEBUG.OFF);
             }

             while ((loResults.next()) && (liTxnCounter < miMaxTransactions))
             {
                liTxnCounter++;
                loBid = (BaseId)loResults.getBaseId("PurchaseOrder") ;
                loPCO = (PCardOrder)loBid.get();
                debug("Retrieved PCardOrder = " + loPCO.getUniqueName() + " for " + foTxnType);
   
                loRequester = (ariba.user.core.User)loPCO.getDottedFieldValue("LineItems[0].Requisition.Requester");
                loPartitionedUser = ariba.common.core.User.getPartitionedUser(loRequester, moPart);
                if (foTxnType.equals(BuyintConstants.TXNTYPE_SEND))
                {
                   sendOrder(loPCO, loPartitionedUser);
                }
                else if (foTxnType.equals(BuyintConstants.TXNTYPE_CANCEL))
                {
                   cancelOrder(loPCO, loPartitionedUser, BuyintConstants.TXNTYPE_CANCEL);
                }
                 
             } // end while
   
             debug("Processed " + liTxnCounter + " " + foTxnType + " transactions");
          }
       }
       else
       {
          Logs.buysense.setLevel(Level.DEBUG);
          Logs.buysense.debug("BuyintPushPCO: (AQLError) " + loResults.getFirstError().toString());
          Logs.buysense.setLevel(Level.DEBUG.OFF);
       }
   }

   private void sendOrder(PCardOrder foPO, ariba.common.core.User foUser)
   {
      debug("Entered sendOrder ... PCardOrder ID is " + foPO.getUniqueName());
      String lsPO = foPO.getUniqueName();

      if (AMSPOPrep.isEncumbranceRequired(foPO)) 
      {
         debug(lsPO + " selected for send ...");
         foPO.setDottedFieldValue("EncumbranceStatus",BuyintConstants.STATUS_ERP_INPROGRESS);
         foPO.setDottedFieldValue("Encumbered",new Boolean(false));
         List lvLines = foPO.getLineItems();
         Requisition loReq = (Requisition)((POLineItem)lvLines.get(0)).getRequisition();
         debug("Req is " + loReq.getUniqueName() + " ... about to create history rec");
         BuysenseUtil.createHistory(foPO, lsPO, RECORD_ENCUMBERING, null, moAribaSystemUser);
         BuysenseUtil.createHistory(loReq, lsPO, RECORD_ENCUMBERING, null, moAribaSystemUser);
         foPO.save();
         BuyintXMLFactory.submit(BuyintConstants.TXNTYPE_SEND, foPO); 
         Base.getSession().transactionCommit();
      }
      else
      {
         debug(lsPO + " not selected for send ...");
      }

      if (foPO.getDottedFieldValue("PreviousVersion") != null)
      {
         debug("Order has a previous version which may need to be cancelled");
         PCardOrder loPCOPrev = (PCardOrder)foPO.getDottedFieldValue("PreviousVersion");
         cancelOrder(loPCOPrev, foUser, BuyintConstants.TXNTYPE_ENC_CANCEL_ON_CHG );
      }
      debug("Leaving sendOrder ...");
   }

   private void cancelOrder(PCardOrder foPO, ariba.common.core.User foUser, String foTxnType)
   {
      debug("Entered cancelOrder ... PCardOrder ID is " + foPO.getUniqueName());
      String lsPO = foPO.getUniqueName();

      String lsEStatus = (String)foPO.getDottedFieldValue("EncumbranceStatus");
      boolean lboolEnc = ((Boolean)foPO.getDottedFieldValue("Encumbered")).booleanValue();
      if (lboolEnc && lsEStatus.equals(BuyintConstants.STATUS_ERP_APPROVE))
      {
         debug(lsPO + " selected for cancellation ...");
         foPO.setDottedFieldValue("EncumbranceStatus",BuyintConstants.STATUS_ERP_CANCELLED);
         List lvLines = foPO.getLineItems();
         Requisition loReq = (Requisition)((POLineItem)lvLines.get(0)).getRequisition();
         debug("Req is " + loReq.getUniqueName() + " ... about to create history rec");
         BuysenseUtil.createHistory(foPO, lsPO, RECORD_ENC_CANCEL, null, moAribaSystemUser);
         BuysenseUtil.createHistory(loReq, lsPO, RECORD_ENC_CANCEL, null, moAribaSystemUser);
         foPO.save();
         if (foTxnType.equals(BuyintConstants.TXNTYPE_CANCEL))
         {
            BuyintXMLFactory.submit(BuyintConstants.TXNTYPE_CANCEL, foPO);
         }
         else
         {
            BuyintXMLFactory.submit(BuyintConstants.TXNTYPE_ENC_CANCEL_ON_CHG, foPO);
         }
         Base.getSession().transactionCommit();
      }
      else
      {
         debug(lsPO + " not selected for cancellation ...");
      }
      debug("Leaving cancelOrder ...");
   }

    private String constructAQL(String fsTranType)
    {
       String lsType = "PurchaseOrder.Type = 'ariba.pcard.core.PCardOrder'";
       debug("About to build AQL for TXNTYPE = " + fsTranType);

       String lsAQL = "SELECT PurchaseOrder FROM ariba.purchasing.core.PurchaseOrder" +
                      " WHERE LastModified >= :Today" +
                      " AND " + lsType +
                      " AND EncumbranceStatus = :EncStatus";

       if (fsTranType.equalsIgnoreCase(BuyintConstants.TXNTYPE_CANCEL))
       {
          lsAQL += " AND StatusString in " + msCancelStatusSet; 
       }
       else if (fsTranType.equalsIgnoreCase(BuyintConstants.TXNTYPE_SEND))
       {
          lsAQL += " AND StatusString in " + msSendStatusSet; 
       }
       else          // default behavior is to treat as SEND ... 
       {
          lsAQL += " AND StatusString in " + msSendStatusSet; 
       }
       // sort in arrival sequence
       lsAQL += " ORDER BY LastModified ASC";
      
       debug("This is the built AQL: " + lsAQL);
       return lsAQL;

    }

    public void init(Scheduler scheduler, String scheduledTaskName, java.util.Map arguments)
    {
        debug("Entered init ...");
        String lsKey = null;
        String lsValue = null;
        super.init(scheduler, scheduledTaskName, arguments);

        for (Iterator loItr = arguments.keySet().iterator(); loItr.hasNext();)
        {
           lsKey = (String)loItr.next() ;

           if(lsKey.equals("MaxTransactions"))
           {
              lsValue = (String)arguments.get(lsKey);
              try 
              {
                 miMaxTransactions = Integer.parseInt(lsValue);
                 if (miMaxTransactions < 0) 
                 {
                    // -ve number implies no limit
                    miMaxTransactions = Integer.MAX_VALUE;
                 }
              }
              catch(NumberFormatException loEx)
              {
                 miMaxTransactions = 5;
              } 
           }

           if(lsKey.equals("MaxTransactionAge"))
           {
              lsValue = (String)arguments.get(lsKey);
              try 
              {
                 miMaxTxnAge = Integer.parseInt(lsValue);
              }
              catch(NumberFormatException loEx)
              {
                 miMaxTxnAge = 0;
              } 
           }

           if(lsKey.equals("SendStatusSet"))
           {
              msSendStatusSet = (String)arguments.get(lsKey);
           }

           if(lsKey.equals("CancelStatusSet"))
           {
              msCancelStatusSet = (String)arguments.get(lsKey);
           }
        }

        debug("Parameters from ScheduledTasks.table: " + 
              "MaxTransactions=" + miMaxTransactions + ", " + 
              "MaxTransactionAge=" + miMaxTxnAge + ", " + 
              "SendStatusSet=" + msSendStatusSet + ", " + 
              "CancelStatusSet=" + msCancelStatusSet);

        moPart = Base.getService().getPartition("pcsv");

        moAribaSystemUser = Core.getService().getAribaSystemUser(moPart);

        debug("init is finit ...");
    }
    
    
    public static void debug(String fsMsg)
    {
        Log.customer.debug("BuyintPushPCO: " + fsMsg);
    }

}
