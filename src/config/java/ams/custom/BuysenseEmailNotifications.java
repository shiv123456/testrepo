package config.java.ams.custom;

import java.io.File;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import ariba.user.util.mail.Notification;
import ariba.base.core.Base;
import ariba.base.core.MultiLingualString;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueInfo;
import ariba.base.fields.ValueSource;
import ariba.procure.core.CategoryLineItemDetails;
import ariba.purchasing.core.POLineItem;
import ariba.purchasing.core.PurchaseOrder;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.user.core.Role;
import ariba.user.core.User;
import ariba.util.core.Constants;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;
import ariba.workforce.core.Contractor;
import ariba.workforce.core.ContractorCandidate;
import ariba.workforce.core.LaborLineItemDetails;
import ariba.workforce.core.TimeSheet;

/**
 * This file is used for ACP notifications only
 * 
 * CSPL # 1470-End engagement email populated with null value
 * 
 * @author Pavan Aluri
 * @Date 03-Dec-2009
 * @version 1.1
 * @Explanation getBody_FmtSil(TimeSheet timeSheet) method is modified to read the values of DLSN and COVA ID from Contractor object instead of Contractor user object.
 * 
 * CSPL # 1462-End engagement notification not sent when change order used to reduce hours and close order
 * @author Pavan Aluri
 * @Date 03-Feb-2010
 * @version 1.2
 * @Explanation Removed ";" for if condition so as to call the notifyUser method only for non-collaboration line items.
 * 
 * CSPL # 1690-No End Engagement email sent to ASO when temp labor order is over-received
 * @author Pavan Aluri
 * @Date 02-Jul-2010
 * @version 1.3
 * @Explanation Added code to accept parameters passed from aml and modified handling action from POLine
 * 
 * CSPL # 1690-No End Engagement email sent to ASO when temp labor Line is deleted or Order that contains Temp Labor is deleted
 * @author Sarath Babu Garre
 * @Date 07-Jul-2010
 * @version 1.4
 * @Explanation Added code to send e-maill for canceld orders.
 *  
 *  
 */

public class BuysenseEmailNotifications extends Action
{
    Contractor contractor = null;
    String     sClassName = "BuysenseEmailNotifications";

    public void setContractor(Contractor contr)
    {
        contractor = contr;
    }

    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        try
        {

            Log.customer.debug(sClassName + ".fire()- valuesource: " + valuesource);
            String fieldParam = propertytable.stringPropertyForKey("TestField");
            Log.customer.debug(sClassName + ".fire()- fieldParam: " + fieldParam);
            if (valuesource != null && valuesource instanceof Requisition)
            {
                Requisition req = (Requisition) valuesource;
                List lreqLineItems = req.getLineItems();
                List lreqDeletedLineItems = req.getDeletedLineItems();

                for (int i = 0; i < lreqLineItems.size(); i++)
                {
                    Log.customer.debug(sClassName + " Checking ReqLI: " + i);
                    ReqLineItem reqLineItem = (ReqLineItem) lreqLineItems.get(i);

                    if (iSAgencySecurityOfficerNotifiable(reqLineItem))
                    {
                        // Passing true for re-hire
                        notifyUser(reqLineItem, true);
                    }
                }
                for (int i = 0; i < lreqDeletedLineItems.size(); i++)
                {
                    Log.customer.debug(sClassName + " Checking Deleted line: " + i);
                    ReqLineItem reqDeletedLineItem = (ReqLineItem) lreqDeletedLineItems.get(i);
                    if (iSAgencySecurityOfficerNotifiable(reqDeletedLineItem))
                    {
                        // Passing false for end engagement
                        notifyUser(reqDeletedLineItem, false);
                    }
                }
            }
            else if (valuesource != null && valuesource instanceof TimeSheet)
            {

                TimeSheet timeSheet = (TimeSheet) valuesource;
                BigDecimal totalRemainingHours = timeSheet.getTotalRemainingHours();
                Log.customer.debug(sClassName + " totalRemainingHours: " + totalRemainingHours);
                if (!(totalRemainingHours.compareTo(Constants.ZeroBigDecimal) == 0))
                {
                    Log.customer.debug(sClassName + " totalRemainingHours is not Zero");
                    return;
                }
                Log.customer.debug(sClassName + " totalRemainingHours is Zero");

                /* CEPL-1690;
                 * Below line is commented so as not to sent end-engagement mail, which will be done through POLine 
                 */
                //notifyUser(timeSheet);
                // Process the composing receipts if any
                BuysenseProcessComposingReceipts.processReceipts(timeSheet);
            }
            else if (valuesource != null && valuesource instanceof POLineItem)
            {
                //This block is called from POLine for field change of Quantity and NumberAccepted
                //Quantity fires when user reduces/increases the Quantity in Change Order. So has to check for previous versions
                //NoumberAccepted fires when quantity is received for the respective line. End- engagement mails has to be sent when NumberAccepted >= Quantity,
                //so no need to check for versions.
                POLineItem poli = (POLineItem) valuesource;
                Log.customer.debug(sClassName + " processing poli: " + poli);
                PurchaseOrder po = (PurchaseOrder) poli.getLineItemCollection();
                Log.customer.debug(sClassName + " po: " + po);
                if (po != null)
                    Log.customer.debug(sClassName + " po.hasPreviousVersion(): " + po.hasPreviousVersion());
                Log.customer.debug(sClassName + " poli.getNumberOnReq(): " + poli.getNumberOnReq());
                boolean bCheckForPreviousVersion = true;
                if (!StringUtil.nullOrEmptyOrBlankString(fieldParam) && fieldParam.equalsIgnoreCase("Quantity"))
                {
                    if (po != null && !po.hasPreviousVersion())
                    {
                        bCheckForPreviousVersion = false;
                    }
                }

                for (Iterator poCLIDiterator = poli.getPOCategoryLineItemDetailsVectorIterator(); poCLIDiterator
                        .hasNext();)
                {
                    CategoryLineItemDetails catLID = (CategoryLineItemDetails) poCLIDiterator.next();
                    Log.customer.debug(sClassName + "catLID: " + catLID);
                    Log.customer.debug(sClassName + "bCheckForPreviousVersion: " + bCheckForPreviousVersion);
                    if (bCheckForPreviousVersion && catLID instanceof LaborLineItemDetails)
                    {
                        Log.customer.debug(sClassName + " po has previous version and category LI: ");
                        BigDecimal qty = poli.getQuantity();
                        BigDecimal noAccepted = poli.getNumberAccepted();
                        Log.customer.debug(sClassName + " Quantity: " + qty);
                        Log.customer.debug(sClassName + " NumberAccepted: " + noAccepted);

                        String sASONotified = (String) poli.getDottedFieldValue("ASONotified");
                        if (StringUtil.nullOrEmptyOrBlankString(sASONotified))
                        {
                            poli.setDottedFieldValue("ASONotified", "NO");
                        }
                        if (noAccepted.compareTo(qty) >= 0 && (!sASONotified.equalsIgnoreCase("YES")))
                        {
                            Log.customer.debug(sClassName + " qty >= noAccepted.");
                            Requisition req = (Requisition) poli.getRequisition().getLatestVersion();
                            Log.customer.debug(sClassName + " req: " + req);
                            ReqLineItem matchedReqLI = (ReqLineItem) req.getLineItem(poli.getNumberOnReq());
                            Log.customer.debug(sClassName + " matchedReqLI: " + matchedReqLI);

                            // Passing false for end engagement
                            notifyUser(matchedReqLI, false);

                            //setting ASONotified
                            poli.setDottedFieldValue("ASONotified", "YES");
                        }
                    }
                }
            }
            return;
        }
        catch (Exception e)
        {
            Log.customer.debug(sClassName + ".fire()-Exception: " + e.getMessage());
            return;
        }
    }

    // This method is to check for re-hire
    private boolean iSAgencySecurityOfficerNotifiable(ReqLineItem reqLI)
    {
        Object oldValues = reqLI.getOldValues();
        int iChangedState = reqLI.getChangedState();
        String sReqStatus = (String) reqLI.getDottedFieldValue("LineItemCollection.StatusString");
        Log.customer.debug(sClassName + "oldValues: " + oldValues + " ,iChangedState: " + iChangedState
                + " ,sReqStatus: " + sReqStatus);
        if (oldValues != null && iChangedState == 4)
        {
            return false;
        }
        // CSPL-1690
        // Same class is called from Requisition to send re-hire mail for newly added line while Receiving  
        // This is to prevent sending mail for old lines in Receiving 
        if (oldValues == null && iChangedState == 0 && sReqStatus.equalsIgnoreCase("Receiving"))
        {
            return false;
        }
        if (oldValues == null || sReqStatus.equalsIgnoreCase("Canceled") || reqLI.getChangedState() == 8)
        {
            for (Iterator itera = reqLI.getCategoryLineItemDetailsVectorIterator(); itera.hasNext();)
            {
                CategoryLineItemDetails catLID = (CategoryLineItemDetails) itera.next();

                if (catLID != null && catLID instanceof LaborLineItemDetails)
                {
                    LaborLineItemDetails labLID = (LaborLineItemDetails) catLID;
                    //if a Labor line is deleted from an order
                    Log.customer.debug("2nd time Deleted line");
                    if (((labLID.getContractor() != null) || (labLID.getSelectedCandidate() != null))
                            && (reqLI.getLineItemCollection().getDeletedLineItems().size() > 0))
                    {
                        Log.customer.debug(sClassName
                                + " iSAgencySecurityOfficerNotifiable()- returns true for deleted lines");
                        return true;
                    }
                    // If Collaboration is set to 'Yes' and vendor proposes an existing contractor
                    // then 'ProcessContractorCreation' process takes care of generating the re-hire email.
                    // We will send the email only when Collaboration is set to 'No'
                    if ((!labLID.getCollaborate().booleanValue()) && (labLID.getContractor() != null))
                    {
                        Log.customer.debug(sClassName + " iSAgencySecurityOfficerNotifiable()- returns true");
                        return true;
                    }
                }
            }
        }
        Log.customer.debug(sClassName + " iSAgencySecurityOfficerNotifiable()- returns false ");
        return false;
    }

    private String getSubject_FmtSil(ReqLineItem reqLineItem, boolean rehire)
    {
        String sContractorName = null;
        String sSubject = null;
        Contractor contractor = getContractor(reqLineItem);
        if (contractor != null)
        {
            sContractorName = (String) contractor.getDottedFieldValue("Name.PrimaryString");
        }
        if (rehire)
        {
            sSubject = Fmt.Sil(Base.getSession().getLocale(), "aml.Collaboration",
                    "AgencySecurityOfficerNotificationSubStr", reqLineItem.getOrderID(), sContractorName);
        }
        else
        {
            String sOrderUName = reqLineItem.getLatestOrder() != null ? reqLineItem.getLatestOrder().getOrderID()
                    : reqLineItem.getOrderID();
            sSubject = Fmt.Sil(Base.getSession().getLocale(), "aml.Collaboration", "AgencySecurityOfficerNotifySubStr",
                    sOrderUName, sContractorName);
        }
        return sSubject;
    }

    private String getBody_FmtSil(ReqLineItem reqLineItem, boolean brehire)
    {
        Contractor contractor = getContractor(reqLineItem);
        String sContractorName = " ";
        String sContractorUniqueName = " ";
        String sClientName = " ";
        String sContractorDLSN = " ";
        String sContractorPersonID = " ";
        String sBody = " ";
        if (contractor != null)
        {
            sContractorName = (String) contractor.getDottedFieldValue("Name.PrimaryString");
            sContractorDLSN = (String) contractor.getDottedFieldValue("DriversLicenseStateNumber");
            sContractorPersonID = (String) contractor.getDottedFieldValue("ContractorPersonID");
            User user = contractor.getUser();
            if (user != null)
            {
                ariba.common.core.User contractorPartitionedUser = ariba.common.core.User.getPartitionedUser(user,
                        reqLineItem.getPartition());
                if (contractorPartitionedUser != null)
                {
                    sClientName = (String) contractorPartitionedUser.getDottedFieldValue("ClientName.UniqueName");
                }
                sContractorUniqueName = (String) contractorPartitionedUser.getDottedFieldValue("UniqueName");
            }
        }

        User workSupervisor = getWorkSupervisor(reqLineItem);
        String sWorkSupervisorName = " ";
        String sWorkSupervisorPh = " ";
        if (workSupervisor != null)
        {
            sWorkSupervisorName = (String) workSupervisor.getDottedFieldValue("Name.PrimaryString");
            ariba.common.core.User partitionedUser = ariba.common.core.User.getPartitionedUser(workSupervisor,
                    reqLineItem.getPartition());
            sWorkSupervisorPh = (String) partitionedUser.getDottedFieldValue("Phone");
        }

        if (brehire)
        {
            // Re-hire body
            sBody = Fmt.Sil(Base.getSession().getLocale(), "aml.Collaboration",
                    "AgencySecurityOfficerNotificationBodyStr", reqLineItem.getOrderID(), sContractorName,
                    sWorkSupervisorName, sWorkSupervisorPh, sContractorUniqueName, sClientName);
            sBody = sBody
                    + Fmt.Sil(Base.getSession().getLocale(), "aml.Collaboration",
                            "AgencySecurityOfficerNotificationBodyStr2", sContractorDLSN, sContractorPersonID);

        }
        else
        {
            // End-engagement body
            String sOrderUName = reqLineItem.getLatestOrder() != null ? reqLineItem.getLatestOrder().getOrderID()
                    : reqLineItem.getOrderID();
            sBody = Fmt.Sil(Base.getSession().getLocale(), "aml.Collaboration", "AgencySecurityOfficerNotifyBodyStr",
                    sOrderUName, sContractorName, sWorkSupervisorName, sWorkSupervisorPh, sContractorUniqueName,
                    sClientName);
            sBody = sBody
                    + Fmt.Sil(Base.getSession().getLocale(), "aml.Collaboration",
                            "AgencySecurityOfficerNotifyBodyStr2", sContractorDLSN, sContractorPersonID);
        }
        return sBody;
    }

    private Contractor getContractor(ReqLineItem reqLineItem)
    {
        CategoryLineItemDetails catLID = null;
        Contractor objContractor = null;
        if (contractor != null)
        {
            return contractor;
        }
        else
        {
            catLID = reqLineItem.getCategoryLineItemDetails();
        }
        if (catLID != null && catLID instanceof LaborLineItemDetails)
        {
            LaborLineItemDetails labLID = (LaborLineItemDetails) catLID;
            if (labLID != null)
            {
                objContractor = labLID.getContractor();
                if (objContractor == null)
                {
                    ContractorCandidate selectedContractor = labLID.getSelectedCandidate();
                    if (selectedContractor != null && selectedContractor.getName() != null)
                    {
                        MultiLingualString name = selectedContractor.getName();
                        String partialGlobalID = selectedContractor.getPartialGlobalID();
                        if (name.getPrimaryString() != null && partialGlobalID != null)
                        {
                            String sClientUniqueName = (String) labLID
                                    .getDottedFieldValue("WorkLocation.ClientName.UniqueName");
                            String sDriversLicenseStateNumber = (String) selectedContractor
                                    .getDottedFieldValue("DriversLicenseStateNumber");
                            Log.customer.debug(sClassName + " Calling contractorMatchingNameAndID: "
                                    + sDriversLicenseStateNumber + " ," + sClientUniqueName);
                            objContractor = eVA_AribaContractorCreationProcessor.contractorMatchingNameAndID(
                                    sDriversLicenseStateNumber, labLID.getPartition(), sClientUniqueName);
                        }

                    }
                    else
                    {
                        Log.customer.debug(sClassName
                                + " skip finding contractor because no candidate info available for: " + labLID);
                    }
                }
            }
        }
        Log.customer.debug(sClassName + " returning objContractor: " + objContractor);
        return objContractor;
    }

    private User getWorkSupervisor(ReqLineItem reqLineItem)
    {
        CategoryLineItemDetails catLID = reqLineItem.getCategoryLineItemDetails();
        if (catLID != null && catLID instanceof LaborLineItemDetails)
        {
            LaborLineItemDetails labLID = (LaborLineItemDetails) catLID;
            if (labLID != null)
            {
                Log.customer.debug(sClassName + " .getWorkSupervisor()- WorkSupervisor is: "
                        + labLID.getWorkSupervisor());
                return labLID.getWorkSupervisor();
            }
        }
        return null;
    }

    public void notifyUser(ReqLineItem reqLineItem, boolean bRehire)
    {
        // If bRehire = true means Re-hire notification, otherwise end engagement notification
        final String fileNameWithPath = "config/variants/vcsv/data/AgencySecurityOfficer.csv";
        String sFormattedSubject = "";
        String sFormattedBody = "";
        String sClientName = "";
        sClientName = (String) reqLineItem.getDottedFieldValue("ClientName.UniqueName");
        File fileToRead = BuysenseUtil_eMailNotifications.readFile(fileNameWithPath);
        String sRoleUniqueName = BuysenseUtil_eMailNotifications.getRoleForClientName(fileToRead, sClientName);
        Role role = BuysenseUtil_eMailNotifications.getRoleFromUniqueNameString(sRoleUniqueName);// get the role for the specified Client Name
        List recipients = ListUtil.list();
        if (role == null)
        {
            Log.customer.debug(sClassName + " System is unable to get the Role for client: " + sClientName
                    + " From file: " + fileToRead + " and hence NO e-Mail notifications will be sent");
        }
        else
        {
            recipients = role.getAllUsers();// add all users of role as recipients
        }

        sFormattedSubject = getSubject_FmtSil(reqLineItem, bRehire);
        sFormattedBody = getBody_FmtSil(reqLineItem, bRehire);
        Notification.emailUsers(recipients, null, sFormattedSubject, sFormattedBody);
        Log.customer.debug(sClassName + " Notifying users: " + recipients + " with Subject-" + sFormattedSubject);
    }

/*    private ariba.common.core.User getContractorUser(TimeSheet timesheet)
    {
        User contractor = timesheet.getRequester();
        if (contractor != null)
        {
            ariba.common.core.User contractorPartitionedUser = ariba.common.core.User.getPartitionedUser(contractor,
                    timesheet.getPartition());
            return contractorPartitionedUser;
        }
        else
        {
            return null;
        }
    }*/

/*    private String getSubject_FmtSil(TimeSheet timeSheet)
    {
        String sContractorName = (String) timeSheet.getDottedFieldValue("Requester.Name.PrimaryString");
        String sOrderID = (String) timeSheet.getDottedFieldValue("Order.UniqueName");
        String sSubject = Fmt.Sil(Base.getSession().getLocale(), "aml.Collaboration",
                "AgencySecurityOfficerNotifySubStr", sOrderID, sContractorName);
        return sSubject;
    }*/

/*    private String getBody_FmtSil(TimeSheet timeSheet)
    {
        String sOrderID = (String) timeSheet.getDottedFieldValue("Order.UniqueName");

        String sContractorName = " ";
        String sContractorUniqueName = " ";
        String sClientName = " ";
        String sContractorDLSN = " ";
        String sContractorPersonID = " ";
        Contractor contractor = timeSheet.getContractor();
        if (contractor != null)
        {
            sContractorName = (String) contractor.getDottedFieldValue("Name.PrimaryString");
            sContractorDLSN = (String) contractor.getDottedFieldValue("DriversLicenseStateNumber");
            sContractorPersonID = (String) contractor.getDottedFieldValue("ContractorPersonID");
            User user = contractor.getUser();
            if (user != null)
            {
                ariba.common.core.User contractorPartitionedUser = getContractorUser(timeSheet);
                if (contractorPartitionedUser != null)
                {
                    sClientName = (String) contractorPartitionedUser.getDottedFieldValue("ClientName.UniqueName");
                }
                sContractorUniqueName = (String) contractorPartitionedUser.getDottedFieldValue("UniqueName");
            }
        }

        User workSupervisor = timeSheet.getRequester().getSupervisor();
        String sWorkSupervisorName = " ";
        String sWorkSupervisorPh = " ";
        if (workSupervisor != null)
        {
            sWorkSupervisorName = (String) workSupervisor.getDottedFieldValue("Name.PrimaryString");
            ariba.common.core.User partitionedUser = ariba.common.core.User.getPartitionedUser(workSupervisor,
                    timeSheet.getPartition());
            sWorkSupervisorPh = (String) partitionedUser.getDottedFieldValue("Phone");
        }
        String sBody = Fmt.Sil(Base.getSession().getLocale(), "aml.Collaboration",
                "AgencySecurityOfficerNotifyBodyStr", sOrderID, sContractorName, sWorkSupervisorName,
                sWorkSupervisorPh, sContractorUniqueName, sClientName);
        sBody = sBody
                + Fmt.Sil(Base.getSession().getLocale(), "aml.Collaboration", "AgencySecurityOfficerNotifyBodyStr2",
                        sContractorDLSN, sContractorPersonID);
        return sBody;
    }*/

    // End engagement notification
/*    private void notifyUser(TimeSheet timeSheet)
    {
        final String fileNameWithPath = "config/variants/vcsv/data/AgencySecurityOfficer.csv";
        String sFormattedSubject = " ";
        String sFormattedBody = " ";
        sFormattedSubject = getSubject_FmtSil(timeSheet);
        sFormattedBody = getBody_FmtSil(timeSheet);
        String sClientName = " ";
        ariba.common.core.User contractor_user = getContractorUser(timeSheet);
        if (contractor_user != null)
        {
            sClientName = (String) contractor_user.getDottedFieldValue("ClientName.UniqueName");
        }

        File fileToRead = BuysenseUtil_eMailNotifications.readFile(fileNameWithPath);
        String sRoleUniqueName = BuysenseUtil_eMailNotifications.getRoleForClientName(fileToRead, sClientName);
        Role role = BuysenseUtil_eMailNotifications.getRoleFromUniqueNameString(sRoleUniqueName);// get the role for the specified Client Name

        List recipients = ListUtil.list();
        if (role == null)
        {
            Log.customer.debug(sClassName + " System is unable to get the Role for client: " + sClientName
                    + " From file: " + fileToRead + " and hence NO e-Mail notifications will be sent");
        }
        else
        {
            recipients = role.getAllUsers();// add all users of role as recipients
        }

        Notification.emailUsers(recipients, null, sFormattedSubject, sFormattedBody);
        Log.customer.debug(sClassName + " Notifying users: " + recipients + " with Subject-" + sFormattedSubject);
    }*/

    /**
        Return the list of valid value types.
    */
    protected ValueInfo getValueInfo()
    {
        return valueInfo;
    }

    /**
        Return the parameter names.
    */
    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    /**
        Return the required parameter names.
    */
    protected String[] getRequiredParameterNames()
    {
        return requiredParameterNames;
    }

    private static final ValueInfo   valueInfo                = new ValueInfo(0);
    private static final ValueInfo[] parameterInfo            = { new ValueInfo("TestField", IsScalar, StringClass) };
    private static final String      requiredParameterNames[] = { "TestField" };
}
