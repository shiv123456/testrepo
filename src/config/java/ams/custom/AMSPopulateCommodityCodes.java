package config.java.ams.custom;

import java.util.Map;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.core.BaseId;
import ariba.base.core.BaseVector;
import ariba.base.core.Partition;
import ariba.base.core.Base;
import ariba.basic.core.CommodityCode;
import ariba.purchasing.core.POLineItem;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.util.core.MapUtil;
import ariba.util.scheduler.ScheduledTask;
import ariba.util.scheduler.Scheduler;
import ariba.util.log.Log;

public class AMSPopulateCommodityCodes extends ScheduledTask {

	public static final String ClassName = "config.java.ams.custom.AMSPopulateCommodityCodes";

	private String msRequisitionQuery = null ;
	private String msCommodityQuery = null ;
	private String msDummyCommodityQuery = null ;

	private int miTransactionIndicator = 0;
	private int miUnNecessaryExec = 0;

	AQLOptions moOptions = null;
	AQLQuery moQuery = null;
	AQLResultCollection moAQLResults = null;
	Partition moPartition = null;
	Partition moNonePartition = null;
	CommodityCode mo822DummyCommodity = null;
	Map mCommodityMapTable = MapUtil.map();

	public void init(Scheduler scheduler, String scheduledTaskName, Map arguments)
	{
        super.init(scheduler, scheduledTaskName, arguments);
        String lsKey = null;
        for(java.util.Iterator e = arguments.keySet().iterator(); e.hasNext();)
        {
           lsKey = (String)e.next();
           if (lsKey.equals("ProcessQuery"))
           {
        	   msRequisitionQuery = (String)arguments.get(lsKey);
           }else if(lsKey.equals("CommodityQuery"))
           {
        	   msCommodityQuery = (String) arguments.get(lsKey);
           }else if(lsKey.equals("DummyCommodityQuery"))
           {
        	   msDummyCommodityQuery = (String) arguments.get(lsKey);
           }
        }
	}

	public void run()
	{
		//Log.customer.debug("Class %s on method run :: variable msRequisitionQuery is %s", ClassName, msRequisitionQuery);
		//Log.customer.debug("Class %s on method run :: variable msCommodityQuery is %s", ClassName, msCommodityQuery);
		//Log.customer.debug("Class %s on method run :: variable msDummyCommodityQuery is %s", ClassName, msDummyCommodityQuery);
		moPartition = Base.getService().getPartition("pcsv");
		moNonePartition = Base.getService().getPartition("None");
		mCommodityMapTable = mPutCommodityCodesInMapTable();
		//Log.customer.debug("Class %s on method run :: variable mCommodityMapTable is %s", ClassName, mCommodityMapTable);
		mo822DummyCommodity = getDummyCommodityCode();
		//Log.customer.debug("Class %s on method run :: variable mo822DummyCommodity is %s", ClassName, mo822DummyCommodity.getUniqueName());
		AQLResultCollection loRequisitions = mAQLReqResults();
		Log.customer.debug("Class %s on method run :: REQ query got executed successfully ", ClassName);
		while (loRequisitions.next())
		{
			BaseId loReqBaseID = (BaseId) loRequisitions.getBaseId(0);
			Requisition loRequisition = (Requisition)Base.getSession().objectFromId(loReqBaseID);
			BaseVector vReqLineItems = (BaseVector)loRequisition.getLineItems();
	        for (int i=0; i < vReqLineItems.size(); i++)
	        {
	            ReqLineItem oReqLineItem =(ReqLineItem) vReqLineItems.get(i);
	            POLineItem oPOLineItem = (POLineItem) oReqLineItem.getPOLineItem();
	            if(oReqLineItem.getCommodityCode() != null && oReqLineItem.getDottedFieldValue("Description.CommonCommodityCode") == null)
	            {
		            String lsPCCUniqueName = (String)oReqLineItem.getDottedFieldValue("CommodityCode.UniqueName");
		            if(lsPCCUniqueName != null && lsPCCUniqueName.endsWith("0000"))
		            {
		            	//Log.customer.debug("Class %s on method run :: variable lsPCCUniqueName is %s", ClassName, lsPCCUniqueName);
		            	//Log.customer.debug("Class %s on method run :: variable processing REQ  %s", ClassName, loRequisition.getUniqueName());
		            	oReqLineItem.setDottedFieldValue("Description.CommonCommodityCode", mo822DummyCommodity);
		            	if(oPOLineItem != null)
		            		oPOLineItem.setDottedFieldValue("Description.CommonCommodityCode", mo822DummyCommodity);
		            		//Log.customer.debug("Class %s on method run :: variable processing REQ-1  %s", ClassName, oPOLineItem.getOrderID());
		            }
		            //else if(lsPCCUniqueName != null && mCommodityMapTable.get(lsPCCUniqueName) != null){
		            else if(lsPCCUniqueName != null)
		            {
		            	CommodityCode loCommodityCode = (CommodityCode) mCommodityMapTable.get(lsPCCUniqueName);
		            	//Log.customer.debug("Class %s on method run :: variable loCommodityCode is %s", ClassName, loCommodityCode);
		            	if(loCommodityCode != null)
		            	{
		            		oReqLineItem.setDottedFieldValue("Description.CommonCommodityCode", loCommodityCode);
		            		miTransactionIndicator = miTransactionIndicator + 1;
		            		//Log.customer.debug("Class %s on method run :: variable processing REQ %s for line # %s", ClassName, loRequisition.getUniqueName(), oReqLineItem.getNumberInCollection());
		            		//Log.customer.debug("Processed REQ %s ORDER %s and Line # %s ", loRequisition.getUniqueName(), oReqLineItem.getNumberInCollection());
			            	if(oPOLineItem != null)
			            		oPOLineItem.setDottedFieldValue("Description.CommonCommodityCode", loCommodityCode);
			            		miTransactionIndicator = miTransactionIndicator + 1;
			            		//Log.customer.debug("Class %s on method run :: variable processing Order %s for line # %s", ClassName, oPOLineItem.getOrderID(), oPOLineItem.getNumberInCollection());
			            		//Log.customer.debug("Processed REQ %s ORDER %s and Line # %s ", loRequisition.getUniqueName(), oReqLineItem.getNumberInCollection());
		            	}
		            }
		            else
		            {
		            	Log.customer.debug("Not able to set CommodityCode for Requisition %s and for line # %s for PCC %s", loRequisition.getUniqueName(), oReqLineItem.getNumberInCollection(), lsPCCUniqueName );
		            	Log.customer.debug("Not able to set CommodityCode for Order %s and for line # %s for PCC %s", oPOLineItem.getOrderID(), oPOLineItem.getNumberInCollection(), lsPCCUniqueName );
		            }
	            }
	            else
	            {
	            	miUnNecessaryExec = miUnNecessaryExec + 1;
	            	//Log.customer.debug("UnNecessaryExec REQ %s ORDER %s and Line # %s ", loRequisition.getUniqueName(), oReqLineItem.getNumberInCollection());
	            }
	        }
	        if(miTransactionIndicator > 500)
	        {
	        	//Log.customer.debug("Class %s on method run :: Before commiting ", ClassName);
	        	Base.getSession().transactionCommit();
	        	Log.customer.debug("Class %s on method run :: Unnecessary executions for this commit %s ", ClassName, miUnNecessaryExec);
            	Log.customer.debug("Class %s on method run :: Transaction commit done successfully::# of lines executed %s ", ClassName, miTransactionIndicator);
            	miTransactionIndicator = 0;
            	miUnNecessaryExec = 0;
	        }
		}
	}

	public AQLResultCollection mAQLReqResults()
	{
	    moOptions = new AQLOptions(moPartition);
        moQuery = AQLQuery.parseQuery(msRequisitionQuery);
        moAQLResults = Base.getService().executeQuery(moQuery, moOptions);
        return moAQLResults;
	}

	public AQLResultCollection mAQLCommodityResults()
	{
	    moOptions = new AQLOptions(moNonePartition);
        moQuery = AQLQuery.parseQuery(msCommodityQuery);
        moAQLResults = Base.getService().executeQuery(moQuery, moOptions);
        return moAQLResults;
	}

	public CommodityCode getDummyCommodityCode()
	{
	    moOptions = new AQLOptions(moNonePartition);
        moQuery = AQLQuery.parseQuery(msDummyCommodityQuery);
        moAQLResults = Base.getService().executeQuery(moQuery, moOptions);
        while (moAQLResults.next())
        {
			BaseId loDummyCommodityBaseID = (BaseId) moAQLResults.getBaseId(0);
			mo822DummyCommodity = (CommodityCode)Base.getSession().objectFromId(loDummyCommodityBaseID);
        }
        return mo822DummyCommodity;
	}

	public Map mPutCommodityCodesInMapTable()
	{
		AQLResultCollection loCommodities = mAQLCommodityResults();
		while (loCommodities.next())
		{
			BaseId loCommodityBaseID = (BaseId) loCommodities.getBaseId(0);
			CommodityCode loCommodityCode = (CommodityCode)Base.getSession().objectFromId(loCommodityBaseID);
			String lsCommodityUniqueName = (String)loCommodityCode.getUniqueName();
			mCommodityMapTable.put(lsCommodityUniqueName, loCommodityCode);
		}
		return mCommodityMapTable;
	}
}