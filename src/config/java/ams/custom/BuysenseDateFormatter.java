 package config.java.ams.custom;

 import ariba.util.core.Assert;
 import ariba.util.core.Date;
 import ariba.util.core.Fmt;
 import ariba.util.formatter.DateFormatter;
 import ariba.util.log.Log;

 import java.util.Locale;

 public class BuysenseDateFormatter extends DateFormatter
 {
	
	   protected String formatObject(Object object, Locale locale)
	   {
	     Assert.that(object instanceof Date, "invalid type");
	     Log.customer.debug("Date format is "+toJavaFormat ((Date)object,locale));
	     String date = toJavaFormat ((Date)object,locale);
	 
	     return Fmt.S("%s", date.toString());
	   }

 }
