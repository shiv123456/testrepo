package config.java.ams.custom;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.common.core.Supplier;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.user.core.User;
import ariba.util.core.PropertyTable;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseSetReqLineFields extends Action
{
    private String       queryString                 = null;
    private final String sClass                      = "BuysenseSetReqLineFields";
    private final String errorStringTable            = "ariba.procure.core";
    private final String sNotonContractUniqueNameKey = "NotonContractUniqueName";
    private final String sNotonContractUniqueName    = ResourceService.getString(errorStringTable,
                                                             sNotonContractUniqueNameKey);

    public void fire(final ValueSource object, final PropertyTable propertytable) throws ActionExecutionException
    {
        Log.customer.debug(sClass + " start ");
        ReqLineItem loReqLineItem = null;
        if (object instanceof ReqLineItem)
        {
            loReqLineItem = (ReqLineItem) object;
            AQLResultCollection aqlResults = null;
            aqlResults = getResults(loReqLineItem);
            if (aqlResults != null)
            {
                if (aqlResults.getErrors() != null)
                {
                    Log.customer.debug(sClass + " Errors in results: " + aqlResults.getErrors());
                    Log.customer.debug(sClass + " Errors in results: " + aqlResults.getErrors());
                    return;
                }
                else
                {
                    Log.customer.debug("aqlResults size: " + aqlResults.getSize());
                    try
                    {
                        if (aqlResults.isEmpty())
                        {
                            Log.customer.debug(sClass + " empty results");
                            final ClusterRoot noContract = Base.getService().objectMatchingUniqueName(
                                    "ariba.core.BuysenseContracts", Base.getService().getPartition("pcsv"),
                                    sNotonContractUniqueName);
                            loReqLineItem.setFieldValue("ContractNumList", noContract);
                        }
                        else if (!aqlResults.isEmpty())
                        {
                            Log.customer.debug(sClass + " non-empty results");
                            loReqLineItem.setFieldValue("ContractNumList", null);
                        }
                    }
                    catch (final Exception e)
                    {
                        Log.customer.debug(sClass + " Exception: " + e);
                    }
                }
            }

        }
    }

    private AQLResultCollection getResults(final ReqLineItem reqLI)
    {
        try
        {
            if (reqLI != null)
            {
                final Supplier supplier = reqLI.getSupplier();
                if (supplier != null)
                {

                    String supplierUniqueName = supplier.getUniqueName();
                    queryString = "SELECT s FROM ariba.core.BuysenseContracts s where s.SupplierID ='"
                            + supplierUniqueName + "'";
                    final Requisition req = (Requisition) reqLI.getLineItemCollection();
                    // getLineItemCollection returns null in case of EXT orders.
                    // If it is 'null' we just don't process anything else. 
                    // As a result ContractNumList will always be set to null for EXT orders
                    if(req == null)
                    {
                        return null;
                    }
                    final User requester = req.getRequester();
                    final ariba.common.core.User partitionedUser = ariba.common.core.User.getPartitionedUser(requester,
                            req.getPartition());
                    final String sClinetUniqueName = (String) partitionedUser
                            .getDottedFieldValue("ClientName.ClientName");

                    if (!StringUtil.nullOrEmptyString(sClinetUniqueName))
                    {
                        queryString += " AND s.AuthorizedAgency in ('" + sClinetUniqueName.substring(0, 4) + "')";
                    }
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    df.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
                    String lsDateTime = df.format(Calendar.getInstance().getTime());
                    lsDateTime = lsDateTime +" 00:00:00";
                    queryString += " AND s.EffectiveBeginDate <= CurrentDate()";
                    queryString += " AND s.EffectiveEndDate >= CalendarDate('" + lsDateTime + "')";
                    final AQLQuery aqlQuery = AQLQuery.parseQuery(queryString);
                    Log.customer.debug(sClass + " Query- " + aqlQuery);
                    final AQLOptions aqlOptions = new AQLOptions(req.getPartition());
                    final AQLResultCollection results = Base.getService().executeQuery(aqlQuery, aqlOptions);
                    return results;

                }
            }
        }
        catch (Exception e)
        {
            Log.customer.debug(sClass + " Excecption in aqlResults(): " + e.getMessage());
        }
        return null;
    }

}
