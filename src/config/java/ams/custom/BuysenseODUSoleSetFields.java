package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseODUSoleSetFields extends Action
{
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        Log.customer.debug("fire method called");
        Approvable loAppr = null;        
        if(valuesource!= null && valuesource instanceof Approvable)
        {
            loAppr = (Approvable)valuesource;                    
            loAppr.setDottedFieldValue("OnlyMeetDeptNeed"," ");
            loAppr.setDottedFieldValue("OnlySource"," ");
            loAppr.setDottedFieldValue("Note","Requirements 1,2, and 3 must be met prior to contract being negotiated by Procurement Services. It is the responsibility of Procurement Services to determine price reasonableness and negotiate and issue a contract that is in the best interest of Old Dominion University. Procurement Services may include the department  in negotiations.");
        }
    }
}
