/*
    Copyright (c) 1996-1999 Ariba/Buysense, Inc.
   Anup November 2000
    ---------------------------------------------------------------------------------------------------

*/

/* 09/03/2003: Updates for Ariba 8.0 (David Chamberlain)
   Replaced All Util.vector()'s with ListUtil.vector()
   Replaced All Util.getInteger()'s with Constants.getInteger() */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */   

package config.java.ams.custom;
// Ariba 8.0: Replaced deprecated ApprovableHook
import ariba.approvable.core.ApprovableHook;
//import ariba.approvable.core.ApprovableHook;
//import ariba.util.core.Util;
import java.util.List;
import ariba.approvable.core.Approvable;
import ariba.base.core.BaseVector;
import ariba.purchasing.core.POLineItem;
import ariba.base.core.BaseObject;
import java.math.BigDecimal;
import ariba.base.core.BaseId;
import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.util.log.Log;
import ariba.base.core.BaseSession;
// Ariba 8.0: Deprecations, methods have changed packages.
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;

public class AMSPaymentWithdrawHook implements ApprovableHook
{
    // Ariba 8.0: Replaced deprecated method
    // 81->822 changed Vector to List
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));
    public List run (Approvable approvable)
    {
        
        // Lock the order that is being withdrawn
        Log.customer.debug("Lock the order whose baseid is %s",
                       approvable.getFieldValue("Order"));
        ClusterRoot order=(ClusterRoot)approvable.getFieldValue("Order");
        BaseId refBaseId = (BaseId)order.getBaseId();
        BaseSession baseSession = Base.getSession();
        order = baseSession.objectForWrite(refBaseId);        
        Log.customer.debug("Finished locking the order");
        
        // Get the Line items vector on the locked order and on the payment 
        BaseVector polines = (BaseVector)order.getFieldValue("LineItems");
        BaseVector pvlines = (BaseVector)approvable.getFieldValue("PaymentItems");
        
        // Update NumberInPaymentProcess for Payment and Order
        for(int i=0;i<polines.size();i++)
        {
            
            POLineItem item = (POLineItem)polines.get(i);
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            BaseObject paymentItem = (BaseObject)pvlines.get(i);
            BigDecimal payingQty = (BigDecimal)paymentItem.getFieldValue("CurPayingQuantity");
            BigDecimal inProcessQty = (BigDecimal)item.getFieldValue("NumberInPaymentProcess");
            // Ariba 8.0: Replaced deprecated method
            if (inProcessQty.compareTo(payingQty) >= 0)
            {
                item.setFieldValue("NumberInPaymentProcess",
                                   inProcessQty.subtract(payingQty));
            }
            else
                return ListUtil.list(Constants.getInteger(-1),
                                         "Cannot reduce quantity in process as current paying quantity is too high");
            
        }
        
        order.save();
        
        //Anup - Generic invocation of dynamically determined Client/Agency edits. - Dev SPL #173 
        List result = EditClassGenerator.invokeHookEdits(approvable,
                                                           this.getClass().getName());
        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
        int errorCode = ((Integer)result.get(0)).intValue();
        Log.customer.debug("Validations in " + this.getClass().getName() + ", error Code --> %s",errorCode); 
        if(errorCode<0)
        {
            Log.customer.debug("Validations Failed in " + this.getClass().getName() + " - returning error to UI"); 
            return result;
        }
        
        return NoErrorResult;
    }
    
    
}
