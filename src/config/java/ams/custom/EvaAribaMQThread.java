package config.java.ams.custom;

import ariba.util.log.Log;

public class EvaAribaMQThread extends Thread
{
    private static String msCN = "EvaAribaMQThread";
    private final long waitingtime = 120000; // 2 mins

    public EvaAribaMQThread()
    {
    }

    public void run()
    {
        EvaSendMessageAribaImpl mqAribaImpleMessg = null;

        while (true)
        {
            while (!EvaAribaMQManager.isMQMessQueueEmpty())
            {
                // Retrieve next message and send it out
                mqAribaImpleMessg = EvaAribaMQManager.getMessage();
                try
                {
                    mqAribaImpleMessg.initializeCamel();
                    Log.customer.debug(msCN + ": starting to send message to MQ");
                    mqAribaImpleMessg.publishMessageToQueue(mqAribaImpleMessg.getMQMessg());
                    Log.customer.debug(msCN + ": sent message to MQ");
                }
                catch (Exception e)
                {
                    // keep going even on exception
                    Log.customer.warning(7100, msCN + ": E2QQ Send Req to QQ ERROR - "+ mqAribaImpleMessg.getMQMessg().getObjID() +" Exception sending message to MQ" + e.getMessage());
                }
            }
            try
            {
                Thread.sleep(waitingtime); // wait after the queue is empty
            }
            catch (Exception e)
            {
                // keep going even on exception
                Log.customer.warning(7100,msCN + ": E2QQ Send Req to QQ ERROR - "+mqAribaImpleMessg.getMQMessg().getObjID()+" Exception sending message to MQ" + e.getMessage());
            }

        }
    }

}
