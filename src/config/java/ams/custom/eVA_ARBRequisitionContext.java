package config.java.ams.custom;

import ariba.htmlui.procure.wizards.ARBRequisitionContext;
import ariba.htmlui.procure.wizards.ARBRequisitionController;

public class eVA_ARBRequisitionContext extends ARBRequisitionContext
{
    public String getAfterAddItemFrame ()
    {
    	String sReturnFrame = super.getAfterAddItemFrame();//Should NEVER return null value;
    	if (sReturnFrame.equals(ARBRequisitionController.ReviewFrameName)) return "AddItemFrame";//Override OOTB to review Accounting frame.
    	else return sReturnFrame;
    }
}