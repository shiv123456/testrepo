/************************************************************************************
 * Author:  David Chamberlain
 * Date:    August 01, 2005
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 07/30/2004        David Chamberlain      Initial VEPI PROD SPL 801

 *
 * @(#)BuysenseSetHoldTillToNull.java     1.0 08/24/2004
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/

package config.java.ams.custom;

import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.purchasing.core.Requisition;
import ariba.base.fields.Action;
import ariba.base.fields.*;

public class BuysenseSetHoldTillToNull extends Action
{
    public void fire (ValueSource object, PropertyTable parameters) throws ActionExecutionException
    {
        Log.customer.debug("* Calling BBuysenseSetHoldTillToNull *");
        Requisition loReq = (Requisition) object;
        //ariba.util.core.Date ldHoldTillDate = (ariba.util.core.Date) loReq.getFieldValue("HoldTillDate");
        loReq.setFieldValue("HoldTillDate", null);

        Log.customer.debug("* Leaving BBuysenseSetHoldTillToNull *");
        return;
    }
}