/**
This batch program is designed to query all receipt of the Ariba OM to a specific agency,
which not yet been paid to create payment documents in an ERP system.

This program is designed to run in a nightly cycle or on demand.
pbreuss, AMS, March 2000
*/

/* 09/05/2003: Updates for Ariba 8.0 (Richard Lee) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/

package config.java.ams.custom;

import ariba.receiving.core.Receipt;
import ariba.base.core.BaseId;
import ariba.util.log.Log;
//import ariba.common.util.*;
import ariba.base.core.Partition;
//import ariba.common.base.*;
import java.util.List;
import java.util.Map;
//Ariba 8.0: added import scheduler
import ariba.util.scheduler.*;
import ariba.base.core.Base;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
//Ariba 8.1: imported ListUtil
import ariba.util.core.ListUtil;

//Ariba 8.0: Replaced SimpleScheduledTask with ScheduledTask
// 81->822 changed Vector to List
// 81->822 changed Hashtable to Map
public class AMSPushPayment extends ScheduledTask
{
    //Partition m_partition;
    private Partition m_partition = null;

    public AMSPushPayment ()
    {
    }
    /**
    This init function get automatoically invoked by Ariba on system start-up time.
    */
    public void init(Scheduler scheduler,
                     String scheduledTaskName, Map arguments)
    {
        Log.customer.debug("AMSPushPayment init was called...!!!");




		//rgiesen dev SPL #39
        //m_partition = Base.getSession().getPartition();
        m_partition = Base.getSession().getPartition();
        //m_partition = partition;
        // Note: pb051500 init was moved to scheduled task AMSLoadXMLData
    }

    /**
    In this scheduled talk, we are querying the Arina OM for PO that have been received (ReceivedState=4)
    and have not yet been paid (PaidState=1)
    All PO that match these criteria are put in a vector and passed to the PV Splitter.
    */
    public void run() throws ScheduledTaskException
    {
        System.out.println("Start PushPayment.run()");
        Log.customer.debug("AMSPushPayment run was called...!!!");

        // setting up fields to return
        // Ariba 8.1: List::List() is deprecated by ListUtil::newVector()
        //List fieldsToReturn = new List();
        List fieldsToReturn = ListUtil.list();

        // setting up constraints
        //Ariba 8.0: FieldConstraint deprecated
        //List userConstraints = new List();

        /* Ariba 8.0: FieldConstraint deprecated
        userConstraints.addElement(
        FieldConstraint.expression(
            "PaidState",
            FieldConstraint.NOT_EQUAL,
            "2"));
        */

        // Ariba 8.0: added to replace FieldConstraint and ObjectsMatching
        String lsSQLStatement = "Select UniqueName from DirectOrder where PaidState <> 2";

        //rgiesen dev SPL #39
        //AQLOptions loOptions = new AQLOptions(Base.getSession().getPartition());
        AQLOptions loOptions = new AQLOptions(m_partition);

        AQLQuery loQuery = AQLQuery.parseQuery(lsSQLStatement);
        AQLResultCollection OrderIDofReceiptsToBePaid = Base.getService().executeQuery(loQuery, loOptions);

        // select all Receipts PaidState <> 2 (not paid yet)
        //Ariba 8.0: objectsMatching deprecated
        /*
        List OrderIDofReceiptsToBePaid = Core.getService().objectsMatching(
        Receipt.ClassName,
        Partition.AnyVector,
        userConstraints,
        fieldsToReturn,
        null,                                   //User.KeyUniqueName,
        0,
        true,
        false);
        */

        //int numberPOsReturned = OrderIDofReceiptsToBePaid.size();
        int numberPOsReturned = OrderIDofReceiptsToBePaid.getSize();
        Log.customer.debug(numberPOsReturned+" records were retrieved");

        // TODO deal with PaymentByAgency flag from parameter.table
        //  boolean paymentByAgency = Core.service().booleanParameter(m_partition, "Application.AMSParameters.PaymentByAgency");
        //System.out.println("Warning: No flag PaymentByAgency in parameter.table"+paymentByAgency);

        // TODO deal with Agency flag on each Payment

        // Ariba 8.1: List::List() is deprecated by ListUtil::newVector()
        //List ReceiptsToBePaid = new List();
        List ReceiptsToBePaid = ListUtil.list();

        //rlee might try while (OrderIDofReceiptsToBePaid.next())
        for (int j=0;j<numberPOsReturned;j++)
        {
            Object[] row = (Object[])OrderIDofReceiptsToBePaid.getObject(j);
            //Ariba 8.0: see above
            //Object[] row = (Object[])OrderIDofReceiptsToBePaid.elementAt(j);
            BaseId rootid = (BaseId)row[0];

            // retrieve receipt
            Receipt receipt = (Receipt)rootid.get();

            //IXM 07/13/00 Not sure whether we need this call R7. Reoded it for now
            // Weird call: this call is apparently necessary to remove the lock
            //ariba.approvable.core.LineItemCollection pol = (ariba.approvable.core.LineItemCollection)receipt.getLineItemCollection();

            ariba.base.core.BaseVector bv = (ariba.base.core.BaseVector) receipt.getReceiptItems();

            // add receit to vector
            // Ariba 8.1: List::addElement() is deprecated by List::add()
            ReceiptsToBePaid.add(receipt);
        }

        Log.customer.debug("Number of vectors in ReceiptsToBePaid: "+ReceiptsToBePaid.size());


        // At this point we have all POs to be paid in the vector ReceiptsToBePaid

        // Now we call the PV Splitter to create PV per Supplier)
        int returnCode = AMSPVSplitter.split(ReceiptsToBePaid, m_partition);
        // TODO deal with error code
        // TODO update paid date
        System.out.println("End PushPayment.run()");
    }

}
