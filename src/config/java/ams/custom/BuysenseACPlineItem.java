package config.java.ams.custom;

import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.fields.Condition;
import ariba.base.fields.ValueInfo;
import ariba.purchasing.core.ReqLineItem;
import ariba.util.core.PropertyTable;
import ariba.workforce.core.Log;

/**
 *  Author			: Sarath Babu Garre.
 *  Date 			: 07-Dec-2009
 *  CSPL           	: 1471
 *  ACP Requirement	: Condition to validate whether the line item being created 
 *                    is a Collaborative line item or normal line item. 
 *  
**/

public class BuysenseACPlineItem extends Condition
{
	private static final ValueInfo parameterInfo[];
	public boolean evaluate(Object obj, PropertyTable params) 
	{		
		BaseObject baseobj  = (BaseObject)params.getPropertyForKey("SourceObject");
		Log.customer.debug("inside BuysenseACPlineItem baseObject"+baseobj);
		if(baseobj instanceof ReqLineItem)
		{			
			ReqLineItem reqLine = (ReqLineItem)baseobj;
			BaseVector categoryLineItemDetailsVector =(BaseVector)reqLine.getCategoryLineItemDetailsVector();
			if(categoryLineItemDetailsVector.isEmpty())			
			{				
				return false;
			}
			else
			{				
				return true;
			}
		}
		return false;
	}
	protected ValueInfo[] getParameterInfo()
	 {
		 return parameterInfo;
	 }
	 static
    {
		 parameterInfo = (new ValueInfo[] {
        new ValueInfo("SourceObject", 0)
        });
	 }
}
