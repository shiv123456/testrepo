package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.fields.Behavior;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.pcard.core.PCardOrder;
import ariba.procure.core.ProcureLineItem;
import ariba.purchasing.core.PurchaseOrder;
import ariba.purchasing.core.Requisition;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseCommodityCodeVisibility extends Condition{
	
	private static final ValueInfo parameterInfo[];

    public boolean evaluate(Object obj, PropertyTable params) throws ConditionEvaluationException
    {
        Log.customer.debug("Inside BuysenseCommodityCodeVisibility" + obj);

        return isCommodityCodeVisible((ProcureLineItem)obj);

    }

    private boolean isCommodityCodeVisible(ProcureLineItem loPli)
    {
        if (loPli != null && loPli instanceof ProcureLineItem)
        {
            Log.customer.debug("isCommodityCodeVisible: Inside Product line item" + loPli);
            boolean bisAdHoc = (Boolean) loPli.getFieldValue("IsAdHoc");
            Approvable loAppr = (Approvable) loPli.getLineItemCollection();
            if(loAppr != null && loAppr instanceof Requisition)
            {
                Log.customer.debug("isCommodityCodeVisible:Inside Requisition now " + loAppr);
                String transactionSource = (String) loAppr.getFieldValue("TransactionSource");
                boolean bBypassApprovers = (Boolean) loAppr.getFieldValue("BypassApprovers");
                if ((transactionSource == null && bisAdHoc)||(!StringUtil.nullOrEmptyOrBlankString(transactionSource)) && transactionSource.equals("EXT")&& !bBypassApprovers )
                {
                    Log.customer
                		.debug("isCommodityCodeVisible :Transaction source null and Adhoc line found"
                                    + loPli);
                    return false;
                }
            }
          
        }
        return true;
    }
    
       

    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    static
    {
        parameterInfo = (new ValueInfo[] { new ValueInfo("SourceObject", 0),
        		new ValueInfo("Message", 0, Behavior.StringClass)});
    }


}


