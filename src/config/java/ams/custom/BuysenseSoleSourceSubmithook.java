package config.java.ams.custom;

import java.util.List;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableHook;
import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;
import ariba.util.log.Log;

public class BuysenseSoleSourceSubmithook implements ApprovableHook
{
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));
    public List run(Approvable approvable)
    {
        String lsAgencyNum = (String)approvable.getFieldValue("AgencyRequestNum");
        Log.customer.debug("AgencyRequestNum  "+lsAgencyNum);
        String lsUniqueNameNum = (String)approvable.getFieldValue("UniqueName");
        String queryString = "select AgencyRequestNum,UniqueName from ariba.core.BuysenseSoleSourceRequest where AgencyRequestNum = '"+lsAgencyNum +"' and UniqueName != '"+lsUniqueNameNum+"'";
        AQLQuery aqlQuery = AQLQuery.parseQuery(queryString);
        Log.customer.debug(" Query- " + aqlQuery);
        Partition partition = Base.getService().getPartition("pcsv");
        AQLOptions option = new AQLOptions(partition);
        AQLResultCollection aqlResults = Base.getService().executeQuery(aqlQuery,option);
        if(aqlResults.getSize()>0 && aqlResults.next())
        {
            String lsUniqueName=aqlResults.getString("UniqueName");
            return ListUtil.list(Constants.getInteger(-1),
                    "The Agency Request Number "+lsAgencyNum+ " has already been used on a previous Sole Source Request ("+lsUniqueName+") for your Agency. Please enter a different value.");
        }
        else
        {
            return NoErrorResult;
        }
    }
}
