package config.java.ams.custom;

/**
 * CSPL #: 4149-Datafix
 * @author Pavan Aluri
 * Date  : 10-Apr-2012
 * @version 1.0
 * Explanation: This is one time task to update DRProposalTotal custom filed in Contractor labor lines for all Proposal documents
 */

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.basic.core.Money;
import ariba.collaboration.core.CollaborationLineItem;
import ariba.collaboration.core.CollaborationLineItemDetails;
import ariba.collaboration.core.Proposal;
import ariba.util.log.Log;
import ariba.util.scheduler.ScheduledTask;
import ariba.util.scheduler.Scheduler;
import ariba.workforce.core.ContractorCandidate;
import ariba.workforce.core.LaborCollaborationLineItemDetails;
import ariba.workforce.core.LaborLineItemDetails;

public class BuysenseSetDRProposalTotal extends ScheduledTask
{
	   private String msClassName = this.getClass().getName();

	   public void init(Scheduler scheduler, String scheduledTaskName, Map arguments)
		{
		   Log.customer.debug("%s - init", msClassName);
		   super.init(scheduler, scheduledTaskName, arguments);
		}
	   
	   public void run()
	   {
		   Log.customer.debug("%s - run", msClassName);
		   AQLResultCollection loProposalResults = getAllProposalstoFix();
		   processProposalDoc(loProposalResults);
		   Log.customer.debug("%s - finished run", msClassName);
	   }

	private void processProposalDoc(AQLResultCollection loProposalResults)
	{
		  Log.customer.debug("%s - processing proposal ", msClassName);
		   CollaborationLineItem loCollabLine = null;
		   CollaborationLineItemDetails loCollabLineDetails = null;
		   LaborCollaborationLineItemDetails lcld = null;
		   ContractorCandidate loContractor = null;
		   LaborLineItemDetails loContractorLL = null;
		   Money loPropTotal = null;
		   Proposal loProposalToFix = null;
		   BaseId bi = null;
	       if (loProposalResults != null) 
	       {
	    	   try
	    	   {
		    	   while (loProposalResults.next())
		           {
		    		   try
		    		   {
		    			   bi = (ariba.base.core.BaseId)loProposalResults.getBaseId(0);
						   loProposalToFix = (Proposal) bi.get();
		    		   }catch(Exception e)
		    		   {
		    			   Log.customer.warning(8000,msClassName+" - Exception in getting Proposal object for BaseId "+ bi+", skipping it");
		    			   continue;
		    		   }
		    		   if (loProposalToFix != null)
		    		   {
		    		   Log.customer.debug("%s - Processing proposal lines for %s", msClassName,loProposalToFix );
		    		   List loCollabLines = loProposalToFix.getLineItems();	
			   		   for (int i=0; i<loCollabLines.size(); i++)
					   {
						   loCollabLine = (CollaborationLineItem) loCollabLines.get(i);
						   loCollabLineDetails = loCollabLine.getCollaborationLineItemDetails();
						   if (loCollabLineDetails instanceof LaborCollaborationLineItemDetails)
						   {
							   lcld = (LaborCollaborationLineItemDetails)loCollabLineDetails;
							   for (Iterator e = lcld.getContractorCandidatesIterator(); e.hasNext();)
							   {
								   loContractor = (ContractorCandidate) e.next();
								   if (loContractor != null && loContractor.getLaborLineItemDetails() != null)
								   {
									   loContractorLL = loContractor.getLaborLineItemDetails();
									   loPropTotal = loContractorLL.getProposalTotal();
									   if(loPropTotal != null)
									   {
										   Log.customer.debug("%s - setting Proposal Total to %s", msClassName,loPropTotal.getAmount());
										   loContractorLL.setDottedFieldValue("DRProposalTotal", loPropTotal.getAmount());
									   }else
									   {
										   Log.customer.debug("%s - for some reasons, got null for Proposal Total", msClassName);
									   }										   
								   }
								   else
								   {
									   Log.customer.debug("%s - ContractorCandidate or its LaborLineItem is null ", msClassName);
								   }
							   }
						   }else
						   {
							   Log.customer.debug("%s - looks like this is not a labor line", msClassName);   
						   }
					   }
		           }
		           }   
	    	   }catch(Exception e)
	    	   {
	    		   Log.customer.warning(8000," Exception in "+ msClassName +" runmethod -" + e.getMessage());
	    	   }
	       }else
	       {
	    	   Log.customer.debug("%s - No Objects to fix", msClassName );
	    	   return;
	       }
	}

	private AQLResultCollection getAllProposalstoFix()
	{
		   Partition pcsvPartition = Base.getService().getPartition("pcsv");
		   AQLOptions loOptions = new AQLOptions(pcsvPartition);
		   String sQuery = "SELECT DISTINCT Proposal FROM ariba.collaboration.core.Proposal INCLUDE INACTIVE " +
		   		"JOIN ariba.collaboration.core.CollaborationLineItem AS LineItems USING Proposal.LineItems " +
		   		"WHERE " +
		   		"LineItems.Description.UpdatedCatalogItemType <> 'system:consultinglabor' ORDER by UniqueName desc ";		   
		   try{
			   AQLQuery loQuery = AQLQuery.parseQuery(sQuery);
			   Log.customer.debug("%s - executing query -%s", msClassName,loQuery);
			   AQLResultCollection loResults = Base.getService().executeQuery(loQuery, loOptions);
			   if (loResults.getFirstError() != null)
			   {
				   Log.customer.warning(8000,"%s - error in AQL results - %s", msClassName,loResults.getFirstError().toString());
				   return null;	   
			   }else
			   {
				   return loResults;
			   }
		   }catch(Exception e)
		   {
			   Log.customer.warning(8000,"%s - Exception in AQL execution -%s", msClassName, e.getMessage());
			   return null;
		   }
	}
}
