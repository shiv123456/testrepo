package config.java.ams.custom;

import ariba.base.core.BaseObject;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.purchasing.core.ReqLineItem;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseEnableReceivingType extends Condition
{
	private static final ValueInfo parameterInfo[];
	
	public boolean evaluate(Object value, PropertyTable params) throws ConditionEvaluationException
    {
		BaseObject loIncomingObject  = (BaseObject)params.getPropertyForKey("SourceObject");
		Log.customer.debug("BuysenseEnableReceivingType::passed loIncomingObject %s", loIncomingObject);
        if(loIncomingObject != null && loIncomingObject instanceof ReqLineItem)
        {
        	ReqLineItem loReqLineItem = (ReqLineItem) loIncomingObject;
        	
        	// Comment or remove below logs after unit testing 
        	Log.customer.debug("BuysenseEnableReceivingType:: loReqLineItem.getAmountAccepted() %s", loReqLineItem.getAmountAccepted());
        	Log.customer.debug("BuysenseEnableReceivingType:: loReqLineItem.getAmountRejected() %s", loReqLineItem.getAmountRejected());
        	Log.customer.debug("BuysenseEnableReceivingType:: loReqLineItem.getNumberAccepted() %s", loReqLineItem.getNumberAccepted());
        	Log.customer.debug("BuysenseEnableReceivingType:: loReqLineItem.getNumberRejected() %s", loReqLineItem.getNumberRejected());

        	if((loReqLineItem.getAmountAccepted() != null && loReqLineItem.getAmountAccepted().getAmount().intValue() != 0) 
        			|| (loReqLineItem.getAmountRejected() != null && loReqLineItem.getAmountRejected().getAmount().intValue() != 0)
        			|| (loReqLineItem.getNumberAccepted() != null && loReqLineItem.getNumberAccepted().intValue() != 0)
        			|| (loReqLineItem.getNumberRejected() != null && loReqLineItem.getNumberRejected().intValue() != 0))
        	{
        		Log.customer.debug("BuysenseEnableReceivingType::returning false, disable BuysenseReceivingTypeField");
        		return false;
        	}
        }
        Log.customer.debug("BuysenseEnableReceivingType::returning true, enable BuysenseReceivingTypeField");
        return true;
    }

	protected ValueInfo[] getParameterInfo()
	 {
		 return parameterInfo;
	 }
	
	static
   {
		 parameterInfo = (new ValueInfo[] {
       new ValueInfo("SourceObject", 0)
       });
	 }

}
