package config.java.ams.custom;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Level;
import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.receiving.core.Receipt;
import ariba.receiving.core.ReceiptItem;
import ariba.user.core.Permission;
import ariba.util.log.Log;
import ariba.util.scheduler.ScheduledTask;
import ariba.util.scheduler.Scheduler;

public class BuysenseImportExternalReceipts extends ScheduledTask implements BuyintConstants
{
	private static String msCN = "BuysenseImportExternalReceipts";
    private static Partition moPart = Base.getSession().getPartition();
    private static DocumentBuilderFactory moDOMFactory;
    private BuyintDBRecExportData moRecords[] = null;
    private BuyintDBRecExportData moDbRec = null;
    private String msDBPassword = "";
    private String msDBURL = "";
    private String msDBUser = "";
    private String msTable = "";
    private BuysenseERPReceiptImport moXMLImporter = new BuysenseERPReceiptImport();
    private int miRetryThreshold = RETRY_DEFAULT ;
    private int miMaxRecords = MAX_TRANSACTIONS_DEFAULT ;
    private String msFailedTXNPermission = "";
    
    public void run()
    {
    	Log.customer.debug("***%s***Called run", msCN);

        try{
            /* Build the SQL string and get response for processing. 
             * Construct SQL based on the table definition*/
            String lsSQL = "SELECT * FROM (SELECT TIN, STATUS, ATTEMPTS, IMPORT_DATA, ERRORS, PROCESSED_PO_NUMBER, RC_NUMBER FROM " + msTable + " " +
            				"WHERE ((STATUS = 'NEW' OR STATUS = 'RTRY') " +
            				"AND ((PROCESS_DATE <= SYSDATE) OR (PROCESS_DATE IS NULL )))" +
            				"ORDER BY PRIORITY, STATUS, CREATION_DATE ASC, " +
            				"PO_NUMBER, NVL(PO_VERSION, 1))" +
            				"WHERE ROWNUM <= " + miMaxRecords;

            moRecords = moDbRec.getRecs(lsSQL);
            String lsTIN = "";
            String lsXML = new String();
            InputStream tempIS = null;

            if (moRecords == null)
            {
                return;
            }
            
            /* Start processing Receipts */
            for (int i = 0; i < moRecords.length; i++)
            {
                try
                {
                    lsXML = moRecords[i].getImportData();
                    if (lsXML == null)
                    {
                       throw new BuysenseXMLImportException("XML is not present in Receiving Import.");
                    }
                    else if (lsXML.trim().length() < 1)
                    {
                       throw new BuysenseXMLImportException("XML is not present in Receiving Import");
                    }
                    else
                    {
                       lsXML.trim();
                    }
                    byte[] moBytes = (byte[]) lsXML.getBytes();
                    tempIS = new ByteArrayInputStream(moBytes);
                    moXMLImporter.importFromXML(tempIS);
               		moRecords[i].setStatus(TXNTYPE_DONE);
               		moRecords[i].setReceiptNumber(BuysenseERPReceiptImport.msRCNumber);
               		moRecords[i].setProcessedPONumber(BuysenseERPReceiptImport.msPONumber);
                    moRecords[i].setAttempts(moRecords[i].getAttempts());
                    moRecords[i].saveRecord(msTable);
                }
                catch (BuysenseXMLImportException ex)
                {
                    Log.customer.debug("***%s***::run::Inside for loop::BuysenseXMLImportException %s", msCN, ex.getMessage());
                    try {
                        /* Clear out the updated values through Receipt Import in case of failure */
                    	clearReceiptImportValues();
                    	updateFailedStatus( moRecords[i], ex);
                    }
                    catch ( Exception loEx)
                    {
                        if (lsTIN.length() > 0)
                        {
                            lsTIN += (", " + moRecords[i].getTINString());
                        }
                        else
                        {
                            lsTIN = "" + moRecords[i].getTINString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                    Log.customer.debug("***%s***::run::Inside for loop::Exception %s", msCN, ex.getMessage());                    
                    try {
                    	clearReceiptImportValues();
                    	updateFailedStatus( moRecords[i], ex);
                    }
                    catch ( Exception loEx)
                    {
                        /* Do nothing; TIN will be updated */
                    }
                    if (lsTIN.length() > 0)
                    {
                        lsTIN += (", " + moRecords[i].getTINString());
                    }
                    else
                    {
                        lsTIN = "" + moRecords[i].getTINString();
                    }
                }
            }
            if (lsTIN != null)
            {
               if (lsTIN.length() > 0)
               {
                   logError ("Buyint:error: Failed to import Receipt for the following TINs: " + lsTIN);
               }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Log.customer.debug("***%s***::run::Outside for loop::processException %s", msCN, ex.getMessage());              
        }
    }

    private void logError(String fsMessage)
    {
        Logs.buysense.setLevel(Level.DEBUG);
        Logs.buysense.debug(fsMessage);
        Logs.buysense.setLevel(Level.DEBUG.OFF);
    }

    private void updateFailedStatus ( BuyintDBRecExportData foRecord, Exception loEx) throws Exception
    {
        try
        {
        	foRecord.setReceiptNumber(BuysenseERPReceiptImport.msRCNumber);
        	foRecord.setProcessedPONumber(BuysenseERPReceiptImport.msPONumber);
        	int liRetryCnt = foRecord.getAttempts();
            Log.customer.debug("***%s***::updateFailedStatus::liRetryCnt %s", msCN, liRetryCnt);

            String lsErrorMessage = (loEx.getMessage()).trim();
            boolean lboolSkipRetry = false;

            if (loEx instanceof BuysenseXMLImportException)
            {
               if (((BuysenseXMLImportException)loEx).getErrorCode() == ERR_CODE_SKIP_RETRY)
               {
                  lboolSkipRetry = true;
               }
            }

            if(liRetryCnt == 0)
            {
                foRecord.setErrors(lsErrorMessage);
            }
            else
            {
                foRecord.setErrors(foRecord.getErrors()+ "; " + lsErrorMessage);
            }

            if ( (liRetryCnt + 1) >= miRetryThreshold || lboolSkipRetry )
            {
                Log.customer.debug("***%s***::updateFailedStatus:: %s failed after max retry attempts.", msCN, foRecord.getTINString());
                foRecord.setStatus("ERR");
                String lsEmailSubject = "BuysenseImportExternalReceipts task failed to process TIN: " + foRecord.getTINString();
                String lsEmailBody = "Receipt " + foRecord.getReceiptNumber() + " with PO Number " + foRecord.getProcessedPONumber() + " could not be processed. "
                                     + "Receiving Import encountered " + lsErrorMessage + ". Please resubmit.";
                sendEmail(lsEmailBody, lsEmailSubject);
            }
            else
            {
            	foRecord.setStatus("RTRY");
            }
            ++liRetryCnt;
            foRecord.setAttempts(liRetryCnt);
            foRecord.saveRecord(msTable);
            Log.customer.debug("***%s***::updateFailedStatus::Record Saved Successfully to DB", msCN);

        }
        catch ( Exception ex)
        {
            throw ex;
        }
    }

    private void sendEmail(String fsEmailBody, String fsEmailSubject)
    {
        Permission pa = Permission.getPermission(msFailedTXNPermission);
        if (pa == null)
        {
            Log.customer.debug("***%s***::sendEmail:: Could not retrieve permission %s.", msCN, msFailedTXNPermission);
            return;
        }

        List users = pa.getAllUsers();
        if(users == null || users.isEmpty())
        {
            Log.customer.debug("***%s***::sendEmail:: No users having permission %s.", msCN, msFailedTXNPermission);
            return;
        }
        Log.customer.debug("***%s***::sendEmail:: Sending email to users %s.", msCN, users.size());
        BuysenseEmailAdapter.sendMail(users, fsEmailSubject, fsEmailBody);
    }

    public void init(Scheduler scheduler, String scheduledTaskName, Map arguments)
    {
        super.init(scheduler, scheduledTaskName, arguments);
        String lsKey = null;

        if (moPart == null)
        {
            moPart = Base.getService().getPartition("pcsv");
        }

        for (Iterator loItr = arguments.keySet().iterator(); loItr.hasNext();)
        {
           lsKey = (String)loItr.next() ;

           if(lsKey.equals("DBURL"))
           {
        	   msDBURL = (String)arguments.get(lsKey);
           }
           else if(lsKey.equals("DBUser"))
           {
        	   msDBUser = (String)arguments.get(lsKey);
           }
           else if(lsKey.equals("DBPassword"))
           {
        	   msDBPassword = (String)arguments.get(lsKey);
           }
           else if(lsKey.equals("ImportDataTable"))
           {
        	   msTable = (String)arguments.get(lsKey);
           }
           else if(lsKey.equals("MaxTransactions"))
           {
              String lsMax = (String)arguments.get(lsKey);
              miMaxRecords = Integer.parseInt(lsMax);
           }
           else if (lsKey.equals("RetryThreshold" ))
           {
              String lsRetryValue = (String)arguments.get(lsKey) ;
              try
                {
                    miRetryThreshold = Integer.parseInt(lsRetryValue) ;
                }
                catch(NumberFormatException loNumExcep)
                {
                    Log.customer.debug("***%s***::init::Invalid retry threshold value specified, resetting to default value.", msCN);                    
                    miRetryThreshold = BuyintERPResponseReader.RETRY_DEFAULT ;
                }
                Log.customer.debug("***%s***::init::Retry Threshold %s .", msCN, miRetryThreshold);
            }
            else if(lsKey.equals("MaxTransactions"))
            {
                String lsMaxTransactionsValue = (String)arguments.get(lsKey) ;
                try
                {
                    miMaxRecords = Integer.parseInt(lsMaxTransactionsValue) ;
                }
                catch(NumberFormatException loNumExcep)
                {
                    Log.customer.debug("***%s***::init::Invalid max transactions value specified, resetting to default value.", msCN);
                    miMaxRecords = BuyintERPResponseReader.MAX_TRANSACTIONS_DEFAULT ;
                }
                Log.customer.debug("***%s***::init::Max Records %s .", msCN, miMaxRecords);
            }
            else if(lsKey.equals("EmailPermission"))
            {
                msFailedTXNPermission = (String) arguments.get(lsKey);
                Log.customer.debug("***%s***::init::Email permission %s .", msCN, msFailedTXNPermission);
            }

        }
        try
        {
            BuyintDBIO moDbIo = new BuyintDBIO(msDBURL,msDBUser,msDBPassword);
            moDbRec = new BuyintDBRecExportData(moDbIo, msTable);

            moDOMFactory = DocumentBuilderFactory.newInstance();
            moDOMFactory.setValidating(true);
            Log.customer.debug("***%s***::init::About to create new DocumentBuilder ...", msCN);
            moDOMFactory.newDocumentBuilder();
        }
        catch (ParserConfigurationException ex)
        {
            Log.customer.debug("***%s***::init::ParserConfigurationException %s .", msCN, ex.getMessage());
        }
    }
    
    public void clearReceiptImportValues()
    {
    	Log.customer.debug("***%s***::clearReceiptImportValues called.", msCN);
    	Receipt loReceipt = (Receipt) BuysenseERPReceiptImport.moComposingReceipt;
    	boolean lbHeader = BuysenseERPReceiptImport.mbHeader;
    	boolean lbDetail = BuysenseERPReceiptImport.mbDetail;
    	Log.customer.debug("***%s***::clearReceiptImportValues::loReceipt %s", msCN, loReceipt);
    	Log.customer.debug("***%s***::clearReceiptImportValues::lbHeader %s", msCN, lbHeader);
    	Log.customer.debug("***%s***::clearReceiptImportValues::lbDetail %s", msCN, lbDetail);
    	if(loReceipt != null && !loReceipt.getStatusString().trim().equals("Approved") &&
    			lbHeader && !lbDetail)
    	{
	    	clearHeader(loReceipt);
    	}
    	else if(loReceipt != null && !loReceipt.getStatusString().trim().equals("Approved") &&
    			lbHeader && lbDetail)
    	{
    		clearAll(loReceipt);
    	}
    	else
    	{
    		Log.customer.debug("***%s***::clearReceiptImportValues::Skipped Clearing", msCN);
    	}
    }
    
    public void clearHeader(Receipt receipt)
    {
    	receipt.getComments().clear();
    	receipt.setFieldValue("ERPPONumber", null);
    	receipt.setFieldValue("ERPReceiptNumber", null);
    	receipt.setFieldValue("CloseOrder", new Boolean(false));
    	receipt.setFieldValue("AutoApprove", new Boolean(false));
    	receipt.save();
    	Log.customer.debug("***%s***::clearHeader::Cleared Header", msCN);
    	Base.getSession().sessionCommit();
    }

    public void clearAll(Receipt receipt)
    {
    	receipt.getComments().clear();
    	receipt.setFieldValue("ERPPONumber", null);
    	receipt.setFieldValue("ERPReceiptNumber", null);
    	receipt.setFieldValue("CloseOrder", new Boolean(false));
    	receipt.setFieldValue("AutoApprove", new Boolean(false));
    	Log.customer.debug("***%s***::clearAll::Cleared Header", msCN);
		
    	List loreceiptLines = (List) receipt.getReceiptItems();
    	for(int i=0; i<loreceiptLines.size(); i++)
    	{
    		ReceiptItem loReceiptItem = (ReceiptItem)loreceiptLines.get(i);
    		loReceiptItem.setNumberAccepted(new BigDecimal(0.00000));
    		loReceiptItem.setNumberRejected(new BigDecimal(0.00000));
    		loReceiptItem.setComment(null);
    		loReceiptItem.setNotifyPurchasingAgent(new Boolean(false));
    		loReceiptItem.setDate(null);
    		loReceiptItem.setERPPONumber(null);
    		loReceiptItem.setERPPOLineNumber(null);
    		loReceiptItem.setFieldValue("ERPReceiptLineNumber", null);
    		Log.customer.debug("***%s***::clearAll::Cleared line # %s", msCN, loreceiptLines.get(i));
    	}
    	receipt.save();
    	Base.getSession().sessionCommit();
    }

}
