/************************************************************************************
 * Author:  Ivan Beraha
 * Date:    June 6, 2006
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 10/6/2006         Richard Lee	    ER 139 - Check Header shipto against line
 *                                                   item shipto.
 * 06/05/2006        Ivan Beraha            VEPI ER 139 - Ship To Address Line to
 *                                          Header Propogation
 *
 *
 * @(#)PunchoutSetSwamFields.java     1.0 10/18/2005
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/

package config.java.ams.custom;

import ariba.procure.core.ProcureLineItem;
import ariba.purchasing.core.Requisition;
import ariba.approvable.core.LineItem;
import ariba.purchasing.core.ReqLineItem;
import ariba.base.core.*;
import ariba.common.core.*;
import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import java.util.List;

public class SetDistinctiveLineItemShipTos extends Action
{
    public void fire (ValueSource object, PropertyTable params)
    {
	Log.customer.debug("Calling SetDistinctiveLineItemShipTos.java");

        Address loShipTo = null;
        LineItem loLineItem = null;
        Requisition loRequisition = null;
        ReqLineItem loReqLineItem = null;
        ReqLineItem loDefaultReqLineItem = null;
        setRegistrationType(object);

        try
        {
            if(object == null)
            {
                return;
            }
            if(object instanceof ReqLineItem)
            {
                loReqLineItem = (ReqLineItem)object;
                loRequisition = (Requisition)loReqLineItem.getLineItemCollection();
            }
            else if (object instanceof Requisition)
            {
                loRequisition = (Requisition)object;
                loLineItem = loRequisition.getLineItem(1);

                if (loLineItem == null)
                {
                    return;
                }
                else
                {
                    loReqLineItem = (ReqLineItem)loLineItem;
                }
            }
            else
            {
                return;
            }

            loShipTo = (Address)loReqLineItem.getDottedFieldValue("ShipTo");
            Log.customer.warning(8888, "SetDistinctiveLineItemShipTos::fire - loShipTo: " + loShipTo);
            //loDefaultReqLineItem = (ReqLineItem)loRequisition.getDefaultLineItems().get(0);
            Log.customer.warning(8888, "SetDistinctiveLineItemShipTos::fire - loRequisition: " + loRequisition);
            if(loRequisition == null)
            {
                return;
            }
            @SuppressWarnings("unchecked")
            List <ReqLineItem> lvDefaultLineItems = (List<ReqLineItem>) loRequisition.getFieldValue("DefaultLineItems") ;
            loDefaultReqLineItem = (ReqLineItem) lvDefaultLineItems.get(0);
            Log.customer.warning(8888, "SetDistinctiveLineItemShipTos::fire - loDefaultReqLineItem: " + loDefaultReqLineItem);
            if(isDistinctiveDefaultShipTo(loShipTo, loRequisition) || isDistinctiveLineItemShipTo(loShipTo, loRequisition))
            {
	        loDefaultReqLineItem.setDottedFieldValue("DistinctiveLineItemShipTos", new Boolean (true));
            }
            else
            {
	        loDefaultReqLineItem.setDottedFieldValue("DistinctiveLineItemShipTos", new Boolean (false));
            }

            Log.customer.debug("Set DistinctiveLineItemShipTos field.");
        }
        catch(Exception loExcep)
        {
            Log.customer.debug("Exception while setting DistinctiveLineItemShipTos field.");
            loExcep.printStackTrace() ;
        }
    }
    private boolean isDistinctiveDefaultShipTo (Address foAddress, Requisition foRequisition)
    {
            ReqLineItem loDefaultReqLineItem = (ReqLineItem)foRequisition.getDefaultLineItems().get(0);

            if (foAddress != (Address)loDefaultReqLineItem.getDottedFieldValue("ShipTo"))
            {
                return true;
            }
            return false;
    }
    private boolean isDistinctiveLineItemShipTo (Address foAddress, Requisition foRequisition)
    {
            BaseVector lvLineItems = foRequisition.getLineItems();
            ReqLineItem loReqLineItem = null;
        for (int rliCount = 0; rliCount < lvLineItems.size(); rliCount++)
        {
            loReqLineItem = (ReqLineItem)lvLineItems.get(rliCount);
            if (foAddress != (Address)loReqLineItem.getDottedFieldValue("ShipTo"))
            {
                return true;
            }
        }
        return false;
    }
    public void setRegistrationType(ValueSource loObj)
    {
    	if(loObj instanceof ReqLineItem)
    	{
    		Log.customer.debug("DistinctiveLineItemShipTos::setRegistrationType::Calling to set Req Line");
    		setRegistration((ReqLineItem) loObj);
    	}
    	else if(loObj instanceof Requisition)
    	{
    		Requisition loRequisition = (Requisition) loObj;
    		List lvLineItems = (List) loRequisition.getLineItems();
            for (int i=0; i<(lvLineItems.size()); i++)
            {
            	Log.customer.debug("DistinctiveLineItemShipTos::setRegistrationType::Calling to set Req");
            	ReqLineItem loLineItem = (ReqLineItem)lvLineItems.get(i);
            	setRegistration((ReqLineItem) loLineItem);
            }
    	}
    }
    public void setRegistration(ReqLineItem reqLineObj)
    {
    	Log.customer.debug("DistinctiveLineItemShipTos::setRegistration SL %s",reqLineObj.getSupplierLocation());
    	if(reqLineObj.getSupplierLocation() != null && reqLineObj.getDottedFieldValue("SupplierLocation.RgstTypeDesc") != null &&
    	   reqLineObj.getDottedFieldValue("SupplierLocation.RgstTypeCd") != null && reqLineObj.getOldValues() == null)
    	{
    		reqLineObj.setFieldValue("RgstTypeDesc", (String) reqLineObj.getDottedFieldValue("SupplierLocation.RgstTypeDesc"));
    		reqLineObj.setFieldValue("RgstTypeCd", (String) reqLineObj.getDottedFieldValue("SupplierLocation.RgstTypeCd"));
    	}
    }
}
