/*
   
    Responsible: imohideen
*/

/* 08/11/2003: Updates for Ariba 8.0 (Jeff Namadan) */
/* 09/03/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLQuery;
/* Ariba 8.0: Replaced ariba.common.core.nametable.NamedObjectNameTable) */
import ariba.base.core.aql.AQLNameTable;
/* Ariba 8.0: Replaced ariba.util.core.Util */
import java.util.List;
import ariba.util.log.Log;




/* Ariba 8.0: Replaced NamedObjectNameTable */
//81->822 changed Vector to List
public class CommCodeNameTable extends AQLNameTable
{
    
    public static final String ClassName="config.java.ams.custom.FieldDataNameTable";
    //public static final String FieldName="FieldDefault2";
    
    
    /**
        Overridden to check the results to see if the approvable is
        an eform.
    */
    
    public List matchPattern (String field, String pattern)
    {
        List results = super.matchPattern(field, pattern);
        return results;
    }
    
    public void addQueryConstraints (AQLQuery query,
                                     String field, String pattern)
    {
        
        // build the default set of field constraints for the pattern
        super.addQueryConstraints(query, field, pattern);
        beginSystemQueryConstraints(query);
        
        // add the constraints
        Log.customer.debug("Building conditions for CommCode");
        String CommCode = "CommCode"; 
        AQLCondition commCodeCond =
            AQLCondition.buildEqual(query.buildField("FieldNumber"),
                                    CommCode);
        
        query.and(commCodeCond);
        
        endSystemQueryConstraints(query);
    }
    
    /* Ariba 8.0: Since NamedObjectNameTable was deprecated by AQLNameTable
       the constraints method is no longer available. If constraints are needed
       see the migration pdf for 7.1a 
    public List constraints (String field, String pattern)
    {
        List constraints = super.constraints(field, pattern);
        return constraints;
    }
    */   
    
    
    
    
    
}
