package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.util.log.Log;
import ariba.util.core.PropertyTable;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionResult;
import ariba.base.core.ClusterRoot;
import java.math.BigDecimal;
import ariba.base.fields.ValueInfo;
import ariba.base.fields.Behavior;

public class BuysenseAmountEditability extends Condition
{

    private static final ValueInfo parameterInfo[];
    public static final String moAmountFieldName = "AmountFieldName";
    public static final String moApproveFieldName = "ApproverFieldName";
    public static final String moEformObj = "EformObj";
    
    
    public boolean evaluate(Object value, PropertyTable params)

    {
        Log.customer.debug("BuysenseAmountEditability:Object %s, Params %s", value, params);
        return evaluateField(value, params);
    }

    protected boolean evaluateField(Object value, PropertyTable params)
    {
        String lsAmountField = (String)params.getPropertyForKey(moAmountFieldName);
        String lsApproverField = (String)params.getPropertyForKey(moApproveFieldName);
        Approvable loEformObj = (Approvable)params.getPropertyForKey(moEformObj);
        Log.customer.debug("BuysenseAmountEditability:AmountField %s, ApproverField %s, EformObj %s", lsAmountField, lsApproverField, loEformObj);
        return isFieldEditable(lsAmountField, lsApproverField, loEformObj);
    }
    
    public boolean isFieldEditable(String amountField, String approverField, Approvable bsoEform)
    {
   		ClusterRoot loApprover = (ClusterRoot) bsoEform.getFieldValue(approverField);
   		BigDecimal loAmount = (BigDecimal) bsoEform.getFieldValue(amountField);
   		Log.customer.debug("BuysenseAmountEditability:Approver %s, Amount %s Object %s", loApprover, loAmount, bsoEform);
   		if(loApprover == null && loAmount != null && loAmount.intValue() > 0)
   		{
   			Log.customer.debug("BuysenseAmountEditability:returning false");
   			return false;
   		}
   		Log.customer.debug("BuysenseAmountEditability:returning true");
   		return true;
    }

    public ConditionResult evaluateAndExplain(Object value,
                                              PropertyTable params)
    {
        if(!evaluateField(value, params))
        {
            String msg = params.stringPropertyForKey("Message");
            Log.customer.debug("BuysenseAmountEditability:msg %s ", msg);
            return new ConditionResult(msg);
        }
        else
        {
            return null;
        }
    }
    
    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }
    
    static
    {
    	parameterInfo = (new ValueInfo[] 
         {
            new ValueInfo("AmountFieldName", 0),
            new ValueInfo("ApproverFieldName", 0),
            new ValueInfo("EformObj", 0),
            new ValueInfo("Message", 0, Behavior.StringClass)
         }
       );
    }
}
