
/************************************************************************************
 * Author:  Richard Lee
 * Date:    June 2005
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 08/05/2005        Richard Lee            ER# 119 - Proprietary and Confidential
 *                                                    button with Comment field
 *
 *
 * @(#)SetPandCField.java     1.0 08/05/2005
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/

/*************************************************************************************
 *
 * The purpose of this trigger is to copy the Proprietary and Confidential (PandC) boolean
 * value from the temporary Comment object to the final Comment object where Ariba
 * will persist the PandC value.
 *
 * The reason why the PandC value does not persist in Ariba is because Ariba actually
 * creats two Comment objects when Adding a comment: one is when the "Add Comment" button
 * is selected; the other is for displaying the new comment.  Unfortunately, Ariba persists
 * the later comment, which does not have the correct PandC value. So, this trigger will
 * copy the PandC value from the first to the second comment object.
 *
 * This trigger will fire when the Text field of a Comment object changes or when
 * the Proprietary and Confidential boolean field of a Comment object has been changed
 * by the user.
 *
 ***************************************************************************************/
package config.java.ams.custom;

import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.base.core.*;
import ariba.approvable.core.Comment;
import ariba.util.core.*;

public class SetPandCField extends Action
{
    public void fire (ValueSource object, PropertyTable params)
    {
        try
        {
            Log.customer.debug("Calling trigger SetPandCField ");

            TransientData tdComment =  Base.getSession().getTransientData();
            Comment loComment = (Comment) object;
            BaseId lbCommentId = (BaseId) loComment.getBaseId();
            Boolean lbPandC = ((Boolean)loComment.getFieldValue("ProprietaryAndConfidential"));
            Boolean lbTDPandC = null;
            String lsBaseId = lbCommentId.toDBString();
            String lsPandC = lbPandC.toString();
            String lsText = "";
            String lsTDBaseId = null;
            String lsTDPandC = null;
            String lsTDText = null;
            LongString llsText = (LongString) loComment.getDottedFieldValue("Text");

            if(llsText != null)
            {
                lsText = (String) llsText.string();
            }

            /**********************************************************************************
            *
            * if current PandC is true, stored info in tdComment for copying later.
            * else user might be changing PandC value or Ariba is creating a second Comment object.
            *
            ***********************************************************************************/
            if((lbPandC != null) && lbPandC.booleanValue())
            {
                tdComment.put("Text", lsText);
                tdComment.put("PandCValue", lsPandC);
                tdComment.put("CommentObject", lsBaseId);
            }
            else
            {
                lsTDPandC = (String) tdComment.get("PandCValue");
                lsTDText = (String) tdComment.get("Text");
                lsTDBaseId = (String) tdComment.get("CommentObject");

                /*******************************************************************************
                *
                * if baseId of two comments are same, update tdComment with user PandC changes.
                * else if two comments are different AND texts are same,
                * update second Comment object with correct PandC value.
                * else do nothing.
                *
                ********************************************************************************/
                if (!StringUtil.nullOrEmptyOrBlankString(lsBaseId) &&
                    !StringUtil.nullOrEmptyOrBlankString(lsTDBaseId) &&
                    lsBaseId.trim().equals(lsTDBaseId.trim()))
                {
                    tdComment.put("PandCValue", lsPandC);
                }
                else if (!StringUtil.nullOrEmptyOrBlankString(lsTDText) &&
                        !StringUtil.nullOrEmptyOrBlankString(lsText) &&
                        lsText.trim().equals(lsTDText.trim()))
                {
                    lsTDPandC = (String) tdComment.get("PandCValue");
                    lbTDPandC = (Boolean) Constants.getBoolean(lsTDPandC);
                    loComment.setFieldValue("ProprietaryAndConfidential", lbTDPandC);
                }
                else
                {
                    Log.customer.debug("One of the comment or Text is null or Texts not equal. ");
                }
            }
        }
        catch (Exception ex)
        {
             Log.customer.debug("Exception: " + ex);
        }
    }
}
