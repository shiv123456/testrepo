/*
    Copyright (c) 1996-1999 Ariba/Buysense, Inc.
    Falahyar March 2000
    ---------------------------------------------------------------------------------------------------


    Developer          Date          Change Description
    -----------------  --------  --------------------------------------------------
    David Chamberlain  11/09/00  WA ST SPL#104:  The contract number is no longer in
                                 "ReqLineFieldDefault1.Name". Must get it from FieldDefault3.
*/
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;


import ariba.purchasing.core.Requisition;
import java.util.List;
import java.math.*;
import ariba.base.core.*;
import ariba.util.log.Log;
import ariba.base.fields.ValueSource;
// Ariba 8.0: Deprecations, methods have changed packages.
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;

//// 81->822 changed Vector to List
public class NonContractProcessor  
{
    
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));
    
    BigDecimal DISAmount; 
    BigDecimal OSPAmount; 
    BigDecimal EOSPAmount;
    BigDecimal OFMAmount; 
    Partition part;
    ariba.purchasing.core.Requisition req; 
    public void processRules (ValueSource object) 
    {
        
        req = (ariba.purchasing.core.Requisition)object;
        if (req instanceof ariba.purchasing.core.Requisition) 
        {
            part = req.getPartition();
            List lineItemCollection = ((Requisition)req).getLineItems();
            
            //We have to reset the vector each time this program is invoked as there may have been changes to the line items
            
            List ncVector = (List)req.getDottedFieldValue("NonContractVector");
            
            Log.customer.debug("at beginning of NonContractProcessor. NonContractVector is %s",ncVector);
            Log.customer.debug("at beginning of NonContractProcessor. REQ is %s",
                           req);
            if (ncVector!=null) 
            {
                int ncVectorSize = ncVector.size();
                for (int k=0;k<ncVectorSize;k++)
                {
                    // Ariba 8.1: Replaced deprecated method
                    BaseObject ncObject = (BaseObject)ncVector.get(k);
                    ncObject.setDottedFieldValue("DISAmount",new BigDecimal(0));
                    ncObject.setDottedFieldValue("OSPAmount",new BigDecimal(0));
                    ncObject.setDottedFieldValue("EOSPAmount",
                                                 new BigDecimal(0));
                    ncObject.setDottedFieldValue("OFMAmount",new BigDecimal(0));
                }
            }
            
            
            // Ariba 8.1: Replaced deprecated method
            for (int j=0 ; j < lineItemCollection.size(); j++) 
            {
                // Ariba 8.1: Replaced deprecated method
                ariba.purchasing.core.ReqLineItem li = (ariba.purchasing.core.ReqLineItem)lineItemCollection.get(j);
                
                // Get the commodity code on the line item
                String commodityCode = (String)li.getDottedFieldValue("PartitionedCommodityCode.UniqueName");
                
                //Determine if the supplier is null. If yes, ignore non contract processing for  that line item
                
                BaseObject supplier = (BaseObject)li.getDottedFieldValue("Supplier");
                if (supplier == null) 
                {
                    Log.customer.debug("encountered null supplier in FOR LOOP for line item : %s",j);
                    //break out and go to next increment in for loop
                    continue;
                }
                
                //Get the supplier id of the vendor. If the vendor is an ad-hoc supplier, then get the Supplier Name
                String vendorID = (String) li.getDottedFieldValue("Supplier.SupplierIDValue");
                if ((vendorID == null) || (vendorID.trim().length() == 0)) 
                {
                    vendorID = (String) li.getDottedFieldValue("Supplier.Name");
                    
                }
                
                
                //determine if the line item is a non contract item 
                String catNonContract =  (String)li.getDottedFieldValue("Description.ContractYesNo");
                Log.customer.debug("catNonContract is %s",catNonContract);
                // WA ST SPL#104: Get the contract number. It is no longer in "ReqLineFieldDefault1.Name". 
                //String reqNonContract =  (String)li.getDottedFieldValue("ReqLineFieldDefault1.Name");
                String reqNonContract =  (String)li.getDottedFieldValue("Accountings.SplitAccountings[0].FieldDefault3.Name");
                Log.customer.debug("reqNonContract is %s",reqNonContract);
                
                // Execute rest of logic and validation only for Non Contract items
                // check for contracct num on the Catalog item and /or on the Requisition Header Line (ReqLineDefault1)
                Log.customer.debug("Check if item is non contract");
                if ( (reqNonContract != null 
                      && (reqNonContract.length() > 0)) 
                        || ( catNonContract != null 
                            && catNonContract.equals("Y"))  )
                {
                    Log.customer.debug("This is a Contract Item");
                    continue;
                }
                
                
                // Look up the different categories of non contract commodity codes from FieldDataTable
                String UniqueName = "ALL:ALL:NCCommCode:"+commodityCode;
                Log.customer.debug("Looking for %s FieldDataTable Object",
                               UniqueName);
                ClusterRoot fieldDataTable = Base.getService().objectMatchingUniqueName
                    ("ariba.core.BuysenseFieldDataTable",
                     req.getPartition(), UniqueName);
                
                String commName = null;
                if (fieldDataTable!=null)
                {
                    commName = (String)fieldDataTable.getDottedFieldValue("Name");
                }
                else 
                {
                    commName = "OSP";
                }
                DISAmount = new BigDecimal(0);
                OSPAmount = new BigDecimal(0);
                EOSPAmount = new BigDecimal(0);
                OFMAmount = new BigDecimal(0);
                Log.customer.debug("NonContractVector - before SET AMOUNT CALL: %s",
                               ncVector);
                // Put the amount of the line item in the correct category of non contract purchases i.e. DIS,OSP,EOSP,OFM
                setAmount(commName,
                          (BigDecimal)li.getDottedFieldValue("Amount.Amount"));
                Log.customer.debug("NonContractVector - after SET AMOUNT CALL: %s",
                               ncVector);
                // Update the Non Contracts vector on the requisition
                Log.customer.debug("NonContractVector - before UPDATE NCVECTOR CALL: %s",ncVector);
                updateNCVector(ncVector,
                               vendorID,
                               DISAmount,OSPAmount,EOSPAmount,OFMAmount);
                Log.customer.debug("NonContractVector - after UPDATE NCVECTOR CALL: %s",ncVector);
                
                
            }
            
            
            //We need to update the Non Contract max Totals object now.
            
            if   (ncVector.size() > 0) 
            {
                Log.customer.debug("nc vECTOR SIZE : %s",ncVector.size());
                Log.customer.debug("need to update max totals now");
                BigDecimal  maxDISAmount = getMaxAmount(ncVector,"DISAmount");
                BigDecimal  maxOSPAmount = getMaxAmount(ncVector,"OSPAmount");
                BigDecimal  maxEOSPAmount = getMaxAmount(ncVector,"EOSPAmount");
                BigDecimal  maxOFMAmount = getMaxAmount(ncVector,"OFMAmount");
                
                //Create a Maximum Amounts field
                Log.customer.debug("Adding Max Amounts field");
                BaseObject maxbo = (BaseObject)req.getDottedFieldValue("NonContractTotals");
                if (maxbo == null) 
                {
                    maxbo = (BaseObject)BaseObject.create("ariba.core.NonContractAmounts",part);
                }
                maxbo.setDottedFieldValue("DISAmount",maxDISAmount);
                maxbo.setDottedFieldValue("OSPAmount",maxOSPAmount);
                maxbo.setDottedFieldValue("EOSPAmount",maxEOSPAmount);
                maxbo.setDottedFieldValue("OFMAmount",maxOFMAmount);
                //  Log.customer.debug("Adding null to req");
                //  req.setFieldValue("NonContractTotals",null);
                //  req.save();
                //  Log.customer.debug("NonContractTotals after null is: %s",req.getDottedFieldValue("NonContractTotals"));
                Log.customer.debug("Adding the object %s to req",maxbo);
                req.setFieldValue("NonContractTotals",maxbo);
                req.save();
                Log.customer.debug("NonContractTotals after final save is %s",
                               req.getDottedFieldValue("NonContractTotals"));
                
                
            }
        }
        
        
    }
    
    private BigDecimal getMaxAmount(List ncVector,String fieldName) 
    {
        
        Log.customer.debug("In getmaxAmount with: %s",fieldName);
        Log.customer.debug("at beginning of getMaxAmount. REQ is %s",req);
        //List ncVector = (List)req.getDottedFieldValue("NonContractVector");
        
        Log.customer.debug("In getmaxAmount. ncVector Passed in is %s",ncVector);
        
        
        
        int ncVectorSize = ncVector.size();
        
        Log.customer.debug("In getmaxAmount. ncVectorsize is %s",ncVectorSize);
        
        // Ariba 8.1: Replaced deprecated method
        BaseObject ncAmt = (BaseObject)ncVector.get(0);
        Log.customer.debug("In getmaxAmount. ncAmt is %s",ncAmt);
        
        BigDecimal maxAmt = (BigDecimal)ncAmt.getDottedFieldValue(fieldName);
        for (int k=0;k<ncVectorSize;k++)
        {
            Log.customer.debug("In getmaxAmount INSIDE IF");
            
            // Ariba 8.1: Replaced deprecated method
            ncAmt = (BaseObject)ncVector.get(k);
            Log.customer.debug("In getmaxAmount INSIDE IF. ncAmt is %s",ncAmt);
            
            BigDecimal newAmt =  (BigDecimal)ncAmt.getDottedFieldValue(fieldName);
            Log.customer.debug("In getmaxAmount after newamt ");
            
            if (newAmt.compareTo(maxAmt) == 1) 
            {
                Log.customer.debug("In getmaxAmount after newamt inside compare ");
                BigDecimal tempMaxAmt = newAmt;
                Log.customer.debug("In getmaxAmount after assignment");
                maxAmt = tempMaxAmt;
                Log.customer.debug("In getmaxAmount after second assignment");
            }
        }
        Log.customer.debug("Max amount for field %s : %s",fieldName,maxAmt);
        
        return maxAmt;
    }
    
    private void updateNCVector(List NonContractVector,
                                String vendorID,
                                BigDecimal lineDISAmount,
                                BigDecimal lineOSPAmount,
                                BigDecimal lineEOSPAmount,
                                BigDecimal lineOFMAmount) 
    {
        
        boolean updateFlag = false;
        
        Log.customer.debug("In updateNC:VendorId:%s,DIS:%s,OFM:%s,EOSP:%s,OSP:%s",
                       vendorID,
                       lineDISAmount,
                       lineOFMAmount,lineEOSPAmount,lineOSPAmount);
        
        for (int j=0 ; j < NonContractVector.size(); j++) 
        {
            
            // Ariba 8.1: Replaced deprecated method
            if (((BaseObject)NonContractVector.get(j)).getFieldValue("VendorID").equals(vendorID)) 
            {
                
                // Ariba 8.1: Replaced deprecated method
                BigDecimal tempDISAmount = (BigDecimal)((BaseObject)NonContractVector.get(j)).getFieldValue("DISAmount");
                Log.customer.debug("Got this amount from object: %s",tempDISAmount);
                Log.customer.debug("Setting DISAmount with %s",
                               lineDISAmount.add(tempDISAmount));
                // Ariba 8.1: Replaced deprecated method
                ((BaseObject)NonContractVector.get(j)).setFieldValue("DISAmount",
                                                                     lineDISAmount.add(tempDISAmount));
                
                // Ariba 8.1: Replaced deprecated method
                BigDecimal tempOSPAmount = (BigDecimal)((BaseObject)NonContractVector.get(j)).getFieldValue("OSPAmount");
                Log.customer.debug("Got this amount from the object: %s",
                               tempOSPAmount);
                Log.customer.debug("Setting OSPAmount with %s",
                               lineOSPAmount.add(tempOSPAmount));
                // Ariba 8.1: Replaced deprecated method
                ((BaseObject)NonContractVector.get(j)).setFieldValue("OSPAmount",
                                                                     lineOSPAmount.add(tempOSPAmount));
                
                
                // Ariba 8.1: Replaced deprecated method
                BigDecimal tempEOSPAmount = (BigDecimal)((BaseObject)NonContractVector.get(j)).getFieldValue("EOSPAmount");
                Log.customer.debug("Got this amount from the object: %s",
                               tempEOSPAmount);
                Log.customer.debug("Setting EOSPAmount with %s",
                               lineEOSPAmount.add(tempEOSPAmount));
                // Ariba 8.1: Replaced deprecated method
                ((BaseObject)NonContractVector.get(j)).setFieldValue("EOSPAmount",lineEOSPAmount.add(tempEOSPAmount));
                
                // Ariba 8.1: Replaced deprecated method
                BigDecimal tempOFMAmount = (BigDecimal)((BaseObject)NonContractVector.get(j)).getFieldValue("OFMAmount");
                Log.customer.debug("Got this amount from the object: %s",
                               tempOFMAmount);
                Log.customer.debug("Setting OFMAmount with %s",
                               lineOFMAmount.add(tempOFMAmount));
                // Ariba 8.1: Replaced deprecated method
                ((BaseObject)NonContractVector.get(j)).setFieldValue("OFMAmount",
                                                                     lineOFMAmount.add(tempOFMAmount));
                
                updateFlag = true;
                Log.customer.debug("NonContractVector in updateNCVector - UPDATE object - before REQ SAVE: %s",NonContractVector);
                req.save();
                Log.customer.debug("NonContractVector in updateNCVector - UPDATE object - after REQ SAVE: %s",NonContractVector);
                
            }
            
        }
        
        if (!updateFlag) 
        {
            //INSERT A NEW ROW INTO THE VECTOR 
            Log.customer.debug("Adding a new object");
            BaseObject bo = (BaseObject)BaseObject.create("ariba.core.NonContractAmounts",part);
            bo.setDottedFieldValue("VendorID",vendorID);
            bo.setDottedFieldValue("DISAmount",lineDISAmount);
            bo.setDottedFieldValue("OSPAmount",lineOSPAmount);
            bo.setDottedFieldValue("EOSPAmount",lineEOSPAmount);
            bo.setDottedFieldValue("OFMAmount",lineOFMAmount);
            Log.customer.debug("Adding the object %s in to the vector",bo);
            Log.customer.debug("NonContractVector in updateNCVector - create object - before add element: %s",NonContractVector);
            
            // Ariba 8.1: Replaced deprecated method
            NonContractVector.add(bo);
            
            Log.customer.debug("NonContractVector in updateNCVector - create object - after add element: %s",NonContractVector);
            req.save();
        }
        
        
    }
    private void setAmount(String commName, BigDecimal Amount) 
    {
        
        Log.customer.debug("In setAmount commName: %s, and Amount: %s",
                       commName,Amount);
        if (commName.equals("DIS")) DISAmount = DISAmount.add(Amount);
        else if (commName.equals("OFM")) OFMAmount = OFMAmount.add(Amount);
        else if (commName.equals("EOSP"))EOSPAmount = EOSPAmount.add(Amount);
        else OSPAmount = OSPAmount.add(Amount);
        Log.customer.debug("At the end of setAmount:DIS:%s,OFM:%s,EOSP:%s,OSP:%s",
                       DISAmount,OFMAmount,EOSPAmount,OSPAmount);
    }
    
    
    
}
