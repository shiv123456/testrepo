/************************************************************************************
 * Author:  Richard Lee
 * Date:    September, 2007
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 09/11/2004        Richard Lee            VePI Prod SPL 1133
 *
 * @(#)AddCommentForDataRetention.java     1.0 09/11/2007
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 */

package config.java.ams.custom;

import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.core.Partition;
import ariba.base.core.BaseId;
import ariba.base.core.Base;
import ariba.approvable.core.Comment;
import ariba.approvable.core.Approvable;
import ariba.util.log.Log;
import ariba.util.scheduler.*;
import ariba.util.core.Date;
import java.util.Map;
import java.util.Properties;
import java.io.*;
import java.util.Iterator;

public class AddCommentForDataRetention extends ScheduledTask
{
    private final String ClassName = "config.java.ams.custom.AddCommentForDataRetention";
    private String msParamFile = null;
    private String msDataRetentionComment = null;
    private String msAQLForDataRetention = null;

    public AddCommentForDataRetention()
    {
    }
    public void init( Scheduler scheduler,
                     String fsScheduledTaskName, Map foArguments )
    
    {
        super.init(scheduler, fsScheduledTaskName, foArguments) ;
        String lsKey = null;
        Iterator e = foArguments.keySet().iterator();
        while (e.hasNext())
        {
           lsKey = (String)e.next();
           if ( lsKey.equals( "ParamFile" ) )
           {
              msParamFile = (String)foArguments.get( lsKey );
           }
        }
    }

    public void run() throws ScheduledTaskException
    {
        try
        {
            Log.customer.debug("Calling " + ClassName);
            if (!loadAndValidateParams())
            {
                Logs.buysense.debug("The parameters in " + msParamFile + " are not valid." );
                return ;
            }
            ProcessParamFile();
        }
        catch ( Exception loExcep )
        {
            Log.customer.debug( "Error: while adding comment to Approvable." ) ;
        }
    }

    /*
     * Return all Reqs that need data retention 
     */
    private void ProcessParamFile()
    {
        Log.customer.debug("Calling "+ ClassName);
        ariba.approvable.core.Approvable loApprovable = null;
        ariba.user.core.User loRequester = null;
        AQLQuery loQuery = null;
        BaseId loApproveBaseID = null;
        AQLResultCollection lvApprovables = null;
        Partition moClassPartition = Base.getSession().getPartition();
        ariba.util.core.Date loThisDate = new Date();
        AQLOptions loOptions = new AQLOptions(moClassPartition);
        loQuery = AQLQuery.parseQuery(msAQLForDataRetention);
        lvApprovables = Base.getService().executeQuery(loQuery, loOptions);
        int liSize = lvApprovables.getSize();
           Log.customer.debug("AQL for all the approvables that need to be modified = " + msAQLForDataRetention);
           Log.customer.debug("Number of approvables to be modified = " + liSize);

        while (lvApprovables.next())
        {
            loApproveBaseID = (BaseId)lvApprovables.getBaseId(0);
            loApprovable = (Approvable)Base.getSession().objectFromId(loApproveBaseID);
            loRequester = (ariba.user.core.User) loApprovable.getRequester();
            Comment loComment = new Comment(loApprovable.getPartition());
            loComment.setUser(loRequester);
            loComment.setTitle("Comment added for Data Retention");
            loComment.setBody(msDataRetentionComment);
            loComment.setType(Comment.TypeApprove);
            loComment.setDate(new ariba.util.core.Date());
            loApprovable.addComment(loComment);
            loApprovable.setFieldValue("LastModified", loThisDate);
            loApprovable.save();
        }
    }

    private boolean loadAndValidateParams()
    {
       Properties loProperties = new Properties();
       FileInputStream loFileInputStream = null;
       try
       {
            Log.customer.debug("Props File is in " + msParamFile);
          loFileInputStream = new FileInputStream(msParamFile);
          loProperties.load(loFileInputStream);
       }
       catch(IOException loIOE)
       {
          Log.customer.debug("ERROR: Problem reading properties file ...");
       }

       msDataRetentionComment = loProperties.getProperty("DataRetentionComment");
       msAQLForDataRetention = loProperties.getProperty("AQLForDataRetention");
       return true;
    }



}
