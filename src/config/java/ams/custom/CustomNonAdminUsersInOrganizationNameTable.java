package config.java.ams.custom;

import ariba.base.core.aql.AQLQuery;
import ariba.base.fields.ValueSource;
import ariba.base.core.BaseId;
import ariba.base.core.Base;
import ariba.user.core.Organization;

import ariba.user.core.nametable.UsersInOrganizationNameTable;
import ariba.util.log.Log;

public class CustomNonAdminUsersInOrganizationNameTable extends UsersInOrganizationNameTable{
	public static final String ClassName="config.java.ams.custom.UserRoleNameTable";
	
    public AQLQuery buildQuery (String field, String pattern)
    {
    	Organization loOrg = null;
    	String sOrgBaseID = "";
    	ariba.user.core.User currentEffectiveUser=(ariba.user.core.User)(Base.getSession().getEffectiveUser());
    	loOrg = (Organization)currentEffectiveUser.getOrganization();
    	Log.customer.debug("The organisation id id "+loOrg);
    	if(loOrg != null)
    	{
            sOrgBaseID = loOrg.getBaseId().toBase36String();
    		Log.customer.debug("The baseid for the organisation is"+sOrgBaseID);
    	}
        Log.customer.debug( "Inside CustomNonAdminUsersInOrganizationNameTable") ;
        
        String lsQuery = "Select usr, usr.Name,cusr.ClientName.ClientName,usr.EmailAddress from ariba.common.core.User cusr JOIN ariba.user.core.User usr USING cusr.User where cusr.UniqueName = usr.UniqueName and usr.Creator IS NULL and usr.Organization IN (baseid('"+sOrgBaseID+"')) AND usr.UniqueName NOT IN ('aribasystem', 'aribaguestsystem')";
        
        AQLQuery userQuery = AQLQuery.parseQuery(lsQuery);
        
		if(pattern == "*")
		{    
            Log.customer.debug( "The final query(1) when pattern is:" +pattern+ "is: " + userQuery) ;
		}
		else
		{
			if(field != null && field.equalsIgnoreCase("Name"))
			{
			    userQuery.andLike("Name.PrimaryString",pattern);
    	    
			    Log.customer.debug( "Final User query(2) when pattern is:" +pattern+ " and field is:" +field+ " is:" + userQuery) ;
  			}
			else if(field!=null && field.equalsIgnoreCase("SearchUniqueName"))
			{
				userQuery.andLike("ClientName.ClientName", pattern);
    	    
    		    Log.customer.debug( "Final User query(2) when pattern is:" +pattern+ " and field is:" +field+ " is:" + userQuery) ;
			}
			else if(field!=null && field.equalsIgnoreCase("SearchEmailAddress"))
			{
			    userQuery.andLike("User.EmailAddress", pattern);
    	    
    		    Log.customer.debug( "Final User query(2) when pattern is:" +pattern+ " and field is:" +field+ " is:" + userQuery) ;
			}
			else if(field!=null && field.equalsIgnoreCase("PasswordAdapter"))
			{
			    userQuery.andLike("User.PasswordAdapter", pattern);
    	    
    		    Log.customer.debug( "Final User query(2) when pattern is:" +pattern+ " and field is:" +field+ " is:" + userQuery) ;
			}			
		}                
        return userQuery; 
    }
}
