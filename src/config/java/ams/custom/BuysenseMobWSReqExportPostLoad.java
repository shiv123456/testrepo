package config.java.ams.custom;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.base.core.LongString;
import ariba.base.core.Partition;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseMobWSReqExportPostLoad extends Action
{

	private static String msCN = "BuysenseMobWSReqExportPostLoad";
	private static String msXML = "Mobwebservices.xml";
	
	//private static java.util.Map moDocSet = new HashMap(5);
    private static DocumentBuilderFactory moDOMFactory;
    private static DocumentBuilder moDOMBuilder;
    private static String msXMLDir = null;
    //private static Partition moPart = null; 

	
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
    	//Log.customer.debug("***%s***Called fire:: valuesource obj %s", msCN, valuesource);
    	Log.customer.debug("***%s***Called fire:: REQ EXPORT WEBSERVICE INVOKED", msCN);
    	ClusterRoot loBMREObj = null;
    	if(valuesource!= null && valuesource instanceof ClusterRoot)
          {
    		loBMREObj = (ClusterRoot) valuesource;
          }
    	else
         {
    		Log.customer.debug("***%s***Called fire:: Returning because of invalid obj", msCN);
            return;
         }
    	
    	String lsPRUniqueName = (String) loBMREObj.getFieldValue("UniqueName");
    	if(lsPRUniqueName != null)
    	{
	    	ariba.purchasing.core.Requisition loReq = (ariba.purchasing.core.Requisition)Base.getService().objectMatchingUniqueName("ariba.purchasing.core.Requisition", loBMREObj.getPartition(), lsPRUniqueName);
	    	String lsReqExportDetails = getReqExportDetails(loReq);
	    	if(lsReqExportDetails != null)
	    	{
	    		LongString loLongReqExportDetails = new LongString(lsReqExportDetails);
	    		//Log.customer.debug("***%s***In fire:: lsReqExportDetails %s", msCN, lsReqExportDetails);
	    		loBMREObj.setFieldValue("ReqExportDetails", loLongReqExportDetails);
	    		Log.customer.debug("***%s***IN fire:: REPLY SENT", msCN);
	    	}
    	}
    }
    
    private static String getReqExportDetails(ClusterRoot foClusterRoot)
    {
    	StringBuffer loXMLBuffer = new StringBuffer();
    	//BuyintExceptionHandler loHandler = new BuyintExceptionHandler();
    	Partition loPart = foClusterRoot.getPartition();
    	BuysenseWebserviceXMLWriter loXMLWriter = null;
    	try
        {
	    	moDOMFactory = DocumentBuilderFactory.newInstance();
	        moDOMFactory.setValidating(true);
	        moDOMBuilder = moDOMFactory.newDocumentBuilder();
	        msXMLDir = Base.getService().getParameter(loPart, "Application.AMSParameters.BuyintXMLDir");
	        	        
	        File loXMLFile = new File(msXMLDir + System.getProperty("file.separator") + msXML);
	        Document loDomDoc = moDOMBuilder.parse(loXMLFile);
	        BuyintXMLContainer loXMLContainer = new BuyintXMLContainer(loDomDoc);
	        
	        loXMLWriter = new BuysenseWebserviceXMLWriter(foClusterRoot);
	        loXMLWriter.setContainer(loXMLContainer);
	
	        Log.customer.debug("***%s***IN getReqExportDetails:: FINISHED CREATING DOM, CONTAINER AND CALLING XML WRITER", msCN);
	        loXMLBuffer = loXMLWriter.renderDoc();
	        Log.customer.debug("***%s***IN getReqExportDetails:: GOT XML AND ABOUT TO SEND", msCN);
        }
        catch (SAXException loEx1)
        {
            loEx1.printStackTrace();
            Log.customer.debug("***%s***In getReqExportDetails:: SAXException %s", msCN, loEx1.getMessage());
            //throw new BuyintException("Error loading XML files from directory " + msXMLDir, BuyintException.EXCPT_XMLGEN_ERROR);
        }
        catch (ParserConfigurationException loEx2)
        {
        	loEx2.printStackTrace();
            Log.customer.debug("***%s***In getReqExportDetails:: ParserConfigurationException %s", msCN, loEx2.getMessage());
        }
        catch (BuyintException loEx3)
        {
        	loEx3.printStackTrace();
            Log.customer.debug("***%s***In getReqExportDetails:: BuyintException %s", msCN, loEx3.getMessage());
        }
        catch (IOException loEx4)
        {
        	loEx4.printStackTrace();
            Log.customer.debug("***%s***In getReqExportDetails:: IOException %s", msCN, loEx4.getMessage());
        }
        catch (Exception loEx5)
        {
        	loEx5.printStackTrace();
            Log.customer.debug("***%s***In getReqExportDetails:: Exception %s", msCN, loEx5.getMessage());
        }
        return loXMLBuffer.toString();
    }
}
