package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.util.core.PropertyTable;
import ariba.workforce.core.Log;

public class BuysenseTimesheetFieldsVisibility extends Condition
{
	private static final ValueInfo parameterInfo[];
	public boolean evaluate(Object value, PropertyTable params) throws ConditionEvaluationException
	{
		BaseObject obj   = (BaseObject)params.getPropertyForKey("SourceObject");
		Log.customer.debug("inside BuysenseTimeSheetHoursVisibility SourceObject"+obj);
		if(obj.instanceOf("ariba.workforce.core.TimeSheet"))
		{
			ariba.user.core.User loggedinUser1 = (ariba.user.core.User) Base.getSession().getEffectiveUser();
			Log.customer.debug("inside BuysenseTimeSheetHoursVisibility  LoggedinUser"+loggedinUser1);
			Boolean bIsContractor = (Boolean) loggedinUser1.getFieldValue("IsContractor");
			if(bIsContractor!=null)
			{				
				if(bIsContractor.booleanValue())
				{
					return false;
				}
				else
				{					 
				   return true;
				}				   
		    }
			else
			{				   
			   return true;				   
			}
		}
		else
		{
			return true;
		}
	}			
	protected ValueInfo[] getParameterInfo()
	{
	   return parameterInfo;
	}	
	static
    {
         parameterInfo = (new ValueInfo[] {new ValueInfo("SourceObject", 0)});
    }
}
