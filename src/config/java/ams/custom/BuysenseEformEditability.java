package config.java.ams.custom;


import java.util.List;

import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.core.BaseObject;
import ariba.htmlui.fieldsui.Log;
import ariba.util.core.ListUtil;

public class BuysenseEformEditability extends Condition
{
    
    public static final String TargetValue1Param = "TargetValue1";
    public static final String TargetValue2Param = "TargetValue2";
    public static final String TargetValue3Param = "TargetValue3";
    public static final String TargetValue4Param = "TargetValue4";
    private static final ValueInfo parameterInfo[];
    private static final String requiredParameterNames[] = 
        {
        "TargetValue"
    };
    
    public boolean evaluate(Object value, PropertyTable params)
    
    {
        Log.editability.debug("Buysense: This is the object we get in the evaluate: %s ", value);
        return testValue(value, params);
    }
    
    protected boolean testValue(Object value, PropertyTable params)
    {
    	String sClientID = null;
    	String sProfName = null;
    	String sProfUN = null;
    	BaseObject oClient = (BaseObject)params.getPropertyForKey("TargetValue");
        BaseObject oProfile = (BaseObject)params.getPropertyForKey("TargetValue1");
        String sFieldName        = (String)params.getPropertyForKey("TargetValue2");
        BaseObject bo   = (BaseObject)params.getPropertyForKey("TargetValue3");
        String appType          = (String)params.getPropertyForKey("TargetValue4");
        
        if(oClient != null && oClient.instanceOf("ariba.core.BuysenseClient"))
        {
        	sClientID = (String)oClient.getFieldValue("ClientID");        	
        }
        
        if(oProfile != null && oProfile.instanceOf("ariba.core.BuysEformProf"))
        {
        	sProfName = (String)oProfile.getFieldValue("ProfileName");
        	sProfUN = (String)oProfile.getFieldValue("UniqueName");
        }
        if (sProfUN == null) 
        {
            Log.customer.info(8888,"BuysenseEditability");
            return false;
        }
        return BuysenseEformUIConditions.isFieldEditable(sClientID ,sProfName,
        		sFieldName,bo,appType);
    }
    
    public ConditionResult evaluateAndExplain(Object value,
                                              PropertyTable params)
    {
        
        if(!testValue(value, params))
        {
            String msg = params.stringPropertyForKey("Message");
            String subject = subjectForMessages(params);
            if(msg != null)
                return new ConditionResult(Fmt.Si(msg, subject));
            else
                return new ConditionResult(Fmt.Sil("ariba.common.core.condition",
                                                   "EqualToMsg1", subject));
        }
        else
        {
            return null;
        }
    }
    
    private List getTargetValues(PropertyTable params)
    {
        // Ariba 8.1: List constructor is deprecated by ListUtil.newVector() 
        List<Object> targetValues = ListUtil.list();
        Object targetValue = params.getPropertyForKey("TargetValue");
        if(targetValue != null)
            targetValues.add(targetValue);
        targetValue = params.getPropertyForKey("TargetValue1");
        if(targetValue != null)
            targetValues.add(targetValue);
        targetValue = params.getPropertyForKey("TargetValue2");
        if(targetValue != null)
            targetValues.add(targetValue);
        targetValue = params.getPropertyForKey("TargetValue3");
        if(targetValue != null)
            targetValues.add(targetValue);
        targetValue = params.getPropertyForKey("TargetValue4");
        if(targetValue != null)
            targetValues.add(targetValue);        
        return targetValues;
    }
    
    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }
    
    protected String[] getRequiredParameterNames()
    {
        return requiredParameterNames;
    }
    
    public BuysenseEformEditability()
    {
    }
    
    static
        {
        parameterInfo = (new ValueInfo[] 
                         {
                new ValueInfo("TargetValue",
                                       0),
                    new ValueInfo("TargetValue1",
                                           0),
                    new ValueInfo("TargetValue2",
                                           0),
                    new ValueInfo("TargetValue3",
                                           0),
                    new ValueInfo("TargetValue4",
                                           0),
                    new ValueInfo("Message", 0, Behavior.StringClass)
            }
        );
    }
}
