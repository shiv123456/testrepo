package config.java.ams.custom;

import java.util.Vector;

import ariba.util.log.Log;

public class EvaAribaMQManager
{

    private static String msCN = "EvaAribaMQManager";
    private static Vector<EvaSendMessageAribaImpl> messages = new Vector<EvaSendMessageAribaImpl>();
    private static boolean mqThreadRunStatus = false;

    /**
     * Default Constructor
     */
    public EvaAribaMQManager()
    {
    }


    public static synchronized boolean hasMQThreadRun()
    {
        if (!mqThreadRunStatus)
        {
            mqThreadRunStatus = true;
            return false;
        }
        return mqThreadRunStatus; // just return 'true' otherwise
    }

    /**
     * Adds the passed message into the queue.
     */
    public static void sendMQMessage(EvaSendMessageAribaImpl messImpl) throws Exception
    {

        try
        {
            messages.addElement(messImpl);

        }
        catch (Exception e)
        {
            Log.customer.warning(7100, msCN + ": Exception adding element to messages:" + e.getMessage());            
            throw e;
        }

    }

    /**
     * Checks if the queue is empty.
     * 
     * @return boolean
     */
    public static boolean isMQMessQueueEmpty()
    {
        boolean result = false;

        if (messages.size() == 0)
        {
            result = true;
        }
        return result;
    }

    /**
     * Retrieves one message from the queue.
     */
    public static synchronized EvaSendMessageAribaImpl getMessage()
    {
        EvaSendMessageAribaImpl message = (EvaSendMessageAribaImpl) messages.elementAt(0);
        messages.removeElementAt(0);
        messages.trimToSize();
        return message;
    }

}
