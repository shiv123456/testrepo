package config.java.ams.custom;

import ariba.approvable.core.AttachmentWrapper;
import ariba.approvable.core.Comment;
import ariba.approvable.core.LineItemCollection;
import ariba.base.core.BaseObject;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.purchasing.core.Requisition;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
/**
 * CSPL #	: 2884 'P&C' checkboxes should not be editable in submitted status
 * @author 	: Sarath Babu Garre
 * Date  	: 24-May-2011
 * @version : 1.0
 * 
 * Explanation: P&C field is editable after submission of a Requsisiton. COVA want it to be non-editable after submission of a Req.   
 */

public class BuysensePandCFieldEditability extends Condition
{
	private static final ValueInfo parameterInfo[];
	public boolean evaluate(Object obj, PropertyTable propertytable) throws ConditionEvaluationException
	{
		Log.customer.debug("inside  BuysensePandCFieldEditability SourceObject"+obj);
		BaseObject baseobj  = (BaseObject)propertytable.getPropertyForKey("SourceObject");
		if (baseobj!=null && baseobj instanceof Comment )
		{			
			BaseObject parent = (BaseObject)baseobj.getFieldValue("Parent");
			if(parent !=null && parent instanceof Requisition)
			{				
				Requisition req = (Requisition)parent;
				String status = (String)req.getStatusString();
				if (status != null && status.equals("Composing"))
				{
					Log.customer.debug("inside  BuysensePandCFieldEditability returning true from Comment");
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		if (baseobj!=null && baseobj instanceof AttachmentWrapper)
		{			
			LineItemCollection Lic = (LineItemCollection)baseobj.getFieldValue("LineItemCollection");
			if(Lic !=null && Lic instanceof Requisition)
			{				
				Requisition req = (Requisition)Lic;
				String status = (String)req.getStatusString();
				if (status != null && status.equals("Composing"))
				{
					Log.customer.debug("inside  BuysensePandCFieldEditability returning true from Attachemnt");
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		return false;
	}
	  protected ValueInfo[] getParameterInfo()
	    {
	        return parameterInfo;
	    }
	 
	    static
	    {
	      parameterInfo = (new ValueInfo[] {
	         new ValueInfo("SourceObject", 0)
	        });
	    }
}
