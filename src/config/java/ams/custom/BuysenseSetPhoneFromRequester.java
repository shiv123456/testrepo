package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.BaseObject;
import ariba.base.fields.Action;
import ariba.base.fields.Log;
import ariba.base.fields.ValueInfo;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;

public class BuysenseSetPhoneFromRequester extends Action{
	
	private static final ValueInfo parameterInfo[];
    public void fire(ValueSource object, PropertyTable params)
    {
        /*
        This method had to be overriden for the Ariba 8.1 upgrade. The shared/partitioned User split
        caused problems in this class. The fields that are copied by this class, are on the
        BuysenseOrg and the BuysenseOrg is no longer simply on the User object (the shared User), it's
        on the Partitioned User. Therefore, we had the change the logic to look on the Partitioned
        User object.  The AML (AmsReqDefaulter.aml) that triggers this code has also been changed for
        this mod.  02/17/2004: Updates for Ariba 8.1 (David Chamberlain)
        */
		BaseObject baseobj  = (BaseObject)params.getPropertyForKey("SourceObject");	
		String lsFieldName    = (String)params.getPropertyForKey("TargetValue");
		Log.customer.debug("Inside BuysenseSetPhoneFromRequester "+baseobj);
		Log.customer.debug("Inside BuysenseSetPhoneFromRequester : the field name is"+lsFieldName);


        if(((Approvable)baseobj).instanceOf("ariba.core.BuysenseVMIAssetTracking"))
        {

                    ariba.user.core.User user = (ariba.user.core.User)((BaseObject)baseobj).getFieldValue("Requester");
                    if (user != null) {
                        ariba.common.core.User cuser = ariba.common.core.User.getPartitionedUser(user, ((BaseObject)object).getPartition());
                            Log.customer.debug("**** In BuysenseSetAgencyFromPreparer::Partitioned User"+cuser.getUniqueName());
                            String lsRequesterPhone = (String)cuser.getDottedFieldValue("Phone");
                            Log.customer.debug("**** In BuysenseSetAgencyFromPreparer::RequesterPhone is "+lsRequesterPhone);
                            baseobj.setFieldValue("RequesterPhone", lsRequesterPhone);
                            Log.customer.debug("**** In BuysenseSetAgencyFromPreparer::RequesterPhone is "+(String)baseobj.getFieldValue("RequesterPhone"));
                    }
        }
    }
    
	 protected ValueInfo[] getParameterInfo()
	 {
		 return parameterInfo;
	 }
	 static
     {
		 parameterInfo = (new ValueInfo[] {
         new ValueInfo("SourceObject", 0),
         new ValueInfo("TargetValue", 0)
         });
	 }
}
