/*
    Copyright (c) 1996-1999 American Management Systems, Inc.
    Responsible: imohideen January 2001
    ---------------------------------------------------------------------------------------------------

*/

/* 08/11/2003: Updates for Ariba 8.0 (Jeff Namadan) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.common.core.Core;
/* Ariba 8.0: Replaced ariba.server.objectserver.Notification */
import ariba.user.util.mail.Notification;
// Ariba 8.1: Replaced ariba.common.core.User with new ariba.user.core.User class.
// Ariba 8.1: Role has moved to ariba.user.core.Role.
import ariba.user.core.*;
import java.util.List;
/* Ariba 8.0: Replaced ariba.util.core.Util */
/* Ariba 8.0: Added ariba.util.core.Assert */
import ariba.util.core.Assert;
import ariba.util.core.ListUtil;
import ariba.util.log.Log;
import ariba.util.parameters.Parameters;
import ariba.base.core.Partition;



/* Acts as a wrapper class for the Ariba Notification functionality.  Anytime Buysense needs to send
email(other than what Ariba sends out of the box) to the Buysense Users, we need to use this class.
The API calls that are used are not covered in Ariba's javadocs.  If Ariba decides to change the Notification
API's this would be the only class to modify.
*/
//81->822 changed Vector to List
public class BuysenseEmailAdapter 
{
    
    public static void sendMail(List recipients,String subject,String body) 
    {
        
        if (isNotificationOn()) 
        {
            Log.customer.debug("BuysenseEmailAdapter-sending email ...");
            // Ariba 8.0: Base.getService was changed to Base.getSession
            ariba.user.core.User sender = Core.getService().getAribaSystemUser(Base.getSession().getPartition()).getUser(); 
            //Log.customer.debug("subject: %s",subject);
            //Log.customer.debug("body: %s",body);
            try 
            {
                Notification.emailUsers (recipients,sender,subject,body);
            }
            catch (Exception e) 
            {
                /* Ariba 8.0: Replaced Util.assertNonFatal */
                Assert.assertNonFatal(false,
                                      "Encountered a problem in sending email");
            }
        }
        else
        {
            /* Ariba 8.0: Replaced Util.assertNonFatal */
            Assert.assertNonFatal(false,
                                  "Email Server is not specified on the Parameter.table file");
        }
    }
    
    
    public static void sendMail(Role role,String subject,String body) 
    {
        
        if (isNotificationOn()) 
        {
            //Log.customer.debug("BuysenseEmailAdapter-sending email ...");
            // Ariba 8.0: Base.getService was changed to Base.getSessions.
            ariba.user.core.User sender = Core.getService().getAribaSystemUser(Base.getSession().getPartition()).getUser(); 
            try 
            {
                // Ariba 8.0: Base.getService was changed to Base.getSession.
                // Ariba 8.1: ariba.common.core.Role getAllUsers() method was changed to no longer accept the partition argument.
                Notification.emailUsers (role.getAllUsers(),sender,subject,body);
            }
            catch (Exception e) 
            {
                /* Ariba 8.0: Replaced Util.assertNonFatal */
                Assert.assertNonFatal(false,
                                      "Encountered a problem in sending email");
            }
        }
        else
        {
            /* Ariba 8.0: Replaced Util.assertNonFatal */
            Assert.assertNonFatal(false,
                                  "Email Server is not specified on the Parameter.table file");
        }
        
    }
    
    private static boolean isNotificationOn()
    {
        
        Parameters p = Parameters.getInstance();
        List  lSMTPServerList = (List) p.getParameter("System.Base.SMTPServerNameList");
        
        if (ListUtil.nullOrEmptyList(lSMTPServerList))
        {
            Log.customer.debug("The SMTPServerNameList is null");            
            return false;
        }
        else
        {
            Log.customer.debug("The SMTPServerNameList is not null");            
            return true;
        }

    }
    
    
    
}
