/*************************************************************************************
File Name          : BuysenseCategorySupplierNameTable.java
Date Created       : 11/03/09
Created By         : Daniel Charlton, Ariba
Description        : This class defines a custom name table for the supplier field, to be
					 used when a category item is selected and collaboration is not used.
Modification Log:
Ver   Date        Modified By       Description
------------------------------------------------------------------------------------
1.0   11/03/09    Daniel Charlton   creation
*************************************************************************************/
package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.core.aql.SearchTermQuery;
import ariba.base.fields.ValueSource;
import ariba.contract.core.category.ContractCategoryUtil;
import ariba.procure.core.CategoryLineItemDetails;
import ariba.procure.core.ProcureLineItem;
import ariba.procure.core.category.ProcureCategoryDriver;
import ariba.util.log.Log;
import config.java.ams.custom.eVAContractCategoryUtil;
import java.lang.Exception;

public class BuysenseCategorySupplierNameTable extends BuysenseSupplierNameTable
{
	public static final String ClassName = "BuysenseCategorySupplierNameTable";
	public static final String dunsCondition = "Supplier.CommonSupplier.OrganizationID.Ids.Domain = 'duns'"; // + eVAContractCategoryUtil.Duns_Domain + "'";
	public static final String KeyOrganizationIDValue = "Supplier.CommonSupplier.OrganizationID.Ids.Value";
	public static final String No_suppliers = "no suppliers";

    public AQLQuery buildQuery(AQLQuery query, String field, String pattern, SearchTermQuery searchTermQuery)
    {
		 ValueSource valueSourceContext = getValueSourceContext();

		 try
		 {
			 // get default query
			 query = super.buildQuery(query, field, pattern, searchTermQuery);

			 Log.customer.debug(ClassName + " query: " + query);

			 // we'll use CategoryLineItemDetails here so that this customization is applicable to future categories
			 if (valueSourceContext instanceof CategoryLineItemDetails)
			 {
				 CategoryLineItemDetails categoryLineItemDetails = (CategoryLineItemDetails)valueSourceContext;
				 ProcureLineItem procureLineItem = categoryLineItemDetails.getLineItem();

				 // get category driver
				 ProcureCategoryDriver driver = categoryLineItemDetails.getProcureCategoryDriver();

				 // get ContractCategoryUtil
				 if (ContractCategoryUtil.get(driver) instanceof eVAContractCategoryUtil)
				 {
					 eVAContractCategoryUtil contractCategoryUtil = (eVAContractCategoryUtil)ContractCategoryUtil.get(driver);

					 // get List of DUNS nubmers for valid category suppliers
					 String dunsNumbers = getDUNSNubmersForValidCategorySuppliers (contractCategoryUtil, procureLineItem);

					 // restrict by category util
					 query.and(AQLCondition.parseCondition(dunsCondition));  // has DUNS OrginizationID
					 query.and(AQLCondition.parseCondition(KeyOrganizationIDValue + " " + AQLCondition.operatorToString(AQLCondition.OpIn) + " " + dunsNumbers));
				 }
			 }
		 }
		 catch (Exception e)
		 {
			 Log.customer.debug(ClassName + " caught exception: " + e);
		 }
		 finally
		 {
			 Log.customer.debug(ClassName + " returning query: " + query);
			 return query;
		 }
	}

	public String getDUNSNubmersForValidCategorySuppliers (eVAContractCategoryUtil contractCategoryUtil,ProcureLineItem procureLineItem)
	{
		 AQLOptions options = new AQLOptions();
         options.setPartition(procureLineItem.getPartition());
         options.setUseCache(false);

		 // get suppliers as defined by category util
		 String supplierCategoryQueryText  = contractCategoryUtil.getQueryForValidSuppliersGivenAttributes(procureLineItem);
		 AQLQuery supplierCategoryQuery = AQLQuery.parseQuery(supplierCategoryQueryText);
         AQLResultCollection results = Base.getService().executeQuery(supplierCategoryQuery, options);
         String dunsNumbers = "";
         if (results.getErrors() == null && results.getSize()>0)
         {
			 while (results.next())
			 {
				 if (dunsNumbers.length() > 0)
				 {
					 dunsNumbers =dunsNumbers+ ", " + wrapSingleQuotes(results.getString(0));
				 }
				 else
				 {
					 dunsNumbers = wrapSingleQuotes(results.getString(0));
				 }
			 }
			 return wrapParentheses(dunsNumbers);
		 }
		 return wrapParentheses(wrapSingleQuotes(No_suppliers));
	}

	public String wrapParentheses(String string)
	{
		return "(" + string + ")";
	}

	public String wrapSingleQuotes(String string)
	{
		return "'" + string + "'";
	}
}