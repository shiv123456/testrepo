package config.java.ams.custom;
import ariba.approvable.core.Approvable;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseResetUserApplication extends Action
{
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        Log.customer.debug("***%s***Called fire:: BuysenseResetUserApplication %s", valuesource);
        Approvable loAppr = null;

        if (valuesource != null && valuesource instanceof Approvable)
        {
            loAppr = (Approvable) valuesource;
            String lsRadioButton = (String) loAppr.getDottedFieldValue("RadioButtons");
            if (lsRadioButton == null || lsRadioButton.equalsIgnoreCase("Deactivate"))
            {
                loAppr.setFieldValue("eMall", new Boolean("false"));
                loAppr.setFieldValue("Other", new Boolean("false"));
                loAppr.setFieldValue("LogiXML", new Boolean("false"));
                loAppr.setFieldValue("QuickQuote", new Boolean("false"));
                loAppr.setFieldValue("VBOBuyer", new Boolean("false"));
                loAppr.setFieldValue("ElectronicForms", new Boolean("false"));
                loAppr.setFieldValue("AcceptableAcknowledgementForm", null);
            }
        }

    }
}
