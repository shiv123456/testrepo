/*
    Copyright (c) 1970-1999 AMS, Inc.
 */

/* 08/13/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 09/23/2003: Updates for Ariba 8.0 (Richard Lee) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
 03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
 */

package config.java.ams.custom;

// Ariba 8.0: Removed import ariba.server.objectserver.workflow.*
// and added ariba.server.worflowserver.* because workflow.class moved.
import org.apache.log4j.Level;

import ariba.server.workflowserver.*; // Ariba 8.0: Removed import ariba.server.objectserver.core.*. Don't need it.
// import ariba.server.objectserver.core.*;
// rgiesen dev SPL #39
import ariba.base.core.BaseId;
import ariba.base.core.Partition;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Base;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;

// Ariba 8.0
import ariba.base.fields.Action;
import ariba.user.core.Organization;
import ariba.util.core.PropertyTable;
import ariba.workforce.core.Contractor;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.common.core.CommonSupplier;

/*
 * TODO integrate this with other hooks
 */
public class AMSUserProfileApproved extends Action
// Ariba 8.0: Replaced ActionInterface with Action
// public class AMSUserProfileApproved implements ActionInterface
{
    /*
     * Ariba 8.0: can no longer use ActionInterface and execute public boolean execute( WorkflowState workflowState, ClusterRoot cr, WorkflowParameter[] parameters) throws WorkflowException {
     */
    // Ariba 8.0: replaced public boolean execute
    public void fire(ValueSource object, PropertyTable parameters) throws ActionExecutionException
    {
        ClusterRoot cr = (ClusterRoot) object;
        Log.customer.debug("AMSUserProfileApproved: In fire method ... ");

        // Code starts by SRINI for ACP implementation
        Organization loUserOrganization = (Organization) cr.getDottedFieldValue("User.User.Organization");
        // ByPass if it is Supplier UserProfile
        if (loUserOrganization != null && loUserOrganization instanceof CommonSupplier)
        {
            return;
        }
        boolean bIsContractor = false;
        Boolean boIsContractor = (Boolean) cr.getDottedFieldValue("User.User.IsContractor");
        if (boIsContractor != null)
        {
            bIsContractor = boIsContractor.booleanValue();
        }
        if (bIsContractor)
        {
            AQLOptions lOptions = null;
            AQLQuery lQuery = null;
            AQLResultCollection lAQLResults = null;

            lOptions = new AQLOptions(cr.getPartition());
            lQuery = new AQLQuery("ariba.workforce.core.Contractor");

            lQuery.andEqual("User", cr.getDottedFieldValue("User.User"));
            lAQLResults = Base.getService().executeQuery(lQuery, lOptions);

            if (lAQLResults.getErrors() != null)
            {
                return;
            }
            while (lAQLResults.next())
            {
                BaseId id = (BaseId) lAQLResults.getObject(0);
                Contractor contractor = (Contractor) id.get();
                String lsContractorPersonID = (String) cr.getDottedFieldValue("Details.ContractorPersonID");
                // There will be only one Contractor associated with User
                if (contractor != null && lsContractorPersonID != null)
                {
                    contractor.setFieldValue("ContractorPersonID", lsContractorPersonID);
                }
            }
            String lsContractorLoginID = (String) cr.getDottedFieldValue("Details.ContractorLoginID");
            lQuery = new AQLQuery("ariba.user.core.User");
            if (lsContractorLoginID != null)
            {
                lQuery.andEqual("UniqueName", lsContractorLoginID);
                lAQLResults = Base.getService().executeQuery(lQuery, lOptions);
                if (lAQLResults.getErrors() != null || lAQLResults.getSize() > 0)
                {
                    // Already user is existing and found duplicate user or errors in system.
                    // This will leads to UniqueKey violation if we proceed further
                    Log.customer.setLevel(Level.DEBUG);
                    Log.customer.debug("AMSUserProfileApproved::fire::Already user is existing with this ContractorLoginID");
                    Log.customer.setLevel(Level.DEBUG.OFF);
                    return;
                }
                else
                {
                    cr.setDottedFieldValue("User.User.UniqueName", lsContractorLoginID);
                    /*
                     * SRINI::Added below code for CSPL-1372 Just return it from here and do not PUSH Contractor UP to BSAdmin
                     */
                    return;
                }
            }
        }
        // Code end by SRINI for ACP implementation

        // Push the transaction
        // Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        // Partitioned User to obtain the extrinsic BuysenseOrg field value.
        // String clientID = (String)cr.getDottedFieldValue("Requester.BuysenseOrg.ClientName.ClientID");
        ariba.user.core.User RequesterUser = (ariba.user.core.User) cr.getDottedFieldValue("Requester");
        // rgiesen dev SPL #39
        // ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
        Partition crPartition = cr.getPartition();
        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser, crPartition);
        String clientID = (String) RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientID");
        // String BSOrgName = (String)Base.getService().getParameter(Base.getSession().getPartition(),
        String BSOrgName = (String) Base.getService().getParameter(crPartition, "Application.AMSParameters.BSORGClient");
        String XMLClientTag = clientID.trim() + "_" + BSOrgName;
        ClusterRoot user = (ClusterRoot) cr.getFieldValue("User");

        Log.customer.debug("AMSUserProfileApproved: About to invoke  AMSTXN.initMappingData()");
        AMSTXN.initMappingData();
        // A8 UAT SPL #32: Changed for Ariba 8.0 User Objects hierarchy.
        Log.customer.debug("AMSUserProfileApproved: About to invoke  AMSTXN.push()");
        int rt = AMSTXN.push(XMLClientTag, "UP", (String) cr.getFieldValue("UniqueName"), "NEW", "RDY", "A", "0", "", user, "User.Groups", "Permissions", cr.getPartition());
        Log.customer.debug("AMSUserProfileApproved: AMSTXN.push() returned %s", rt);

        cr.save();
        // Ariba 8.0: fire with return only
        return;
        // return true;

    }
}
