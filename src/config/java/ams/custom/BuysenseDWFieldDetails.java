package config.java.ams.custom;

public class BuysenseDWFieldDetails
{
    private String aribafieldname;
    private String aribafieldtype;
    private String dwoutputstr;
    private String aribagetmethod;
    private String allowedinvalues; // these are the only "allowed values" in this record
    // similar to saying AQL Example - select * from ComplexTypeImpl where attribute in ('yearsofexperience','zone')
    // If value doesn't match, the whole object is removed from the JSON output
    // This will be comma separated string values in input JSON - "yearofexperience,zone"

    public String getAribaFieldName()
    {
        return aribafieldname;
    }

    public String getAribaFieldType()
    {
        return aribafieldtype;
    }

    public String getDwOutputStr()
    {
        return dwoutputstr;
    }

    public String getAribaGetMethod()
    {
        return aribagetmethod;
    }

    public String getAllowedInValues()
    {
        return allowedinvalues;
    }

}
