package config.java.ams.custom;

import java.util.List;
import ariba.base.core.*;
import ariba.base.core.aql.*;
import ariba.purchasing.core.Requisition;
import ariba.user.core.Role;
import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.workforce.core.LaborLineItemDetails;
import ariba.base.fields.*;
import java.lang.String;
import java.util.Iterator;

/**
 * Date : 04-June-2009 CER-8: 
 * eMall: Unfiltered "On behalf of" field
 * Description : Modified the * logic to retrieve the users for the OnBehalf Of field. If a preparer has the role eVA-OnBehalOf
 *               role he will be able to select any user in any Agency(any user in COVA) If the user is assigned one or more 
 *               agency-specific OnBehalfOf roles (for example, A213-OnBehalfOf), the pick list options will include users in 
 *               the agencies specified in the role names (for example, Client Name A213) as well as users in the Preparers agency.
 * Date : 12-Aug-2009 
 * ACP Requirement: Filter values for Work Supervisorshould be the same logic as On Behalf Of, including logic for 
 *                  OnBehalfOf enhancement CER-8.
 * Date : 03-May-2010
 * Consulting requiremnt : The Filed, 'Project Lead' in consulting header and 'Verifier' in consulting Milestone Line should filter
 *                         with 'On-Behalf' Of logic.
 *                         As contractor users came into picture after ACP Implementation, The On-Behalf Of logic is modified in such a 
 *                         way so that it will not retrive contractors in the search reasults(only actual buyer users will be returned).                            
 * 
 */

public class BuysenseRequisitionSupervisorNameTable extends AQLNameTable
{
    private static final String UserQueryString       = "SELECT pu.User, %s FROM "
                                                              + ariba.common.core.User.class.getName()
                                                              + " pu Partition pcsv ";

    private List                selectFields          = ListUtil.list();
    private static String       PartitionedUserSuffix = "pu";
    private static String       SharedUserSuffix      = "su";

    public List getSelectFields()
    {
        if (ListUtil.nullOrEmptyList(selectFields))
        {
            List fields = getFetchFields();
            if (fields != null)
            {
                for (int i = 0; i < fields.size(); i++)
                {
                    String field = (String) fields.get(i);
                    addField(field);
                }
            }
        }
        return selectFields;
    }

    private void addField(String field)
    {
        if (field.startsWith("PartitionedUser"))
        {
            int index = field.indexOf(".");
            if (index == -1)
            {
                selectFields.add(PartitionedUserSuffix);
            }
            String massagedField = field.substring(index);
            massagedField = StringUtil.strcat(PartitionedUserSuffix, massagedField);
            Log.customer.debug("Field Name " + massagedField);
            selectFields.add(massagedField);
        }
        else
        {
            String massagedField;
            ValueSource context = getValueSourceContext();
            // Added null check as part of CSPL-2066
            if (context!=null && context.toString().indexOf("ConsultingLineItemDetails") >= 0)
            {
                massagedField = StringUtil.strcat(PartitionedUserSuffix, ".", field);
            }
            else
            {
                massagedField = StringUtil.strcat(SharedUserSuffix, ".", field);
            }
            selectFields.add(massagedField);
        }
    }

    private String getSelectString()
    {
        List selectFields = getSelectFields();
        for (int j = 0; j < selectFields.size(); j++)
        {
            Log.customer.debug("*** selectFields[" + j + "]: " + (String) selectFields.get(j));
        }
        int sz = selectFields.size();
        FastStringBuffer fsb = new FastStringBuffer();
        for (int i = 0; i < sz; i++)
        {
            String field = (String) selectFields.get(i);
            fsb.append(field);
            if (i != sz - 1)
            {
                fsb.append(",");
            }
        }
        String retVal = fsb.toString();
        return retVal;
    }

    public AQLQuery buildQuery(String field, String pattern)
    {
        String retVal = Fmt.S(UserQueryString, getSelectString());
        String lsDeactivated = "(Deactivated)%";
        String lsPattern = pattern;
        int liFieldPre = field.indexOf(".");
        String lsField = field.substring(liFieldPre + 1);
        AQLQuery query = AQLQuery.parseQuery(retVal);
        Log.customer.debug("BuysenseRequisitionSupervisorNameTable: query: %s", query);
        ValueSource context = getValueSourceContext();
        ariba.user.core.User loPreparer = null;
        if (context instanceof Requisition || 
            (context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.BuysenseExemptionRequest")) || (context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.ExemptionRequest")) || (context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.BuysenseUserProfileRequest"))|| (context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.BuysenseSoleSourceRequest")) || (context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.BuysenseVMIAssetTracking"))||(context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.BuysenseODUSoleSourceRequest"))||(context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.BuysenseODUEmergencyProcurement")))
        {
            loPreparer = (ariba.user.core.User) context.getFieldValue("Preparer");
        }
        //Added by SRINI for CSPL-1770, Actually below variables shifted from bottom of the file to here 
        String conditionisnull = "pu.User.IsContractor IS NULL";
        String conditionisisfalse = "pu.User.IsContractor = FALSE";
        AQLCondition POAQLArchivedCondition1 = AQLCondition.parseCondition(conditionisnull);
        AQLCondition POAQLArchivedCondition2 = AQLCondition.parseCondition(conditionisisfalse);
        AQLCondition POAQLArchivedConditionor = AQLCondition.buildOr(POAQLArchivedCondition1, POAQLArchivedCondition2);
        // Added null check as part of CSPL-2066
        if (context!=null && (context instanceof LaborLineItemDetails || context.toString().indexOf("ConsultingLineItemDetails") >= 0))
        {
            loPreparer = (ariba.user.core.User) context.getDottedFieldValue("LineItem.LineItemCollection.Preparer");
            Log.customer.debug("BuysenseRequisitionSupervisorNameTable: loPreparer " + loPreparer);
        }
        if (loPreparer.hasRole(Role.getRole("eVA-OnBehalfOf")))
        {
            query.andLike(lsField, lsPattern);
            //Added by SRINI for CSPL-1770
            query.and(POAQLArchivedConditionor);
            query.andNotLike("Name.PrimaryString", lsDeactivated);
            Log.customer.debug("BuysenseRequisitionSupervisorNameTable: eva-OnBehalfOf Role query " + query);
            return query;
        }
        BaseObject lOClientName = null;
        BaseId lOClientID = null;
        Iterator roleIter = loPreparer.getAllRoles().iterator();
        Role oRole = null;
        Object obj = null;
        String sRole = null;
        AQLQuery queryClient = null;
        String KeyAgency = "pu.User";
        while (roleIter.hasNext())
        {
            obj = roleIter.next();
            oRole = (Role) Base.getSession().objectFromId((BaseId) obj);
            sRole = oRole.getName().getPrimaryString();
            Log.customer.debug("BuysenseRequisitionSupervisorNameTable::User:: %s has Role: %s", loPreparer.getName()
                    .getPrimaryString(), sRole);
            if (sRole.indexOf("-OnBehalfOf") != -1)
            {
                queryClient = new AQLQuery("ariba.core.BuysenseClient");
                queryClient.andLike("ClientName", sRole.substring(0, 4) + "%");
                Object ClientName = Base.getSession()
                        .objectFromId(
                                Base.getService().objectMatching(queryClient,
                                        new AQLOptions(Base.getSession().getPartition())));
                AQLQuery queryInner = new AQLQuery("ariba.common.core.User", ListUtil.list("User"));
                queryInner.andEqual("ClientName", ClientName);
                try
                {
                    query.or(AQLCondition
                            .buildIn(new AQLFieldExpression(KeyAgency), AQLQuery.buildSubquery(queryInner)));
                    Log.customer.debug("BuysenseRequisitionSupervisorNameTable:query with OnBehalfOf roles  %s", query);
                }
                catch (Exception ex2)
                {
                    Log.customer.debug("SubQuery Exception: %s", ex2);
                }
            }
        }
        lOClientName = (BaseObject) getClientName(context);
        lOClientID = lOClientName.getBaseId();
        AQLQuery queryInner = new AQLQuery("ariba.common.core.User", ListUtil.list("User"));
        queryInner.andEqual("ClientName", lOClientID);
        try
        {
            query.or(AQLCondition.buildIn(new AQLFieldExpression(KeyAgency), AQLQuery.buildSubquery(queryInner)));
            Log.customer.debug("BuysenseRequisitionSupervisorNameTable:query in second try:  %s", query);
        }
        catch (Exception ex2)
        {
            Log.customer.debug("SubQuery Exception: %s", ex2);
        }
        query.andLike(lsField, lsPattern);
        query.and(POAQLArchivedConditionor);        
        query.andNotLike("Name.PrimaryString", lsDeactivated);
        Log.customer.debug("BuysenseRequisitionSupervisorNameTable: Final Query " + query);        
        return query;
    }

    private ValueSource getClientName(ValueSource context)
    {
        if (context instanceof ariba.purchasing.core.Requisition || 
            (context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.BuysenseExemptionRequest")) || (context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.ExemptionRequest")) || (context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.BuysenseUserProfileRequest")) || (context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.BuysenseSoleSourceRequest"))||(context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.BuysenseVMIAssetTracking"))||(context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.BuysenseODUSoleSourceRequest"))||(context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.BuysenseODUEmergencyProcurement")))
        {
        	Log.customer.debug("BuysenseRequisitionSupervisorNameTable::First if called, context is " + context);
            ariba.user.core.User RequesterUser = (ariba.user.core.User) context.getDottedFieldValue("Preparer");
            ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,
                    Base.getSession().getPartition());
            return (ValueSource) RequesterPartitionUser.getDottedFieldValue("ClientName");
        }
        else if (context != null
                && (context instanceof LaborLineItemDetails || context.toString().indexOf("ConsultingLineItemDetails") >= 0))
        {
            Log.customer.debug("BuysenseRequisitionSupervisorNameTable: Labor Line item Details " + context);
            ariba.user.core.User PreparerUser = (ariba.user.core.User) context
                    .getDottedFieldValue("LineItem.LineItemCollection.Preparer");
            Log.customer.debug("BuysenseRequisitionSupervisorNameTable: PreparerUser " + PreparerUser);
            ariba.common.core.User PreparerPartitionUser = ariba.common.core.User.getPartitionedUser(PreparerUser, Base
                    .getSession().getPartition());
            Log.customer.debug("BuysenseRequisitionSupervisorNameTable: Return Client Name "
                    + (ValueSource) PreparerPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName"));
            return (ValueSource) PreparerPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName");
        }
        else
            return null;
    }
}
