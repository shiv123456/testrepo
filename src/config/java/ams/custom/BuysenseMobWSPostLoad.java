package config.java.ams.custom;

import java.util.List;

import ariba.approvable.core.Access;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableUtil;
import ariba.approvable.core.BadTokenException;
import ariba.approvable.core.Comment;
import ariba.approvable.core.Folder;
import ariba.approvable.core.SimpleRecord;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseSession;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.user.core.User;
import ariba.util.core.Date;
import ariba.util.core.ListUtil;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseMobWSPostLoad extends Action
{
    private BaseId approverId;
    private BaseId approvableId;
    private BaseId delagateeId;
    private int    action;
    private String sComment;
    private boolean bProprietaryAndConfidential;
    private boolean bExternalComment;
    
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {

        log("BuysenseMobWSPostLoad fire has been called");
        ClusterRoot bwso = null;
        try
        {
            if (valuesource == null)
            {
                log("valuesource is null");
                return;
            }
            else if (valuesource instanceof ClusterRoot)
            {
                bwso = (ClusterRoot) valuesource;
            }
            else
            {
                log("valuesource: " + valuesource.getClass().getName());
                log("valuesource is not instance of BuysenseMobWSApprovalAuditInfo");
                return;
            }
            getValues(bwso);
            setValues(bwso);
            int iStatusCode = approveDeny(bwso);

            if (iStatusCode == 1)
            {
                bwso.setDottedFieldValue("StatusCode", iStatusCode);
                bwso.setDottedFieldValue("Status", "WARNING");
                if (StringUtil.nullOrEmptyOrBlankString((String) bwso.getDottedFieldValue("StatusMessage")))
                bwso.setDottedFieldValue("StatusMessage", "Approval request processed succesfully");
                log("WARNING status is set for BuysenseMobWSApprovalAuditInfo");
                addHistoryRecord(bwso);
            }
            else if (iStatusCode == 0)
            {
                bwso.setDottedFieldValue("StatusCode", iStatusCode);
                bwso.setDottedFieldValue("Status", "SUCCESS");
                if (StringUtil.nullOrEmptyOrBlankString((String) bwso.getDottedFieldValue("StatusMessage")))
                bwso.setDottedFieldValue("StatusMessage", "Approval request processed succesfully");
                log("SUCCESS status is set for BuysenseMobWSApprovalAuditInfo");
                addHistoryRecord(bwso);
            }
            else if (iStatusCode == -2)
            {
                bwso.setDottedFieldValue("StatusCode", iStatusCode);
                bwso.setDottedFieldValue("Status", "FAILURE");
                bwso.setDottedFieldValue("StatusMessage", "Some of required inputs are invalid or null");
                log("FAILURE status is set for BuysenseMobWSApprovalAuditInfo");
            }
            else if (iStatusCode == 8)
            {
                bwso.setDottedFieldValue("StatusCode", iStatusCode);
                bwso.setDottedFieldValue("Status", "FAILURE");
                bwso.setDottedFieldValue("StatusMessage", "Trying to approve an approved Req");
                log("FAILURE status is set for BuysenseMobWSApprovalAuditInfo");
            }
            else
            {
                bwso.setDottedFieldValue("StatusCode", iStatusCode);
                bwso.setDottedFieldValue("Status", "FAILURE");
                bwso.setDottedFieldValue("StatusMessage", "Approval request processing FAILED");
            }
            bwso.setDottedFieldValue("ProcessedDate", Date.getNow());
        }
        catch (BadTokenException e)
        {
            bwso.setDottedFieldValue("StatusCode", -1);
            bwso.setDottedFieldValue("Status", "FAILURE");
            bwso.setDottedFieldValue("StatusMessage", e.getErrorOrWarningMessage());
            bwso.setDottedFieldValue("ProcessedDate", Date.getNow());
            log("BadTokenException in run() " + e.getErrorOrWarningMessage());
        }
        catch (Exception e)
        {
            bwso.setDottedFieldValue("StatusCode",-2);
            bwso.setDottedFieldValue("Status", "FAILURE");
            bwso.setDottedFieldValue("StatusMessage", "EXCEPTION in Ariba while processing the request");
            bwso.setDottedFieldValue("ProcessedDate", Date.getNow());
            log("Exception in run() " + e.getMessage());
        }
    }

    private void setValues(ClusterRoot bwso)
    {
        try
        {
            String sApprovableBID = (String) bwso.getDottedFieldValue("ApprovableBaseId");
            BaseId approvablebid = BaseId.nullSafeParse(sApprovableBID);
            bwso.setDottedFieldValue("Approvable", (Approvable) approvablebid.get());

            String sApprBID = (String) bwso.getDottedFieldValue("ApproverBaseId");
            BaseId apprbid = BaseId.nullSafeParse(sApprBID);
            bwso.setDottedFieldValue("ApproverUser", (User) apprbid.get());

            String sDelagateeBID = (String) bwso.getDottedFieldValue("DelagateeBaseId");
            BaseId dbid = BaseId.nullSafeParse(sDelagateeBID);
            bwso.setDottedFieldValue("DelagateeUser", (User) dbid.get());

        }
        catch (Exception e)
        {
            log("Exception in setValues() " + e.getMessage());
        }

    }

    private void addHistoryRecord(ClusterRoot bwso)
    {
        Approvable approvable = (Approvable) bwso.getDottedFieldValue("Approvable");
        User aribaSystemUser = User.getAribaSystemUser();
        Comment commentObj = null;
        if (!StringUtil.nullOrEmptyOrBlankString(sComment))
        {
            commentObj = new Comment(approvable.getPartition());
            commentObj.setBody(sComment);
            commentObj.setDate(Date.getNow());
        }
        new SimpleRecord(approvable, aribaSystemUser, null, "PDAApprovalRecord");
    }

    private void getValues(ClusterRoot bwso)
    {
        log("In getValues()");
        approverId = BaseId.parse((String) bwso.getDottedFieldValue("ApproverBaseId"));
        approvableId = BaseId.parse((String) bwso.getDottedFieldValue("ApprovableBaseId"));
        delagateeId = BaseId.parse((String) bwso.getDottedFieldValue("DelagateeBaseId"));
        action = (Integer) bwso.getDottedFieldValue("Action");
        sComment = (String) bwso.getDottedFieldValue("Comment");
        Object oProprietaryAndConfidential = bwso.getDottedFieldValue("ProprietaryAndConfidential");
        if(oProprietaryAndConfidential != null)
        {
            bProprietaryAndConfidential = (Boolean) oProprietaryAndConfidential;
        }else
        {
            bProprietaryAndConfidential = false;
        }
        Object oExternalComment = bwso.getDottedFieldValue("ExternalComment");
        if(oExternalComment != null)
        {
            bExternalComment = (Boolean) oExternalComment;
        }else
        {
            bExternalComment = false;
        }
        log("getValues() returning - " + "approverId:" + approverId + ",approvableId" + approvableId + "delagateeId"
                + delagateeId);
        log("getValues() returning - " + ",action " + action + ",comment" + sComment);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private int approveDeny(ClusterRoot bwso) throws Exception
    {
        log("In approve()");
        BaseSession session;
        Partition origPartition;
        BaseId origEffectiveUserId;
        BaseId origRealUserId;
        String fromEmail = null;
        if (approverId != null && approvableId != null && delagateeId != null && (action == 1 || action == 2 || action == 3))
        {
            //int iResult = Approvable.approveOrDenyByNonSessionUser(approverId, approvableId, delagateeId, " ", action, sComment); 
            Approvable approvable = (Approvable) bwso.getDottedFieldValue("Approvable");
            ariba.user.core.Approver approver = (ariba.user.core.Approver) bwso.getDottedFieldValue("ApproverUser");
            //Approver delagatee = (Approver) bwso.getDottedFieldValue("DelagateeUser");

            session = Base.getSession();
            origPartition = session.getPartition();
            origEffectiveUserId = session.getEffectiveUserId();
            origRealUserId = session.getRealUserId();
            if (origPartition == null)
            {
                origPartition = approvable.getPartition();
            }
            if (origEffectiveUserId == null)
            {
                session.setEffectiveUser(approverId);
            }
            if (origRealUserId == null)
            {
                session.setRealUser(approverId, ((User) approver).getLocale());
            }

            Comment commentObj = null;
            if (!StringUtil.nullOrEmptyOrBlankString(sComment))
            {
                commentObj = new Comment(approvable.getPartition());
                commentObj.setBody(sComment);
                commentObj.setDate(Date.getNow());
                commentObj.setDottedFieldValue("ProprietaryAndConfidential", bProprietaryAndConfidential);                
                commentObj.setExternalComment(bExternalComment);
            }

            int returnCode = -1;
            switch (action)
            {
                case 1:
                    log("In approve action");
                    returnCode = approvable.getAccess(3, approver, null);
                    log("returnCode: " + returnCode);
                    log("Access.Now: " + Access.Now);
                    if (returnCode == Access.Now)
                    {
                        List v = ListUtil.list();
                        try
                        {
                            v = ApprovableUtil.getService().getApprovableServer().checkApprove(approvable);
                        }
                        catch (Exception e)
                        {

                        }
                        int iHookReturnCode = ((Integer) ListUtil.firstElement(v)).intValue();
                        log("iHookReturnCode: " + iHookReturnCode);
                        if (iHookReturnCode > 0) // meaning hook returned warning message
                        {
                            bwso.setDottedFieldValue("StatusMessage","Approval request processed succesfully insipte of warning message: "+ v.get(1).toString().substring(0, Math.max(150, v.get(1).toString().length())));
                            return 1;
                        }
                        
                        if (commentObj != null)
                            commentObj.setType(8);
                        Folder approveFolder = null;
                        Folder archiveFolder = null;
                        if (approver instanceof User)
                        {
                            approveFolder = Folder.lookupFolder(2, (User) approver, approvable);
                            archiveFolder = Folder.lookupFolder(4, (User) approver, approvable);
                        }

                        try
                        {
                            if (v == null || v.isEmpty() || (iHookReturnCode == 0))
                                returnCode = ApprovableUtil.getService().getApprovableServer().approve(approvable, approver, commentObj, approveFolder, archiveFolder, fromEmail);
                        }
                        catch (Exception e)
                        {
                            log("Exception while approving: " + e.getMessage());
                        }

                        if (iHookReturnCode < 0)
                        {
                            String sErrorMessages = "";
                            for (int i = 1; i < v.size(); i++)
                            {
                                sErrorMessages = sErrorMessages + v.get(i).toString();
                            }
                            throw new BadTokenException("ApprovalHookFailed", sErrorMessages);
                        }
                    }
                    return returnCode;

                case 2:
                    log("In deny action");
                    if (commentObj != null)
                        commentObj.setType(16);
                    Folder approveFolder = null;
                    Folder archiveFolder = null;
                    if (approver instanceof User)
                    {
                        approveFolder = Folder.lookupFolder(2, (User) approver, approvable);
                        archiveFolder = Folder.lookupFolder(4, (User) approver, approvable);
                    }
                    returnCode = ApprovableUtil.getService().getApprovableServer().deny(approvable, approver, commentObj, approveFolder, archiveFolder, fromEmail);
                    return returnCode;
                case 3:
                    log("In force approve action");
                    returnCode = approvable.getAccess(3, approver, null);
                    log("returnCode: " + returnCode);
                    log("Access.Now: " + Access.Now);
                    if (returnCode == Access.Now)
                    {
                        if (commentObj != null)
                            commentObj.setType(8);
                        Folder approveFolder1 = null;
                        Folder archiveFolder1 = null;
                        if (approver instanceof User)
                        {
                            approveFolder1 = Folder.lookupFolder(2, (User) approver, approvable);
                            archiveFolder1 = Folder.lookupFolder(4, (User) approver, approvable);
                        }
                        List v = ListUtil.list();
                        try
                        {
                            v = ApprovableUtil.getService().getApprovableServer().checkApprove(approvable);
                        }
                        catch (Exception e)
                        {

                        }
                        int iHookReturnCode = ((Integer) ListUtil.firstElement(v)).intValue();
                        log("iHookReturnCode: " + iHookReturnCode);
                        try
                        {
                            if (v == null || v.isEmpty() ||  iHookReturnCode > 0)
                                returnCode = ApprovableUtil.getService().getApprovableServer().approve(approvable, approver, commentObj, approveFolder1, archiveFolder1, fromEmail);
                        }
                        catch (Exception e)
                        {
                            log("Exception while approving: " + e.getMessage());
                        }
                        if (iHookReturnCode > 0)
                        {
                            bwso.setDottedFieldValue("StatusMessage","Approval request processed succesfully insipte of warning message/s: ");
                        }
                        if (iHookReturnCode < 0)
                        {
                            String sErrorMessages = "";
                            for (int i = 1; i < v.size(); i++)
                            {
                                sErrorMessages = sErrorMessages + v.get(i).toString();
                            }
                            throw new BadTokenException("ApprovalHookFailed", sErrorMessages);
                        }
                    }
                    return returnCode;
                default:
                    return returnCode;
            }
        }
        else
        {
            log("some of required inputs are null");
            return Integer.parseInt("-2");
        }

    }

    private void log(String s)
    {
        Log.customer.debug(s);
    }
}
