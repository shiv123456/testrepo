/*
 * @(#) BuysenseNonCatCommodityNameTable.java     1.0 07/24/2008
 *
 * Copyright 2008 by CGI
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of CGI ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with CGI
 */

package config.java.ams.custom;

import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLNameTable;
import ariba.base.fields.*;
import java.util.List;
import ariba.util.log.Log;

public class BuysenseNonCatCommodityNameTable extends AQLNameTable
implements InitializeValueSource
{
/****************************************************************************************************************
 *
 * This BuysenseNonCatCommodityNameTable is a nameTableClass properties for the ReqLineItem CommodityCode field.
 * It extends the AQLNameTable to resolve CSPLU-126 that user could not change and search commodity code
 * in non-catalog line item.
 *
 ****************************************************************************************************************/

    public void initializeValueSource(ValueSource vs)
    {
        Log.customer.debug("Calling BuysenseNonCatCommodityNameTable vs ="+vs);
    }
    public List matchPattern (String field, String pattern)
    {
        List results = super.matchPattern(field, pattern);
        return results;
    }
    public void addQueryConstraints(AQLQuery query, String field, String pattern) {
        super.addQueryConstraints(query, field, pattern);
        query.andNotEqual("UniqueName", "[Unspecified]");
    }
}
