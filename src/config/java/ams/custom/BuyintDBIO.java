package config.java.ams.custom;

import java.sql.*;
import javax.naming.*;
import javax.sql.*;

public class BuyintDBIO
{
   /** JNDI Name for datasource for current instance */
   private String moJNDIName = null;

   /** Fields for JDBC access to current instance */
   private static final String DBDRIVER_ORACLE = "oracle.jdbc.driver.OracleDriver";
   private boolean mboolIsOffline = true;
   private String moDBURL = null;
   private String moDBUser = null;
   private String moDBPassword = null;

   /** Constructor expects DataSource JNDI name to be passed
       There is no default constructor.
    */
   public BuyintDBIO(String foJNDIName)
   {
      mboolIsOffline = false;
      moJNDIName = foJNDIName;
   }

   /** Constructor expects URL, User and Password.
       There is no default constructor.
    */
   public BuyintDBIO(String fsURL, String fsUser, String fsPassword)
   {
      mboolIsOffline = true;
      moDBURL = fsURL;
      moDBUser = fsUser;
      moDBPassword = fsPassword;
   }

   public Connection getConnection()
   {
          BuysenseUtil.showCallerInfo(new Throwable(), this.getClass().getName(), this);

          Connection loConn = null;
          if (mboolIsOffline)
          {
             loConn = getConnection(moDBURL, moDBUser, moDBPassword);
          }
          else
          {
             loConn = getConnection(moJNDIName);
          }
          return loConn;
   }

   public static Connection getConnection(String fsURL, String fsUser, String fsPassword)
   {
      Connection loConn = null;
      try
      {
         Class.forName(DBDRIVER_ORACLE);
         loConn = DriverManager.getConnection(fsURL,fsUser, fsPassword);
      }
      catch (SQLException loEx1)
      {
         loEx1.printStackTrace();
      }
      catch (ClassNotFoundException loEx2)
      {
         loEx2.printStackTrace();
      }
      return loConn;
   }

   public static Connection getConnection(String fsJNDIName)
   {
      Connection loConn = null;
      try
      {
         InitialContext loContext = new InitialContext();
         DataSource loSrc = (DataSource)loContext.lookup(fsJNDIName);
         loConn = loSrc.getConnection();
      }
      catch (SQLException loEx1)
      {
         loEx1.printStackTrace();

      }
      catch (NamingException loEx2)
      {
         loEx2.printStackTrace();
      }
      return loConn;
   }

   public static void closeConnection(Connection foConn)
   {
      try
      {
         foConn.close();
      }
      catch(SQLException loEx)
      {
         // ignore
      }
   }
}
