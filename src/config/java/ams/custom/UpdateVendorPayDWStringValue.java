/*
 * @(#)UpdateVendorPayDWStringValue.java     1.0 23/10/2008
 *
 * Copyright 2008 by CGI
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of CGI ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with CGI
 *
 * Author: Mommasani Srinivasulu Reddy
   Comments: This java class is called from ReqExtrinsicFields.aml. Field VendorPayDWString is added for CSPL-699.
 *
 */

package config.java.ams.custom;

import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import ariba.util.log.Log;
import ariba.util.core.PropertyTable;
import ariba.purchasing.core.Requisition;

public class UpdateVendorPayDWStringValue extends Action
{
    private static final String trueValue = "true";
    private static final String falseValue = "false";

    public void fire (ValueSource object, PropertyTable params)
    {
        Log.customer.debug("Calling UpdateVendorPayDWStringValue");
        Requisition loReq = (Requisition) object;
    	if(object == null){
    	   	return;
    	}else{
			if(loReq.getFieldValue("VendorPay") == null){
				loReq.setFieldValue("VendorPayDWString", null);
			}
			else{
				boolean bVendorPay = ((Boolean)loReq.getFieldValue("VendorPay")).booleanValue();
				mUpdateVendorPayDWString(bVendorPay, loReq);
			}
		}
		Log.customer.debug("UpdateVendorPayDWStringValue::fire::VendorPayDWString" +loReq.getFieldValue("VendorPayDWString"));
	}

	public void mUpdateVendorPayDWString(boolean lbVendorPay, Requisition req){
		if(lbVendorPay)
			req.setFieldValue("VendorPayDWString", trueValue);
		else if(!lbVendorPay)
			req.setFieldValue("VendorPayDWString", falseValue);
	}
}
