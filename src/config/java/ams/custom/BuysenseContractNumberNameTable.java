package config.java.ams.custom;


import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Calendar;
import java.util.TimeZone;
import ariba.base.core.Base;
import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLNameTable;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.SearchTermQuery;
import ariba.base.fields.ValueSource;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.user.core.User;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;
import ariba.common.core.Supplier;

public class BuysenseContractNumberNameTable extends AQLNameTable
{
    String className = "BuysenseContractNumberNameTable";

    public void addQueryConstraints(AQLQuery query, String field, String pattern, SearchTermQuery searchTermQuery)
    {
        super.addQueryConstraints(query, field, pattern, searchTermQuery);
        Log.customer.debug(className + " initial query: " + query);
        // Here add the condition to filter the Contracts for that User Agency and selected Supplier Only
        ValueSource valueSourceCurrent = getValueSourceContext();
        if (valueSourceCurrent instanceof ReqLineItem)
        {
            ReqLineItem reqLI = (ReqLineItem) valueSourceCurrent;
            if (reqLI != null)
            {
                /**************************************************************************************************
                * CSPL-1890 
                * Use supplier uniquename to return BuysenseContracts instead of using supplierlocation uniquename
                ****************************************************************************************************/
                Supplier loSupplier = reqLI.getSupplier();
                if (loSupplier == null)
                {
                    query.andFalse();
                    return;
                }
                else
                {
                    String supplierId = loSupplier.getUniqueName();
                    query.and(AQLCondition.parseCondition("SupplierID in('" + supplierId + "','ALL')"));
                    //Get On Behalf Of (Requester) client name instead of reqLine.
                    Requisition req = (Requisition) reqLI.getLineItemCollection();
                    User requester = req.getRequester();
                    ariba.common.core.User partitionedUser = ariba.common.core.User.getPartitionedUser(requester, req
                            .getPartition());
                    String sClinetUniqueName = (String) partitionedUser.getDottedFieldValue("ClientName.ClientName");
                    sClinetUniqueName = sClinetUniqueName.substring(0, 4);
                    if (!StringUtil.nullOrEmptyString(sClinetUniqueName))
                    {
                        query.and(AQLCondition.parseCondition("AuthorizedAgency  in ('" + sClinetUniqueName
                                + "','ALL')"));
                    }
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    df.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
                    String lsDateTime = df.format(Calendar.getInstance().getTime());
                    lsDateTime = lsDateTime +" 00:00:00";
                    query.and(AQLCondition.parseCondition("EffectiveBeginDate <= CurrentDate()"));
                    query.and(AQLCondition.parseCondition("EffectiveEndDate >= CalendarDate('" + lsDateTime + "')"));
                    // Note:  CurrentDate() < EndDate does't test for HH:MM precision. It only checks by date.
                    // 22 Feb 2010 08:30:00 < 22 Feb 2010 05:00:00  returns results though the contract EXPIRED @ 05:00
                    // 22 Feb 2010 08:30:00 < 21 Feb 2010 05:00:00  doesn't return results.
                    // Comparisons are in GMT                    
                }
            } // end of 'reqLI != null'
        }// end of 'valueSourceCurrent instanceof ReqLineItem'
        else
        {
            // don't add any thing to query
            query.andFalse();
            Log.customer.debug(className + " No constraints are added to the Query : ");
        }
    }

    /*CSPL-8740: Truncate contract description in Non-Catalog Drop-Down
     This method returns the truncated string for drop down value*/
    public String getTitleForObject(ValueSource object, Locale locale)
    {
        String sTitle = super.getTitleForObject(object, locale);
        Log.customer.debug(className + " Title from Name Table: " + sTitle);
        if (!StringUtil.nullOrEmptyString(sTitle))
        {
            int iDescCharWidth = 60;
            int iTitleLength = sTitle.length();
            int iDescIndex = sTitle.lastIndexOf("|");
            int iDescLength = iTitleLength - (iDescIndex + 1);
            String moreString = ResourceService.getString("ariba.html.baseui", "MoreString", Base.getSession().getLocale());
            int moreStringLength = moreString.length();
            if (iDescLength > iDescCharWidth)
            {
                sTitle = StringUtil.strcat(sTitle.substring(0, iDescIndex + iDescCharWidth - moreStringLength), moreString);
                Log.customer.debug(className + " Title after truncating: " + sTitle);
            }
        }
        return sTitle;
    }
}
