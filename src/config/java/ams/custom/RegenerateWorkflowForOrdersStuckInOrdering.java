package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.Partition;
import java.util.Map;

import org.apache.log4j.Level;

import ariba.common.core.Log;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLResultCollection;
import ariba.util.core.Fmt;
import ariba.base.core.aql.AQLScalarExpression;
import ariba.util.scheduler.Scheduler;
import ariba.util.scheduler.SimpleScheduledTask;
import ariba.purchasing.core.OrderRecipient;
import ariba.server.workflowserver.WorkflowService;
import ariba.server.workflowserver.WorkflowEventListener;


// New version adds code to the resetRequisitionFieldValues method
// that removes any record of the orders from the Requisition history tab

//81->822 changed Hashtable to Map
//81->822 changed Enumeration to Iterator
public class RegenerateWorkflowForOrdersStuckInOrdering extends SimpleScheduledTask
{

     private boolean mboolFixData = false;

     public void init (Scheduler scheduler,
                      String    scheduledTaskName,
                      Map arguments)

    {
        super.init(scheduler, scheduledTaskName, arguments);

        java.util.Iterator e = arguments.keySet().iterator();
 
        while (e.hasNext()) 
        {
            String key = (String)e.next();
            if (key.equals("FixData")) 
            {
                String argument = (String)arguments.get(key);
                mboolFixData = false;
                if (argument.equalsIgnoreCase("TRUE"))
                {
                    mboolFixData = true;
                }
            }
        }
    }

    public void run ()
    {
        Log.customer.debug("In RegenerateWorkflowForOrdersStuckInOrdering");

        Partition partition = Base.getService().getPartition();

        AQLResultCollection results = getOrderRecipientsToProcess(partition);

        Logs.buysense.setLevel(Level.DEBUG);
        while (results.next()) {
            Log.customer.debug("RegenerateWorkflowForOrdersStuckInOrdering in while processing result");
            OrderRecipient orrec  = (OrderRecipient)Base.getSession().objectForWrite(results.getBaseId(0));
            String po = orrec.getPurchaseOrder().getUniqueName();
            Logs.buysense.debug("RegenerateWorkflowForOrdersStuckInOrdering processing orre: " + orrec + " corresp to PO: " + po);
            regenerateWorkflow(partition, orrec);
            if (mboolFixData)
            {
                Base.getSession().transactionCommit();
            }
            Logs.buysense.debug("RegenerateWorkflowForOrdersStuckInOrdering finished processing orre: " + orrec);
        }
        Logs.buysense.setLevel(Level.DEBUG.OFF);
    }



    public void regenerateWorkflow (Partition partition, OrderRecipient orrec)
    {
        Log.customer.debug("In RegenerateWorkflowForOrdersStuckInOrdering regenerateWorkflow");

        String WorkflowEventType = getWorkFlowEventType(partition, orrec);
        Log.customer.debug("RegenerateWorkflowForOrdersStuckInOrdering regenerateWorkflow WorkflowEventType: " + WorkflowEventType);

        if (mboolFixData)
        {
            Logs.buysense.debug("RegenerateWorkflowForOrdersStuckInOrdering about to invoke fireWorkflowEvent for orre: " + orrec);
            WorkflowService.getService().fireWorkflowEvent(orrec, WorkflowEventType);
            Base.getSession().transactionCommit();
            Logs.buysense.debug("RegenerateWorkflowForOrdersStuckInOrdering done with fireWorkflowEvent for orre: " + orrec);
        }
        else
        {
            Logs.buysense.debug("RegenerateWorkflowForOrdersStuckInOrdering report-only ... fireWorkflowEvent not invoked - orre: " + orrec);
        }
    }


    public String getWorkFlowEventType (Partition partition, OrderRecipient orrec)
    {

        Log.customer.debug("In RegenerateWorkflowForOrdersStuckInOrdering getWorkFlowEventType");

        String WorkFlowEventType = "";

        String querytext =  ("select " +
                            "wfel " +
                            "from " +
                            "WorkflowEventListener wfel " +
                            "where " +
                            "wfel.WorkflowBusinessObject = %s " +
                            "and " +
                            "wfel.TransitionName = 'CREATED_TO_SENDING' ");
        querytext = Fmt.S(querytext,
                          AQLScalarExpression.buildLiteral(orrec));


        Log.customer.debug("In RegenerateWorkflowForOrdersStuckInOrdering getWorkFlowEventType querytext: " + querytext);

        AQLOptions options = new AQLOptions(partition, false);

        AQLResultCollection results = Base.getService().executeQuery(querytext,options);

        while (results.next()) {
            Log.customer.debug("In RegenerateWorkflowForOrdersStuckInOrdering getWorkFlowEventType in while");
            WorkflowEventListener wfel  = (WorkflowEventListener)Base.getSession().objectForWrite(results.getBaseId(0));
            Log.customer.debug("RegenerateWorkflowForOrdersStuckInOrdering getWorkFlowEventType wfel: " + wfel);

            WorkFlowEventType = wfel.getWorkflowEventType();
            Log.customer.debug("RegenerateWorkflowForOrdersStuckInOrdering getWorkFlowEventType WorkFlowEventType: " + WorkFlowEventType);
        }

        return WorkFlowEventType;
    }


    private AQLResultCollection getOrderRecipientsToProcess (Partition partition)
    {
        
        Log.customer.debug("In RegenerateWorkflowForOrdersStuckInOrdering getOrderRecipientsToProcess");
        

        String querytext = ("select orrec " +
                            "from OrderRecipient orrec " +
                            "where " +
                            "orrec.State = 1 " +
                            "and " +
                            "orrec.PurchaseOrder.Active = true " +
                            "and " +
                            "orrec.PurchaseOrder.StatusString = 'Ordering' " +
                            "and " +
                            "exists " +
                            "(" +
                            "select " +
                            "wfl " +
                            "from " +
                            "Workflow wfl " +
                            "where " +
                            "WorkflowBusinessObject = orrec " +
                            "and " +
                            "CurrentStateName = 'CREATED' " +
                            ")" +
                            "and " +
                            "exists " +
                            "(" +
                            "select " +
                            "wfel " +
                            "from " +
                            "WorkflowEventListener wfel " +
                            "where " +
                            "wfel.WorkflowBusinessObject = orrec " +
                            "and " +
                            "wfel.TransitionName = 'CREATED_TO_SENDING' " +
                            ")");



        querytext = Fmt.S(querytext);

        Log.customer.debug("RegenerateWorkflowForOrdersStuckInOrdering getOrderRecipientsToProcess querytext: " + querytext);
        AQLOptions options = new AQLOptions(partition, false);

        AQLResultCollection results = Base.getService().executeQuery(querytext,options);

        return results;
    }

}
