package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.procure.core.LineItemProductDescription;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class SetOrderAttributesDisplayName extends Action
{
   public void fire(ValueSource object, PropertyTable param) throws ActionExecutionException
   {
	  Log.customer.debug("Calling SetOrderAttributesDisplayName.java"+object);
	  Requisition Req = (Requisition) object;
	  if(Req.getFieldValue("StatusString")=="Submitted")
	  {
	     BaseVector loLineItems = (BaseVector)Req.getFieldValue("LineItems");
	     LineItemProductDescription LiProdDesc= null;
	     Partition NonePartition = null;
	     BaseObject AttributeImplObj=null;
	     boolean btrue= true;
	     Boolean Btrue= Boolean.valueOf(btrue);
	     String AttributeImpDisplayName = null;
	     for(int c=0;c<loLineItems.size();c++)
	     {
	        ReqLineItem loLineItem =(ReqLineItem)loLineItems.get(c);
			BaseObject CategoryLineItemDetails = ((BaseObject)loLineItem.getFieldValue("CategoryLineItemDetails"));
			if (CategoryLineItemDetails==null)
			{
			   continue;
			}
			LiProdDesc = (LineItemProductDescription)loLineItem.getDottedFieldValue("Description");
		    String sUpdatedCatalogItemType = (String) LiProdDesc.getDottedFieldValue("UpdatedCatalogItemType");
		    sUpdatedCatalogItemType = sUpdatedCatalogItemType.substring(7);
		    AQLQuery userQuery = AQLQuery.parseQuery("SELECT s from ariba.catalog.base.core.ComplexTypeImpl s");
		    userQuery.andEqual("Name",sUpdatedCatalogItemType);
		    userQuery.andEqual("IsLatest",Btrue);
		    NonePartition = Base.getService().getPartition("None");
		    AQLOptions options = new AQLOptions(NonePartition);
		    Log.customer.debug("userQuery before execution"+userQuery);
		    AQLResultCollection ComplexType = (AQLResultCollection)Base.getService().executeQuery(userQuery, options);
		    Log.customer.debug("Querry Executed ComplexType"+ComplexType);
		    if (ComplexType.next() == false)
		    {
			   return;
		    }
		    BaseObject ComplexTypeImplObj =(BaseObject)Base.getSession().objectFromId(ComplexType.getBaseId(0));
		    Log.customer.debug("ComplexTypeImplObj"+ComplexTypeImplObj);
		    BaseVector OrderAttributes = (BaseVector)LiProdDesc.getFieldValue("OrderAttributes");
		    Log.customer.debug("OrderAttributes size"+OrderAttributes+" "+OrderAttributes.size());
		    for (int i=0;i<OrderAttributes.size();i++)
		    {
			   BaseObject NamedPairObj=(BaseObject)OrderAttributes.get(i);
			   String name = (String)NamedPairObj.getFieldValue("Name");
			   BaseVector ComplexAttributes = (BaseVector)ComplexTypeImplObj.getFieldValue("Attributes");
			   for (int j=0;j<ComplexAttributes.size();j++)
			   {
				   AttributeImplObj = (BaseObject)Base.getSession().objectFromId((BaseId) ComplexAttributes.get(j));
				   String AttributeImplName = (String)AttributeImplObj.getFieldValue("Name");
				   AttributeImpDisplayName = (String)AttributeImplObj.getDottedFieldValue("DisplayName.PrimaryString");
				   if(name.equals(AttributeImplName))
				   {
					  BaseObject enumstring =(BaseObject)AttributeImplObj.getFieldValue("Enumeration");
					  BaseVector emumparts = (BaseVector)enumstring.getFieldValue("StringEnumerationParts");
					  for(int k=0;k<emumparts.size();k++)
					  {
					     BaseObject enumpart = (BaseObject)emumparts.get(k);
						 String CanonVal = (String) enumpart.getFieldValue("CanonicalValue");
						 String value = (String)NamedPairObj.getFieldValue("Value");
						 if(value.equals(CanonVal))
						 {
						    String DisplayName = (String) enumpart.getDottedFieldValue("DisplayName.PrimaryString");
							NamedPairObj.setFieldValue("Value", DisplayName);
							NamedPairObj.setFieldValue("Name", AttributeImpDisplayName);
							break;
						 }
					  }
				   }
			    }
		     }
		  }
		  Log.customer.debug("Exiting SetOrderAttributesDisplayName.java"+object);
	   }
	   else
	   {
		  return;
	   }
	}
}
