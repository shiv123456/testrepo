
package config.java.ams.custom;

import ariba.base.core.BaseObject;
import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLNameTable;
import java.util.List;
import ariba.util.log.Log;
import ariba.base.fields.ValueSource;
import ariba.base.core.Partition;


public class BuysDynamicFieldDataNameTable extends AQLNameTable

{
public static final String ClassName="config.java.ams.custom.BuysDynamicFieldDataNameTable";
String appType = new String();

public List matchPattern (String field, String pattern)
{
    List results = super.matchPattern(field, pattern);
    return results;
}

public void addQueryConstraints (AQLQuery query, String field, String pattern)

{
    String fieldNum = null;
    String clientUniquename = null;
    String clientID = null;
    String lsProf = null;


    Log.customer.debug("Buysense: Entered the addQueryConstraints");

    ValueSource vs = getValueSourceContext();
    Log.customer.debug("This is vs: %s and the type: %s" ,vs,vs.getTypeName());
    BaseObject bo = (BaseObject)vs ;
    Log.customer.debug("Got this through getValueSource: %s ", bo);
    Log.customer.debug("This is the passed in field in addQueryConstraints: %s", field);
    String chooserField = fieldName();
    Log.customer.debug("The chooser field is :"+chooserField);

    Log.customer.debug("NameTable is executing on %s.  ",chooserField);
 
    Partition boPartition = bo.getPartition();

    if (vs.getTypeName().equals("ariba.core.BuysDynamicEform")) {
        Log.customer.debug("Working on the Dynamic Eform");
        ariba.user.core.User RequesterUser = (ariba.user.core.User)bo.getDottedFieldValue("Requester");
        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,boPartition);
        clientID = (String)RequesterPartitionUser.getDottedFieldValue("ClientName.ClientID");
        lsProf = (String)bo.getDottedFieldValue("EformChooser.ProfileName");
        appType="BuysDynamicEform";
       }
    else  { Log.customer.debug("This NameTable class can not handle the passed in Object");}

   if (clientID == null) Log.customer.debug("Got a null client");


    super.addQueryConstraints(query, field, pattern);
    beginSystemQueryConstraints(query);

    Log.customer.debug("Building conditions for ChooserField: %s, ClientID: %s and Profile Value: %s",chooserField,clientID,lsProf);
    AQLCondition fieldNumberCond =  AQLCondition.buildEqual(query.buildField("FieldName"),chooserField);
    AQLCondition clientIDCond = AQLCondition.buildEqual(query.buildField("ClientID"), clientID);
    
    AQLCondition profileCond = AQLCondition.buildEqual(query.buildField("ProfileName"),lsProf);
                                    
    Log.customer.debug("Adding just the Fieldname ,ClientID and  profile conditions");
    query.and(fieldNumberCond);
    query.and(clientIDCond);
    query.and(profileCond);


    endSystemQueryConstraints(query);
}

public AQLQuery  buildQuery(String field, String pattern)
{
 Log.customer.debug("We are ib buildQuery: field = %s ", field);
 return super.buildQuery(field,pattern);
}

private int searchFieldValues(List fieldValuesVector,String chooserField)
{
int vectorSize = fieldValuesVector.size();
for (int i =0; i<vectorSize;i++) {
    String fieldValue = (String)fieldValuesVector.get(i);
    if (fieldValue.equals(chooserField)) { Log.customer.debug("Found a match"); return i;}
}
return -1;
}
}

