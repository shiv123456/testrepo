package config.java.ams.custom;

import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.core.Base;
import ariba.base.core.BaseObject;
// Ariba 8.1: Replaced ariba.common.core.User
import ariba.user.core.User;
import ariba.purchasing.core.Requisition;

//81->822 changed ConditionValueInfo to ValueInfo
public class AMSValidateRequester extends Condition
{
   private static final ValueInfo parameterInfo[];

   public boolean evaluate(Object value, PropertyTable params)
   {
      BaseObject obj   = (BaseObject)params.getPropertyForKey("SourceObject");
      if (obj.instanceOf("ariba.purchasing.core.Requisition"))
      {
         User RequesterUser = (User)((Requisition)obj).getRequester();
         if (RequesterUser == null)
            return false;
      }
      return true;
   }

   public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
   {
      if (!evaluate(value,params))
      {
         return new ConditionResult(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core", "NullRequesterMsg"));
      }
      else
      {
         return null;
      }
   }

   protected ValueInfo[] getParameterInfo()
   {
      return parameterInfo;
   }

   static
   {
      parameterInfo = (new ValueInfo[] {
         new ValueInfo("SourceObject", 0)
        });
   }
}
