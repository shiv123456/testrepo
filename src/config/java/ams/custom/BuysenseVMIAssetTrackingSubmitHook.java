 package config.java.ams.custom;

 import java.util.List;

 import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableHook;
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

 public class BuysenseVMIAssetTrackingSubmitHook implements ApprovableHook{
 private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));
 public List run(Approvable approvable)
 {
  Approvable loAppr = approvable;
  
  Log.customer.debug("Inside BuysenseVMIAssetTrackingSubmitHook");
  
  Log.customer.debug("Setting the BuysenseReFireWorkFlow to false");
  loAppr.setFieldValue("BuysenseReFireWorkflow", new Boolean("false"));
  
  Boolean lbLine1 = true;
  Boolean lbLine2 = true;
  Boolean lbLine3 = true;
  Boolean lbLine4 = true;
  Boolean lbLine5 = true;
  Boolean lbErrorValue = false;
  String lsLineNo = null;
  
  Boolean lbITRelated1 = (Boolean)loAppr.getFieldValue("ItemITrelated1");
  Boolean lbITRelated2 = (Boolean)loAppr.getFieldValue("ItemITrelated2");
  Boolean lbITRelated3 = (Boolean)loAppr.getFieldValue("ItemITrelated3");
  Boolean lbITRelated4 = (Boolean)loAppr.getFieldValue("ItemITrelated4");
  Boolean lbITRelated5 = (Boolean)loAppr.getFieldValue("ItemITrelated5");
  
  String lsItemVMIIDNum1 = (String)loAppr.getFieldValue("ItemVMIIDNum1");
  String lsItemVMIIDNum2 = (String)loAppr.getFieldValue("ItemVMIIDNum2");
  String lsItemVMIIDNum3 = (String)loAppr.getFieldValue("ItemVMIIDNum3");
  String lsItemVMIIDNum4 = (String)loAppr.getFieldValue("ItemVMIIDNum4");
  String lsItemVMIIDNum5 = (String)loAppr.getFieldValue("ItemVMIIDNum5");
  
  String lsItemDescription1 = (String)loAppr.getFieldValue("ItemDescription1");
  String lsItemDescription2 = (String)loAppr.getFieldValue("ItemDescription2");
  String lsItemDescription3 = (String)loAppr.getFieldValue("ItemDescription3");
  String lsItemDescription4 = (String)loAppr.getFieldValue("ItemDescription4");
  String lsItemDescription5 = (String)loAppr.getFieldValue("ItemDescription5");  
  
  String lsItemModelNum1 = (String)loAppr.getFieldValue("ItemModelNum1");
  String lsItemModelNum2 = (String)loAppr.getFieldValue("ItemModelNum2");
  String lsItemModelNum3 = (String)loAppr.getFieldValue("ItemModelNum3");
  String lsItemModelNum4 = (String)loAppr.getFieldValue("ItemModelNum4");
  String lsItemModelNum5 = (String)loAppr.getFieldValue("ItemModelNum5");
  
  String lsItemSerialNum1 = (String)loAppr.getFieldValue("ItemSerialNum1");
  String lsItemSerialNum2 = (String)loAppr.getFieldValue("ItemSerialNum2");
  String lsItemSerialNum3 = (String)loAppr.getFieldValue("ItemSerialNum3");
  String lsItemSerialNum4 = (String)loAppr.getFieldValue("ItemSerialNum4");
  String lsItemSerialNum5 = (String)loAppr.getFieldValue("ItemSerialNum5");
  
  String lsItemReason1 = (String)loAppr.getFieldValue("ItemReason1");
  String lsItemReason2 = (String)loAppr.getFieldValue("ItemReason2");
  String lsItemReason3 = (String)loAppr.getFieldValue("ItemReason3");
  String lsItemReason4 = (String)loAppr.getFieldValue("ItemReason4");
  String lsItemReason5 = (String)loAppr.getFieldValue("ItemReason5");    
  
  if((!(loAppr.getDottedFieldValue("ItemITrelated1") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated1")).booleanValue())) && 
      (StringUtil.nullOrEmptyOrBlankString(lsItemVMIIDNum1))&&(StringUtil.nullOrEmptyOrBlankString(lsItemModelNum1))&&(StringUtil.nullOrEmptyOrBlankString(lsItemSerialNum1)) &&
      (StringUtil.nullOrEmptyOrBlankString(lsItemDescription1)) && (StringUtil.nullOrEmptyOrBlankString(lsItemReason1)))
        {
          Log.customer.debug("First Line is empty");
          lbLine1 = false;
        }
  else
     {
        Log.customer.debug("Line 1 isn't completely empty");
        lbLine1 = true;
     }

  if((!(loAppr.getDottedFieldValue("ItemITrelated2") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated2")).booleanValue())) && 
      (StringUtil.nullOrEmptyOrBlankString(lsItemVMIIDNum2))&&(StringUtil.nullOrEmptyOrBlankString(lsItemModelNum2))&&(StringUtil.nullOrEmptyOrBlankString(lsItemSerialNum2)) &&
      (StringUtil.nullOrEmptyOrBlankString(lsItemDescription2)) && (StringUtil.nullOrEmptyOrBlankString(lsItemReason2)))
      {
         Log.customer.debug("Second Line is empty");
           lbLine2 = false;
      }
  else
     {
        Log.customer.debug("Line 2 isn't completely empty");
        lbLine2 = true;
     }

      
   if((!(loAppr.getDottedFieldValue("ItemITrelated3") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated3")).booleanValue())) && 
       (StringUtil.nullOrEmptyOrBlankString(lsItemVMIIDNum3))&&(StringUtil.nullOrEmptyOrBlankString(lsItemModelNum3))&&(StringUtil.nullOrEmptyOrBlankString(lsItemSerialNum3)) &&
       (StringUtil.nullOrEmptyOrBlankString(lsItemDescription3))&& (StringUtil.nullOrEmptyOrBlankString(lsItemReason3)))
       {
          Log.customer.debug("Third Line is empty");
          lbLine3 = false;
       }
   else
      {
        Log.customer.debug("Line 3 isn't completely empty");
        lbLine3 = true;
      }
   

   if((!(loAppr.getDottedFieldValue("ItemITrelated4") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated4")).booleanValue())) && 
       (StringUtil.nullOrEmptyOrBlankString(lsItemVMIIDNum4))&&(StringUtil.nullOrEmptyOrBlankString(lsItemModelNum4))&&(StringUtil.nullOrEmptyOrBlankString(lsItemSerialNum4)) &&
       (StringUtil.nullOrEmptyOrBlankString(lsItemDescription4)) && (StringUtil.nullOrEmptyOrBlankString(lsItemReason4)))
       {
          Log.customer.debug("Fourth Line is empty");
          lbLine4 = false;
       }            
   else
      {
        Log.customer.debug("Line 4 isn't completely empty");
        lbLine4 = true;
      }

   if((!(loAppr.getDottedFieldValue("ItemITrelated5") != null && ((Boolean)loAppr.getFieldValue("ItemITrelated5")).booleanValue())) && 
      (StringUtil.nullOrEmptyOrBlankString(lsItemVMIIDNum5))&&(StringUtil.nullOrEmptyOrBlankString(lsItemModelNum5))&&(StringUtil.nullOrEmptyOrBlankString(lsItemSerialNum5))&&
      (StringUtil.nullOrEmptyOrBlankString(lsItemDescription5))&&(StringUtil.nullOrEmptyOrBlankString(lsItemReason5)))
      {
          Log.customer.debug("Fifth Line is empty");
          lbLine5 = false;
      }            
   else
     {
        Log.customer.debug("Line 5 isn't completely empty");
        lbLine5 = true;
     }
   if(lbLine5 == false && lbLine4 == false && lbLine3 == false && lbLine2 == false && lbLine1 == false)
      {
          Log.customer.debug("All five lines are empty");
          Log.customer.debug("All the Item Description and Item reason fields are null");
          lbErrorValue = true;
      }
   if(lbLine1)
   {
       if((StringUtil.nullOrEmptyOrBlankString(lsItemDescription1) && StringUtil.nullOrEmptyOrBlankString(lsItemReason1)) || 
               (!StringUtil.nullOrEmptyOrBlankString(lsItemDescription1) && StringUtil.nullOrEmptyOrBlankString(lsItemReason1)) ||
               (StringUtil.nullOrEmptyOrBlankString(lsItemDescription1) && !StringUtil.nullOrEmptyOrBlankString(lsItemReason1)))
        {
            lsLineNo = "Line:1";

              Log.customer.debug("The Item Description and Item reason fields in line 1 are null with other fields in this line having values");
              lbErrorValue = true;
        }
   }   
   if(lbLine2)
   {
       if((StringUtil.nullOrEmptyOrBlankString(lsItemDescription2) && StringUtil.nullOrEmptyOrBlankString(lsItemReason2)) || 
         (!StringUtil.nullOrEmptyOrBlankString(lsItemDescription2) && StringUtil.nullOrEmptyOrBlankString(lsItemReason2)) ||
         (StringUtil.nullOrEmptyOrBlankString(lsItemDescription2) && !StringUtil.nullOrEmptyOrBlankString(lsItemReason2)))  
       {
            if(lsLineNo != null)
            {
                lsLineNo = lsLineNo +","+"Line:2";
            }
            else
            {
                lsLineNo = "Line:2";
            }
              Log.customer.debug("The Item Description and Item reason fields in line 2 are null with other fields in this line having values");
              lbErrorValue = true;
        }
     }
   if(lbLine3)
   {        
       if((StringUtil.nullOrEmptyOrBlankString(lsItemDescription3) && StringUtil.nullOrEmptyOrBlankString(lsItemReason3))|| 
         (!StringUtil.nullOrEmptyOrBlankString(lsItemDescription3) && StringUtil.nullOrEmptyOrBlankString(lsItemReason3)) ||
         (StringUtil.nullOrEmptyOrBlankString(lsItemDescription3) && !StringUtil.nullOrEmptyOrBlankString(lsItemReason3)))           
        {
            if(lsLineNo != null)
             {
                   lsLineNo = lsLineNo +","+"Line:3";
             }
            else
             {
                   lsLineNo = "Line:3";
             }    
              Log.customer.debug("The Item Description and Item reason fields in line 3 are null with other fields in this line having values");
              lbErrorValue = true;
        }
   }   
   if(lbLine4)
   {        
       if((StringUtil.nullOrEmptyOrBlankString(lsItemDescription4) && StringUtil.nullOrEmptyOrBlankString(lsItemReason4))|| 
         (!StringUtil.nullOrEmptyOrBlankString(lsItemDescription4) && StringUtil.nullOrEmptyOrBlankString(lsItemReason4)) ||
         (StringUtil.nullOrEmptyOrBlankString(lsItemDescription4) && !StringUtil.nullOrEmptyOrBlankString(lsItemReason4)))           
        {
            if(lsLineNo != null)
             {
                 lsLineNo = lsLineNo +","+"Line:4";
             }
            else
             {
                 lsLineNo = "Line:4";
             }
              Log.customer.debug("The Item Description and Item reason fields in line 4 are null with other fields in this line having values");
              lbErrorValue = true;
        }
   } 
   if(lbLine5)
   {        
       if((StringUtil.nullOrEmptyOrBlankString(lsItemDescription5) && StringUtil.nullOrEmptyOrBlankString(lsItemReason5))|| 
         (!StringUtil.nullOrEmptyOrBlankString(lsItemDescription5) && StringUtil.nullOrEmptyOrBlankString(lsItemReason5)) ||
         (StringUtil.nullOrEmptyOrBlankString(lsItemDescription5) && !StringUtil.nullOrEmptyOrBlankString(lsItemReason5)))           
        {
            if(lsLineNo != null)
             {
                 lsLineNo = lsLineNo +","+"Line:5";
             }
            else
             {
                lsLineNo = "Line:5";
             }  
              Log.customer.debug("The Item Description and Item reason fields in line 5 are null with other fields in this line having values");
              lbErrorValue = true;
        }
   }
  if(lbErrorValue == true)
  {
       if(lsLineNo != null)
       {
         Log.customer.debug("The values for Item Description and Item Reason must be set for ("+lsLineNo+")");
         String lsLine = lsLineNo;
         lsLineNo = null;
         return ListUtil.list(Constants.getInteger(-1),
                    "Item Description and Reason For Disposal/Transfer required for item(s) in "+lsLine+" ");

       }
       else
       {
           Log.customer.debug("The values for Item Description and Item Reason must be set");
           return ListUtil.list(Constants.getInteger(-1),
                         "Item Description and Reason For Disposal/Transfer required for item(s)"); 
       }

  }
  else
  {
     Log.customer.debug("Values are all proper");
     return NoErrorResult;
  }
}
     
}     