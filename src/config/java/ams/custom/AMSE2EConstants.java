/*
 * @(#)AMSE2EConstants.java     1.0
 *
 * Copyright 2000 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 * PVCS Modification Log:
 *
 * $Log:   R:\Software\development\archives\Ariba 8.x Customizations\app\buyer\Server\classes\extensions\config\java\ams\custom\AMSE2EConstants.java-arc  $
 * 
 *    Rev 1.0   21 Jan 2004 15:11:58   cm
 * Initial revision.
 * 
 *    Rev 1.2   13 Nov 2002 20:22:20   nrao
 * (e2e, Dev SPL #34) Removed prefix "EP " from OrderID. 
 * 
 *    Rev 1.0   17 Oct 2002 20:56:52   cm
 * Initial revision.
 *
 */

package config.java.ams.custom;

public interface AMSE2EConstants
    {
    // Constants for symbolic names/keys
    public static final String STRING_ALLCLIENTNAMES = "ALLDEPT";
    public static final String STRING_ALLCOMMODITIES = "ALL";
    public static final String STRING_ALL            = "ALL";
    
    // Constants for prefixes/suffixes
    public static final String PREFIX_ORDERID = "";
    public static final String PREFIX_E2EPOB = "E2E:";
    
    // Constants for Transaction Source
    public static final String TXNSOURCE_E2EPROCURE  = "E2E";
    public static final String TXNSOURCE_QUICKQUOTE  = "QQ";
    public static final String TXNSOURCE_INTERFACES  = "EXT";
    
    // Constants for field lengths/limits
    public static final int FLDLENGTH_COMMODITYCODE = 8;
}
