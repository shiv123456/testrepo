package config.java.ams.custom;

import javax.jms.JMSException;

import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.common.core.Core;
import ariba.common.core.User;
import ariba.purchasing.core.Requisition;
import ariba.util.core.Fmt;
import ariba.util.log.Log;

import com.cgi.evamq.common.EvaQueueMessage;
import com.cgi.evamq.consumer.EvaConsumeCallBack;
import com.google.gson.Gson;

public class BuysenseUpdateReqHistForQQStatus implements EvaConsumeCallBack
{
    Partition moPart = null;
    private static String msCN = "BuysenseUpdateReqHistForQQStatus";

    public String getQueueName() throws JMSException, Exception
    {
        // for ariba get the Endpoint from P.table
        Log.customer.debug(msCN + ": fetching QueueName from P.table");
        System.out.println(msCN + ": fetching QueueName from P.table");
        moPart = Base.getService().getPartition("pcsv");
        if (moPart == null)
        {
            moPart = Base.getSession().getPartition();
        }

        return (String) Base.getService().getParameter(moPart, "Application.AMSParameters.EvaMessageQueueNameQQStatus");
    }

    public String getEndPoint() throws JMSException, Exception
    {
        // for ariba get the Endpoint from P.table
        Log.customer.debug(msCN + ": fetching EvaMessageQueueEndpoint from P.table");
        System.out.println(msCN + ": fetching EvaMessageQueueEndpoint from P.table");
        Partition moPart = null;
        moPart = Base.getService().getPartition("pcsv");
        if (moPart == null)
        {
            moPart = Base.getSession().getPartition();
        }

        return (String) Base.getService().getParameter(moPart, "Application.AMSParameters.EvaMessageQueueEndpoint");
        // return "tcp://162.70.7.107:31108"; //
        // "failover://(tcp://162.70.7.107:31108,tcp://162.70.7.107:31208,network:static://(tcp://162.70.7.107:31108,tcp://162.70.7.107:31208))?randomize=false";
    }

    public void updateMessage(String fsQueueMessage) throws Exception
    {
        Gson gson = new Gson();
        ariba.purchasing.core.Requisition req = null;
        String reqUniqueName = "PRXXX"; // retrive it from teh JSON

        moPart = Base.getService().getPartition("pcsv");
        if (moPart == null)
        {
            moPart = Base.getSession().getPartition();
        }

        try
        {
            EvaQueueMessage loQueueMessage;
            loQueueMessage = gson.fromJson(fsQueueMessage, EvaQueueMessage.class);
            reqUniqueName = loQueueMessage.getDocSourceID();
           // String lsRecordType = loQueueMessage.getEvaOtherPayLoad();
            Log.customer.debug(msCN + ": obj EvaOtherPayLoad:" + loQueueMessage.getEvaOtherPayLoad());
            Log.customer.debug(msCN + ": obj origin ID:" + loQueueMessage.getObjOriginID());
            Log.customer.debug(msCN + ": doc source2 ID:" + loQueueMessage.getDocSourceID());

            System.out.println(msCN + ": obj origin ID:" + loQueueMessage.getObjOriginID());
            System.out.println(msCN + ": doc source ID:" + loQueueMessage.getDocSourceID());

            System.out.println(msCN + ": reqUniqueName:" + reqUniqueName);
            System.out.println(msCN + ": moPart:" + moPart.toString());
            
            AQLResultCollection loBuysenseClientResultSet;
            AQLQuery loWhereQuery = AQLQuery.parseQuery("select * from Requisition where UniqueName=" + reqUniqueName );

            AQLOptions loOptions = new AQLOptions();            
            loOptions.setUserLocale(Base.getSession().getLocale());
            loOptions.setUserPartition(moPart);
            loBuysenseClientResultSet = (AQLResultCollection) Base.getService().executeQuery(loWhereQuery, loOptions);

            String lsRecordType = "QQStatusRecord";
            String lsMessage = "QQ Status:";
            User loAribaSystemUser = Core.getService().getAribaSystemUser(moPart);

            if (loBuysenseClientResultSet.getFirstError() != null)
            {            
                Log.customer.warning(8888, msCN + "::run() AQL execution error: " + loBuysenseClientResultSet.getFirstError().toString());
                System.out.println(msCN + "::run() AQL execution error: " + loBuysenseClientResultSet.getFirstError().toString());
                return;
            }
            while(loBuysenseClientResultSet.next())
            {
                req = (ariba.purchasing.core.Requisition )loBuysenseClientResultSet.getObject(0);
                BuysenseUtil.createHistory(req, reqUniqueName, lsRecordType, lsMessage, loAribaSystemUser);
                System.out.println(msCN + ": KKKKKKKKK:");
            }
            
            //req = (ariba.purchasing.core.Requisition) Base.getService().objectMatchingUniqueName("ariba.purchasing.core.Requisition", moPart, reqUniqueName);
            // String lsMessage = Fmt.Sil("ariba.procure.core","PreEncEditMessage");
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.customer.debug("MQUpdateAward Update Award: Exception: " + e);            
            System.out.println("MQUpdateAward Update Award: Exception: " + e);
        }
        finally
        {
        }
    }

}
