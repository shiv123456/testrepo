//
//***************************************************************************/
//  Anup - Dec 2003 - Initial version..
//
//***************************************************************************/

/* 04/07/2004: Updates for Ariba 8.x (Jeff Namadan) */

package config.java.ams.custom;

import ariba.approvable.core.Approvable;

import java.io.FileInputStream;
import java.util.List;

import ariba.purchasing.core.PurchaseOrder;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.purchasing.core.ordering.OrderMethod;
import ariba.basic.core.CommodityCode;
import ariba.base.fields.*;
import ariba.util.core.*;
// Ariba 8.x: commented out the next two imports; they aren't needed anymore.
// If we do need it in the future, workflow has moved to ariba.server.workflowserver.*
//import ariba.server.objectserver.workflow.*;
//import ariba.server.objectserver.core.*;
//import ariba.server.workflowserver.*;
import ariba.base.core.*;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.common.core.*;
import ariba.util.log.Log;
// Ariba 8.x
import ariba.base.fields.Action;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import com.cgi.evamq.common.EvaQueueMessage;
import com.google.gson.Gson;

// Ariba 8.x: ActionInterface no longer used by Ariba
//public class AMSRequisitionApproved implements ActionInterface
//81->822 changed Vector to List
public class AMSRequisitionApproved extends Action
{
    
private String sClassName = "AMSRequisitionApproved";

// Ariba 8.x: No longer used by Ariba
/* public boolean execute( WorkflowState workflowState,
                           ClusterRoot cr,
                           WorkflowParameter[] parameters)
                           throws WorkflowException */
    // Ariba 8.x: replaced public boolean execute
   public void fire (ValueSource object,
                     PropertyTable parameters)
       throws ActionExecutionException
  {
       //Pavan - added below method for "eMall to Quick Quote functionality" (CSPL-xxxx)
       prepAndSendReqtoQQ((Requisition)object);
       
    //SRINI: START: SEV changes for SupplierLocation
    AMSReqSubmitHook.setRegistrationType((Requisition)object);
    //SRINI: END: SEV changes for SupplierLocation
	 
     ClusterRoot cr = (ClusterRoot)object;
// Ariba 8.x: Change Log.util.debug to Log.customer.debug
     Log.customer.debug("In execute of AMSRequisitionApproved"+cr);
     List  loLineItems = null ;
     ReqLineItem loReqLineItem  = null ;
     loLineItems = (List)((Approvable)cr).getDottedFieldValue( "LineItems" ) ;
     for ( int i=0 ; i < loLineItems.size() ; i++ )
     {
// Ariba 8.x: Change Log.util.debug to Log.customer.debug
        Log.customer.debug( "Processing ReqLineItem #" + ( i + 1 ) ) ;
        loReqLineItem = (ReqLineItem)loLineItems.get( i ) ;
        if ( (Boolean)loReqLineItem.getFieldValue("SolicitationLine") == null)
                    continue;
        if ( ((Boolean)loReqLineItem.getFieldValue("SolicitationLine")).booleanValue())
        {
           if ( loReqLineItem.getIsAdHoc() )
           {
              // Mark this line for solicitiation
              markNonCatalogReqLineForSolicitation( (Approvable)cr , loReqLineItem ) ;
           }
           else
           {
              // Convert the catalog line into a non-catalog line - beware of Punchout,
              // PunchOut must be set to null so that it can be converted to NonCatalog
              if ( loReqLineItem.getDottedFieldValue("PunchOut") != null)
              {
                 loReqLineItem.setDottedFieldValue("PunchOut", null);
              }
              
              convertCatalogReqLineToNonCatalog( (Approvable)cr , loReqLineItem ) ;

              // Mark this line for solicitiation
              markNonCatalogReqLineForSolicitation( (Approvable)cr , loReqLineItem ) ;
           }
        }
     }
     // Ariba 8.x: cannot use reture true with fire
     //return true;
     return;
  }


  private void markNonCatalogReqLineForSolicitation(Approvable foApprovable,
                                                    ReqLineItem foReqLineItem )
  {
     String lsSolicitationVendor = Base.getService().getParameter( foApprovable.getPartition(),  
                                               "Application.AMSParameters.SolicitationVendor") ;

     // Change the present supplier code to the solicitation supplier code
     BaseObject loSupplier = Base.getService().objectMatchingUniqueName("ariba.common.core.Supplier",                               
                                                                        foApprovable.getPartition(),             
                                                                        lsSolicitationVendor); 

     foReqLineItem.setDottedFieldValue( "Supplier", loSupplier ) ;
  }

  private void convertCatalogReqLineToNonCatalog(Approvable foApprovable,
                                               ReqLineItem foReqLineItem )
  {
     CommodityCode loCommonComObject = null ;

     //
     // First, change the LineItemProductDescription so it does not
     // reference the catalog item anymore.
     //
     foReqLineItem.setDottedFieldValue( "Description.ItemNumber", "" ) ;
     foReqLineItem.setDottedFieldValue( "Description.CatalogItemRef", null ) ;
     foReqLineItem.setDottedFieldValue( "Description.SupplierPartNumber", "" ) ;
     foReqLineItem.setDottedFieldValue( "Description.ClassificationCodeAux", "" ) ;
     foReqLineItem.setDottedFieldValue( "Description.ManPartNumber", "" ) ;
     foReqLineItem.setDottedFieldValue( "Description.ManName", "" ) ;
     foReqLineItem.setDottedFieldValue( "Description.ManURL", "" ) ;
     foReqLineItem.setDottedFieldValue( "Description.ContractNum", null ) ;
     foReqLineItem.setDottedFieldValue( "Description.ContractYesNo", null ) ;

     //
     // Next, change the ReqLineItem so it is marked as AdHoc
     //
     foReqLineItem.setIsAdHoc( true ) ;

     //
     // Finally, finish the ReqLineItem
     //
     // 81->822 New commodity code changes     
     //loCommonComObject = (CommodityCode)foReqLineItem.getDottedFieldValue( "Description.CommodityCode" ) ;
     loCommonComObject = foReqLineItem.getDescription().getCommonCommodityCode() ;
     
     if(loCommonComObject  == null)
     {
        Log.customer.debug( "Could not find mapped commodity code." ) ;
        foReqLineItem.setDottedFieldValue( "CommodityCode", null ) ;
        return;
     }
     
     List cemList = foReqLineItem.getDescription().getCommodityExportMapChoices(foReqLineItem.getPartition()); 
     if (cemList.size() != 0) 
     {
	CommodityExportMapEntry cemValue = (CommodityExportMapEntry)cemList.get(0); 
        PartitionedCommodityCode loComObject = (PartitionedCommodityCode)cemValue.getDottedFieldValue( "PartitionedCommodityCode" ) ;
        foReqLineItem.setDottedFieldValue( "CommodityCode", loComObject ) ;
     }
     else
     {
// Ariba 8.x: Change Log.util.debug to Log.customer.debug
        Log.customer.debug( "Could not find mapped commodity code." ) ;
        foReqLineItem.setDottedFieldValue( "CommodityCode", null ) ;
     }

     foReqLineItem.setDottedFieldValue( "Description.CommonCommodityCode", null ) ;
  }
  
  private void prepAndSendReqtoQQ(Requisition loReq)
    {

        Log.customer.debug(sClassName + "::prepAndSendReqtoQQ loReq: " + loReq);

        /* If ReqHeadCB5 (Send PR to Quick Quote) is checked and 'Send PR to Quick Quote' is enabled for the user agency,
           The supplier for all line items will be changed to PROCUREMENT OFFICE.
           A blank order for $0.00 will be created with no Order Number.
           Any punchout and catalog items in the cart will be converted to non-catalog items. */

        boolean bClientSendEmallReqToQQ = false;
        boolean bReqSendEmallReqToQQ = false;
        String sLogKeyWord = "E2QQ Send Req to QQ ERROR";

        ariba.user.core.User lRequesterUser = (ariba.user.core.User) loReq.getRequester();
        ariba.common.core.User lRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(lRequesterUser, loReq.getPartition());
        bClientSendEmallReqToQQ = ((Boolean) lRequesterPartitionUser.getDottedFieldValue("ClientName.SendEmallReqToQQ")).booleanValue();
        String sRequesterClient = (String) lRequesterPartitionUser.getDottedFieldValue("ClientName.ClientName");

        bReqSendEmallReqToQQ = ((Boolean) loReq.getDottedFieldValue("ReqHeadCB5Value")).booleanValue();

        Log.customer.debug(sClassName + "::prepAndSendReqtoQQ bClientSendEmallReqToQQ: " + bClientSendEmallReqToQQ + ", bReqSendEmallReqToQQ: "
                + bReqSendEmallReqToQQ);

        if (bClientSendEmallReqToQQ && bReqSendEmallReqToQQ)
        {

            BaseVector lineItemVector = loReq.getLineItems();
            for (int i = 0; i < lineItemVector.size(); i++)
            {
                ReqLineItem loReqLineItem = (ReqLineItem) lineItemVector.get(i);

                // PunchOut must be set to null so that it can be converted to NonCatalog
             // if getIsAdHoc is true means this is a non-catalog item
                if ( !(loReqLineItem.getIsAdHoc()) )
                {
                    // Convert the catalog line into a non-catalog line
                    if (loReqLineItem.getDottedFieldValue("PunchOut") != null)
                    {
                        loReqLineItem.setDottedFieldValue("PunchOut", null);
                    }
                    
                    setReqLineNIGPCommodityCode(loReqLineItem);
                    
                // Convert the catalog line into a non-catalog line
                    convertCatalogReqLineToNonCatalogForQQ((Approvable) loReq, loReqLineItem);
                }

                String lsSolicitationVendor = Base.getService().getParameter(loReq.getPartition(), "Application.AMSParameters.SolicitationVendor");

                // Change the present supplier to the solicitation supplier (PROCUREMENT OFFICE)
                BaseObject loSupplier = Base.getService().objectMatchingUniqueName("ariba.common.core.Supplier", loReq.getPartition(),
                        lsSolicitationVendor);

                loReqLineItem.setDottedFieldValue("Supplier", loSupplier);

            }

        }
        else
        {
            Log.customer.warning(8888, sClassName + "::prepAndSendReqtoQQ Not sending Req - " + loReq.getUniqueName()
                    + " to MQ as its Requster client (ClientName=" + sRequesterClient + ").(SendEmallReqToQQ=" + bClientSendEmallReqToQQ
                    + ") is not enabled to send Req to QQ and/or flag is not selected on Req (ReqHeadCB3Value= " + bReqSendEmallReqToQQ + ").");
            return;
        }

        //Send Req to MQ queue in JSON format

        try
        {
            String sReqJSON = generateReqJSON(loReq);
            Log.customer.debug(sClassName + "::prepAndSendReqtoQQ sReqJSON: " + sReqJSON);

            EvaQueueMessage eVAQueueMsg = new EvaQueueMessage();
            eVAQueueMsg.setMsgCreator(EvaQueueMessage.APPLICATION_ARIBA);
            eVAQueueMsg.setObjID(loReq.getUniqueName());
            eVAQueueMsg.setObjOrigin(eVAQueueMsg.APPLICATION_ARIBA);
            eVAQueueMsg.setEvent("New PR");
            eVAQueueMsg.setDocSourceID(loReq.getUniqueName());
            eVAQueueMsg.setEvaOtherPayLoad(sReqJSON);

            Log.customer.debug(sClassName + "::prepAndSendReqtoQQ eVAQueueMsg - " + eVAQueueMsg);
            
            //converting the message to JSON
            Gson gson = new Gson();
            String sQueueMsg = (new StringBuilder()).append(gson.toJson(eVAQueueMsg)).toString();
            
            Log.customer.debug(sClassName + "::prepAndSendReqtoQQ sQueueMsg - " + sQueueMsg);
            
            //Writing message to Ariba DB log table before sending
            BuysenseMQMessageLogging loMQLog = new BuysenseMQMessageLogging();
            loMQLog.logMQMessagetoDB(loReq.getUniqueName(), "E2QQ1", sQueueMsg); // APP_NAME E2QQ1 - is for sending emall Req to QQ
                        
            Log.customer.debug(sClassName + "::prepAndSendReqtoQQ starting to send message to MQ");
            EvaSendMessageAribaReqToQQ aribaReqMsg = new EvaSendMessageAribaReqToQQ();
            Log.customer.debug(sClassName + "::prepAndSendReqtoQQ aribaReqMsg: "+aribaReqMsg);
            aribaReqMsg.initializeCamel();
            Log.customer.debug(sClassName + "::prepAndSendReqtoQQ initializeCamel() called ");
            aribaReqMsg.publishMessageToQueue(eVAQueueMsg);
            Log.customer.debug(sClassName + "::prepAndSendReqtoQQ sent message to MQ");

        }
        catch (Throwable e)
        {
            Log.customer.error(sClassName + " "+sLogKeyWord+" for "+loReq+". EXCEPTION/ERROR IN SENDING MSG TO QQ - "+e.getMessage());
            e.printStackTrace();
        }

    }
  
    private void setReqLineNIGPCommodityCode(ReqLineItem loReqLineItem)
    {

        PartitionedCommodityCode loPartitionedCommodityCode = loReqLineItem.getCommodityCode();
        BaseId sBID = null;
        if (loPartitionedCommodityCode != null)
        {
            String sUNSPSCCode = loPartitionedCommodityCode.getUniqueName();

            String sQuery = "select b from ariba.core.BuysenseNIGPCommodityCode b where b.UNSPSCCode.UniqueName = '" + sUNSPSCCode + "'";
            AQLQuery aqlQuery = AQLQuery.parseQuery(sQuery);
            Partition partition = Base.getService().getPartition("pcsv");
            AQLOptions aqlOptions = new AQLOptions();
            aqlOptions.setPartition(partition);
            AQLResultCollection aqlResults = Base.getService().executeQuery(aqlQuery, aqlOptions);

            if (aqlResults.getErrors() == null)
            {
                while (aqlResults.next())
                {
                    sBID = aqlResults.getBaseId(0);
                }
            }
            Log.customer.debug(sClassName + "::setReqLineNIGPCommodityCode() setting line ReqLineNIGPCommodityCode field to - " + sBID);
            loReqLineItem.setDottedFieldValue("ReqLineNIGPCommodityCode", sBID);
        }
    }

  public String generateReqJSON(Requisition loReq) throws Exception
    {

        Log.customer.debug(sClassName + "::generateJSON() loReq: " + loReq);
        BuysenseDW2ExtractEngine buyExtrEng = new BuysenseDW2ExtractEngine();

        String sEvaMessageJsonInput = Base.getService().getParameter(Base.getSession().getPartition(),
                "Application.AMSParameters.EvaMessageJsonInput");

        Log.customer.debug(sClassName + "::generateJSON sEvaMessageJsonInput: " + sEvaMessageJsonInput);

        FileInputStream loFileInputStream = new FileInputStream(sEvaMessageJsonInput);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.readTree(loFileInputStream);

        JsonNode qqreqObjHeirarchy = rootNode.path("emalltoqqreq_objhierarchy");
        JsonNode qqreqFieldDetails = rootNode.path("emalltoqqreq_allfielddetails");

        String qqreqObjHeirarchyStr = "{" + "\"objhierarchy\":" + qqreqObjHeirarchy.toString() + "}";
        String qqreqFieldDetailsStr = "{" + "\"allfielddetails\":" + qqreqFieldDetails.toString() + "}";
        String qqJson = buyExtrEng.extractObj(qqreqObjHeirarchyStr, qqreqFieldDetailsStr, loReq);

        Log.customer.debug(sClassName + "::generateJSON returning qqJson: " + qqJson);
        return qqJson;
    }
  
  private void convertCatalogReqLineToNonCatalogForQQ(Approvable foApprovable, ReqLineItem foReqLineItem )
    {
        CommodityCode loCommonComObject = null;

        //
        // First, change the LineItemProductDescription so it does not
        // reference the catalog item anymore.
        //
        foReqLineItem.setDottedFieldValue("Description.ItemNumber", "");
        foReqLineItem.setDottedFieldValue("Description.CatalogItemRef", null);
        //foReqLineItem.setDottedFieldValue( "Description.SupplierPartNumber", "" ) ; // null out SPN as per requirement
        foReqLineItem.setDottedFieldValue("Description.ClassificationCodeAux", "");
        foReqLineItem.setDottedFieldValue("Description.ManPartNumber", "");
        foReqLineItem.setDottedFieldValue("Description.ManName", "");
        foReqLineItem.setDottedFieldValue("Description.ManURL", "");
        foReqLineItem.setDottedFieldValue("Description.ContractNum", null);
        foReqLineItem.setDottedFieldValue("Description.ContractYesNo", null);

        foReqLineItem.setIsAdHoc(true);

        loCommonComObject = foReqLineItem.getDescription().getCommonCommodityCode();

        if (loCommonComObject == null)
        {
            Log.customer.debug("Could not find mapped commodity code.");
            foReqLineItem.setDottedFieldValue("CommodityCode", null);
            return;
        }

        List cemList = foReqLineItem.getDescription().getCommodityExportMapChoices(foReqLineItem.getPartition());
        if (cemList.size() != 0)
        {
            CommodityExportMapEntry cemValue = (CommodityExportMapEntry) cemList.get(0);
            PartitionedCommodityCode loComObject = (PartitionedCommodityCode) cemValue.getDottedFieldValue("PartitionedCommodityCode");
            foReqLineItem.setDottedFieldValue("CommodityCode", loComObject);
        }
        else
        {

            Log.customer.debug("Could not find mapped commodity code.");
            foReqLineItem.setDottedFieldValue("CommodityCode", null);
        }

        foReqLineItem.setDottedFieldValue("Description.CommonCommodityCode", null);
    }
  
}
