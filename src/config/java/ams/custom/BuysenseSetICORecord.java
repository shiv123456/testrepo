package config.java.ams.custom;

import java.util.List;

import ariba.util.log.Log;
import ariba.purchasing.core.PurchaseOrder;
import ariba.purchasing.core.Requisition;
import ariba.base.core.BaseObject;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.common.core.Core;
import ariba.util.core.PropertyTable;


public class BuysenseSetICORecord extends Action
{
	private static String ClassName = "config.java.ams.custom.BuysenseResetGroupsPCards";

	public void fire (ValueSource object, PropertyTable parameters) throws ActionExecutionException
    {
		Log.customer.debug("***%s***Called::fire", ClassName);

		PurchaseOrder loPO = (PurchaseOrder)object;
		String lsPOMethod = (String) loPO.getSupplierLocation().getPreferredOrderingMethod();
		if(lsPOMethod != null && (lsPOMethod.equals("URL") || lsPOMethod.equals("Print") || lsPOMethod.equals("AribaEmail") || lsPOMethod.equals("AribaFax")))
		{
			String lsRecordType = getRecordType(lsPOMethod);

			Requisition loReq = (Requisition) loPO.getFirstRequisition();
			ariba.common.core.User loAribaSystemUser = Core.getService().getAribaSystemUser(loPO.getPartition());
			Log.customer.debug("***%s***Called::fire::PO -- % REQ -- %", ClassName, loPO.getUniqueName(), loReq.getUniqueName());

			BaseObject po_hist = (ariba.procure.core.SimpleProcureRecord) BaseObject.create("ariba.procure.core.SimpleProcureRecord", loPO.getPartition());
		    po_hist.setFieldValue("Approvable", loPO);
		    po_hist.setFieldValue("ApprovableUniqueName", loPO.getUniqueName());

		    ariba.util.core.Date  loDate =  new ariba.util.core.Date();
		    po_hist.setFieldValue("Date", loDate);

		    po_hist.setFieldValue("User", loAribaSystemUser.getUser());
		    po_hist.setFieldValue("RecordType", lsRecordType);

	       ((List)loReq.getFieldValue("Records")).add(po_hist) ;
		}
   }

	private String getRecordType(String orderingMethod)
	{
		String lsRecordType = null;
		if(orderingMethod.equals("URL"))
		{
			lsRecordType = "ASNInternalChangeOrderRecord";
		}
		else if(orderingMethod.equals("Print"))
		{
			lsRecordType = "PrintInternalChangeOrderRecord";
		}
		else if(orderingMethod.equals("AribaEmail"))
		{
			lsRecordType = "ASNInternalChangeOrderRecord";
		}
		else if(orderingMethod.equals("AribaFax"))
		{
			lsRecordType = "ASNInternalChangeOrderRecord";
		}
		else
		{
			//Code should never get here
			lsRecordType = "";
		}
		return lsRecordType;
	}
}
