package config.java.ams.custom;

import ariba.base.core.BaseObject;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
import ariba.procure.core.LineItemProductDescription;
import ariba.procure.core.ProcureLineItem;
import ariba.purchasing.core.Requisition;
import ariba.base.fields.Condition;
import ariba.procure.core.LineItemProductDescription;
import config.java.ams.custom.BuysenseCopyFields;


/**
 * CSPL #: 3872
 * @author Partho Sarathi
 * Date  : 12-Jan-2012
 * @version 1.0
 * @see    config\variants\vcsv\extensions\BuysenseUpdateShortNameTrigger.aml
 * Explanation: This class is a custom condition to check for External requisition('QQ','EXT','E2E')
 * 				through Transaction Source value in Requisition header.
 *
 */

public class BuysenseCheckTransactionSource extends Condition{

	private static final ValueInfo parameterInfo[];
	public boolean evaluate(Object obj, PropertyTable params) throws ConditionEvaluationException
	{
		Log.customer.debug("Inside BuysenseCheckTransactionSource"+obj);
		if(obj != null && obj instanceof LineItemProductDescription)
		{
			LineItemProductDescription lipd = (LineItemProductDescription)obj;
			ProcureLineItem loPli = lipd.getLineItem();
			if(loPli!= null )
			{
				Log.customer.debug("BuysenseCheckTransactionSource: Inside Product line item"+loPli);
				Requisition loReq = (Requisition) loPli.getLineItemCollection();
				if(loReq!=null)
				{
					BuysenseCopyFields loBCP = new BuysenseCopyFields();
					boolean lbCopiedReq = loBCP.isCopiedReq(loReq);
					Log.customer.debug("BuysenseCheckTransactionSource:Inside Requisition now "+loReq);
					String transactionSource = (String)loReq.getFieldValue("TransactionSource");
					if((transactionSource != null && (transactionSource.equals("QQ") || transactionSource.equals("E2E") || transactionSource.equals("EXT"))|| lbCopiedReq))
					{
						Log.customer.debug("BuysenseCheckTransactionSource:Inside CheckTransactionSource:External Requisition found"+obj);
						return true;
					}
				}
			}
	    }
		return false;
	}
	 protected ValueInfo[] getParameterInfo()
	 {
		 return parameterInfo;
	 }
	 static
     {
		 parameterInfo = (new ValueInfo[] {
         new ValueInfo("SourceObject", 0)
         });
	 }
}
