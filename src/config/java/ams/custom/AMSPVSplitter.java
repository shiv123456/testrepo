/**
The PV Splitter receives a vector of receipts and creates Payment Vouchers (PVs).
All receipts that have the same supplier, are put on one PV.

This program is called by AMSPushPayment.java

pbreuss, AMS, March 2000
*/
/* 09/10/2003: Updates for Ariba 8.0 (David Chamberlain) */
/*
03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/04/2004: rgiesen dev SPL #38 - replace getLeadCurrency with Currency.getDefaultCurrency(partition)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/

package config.java.ams.custom;
import ariba.receiving.core.Receipt;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.LineItemCollection;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.core.Partition;
import ariba.util.log.Log;
import java.util.List;
import java.util.Map;
import ariba.common.core.*;
// Ariba 8.1: Added new ariba.user.core.User class.
import ariba.util.core.Date;
import java.math.BigDecimal;
// Ariba 8.1: We are using Iterator instead of Enumeration now
import java.util.Iterator;
// Ariba 8.1: The Map constructor has been deprecated by MapUtil::newTable()
import ariba.util.core.MapUtil;
// Ariba 8.1: Money and Currency have moved
import ariba.basic.core.Money;
import ariba.basic.core.Currency;

//81->822 changed Vector to List
//81->822 changed Hashtable to Map
public class AMSPVSplitter
{
    /**
    Receivs a vector of cluster roots and creates Payment Vouchers (PVs) out of it. At the end of this function,
    all PVs are written to the TXN table by calling TXN.write.
    */
    public static int split(List ReceiptsToBePaid, Partition part)
    {
        System.out.println("Start PVSplitter.split()");
        Log.customer.debug("AMSPVSplitter.split run was called...!!!");

        int returnCode = 0;

        // will contain all generated PVs keyed by supplier
        // Ariba 8.1: The Map constructor has been deprecated by MapUtil::newTable()
        Map PVHashtable = MapUtil.map();

        // will contain the number of lines process of each PO
        Map POHashtable = MapUtil.map();

        int numberReceiptsToBePaid = ReceiptsToBePaid.size();
        Log.customer.debug(numberReceiptsToBePaid+" records were passed in");

        // looping thru the receipts that need to be paid
        for (int j=0;j<numberReceiptsToBePaid;j++)
        {
            // retrieve receipt
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            Receipt receipt = (Receipt) ReceiptsToBePaid.get(j);
            Log.customer.debug("Receipt for %s",
                           receipt.getOrder().getSupplier().toString());

            // set paid state to paying so that we dont retrieve it in the next run
            receipt.setFieldValue("PaidState", new Integer(2));

            // we use this reference to store created PVs
            ClusterRoot PV;

            // check if PV was already created for this supplier
            //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
            //Partitioned User to obtain the extrinsic BuysenseOrg field value.
            //String key = receipt.getOrder().getSupplier().toString()+receipt.getDottedFieldValue("Requester.BuysenseOrg").toString();
            ariba.user.core.User RequesterUser = (ariba.user.core.User)receipt.getDottedFieldValue("Requester");
            //rgiesen dev SPL #39
            //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
            ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,part);
            String key = receipt.getOrder().getSupplier().toString()+RequesterPartitionUser.getDottedFieldValue("BuysenseOrg").toString();
            if ((PV = (ClusterRoot)PVHashtable.get( key )) != null)
            {
                Log.customer.debug("PV for current supplier found - add receipt to existing PV");

                // -----------------------------------------------------------------
                // update PV
                // -----------------------------------------------------------------

                // now set PV line (which also updates DOCUMENT_TOTAL)
                setPVDetail(receipt, PV, part, POHashtable);
            }
            else
            {
                // -----------------------------------------------------------------
                // create new PV
                // -----------------------------------------------------------------

                Log.customer.debug("Creating new PV - add receipt to PV");

                PV = (Approvable) Approvable.create("ariba.core.Buysense.PV",
                                                    part);

                // setting name of new PV
                PV.setFieldValue("Name",
                                 "Payment to "+receipt.getDottedFieldValue("Order.Supplier.Name"));

                // setting Payment agenct (Preparer and requester of PV)
                // here we determine the supervisor of the creator of the PO of this Receipt for approval purposes
                BaseObject UserBaseObj=null;
                String ClientName = null;
                String Agency = null;
                try
                {
                    //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
                    //Partitioned User to obtain the extrinsic BuysenseOrg field value.
                    UserBaseObj = (BaseObject)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.PaymentOfficer");
                    ClientName = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.Name");
                    Agency = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.FieldDefault1.Name");
                }
                catch (Exception e)
                {
                }
                finally
                {
                    if (UserBaseObj==null)
                        UserBaseObj = Core.getService().getAribaSystemUser(part);
                    if (ClientName==null)
                    {
                        ClientName = "NOT SET";
                        Log.customer.debug("ClientName could not be retrieved from BuySenseOrg");
                    }
                    if (Agency==null)
                    {
                        Agency = "NOT SET";
                        Log.customer.debug("Agency could not be retrieved from BuySenseOrg");
                    }
                }
                PV.setFieldValue("Requester",       UserBaseObj);
                PV.setFieldValue("Preparer",        UserBaseObj);

                // set client name and agency of PV
                PV.setFieldValue("ClientName",      ClientName);
                PV.setFieldValue("Agency",  Agency);

                // setting paid state and status string
                PV.setFieldValue("PaidState", new Integer(1));
                PV.setFieldValue("PaymentStatusString", "New");

                // set other important fields
                PV.setFieldValue("SCHED_PYMT_DATE",         new Date());
                PV.setFieldValue("VENDOR_NUMBER",
                                 receipt.getDottedFieldValue("Order.Supplier.UniqueName"));
                PV.setFieldValue("VENDOR_ADDR_IND",
                                 receipt.getDottedFieldValue("Order.SupplierLocation.UniqueName"));

                // Changed by Sunil 6.x to 7.0 -- Currency.leadCurrency() to Currency.getLeadCurrency()
                // Ariba 8.1: Currency is no longer static so modified
                //PV.setFieldValue("DOCUMENT_TOTAL",                new Money(0.00, Currency.getLeadCurrency()));
				//rgiesen dev SPL #38 - replace getLeadCurrency with Currency.getDefaultCurrency(partition)
                PV.setFieldValue("DOCUMENT_TOTAL",
                                 new Money(0.00, Currency.getDefaultCurrency(part)));

                // set supplier information
                PV.setFieldValue("Supplier",
                                 receipt.getDottedFieldValue("Order.Supplier"));
                PV.setFieldValue("SupplierLocation",
                                 receipt.getDottedFieldValue("Order.SupplierLocation"));

                // setting creation method
                PV.setFieldValue("CREATIONMETHOD",          "BATCH");

                // now set PV line (which also updates DOCUMENT_TOTAL)
                setPVDetail(receipt, PV, part, POHashtable);

                // save new PV
                PV.save();

                // add PV to hashtable with supplier as key
                //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
                //Partitioned User to obtain the extrinsic BuysenseOrg field value.
                String hashKey = receipt.getDottedFieldValue("Order.Supplier").toString()+RequesterPartitionUser.getDottedFieldValue("BuysenseOrg").toString();
                PVHashtable.put(hashKey, PV);

            }// end if voucher number already created

        }// end for loop for a PVs

        Log.customer.debug("calling TXN...");


        // ------------------------------------------------
        // Push PV to TXN if PaymentMustBeApproved = false
        // ------------------------------------------------

        // If not PaymentMustBeApproved, send PVs to ERP without approval
        if(!Base.getService().getBooleanParameter(part,
                                                  "Application.AMSParameters.PaymentMustBeApproved"))
        {
            // now go thru all PVs and write them to TXN
            // Ariba 8.1: Since Map::elements() has been deprecated, we are going to Iterator now
            //for (Enumeration e = PVHashtable.elements() ; e.hasMoreElements() ;)
            for (Iterator e = PVHashtable.values().iterator() ; e.hasNext() ;)
            {
                ClusterRoot PV = (ClusterRoot)e.next();

                AMSTXN.initMappingData();
                // agency is still hardcoded
                //ff  int ret = AMSTXN.push((String)PV.getFieldValue("ClientName"), "PV", (String)PV.getFieldValue("Agency")+(String)PV.getFieldValue("UniqueName"), "NEW", "RDY", "A", "0", "BATCH", PV, "PVDETAIL", part);

                /* Ariba 8.0: Changed to parameter "PVDETAIL" to new String[] {"PVDETAIL"}.
                   It wouldn't compile because this method signature wants an array parameter. */
                int ret = AMSTXN.push((String)PV.getFieldValue("ClientName"),
                                      "PV",
                                      (String)PV.getFieldValue("UniqueName"),
                                      "NEW",
                                      "RDY",
                                      "A", "0", "BATCH", PV, new String[]
                                      {
                        "PVDETAIL"
                    }
                    , part);

                returnCode = Math.max(ret, returnCode);
            }
        }// end not PaymentMustBeApproved

        System.out.println("End PVSplitter.split()");
        return returnCode;
    }


    /**
    This function takes values from receipt, and sets it in PV
    */
    private static void setPVDetail(BaseObject receipt,
                                    BaseObject PV,
                                    Partition part, Map POHashtable)
    {
        // -----------------------------------------------------------------
        // update receipt
        // -----------------------------------------------------------------

        // setting the PVNumber on PO
        String uniqueName = (String)PV.getFieldValue("UniqueName");
        receipt.setDottedFieldValue("LineItemCollection.PVNumber", uniqueName);

        // checking ERP Order - determine PC Line number
        LineItemCollection li = (LineItemCollection)receipt.getFieldValue("LineItemCollection");
        Integer counter=null;
        if (( counter = (Integer)POHashtable.get( li )) == null)
        {
            // new PO
            POHashtable.put(li, new Integer(1));
        }
        else
        {
            // add 1 to the
            POHashtable.remove(li);
            POHashtable.put(li, new Integer(counter.intValue()+1));
        }

        //          String PCLineNum = ""+((Integer)POHashtable.get( li )).intValue();

        try
        {
            // ------------------------------------------------
            // deal with lines
            // ------------------------------------------------

            // retrieve the detail line items vector from new PV so that we can add lines
            List PVDetailVector = (List)PV.getFieldValue("PVDETAIL");

            // retrieve number of lines we have already in PV
            int numberOfLines = PVDetailVector.size();

            // create new PV Line item
            ClusterRoot PVLineItem = (ClusterRoot) ClusterRoot.create("ariba.core.Buysense.PVDetail", part);

            try
            {
                Log.customer.debug("Creating PV Detail Line...");

                // make sure that line number is 2 char
                String lineNr = ""+(numberOfLines+1);
                if (lineNr.length()==1)
                    lineNr = "0" + lineNr;

                //IXM Not sure of this?
                Integer RefLine=(Integer)receipt.getDottedFieldValue("ReceiptItems[0].NumberInCollection");
                String RefTranLine=RefLine.toString();
                if (RefTranLine.length()==1)
                    RefTranLine = "0" + RefTranLine;
                String PCLineNum=(String)receipt.getDottedFieldValue("ReceiptItems[0].LineItem.RefPCHeaderLineNum");

                PVLineItem.setFieldValue("LINE_NO", lineNr);
                PVLineItem.setFieldValue("PC_LINE_NUMBER", PCLineNum);
                PVLineItem.setFieldValue("REF_TRANS_LINE", RefTranLine);

                PVLineItem.setFieldValue("ERPPONUMBER",
                                         receipt.getDottedFieldValue("ReceiptItems[0].ERPPONumber"));
                PVLineItem.setFieldValue("DESCRIPTION",
                                         receipt.getDottedFieldValue("ReceiptItems[0].LineItem.Description.Description"));
                PVLineItem.setFieldValue("QUANTITY",
                                         new Double(((BigDecimal)receipt.getDottedFieldValue("ReceiptItems[0].NumberAccepted")).doubleValue()));
                PVLineItem.setFieldValue("UNTAXED_LINE_AMOUNT",
                                         new Money(Money.multiply((Money)receipt.getDottedFieldValue("ReceiptItems[0].LineItem.Description.Price"), (BigDecimal)receipt.getDottedFieldValue("NumberAccepted"))));

                // set fields from PO line item [0]
                ariba.common.core.SplitAccounting acctg = (ariba.common.core.SplitAccounting)receipt.getDottedFieldValue("ReceiptItems[0].LineItem.Accountings.SplitAccountings[0]");
                PVLineItem.setFieldValue("FUND",
                                         receipt.getDottedFieldValue("acctg.FieldDefault3.Name"));
                PVLineItem.setFieldValue("AGENCY",
                                         receipt.getDottedFieldValue("acctg.FieldDefault1.Name"));
                PVLineItem.setFieldValue("XORGANIZATION",
                                         receipt.getDottedFieldValue("acctg.FieldDefault4.Name"));
                PVLineItem.setFieldValue("OBJECT",
                                         receipt.getDottedFieldValue("acctg.FieldDefault2.Name"));

                Log.customer.debug("Completed PV Line Processing...");

            }
            catch (Exception e)
            {
                Log.customer.debug("Exception when setting PV line fields: ");
                e.printStackTrace();
            }


            // now update DOCUMENT_TOTAL on PV
            try
            {
                // add amount of PV to document total
                Money amount = (Money)PV.getFieldValue("DOCUMENT_TOTAL");
                amount.addTo( (Money)PVLineItem.getFieldValue("UNTAXED_LINE_AMOUNT") );
            }
            catch(NumberFormatException e)
            {
                Log.customer.debug("NumberFormatException in AMSPVSplitter: "+e.toString());
            }
            catch (Exception e)
            {
                Log.customer.debug("Exception when setting PV header fields: "+e.toString());
            }

            // finally save new line item
            PVLineItem.save();

            // add PV line item to PV
            // Ariba 8.1: List::addElement() is deprecated by List::add()
            PVDetailVector.add(PVLineItem);

            PVLineItem = null;

        }
        catch (Exception e)
        {
            Log.customer.debug("Exception when setting PV header fields: ");
            e.printStackTrace();
        }// end for each PO line item

    }


}
