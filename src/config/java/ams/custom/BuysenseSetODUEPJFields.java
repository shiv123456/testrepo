package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseSetODUEPJFields extends Action
{
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        Log.customer.debug("fire method called");
        Approvable loAppr = null;        
        if(valuesource!= null && valuesource instanceof Approvable)
        {
            loAppr = (Approvable)valuesource;                    
            loAppr.setFieldValue("DescriptionOfEmergency"," ");
            loAppr.setFieldValue("DateOfEmergency"," ");
            loAppr.setFieldValue("CauseOfEmergency", " ");
            loAppr.setFieldValue("HowEmergencyArised", " ");
        }
    }
}
