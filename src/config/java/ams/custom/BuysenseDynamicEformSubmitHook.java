//package config.java.ams.custom;
package config.java.ams.custom;

import java.util.List;

import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableHook;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.util.core.Constants;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseDynamicEformSubmitHook implements ApprovableHook {
	private static final List    NoErrorResult = ListUtil.list(Constants
			.getInteger(0));
	protected String             sResourceFile = "buysense.DynamicEform";
    private static String        lsLeftBrace   = "(";
    private static String        lsRightBrace  = ")";
    private static String        lsDotSpace    = " ";
    private static final boolean DEBUG         = true;
    public List valVector;

	public List run(Approvable approvable) {
		
		List loRetVector = NoErrorResult;
		Log.customer.debug("Inside BuysenseDynamicEformSubmitHook");
		Approvable loAppr = approvable;
		Log.customer.debug("The Approvable object is" + loAppr.getClassName());
		
		loRetVector = validateReq(loAppr);

        if (((Integer) loRetVector.get(0)).intValue() < 0)
        {
            return loRetVector;
        }
        else
        {
            loRetVector = NoErrorResult;
        }
        
        if (DEBUG)
        {
            Log.customer.debug("Finished BuysenseDynamicEformSubmitHook.");
        }
        return loRetVector;
	}

    private List validateReq(Approvable loAppr) {
    	
    	String lsProfile = null;
    	List llProfileList = ListUtil.list();
    	
 		if((ariba.user.core.User)loAppr.getDottedFieldValue("Requester") != null)
 		{    	
	    ariba.user.core.User RequesterUser = (ariba.user.core.User)loAppr.getDottedFieldValue("Requester");

	    Partition appPartition = loAppr.getPartition();

	    ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,appPartition);
	    BaseVector lvProfiles = (BaseVector)RequesterPartitionUser.getFieldValue("BuysEformProfiles");
	    if(lvProfiles != null)
	    {
	    	int liSize = lvProfiles.size();
	    	Log.customer.debug("The size of the list is :"+liSize);
	    	for(int i=0;i<liSize;i++)
	    	{
	    		BaseObject oProfile = (BaseObject)Base.getSession().objectFromId((BaseId) lvProfiles.get(i));
	    		if(oProfile!=null)
	    		{
	    			Log.customer.debug("The object picked is :"+oProfile+" and the profile name is"+oProfile.getFieldValue("ProfileName"));
	    			llProfileList.add((String)oProfile.getFieldValue("ProfileName"));
	    		}
	    	}
	    }
	    else
	    {
    		String lsError = ResourceService.getString(sResourceFile, "NoUserProfileError");
    		return ListUtil.list(Constants.getInteger(-1),lsError);	    	
	    }
	    
    	lsProfile = (String)loAppr.getDottedFieldValue("EformChooser.ProfileName");
    	if(lsProfile == null)
    	{
    		String lsError = ResourceService.getString(sResourceFile, "NoProfileError");
    		return ListUtil.list(Constants.getInteger(-1),lsError);
    	}
    	else
    	{
             if(!(llProfileList.contains(lsProfile))){
        		String lsError = ResourceService.getString(sResourceFile, "ProfileMismatchError");
        		return ListUtil.list(Constants.getInteger(-1),lsError);    	    	
    	       }
             else
             {
         	    BuysenseEformValidationEngine eVAValEng = new BuysenseEformValidationEngine();

                valVector = eVAValEng.runEformValidation(loAppr,lsProfile); 
                if (((Integer)valVector.get(0)).intValue() == -1)
                {
                    return valVector;
                }
             }
    	    
    	}  
    	
        }
 		else
 		{
 			String lsError = ResourceService.getString(sResourceFile, "NoOnBehalfOfError");
 			return ListUtil.list(Constants.getInteger(-1),lsError);
 		}
    	return NoErrorResult;
    	}


	public static String getFormattedNumberString(int liWarningNum)
    {
        StringBuffer lsbTemp = new StringBuffer();
        lsbTemp.append(lsLeftBrace + liWarningNum + lsRightBrace + lsDotSpace);
        return lsbTemp.toString();
    }
    
    

}
