package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.common.core.User;
import ariba.purchasing.core.Requisition;
import ariba.util.core.StringUtil;
import ariba.util.javascript.Log;

public class BuysenseAdminBSORuleJSUtil
{

    public static boolean checkBSO(Requisition r)
    {
        Log.javascript.debug("Called fire BuysenseadminBSORuleJSUtil");
        try
        {
            User partndRequester = ariba.common.core.User.getPartitionedUser(r.getRequester(), r.getPartition());
            String requesterBSO = (String) partndRequester.getDottedFieldValue("BuysenseOrg.UniqueName");
            ariba.user.core.User currentEffectiveUser = (ariba.user.core.User) (Base.getSession().getEffectiveUser());
            ariba.common.core.User currentUser = ariba.common.core.User.getPartitionedUser(currentEffectiveUser, Base.getSession().getPartition());
            String currentUserBSO = (String) currentUser.getDottedFieldValue("BuysenseOrg.UniqueName");
            String sClientID = (String) partndRequester.getDottedFieldValue("ClientName.UniqueName");
            
            String sQuery = "SELECT b.UniqueName,b from ariba.core.BuysenseFieldDataTable b where b.UniqueName like \'"
                    + sClientID + ":AdminBSORule:%\' Order by b.Name";
            AQLQuery aqlQuery = AQLQuery.parseQuery(sQuery);
            Partition partition = Base.getService().getPartition("pcsv");
            AQLOptions aqlOptions = new AQLOptions(partition);
            ariba.util.javascript.Log.javascript.debug("checkBSO aqlQuery: " + aqlQuery);
            AQLResultCollection aqlResults = Base.getService().executeQuery(aqlQuery, aqlOptions);

            if (aqlResults.getErrors() == null)
            {
                while (aqlResults.next())
                {
                    String sUN = aqlResults.getString(0);
                    String[] splitUniqueName = sUN.split(":");
                    // // EVA001:A247GMU:AdminBSORule:A247-Professional Buyer BSO
                    Log.javascript.debug("Called fire BuysenseadminBSORuleJSUtil::aqlResults.next()" + splitUniqueName);
                    if (splitUniqueName.length < 4) // 
                    {
                        Log.javascript.warning(8888, "BuysenseadminBSORuleJSUtil::Uniquename not set correctly");
                        // continue to the next entry
                        continue;
                    }
                    if((requesterBSO).equals(currentUserBSO))
                    {
                        Log.javascript.debug("BuysenseadminBSORuleJSUtil::Inside getRequester()");
                        return false;
                    }

                    if((requesterBSO).equals(currentUserBSO))
                    {
                        Log.javascript.debug("BuysenseadminBSORuleJSUtil::Inside requesterBSO");
                        return false;
                    }
                    if (!StringUtil.nullOrEmptyOrBlankString(splitUniqueName[3]))
                    {
                        Log.javascript.debug("Called fire BuysenseadminBSORuleJSUtil ::nullOrEmptyOrBlankString" +splitUniqueName[3] );
                        if ((requesterBSO).equals(splitUniqueName[3]) && (!(currentUserBSO.equals(splitUniqueName[3]))) )
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        catch (Exception e)
        {
            Log.javascript.debug("Exception in BuysenseadminBSORuleJSUtil.checkBSO() method" + e.getMessage());
            ariba.util.javascript.Log.javascript.debug("Exception in BuysenseadminBSORuleJSUtil.checkBSO() method "
                    + e.getMessage());
        }
        return false;
    }

    public static String getBSORuleApprover(Requisition r)
    {
        // EVA001:A247GMU:AdminBSORule:A247-Professional Buyer BSO
        // start get BFTD table data

        String sApproverVal = "";
        try
        {

            User partndRequester = ariba.common.core.User.getPartitionedUser(r.getRequester(), r.getPartition());
            String sClientID = (String) partndRequester.getDottedFieldValue("ClientName.UniqueName");
            
            String sQuery = "SELECT b.UniqueName,b,b.Name from ariba.core.BuysenseFieldDataTable b where b.UniqueName like \'"
                    + sClientID + ":AdminBSORule:%\' Order by b.Name";
            AQLQuery aqlQuery = AQLQuery.parseQuery(sQuery);
            Partition partition = Base.getService().getPartition("pcsv");
            AQLOptions aqlOptions = new AQLOptions(partition);
            ariba.util.javascript.Log.javascript.debug("getBSORuleApprover aqlQuery: " + aqlQuery);
            AQLResultCollection aqlResults = Base.getService().executeQuery(aqlQuery, aqlOptions);

            if (aqlResults.getErrors() == null)
            {
                while (aqlResults.next())
                {
                    String sUN = aqlResults.getString(0);
                    String[] splitUniqueName = sUN.split(":");
                    // // EVA001:A247GMU:AdminBSORule:A247-Professional Buyer BSO
                    Log.javascript.debug("Called fire BuysenseadminBSORuleJSUtil::getBSORuleApprover()" +splitUniqueName);
                    if (splitUniqueName.length < 4) // 
                    {
                        Log.javascript.warning(8888, "BuysenseadminBSORuleJSUtil::Uniquename not set correctly");
                        // continue to the next entry
                        continue;
                    }
                    
                    if (!StringUtil.nullOrEmptyOrBlankString(splitUniqueName[4]))
                    {
                           sApproverVal = splitUniqueName[4];
                    }

                }
            }
        }
        catch (Exception e)
        {
            Log.javascript.debug("Exception in BuysenseadminBSORuleJSUtil.getBSORuleApprover() method" + e.getMessage());
            ariba.util.javascript.Log.javascript.debug("Exception in BuysenseadminBSORuleJSUtil.getBSORuleApprover() method " + e.getMessage());
        }
        return sApproverVal;
    }

}
