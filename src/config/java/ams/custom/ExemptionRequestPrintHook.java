package config.java.ams.custom;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.Comment;
import ariba.approvable.core.PrintApprovableHook;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseSession;
import ariba.base.core.LongString;
import ariba.base.fields.FieldProperties;
import ariba.approvable.core.ApprovalRequest;
import ariba.basic.core.Money;
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;
import ariba.util.core.StringUtil;
import ariba.util.formatter.BigDecimalFormatter;
import ariba.util.log.Log;
import ariba.util.net.HTMLPrintWriter;

public class ExemptionRequestPrintHook implements PrintApprovableHook
{
	 private static String msSubmitNote = "<p>The following must be submitted and attached to this request: <br>" +
             "&nbsp;&nbsp;&nbsp;&nbsp; A. If the following documents were not posted in VBO, provide a copy of the solicitation, including all addenda, and the contract award documents, including all modifications. <br>" +
             "&nbsp;&nbsp;&nbsp;&nbsp; B. If solicitation was not posted in VBO, provide documentation that Virginia vendors were given an opportunity to participate in the solicitation and/or received an award. <br>" + 
             "&nbsp;&nbsp;&nbsp;&nbsp; C. Verification that no Mandatory Source or State Contract exists to satisfy the requirement. <br>" +
             "&nbsp;&nbsp;&nbsp;&nbsp; D. A memorandum that addresses the following: <br>" +
                                             "a. Specifically how does the scope of work and pricing of this Cooperative Contract <br>" +
												 "provide for the goods or services the agency is seeking to purchase? <br>" +
                                             "b. Explain why the use of this Cooperative Contract is the best option for the <br>" +
												 "Commonwealth, including why the prices offered in the Cooperative Contract are <br>" + 
												 "considered fair and reasonable.</p>";    
    private static final List NoErrorResult = ListUtil.list(Constants.getInteger(NoError));
    Hashtable hHdrParams = new Hashtable();
        
    public List run(Approvable approvable, PrintWriter out, Locale locale, boolean printForEmail)
    {            
         HTMLPrintWriter loHout = new HTMLPrintWriter(out);
         String lsHtmlPage=null;
         String lsReqType=approvable.getTypeName();
         if (printForEmail)
         {
            Log.customer.debug("PrintForEmail is true");
            return NoErrorResult;
         }
         try
         {           
            if (lsReqType.equals("ariba.core.ExemptionRequest"))
            {    
               int liTag = 0, liTagEnd = -1;
               String lsTag = null;
                 lsHtmlPage = PSGFunctions.ReadFile("config/htmlui/resource/ariba.core.ExemptionRequest.html");
            
              liTag = lsHtmlPage.lastIndexOf("<!--Hdr.");
              liTagEnd = -1;
              Object loFieldVal=null;
              while (liTag >= 0)
              {
                liTagEnd = lsHtmlPage.indexOf("-->", liTag + 1);
                lsTag = lsHtmlPage.substring(liTag + 4, liTagEnd);
                Log.customer.debug("HdrKey: " + lsTag);
                hHdrParams.put(lsTag, "[?]");
                liTag = lsHtmlPage.lastIndexOf("<!--Hdr.", liTag - 1);
              }
              Enumeration loEnumParams = hHdrParams.keys();
              while (loEnumParams.hasMoreElements())
              {                  
                  lsTag = loEnumParams.nextElement().toString();
                  String sFieldName = lsTag.substring(4);
                  if(sFieldName.contains("$"))
                  {   
                      int liFirstIndex=sFieldName.indexOf('$');
                      int liLastIndex=sFieldName.indexOf('$', liFirstIndex+1);
                      String lsFieldName=sFieldName.substring(liLastIndex+2);  
                      String lsStringObj=sFieldName.substring(liFirstIndex+1,liLastIndex);                      
                      loFieldVal = getFieldValue(lsFieldName,lsStringObj,approvable);
                      Log.customer.debug("field value "+loFieldVal);
                  }
                  
                  else if(sFieldName.contains("#"))
                  {
                      Log.customer.debug("Getting the label value");
                      String lsActualFieldName = sFieldName.substring(1);
                      loFieldVal = getLabel(lsActualFieldName,approvable);
                      Log.customer.debug("field value "+loFieldVal);
                  }
                  
                  else if(sFieldName.startsWith("&"))
                  {
                      String lsActualFieldName = sFieldName.substring(1);
                      
                      if(lsActualFieldName.equals("CooperativeContract"))
                      {
                         Object loCooperativeContract = approvable.getDottedFieldValue("CooperativeContract");
                          if (loCooperativeContract != null && !StringUtil.nullOrEmptyOrBlankString((String)loCooperativeContract))
                          {
                             loFieldVal = "<tr>";
                          } 
                          else 
                           {
                             String lsClassValue = "hidden";
                             loFieldVal = "<tr class='" + lsClassValue+ "'>";
                           }
                      }
                      
                      if(lsActualFieldName.equals("Issuedby"))
                      {
                          Object loIssuedby = approvable.getDottedFieldValue("Issuedby");
                          if (loIssuedby != null && !StringUtil.nullOrEmptyOrBlankString((String)loIssuedby))
                          {
                             loFieldVal = "<tr>";
                          } 
                          else 
                           {
                             String lsClassValue = "hidden";
                             loFieldVal = "<tr class='" + lsClassValue+ "'>";
                           }
                      }
                      
                      if(lsActualFieldName.equals("Contact"))
                      {
                          Object loContact = approvable.getDottedFieldValue("Contact");
                          if (loContact != null && !StringUtil.nullOrEmptyOrBlankString((String)loContact))
                          {
                             loFieldVal = "<tr>";
                          } 
                          else 
                           {
                             String lsClassValue = "hidden";
                             loFieldVal = "<tr class='" + lsClassValue+ "'>";
                           }
                      }
                      
                      if(lsActualFieldName.equals("SubmitNote"))
                      {
                          Object oCooperativeContractCB =  (Object)approvable.getDottedFieldValue("CooperativeContractCB");
                          if (oCooperativeContractCB != null && (Boolean)oCooperativeContractCB)
                          {
                             loFieldVal = "<tr>";
                          } 
                          else 
                           {
                             String lsClassValue = "hidden";
                             loFieldVal = "<tr class='" + lsClassValue+ "'>";
                           }
                      }
                  }
                  else if(sFieldName.equals("OtherProcurementFieldName"))
                       {
                         Object loProposedProcObject = approvable.getDottedFieldValue("ProposedProcurement");                          
                         if(loProposedProcObject.equals("Other"))
                         {
                            loFieldVal="Other Proposed Procurement:";
                         }  
                         else
                         {
                            loFieldVal=" ";
                         }
                       }
                 else if(sFieldName.equals("ProcurementMoney") && approvable.getDottedFieldValue(sFieldName) != null)
                       {
                         Money loProcMoney=(Money)approvable.getDottedFieldValue(sFieldName);                             
                         loProcMoney = new Money(BigDecimalFormatter.round(loProcMoney.getAmount(), 2), loProcMoney.getCurrency());
                         loFieldVal= (loProcMoney.asString()).substring(0, (loProcMoney.asString().indexOf(".")+3)).concat(" ").concat(loProcMoney.getCurrency().getSuffix());   
                         Log.customer.debug("Value of ProcurementMoney field  : "+loFieldVal);
                       }
                 else if(sFieldName.equals("Table01Field4") && approvable.getDottedFieldValue(sFieldName) != null)
                 {
                   Money loTable01Field4=(Money)approvable.getDottedFieldValue(sFieldName);                             
                   loTable01Field4 = new Money(BigDecimalFormatter.round(loTable01Field4.getAmount(), 2), loTable01Field4.getCurrency());
                   loFieldVal= (loTable01Field4.asString()).substring(0, (loTable01Field4.asString().indexOf(".")+3)).concat(" ").concat(loTable01Field4.getCurrency().getSuffix());   
                   Log.customer.debug("Value of ProcurementMoney field  : "+loFieldVal);
                 }
                 else if(sFieldName.equals("Table01Field5") && approvable.getDottedFieldValue(sFieldName) != null)
                 {
                   Money loTable01Field5=(Money)approvable.getDottedFieldValue(sFieldName);                             
                   loTable01Field5 = new Money(BigDecimalFormatter.round(loTable01Field5.getAmount(), 2), loTable01Field5.getCurrency());
                   loFieldVal= (loTable01Field5.asString()).substring(0, (loTable01Field5.asString().indexOf(".")+3)).concat(" ").concat(loTable01Field5.getCurrency().getSuffix());   
                   Log.customer.debug("Value of ProcurementMoney field  : "+loFieldVal);
                 }
                  
                 else if(sFieldName.equals("Table01Field41") && approvable.getDottedFieldValue(sFieldName) != null)
                 {
                   Money loTable01Field41=(Money)approvable.getDottedFieldValue(sFieldName);                             
                   loTable01Field41 = new Money(BigDecimalFormatter.round(loTable01Field41.getAmount(), 2), loTable01Field41.getCurrency());
                   loFieldVal= (loTable01Field41.asString()).substring(0, (loTable01Field41.asString().indexOf(".")+3)).concat(" ").concat(loTable01Field41.getCurrency().getSuffix());   
                   Log.customer.debug("Value of ProcurementMoney field  : "+loFieldVal);
                 }
                 else if(sFieldName.equals("Table01Field51") && approvable.getDottedFieldValue(sFieldName) != null)
                 {
                   Money loTable01Field51=(Money)approvable.getDottedFieldValue(sFieldName);                             
                   loTable01Field51 = new Money(BigDecimalFormatter.round(loTable01Field51.getAmount(), 2), loTable01Field51.getCurrency());
                   loFieldVal= (loTable01Field51.asString()).substring(0, (loTable01Field51.asString().indexOf(".")+3)).concat(" ").concat(loTable01Field51.getCurrency().getSuffix());   
                   Log.customer.debug("Value of ProcurementMoney field  : "+loFieldVal);
                 }
                  
                 else if(sFieldName.equals("Table01Field411") && approvable.getDottedFieldValue(sFieldName) != null)
                 {
                   Money loTable01Field411=(Money)approvable.getDottedFieldValue(sFieldName);                             
                   loTable01Field411 = new Money(BigDecimalFormatter.round(loTable01Field411.getAmount(), 2), loTable01Field411.getCurrency());
                   loFieldVal= (loTable01Field411.asString()).substring(0, (loTable01Field411.asString().indexOf(".")+3)).concat(" ").concat(loTable01Field411.getCurrency().getSuffix());   
                   Log.customer.debug("Value of ProcurementMoney field  : "+loFieldVal);
                 }
                 else if(sFieldName.equals("Table01Field511") && approvable.getDottedFieldValue(sFieldName) != null)
                 {
                   Money loTable01Field511=(Money)approvable.getDottedFieldValue(sFieldName);                             
                   loTable01Field511 = new Money(BigDecimalFormatter.round(loTable01Field511.getAmount(), 2), loTable01Field511.getCurrency());
                   loFieldVal= (loTable01Field511.asString()).substring(0, (loTable01Field511.asString().indexOf(".")+3)).concat(" ").concat(loTable01Field511.getCurrency().getSuffix());   
                   Log.customer.debug("Value of ProcurementMoney field  : "+loFieldVal);
                 }
                  
                 else if(sFieldName.equals("Table01Field4111") && approvable.getDottedFieldValue(sFieldName) != null)
                 {
                   Money loTable01Field4111=(Money)approvable.getDottedFieldValue(sFieldName);                             
                   loTable01Field4111 = new Money(BigDecimalFormatter.round(loTable01Field4111.getAmount(), 2), loTable01Field4111.getCurrency());
                   loFieldVal= (loTable01Field4111.asString()).substring(0, (loTable01Field4111.asString().indexOf(".")+3)).concat(" ").concat(loTable01Field4111.getCurrency().getSuffix());   
                   Log.customer.debug("Value of ProcurementMoney field  : "+loFieldVal);
                 }
                 else if(sFieldName.equals("Table01Field5111") && approvable.getDottedFieldValue(sFieldName) != null)
                 {
                   Money loTable01Field5111=(Money)approvable.getDottedFieldValue(sFieldName);                             
                   loTable01Field5111 = new Money(BigDecimalFormatter.round(loTable01Field5111.getAmount(), 2), loTable01Field5111.getCurrency());
                   loFieldVal= (loTable01Field5111.asString()).substring(0, (loTable01Field5111.asString().indexOf(".")+3)).concat(" ").concat(loTable01Field5111.getCurrency().getSuffix());   
                   Log.customer.debug("Value of ProcurementMoney field  : "+loFieldVal);
                 }    
                  
                 else if(sFieldName.equals("Table01Field41111") && approvable.getDottedFieldValue(sFieldName) != null)
                 {
                   Money loTable01Field41111=(Money)approvable.getDottedFieldValue(sFieldName);                             
                   loTable01Field41111 = new Money(BigDecimalFormatter.round(loTable01Field41111.getAmount(), 2), loTable01Field41111.getCurrency());
                   loFieldVal= (loTable01Field41111.asString()).substring(0, (loTable01Field41111.asString().indexOf(".")+3)).concat(" ").concat(loTable01Field41111.getCurrency().getSuffix());   
                   Log.customer.debug("Value of ProcurementMoney field  : "+loFieldVal);
                 }
                 else if(sFieldName.equals("Table01Field51111") && approvable.getDottedFieldValue(sFieldName) != null)
                 {
                   Money loTable01Field51111=(Money)approvable.getDottedFieldValue(sFieldName);                             
                   loTable01Field51111 = new Money(BigDecimalFormatter.round(loTable01Field51111.getAmount(), 2), loTable01Field51111.getCurrency());
                   loFieldVal= (loTable01Field51111.asString()).substring(0, (loTable01Field51111.asString().indexOf(".")+3)).concat(" ").concat(loTable01Field51111.getCurrency().getSuffix());   
                   Log.customer.debug("Value of ProcurementMoney field  : "+loFieldVal);
                 } 
                  
                 else if(sFieldName.equals("SubmitNote"))
                       {
                         loFieldVal=msSubmitNote;
                       }
                 else if(sFieldName.equals("username"))
                       {
                         String loStatus=(String)approvable.getDottedFieldValue("StatusString"); 
                         if(loStatus.equals("Denied"))
                         {
                            loFieldVal="Denied by:";
                         }
                         else
                         {
                           loFieldVal="Approved by:";                                         
                         }                                           
                       }
                 else if(sFieldName.equals("date"))
                       {
                         String loStatus=(String)approvable.getDottedFieldValue("StatusString"); 
                         if(loStatus.equals("Denied"))
                         {
                           loFieldVal="Denied Date:";
                         }
                        else
                        {
                           loFieldVal="Approved Date:";                                              
                        }                                               
                      }
                 else if(sFieldName.contains("Preparer") || sFieldName.contains("Requester"))
                     {
                        String lsObjval=sFieldName.substring(0,sFieldName.indexOf("@"));
                        Log.customer.debug("Object value: "+lsObjval);
                        String lsFieldVal=sFieldName.substring(sFieldName.indexOf("@")+1);
                        Log.customer.debug("field value: "+lsFieldVal);
                        loFieldVal=getReqValue(lsObjval,lsFieldVal,approvable);
                     }
                 else
                     {   
                        loFieldVal = approvable.getDottedFieldValue(sFieldName);                        
                        if(sFieldName.equals("RecurringCommodity") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))
                        {
                          loFieldVal="Yes";
                        }
                        else if(sFieldName.equals("RecurringCommodity") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))
                        {
                           loFieldVal="No";
                        }
                        else if(sFieldName.equals("TypeofRequest"))
                               {
                            
                                String sTypeofRequest = "";
                                Log.customer.debug("converting boolean true to yes for field name :"+sFieldName);
                               // Object oVCERelease = approvable.getDottedFieldValue("VCERelease");
                                Object oContractExemption = approvable.getDottedFieldValue("ContractExemption");
                                Object oExceedDelegatedAuthority = approvable.getDottedFieldValue("ExceedDelegatedAuthority");
                                Object oCooperativeContractCB = approvable.getDottedFieldValue("CooperativeContractCB");
                                Object oOther = approvable.getDottedFieldValue("Other");
                                
/*                                if(oVCERelease != null && (Boolean)oVCERelease)
                                {
                                    sTypeofRequest = sTypeofRequest + "VCE Release,";
                                }*/
                                
                                if(oContractExemption != null && (Boolean)oContractExemption)
                                {
                                    sTypeofRequest = sTypeofRequest + "DPS State Contract ,";
                                }
                                
                                if(oExceedDelegatedAuthority != null && (Boolean)oExceedDelegatedAuthority)
                                {
                                    sTypeofRequest = sTypeofRequest + "VIB,";
                                }
                                
                                if(oCooperativeContractCB != null && (Boolean)oCooperativeContractCB)
                                {
                                    sTypeofRequest = sTypeofRequest + "VDC,";
                                }
                                
                                if(oOther != null && (Boolean)oOther)
                                {
                                    sTypeofRequest = sTypeofRequest + "OGC,";
                                }
                                

                                if(sTypeofRequest!= null && !StringUtil.nullOrEmptyOrBlankString(sTypeofRequest))
                                {
                                    sTypeofRequest = sTypeofRequest.substring(0, (sTypeofRequest.length()-1));
                                    loFieldVal= sTypeofRequest;
                                }
                                 
                               }
                        else if(sFieldName.equals("TypeofProcurement"))
                        {
                     
                         String sTypeofProcurement = "";
                         Log.customer.debug("converting boolean true to yes for field name :"+sFieldName);
                         Object oCooperativeContractCB1 = approvable.getDottedFieldValue("CooperativeContractCB1");
                         Object oExceedDelegatedAuthority1 = approvable.getDottedFieldValue("ExceedDelegatedAuthority1");
                         Object oContractModification1 = approvable.getDottedFieldValue("ContractModification1");
                         Object oFederalGrant = approvable.getDottedFieldValue("FederalGrant");
                         Object oStateGrant = approvable.getDottedFieldValue("StateGrant");
                         Object oOtherField = approvable.getDottedFieldValue("OtherField");
                         
/*                                if(oVCERelease != null && (Boolean)oVCERelease)
                         {
                             sTypeofRequest = sTypeofRequest + "VCE Release,";
                         }*/
                         
                         if(oCooperativeContractCB1 != null && (Boolean)oCooperativeContractCB1)
                         {
                        	 sTypeofProcurement = sTypeofProcurement + "Cooperative Contract ,";
                         }
                         
                         if(oExceedDelegatedAuthority1 != null && (Boolean)oExceedDelegatedAuthority1)
                         {
                        	 sTypeofProcurement = sTypeofProcurement + "Exceed Delegated Authority,";
                         }
                         
                         if(oContractModification1 != null && (Boolean)oContractModification1)
                         {
                        	 sTypeofProcurement = sTypeofProcurement + "Contract Modification,";
                         }
                         
                         if(oFederalGrant != null && (Boolean)oFederalGrant)
                         {
                        	 sTypeofProcurement = sTypeofProcurement + "Federal Grant,";
                         }
                         
                         if(oStateGrant != null && (Boolean)oStateGrant)
                         {
                        	 sTypeofProcurement = sTypeofProcurement + "State Grant,";
                         }
                         if(oOtherField != null && (Boolean)oOtherField)
                         {
                        	 sTypeofProcurement = sTypeofProcurement + "Not listed on form (explain in comments below),";
                         }
                         if(sTypeofProcurement!= null && !StringUtil.nullOrEmptyOrBlankString(sTypeofProcurement))
                         {
                        	 sTypeofProcurement = sTypeofProcurement.substring(0, (sTypeofProcurement.length()-1));
                             loFieldVal= sTypeofProcurement;
                         }
                          
                        }                        

                        }                                      
                if (loFieldVal == null)
                {
                   loFieldVal = "";
                }              
                 hHdrParams.put(lsTag, String.valueOf(loFieldVal));
             }
            lsHtmlPage = PSGFunctions.ReplaceHtml(lsHtmlPage, hHdrParams);            
            loHout.println(lsHtmlPage);  
          }
            
        }
        catch (Exception e) {
            Log.customer.debug(e);
          
          }
         return NoErrorResult;
     }
    
    public String getFieldValue(String fieldName,String stringObj,Approvable approvable)
    {
        Log.customer.debug("Inside getFieldValue method, fieldName :"+fieldName);        
        String lsResultValue="";           
        List loObjlist = (List)approvable.getFieldValue(stringObj);     
        Log.customer.debug("list:" +loObjlist);       
        BaseSession loSession = Base.getSession();       
            for (int i = 0; i < loObjlist.size(); i++)
            {             
                if(i>0)
                {
                    if(fieldName.equals("Text"))
                    {
                     lsResultValue=lsResultValue+"<br>";
                    }
                    else
                    {
                    lsResultValue=lsResultValue+", ";
                    }
                }                
                Object obj = loObjlist.get(i);                             
                BaseObject loObjectName = null;
                try
                {
                    if(obj instanceof Comment)
                    {
                       Comment loCmt = (Comment)loObjlist.get(i);   
                       loObjectName = loCmt;
                    }
                    else if(obj instanceof ApprovalRequest)
                        {
                          ApprovalRequest loAppReq=(ApprovalRequest)loObjlist.get(i);
                          loObjectName = loAppReq;
                        }
                        else
                        {    
                          BaseId loId = (BaseId)loObjlist.get(i);
                          loObjectName = loSession.objectFromId(loId);
                        }                                                   
                    Log.customer.debug("Inside for of getFieldValue, object:"+loObjectName);
                    Object loObjectValue=null;
                    if(fieldName.equals("Text"))
                    {                                    
                        LongString llsText = (LongString) loObjectName.getDottedFieldValue(fieldName);
                        if(llsText != null)
                        {
                           loObjectValue = (String) llsText.string();
                        }                    
                    }
                    else
                    {
                        loObjectValue = loObjectName.getDottedFieldValue(fieldName);
                    } 
                    Log.customer.debug("field name " +loObjectValue);                   
                    if (loObjectValue == null)
                        loObjectValue = ""; // replace NullValue with blank.
                    lsResultValue=lsResultValue+String.valueOf(loObjectValue);                    
                }catch(Exception e)
                {
                    Log.customer.debug("Excception: "+e.getMessage());
                    lsResultValue = "EXCEPTION";
                }
                                                                
             }           
            return lsResultValue;
     }
    public String getReqValue(String objName,String fieldName,Approvable approvable)
    {
        ariba.user.core.User loUser = (ariba.user.core.User)approvable.getDottedFieldValue(objName);
        ariba.common.core.User loPartitionUser = ariba.common.core.User.getPartitionedUser(loUser,approvable.getPartition());
        Object loFieldValue = loPartitionUser.getDottedFieldValue(fieldName);
        if (loFieldValue == null)
        {
            loFieldValue = "";
        }
        Log.customer.debug("Object value in getReqValue mathod: " +loFieldValue);
        String lsResultValue=loFieldValue.toString(); 
        return lsResultValue;
    } 
    private Object getLabel(String lsActualFieldName, Approvable approvable) {

        Log.customer.debug("Inside getLabel method ");
        FieldProperties lfpFieldProperty1 = approvable.getFieldProperties(lsActualFieldName);
        String lsLabel = lfpFieldProperty1.getLabel();
        return lsLabel;
    }

}
