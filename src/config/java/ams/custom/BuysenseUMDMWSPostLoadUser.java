package config.java.ams.custom;

import java.util.List;

import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseVector;
import ariba.base.core.ClusterRoot;
import ariba.base.core.LongString;
import ariba.base.core.LongStringElement;
import ariba.base.core.Partition;
import ariba.basic.core.Currency;
import ariba.basic.core.Money;
import ariba.user.core.Approver;
import ariba.user.core.Group;
import ariba.user.core.User;
import ariba.util.core.ListUtil;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;

public class BuysenseUMDMWSPostLoadUser extends BuysenseUMDMWSPostLoad
{

    private String sClassName = "BuysenseUMDMWSPostLoadUser";
    private String sInernalServiceReply = "SUCCESS";

    boolean checkIfUniqueAribaInstanceExists(ClusterRoot oCustomObj)
    {
        Object cObjUN = oCustomObj.getFieldValue("UniqueName");
        if (cObjUN != null && (cObjUN instanceof String))
        {
            String sUniqueName = (String) cObjUN;
            Partition nonePartition = Base.getService().getPartition("None");
            log(" searching for Shared User " + sUniqueName);
            Object oSharedUser = Base.getService().objectMatchingUniqueName("ariba.user.core.User", nonePartition, sUniqueName);
            if (oSharedUser == null)
            {
                log(" User do not exist in Ariba");
                return false;
            }
        }
        return true;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected void callCreateService()
    {

        boolean bCheckWSCSVFields = true;
        if (!bCheckWSCSVFields)
        {

            appendStatus(cObj, iError, "", "All elments required to fire web service not there in CSV. Ask eMall team to take a look");
            return;
        }

        // get Shared user soap details
        String sSUSOAPMsgFrame = ResourceService.getString(sResourceFile, "UserServiceCreateSUSOAPMsgFrame");
        String sSUSOAPReqElements = ResourceService.getString(sResourceFile, "UserServiceCreateSUSOAPReqElements");
        String sSUSOAPRplElements = ResourceService.getString(sResourceFile, "UserServiceCreateSUSOAPRplElements");
        String sSharedUserSOAPRequestMsg = BuysenseUMDMWSSoap.getSOAPmsg("Plain", "None", sSUSOAPMsgFrame, sSUSOAPReqElements, sSUSOAPRplElements, sInernalServiceReply, cObj);

        log(" shared SOAP msg - " + sSharedUserSOAPRequestMsg.substring(0, sSharedUserSOAPRequestMsg.length() / 2));
        log(" shared SOAP msg - " + sSharedUserSOAPRequestMsg.substring((sSharedUserSOAPRequestMsg.length() / 2)));

        // call shared user service
        log(sClassName + " calling shared user service");
        boolean bSUWSCallSuccessful = BuysenseUMDMWSSoap.invokeWebservice("BuysenseWSSharedUserCreate", "None", sSharedUserSOAPRequestMsg, sSUSOAPRplElements, sInernalServiceReply);
        log(sClassName + " shared user invokeWebservice success?" + bSUWSCallSuccessful);

        // get common user soap details
        String sCUSOAPMsgFrame = ResourceService.getString(sResourceFile, "UserServiceCreateCUSOAPMsgFrame");
        String sCUSOAPReqElements = ResourceService.getString(sResourceFile, "UserServiceCreateCUSOAPReqElements");
        String sCUSOAPRplElements = ResourceService.getString(sResourceFile, "UserServiceCreateCUSOAPRplElements");

        String sCommonUserSOAPRequestMsg = BuysenseUMDMWSSoap.getSOAPmsg("vcsv", "pcsv", sCUSOAPMsgFrame, sCUSOAPReqElements, sCUSOAPRplElements, sInernalServiceReply, cObj);

        log(" common SOAP msg - " + sCommonUserSOAPRequestMsg.substring(0, sCommonUserSOAPRequestMsg.length() / 2));
        log(" common SOAP msg - " + sCommonUserSOAPRequestMsg.substring((sCommonUserSOAPRequestMsg.length() / 2)));
        // before calling common user service, clear BuysenseCatalogController
        // field as this will not be cleared when it is null in wrapper.

        Object cObjUN = cObj.getFieldValue("UniqueName");
        String sUniqueName = (cObjUN == null || !(cObjUN instanceof String)) ? " " : (String) cObjUN;
        ClusterRoot oSharedUser = Base.getService().objectMatchingUniqueName("ariba.user.core.User", Partition.None, sUniqueName);
        Partition pcsvPartition = Base.getService().getPartition("pcsv");
        ariba.user.core.User cSharedUser = (oSharedUser != null && oSharedUser instanceof ariba.user.core.User) ? (ariba.user.core.User) oSharedUser : null;
        ariba.common.core.User cPartitionedUser = null;
        /*
         * if(cSharedUser != null) { cPartitionedUser =
         * ariba.common.core.User.getPartitionedUser(cSharedUser,
         * pcsvPartition); if(cPartitionedUser != null) {
         * cPartitionedUser.setDottedFieldValue("BuysenseCatalogController",
         * null); }
         * 
         * }
         */

        // call common user service
        log(sClassName + " calling common user service");
        boolean bCUWSCallSuccessful = BuysenseUMDMWSSoap.invokeWebservice("BuysenseWSCommonUserCreate", "pcsv", sCommonUserSOAPRequestMsg, sCUSOAPRplElements, sInernalServiceReply);
        log(sClassName + " common user invokeWebservice success? " + bCUWSCallSuccessful);

        // process response and set appropriate status
        if (!bSUWSCallSuccessful || !bCUWSCallSuccessful)
        {
            appendStatus(cObj, iError, "", "Error in invoking ariba web service. Ask eMall team to take a look");
            return;
        }

        // add this user to respective Groups

        Object oGroups = cObj.getDottedFieldValue("Roles");
        // User can have zero Groups (Roles). if Roles field is null, remove
        // User from all existing Group(s).
        if (oGroups == null)
        {
            removeUserFromAllGroups(oSharedUser);
        }
        else
        {

            // for update operation, remove this user from all groups and then
            // add to the specific groups
            // Scenario: Say user is in 5 groups and in the update, only 3
            // groups are sent. (the intension is to remove the user from 2
            // groups).
            // In this case, we do not know what all Groups removed. Hence, need
            // to re-map fully.
            String sCustomObjOperation = (String) cObj.getDottedFieldValue("Operation");
            if (BuysenseUMDMWSOperations.isUpdateOperation(sCustomObjOperation))
            {
                removeUserFromAllGroups(oSharedUser);
                removeUserFromAllRoles(cSharedUser);
            }

            LongString pLS = (LongString) oGroups;
            List lStrings = pLS.getStrings();
            String sGroupString = "";
            for (int p = 0; p < lStrings.size(); p++)
            {
                log(sClassName + " string values " + ((LongStringElement) lStrings.get(p)).getString());
                sGroupString = sGroupString + ((LongStringElement) lStrings.get(p)).getString();
            }
            log(sClassName + " final string values:" + sGroupString);

            String sGroups[] = sGroupString.split(",");
            for (int k = 0; k < sGroups.length; k++)
            {
                String sGroupUN = sGroups[k];
                log(sClassName + " sGroupUN " + sGroupUN);
                ClusterRoot cGroup = Base.getService().objectMatchingUniqueName("ariba.user.core.Group", Partition.None, sGroupUN);
                if (cGroup != null)
                {
                    Group group = (Group) cGroup;
                    log(sClassName + " group " + group);
                    List lUsers = group.getUsers();
                    log(sClassName + " oSharedUser.id " + oSharedUser.id);
                    ListUtil.addElementIfAbsent(lUsers, oSharedUser.id);
                    group.save();
                }
            }
        }

        // Update for common User

        if (cSharedUser != null)
        {
            cPartitionedUser = ariba.common.core.User.getPartitionedUser(cSharedUser, pcsvPartition);
        }

        Object cExpApprover = cObj.getFieldValue("ExpendLimitExceededApprover");
        Object cExpUserRoleFlag = cObj.getFieldValue("ExpendLimitExceededUserRoleFlag");

        if (cExpApprover != null && cExpUserRoleFlag != null)
        {

            // cExpApprover ex - B222-Expenditure Limit Approver
            // cExpUserRoleFlag will - Role/User

            String sUName = (!StringUtil.nullOrEmptyOrBlankString((String) cExpApprover)) ? ((String) cExpApprover).trim() : " ";
            String sApproverType = (!StringUtil.nullOrEmptyOrBlankString((String) cExpUserRoleFlag)) ? ((String) cExpUserRoleFlag).trim() : " ";
            Approver oApprover = null;

            if (!StringUtil.nullOrEmptyOrBlankString(sApproverType) && sApproverType.equalsIgnoreCase("Role"))
            {
                ClusterRoot crGroup = Base.getService().objectMatchingUniqueName("ariba.user.core.Group", Partition.None, sUName);
                oApprover = (crGroup != null) ? (Approver) crGroup : null;
            }
            if (!StringUtil.nullOrEmptyOrBlankString(sApproverType) && sApproverType.equalsIgnoreCase("User"))
            {
                oApprover = User.getUser(sUName, "SingleSignOnWSAdapter");
            }

            log(sClassName + " setting Expenditure_Limit_Exceeded_Approver " + oApprover);
            cPartitionedUser.setDottedFieldValue("Expenditure_Limit_Exceeded_Approver", oApprover);
        }
        // Set Expenditure_Limit & Delegated_Purchasing_Authority

        Object cExpLimit = cObj.getFieldValue("Expenditure_Limit");
        Object cDPAmount = cObj.getFieldValue("Delegated_Purchasing_Authority");
        if (cExpLimit == null)
        {
            cPartitionedUser.setDottedFieldValue("Expenditure_Limit", null);
        }
        else
        {
            Money expLimitAmount = new Money(Double.parseDouble((String) cExpLimit), Currency.getDefaultCurrency(Partition.None));
            cPartitionedUser.setDottedFieldValue("Expenditure_Limit", expLimitAmount);
            log(sClassName + " user Expenditure_Limit set to " + expLimitAmount);
        }

        if (cDPAmount == null)
        {
            cPartitionedUser.setDottedFieldValue("Delegated_Purchasing_Authority", null);
        }
        else
        {
            Money dpaAmount = new Money(Double.parseDouble((String) cDPAmount), Currency.getDefaultCurrency(Partition.None));
            cPartitionedUser.setDottedFieldValue("Delegated_Purchasing_Authority", dpaAmount);
            log(sClassName + " user DPA set to " + dpaAmount);
        }
        log(sClassName + " obj cc:" + cObj.getFieldValue("BuysenseCatalogController"));
        if (cObj.getFieldValue("BuysenseCatalogController") != null)
        {
            ClusterRoot clrCatalogController = Base.getService().objectMatchingUniqueName("ariba.core.BuySenseCatalogController", pcsvPartition,
                    (String) cObj.getFieldValue("BuysenseCatalogController"));
            cPartitionedUser.setDottedFieldValue("BuysenseCatalogController", clrCatalogController);
        }
        else
        {
            cPartitionedUser.setDottedFieldValue("BuysenseCatalogController", null);
        }
        log(sClassName + " CC:" + cPartitionedUser.getDottedFieldValue("BuysenseCatalogController"));
        if (cObj.getFieldValue("BillingAddress") == null)
        {
            cPartitionedUser.getUser().getBillingAddresses().clear();
        }

        // jb - changes for mapping BuysEformProfiles to users CSPL-7092
        Object cEformProfiles = cObj.getDottedFieldValue("EformProfiles");
        // remove all if no profiles are sent
        // use user cPartitionedUser
        if (cEformProfiles == null)
        {
            removeAllEformProfilesFromUser(cPartitionedUser);
        }
        else
        {

            // for update operation, remove all profiles from this user and then
            // add the ones specified
            String sCustomObjOperation = (String) cObj.getDottedFieldValue("Operation");
            if (BuysenseUMDMWSOperations.isUpdateOperation(sCustomObjOperation))
            {
                removeAllEformProfilesFromUser(cPartitionedUser);
            }

            LongString pLS = (LongString) cEformProfiles;
            List lStrings = pLS.getStrings();
            String sEformProfString = "";
            for (int p = 0; p < lStrings.size(); p++)
            {
                log(sClassName + " eform prof string values " + ((LongStringElement) lStrings.get(p)).getString());
                sEformProfString = sEformProfString + ((LongStringElement) lStrings.get(p)).getString();
            }
            log(sClassName + " final eform prof string values:" + sEformProfString);

            String sEformProfiles[] = sEformProfString.split(",");
            for (int k = 0; k < sEformProfiles.length; k++)
            {
                String sEformProfUN = sEformProfiles[k];
                log(sClassName + " sEformProfUN " + sEformProfUN);
                ClusterRoot cEformProf = Base.getService().objectMatchingUniqueName("ariba.core.BuysEformProf", pcsvPartition, sEformProfUN);
                if (cEformProf != null)
                {
                    log(sClassName + " cEformProf " + cEformProf);
                    BaseVector eforProfList = (BaseVector) cPartitionedUser.getDottedFieldValue("BuysEformProfiles");
                    if (eforProfList != null)
                    {
                        log(sClassName + " eforProfList not null");
                    }
                    eforProfList.add(cEformProf.getBaseId());
                    cPartitionedUser.save();
                    // log(sClassName + " oSharedUser.id " + oSharedUser.id);
                }
            }
        }
        // end of CSPL-7092 coding
    }

    @SuppressWarnings({ "rawtypes" })
    private void removeUserFromAllGroups(ClusterRoot oSharedUser)
    {

        log(sClassName + " in removeUserFromAllGroups");
        List lAllGroups = (List) oSharedUser.getDottedFieldValue("Groups");

        if (lAllGroups == null)
        {
            return;
        }

        for (int i = 0; i < lAllGroups.size(); i++)
        {
            BaseId groupBID = (BaseId) lAllGroups.get(i);
            Group group = (Group) groupBID.get();
            log(sClassName + " group " + group);
            List lUsers = group.getUsers();
            log(sClassName + " oSharedUser.id " + oSharedUser.id);
            boolean isRemoved = lUsers.remove(oSharedUser.id); // remove will
                                                               // not throw
                                                               // exception, if
                                                               // the object is
                                                               // not in list
            log(sClassName + " User was removed from Group? " + isRemoved);
            group.save();
            // Base.getSession().transactionCommit();
        }
    }

    @SuppressWarnings("unused")
    private void removeUserFromAllRoles(ClusterRoot oSharedUser)
    {
        oSharedUser = (ariba.user.core.User) oSharedUser;
        log(sClassName + " in removeUserFromAllGroups");
        List<?> lRoles = (List<?>) oSharedUser.getDottedFieldValue("Roles");

        if (lRoles == null)
        {
            return;
        }
        lRoles.clear();
    }

    protected void update()
    {
        log(sClassName + " in update");
        callCreateService();
    }

    protected void delete()
    {
        log(sClassName + " in delete");
        Partition pcsvPartition = Base.getService().getPartition("pcsv");
        String sUniqueName = (String) cObj.getFieldValue("UniqueName");
        ariba.user.core.User cSharedUser = (ariba.user.core.User) Base.getService().objectMatchingUniqueName("ariba.user.core.User", Partition.None, sUniqueName);
        // Shared user deactivated
        removeUserFromAllGroups(cSharedUser); // De-link Users from all Groups
                                              // if any
        removeUserFromAllRoles(cSharedUser); // De-link Users from all Roles if
                                             // any
        cSharedUser.setAdapterSource("None:SharedUserDelete.csv");
        cSharedUser.setFieldValue("Active", false);
        ariba.common.core.User cPartitionedUser = ariba.common.core.User.getPartitionedUser(cSharedUser, pcsvPartition);
        // Partitioned user deactivated
        cPartitionedUser.setAdapterSource("pcsv:UserDelete.csv");
        cPartitionedUser.setFieldValue("Active", false);
    }

    @SuppressWarnings({ "rawtypes" })
    private void removeAllEformProfilesFromUser(ClusterRoot oSharedUser)
    {

        log(sClassName + " in removeAllEformProfilesFromUser");
        BaseVector lAllEformProfiles = (BaseVector) oSharedUser.getDottedFieldValue("BuysEformProfiles");

        if (lAllEformProfiles == null)
        {
            return;
        }

        lAllEformProfiles.removeAllElements();
    }

}
