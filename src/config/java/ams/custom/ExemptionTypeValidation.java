package config.java.ams.custom;
//Checks the field validity of ReqHeadCB5Value and BypassPreEncumbranceEncumbrance. If ReqHeadCB5Value is true we have to check BypassPreEncumbranceEncumbrance .
import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ConditionResult;
import ariba.base.fields.ValueInfo;
import ariba.util.core.Fmt;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class ExemptionTypeValidation extends Condition
{
    private static final ValueInfo parameterInfo[];
    protected String             sResourceFile = "ariba.procure.core";
    private static         String        errorStringTable            = "buysense.eform";
    private static         String        eVAExemptionReqValidation  = "ExemptionRequestError";
    private static         String        eVAExemptionReqValidationError  = "TypeOfExemptionRequestError";
    String msMessage = null;
    
    public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
    {
        Log.customer.debug("Calling ExemptionTypeValidation .");
        try
        {
            if(!evaluate(value, params))
            {
                String retMsg = null;
                if(!StringUtil.nullOrEmptyOrBlankString(msMessage))
                {
                    retMsg = msMessage;
                    msMessage = null;
                    return new ConditionResult(retMsg);
                }
                else
                    return null;
            }
        }
        catch (ConditionEvaluationException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
    public boolean evaluate(Object value, PropertyTable params) throws ConditionEvaluationException
    {

        BaseObject obj   = (BaseObject)params.getPropertyForKey("SourceObject");
        Log.customer.debug("inside ExemptionTypeValidation SourceObject"+obj);
        
        if(obj.instanceOf("ariba.core.ExemptionRequest"))
        {
          Boolean lbContractExemption = (obj.getFieldValue("ContractExemption") == null)? false:(Boolean) obj.getFieldValue("ContractExemption");
          Boolean lbExceedDelegatedAuthority = (obj.getFieldValue("ExceedDelegatedAuthority") == null)? false:(Boolean) obj.getFieldValue("ExceedDelegatedAuthority");
          Boolean lbCooperativeContractCB = (obj.getFieldValue("CooperativeContractCB") == null)? false:(Boolean) obj.getFieldValue("CooperativeContractCB");
          Boolean lbOther = (obj.getFieldValue("Other") == null)? false:(Boolean) obj.getFieldValue("Other");
          Boolean lbCooperativeContractCB1 = (obj.getFieldValue("CooperativeContractCB1") == null)? false:(Boolean) obj.getFieldValue("CooperativeContractCB1");
          Boolean lbExceedDelegatedAuthority1 = (obj.getFieldValue("ExceedDelegatedAuthority1") == null)? false:(Boolean) obj.getFieldValue("ExceedDelegatedAuthority1");
          Boolean lbContractModification1 = (obj.getFieldValue("ContractModification1") == null)? false:(Boolean) obj.getFieldValue("ContractModification1");
          Boolean lbFederalGrant = (obj.getFieldValue("FederalGrant") == null)? false:(Boolean) obj.getFieldValue("FederalGrant");
          Boolean lbStateGrant = (obj.getFieldValue("StateGrant") == null)? false:(Boolean) obj.getFieldValue("StateGrant");
          Boolean lbOtherField = (obj.getFieldValue("OtherField") == null)? false:(Boolean) obj.getFieldValue("OtherField");  
       
          if((lbContractExemption == false)&&(lbExceedDelegatedAuthority == false)&&(lbCooperativeContractCB == false) && (lbStateGrant == false) && (lbOtherField == false) && (lbFederalGrant == false) && (lbContractModification1 == false) && (lbOther == false) && (lbExceedDelegatedAuthority1 == false) && (lbCooperativeContractCB1 == false))
          {     
              Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : None of the checkbox have been set");
              msMessage = Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidationError);
              return false;
          } 
          }
        else
        {
            Log.customer.debug("ariba.core.ExemptionRequest");
            return false;
        }
        return false; 
    }         
    

    
    protected ValueInfo[] getParameterInfo()
    {
       return parameterInfo;
    }   
    static
    {
         parameterInfo = (new ValueInfo[] {new ValueInfo("SourceObject", 0)});
    }
}


