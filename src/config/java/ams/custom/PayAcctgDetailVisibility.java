// Version 1.0

//***************************************************************************/
//  Anup - September 2001 - Trigger for visibility of accounting lines in Accounting Details  
//
//***************************************************************************/
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.core.*;
import java.math.BigDecimal;
import ariba.util.log.Log;


//81->822 changed ConditionValueInfo to ValueInfo
public class PayAcctgDetailVisibility extends Condition
{

    private static final ValueInfo parameterInfo[];
    private static final String EqualToMsg1 = "EqualToMsg1";

    public boolean evaluate(Object value, PropertyTable params)
    {
       Log.customer.debug("PayAcctgDetailVisibility: This is the object we get in the evaluate: %s ", value);
       BaseObject bo = (BaseObject)params.getPropertyForKey("TargetValue");
       
       BigDecimal payingQty = (BigDecimal)bo.getFieldValue("CurPayingQuantity");
       if (payingQty.compareTo(new BigDecimal(0)) > 0)
        return true;
       else
        return false;
             
    }

    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    static
    {
        parameterInfo = (new ValueInfo[] {
            new ValueInfo("TargetValue", 0),new ValueInfo("Message", 0, Behavior.StringClass)
        });
    }
}
