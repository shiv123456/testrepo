/*


    Responsible: rangle
*/
/* 08/11/2003: Updates for Ariba 8.0 (Jeff Namadan) */
/* 09/05/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLQuery;
import ariba.common.core.*;
import ariba.base.fields.*;
/* Ariba 8.0: Replaced ariba.common.core.nametable.NamedObjectNameTable) */
import ariba.base.core.aql.AQLNameTable;
/* Ariba 8.0: Replaced ariba.util.core.Util */
import java.util.List;
import ariba.util.log.Log;

/**
        This class changes the behaviour of the BuysenseOrg chooser in the User Profile UI. It filters the BuysenseOrg
        based on the ClientName (i.e. only BuysenseOrgs associated with the same client that a user selects on the user profile
        will be shown).
*/

/* Ariba 8.0: Replaced NamedObjectNameTable */
//81->822 changed Vector to List
public class BuysenseUserProfileBuysenseOrgNameTable extends AQLNameTable implements InitializeValueSource 
{
    /**
        Overridden to check the results to see if the approvable is
        an eform.
    */
    public void initializeValueSource(ValueSource vs) 
    {
        Log.customer.debug("Hello from CUSTOM SUPPLIER LOCATION NAME TABLE CLASS 0");
        /**
        if (vs instanceof SupplierLocation) {
            Supplier supplier = supplier();
            if (supplier != null)
                ((SupplierLocation)vs).setSupplier(supplier);
        }
        */
    }
    
    public List matchPattern (String field, String pattern) 
    {
        Log.customer.debug("Hello from CUSTOM SUPPLIER LOCATION NAME TABLE CLASS 1");
        List results = super.matchPattern(field, pattern);
        return results;
    }
    
    public void addQueryConstraints (AQLQuery query,
                                     String field, String pattern) 
    {
        Log.customer.debug("My name is rohit");
        
        // build the default set of field constraints for the pattern
        super.addQueryConstraints(query, field, pattern);
        beginSystemQueryConstraints(query);
        
        /** 
            Extract a ClientName from the UserProfile or UserProfileDetails, and use it 
            to create an AQL statement
        */
        
        // Get the client id selected for the UserProfileDetails
        BaseObject clientName = (BaseObject)clientName();
        BaseId id = clientName == null ? new BaseId(0L,
                                                    0) : clientName.getBaseId();
        
        // Create the AQL based on the ClientName that was returned
        AQLCondition clientNameCond =
            AQLCondition.buildEqual(query.buildField("ClientName"),id);
        
        query.and(clientNameCond);
        
        endSystemQueryConstraints(query);
    }
    
    /* Ariba 8.0: NamedObjectNameTable was deprecated by AQLNameTable.
       This class was changed to derive from AQLNameTable and the
       method constraints() is not in this class and isn't needed.
    public List constraints (String field, String pattern) {
        Log.customer.debug("Hello from CUSTOM SUPPLIER LOCATION NAME TABLE CLASS 3");
        List constraints = super.constraints(field, pattern);
        return constraints;
    } */
    
    private ValueSource clientName() 
    {
        Log.customer.debug("in clientName method");
        ValueSource context = getValueSourceContext();
        if (context instanceof UserProfile) 
        {
            return (ValueSource)context.getDottedFieldValue("Details.ClientName");            
        }
        if (context instanceof UserProfileDetails) 
        {
            return (ValueSource)context.getFieldValue("ClientName");
        }
        else
            return null;
    }
}
