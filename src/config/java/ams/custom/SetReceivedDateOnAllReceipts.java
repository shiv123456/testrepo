package config.java.ams.custom;

import java.math.BigDecimal;

import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import ariba.util.core.Date;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
public class SetReceivedDateOnAllReceipts extends Action
{

    public void fire(ValueSource object, PropertyTable params)
    {
    	Log.customer.debug("Calling SetReceivedDateOnAllReceipts.");
        BaseVector rclines = (BaseVector)object.getFieldValue("ReceiptItems");
        Date editDate = (Date)object.getFieldValue("BuysenseEditDate");
        Log.customer.debug("the receipts lines." + rclines);
        int rcLines = rclines.size();
        if(rclines!=null)
        for(int i=0;i<rcLines;i++)
        {
            Log.customer.debug("Inside for Loop...# : %s", new Integer(i));
            BaseObject receiptItem = (BaseObject)rclines.get(i);
			Boolean loMassEdit = ((Boolean) receiptItem.getFieldValue("MassEditForLine"));
			BigDecimal loNumAcc = (BigDecimal) receiptItem.getFieldValue("NumberAccepted");
            Log.customer.debug("loNumAcc = "+loNumAcc);
            BigDecimal loNumRej = (BigDecimal) receiptItem.getFieldValue("NumberRejected");
            Log.customer.debug("loNumAcc = "+loNumRej);
            
            if(editDate !=null && (loMassEdit!=null && loMassEdit.booleanValue()) && (loNumAcc.longValue()!= 0 || loNumRej.longValue()!=0) )
            {
            	receiptItem.setFieldValue("Date",editDate);
            	
            }
        }
    }
}
