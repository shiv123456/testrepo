//***************************************************************************/
//  Copyright (c) 1996-2000 Ariba Technologies, Inc.
//  All rights reserved. Patents pending.
//
//  Description:  Implementation of failed transactions scheduled task
//
//  Requires the following entry in ScheduledTasks.table
//
//    AMSFailedTXN = {
//      ScheduledTaskClassName="config.java.ams.custom.AMSFailedTXN";
//      EmailPermission = "Users with this permission will receive email notification";
//      DBType = DB Type;
//      DBDriver = DB Driver location;
//      DBPassword = password;
//      DBURL = URL including DB Host Name or IP/Port/Instance;
//      DBUser = userId;
//    };
//
//  Change History:
//  04-09-01 KTL - Created Class
//  08-06-03 Anup - Redid the task to be compliant with Buyer 7.1a.
//                  Parameterized the repush logic, enhanced logging.
//
//***************************************************************************/

/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/

package config.java.ams.custom;

import java.util.Map;
import java.util.List;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.core.Partition;
import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
// Ariba 8.1: Deprecate ariba.integration.meta.ConnectionInfo;
//import ariba.integration.meta.ConnectionInfo;
// Ariba 8.1: Deprecate ariba.common.core.Permission;
import ariba.user.core.Permission;
import ariba.util.core.*;
import ariba.util.formatter.DateFormatter;
import ariba.util.log.Log;
// Ariba 8.1: Deprecate ariba.server.objectserver.core.ServerParameters;
//import ariba.server.objectserver.core.ServerParameters;
// Ariba 8.1: Deprecate ariba.server.objectserver.SimpleScheduledTask
import ariba.util.scheduler.*;
import java.sql.*;
import java.util.Properties;
import java.util.StringTokenizer;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;

import org.apache.log4j.Level;

import ariba.approvable.core.ApprovableType;

// Ariba 8.1: Changed FieldListTask to extend from ScheduledTask instead of SimpleScheduledTask
// Since SimpleScheduledTask is deprecated and ScheduledTask is offered as the alternative.
public class AMSFailedTXN extends ScheduledTask {

    private final String ERPORDER = "ORDERS";
    private final String BUYSENSEORG = "BSOS";
    private final String USERPROFILE = "PROFILES";

    private String msFailedTXNPermission = "";
    private String msDBType = null ;
    private String msDBDriver = null ;
    private String msDBPassword = null ;
    private String msDBURL = null ;
    private String msDBUser = null ;
    private String msFailedTXNParamFile = null;
    private String msTransactions = null;
    private String msOrderPrefix = null ;
    private String msBSOPrefix = null ;
    private String msUPPrefix = null ;
    private String msStartDate = null ;
    private String msEndDate = null ;
    private ariba.util.core.Date moStartDate = null;
    private ariba.util.core.Date moEndDate = null;
    private boolean mbRepush = false;
    private boolean mbLogging = false;
    private boolean mbTransactionTypes = false ;
    private boolean mbAllTransactions = false ;
    // Ariba 8.1: The Map constructor has been deprecated by MapUtil::newTable()
    // 81->822 changed Vector to List
    // 81->822 changed Hashtable to Map
    private Map loParamHashtable = MapUtil.map();

    private Connection moJDBCConn = null;
    private String msEmailBody = "";

    // Ariba 8.1: Replace partition with scheduler
    public void init(Scheduler scheduler, String scheduledTaskName, Map arguments)
    {
        // Ariba 8.1: Replace partition with scheduler
        super.init(scheduler, scheduledTaskName, arguments);
        String lsKey = null;
        // Ariba 8.1: Now using java's Enumeration
        // Ariba 8.1: Map::keys() is deprecated by Map.keySet.iterator()
        // 81->822 changed Enumeration to Iterator
        for(java.util.Iterator e = arguments.keySet().iterator(); e.hasNext();)
        {
           lsKey = (String)e.next();
           if ( lsKey.equals( "DBType" ) )
           {
              msDBType = (String)arguments.get( lsKey );
              Log.customer.debug( "Connection parameter specified as " + msDBType + "." ) ;
           }
           else if ( lsKey.equals( "DBDriver" ) )
           {
              msDBDriver = (String)arguments.get( lsKey );
              Log.customer.debug( "Connection parameter specified as " + msDBDriver + "." ) ;
           }
           else if ( lsKey.equals( "DBPassword" ) )
           {
              msDBPassword = (String)arguments.get( lsKey );
              Log.customer.debug( "Connection parameter specified as " + msDBPassword + "." ) ;
           }
           else if ( lsKey.equals( "DBURL" ) )
           {
              msDBURL = (String)arguments.get( lsKey );
              Log.customer.debug( "Connection parameter specified as " + msDBURL + "." ) ;
           }
           else if ( lsKey.equals( "DBUser" ) )
           {
              msDBUser = (String)arguments.get( lsKey );
              Log.customer.debug( "Connection parameter specified as " + msDBUser + "." ) ;
           }
           else if(lsKey.equals("EmailPermission"))
           {
              msFailedTXNPermission = (String) arguments.get(lsKey);
              Log.customer.debug("Email permission parameter specified as " + msFailedTXNPermission + ".");
           }
           else if(lsKey.equals("ParamFile"))
           {
              msFailedTXNParamFile = (String) arguments.get(lsKey);
              Log.customer.debug("The parameters file specified as " + msFailedTXNParamFile + ".");
           }

        }
    }

    public void run()
    {
        msEmailBody = "";

        if (OpenTXNConn() == false)
        {
            Log.customer.debug("Could not open jdbc connection.  Terminating...");
            return;
        }

        ProcessTXN();

        if (msEmailBody.length() != 0)
        {
            SendSummary();
        }

        try
        {
            moJDBCConn.close();
        }
        catch (Exception e)
        {
            Log.customer.debug("Error in closing jdbc connection: " + e.toString());
        }
    }

    private boolean OpenTXNConn()
    {
       try
       {
          Class.forName(msDBDriver);
          moJDBCConn = (Connection) DriverManager.getConnection(msDBURL, msDBUser, msDBPassword);
          moJDBCConn.setAutoCommit(false);
       }
       catch (Exception e)
       {
          Log.customer.debug("Error in opening jdbc connection: " + e.toString());
          return false;
       }

       return true;
    }

    private boolean FindTXN(String tin)
    {
       String lsSql = "select integratorstatus from txnheader where tin = '" + tin + "'";
       boolean status = true;

       try
       {
          java.sql.Statement selSQL = moJDBCConn.createStatement();
          ResultSet txnRS = selSQL.executeQuery(lsSql);
          status = txnRS.next();
          txnRS.close();
       }
       catch (Exception e)
       {
          Log.customer.debug("Error in selecting TIN: " + tin + ", " + e.toString());
          return false;
       }

       return status;
    }

    private void ProcessTXN()
    {
       //Get and validate parameters to determine scope of processing
       if (!loadAndValidateParams())
       {
          Logs.buysense.debug("The parameters in " + msFailedTXNParamFile + " are not valid." );
          return ;
       }

       // Construct SQL
       String lsSQLStatement = constructSQL();

       /* Ariba 8.0: The constructor for AQLOptions has been deprecated.
          No longer need to pass a second boolean parameter.  */

       //rgiesen dev SPL #39
       Partition partition = Base.getSession().getPartition();
       //AQLOptions loOptions = new AQLOptions(Base.getSession().getPartition());
       AQLOptions loOptions = new AQLOptions(partition);

       loOptions.setActualParameters(loParamHashtable);
       AQLQuery loQuery = AQLQuery.parseQuery(lsSQLStatement);
       AQLResultCollection loResults = Base.getService().executeQuery(loQuery, loOptions);

       int liNumOfTxn = loResults.getSize();
       Log.customer.debug("ProcessTXN returned " + liNumOfTxn + " transactions in sending status.");
       Logs.buysense.debug("Based on criteria, number of candidate transactions" +
                           " for repush evaluation = " + liNumOfTxn);

       String lsCurTin = null;
       int liNumRepushed = 0;
       while (loResults.next())
       {
          lsCurTin = (String) loResults.getString(0);

          if (FindTXN(lsCurTin) == false)
          {
             try
             {
                Log.customer.debug("TIN " + lsCurTin + " not found in transaction table.");

                ClusterRoot loTxnObj = null;
                loTxnObj = (ClusterRoot)Base.getService().objectMatchingUniqueName(
                                                "ariba.core.Buysense.TXN",
                                                partition,lsCurTin);
                                                //Base.getSession().getPartition(),lsCurTin);
                                                //rgiesen dev SPL #39

                Logs.buysense.debug("TIN : " + lsCurTin + " Approvable : " +
                                    (String) loTxnObj.getFieldValue("ERPNUMBER") +
                                    " is candidate for repush.");


                if (mbRepush)
                {
                   Log.customer.debug("Repushing TIN : " + lsCurTin + ".");
                   AMSTXN.txnPublish(loTxnObj);
                   msEmailBody = msEmailBody + "TIN: " +
                             (String) loTxnObj.getFieldValue("TIN") +
                             ", Type: " +
                             (String) loTxnObj.getFieldValue("TXNTYPE") +
                             ", ERPNumber: " +
                             (String) loTxnObj.getFieldValue("ERPNUMBER") +
                             " has been republished.\n";

                   Logs.buysense.debug("TIN : " + lsCurTin + " repushed.");
                }

                liNumRepushed++;
             }
             catch (Exception e)
             {
                Log.customer.debug("Caught exception with publish, " + e.toString());
                continue;
             }
          }
          else
          {
             Log.customer.debug("TIN " + lsCurTin + " found in transaction table.");
          }
       }// end while

       if (mbRepush)
       {
          Logs.buysense.debug("Repushed " + liNumRepushed + " transactions.");
       }
       else
       {
          Logs.buysense.debug("Based on criteria and scan of transaction table, number of" +
                              " transactions found as repush candidates = " + liNumRepushed);
       }
    }

    private void SendSummary()
    {
        Permission pa = Permission.getPermission(msFailedTXNPermission);
        if (pa == null)
        {
            Log.customer.debug("Could not retrieve permission " + msFailedTXNPermission + ".");
            return;
        }

        // Ariba 8.1: List::users() has been deprecated by List::getAllUsers()
        List users = pa.getAllUsers();
        if(users == null || users.isEmpty())
        {
            Log.customer.debug("No users found that has the " + msFailedTXNPermission + " permission.");
            return;
        }

        String subject = "FailedTXN scheduled task republished the following transactions.";

        BuysenseEmailAdapter.sendMail(users, subject, msEmailBody);
    }

    private boolean loadAndValidateParams()
    {
       Properties loProperties = new Properties();
       FileInputStream loFileInputStream;
       try
       {
          loFileInputStream = new FileInputStream(msFailedTXNParamFile);
          loProperties.load(loFileInputStream);
       }
       catch(IOException loIOE)
       {
          Log.customer.debug("Problem reading properties file ...");
       }

       // Now determine if we have the necessary inputs
       msTransactions = loProperties.getProperty("Transactions").toUpperCase();
       msStartDate = loProperties.getProperty("StartDate");
       msEndDate = loProperties.getProperty("EndDate");

       String lsRepush = loProperties.getProperty("Repush");
       /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
       if (StringUtil.nullOrEmptyOrBlankString(lsRepush))
          mbRepush = false;
       else
          /* Ariba 8.1: Replaced Util.getBoolean */
          mbRepush = Constants.getBoolean(lsRepush).booleanValue();

       String lsLogging = loProperties.getProperty("Logging");
       /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
       if (StringUtil.nullOrEmptyOrBlankString(lsLogging))
          mbLogging = false;
       else
          /* Ariba 8.1: Replaced Util.getBoolean */
          mbLogging = Constants.getBoolean(lsLogging).booleanValue();

       if (mbLogging)
          //Logs.buysense.setDebugOn();
           Log.customer.setLevel(Level.DEBUG);

       /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
       if (StringUtil.nullOrEmptyOrBlankString(msTransactions))
       {
          // No transactions are specified - error
          return false;
       }

       // Clean up unnecessary comma separators
       if (msTransactions.startsWith(","))
          msTransactions = msTransactions.substring(1);

       if (msTransactions.endsWith(","))
          msTransactions = msTransactions.substring(0, msTransactions.length() -1);


       /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
       if (StringUtil.nullOrEmptyOrBlankString(msTransactions))
       {
          // No transactions are specified - error
          return false;
       }

       // Get the UniqueName prefix for ERPOrder, BuysenseOrg and UserProfile
       ApprovableType loTargetType = null;

	   //rgiesen dev SPL #39
	   Partition partition = Base.getSession().getPartition();

       loTargetType = (ApprovableType)Base.getService().objectMatchingUniqueName(
                                "ariba.approvable.core.ApprovableType",
                                partition,
                                "ariba.purchasing.core.ERPOrder");
       msOrderPrefix = loTargetType.getIdPrefix();

       loTargetType = (ApprovableType)Base.getService().objectMatchingUniqueName(
                                       "ariba.approvable.core.ApprovableType",
                                       partition,
                                       "ariba.core.BuysenseOrgEform");
       msBSOPrefix = loTargetType.getIdPrefix();

       loTargetType = (ApprovableType)Base.getService().objectMatchingUniqueName(
                                              "ariba.approvable.core.ApprovableType",
                                              partition,
                                              "ariba.common.core.UserProfile");
       msUPPrefix = loTargetType.getIdPrefix();

       // Reset booleans for multiple runs of the task
       mbAllTransactions = false;
       mbTransactionTypes = false;
       loParamHashtable.clear();

       if ((msTransactions.indexOf("ALL")) >= 0 )
       {
          mbAllTransactions = true ;
       }

       if (mbAllTransactions)
       {
          if ((msTransactions.indexOf(ERPORDER))      >= 0 ||
              (msTransactions.indexOf(BUYSENSEORG))   >= 0 ||
              (msTransactions.indexOf(USERPROFILE))   >= 0 ||
              (msTransactions.indexOf(msOrderPrefix)) >= 0 ||
              (msTransactions.indexOf(msBSOPrefix))   >= 0 ||
              (msTransactions.indexOf(msUPPrefix))    >= 0
             )
          {
             // All transaction types and specific transaction types/transactions must
             // not be specified for the same run of the task - error
             return false ;
          }
       }
       else
       {
          if ((msTransactions.indexOf(ERPORDER))    >= 0  ||
              (msTransactions.indexOf(BUYSENSEORG)) >= 0  ||
              (msTransactions.indexOf(USERPROFILE)) >= 0
             )
          {
             if ((msTransactions.indexOf(msOrderPrefix)) >= 0 ||
                 (msTransactions.indexOf(msBSOPrefix))   >= 0 ||
                 (msTransactions.indexOf(msUPPrefix))    >= 0
                )
             {
                // Specific transaction Types and transactions must not be
                // specified for the same run of the task - error
                return false;
             }
             else
             {
                mbTransactionTypes = true;
             }
          }
          else
          {
             if (!((msTransactions.indexOf(msOrderPrefix)) >= 0 ||
                   (msTransactions.indexOf(msBSOPrefix))   >= 0 ||
                   (msTransactions.indexOf(msUPPrefix))    >= 0
                  )
                )
             {
                // Specific transaction Types or transactions must be
                // specified for the same run of the task - error
                return false;
             }

          }
       }

       if (!(mbAllTransactions || mbTransactionTypes))
       {
          // we have specific transactions to process
          // insert single quotes for each transaction
          String lsTempTransactions = null;
          String lsToken = null;
          int i = 0;
          StringTokenizer loStrTokenizer = null;

          loStrTokenizer = new StringTokenizer(msTransactions, "," );
          while ( loStrTokenizer.hasMoreTokens() )
          {
             lsToken = (loStrTokenizer.nextToken()).trim();

             if (i ==0)
             {
                lsTempTransactions = "'" + lsToken + "'";
             }
             else
             {
                lsTempTransactions = lsTempTransactions + ",'" + lsToken + "'";
             }

             i++;
          }

          msTransactions = lsTempTransactions;

       }

       // Validate dates if specified
       /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
       if (!StringUtil.nullOrEmptyOrBlankString(msStartDate))
       {
          try
          {
             moStartDate = DateFormatter.parseDate(msStartDate, "MM/dd/yyyy");
          }
          catch (ParseException loPE)
          {
             Logs.buysense.debug("Exception when parsing StartDate.");
             return false;
          }
       }


       /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
       if (!StringUtil.nullOrEmptyOrBlankString(msEndDate))
       {
          try
          {
             moEndDate = DateFormatter.parseDate(msEndDate, "MM/dd/yyyy");
             if (moStartDate.compareTo(moEndDate) > 0)
             {
                //Start Date cannot be after End Date
                Logs.buysense.debug("StartDate cannot be after EndDate.");
                return false;
             }
             ariba.util.core.Date.addDays(moEndDate, 1);
             msEndDate = (ariba.util.core.Date.getMonth(moEndDate) + 1) + "/" +
                          ariba.util.core.Date.getDayOfMonth(moEndDate) + "/" +
                          ariba.util.core.Date.getYear((java.util.Date)moEndDate);
          }
          catch (ParseException loPE)
          {
             Logs.buysense.debug("Exception when parsing EndDate.");
             return false;
          }
       }

       loParamHashtable.put("StartDate", moStartDate);
       loParamHashtable.put("EndDate", moEndDate);

       return true;
    }

    private String constructSQL()
    {
       String lsSQL = "SELECT UniqueName FROM ariba.core.Buysense.TXN" +
                      " WHERE PUSHSTATUS = 'Sending'";

       if (moStartDate != null)
          lsSQL = lsSQL + " And CREATEDATE >= :StartDate";

       if (moEndDate != null)
          lsSQL = lsSQL + " And CREATEDATE <= :EndDate";

       if (mbAllTransactions)
       {
          lsSQL = lsSQL + " And (ERPNUMBER like '" + msBSOPrefix + "%' Or" +
                          " ERPNUMBER like '" + msUPPrefix + "%' Or" +
                          " (ERPNUMBER like '" + msOrderPrefix + "%' And TXNTYPE = 'PC'))";
       }
       else if (mbTransactionTypes)
       {
          boolean lbAddedTypeWhere = false;
          if (msTransactions.indexOf(BUYSENSEORG) >= 0)
          {
             lsSQL = lsSQL + " And (ERPNUMBER like '" + msBSOPrefix + "%'";
             lbAddedTypeWhere = true;
          }
          if (msTransactions.indexOf(USERPROFILE) >= 0)
          {
             if (lbAddedTypeWhere)
             {
                lsSQL = lsSQL + " Or ERPNUMBER like '" + msUPPrefix + "%'";
             }
             else
             {
                lsSQL = lsSQL + " And (ERPNUMBER like '" + msUPPrefix + "%'";
                lbAddedTypeWhere = true;
             }
          }
          if (msTransactions.indexOf(ERPORDER) >= 0 )
          {
             if(lbAddedTypeWhere)
             {
                lsSQL = lsSQL + " Or (ERPNUMBER like '" + msOrderPrefix + "%' And TXNTYPE = 'PC')";
             }
             else
             {
                lsSQL = lsSQL + " And (ERPNUMBER like '" + msOrderPrefix + "%' And TXNTYPE = 'PC'";
             }
          }

          lsSQL = lsSQL + ")";
       }
       else
       {
          lsSQL = lsSQL + " And ERPNUMBER IN (" + msTransactions + ")";
       }

       Log.customer.debug("SQL = " + lsSQL);

       return lsSQL;

    }

}
