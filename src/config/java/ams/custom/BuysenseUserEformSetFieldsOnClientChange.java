package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseUserEformSetFieldsOnClientChange extends Action
{
    public final String    sClassName         = "config.java.ams.custom.BuysenseUserEformSetFieldsOnClientChange";
    private final String[] formFields_toClear = { "RadioButtons", "eMall", "ElectronicForms", "Other", "LogiXML", "QuickQuote", "VBOBuyer" };

    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        Log.customer.debug(sClassName + " valuesource obj:" + valuesource);
        Approvable loAppr = null;

        if (valuesource != null && valuesource instanceof Approvable)
        {
            loAppr = (Approvable) valuesource;
            Object oAgency = loAppr.getDottedFieldValue("Client.ClientName");

            loAppr.setFieldValue("Agency", oAgency);
            for (int i = 0; i < (formFields_toClear.length - 1); i++)
            {
                String sFformFieldName = formFields_toClear[i];
                Log.customer.debug(sClassName + " Field " + sFformFieldName + " is setting to null");
                loAppr.setDottedFieldValueRespectingUserData(sFformFieldName, null);
            }

            setCustomTitle((String) oAgency, loAppr);
            loAppr.save();
        }
    }

    public void setCustomTitle(String clientName, Approvable appr)
    {
        String lsFirstName = (String) appr.getFieldValue("FirstName");
        String lsLastName = (String) appr.getFieldValue("LastName");
        String lsName = lsFirstName + " " + lsLastName;
        String lsTitle = "New User Request For ";
        if (lsName != null)
        {
            appr.setName(clientName + " - " + lsTitle + " " + lsName);
            Log.customer.debug("The form title is now" + appr.getFieldValue("Name"));
        }
    }
}
