package config.java.ams.custom;

/**
 * CSPL #: 4205-Datafix
 * @author Partho Sarathi
 * Date  : 17-Apr-2012
 * @version 1.0
 * Explanation: This is one time task to update drzone and dryearsofexperience fields
 * for historical CounterProposal documents
 */

import java.util.List;
import java.util.Map;

import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.collaboration.core.CollaborationLineItem;
import ariba.collaboration.core.CounterProposal;
import ariba.procure.core.CategoryLineItemDetails;
import ariba.purchasing.core.ReqLineItem;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;
import ariba.util.scheduler.ScheduledTask;
import ariba.util.scheduler.Scheduler;
import ariba.workforce.core.LaborLineItemDetails;

public class BuysenseCounterProposalDataFix extends ScheduledTask
{


	   private String msClassName = this.getClass().getName();

	   public void init(Scheduler scheduler, String scheduledTaskName, Map arguments)
		{
		   Log.customer.debug("%s - init", msClassName);
		   super.init(scheduler, scheduledTaskName, arguments);
		}

	   public void run()
		{
           //Execute the query to identify the CounterProposal's to fix.
		   Log.customer.debug("%s - run", msClassName);
		   AQLResultCollection loProposalResults = getAllCounterProposaltoFix();
		   processCounterProposalDoc(loProposalResults);

		}


	private AQLResultCollection getAllCounterProposaltoFix()
	{
		Partition pcsvPartition = Base.getService().getPartition("pcsv");
		AQLOptions loOptions = new AQLOptions(pcsvPartition);
		String sQuery = "SELECT distinct CounterProposal FROM ariba.collaboration.core.CounterProposal INCLUDE INACTIVE " +
	   		"JOIN ariba.collaboration.core.CollaborationLineItem AS LineItems USING CounterProposal.LineItems " +
	   		"JOIN ariba.procure.core.CategoryLineItemDetails AS CategoryLineItemDetailsVector USING LineItems.CLICategoryLineItemDetailsVector " +
	   		"JOIN ariba.workforce.core.LaborLineItemDetails AS LLI USING LineItems.CLICategoryLineItemDetailsVector[0] " +
	   		"JOIN ariba.purchasing.core.ReqLineItem AS rli USING LineItems.OriginatingLineItem " +
	   		"WHERE " +
	   		"(LLI.drzone is null or LLI.dryearsexperience is null) AND " +
	   		"(LineItems.Description.UpdatedCatalogItemType != 'system:consultinglabor') AND " +
	   		"rli is not null Order by UniqueName desc ";
		   try{
			   AQLQuery loQuery = AQLQuery.parseQuery(sQuery);
			   Log.customer.debug("%s - executing query -%s", msClassName,loQuery);
			   AQLResultCollection loResults = Base.getService().executeQuery(loQuery, loOptions);
			   if (loResults.getFirstError() != null)
			   {
				   Log.customer.warning(8000,"%s - error in AQL results - %s", msClassName,loResults.getFirstError().toString());
				   return null;
			   }else
			   {
				   return loResults;
			   }
		   }catch(Exception e)
		   {
			   Log.customer.warning(8000,"%s - Exception in AQL execution -%s", msClassName, e.getMessage());
			   return null;
		   }
	}


	private void processCounterProposalDoc(AQLResultCollection loCounterProposalResults)
	{
		Log.customer.debug("%s - processing counterproposal ", msClassName);
	       if (loCounterProposalResults != null)
	       {
	    	   try
	    	   {
		    	   while (loCounterProposalResults.next())
		           {
		    		   BaseId bi = null;
					try
		    	       {
							bi = (ariba.base.core.BaseId)loCounterProposalResults.getBaseId(0);
							CounterProposal loCounterProposalToFix = (CounterProposal) bi.get();
							Log.customer.debug("%s - Processing counterproposal lines for %s", msClassName,loCounterProposalToFix );
							processCLIs(loCounterProposalToFix.getLineItems());
		    	       }
					   catch(Exception e)
		    	       {
						   Log.customer.warning(8000,msClassName+" - Exception in getting Proposal object for BaseId "+ bi+", skipping it");
						   continue;
		    	       }

		           }
	    	   }catch(Exception e)
	    	   {
	    		   Log.customer.warning(8000," Exception in "+ msClassName +" runmethod -" + e.getMessage());
	    	   }
	       }else
	       {
	    	   Log.customer.debug("%s - No Objects to fix", msClassName );
	    	   return;
	       }
	}

	private void processCLIs(List loCollabLines)
	{
		   CollaborationLineItem loCollabLine = null;
		   for (int i=0; i<loCollabLines.size(); i++)
		   {
			   loCollabLine = (CollaborationLineItem) loCollabLines.get(i);
			   if(loCollabLine.getCategoryLineItemDetails() != null &&
						  loCollabLine.getCategoryLineItemDetails() instanceof LaborLineItemDetails)
					   {
						   Log.customer.debug("%s processCLIs - processing line %s", msClassName, (i+1));
						   setDRFields(loCollabLine);
					   }

		   }
	}

	private void setDRFields(CollaborationLineItem loCounterPropLine)
	{
		Log.customer.debug("%s - setting fields ", msClassName );
		String sDRZONE = "drzone";
		String sDRYOE = "dryearsexperience";
		CategoryLineItemDetails loCounterPropCatline = loCounterPropLine.getCategoryLineItemDetails();
		if(loCounterPropLine.getOriginatingLineItem() != null)
		{
			ReqLineItem loReqLine = (ReqLineItem)loCounterPropLine.getOriginatingLineItem();
			CategoryLineItemDetails loReqCatline = loReqLine.getCategoryLineItemDetails();
			if(loReqCatline != null)
			{
				String lsDRZONE = (String) loReqCatline.getDottedFieldValue(sDRZONE);
				String lsDRYOE = (String) loReqCatline.getDottedFieldValue(sDRYOE);

				if(!StringUtil.nullOrEmptyOrBlankString(lsDRZONE))
				{
					loCounterPropCatline.setDottedFieldValue(sDRZONE, lsDRZONE);
				}

				if(!StringUtil.nullOrEmptyOrBlankString(lsDRYOE))
				{
					loCounterPropCatline.setDottedFieldValue(sDRYOE, lsDRYOE);
				}
			}
		}
		else
		{
			Log.customer.warning(8000," No Req details available for above line, skipping the line (respective Order may be in Ordering)", msClassName);
		}


	}

}
