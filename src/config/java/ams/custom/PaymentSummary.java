// Version 1.1

// ahiranandani - WA ST SPL # 475/Baseline Dev SPL # 229 - PaymentAccountings --> Accountings.PaymentAccountings
// ahiranandani - WA Dev SPL # 223 - Called CalculateTotals.java to sync up accounting line changes on summary page.
// ahiranandani - WA Dev SPL # 232 - Modified logic to add CM lines in Accounting Summary. Removed call to CalculateTotals.java.
/* 09/05/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 10/10/2003: Updates for Ariba 8.1 (Richard Lee) */
// Ariba 8.1 replaced all the elementAt with get
/*
03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/04/2004: rgiesen dev SPL #38 - replace getLeadCurrency with Currency.getDefaultCurrency(partition)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/

package config.java.ams.custom;

import ariba.base.fields.Action;
import ariba.base.fields.ValueInfo;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import java.util.List;
// Ariba 8.0: Replaced deprecated Util with StringUtil
import ariba.util.core.StringUtil;
//import ariba.util.core.Util;
import ariba.base.core.ClusterRoot;
import ariba.base.core.BaseObject;
import ariba.util.log.Log;
import ariba.base.core.Base;
import ariba.base.core.Partition;
import java.math.BigDecimal;
// Ariba 8.1 Money and Currency has changed from common to basic
import ariba.basic.core.Money;
import ariba.basic.core.Currency;

//import ariba.core.BuysenseFieldDataTable;


//import ariba.core.BuysenseFieldDataTable;
//81->822 changed ConditionValueInfo to ValueInfo
//81->822 changed Vector to List
public class PaymentSummary extends Action
{
    static int lineNum=0;
    //private static final List RefAcctgFieldsVector = (List)FieldListContainer.getRefAcctgFields();
    //private static final List AcctgFieldsVector = (List)FieldListContainer.getAcctgChooserFieldValues();
    private static List RefAcctgFieldsVector = (List)FieldListContainer.getRefAcctgFields();
    private static List AcctgFieldsVector = (List)FieldListContainer.getAcctgChooserFieldValues();
    static ClusterRoot cr;
    static final String Value = "Value";
    private static final ValueInfo[] parameterInfo =
        {
        new ValueInfo(TargetParam, true, IsScalar, StringClass)
    };

    private static final String[] requiredParameterNames =
        {
        TargetParam
    };

    private static final ValueInfo valueInfo =
        new ValueInfo(IsScalar, "ariba.core.PaymentEform");



    public void fire (ValueSource object, PropertyTable params)
    {
        // Dev SPL 232 - Remove call to CalculateTotals.java
        //Dev SPL 223 - Call CalculateTotals
        //CalculateTotals ct = new CalculateTotals();
        //ct.fire(object,params);


        RefAcctgFieldsVector = (List)FieldListContainer.getRefAcctgFields();
        AcctgFieldsVector = (List)FieldListContainer.getAcctgChooserFieldValues();
        Log.customer.debug("In fire of Payment Summary");
        // Ariba 8.1: List::count() is deprecated by List::size()
        Log.customer.debug("Size of RefAcctgFieldsVector: %s",
                       RefAcctgFieldsVector.size());
        // Ariba 8.1: List::count() is deprecated by List::size()
        Log.customer.debug("Size of AcctgFieldsVector: %s",
                       AcctgFieldsVector.size());
        //Get the Payment Eform as a cluster root
        cr = (ClusterRoot)object;

        /*
        String triggerString=(String)cr.getFieldValue("");
        Log.customer.debug("Summary Trigger: %s",triggerString);
        if(triggerString==null)
        {
            return;
        }
        */

        List paymentAcctgSummary = (List)cr.getFieldValue("PaymentAcctgSummaryItems");
        if(paymentAcctgSummary.size()!=0)
        {
            paymentAcctgSummary.clear();
            cr.setFieldValue("PaymentAcctgSummaryItems",paymentAcctgSummary);
        }

        int i;
        int numOfUniqueLines = 0;
        List LineItems = (List)cr.getFieldValue("PaymentItems");
        // Ariba 8.1: List::count() is deprecated by List::size()
        for (i=0;i<(LineItems.size());i++)
        {
            BaseObject li = (BaseObject) BaseObject.create("ariba.core.PaymentItem", cr.getPartition());
            li = (BaseObject) LineItems.get(i);

            //Get SplitAccounting vector
            List SplitAccounting = (List)li.getDottedFieldValue("Accountings.PaymentAccountings");
            // Ariba 8.1: List::count() is deprecated by List::size()
            Log.customer.debug("PaymentAccountings count is %s",
                           SplitAccounting.size());
            Log.customer.debug("PaymentSplitAccounting vector created!");
            // Ariba 8.1: List::count() is deprecated by List::size()
            for (int acctLinePerLineItem = 0; acctLinePerLineItem < (SplitAccounting.size()); acctLinePerLineItem++)
            {
                // Dev SPL 232
                /*Boolean checkBoxValue = (Boolean)(((BaseObject)SplitAccounting.get(acctLinePerLineItem)).getFieldValue("LineAcctCB1Value"));
                  if(checkBoxValue != null)
                  {
                      if(checkBoxValue.booleanValue())
                      {
                          continue;
                      }
                  }  */
                if (isLineUnique(li, acctLinePerLineItem))
                {
                    numOfUniqueLines++;
                    Log.customer.debug("numOfUniqueLines: %s", numOfUniqueLines );
                    Log.customer.debug("Matching Element: %s",  lineNum );

                    //Sunil:
                    //ClusterRoot refActg = (ClusterRoot)ClusterRoot.create("ariba.core.RefAccounting", cr.getPartition());
                    BaseObject refActg = (BaseObject)BaseObject.create("ariba.core.ReferenceAccounting", cr.getPartition());
                    /**
                    Set the Accounting field values in the refAccounting object and
                   push it into vector
                   */

                    Log.customer.debug("IXM Got here %s", i );

                    Log.customer.debug("Line Item %s", li.toString() );

                    List PayAccountings = (List)li.getDottedFieldValue("Accountings.PaymentAccountings");
                    Boolean checkBoxValue = (Boolean)(((BaseObject)PayAccountings.get(acctLinePerLineItem)).getFieldValue("LineAcctCB1Value"));
                    if((checkBoxValue == null) || !checkBoxValue.booleanValue())
                        refActg.setFieldValue("LineAcctText10Value", "");
                    else
                        refActg.setFieldValue("LineAcctText10Value", "CM");

                    refActg.setFieldValue("LineNum",
                                          new String(""+lineNum));
                    refActg.setFieldValue("RXRefLineNum",
                                          (String)li.getDottedFieldValue("Accountings.SplitAccountings[" + acctLinePerLineItem + "].RefRXHeaderLineNum"));

                    /**
                    Loop through static vectors and pick-up Buysense-related attribute names; Set field values on refActg by
                    dynamically creating the following command:
                    refActg.setFieldValue("Field1",(String)li.getDottedFieldValue
                                                       ("Accountings.SplitAccountings[" + acctLinePerLineItem + "].FieldDefault1.Name"));
                    */
                    Log.customer.debug("IIB Finished creating RefAcctgFieldsVector and AcctgFieldsVector");
                    // Ariba 8.1: List::count() is deprecated by List::size()
                    for (int x = 0; x < RefAcctgFieldsVector.size(); x++)
                    {
                        String RefAcctgField = (String)RefAcctgFieldsVector.get(x);
                        String AcctgField = (String)AcctgFieldsVector.get(x);
                        refActg.setFieldValue(RefAcctgField,
                                              (String)li.getDottedFieldValue
                                              ("Accountings.PaymentAccountings[" + acctLinePerLineItem + "]." + AcctgField + ".Name"));
                    }
                    BigDecimal temp = (BigDecimal)li.getDottedFieldValue
                        ("Accountings.PaymentAccountings[" + acctLinePerLineItem + "].PaymentAmount.Amount");
                    if (temp == null)
                        temp = new BigDecimal(0);
                    temp = temp.setScale(2, BigDecimal.ROUND_HALF_UP);
                    refActg.setFieldValue("LineAmount",
                                          temp);

                    //??- ask Rajiv
                    //refActg.setFieldValue("RefReqUniqueName",(String)li.getDottedFieldValue("Requisition.UniqueName"));

                    Log.customer.debug("IXM Got past setting values");
                    //Sunil:
                    //refActg.save();

                    // Ariba 8.1: List::addElement() is deprecated by List::add()
                    ((List)cr.getFieldValue("PaymentAcctgSummaryItems")).add(refActg);

                    li.setDottedFieldValue("Accountings.PaymentAccountings[" + acctLinePerLineItem + "].RefPCHeaderLineNum",new String("" + lineNum));
                    Log.customer.debug("IXM Got past setting the vector "  );

                }
                else
                {
                    Log.customer.debug("IXM in the non-unique section setting the line num "  );
                    li.setDottedFieldValue("Accountings.PaymentAccountings[" + acctLinePerLineItem + "].RefPCHeaderLineNum",new String("" + lineNum));
                }
            }//end of for 1
        }//end of for 2
        // Call setPaymentSummaryLabels in ReqDefaulter to default the labels for summary vector.
        /* ReqDefaulter reqDefaulter = new ReqDefaulter();
        Log.customer.debug("After instantiating reqDefaulter");
        reqDefaulter.setPaymentSummaryLabels(object,params);
        Log.customer.debug("After calling setPaymentSummaryLabels from PaymentSummary");
        */
        //cr.save();
        // Set Net Amount
        Money total = (Money)cr.getFieldValue("TotalPaymentAmount");
        BigDecimal CMLineAmount = new BigDecimal(0);
        BigDecimal CMLinesTotal = new BigDecimal(0);
        List RefActgVector = (List) cr.getFieldValue("PaymentAcctgSummaryItems");
        // Ariba 8.1: List::count() is deprecated by List::size()
        for (int j=0 ; (j < RefActgVector.size()); j++)
        {
            BaseObject crRefActg = (BaseObject)RefActgVector.get(j);
            String CMCheck = (String)crRefActg.getFieldValue("LineAcctText10Value");
            // Ariba 8.0: Replaced deprecated method
            if (StringUtil.nullOrEmptyOrBlankString(CMCheck))
                continue;
            else
            {
                CMLineAmount = (BigDecimal)crRefActg.getDottedFieldValue("LineAmount");
                CMLinesTotal = CMLinesTotal.add(CMLineAmount);
            }
        }
        BigDecimal netAmt = (total.getAmount()).subtract(CMLinesTotal);

        //rgiesen dev SPL #38 - replace getLeadCurrency with Currency.getDefaultCurrency(partition)
        Partition part = cr.getPartition();
        Money cmPaymentAmount = new Money(CMLinesTotal,
                                          Currency.getDefaultCurrency(part));
        Money netPaymentAmount = new Money(netAmt,
                                           Currency.getDefaultCurrency(part));
        cr.setFieldValue("CMPaymentAmount", cmPaymentAmount);
        cr.setFieldValue("NetPaymentAmount", netPaymentAmount);
    }

    private static boolean isLineUnique(BaseObject bo, int acctLinePerLineItem)
    {
        String actgLine = "";
        List PayAccountings = (List)bo.getDottedFieldValue("Accountings.PaymentAccountings");
        Boolean checkBoxValue = (Boolean)(((BaseObject)PayAccountings.get(acctLinePerLineItem)).getFieldValue("LineAcctCB1Value"));
        if((checkBoxValue == null) || !checkBoxValue.booleanValue())
            actgLine += "N";
        else
            actgLine += "Y";

        Log.customer.debug("In is like unique for #: %s",acctLinePerLineItem);
        Log.customer.debug("At the begining of isLineUnique");
        List RefActgVector = (List) cr.getFieldValue("PaymentAcctgSummaryItems");
        // Ariba 8.1: List::count() is deprecated by List::size()
        Log.customer.debug("After getting RefActgVector count is: %s",
                       new Integer(RefActgVector.size()));
        Log.customer.debug("At the begining of isLineUnique 2 ");
        int j;

        //String actgLine = (String)bo.getFieldValue("RefRXHeaderLineNum") ;
        //Retrieve the SplitAccountings amount information from the LineItems
        BigDecimal actgAmount = (BigDecimal)bo.getDottedFieldValue
            ("Accountings.PaymentAccountings[" + acctLinePerLineItem + "].PaymentAmount.Amount");
        if (actgAmount == null)
            actgAmount = new BigDecimal(0);
        actgAmount = actgAmount.setScale(2, BigDecimal.ROUND_HALF_UP);

        //Create actgLine string for comparisson with the existing refLine string

        /**
        Loop through static vectors and pick-up Buysense-related attribute names; Create actgLine string by
        dynamically creating the following command:
        String actgLine = (String)bo.getDottedFieldValue
                          ("Accountings.SplitAccountings[" + acctLinePerLineItem + "].FieldDefault1.Name") + [...]
        */

        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        //String clientUniqueName = (String)cr.getDottedFieldValue("Requester.BuysenseOrg.ClientName.UniqueName");
        ariba.user.core.User RequesterUser = (ariba.user.core.User)cr.getDottedFieldValue("Requester");
        //rgiesen dev SPL #39
        Partition crPartition = cr.getPartition();
        //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,crPartition);
        String clientUniqueName = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.UniqueName");
        String UniqueName, targetFieldName;
        Boolean payVisible;
        Log.customer.debug("IIB isLineUnique actgLine");
        // Ariba 8.1: List::count() is deprecated by List::size()
        for (int z = 0; z < RefAcctgFieldsVector.size(); z++)
        {
            // Summary Fix
            targetFieldName = (String)RefAcctgFieldsVector.get(z);
            UniqueName = (clientUniqueName+":"+targetFieldName);
            ClusterRoot fieldTable = Base.getService().objectMatchingUniqueName
                ("ariba.core.BuysenseFieldTable",
                 	crPartition, UniqueName);
                //rgiesen dev SPL #39
                //Base.getSession().getPartition(), UniqueName);
            payVisible = ((Boolean)fieldTable.getFieldValue("VisibleOnPayment"));
            if (payVisible.booleanValue())
            {
                String tempZ = (String)AcctgFieldsVector.get(z);
                Log.customer.debug("After tempz %s",tempZ);
                actgLine += (String)bo.getDottedFieldValue("Accountings.PaymentAccountings[" + acctLinePerLineItem + "]." + tempZ + ".Name");
            }

        }

        Log.customer.debug("At the begining of isLineUnique 3 ");
        Log.customer.debug("This is the actgline : %s ", actgLine);
        // Ariba 8.1: List::count() is deprecated by List::size()
        Log.customer.debug("After getting RefActgVector count is: %s" ,
                       new Integer(RefActgVector.size()));

        // Ariba 8.1: List::count() is deprecated by List::size()
        for (j=0 ; (j < RefActgVector.size()); j++)
        {
            Log.customer.debug("Beginning of for %s",j);
            //Sunil: care
            //BaseId refBaseId = (BaseId)RefActgVector.get(j);

            //Log.customer.debug("After setting BaseID to"+ refBaseId);
            //Sunil:
            //ClusterRoot crRefActg = Base.getService().getObjectWithReadLock(refBaseId);

            //Sunil: care
            //BaseObject crRefActg = Base.getService().getObjectWithReadLock(refBaseId);
            BaseObject crRefActg = (BaseObject)RefActgVector.get(j);
            Log.customer.debug("After setting BaseObject crRefActg to %s",
                           crRefActg);
            Log.customer.debug("After casting");

            //Create refLine string

            /**
            Loop through static vectors and pick-up Buysense-related attribute names; Create refLine string by
            dynamically creating the following command:
            String refLine = (String)crRefActg.getDottedFieldValue("Field1") + [...]
            */
            Log.customer.debug("IIB refLine");
            String refLine = "";
            String CMFlag = (String)crRefActg.getFieldValue("LineAcctText10Value");
            // Ariba 8.0: Replaced deprecated method
            if (StringUtil.nullOrEmptyOrBlankString(CMFlag))
                refLine += "N";
            else
                refLine += "Y";

            // Ariba 8.1: List::count() is deprecated by List::size()
            for (int y = 0; y < RefAcctgFieldsVector.size(); y++)
            {
                targetFieldName = (String)RefAcctgFieldsVector.get(y);
                UniqueName = (clientUniqueName+":"+targetFieldName);
                ClusterRoot fieldTable = Base.getService().objectMatchingUniqueName
                    ("ariba.core.BuysenseFieldTable",
                     	crPartition, UniqueName);
                //rgiesen dev SPL #39
                     //Base.getSession().getPartition(), UniqueName);
                payVisible = ((Boolean)fieldTable.getFieldValue("VisibleOnPayment"));
                if (payVisible.booleanValue())
                {
                    String tempW = (String)RefAcctgFieldsVector.get(y);
                    refLine += (String)crRefActg.getDottedFieldValue(tempW);
                }
            }

            Log.customer.debug("This is the refline : %s", refLine);

            /**
            If the actgLine is the same as an existing refLine, its dollar
            amount will be added to the existing refLine, and no new refLine
            will be created
            */
            Log.customer.debug("Washington - This is the actgline : %s ", actgLine);
            Log.customer.debug("This is the actgline : %s ", refLine);
            if (refLine.equals(actgLine))
            {
                lineNum = j+1;
                BigDecimal totalAmount = (BigDecimal)crRefActg.getDottedFieldValue("LineAmount");
                totalAmount = totalAmount.add(actgAmount);
                totalAmount = totalAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
                Log.customer.debug("This is the total amount 1: %s", totalAmount);
                crRefActg.setFieldValue("LineAmount",totalAmount);
                Log.customer.debug("In is like unique for #: %s",
                               acctLinePerLineItem);
                return false;
            }
        }
        lineNum = j+1;
        Log.customer.debug("This is the line num in the method  : %s", lineNum);
        Log.customer.debug("In is like unique for #: %s",acctLinePerLineItem);
        return true;
    }

    /**
        Return the list of valid value types.
    */
    protected ValueInfo getValueInfo ()
    {
        return valueInfo;
    }

    /**
        Return the parameter names.
    */
    protected ValueInfo[] getParameterInfo ()
    {
        return parameterInfo;
    }

    /**
        Return the required parameter names.
    */
    protected String[] getRequiredParameterNames ()
    {
        return requiredParameterNames;
    }
}
