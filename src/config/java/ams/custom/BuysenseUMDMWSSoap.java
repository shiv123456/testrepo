package config.java.ams.custom;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;
import ariba.base.core.ClusterRoot;
import ariba.util.core.ListUtil;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;
import ariba.util.parameters.AppInfo;
import ariba.util.parameters.Parameters;

public class BuysenseUMDMWSSoap
{
    private static String sClassName = "BuysenseUMDMWSSoap";

    public static String getSOAPmsg(String sVariant, String sPartition, String sMsgFrame, String sSOAPReqElements,
            String sSOAPRplElement, String sInernalServiceReply, ClusterRoot cObj2)
    {
        String sSoapMsgFrame = sMsgFrame;
        List<String> lSOAPElements = ListUtil.arrayToList(sSOAPReqElements.split(";"));

        Log.customer.debug(sClassName + " sVariant:" + sVariant + " sPartition:" + sPartition + " sMsgFrame:"
                + sMsgFrame);
        Log.customer.debug(sClassName + " sSOAPReqElements:" + sSOAPReqElements + " sSOAPRplElement:" + sSOAPRplElement
                + " sInernalServiceReply:" + sInernalServiceReply);

        // get instance name Example: dev is DEV9r1
        // Partition sPart = Base.getSession().getPartition(sPartition);
        Parameters p = Parameters.getInstance();
        String sInstanceName = (String) p.getParameter("System.Base.InstanceName");

        sSoapMsgFrame = PSGFunctions.ReplaceString(sSoapMsgFrame, "<!--InstanceName-->", sInstanceName, true);

        for (int i = 0; i < lSOAPElements.size(); i++)
        {
            String sCurrentElement = lSOAPElements.get(i);
            if (sCurrentElement.equalsIgnoreCase("variant"))
            {
                sSoapMsgFrame = PSGFunctions.ReplaceString(sSoapMsgFrame, "<!--" + sCurrentElement + "-->", sVariant,
                        true);
                continue;
            }

            if (sCurrentElement.equalsIgnoreCase("partition"))
            {
                sSoapMsgFrame = PSGFunctions.ReplaceString(sSoapMsgFrame, "<!--" + sCurrentElement + "-->", sPartition,
                        true);
                continue;
            }
            // set WSOperation in ariba object. We check the status after calling web service to determine if call was successful or not
            // assuming that there will be one such element defined. Not expecting multiple values in this field
            if (sSOAPRplElement != null && sCurrentElement.equalsIgnoreCase(sSOAPRplElement))
            {
                sSoapMsgFrame = PSGFunctions.ReplaceString(sSoapMsgFrame, "<!--" + sCurrentElement + "-->",
                        sInernalServiceReply, true);
                continue;
            }

            String sElementValue = "";
            Object oFieldValue = cObj2.getFieldValue(sCurrentElement);
            if (oFieldValue != null && oFieldValue instanceof String)
            {
                sElementValue = (String) oFieldValue;
            }
            else if (oFieldValue != null && oFieldValue instanceof Boolean)
            {
                sElementValue = String.valueOf(((Boolean) oFieldValue).booleanValue());
            }

            //Need to set "-123.45" for Amount fields, when ever they are blank or null to over come Ariba issue.
            //Same work around is used for ITK data load. Refer ExpenditureLimitDPAPostLoad.java for more info.
            /*if((sCurrentElement.equalsIgnoreCase("Expenditure_Limit") || sCurrentElement.equalsIgnoreCase("Delegated_Purchasing_Authority")) && StringUtil.nullOrEmptyOrBlankString(sElementValue))
                
            {
                sElementValue = "-123.45";
            }*/
            
            sSoapMsgFrame = PSGFunctions.ReplaceString(sSoapMsgFrame, "<!--" + sCurrentElement + "-->", "<![CDATA["+sElementValue+"]]>",
                    true);
        }

        return sSoapMsgFrame;
    }

    public static boolean invokeWebservice(String serviceName, String partition, String SOAPRequestMsg,
            String rplElement, String internalServiceReply)
    {
        URL url = null;
        HttpURLConnection conn = null;
        DataOutputStream wr = null;
        boolean isInternalUMDMWSSuccessful = false;
        BufferedReader rd = null;

        Log.customer.debug(sClassName + " inside invokeWebservice");

        try
        {
            Parameters p = Parameters.getInstance();
            String sInstanceName = (String) p.getParameter("System.Base.InstanceName");
            Log.customer.debug(sClassName + " sInstanceName " + sInstanceName);

            String sWSHost = (String) p.getParameter("System.Nodes.buyerserver1.Host");
            Log.customer.debug(sClassName + " sWSHost " + sWSHost);

            AppInfo appInfo = AppInfo.getInstalledAppInfo(sInstanceName);
            String sHostnPort = appInfo.getInternalURL();
            //DEV3 sHostnPort "http://162.70.7.104:8550"

            Log.customer.debug(sClassName + " sHostnPort:" + sHostnPort);

            url = new URL(sHostnPort + "/Buyer/soap/" + partition + "/" + serviceName);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "text/xml");
            conn.setRequestProperty("Content-Length", Integer.toString(SOAPRequestMsg.length()));
            conn.addRequestProperty("SOAPAction", "/Process Definition");
            conn.addRequestProperty("Host", sWSHost);
            conn.addRequestProperty("User-Agent", "Through Ariba PostLoad trigger");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(SOAPRequestMsg);
            wr.flush();
            wr.close();
            conn.connect();
            Log.customer.debug(sClassName + " connected to : " + url);
            Log.customer.debug(sClassName + " conn response code: " + conn.getResponseCode() + ", status message: "
                    + conn.getResponseMessage());
            if (conn.getResponseCode() != 200)
            {
                Log.customer.debug(sClassName + " Exception: Failed : HTTP error code : " + conn.getResponseCode());
                isInternalUMDMWSSuccessful = false;
                return isInternalUMDMWSSuccessful;

            }
            Log.customer.debug(sClassName + " rplElement: " + rplElement);

            if (rplElement != null && !StringUtil.nullOrEmptyOrBlankString(rplElement))
            {
                StringBuilder sStrBuild = new StringBuilder();
                rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String line;
                for (line = rd.readLine(); line != null; line = rd.readLine())
                {
                    // skip SOAP namespace etc.
                    sStrBuild.append(line);
                    Log.customer.debug(sClassName + " line: " + line);

                }
                rd.close();

                String sXMLResponseContent = sStrBuild.toString();
                Log.customer.debug(sClassName + " sXMLResponseContent: " + sXMLResponseContent);

                String sStartRplXMLTag = "<" + rplElement + ">";
                String sEndRplXMLTag = "</" + rplElement + ">";
                int startIndex = sXMLResponseContent.indexOf(sStartRplXMLTag);
                int endElement = sXMLResponseContent.indexOf(sEndRplXMLTag);
                String sStatusValue = sXMLResponseContent.substring(startIndex + sStartRplXMLTag.length(), endElement);
                if (sStatusValue == null)
                {
                    Log.customer.debug(sClassName + " sStatusValue is null");
                }
                else
                {
                    Log.customer.debug(sClassName + " sStatusValue: " + sStatusValue);
                    isInternalUMDMWSSuccessful = (sStatusValue.equalsIgnoreCase(internalServiceReply)) ? true : false;
                }
            }
            else
            {
                Log.customer.debug(sClassName + " mark status as successful");
                isInternalUMDMWSSuccessful = true; // no reply elements to check and status is OK if it reaches here
            }
        }
        catch (MalformedURLException e)
        {
            Log.customer.debug(sClassName + " Malformed URL Exception: " + e.getMessage());
        }
        catch (ProtocolException e)
        {
            Log.customer.debug(sClassName + " Protocol Exception: " + e.getMessage());
        }
        catch (IOException e)
        {
            Log.customer.debug(sClassName + " IO Exception: " + e.getMessage());
        }
        catch (Exception e)
        {
            Log.customer.debug(sClassName + " General Exception: " + e.getMessage());
        }
        finally
        {
            // close the connection, set all objects to null
            conn.disconnect();
            wr = null;
            rd = null;
            conn = null;
        }
        return isInternalUMDMWSSuccessful;
    }
}
