/*
 * @(#)BuysenseXMLReader.java     1.0 07/18/2001
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 * PVCS Modification :
 *
 * $Log:   R:\Software\development\archives\Ariba 8.x Customizations\app\buyer\Server\classes\extensions\config\java\ams\custom\BuysenseXMLReader.java-arc  $
 *
 *    Rev 1.5   23 Feb 2007 17:43:08   iberaha
 * VEPI ST#1797 - Changed getUsers() to getAllUsers() to account for changes in User/Role/Group introduced by 8.1 upgrade.
 *
 *    Rev 1.4   16 Jun 2005 15:43:26   nrao
 * VEPI PROD SPL #729: Incorrect messages on change order when Req in Received/Receiving/Composing. Also fixed logic for multiple change order requests in same session.
 *
 *    Rev 1.3   14 Dec 2004 11:52:10   nrao
 * A8 Dev SPL #159: Log import statistics - transaction count and elapsed time.
 *
 *    Rev 1.2   29 Oct 2004 14:10:28   ahiranan
 * UAT SPL 88 - Remove references in PreviousVersion and NextVersion when deleting requisitions due to errors in import process.
 *
 *    Rev 1.1   01 Mar 2004 10:35:18   jnamada
 * Ariba 8.x Dev SPL #31 - Globally change Log.util.debug to Log.customer.debug
 *
 *    Rev 1.0   21 Jan 2004 15:12:32   cm
 * Initial revision.
 *
 *    Rev 1.11   18 Nov 2002 10:27:16   smokkapa
 * (eVA, Dev SPL #100) - Changed import task to order transactions by Version number
 *
 *    Rev 1.0   27 Sep 2002 13:07:12   cm
 * Initial revision.
 *
 *    Rev 1.11   27 Sep 2002 ahiranan
 * (e2e, Dev SPL # 40) - Changed the logic to get connection info components from hashtable instead of ConnectionInfo.table.
 *    Rev 1.10   25 Jul 2002 18:14:52   iberaha
 * (eVA, Dev SPL # 88) - Changed the logic when dealing with RTRY v ERR transactions to also delete the RTRY ones.  ERR only will send e-mails.  This takes care of an issue of having RTRY transactions being imported into Ariba when force_import = true.
 *
 *    Rev 1.9   12 Jul 2002 14:03:18   iberaha
 * (eVA, Dev SPL # 87) - Enhanced the Error processing, included FORCE_IMPORT logic.
 *
 *    Rev 1.7   May 15 2002 11:22:26   iberaha
 * (eVA, ER # 60) - Paramterized the import table name, and changed the ORDER BY clause to order by PRIORITY first.
 *
 *    Rev 1.6   May 09 2002 16:34:16   iberaha
 * (eVA, ER # 59) - Added error accumulation logic
 *
 *    Rev 1.4   May 06 2002 08:06:38   iberaha
 * (eVA, ER # 57) - Limit the max number of transactions to be imported.  Intorduced the varaible MaxTransactions which utilizes SQL's ROWNUM parameter to retrieve only a certain number of records which meet the criteria.
 *
 *    Rev 1.3   Aug 23 2001 18:12:48   iberaha
 * (e2e, Dev SPL # 14) - Changed the SQL select logic to include selection of records which PROCESS_DATE is lesser or equal to SYSDATE, if present.
 *
 *    Rev 1.2   Aug 07 2001 12:13:46   ghodum
 * Added comments and updated SQL select statement
 *
 *    Rev 1.1   Aug 01 2001 15:51:24   ghodum
 * Cleaned up file, added error recovery
 *
 *    Rev 1.0   Jul 18 2001 12:11:52   smokkapa
 * Initial revision.
 *
 */


/* 09/09/2003: Updates for Ariba 8.0: (Richard Lee) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import java.util.Map;
import java.util.List;
import ariba.base.core.Partition;
import ariba.base.core.ClusterRoot;
// Ariba 8.0: ConnectionInfo isn't being used in this module; also it has moved in Ariba 8.0.
//import ariba.integration.meta.ConnectionInfo;
import ariba.approvable.core.Approvable;
// Ariba 8.1: Permission moved
import ariba.user.core.Permission;
import ariba.util.log.Log;
//import ariba.server.objectserver.core.ServerParameters;
//Ariba 8.0: SimpleScheduleTask has been deprecated
//import ariba.server.objectserver.SimpleScheduledTask;
import java.io.*;
import java.sql.*;
import java.util.Arrays;

import org.apache.log4j.Level;
// Ariba 8.1: need ListUtil
import ariba.util.core.ListUtil;
// Ariba 8.1: need MapUtil
import ariba.util.core.MapUtil;

//81->822 changed Vector to List
//81->822 changed Hashtable to Map
public class BuysenseXMLReader
{
    public final static String IMPORT_DATA_TABLE_DEFAULT = "BUYSENSE_IMPORT_DATA" ;
    public final static int RETRY_DEFAULT = 3 ;
    public final static int MAX_TRANSACTIONS_DEFAULT = 10 ;
    public final static int SEVERITY_DELETE = 3 ;
    public final static int SEVERITY_RETAIN = 2 ;
    public final static int SEVERITY_FREE_RETRY = 1 ;

    // Ariba 8.1: Map constuctor has been deprecated by MapUtil::newTable()
    private Map mhConnInfo = MapUtil.map();
    private String msFailedTransactionPermissions = null ;
    private String msImportDataTable = null ;
    private Connection moConnection = null ;
    // Ariba 8.1: List::List() is deprecated by ListUtil::newVector()
    public  List mvTins = ListUtil.list() ;
    private BuysenseXMLImport moXMLImporter = null ;
    private int miRetryThreshold = RETRY_DEFAULT ;
    private int miMaxTransactions = MAX_TRANSACTIONS_DEFAULT ;
    private int miSeverityDelete = SEVERITY_DELETE ;
    private int miSeverityFreeRetry = SEVERITY_FREE_RETRY ;

    public void setImportDataTable( String fsImportDataTable )
    {
        msImportDataTable = fsImportDataTable ;
    }

    public void setRetryThreshold( int fiRetryThreshold )
    {
        miRetryThreshold = fiRetryThreshold ;
    }

    public void setMaxTransactions( int fiMaxTransactions )
    {
        miMaxTransactions = fiMaxTransactions ;
    }

    public void setFailedTransactionPermission( String fsPermission )
    {
        msFailedTransactionPermissions = fsPermission ;
    }

    public void readXML( Map fhOutsideConnection,
                        Partition foPartition )
    {
        int liRecordCnt = -1 ;

        mhConnInfo  = fhOutsideConnection ;
        moXMLImporter = new BuysenseXMLImport();

        if ( !openTXNConn() )
        {
            Log.customer.debug( "Error: Could not open jdbc connection.  Terminating..." ) ;
            return ;
        }

        if ( ( liRecordCnt = processPendingRecords() ) == -1 )
        {
            Log.customer.debug( "Error while processing pending import records..." ) ;
        }
        else
        {
            // Dev SPL #159: Log import statistics always, not only in debug mode
            Logs.buysense.setLevel(Level.DEBUG);
            Logs.buysense.debug( "Processed " + liRecordCnt + " records." ) ;
            Logs.buysense.setLevel(Level.DEBUG.OFF);
        }

        try
        {
            moConnection.close();
        }
        catch ( Exception loExcep )
        {
            Log.customer.debug( "Error: could not close JDBC connection: " + loExcep.toString() ) ;
        }

        return ;
    }

    private boolean openTXNConn()
    {
        if ( mhConnInfo == null || mhConnInfo.isEmpty() )
        {
            return false ;
        }

        String lsDriver   = (String)mhConnInfo.get( "DBDriver" ) ;
        String lsPassword = (String)mhConnInfo.get( "DBPassword" ) ;
        String lsUser     = (String)mhConnInfo.get( "DBUser" ) ;
        String lsURL      = (String)mhConnInfo.get( "DBURL" ) ;

        Log.customer.debug( "Driver: " + lsDriver ) ;
        Log.customer.debug( "Password: " + lsPassword ) ;
        Log.customer.debug( "User: " + lsUser ) ;
        Log.customer.debug( "URL: " + lsURL ) ;

        try
        {
            Class.forName( lsDriver ) ;
            moConnection = (Connection)DriverManager.getConnection( lsURL,
                                                                   lsUser,
                                                                   lsPassword ) ;
            moConnection.setAutoCommit( false ) ;
            Log.customer.debug( "Established the connection" ) ;
        }
        catch ( Exception loExcep )
        {
            Log.customer.debug( "Error: while opening JDBC connection: " + loExcep.toString() ) ;
            return false ;
        }

        return true ;
    }


    private int processPendingRecords()
    {
        //
        // The following query will return records that need to be processed: NEW and RTRY records.
        // We add the ORDER BY clause to this query so we get the highest priority records
        // with a status of NEW and the oldest creation dates first. This way we give higher priority
        // to the newest records and we process them in the order in which they were inserted into the table
        //
        String lsSQLQuery = "SELECT * FROM ( SELECT TIN, STATUS, ATTEMPTS, IMPORT_DATA FROM " + msImportDataTable + " " +
            "WHERE ( ( STATUS = 'NEW' OR STATUS = 'RTRY' ) " +
            "AND ( ( PROCESS_DATE <= SYSDATE ) OR ( PROCESS_DATE IS NULL ) ) )" +
            "ORDER BY PRIORITY, STATUS, CREATION_DATE ASC, " +
            "ERP_PO_NUMBER, NVL( ERP_PO_VERSION, 1 ) )" +
            "WHERE ROWNUM <= " + miMaxTransactions;

        // Ariba 8.1: List::List() is deprecated by ListUtil::newVector()
        List    loTxnErrVector = ListUtil.list() ;
        ResultSet loResultSet    = null ;
        int       liNumOfRecords = 0 ;

        try
        {
            java.sql.Statement loStatement = moConnection.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
                                                                          ResultSet.CONCUR_UPDATABLE);

            loResultSet = loStatement.executeQuery( lsSQLQuery ) ;

            //This logic will loop through all the records that the ResultSet contains
            while ( loResultSet.next() )
            {
                // Note: we do not use the CLOB API here because the current version
                // of the driver being used does not implement these methods.
                // The getAsciiStream() method seems to work fine, so we use that for now.
// UTF-8 Changes
//              InputStream tempIS = loResultSet.getAsciiStream("IMPORT_DATA");
                Reader tempRDR = loResultSet.getCharacterStream("IMPORT_DATA");
                liNumOfRecords++ ;

                try
                {
// UTF-8 Changes
//                  moXMLImporter.importFromXML(tempIS);
//                  tempIS.close();
                    moXMLImporter.importFromXML(tempRDR);
                    tempRDR.close();

                    //Save the TIN from the record that was read
                    accumulateTins(loResultSet);

                    //Update the INTEGRATORSTATUS to "XMLRead"
                    updateDoneStatus(loResultSet);
                }
                catch (BuysenseXMLImportException ex)
                {
                    ClusterRoot loHeader = null ;
                    String      lsErrorMessage = ex.getMessage() ;

                    Log.customer.debug( "BuysenseXMLImportException: " + lsErrorMessage ) ;

                    // We need to accumulate error message
                    // because this import has failed but the processing
                    // continues, so add this to the ErrorList vector
                    // on BuysenseXMLImport object
                    if (lsErrorMessage != null && lsErrorMessage.startsWith("Info"))
                    {
                       moXMLImporter.addToErrorList( lsErrorMessage, miSeverityFreeRetry ) ;
                    }
                    else
                    {
                       moXMLImporter.addToErrorList( lsErrorMessage ) ;
                    }

                    //delete the approvable here
                    if ( ( loHeader = moXMLImporter.getHeaderObject() ) != null )
                    {
                        Log.customer.debug( "Deleting incomplete req..." ) ;
                        //loHeader.setActive( false ) ;
                        //loHeader.save() ;
                        deleteApprovable((Approvable)loHeader);
                    }
                }
                catch (Exception ex)
                {
                    ClusterRoot loHeader = null ;
                    String      lsErrorMessage = ex.getMessage() ;

                    Log.customer.debug( "Exception: " + lsErrorMessage ) ;

                    if ( updateFailedStatus( loResultSet, lsErrorMessage ) )
                    {
                        // We need to send an e-mail message
                        // because this import has failed without
                        // anymore retries, so add this to the message vector
                        // Ariba 8.1: List::List() is deprecated by ListUtil::newVector()
                        List lvError = ListUtil.list( 2 ) ;
                        // Ariba 8.1: List::addElement() is deprecated by List::add()
                        lvError.add( loResultSet.getString( "TIN" ) ) ;
                        // Ariba 8.1: List::addElement() is deprecated by List::add()
                        lvError.add( lsErrorMessage ) ;

                        // Ariba 8.1: List::addElement() is deprecated by List::add()
                        loTxnErrVector.add( lvError ) ;
                    }

                    //delete the approvable here
                    if ( ( loHeader = moXMLImporter.getHeaderObject() ) != null )
                    {
                        Log.customer.debug( "Deleting incomplete req..." ) ;
                        //loHeader.setActive( false ) ;
                        //loHeader.save() ;
                        deleteApprovable((Approvable)loHeader);
                    }
                }
                // In this section, process all accumulated errors, if any
                finally
                {
                    // Get all errors which were accumulated in ErrorList
                    // The first element (i=0) is Error Severity, and all subsequent
                    // ones are accumulated errors
                    ClusterRoot  loHeader = null ;
                    List       tempVectorOfErrors = moXMLImporter.getErrorList() ;
                    StringBuffer lsbErr = new StringBuffer() ;
                    String       lsSeverity = null ;

                    for ( int i = 0 ; i < tempVectorOfErrors.size() ; i++ )
                    {
                        if ( i == 0 )
                        {
                            // Ariba 8.0: List::elementAt(int) is deprecated by List::get(int)
                            lsSeverity = ( String ) tempVectorOfErrors.get(0) ;
                        }
                        else
                        {
                            // Ariba 8.0: List::elementAt(int) is deprecated by List::get(int)
                            String tempString = ( String ) tempVectorOfErrors.get(i) ;
                            lsbErr.append( tempString ) ;
                            lsbErr.append( ";" ) ;
                        }
                    }

                    // Only delete the approvable if Severity = 3
                    Log.customer.debug( "Severity passed: " + lsSeverity ) ;

                    if ( lsbErr.length() > 0 &&
                            ( new Integer ( lsSeverity ) ).intValue() == miSeverityDelete )
                    {
                        if ( updateFailedStatus( loResultSet,
                                                lsbErr.toString() ) )
                        {
                            Log.customer.debug( "Accumulated Errors : " + lsbErr.toString() ) ;

                            // We need to send an e-mail message
                            // because this import has failed without
                            // anymore retries, so add this to the message vector
                            // Ariba 8.1: List::List() is deprecated by ListUtil::newVector()
                            List lvError = ListUtil.list( 2 ) ;
                            // Ariba 8.1: List::addElement() is deprecated by List::add()
                            lvError.add( loResultSet.getString( "TIN" ) ) ;
                            // Ariba 8.1: List::addElement() is deprecated by List::add()
                            lvError.add( lsbErr.toString() ) ;

                            // Ariba 8.1: List::addElement() is deprecated by List::add()
                            loTxnErrVector.add( lvError ) ;
                        }

                        // delete the approvable here for both 'RTRY' and 'ERR'
                        if ( ( loHeader = moXMLImporter.getHeaderObject() ) != null )
                        {
                            Log.customer.debug( "Deleting incomplete req..." ) ;
                            //loHeader.setActive( false ) ;
                            //loHeader.save() ;
                            deleteApprovable((Approvable)loHeader);
                        }
                    }
                    else if ( lsbErr.length() > 0
                            && ( new Integer ( lsSeverity ).intValue() == miSeverityFreeRetry ) )
                    {
                        // Mark the transaction for retry without incrementing counter
                        updateStatusForFreeRetry( loResultSet, lsbErr.toString() ) ;
                    }
                    else if ( lsbErr.length() > 0
                            && ( new Integer ( lsSeverity ).intValue() != miSeverityDelete ) )
                    {
                        // Mark the transaction as 'DONE'
                        updateDoneStatus( loResultSet, lsbErr.toString() ) ;

                        Log.customer.debug( "Accumulated Errors : " + lsbErr.toString() ) ;

                        // We need to send an e-mail message
                        // because this import has failed without
                        // anymore retries, so add this to the message vector
                        // Ariba 8.1: List::List() is deprecated by ListUtil::newVector()
                        List lvError = ListUtil.list( 2 ) ;
                        // Ariba 8.1: List::addElement() is deprecated by List::add()
                        lvError.add( loResultSet.getString( "TIN" ) ) ;
                        // Ariba 8.1: List::addElement() is deprecated by List::add()
                        lvError.add( lsbErr.toString() ) ;

                        // Ariba 8.1: List::addElement() is deprecated by List::add()
                        loTxnErrVector.add( lvError ) ;
                    }
                }
            }

            if ( loTxnErrVector.size() > 0 )
            {
                // Need to send of an e-mail with failed transactions
                sendMailSummary( loTxnErrVector,
                                msFailedTransactionPermissions ) ;
            }

            loResultSet.close() ;
        }
        catch ( Exception loExcep )
        {
            if ( loResultSet != null )
            {
                try
                {
                    loResultSet.close();
                }
                catch ( SQLException loEx )
                {
                }
            }

            Log.customer.debug( "Error in processing import records: " + loExcep.toString() ) ;
            //temp
            loExcep.printStackTrace();
            //temp
            return -1 ;
        }

        return liNumOfRecords ;
    }

    private void deleteApprovable(Approvable foApprovable)
    {
       if(foApprovable.hasPreviousVersion())
       {
          Approvable loPrevApprovable = (Approvable)foApprovable.getPreviousVersion();
          loPrevApprovable.setNextVersion(null);
          foApprovable.setPreviousVersion(null);
       }

       foApprovable.setActive(false);
       foApprovable.save();
    }

    private void accumulateTins( ResultSet fsResultSet )
    {
        String lsCurrentTin = null ;

        try
        {
            lsCurrentTin = fsResultSet.getString( "TIN" ) ;
            // Ariba 8.1: List::addElement() is deprecated by List::add()
            mvTins.add( lsCurrentTin ) ;
        }
        catch ( SQLException e )
        {
            Log.customer.debug( "Error: Could not get TIN field. Stack: %s",
                           e.getMessage() ) ;
            e.printStackTrace();
        }
    }

    public List getTins()
    {
        return mvTins;
    }

    private void updateDoneStatus( ResultSet rs )
        throws SQLException
    {
        updateDoneStatus( rs, null ) ;
    }

    private void updateDoneStatus( ResultSet rs, String fsErrorMessage )
        throws SQLException
    {
        try
        {
            String lsStmt         = null ;
            String lsErrStmt      = null ;
            String lsErrorMessage = fsErrorMessage ;
            String lsTIN          = rs.getString( "TIN" ) ;
            int    liRetryCnt     = rs.getInt( "ATTEMPTS" ) ;
            Statement selSQL      = moConnection.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
                                                                 ResultSet.CONCUR_UPDATABLE ) ;

            lsStmt = createStatusSQLString( lsTIN, "DONE", ++liRetryCnt ) ;
            Log.customer.debug("SQL Statement : " + lsStmt);
            selSQL.executeUpdate( lsStmt ) ;

            // Capture the Error message being passed, and save it to ERRORS column
            // This will automatically delete any exisiting errors in that column
            if ( lsErrorMessage != null )
            {
                lsErrStmt = createErrorsSQLString ( lsTIN, lsErrorMessage ) ;
            }
            else
            {
                lsErrStmt = createErrorsSQLString ( lsTIN, "" ) ;
            }

            selSQL.executeUpdate( lsErrStmt ) ;
            moConnection.commit() ;
        }
        catch ( SQLException e )
        {
            Log.customer.debug("Error: Could not update STATUS field. Stack: %s",
                           e.getMessage());
            e.printStackTrace();
            throw e ;
        }
    }

    private boolean updateFailedStatus( ResultSet rs, String fsErrorMessage )
    {
        try
        {
            String lsStmt         = null ;
            String lsErrStmt      = null ;
            String lsErrorMessage = fsErrorMessage ;
            String lsTIN          = rs.getString( "TIN" ) ;
            int    liRetryCnt     = rs.getInt( "ATTEMPTS" ) ;
            Statement selSQL      = moConnection.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
                                                                 ResultSet.CONCUR_UPDATABLE ) ;

            if ( ( liRetryCnt + 1 ) < miRetryThreshold )
            {
                lsStmt = createStatusSQLString( lsTIN, "RTRY", ++liRetryCnt ) ;
                selSQL.executeUpdate( lsStmt ) ;
                moConnection.commit() ;
                return false ;
            }
            else
            {
                lsStmt = createStatusSQLString( lsTIN,
                                               "ERR", miRetryThreshold ) ;

                // Capture the Error message being passed, and save it to ERRORS column
                // This will automatically delete any exisiting errors in that column
                if ( lsErrorMessage != null )
                {
                    lsErrStmt = createErrorsSQLString ( lsTIN,
                                                       lsErrorMessage ) ;
                }
                else
                {
                    lsErrStmt = createErrorsSQLString ( lsTIN, "" ) ;
                }

                selSQL.executeUpdate( lsStmt ) ;
                selSQL.executeUpdate( lsErrStmt ) ;
                moConnection.commit() ;
                return true ;
            }
        }
        catch (SQLException e)
        {
            Log.customer.debug("Error: Could not update either STATUS or ERRORS fields to indicate failure. " +
                           "Stack: %s", e.getMessage());
            e.printStackTrace();
            return true ;
        }
    }

    private void updateStatusForFreeRetry ( ResultSet rs, String fsErrorMessage )
    {
        try
        {
            String lsStmt         = null ;
            String lsTIN          = rs.getString( "TIN" ) ;
            int    liRetryCnt     = rs.getInt( "ATTEMPTS" ) ;
            Statement selSQL      = moConnection.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
                                                                 ResultSet.CONCUR_UPDATABLE ) ;

            lsStmt = createStatusSQLString( lsTIN, "RTRY", liRetryCnt ) ;
            selSQL.executeUpdate( lsStmt ) ;
            moConnection.commit() ;
        }
        catch (SQLException e)
        {
            Log.customer.debug("Error: Could not update STATUS for free RETRY. " +
                           "Stack: %s", e.getMessage());
            e.printStackTrace();
        }
    }


    private String createStatusSQLString( String fsTIN,
                                         String fsStatusString,
                                         int fiAttemptCount)
    {
        return ( "UPDATE " + msImportDataTable + " SET STATUS = '" + fsStatusString +
                "', ATTEMPTS = " + fiAttemptCount + ", ERRORS = '' WHERE TIN = '" + fsTIN + "'" ) ;
    }

    private String createErrorsSQLString( String fsTIN,
                                         String fsErrorsString )
    {
        return ( "UPDATE " + msImportDataTable + " SET ERRORS = '" + fsErrorsString +
                "' WHERE TIN = '" + fsTIN + "'" ) ;
    }

    private void sendMailSummary( List foFailedTins,
                                 String fsFailedTXNPermission )
    {
        Permission   loPermisson = null ;
        List       loUsers     = null ;
        String       lsSubject   = null ;
        StringBuffer lsbBody     = new StringBuffer() ;

        if ( ( fsFailedTXNPermission == null ) ||
                ( loPermisson = Permission.getPermission( fsFailedTXNPermission ) ) == null )
        {
            Log.customer.debug( "Could not retrieve permission " + fsFailedTXNPermission + "." ) ;
            return ;
        }

        // Ariba 8.1: Permission::users() is deprecated by Permission::getAllUsers()
        loUsers = loPermisson.getAllUsers() ;

        if ( loUsers == null || loUsers.isEmpty() )
        {
            Log.customer.debug( "No users found that has the " + fsFailedTXNPermission + " permission." ) ;
            return ;
        }

        lsSubject = "Buysense XML import task failed. See e-mail body for details...";

        lsbBody.append( new java.util.Date() ) ;
        lsbBody.append( "\n\n" ) ;
        lsbBody.append( "The following records failed to import:\n" ) ;
        lsbBody.append( "================================================================================\n" ) ;

        for ( int i = 0 ; i < foFailedTins.size() ; i++  )
        {
            // Ariba 8.0: List::elementAt(int) is deprecated by List::get(int)
            List loErrorElement = (List)foFailedTins.get( i ) ;
            String lsRecordId = null ;
            char[] lcFillArray ;

            // Ariba 8.0: List::elementAt(int) is deprecated by List::get(int)
            lsRecordId = (String)loErrorElement.get( 0 ) ;

            // Create extra padding to make everthing fixed width;
            // the max size of the record id is 50
            lcFillArray = new char[ ( 55 - lsRecordId.length() ) ] ;
            Arrays.fill( lcFillArray, ' ' ) ;

            lsbBody.append( lsRecordId ) ;
            lsbBody.append( lcFillArray ) ;

            // Ariba 8.0: List::elementAt(int) is deprecated by List::get(int)
            String lsErrorMsg = (String)loErrorElement.get( 1 ) ;

            if ( lsErrorMsg == null )
            {
                lsbBody.append( "Unknown error." ) ;
            }
            else
            {
                lsbBody.append( lsErrorMsg ) ;
            }

            lsbBody.append( "\n" ) ;
        }

        BuysenseEmailAdapter.sendMail( loUsers,
                                      lsSubject, lsbBody.toString() ) ;
    }
}
