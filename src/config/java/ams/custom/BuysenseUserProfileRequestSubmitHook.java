package config.java.ams.custom;

import java.util.List;

import ariba.admin.core.Log;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableHook;
import ariba.base.core.Base;
import ariba.base.core.BaseVector;
import ariba.util.core.Constants;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;
import ariba.basic.core.Money;
import ariba.user.core.*;

public class BuysenseUserProfileRequestSubmitHook implements ApprovableHook
{
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));
    private static         String        errorStringTable            = "ariba.procure.core";
    private static         String        eVAApplicationValidationError  = "eVAApplicationValidationError";
    
    public List run(Approvable approvable)
    {
    	Log.customer.debug("Inside BuysenseUserProfileRequestSubmitHook "+approvable);
    	
        Boolean lbeMall = (approvable.getFieldValue("eMall") == null)? false:(Boolean) approvable.getFieldValue("eMall");
        Boolean lbOther = (approvable.getFieldValue("Other") == null)? false:(Boolean) approvable.getFieldValue("Other");
        Boolean lbLogiXML = (approvable.getFieldValue("LogiXML") == null)? false:(Boolean) approvable.getFieldValue("LogiXML");
        Boolean lbQuickQuote = (approvable.getFieldValue("QuickQuote") == null)? false:(Boolean) approvable.getFieldValue("QuickQuote");
        Boolean lbVBOBuyer = (approvable.getFieldValue("VBOBuyer") == null)? false:(Boolean) approvable.getFieldValue("VBOBuyer");
        Boolean lbElectronicForms = (approvable.getFieldValue("ElectronicForms") == null)? false:(Boolean) approvable.getFieldValue("ElectronicForms");
 	   	

 	   	
 	   	if((lbeMall == false || lbeMall==null)&&(lbOther == false || lbOther==null )&&(lbLogiXML == false || lbLogiXML==null)&&(lbQuickQuote == false || lbQuickQuote==null)&&(lbVBOBuyer == false || lbVBOBuyer==null)&&(lbElectronicForms == false || lbElectronicForms==null))
 	   	{	
 	   	 String lsRadioButton = (String) approvable.getDottedFieldValue("RadioButtons");
 	   	 
 	   	 if(lsRadioButton.equalsIgnoreCase("Deactivate") || lsRadioButton == null)
 	   	 {
 	   	     return NoErrorResult;
 	   	 }
 	   	 else
 	   	 {
 	   	    Log.customer.debug("Inside BuysenseUserProfileRequestSubmitHook : None of the eVA Applications have been set");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAApplicationValidationError));
 	   	 }
 	   	} 

 	   	return NoErrorResult;
 	   	
 	 }

}

