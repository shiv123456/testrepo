package config.java.ams.custom;

import ariba.base.core.BaseObject;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.util.core.PropertyTable;

/**
 *  Date : 04-June-2009
 *  CER-8: eMall: Unfiltered "On behalf of" field
 *  Description : If the transaction is a POB the On Behalf Of field will not be editable.           
 */

public class BuysenseOnBehalfOfEditability extends Condition 
{
	  private static final ValueInfo parameterInfo[];
	   
	  public boolean evaluate(Object value, PropertyTable params)throws ConditionEvaluationException 
	  {
		   BaseObject obj   = (BaseObject)params.getPropertyForKey("SourceObject");
		   String sTransactionSource=(String) obj.getFieldValue("TransactionSource");
		   if (sTransactionSource!=null)
		   {
			   if ((obj.instanceOf("ariba.purchasing.core.Requisition"))&&(sTransactionSource.startsWith("E2E")))
			   {
				   return false;   
			   }			   
			   else
			   {
				   return true;   
			   }			   
		   }
		   return true;
	  }
	  protected ValueInfo[] getParameterInfo()
	  {
	        return parameterInfo;
	  }
	
	  static
      {
          parameterInfo = (new ValueInfo[] {
             new ValueInfo("SourceObject", 0)
            });
       }
  }