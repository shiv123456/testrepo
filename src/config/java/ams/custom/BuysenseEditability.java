/**
Version :  1.2

Baseline DEV SPL 233 - Added a log message for missing clientName.
                       Took out the NonFatalexception as it puts out a stack trace.

*/



package config.java.ams.custom;


import java.util.List;
import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.core.BaseObject;
import ariba.htmlui.fieldsui.Log;
import ariba.util.core.ListUtil;

//81->822 changed Vector to List
//81->822 changed ConditionValueInfo to ValueInfo
public class BuysenseEditability extends Condition
{
    
    public static final String TargetValue1Param = "TargetValue1";
    public static final String TargetValue2Param = "TargetValue2";
    public static final String TargetValue3Param = "TargetValue3";
    private static final ValueInfo parameterInfo[];
    private static final String requiredParameterNames[] = 
        {
        "TargetValue"
    };
    public boolean evaluate(Object value, PropertyTable params)
    
    {
        Log.editability.debug("Buysense: This is the object we get in the evaluate: %s ", value);
        return testValue(value, params);
    }
    
    protected boolean testValue(Object value, PropertyTable params)
    {
        String clientUniqueName = (String)params.getPropertyForKey("TargetValue");
        String fieldName        = (String)params.getPropertyForKey("TargetValue1");
        BaseObject bo   = (BaseObject)params.getPropertyForKey("TargetValue2");
        String appType          = (String)params.getPropertyForKey("TargetValue3");
        
        //Log.editability.debug("This is the BaseObject from the params: %s",bo);
        //Log.editability.debug("ClientUN: %s fieldName: %s BaseObject:%s",clientUniqueName,fieldName,bo);
        
        //Baseline DEV SPL 233
        if (clientUniqueName == null) 
        {
            Log.customer.info(8888,"BuysenseEditability");
            // Util.assertNonFatal(false,"Did not receive the Client Name");
            return false;
        }
        return BuysenseUIConditions.isFieldEditable(clientUniqueName ,
                                                    fieldName,bo,appType);
    }
    
    public ConditionResult evaluateAndExplain(Object value,
                                              PropertyTable params)
    {
        
        if(!testValue(value, params))
        {
            String msg = params.stringPropertyForKey("Message");
            String subject = subjectForMessages(params);
            if(msg != null)
                return new ConditionResult(Fmt.Si(msg, subject));
            else
                return new ConditionResult(Fmt.Sil("ariba.common.core.condition",
                                                   "EqualToMsg1", subject));
        }
        else
        {
            return null;
        }
    }
    
    private List getTargetValues(PropertyTable params)
    {
        // Ariba 8.1: List constructor is deprecated by ListUtil.newVector() 
        List targetValues = ListUtil.list();
        Object targetValue = params.getPropertyForKey("TargetValue");
        // Ariba 8.1: List::addElement() is deprecated by List::add()
        if(targetValue != null)
            targetValues.add(targetValue);
        targetValue = params.getPropertyForKey("TargetValue1");
        // Ariba 8.1: List::addElement() is deprecated by List::add()
        if(targetValue != null)
            targetValues.add(targetValue);
        targetValue = params.getPropertyForKey("TargetValue2");
        // Ariba 8.1: List::addElement() is deprecated by List::add()
        if(targetValue != null)
            targetValues.add(targetValue);
        targetValue = params.getPropertyForKey("TargetValue3");
        // Ariba 8.1: List::addElement() is deprecated by List::add()
        if(targetValue != null)
            targetValues.add(targetValue);
        return targetValues;
    }
    
    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }
    
    protected String[] getRequiredParameterNames()
    {
        return requiredParameterNames;
    }
    
    public BuysenseEditability()
    {
    }
    
    static
        {
        parameterInfo = (new ValueInfo[] 
                         {
                new ValueInfo("TargetValue",
                                       0),
                    new ValueInfo("TargetValue1",
                                           0),
                    new ValueInfo("TargetValue2",
                                           0),
                    new ValueInfo("TargetValue3",
                                           0),
                    new ValueInfo("Message", 0, Behavior.StringClass)
            }
        );
    }
}
