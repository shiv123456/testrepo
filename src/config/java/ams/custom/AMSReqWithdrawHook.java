/************************************************************************************
 * Author:  Richard Lee
 * Date:    July 30, 2004
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 07/30/2004        Richard Lee            Integration Interface
 * 10/14/2004        Richard Lee            UAT SPL 26 - Errors from PreEncumbrance logic
 *
 * @(#)AMSReqWithdrawHook.java     1.0 08/24/2004
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/
/*
 * rlee, 10/14/2004: UAT SPL 26 and 36 - Errors from PreEncumbrance logic not handling null values from Ariba 7.1
 *
 * RLee Aug 2004: XML Integration Interface
 * This AMSReqWithdrawHook is called when an user such as the Requester wants to withdraw a requisition.
 * If the requisition is being sent to ERP system and waiting for reply, this requisition is not allowed
 * to be withdrawn.
 * If the requisition has been fully PreEncumbranced meaning the ERP system has approved this requisition,
 * then a Cancel will be sent to the ERP system stating that this requisition has been withdrawn.
 *
*/

package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import java.util.List;
import ariba.base.core.*;
import ariba.common.core.*;
import ariba.util.core.*;
import ariba.approvable.core.ApprovableHook;
import ariba.util.log.Log;
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;
import ariba.util.core.StringUtil;
import ariba.util.formatter.BooleanFormatter;

//81->822 changed Vector to List
public class AMSReqWithdrawHook implements ApprovableHook, BuyintConstants
{
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));

    public List run (Approvable approvable)
    {
        Log.customer.debug("Calling AMSReqWithdrawHook.");
        Approvable loReq = approvable;
        String lsPECheck = (String) loReq.getDottedFieldValue("PreEncumbranceStatus");
        Boolean lbPreEncumbered = (Boolean)loReq.getDottedFieldValue("PreEncumbered");
        if( StringUtil.nullOrEmptyOrBlankString(lsPECheck))
        {
            loReq.setFieldValue("PreEncumbranceStatus", STATUS_ERP_READY);
            lsPECheck = STATUS_ERP_READY;
        }
        if(StringUtil.nullOrEmptyOrBlankString(BooleanFormatter.getStringValue(lbPreEncumbered)))
        {
            loReq.setFieldValue("PreEncumbered", new Boolean(false));
            lbPreEncumbered = new Boolean(false);
        }
        if((lsPECheck.equals(STATUS_ERP_APPROVE))&& lbPreEncumbered.booleanValue())
        {
            // Call Simha BuyintXMLFactory.push (req, cancel);
            BuyintXMLFactory.submit(TXNTYPE_PREENC_CANCEL, loReq);
            BaseObject loReqObject = (BaseObject) loReq;
            String lsReqUN = (String) loReq.getDottedFieldValue("UniqueName");
            String lsRecordType = "PreEncCRecord";
            String lsMessage = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","PreEncWithdrawMessage");
            Partition loPartition = loReq.getPartition();
            User loAribaSystemUser = Core.getService().getAribaSystemUser(loPartition);
            // Update History tab
            BuysenseUtil.createHistory(loReqObject, lsReqUN,  lsRecordType, lsMessage, loAribaSystemUser);
            loReq.setDottedFieldValue("PreEncumbranceStatus", STATUS_ERP_CANCELLED);
            loReq.setDottedFieldValue("PreEncumbered", new Boolean(false));
            return NoErrorResult;
        }
        else if (lsPECheck.equals(STATUS_ERP_INPROGRESS))
        {
            // waiting for ERP system's reply; this Req cannot be withdraw
            Log.customer.debug("ReqIntTxn inprogress; do not allow withdraw. " );
            return ListUtil.list(Constants.getInteger(-1),
                                     "This Requisition is pending ERP response therefore cannot be withdrawn at this time.");

        }
        else
        {
            // user Withdraw Req
            return NoErrorResult;
        }

    }
}
