package config.java.ams.custom;

import java.util.List;

import ariba.approvable.core.Approvable;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.ClusterRoot;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionResult;
import ariba.base.fields.ValueInfo;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.util.core.Date;
import ariba.util.core.Fmt;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

/** 
* @author Manoj Gaur
* @version 1 DEV
* @reference CSPL-593
* @see Date 08-12-2008
* @Explanation Getting fields from different groups and nullify those objects if they have ERPValue is n/a.
* */

public class BSOActiveClusterRoot extends Condition
{

    public boolean evaluate(Object value, PropertyTable params)
    {
        return evaluateImpl(value, params);
    }

    private final boolean evaluateImpl(Object value, PropertyTable params)
    {
        if(value == null)
        {
            return true;
        } else
        {
            ClusterRoot cr = (value instanceof BaseId) ? Base.getSession().objectFromId((BaseId)value) : (ClusterRoot)value;
            return cr.getActive()? true: isBSOValid(cr);
        }
    }

    public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
    {
        if(!evaluateImpl(value, params))
            return new ConditionResult(Fmt.Sil("ariba.common.core.condition", "ActiveClusterRootMsg1", subjectForMessages(params)));
        else
            return null;
    }

    public boolean isBSOValid(ClusterRoot cr)
	{
		ClusterRoot objBuysenseFDT = cr;
		String sERPVal = (String)objBuysenseFDT.getDottedFieldValue("FieldTable.ERPValue");
		return sERPVal.equals("n/a");		
	}
    
    /**
     * CSPL-593 (Manoj) 10/03/08
     * @param appr
     * @explanation performing field validation for each BSO fields 
     */
    public void performFieldValidation(Approvable appr)
	{
		Requisition objRequisition = null;
		if (appr != null && appr instanceof Requisition)
			objRequisition = (Requisition) appr;				
		if(objRequisition != null)
		{
			Log.customer.debug(" Req Object: %s",objRequisition);
			nullifyObj("HeaderDetailsEditable",FieldListContainer.getHeaderFieldValues(),objRequisition);
			nullifyObj("LineItemGeneralFields",FieldListContainer.getLineFieldValues(),objRequisition);
			nullifyObj("ProcureAccountingFields",FieldListContainer.getAcctgFieldValues(),objRequisition);
		}
	}
	
    /**
     * CSPL-593 (Manoj) 1/03/08
     * @param Group
     * @param fields
     * @param objAppr
     */
	public void nullifyObj(String Group,List fields,Approvable objAppr)
	{
		Log.customer.debug("Entered in nullifyObj method for "+Group+" and Approvable "+objAppr);
		if(objAppr!=null && objAppr instanceof Requisition)
		{	
			Requisition objReq = (Requisition) objAppr;
			List fieldNames = fields;
			
			String sFieldName;
			String sFullFieldName="";		
			
			for (int i = 0; i < fieldNames.size(); i++)
			{
				sFieldName = (String) fieldNames.get(i);
				if(Group.equals("LineItemGeneralFields"))
				{
					List lineItems = objReq.getLineItems();	
					for(int j=0; j<lineItems.size();j++)
					{
						sFullFieldName = Fmt.S("%s.%s","LineItems["+new Integer(j).toString()+"]",sFieldName);						
						setNull(objAppr,sFullFieldName);
					}					
				}
				else if (Group.equals("ProcureAccountingFields"))
				{
					List lineItems = objReq.getLineItems();	
					for(int j=0; j<lineItems.size();j++)
					{
						ReqLineItem objReqLineItem = (ReqLineItem)lineItems.get(j);
						List accountings = objReqLineItem.getAccountings().getAllSplitAccountings();
						for(int k=0; k<accountings.size();k++)
						{
							sFullFieldName = Fmt.S("%s.%s.%s","LineItems["+new Integer(j).toString()+"]","Accountings.SplitAccountings["+new Integer(k).toString()+"]",sFieldName);							
							setNull(objAppr,sFullFieldName);								
						}
					}
				}
				else 
				{
					sFullFieldName = sFieldName;
					setNull(objAppr,sFullFieldName);					
				}				
			}		
		}
		else
		{
			Log.customer.debug("Object is not instance of Requisition");
		}
	}	
	
	/**
	 * CSPL-593 (Manoj) 1/03/08
	 * @param appr
	 * @param fieldName
	 * @explanation setting null for fieldName as per condition ERPValue has n/a
	 */
	public static void setNull(Approvable appr,String fieldName)
	{
		try
		{
			Object obj = appr!=null? appr.getDottedFieldValue(fieldName) : null;
			ClusterRoot objBuysenseFDT;
			if(obj instanceof ClusterRoot)
			{
				objBuysenseFDT = (ClusterRoot) obj;
				String sERPVal = (String)objBuysenseFDT.getDottedFieldValue("FieldTable.ERPValue");
				if(sERPVal!= null && sERPVal.equals("n/a"))
				{
					appr.setDottedFieldValue(fieldName, null);
					Log.customer.debug(" For %s has set null");
				}
			}
		}
		catch(Exception e)
		{
			Log.customer.debug(" Exception from DeleteBSOReqObject.setNull()");
			e.printStackTrace();
		}
	}

    protected ValueInfo getValueInfo()
    {
        return valueInfo;
    }

    public BSOActiveClusterRoot()
    {
    }

    private static final ValueInfo valueInfo = new ValueInfo(0, "ariba.base.core.ClusterRoot");

}