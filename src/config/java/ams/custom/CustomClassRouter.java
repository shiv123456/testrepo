/*
   Anup - New class for routing client specific computations/validations.
   Feb 2000
*/

/* 09/03/2003: Updates for Ariba 8.0 (David Chamberlain)
   Replaced all Util.integer()'s with Constants.getInteger()
   Replaced all Util.vector()'s with ListUtil.vector()
   Replaced all Util.getInteger()'s with Constants.getInteger()
*/
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.BaseObject;
import ariba.base.core.Base;
import java.util.List;
// Ariba 8.0: Replaced deprecated method
import ariba.util.core.Assert;
//import ariba.util.core.Util;
import java.lang.reflect.*;
import ariba.util.log.Log;
// Ariba 8.0: Deprecations, methods have changed packages.
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;

//81->822 changed Vector to List
public class CustomClassRouter
{
    // Ariba 8.0: Replaced deprecated method
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));
    private static final Boolean NoError = new Boolean(true);
    
    public static List routeRequest(BaseObject obj, String param) 
    {
        String customClass = Base.getService().getParameter(Base.getSession().getPartition(), param);
        Log.customer.debug("Hello washington - in Router, param is --> %s", param);
        Log.customer.debug("Hello washington - in Router, obj is --> %s", obj);
        
        if (!(customClass == null) && !(customClass.trim().equals("")))
        {
            try
            {
                Class actionClass = Class.forName(customClass);
                Object instance = actionClass.newInstance();
                Object[] args = 
                    {
                    obj
                };
                Method theMethod = actionClass.getMethod("execute", new Class[] 
                                                         {
                        ariba.base.core.BaseObject.class
                    }
                );
                Object retVal = theMethod.invoke(instance, args);
                return (List)retVal;
            }
            catch(ClassNotFoundException cnfe)
            {
                Log.customer.debug("In CustomClassRouter, Class " + customClass +  " not found.");
                // Ariba 8.0: Replaced deprecated method
                return ListUtil.list(Constants.getInteger(-1),
                                         "In CustomClassRouter, Class " + customClass +  " not found.");
            }
            catch(InstantiationException ie)
            {
                // Ariba 8.0: Replaced deprecated method
                Assert.assertNonFatal(false,
                                      "In CustomClassRouter, Class " + customClass +  " could not be instantiated.");
                // Ariba 8.0: Replaced deprecated method
                return ListUtil.list(Constants.getInteger(-1),
                                         "In CustomClassRouter, Class " + customClass +  " could not be instantiated.");
            }
            catch(NoSuchMethodException nsme)
            {
                // Ariba 8.0: Replaced deprecated method
                Assert.assertNonFatal(false,
                                      "In CustomClassRouter, execute method not found.");
                // Ariba 8.0: Replaced deprecated method
                return ListUtil.list(Constants.getInteger(-1),
                                         "In CustomClassRouter, execute method not found.");
            }
            catch(IllegalAccessException iae)
            {
                // Ariba 8.0: Replaced deprecated method
                Assert.assertNonFatal(false,
                                      "In CustomClassRouter, access to class " + customClass +  " not permitted.");
                return ListUtil.list(Constants.getInteger(-1),
                                         "In CustomClassRouter, access to class " + customClass +  " not permitted.");
            }
            catch(InvocationTargetException ite)
            {
                // Ariba 8.0: Replaced deprecated method
                Assert.assertNonFatal(false,
                                      "In CustomClassRouter, exception when invoking execute method.");
                return ListUtil.list(Constants.getInteger(-1),
                                         "In CustomClassRouter, exception when invoking execute method.");
            }
            
        }
        Log.customer.debug("CustomClassRouter did not find value for " + param + " in Parameters.table");   
        return NoErrorResult;
        
    }
    
    public static List routeHookRequest(Approvable obj,
                                          String className, String method) 
    {
        
        try
        {
            Class actionClass = Class.forName(className);
            Object instance = actionClass.newInstance();
            Object[] args = 
                {
                obj
            };
            Method theMethod = actionClass.getMethod(method, new Class[] 
                                                     {
                    ariba.approvable.core.Approvable.class
                }
            );
            Object retVal = theMethod.invoke(instance, args);
            return (List)retVal;
        }
        catch(ClassNotFoundException cnfe)
        {
            Log.customer.debug("In CustomClassRouter, Class " + className +  " not found.");
            return NoErrorResult;
        }
        catch(InstantiationException ie)
        {
            Log.customer.debug("In CustomClassRouter, Class " + className +  " could not be instantiated.");
            return NoErrorResult;
        }
        catch(NoSuchMethodException nsme)
        {
            Log.customer.debug("In CustomClassRouter, " + method + " method not found.");
            return NoErrorResult;
        }
        catch(IllegalAccessException iae)
        {
            Log.customer.debug("In CustomClassRouter, access to class " + className +  " not permitted.");
            return NoErrorResult;
        }
        catch(InvocationTargetException ite)
        {
            Log.customer.debug("In CustomClassRouter, exception when invoking " + method + " method.");
            return NoErrorResult;
        }
        
    }
    
    public static Boolean routeStatusRequest(Approvable obj,
                                             String className, String method) 
    {
        
        try
        {
            Class actionClass = Class.forName(className);
            Object instance = actionClass.newInstance();
            Object[] args = 
                {
                obj
            };
            Method theMethod = actionClass.getMethod(method, new Class[] 
                                                     {
                    ariba.approvable.core.Approvable.class
                }
            );
            Object retVal = theMethod.invoke(instance, args);
            return (Boolean)retVal;
        }
        catch(ClassNotFoundException cnfe)
        {
            Log.customer.debug("In CustomClassRouter, Class " + className +  " not found.");
            return NoError;
        }
        catch(InstantiationException ie)
        {
            Log.customer.debug("In CustomClassRouter, Class " + className +  " could not be instantiated.");
            return NoError;
        }
        catch(NoSuchMethodException nsme)
        {
            Log.customer.debug("In CustomClassRouter, " + method + " method not found.");
            return NoError;
        }
        catch(IllegalAccessException iae)
        {
            Log.customer.debug("In CustomClassRouter, access to class " + className +  " not permitted.");
            return NoError;
        }
        catch(InvocationTargetException ite)
        {
            Log.customer.debug("In CustomClassRouter, exception when invoking " + method + " method.");
            return NoError;
        }
        
    }
    
}

