package config.java.ams.custom;

import ariba.base.core.BaseObject;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.purchasing.core.Requisition;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

/**
 * CSPL # : 5525 'Confirming Order: Do not Duplicate' (BuysenseOrderConfirm checkbox) should not be editable in submitted status or beyond that
 * 
 * @author : Jackie Bhadange Date : 18-SEP-2013
 * @version : 1.0
 * 
 * Explanation: 'Confirming Order: Do not Duplicate' (BuysenseOrderConfirm checkbox) should not be editable after submission of a Requsisiton.
 */

public class BuysenseOrderConfirmEditability extends Condition
{

    private static final ValueInfo parameterInfo[];

    public boolean evaluate(Object obj, PropertyTable propertytable) throws ConditionEvaluationException
    {
        Log.customer.debug("inside  BuysenseOrderConfirmEditability SourceObject" + obj);
        BaseObject baseobj = (BaseObject) propertytable.getPropertyForKey("SourceObject");

        if (baseobj == null || baseobj instanceof Requisition)
        {
            Requisition req = (Requisition) baseobj;
            String status = (String) req.getStatusString();

            String uniquename = (String) req.getUniqueName();
            Log.customer.debug("inside  BuysenseOrderConfirmEditability returning name: " + uniquename + " status: " + status);

            if (status != null && (status.equals("Submitted") || status.equals("Composing")))
            {
                Log.customer.debug("inside  BuysenseOrderConfirmEditability returning true");
                return true;
            }
        }
        Log.customer.debug("inside  BuysenseOrderConfirmEditability returning false");

        return false;
    }

    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    static
    {
        parameterInfo = (new ValueInfo[] { new ValueInfo("SourceObject", 0) });
    }
}
