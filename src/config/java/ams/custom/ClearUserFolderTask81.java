package config.java.ams.custom;

import ariba.approvable.core.Folder;
import ariba.approvable.core.FolderItem;
import ariba.base.core.*;
import ariba.user.core.User;
import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.util.scheduler.*;
import java.util.*;
import java.io.*;

public class ClearUserFolderTask81 extends ScheduledTask 
{

    private String UserNamesArgument;
    private String msPasswordAdapterName = null;
    private String msFolderName = null;
    private String msUserList = null;
    private String msHowManyToClear = null;
    private String msParamFile = null;
    private String ALL = "ALL";
    private int liNumToClear = 0;
    private int liFolderItemsSize = 0;
    private List llFolderItems = null;
    private List llUserList = null;
    private FolderItem loItem = null;
    private BaseId loBaseID = null;
    private Folder loFolders = null;
    private Folder loEachFolder = null;

    public ClearUserFolderTask81() 
    {
        UserNamesArgument = null;
    }

    // CSPL-715 - Hashtable replaced by Map
    //public void init(Scheduler scheduler, String scheduledTaskName, Hashtable parameters) 
    public void init(Scheduler scheduler, String scheduledTaskName, Map parameters) 
    {
        super.init(scheduler, scheduledTaskName, parameters);
        for(Iterator e = parameters.keySet().iterator(); e.hasNext();) 
        {
            String key = (String)e.next();
            if(key.equals("ParamFile"))
            {
               msParamFile = (String) parameters.get(key);
            }
        }
    }

    public void run() throws ScheduledTaskException 
    {
        User user = null;
        try
        {
            Log.customer.debug("Calling ClearUsersFolderTask");
            if (!loadAndValidateParams())
            {
                Logs.buysense.debug("The parameters in " + msParamFile + " are not valid." );
                return ;
            }
            llUserList = getIdentifiersFromArgument(msUserList);
              Log.customer.debug("UserList ="+ llUserList);
            int liNumUsers = llUserList.size();
              Log.customer.debug("Number of Users: " + liNumUsers);

            for(int i = 0; i < liNumUsers; i++) 
            {
                String lsUserName = (String)llUserList.get(i);
                  Log.customer.debug(" UserName = "+ lsUserName);
                user = User.getUser(lsUserName, msPasswordAdapterName);

                if(user == null) {
                  Log.customer.debug("Unable to find the user for the object :%s", lsUserName);
                } 
                else 
                {
                   ClearOutArchiveFolder(user);
                }
            }
        }
        catch ( Exception loExcep )
        {
            Log.customer.debug( "Error: while Clearing User Folder." ) ;
        }
    }

    public void ClearOutArchiveFolder(User user) 
    {
        loFolders = Folder.getFolders(user);
        // CSPL-715 81->822 changes
        //Enumeration enumloFolders = (Enumeration)loFolders.getFoldersIterator();
        Iterator enumloFolders = (Iterator)loFolders.getFoldersIterator();

        // CSPL-715 81->822 changes
        //while(enumloFolders.hasMoreElements())  
        while(enumloFolders.hasNext())  
        {
            // CSPL-715 81->822 changes
            //loEachFolder = (Folder) Base.getSession().objectFromId((BaseId)enumloFolders.nextElement());
            loEachFolder = (Folder) Base.getSession().objectFromId((BaseId)enumloFolders.next());
            Log.customer.debug("Folder Name = " + loEachFolder.getFolderName());

            // Clear only requested Folder stated in ClearUserFolder81.props file
            if(loEachFolder.rawFolderName().equals(msFolderName)) 
            {
                Log.customer.debug("clearing out Folder = " + msFolderName);

                llFolderItems = loEachFolder.getItems();
                liFolderItemsSize = llFolderItems.size();
                  Log.customer.debug("msHowManyToClear = " + msHowManyToClear);
 
                if(liFolderItemsSize > 0 && (!StringUtil.nullOrEmptyOrBlankString(msHowManyToClear)))
                {
                    if(msHowManyToClear.trim().equalsIgnoreCase(ALL))
                    {
                          Log.customer.debug("Removing all FolderItems = " + llFolderItems);
                        llFolderItems.clear();
                    }
                    else
                    {
                        liNumToClear = (Integer.valueOf(msHowManyToClear)).intValue();
                        if(liNumToClear > liFolderItemsSize)
                        { 
                            liNumToClear = liFolderItemsSize;
                        }
                        Log.customer.debug("liNumToClear: " + liNumToClear);
                        int liClearIndex = liNumToClear - 1;

                        for(int i = 0; i < liNumToClear; i++) 
                        {
                              Log.customer.debug("liClearIndex: " + liClearIndex);
                            loBaseID = (BaseId)llFolderItems.get(liClearIndex);

                            loItem = (FolderItem)loBaseID.get();
                              Log.customer.debug("delete loItem: " + loItem);
                            loItem.delete();
                            llFolderItems.remove(liClearIndex);
                            liClearIndex--;
                        }
                    }
                    Base.getSession().transactionCommit();
                }
                    liFolderItemsSize = llFolderItems.size();
                      Log.customer.debug("Remaining FolderItemsSize: " + liFolderItemsSize);
            }
        }
    }

    private List getIdentifiersFromArgument(String argument) 
    {
        Log.customer.debug("argument = "+argument);

        List identifiers = ListUtil.list();
        if(!StringUtil.nullOrEmptyOrBlankString(argument)) 
        {
            int end = 0;
            int begin = 0;
            String identifier = null;
            do {
                end = argument.indexOf(",", begin);
                if(end != -1) {
                    identifier = argument.substring(begin, end);
                    begin = end + 1;
                } else {
                    identifier = argument.substring(begin, argument.length());
                }
                identifier = identifier.trim();
                if(!StringUtil.nullOrEmptyOrBlankString(identifier))
                    identifiers.add(identifier);
            } while(end != -1 && !StringUtil.nullOrEmptyOrBlankString(identifier));
        }
        return identifiers;
    }

    private boolean loadAndValidateParams()
    {
        Properties loProperties = new Properties();
        FileInputStream loFileInputStream = null;
        try
        {
            Log.customer.debug("Props File is in " + msParamFile);
            loFileInputStream = new FileInputStream(msParamFile);
            loProperties.load(loFileInputStream);
        }
        catch(IOException loIOE)
        {
            Log.customer.debug("ERROR: Problem reading properties file ...");
        }
        msFolderName = loProperties.getProperty("FolderName");
        msUserList = loProperties.getProperty("UserList");
        msHowManyToClear = loProperties.getProperty("HowManyRecordToDelete");
        // CSPL-715 - Replace how the password adapter is retrieved
        //msPasswordAdapterName = loProperties.getProperty("PasswordAdapterName");
        Partition loPartition = Base.getService().getPartition("pcsv");
        msPasswordAdapterName = Base.getService().getParameter(loPartition, "Application.Authentication.PasswordAdapter");
        return true;
    }
}
