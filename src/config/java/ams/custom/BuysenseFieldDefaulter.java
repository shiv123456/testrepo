/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.base.fields.Action;
import ariba.base.fields.ValueInfo;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import java.util.List;
// Ariba 8.1: Various methods have changed from the java.util.List package to the ariba.util.core.ListUtil package.
import ariba.util.core.ListUtil;
import ariba.base.core.ClusterRoot;
import ariba.base.core.BaseObject;
import ariba.util.log.Log;
import ariba.base.core.Base;
import ariba.base.fields.FieldProperties;
// Ariba 8.1: Use Iterators instead of Enumerations
import java.util.Iterator;

/**
    An action that updates the dates of the entries, based on
    the time card's EndingDate.
*/
//81->822 changed Vector to List
//81->822 changed ConditionValueInfo to ValueInfo
public class BuysenseFieldDefaulter extends Action
{
    static final String Value = "Value";
    private static final ValueInfo[] parameterInfo = 
        {
        new ValueInfo(TargetParam,
                               true,
                               IsScalar, "ariba.core.BuysenseFieldDataTable")
    };
    
    private static final String[] requiredParameterNames = 
        {
        TargetParam 
    };
    
    // allowed types for the value
    private static final ValueInfo valueInfo =
        new ValueInfo(IsScalar, "ariba.core.BuysenseOrgEform");
    
    /**
        Executes the given action.

        @param object The object receiving the action.
        @param params Parameters used to perform the action.
    */
    public void fire (ValueSource object, PropertyTable params)
    {
        //String day = params.stringPropertyForKey(TargetParam);
        
        String targetFieldName;
        String targetFieldValueName;
        String label=null;
        Log.customer.debug("Custom Action in BuysenseFieldDefaulter is called");
        BaseObject bo = (BaseObject)object;
        FieldProperties fp;
        
        Log.customer.debug("This is the object passed in: %s ", object);
        
               /*
                      //test set label
                       Log.customer.debug("Field property test 1");
                       BaseObject bo = (BaseObject)cr;
                      FieldProperties cp = bo.getFieldProperties("FieldDefault1");

                      String templabel ="IsmailTest";
                        Log.customer.debug("Field property test 2");
                      cp.setPropertyForKey("Label",templabel);

              End test  */
        
        // Ariba 8.1: Replaced deprecated new List() Constructor with ListUtil.newVector();
        // The following 2 lines were modified accordingly.
        List fieldsVector = ListUtil.list();
        List fieldValuesVector = ListUtil.list();
        ClusterRoot client = (ClusterRoot) bo.getFieldValue("ClientName");
        
        if (client == null) return;
        
        String clientUniqueName = (String) client.getFieldValue("UniqueName");
        
        //** Get the list of fields, fieldValues from the FieldList container */
        
        List actgFieldsVector = FieldListContainer.getAcctgFields();
        List actgFieldValuesVector = FieldListContainer. getAcctgFieldValues();
        List headerFieldsVector = FieldListContainer. getHeaderFields();
        List headerFieldValuesVector = FieldListContainer.getHeaderFieldValues();
        List lineFieldsVector = FieldListContainer. getLineFields();
        List lineFieldValuesVector = FieldListContainer.getLineFieldValues();
        
        
        
        /** Put all the fields in one vector */
        // Ariba 8.1: Replaced deprecated addElement() method with add() method.
        // The following 5 lines were modified accordingly.
        fieldsVector.add(actgFieldsVector);
        fieldsVector.add(headerFieldsVector);
        fieldsVector.add(lineFieldsVector);
        fieldsVector.add(FieldListContainer.getPaymentHeaderFields());
        fieldsVector.add(FieldListContainer.getPaymentLineFields());
        
        /** Put all the field values in one vector */
        // Ariba 8.1: Replaced deprecated addElement() method with add() method.
        // The following 5 lines were modified accordingly.
        fieldValuesVector.add(actgFieldValuesVector);
        fieldValuesVector.add(headerFieldValuesVector);
        fieldValuesVector.add(lineFieldValuesVector);
        fieldValuesVector.add(FieldListContainer.getPaymentHeaderFieldValues());
        fieldValuesVector.add(FieldListContainer.getPaymentLineFieldValues());
        
        // fieldsVector = FieldListContainer.getTestFields();
        //fieldValuesVector = FieldListContainer. getTestFieldValues();
        
        Log.customer.debug("The fields vector has %s elements",
                       fieldsVector.size());
        Log.customer.debug("The field values vector has %s elements",
                       fieldValuesVector.size());
        
        int vectorSize = fieldsVector.size();
        for(int i=0;i<vectorSize;i++) 
        {
            
            label = null;
            //String targetFieldName = "Field1" ;
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            targetFieldName = (String)fieldsVector.get(i);
            String UniqueName = (clientUniqueName+":"+targetFieldName);
            Log.customer.debug("client.uniquename = %s",UniqueName);
            ClusterRoot fieldTable = Base.getService().objectMatchingUniqueName("ariba.core.BuysenseFieldTable", client.getPartition(), UniqueName);
            
            if(fieldTable != null)
            {
                label = (String)fieldTable.getFieldValue("ERPValue");
                Log.customer.debug("This is the label: %s",label);
            }
            if (label != null) 
            {
                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                fp = bo.getFieldProperties((String)fieldValuesVector.get(i));
                //im - setting all the field properties to true before hiding the 
                //selected fields
                fp.setPropertyForKey("Hidden",new Boolean(false));
                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                Log.customer.debug("Seting %s to %s ",
                               (String)fieldValuesVector.get(i),label);
                fp.setPropertyForKey("Label",label);
                //We want to hide if the field name is n/a
                if (label.equals("n/a")) 
                {
                    // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                    Log.customer.debug("Hit n/a section for this field %s",
                                   (String)fieldValuesVector.get(i));
                    fp.setPropertyForKey("Hidden",new Boolean(true));
                }
                
            }
            
        }
        
        
        //Set all the FieldDefaultValues to null
        
        // Ariba 8.1: List::elements() is deprecated by AbstractList::iterator().
        //for(Enumeration e = fieldValuesVector.elements();e.hasMoreElements();) 
        for(Iterator e = fieldValuesVector.iterator();e.hasNext();) 
        {
            targetFieldValueName = (String)e.next();
            bo.setDottedFieldValue(targetFieldValueName,null);
        }
        
        List ApproversAmountsVector = FieldListContainer.getApproversAmounts();
        
        // Ariba 8.1: List::elements() is deprecated by AbstractList::iterator().
        //for(Enumeration e = fieldValuesVector.elements();e.hasMoreElements();) 
        for(Iterator e = fieldValuesVector.iterator();e.hasNext();) 
        {
            bo.setDottedFieldValue((String)e.next(),null);
        }
        
        
    }
    
    /**
        Return the list of valid value types.
    */
    protected ValueInfo getValueInfo ()
    {
        return valueInfo;
    }
    
    /**
        Return the parameter names.
    */
    protected ValueInfo[] getParameterInfo ()
    {
        return parameterInfo;
    }
    
    /**
        Return the required parameter names.
    */
    protected String[] getRequiredParameterNames ()
    {
        return requiredParameterNames;
    }
}
