package config.java.ams.custom;

//import ariba.basic.util.Log;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ariba.util.log.Log;
import ariba.approvable.core.Approvable;
import ariba.user.core.Approver;
import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.fields.*;
import ariba.basic.core.Money;
import ariba.util.core.*;

//81->822 changed ConditionValueInfo to ValueInfo
public class BuysenseUserProfileFieldValidation extends Condition
{
    private static final ValueInfo parameterInfo[];
    String msMessage = null;
    private static         String        errorStringTable        = "ariba.procure.core";
    private static         String        phonenumberValidationError   = "PhoneNumberValidationError";
    private static         String        faxnumberValidationError   = "FaxNumberValidationError";
    private static         String        expApproverValidationError  = "ExpApproverValidationError";
    private static         String        aribaStandardRoles          = "StandardRoleValidationError";
    private static         String        patternToMatch          = "PatternToCheck1";
    List llaribaStandardRoles  = ListUtil.list();

    public ConditionResult evaluateAndExplain(Object object, PropertyTable params)
    {
        Log.customer.debug("Calling BuysenseUserProfileFieldValidation for new User Profile Request Eform.");
        if(!evaluate(object, params))
        {
            String retMsg = null;
            if(!StringUtil.nullOrEmptyOrBlankString(msMessage))
            {
                retMsg = msMessage;
                msMessage = null;
                return new ConditionResult(retMsg);
            }
            else
                return null;
        }
        return null;
    }

    public boolean evaluate(Object object, PropertyTable params)
    {
		BaseObject baseobj  = (BaseObject)params.getPropertyForKey("SourceObject");	
		String lsFieldName    = (String)params.getPropertyForKey("TargetValue1");
		Log.customer.debug("Inside BuysenseUserProfileFieldValidation "+baseobj);
		Log.customer.debug("Inside BuysenseUserProfileFieldValidation : the field name is"+lsFieldName);
		
		if(((Approvable)baseobj).instanceOf("ariba.core.BuysenseUserProfileRequest"))
		{
     		Log.customer.debug("Inside BuysenseUserProfileFieldValidation : The value to be validated is Field:"+lsFieldName);
     		return validateFields(lsFieldName,baseobj);
		}
		else
			return true;
    }

    protected boolean validateFields(String lsFieldName,BaseObject baseobj)
    {
    	Boolean lbUserInput = false;
    	String lsUserInput = null;
    	    if(lsFieldName.equalsIgnoreCase("AribaExpenditureLimitApprover"))
			{
				Log.customer.debug("Inside BuysenseUserProfileFieldValidation:field being checked now is"+lsFieldName);
				Money lmAribaExpenditureLimitAmt = (Money)baseobj.getFieldValue("AribaExpenditureLimitAmt");
				Approver laAribaExpenditureLimitApprover = (Approver)baseobj.getFieldValue(lsFieldName);
				if(lmAribaExpenditureLimitAmt != null)
				{
					if(laAribaExpenditureLimitApprover != null)
					{
						lbUserInput = true;
 		}
		else 
		{
						msMessage = Fmt.Sil(Base.getSession().getLocale(),errorStringTable, expApproverValidationError);
						lbUserInput = false;
					}
				}
				else
				{
					lbUserInput = true;
				}
			}
    	    else  if(lsFieldName.equalsIgnoreCase("AribaStandardRoles"))
			{
				Log.customer.debug("Inside BuysenseUserProfileFieldValidation:field being checked now is"+lsFieldName);
				llaribaStandardRoles = (List)baseobj.getFieldValue(lsFieldName);
				if(llaribaStandardRoles.isEmpty())
				{
					Log.customer.debug("Inside BuysenseUserProfileFieldValidation: Ariba Standard Roles is not populated");
					msMessage = Fmt.Sil(Base.getSession().getLocale(),errorStringTable, aribaStandardRoles);
					lbUserInput = false;
			}
			else
			{
					Log.customer.debug("Inside BuysenseUserProfileFieldValidation: Ariba Standard Roles is populated");
					lbUserInput = true;
				}
			}
			else
			{
				lsUserInput = (String)baseobj.getFieldValue(lsFieldName);
				lbUserInput = validateNumber(lsUserInput,lsFieldName);
			}
			
			if(lbUserInput)
			{
        		return true;
			}
			else
			{
				return false;
			}

    }
    
    protected boolean validateNumber(String lsUserInput,String lsFieldName)
    {
		String lsPattern = ResourceService.getString(errorStringTable, patternToMatch);
	    Pattern pattern = Pattern.compile(lsPattern);
	    Boolean lbreturnvalue = false;
	    if((!(StringUtil.nullOrEmptyOrBlankString(lsUserInput))&& lsFieldName.equalsIgnoreCase("PhoneNumber"))||(lsFieldName.equalsIgnoreCase("VBOFaxNumber")))
	    {	    
	    	if((lsFieldName.equalsIgnoreCase("VBOFaxNumber")) && (StringUtil.nullOrEmptyOrBlankString(lsUserInput)))
	    	{
	    		Log.customer.debug("Inside BuysenseUserProfileFieldValidation : Fax Number field value is null, hence no validation required");
	    	}
	    	else
	    	{
	    int count = lsUserInput.length();
	    if(!(count == 10) )
	    {
	    	Log.customer.debug("Inside BuysenseUserProfileFieldValidation : Pattern is "+pattern);
	    Matcher matcher = pattern.matcher(lsUserInput);
		if(matcher.matches())
		{
	    			Log.customer.debug("Inside BuysenseUserProfileFieldValidation : Pattern is matching");
	    			lbreturnvalue = true;
	    		}
		else
		{
				Log.customer.debug("Inside BuysenseUserProfileFieldValidation : Pattern is not matching");
			if(lsFieldName.equalsIgnoreCase("PhoneNumber"))
			{
				msMessage = Fmt.Sil(Base.getSession().getLocale(),errorStringTable, phonenumberValidationError);
			}
			else if(lsFieldName.equalsIgnoreCase("VBOFaxNumber"))
			{
				msMessage = Fmt.Sil(Base.getSession().getLocale(),errorStringTable, faxnumberValidationError);
			}
			lbreturnvalue = false;
		}
	    }
		else
		{
			Log.customer.debug("Inside BuysenseUserProfileFieldValidation : Pattern is not matching");
			if(lsFieldName.equalsIgnoreCase("PhoneNumber"))
			{
				msMessage = Fmt.Sil(Base.getSession().getLocale(),errorStringTable, phonenumberValidationError);
			}
			else if(lsFieldName.equalsIgnoreCase("VBOFaxNumber"))
			{
				msMessage = Fmt.Sil(Base.getSession().getLocale(),errorStringTable, faxnumberValidationError);
			}
			lbreturnvalue = false;
		}
	    	}
    }
		else
    {
			Log.customer.debug("Inside BuysenseUserProfileFieldValidation : Pattern is not matching");
			if(lsFieldName.equalsIgnoreCase("PhoneNumber"))
		{
				msMessage = Fmt.Sil(Base.getSession().getLocale(),errorStringTable, phonenumberValidationError);
 		}
			else if(lsFieldName.equalsIgnoreCase("VBOFaxNumber"))
		{
				msMessage = Fmt.Sil(Base.getSession().getLocale(),errorStringTable, faxnumberValidationError);
		}
			lbreturnvalue = false;
		}
	    return lbreturnvalue;
    }

	 protected ValueInfo[] getParameterInfo()
	 {
		 return parameterInfo;
	 }
	 static
     {
		 parameterInfo = (new ValueInfo[] {
         new ValueInfo("SourceObject", 0),
         new ValueInfo("TargetValue1", 0)
         });
	 }
}

