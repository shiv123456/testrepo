/*
 * @(#)BuysenseCreateApprovableTask.java     1.0 07/18/2001
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 * PVCS Modification :
 *
 * $Log:   R:\Software\development\archives\Ariba 8.x Customizations\app\buyer\Server\classes\extensions\config\java\ams\custom\BuysenseCreateApprovableTask.java-arc  $
 * 
 *    Rev 1.4   23 Feb 2007 17:43:06   iberaha
 * VEPI ST#1797 - Changed getUsers() to getAllUsers() to account for changes in User/Role/Group introduced by 8.1 upgrade.
 * 
 *    Rev 1.3   14 Dec 2004 11:52:10   nrao
 * A8 Dev SPL #159: Log import statistics - transaction count and elapsed time.  
 * 
 *    Rev 1.2   15 Mar 2004 09:34:26   rgiesen
 * Dev SPL #39 - replaced Base.getSession().getPartition() with a better method
 *
 *    Rev 1.1   01 Mar 2004 10:35:24   jnamada
 * Ariba 8.x Dev SPL #31 - Globally change Log.util.debug to Log.customer.debug
 *
 *    Rev 1.0   21 Jan 2004 15:12:14   cm
 * Initial revision.
 *
 *    Rev 1.0   27 Sep 2002 13:07:12   cm
 * Initial revision.
 *
 *    Rev 1.7   27 Sep 2002 ahiranan
 * (e2e, Dev SPL # 40) - Get connection info components from task parameters and load then in hashtable for XMLReader.
 *    Rev 1.6   12 Jul 2002 19:23:36   iberaha
 * (eVA, Dev SPL # 87) - Added the time to run task.
 *
 *    Rev 1.5   May 15 2002 11:17:30   iberaha
 * (eVA, ER # 60) - Parametrized import table to be taken from the ScheduledTask, and changed ORDER BY to order first by PRIORITY.
 *
 *    Rev 1.4   May 06 2002 08:10:32   iberaha
 * (eVA, ER # 57) - Added the logic to read MaxTransactions parameter from ScheduledTasks.table
 *
 *    Rev 1.3   Oct 31 2001 18:58:08   iberaha
 * (e2e, Dev SPL # 26) - Made the filepath parameter driven.
 *
 *    Rev 1.1   Aug 01 2001 15:53:52   ghodum
 * Cleaned up file, added error recovery
 *
 *    Rev 1.0   Jul 18 2001 12:11:52   smokkapa
 * Initial revision.
 *
 */
package config.java.ams.custom;

// 08/07/2003: Updates for Ariba 8.0 (David Chamberlain)
/* 09/05/2003: Updates for Ariba 8.0 (Richard Lee) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/

import java.util.Map;
import java.util.List;
import ariba.base.core.Partition;
import ariba.base.core.Base;
// Ariba 8.0: Don't need ConnectionInfo in this module; it's gone in 8.0 anyhow.
//import ariba.integration.meta.ConnectionInfo;
// Ariba 8.1: Permission has moved
import ariba.user.core.Permission;
import ariba.util.log.Log;
// Ariba 8.0: not needed, import ariba.server.objectserver.core.ServerParameters;
// Ariba 8.0: SimpleScheduledTask moved from ariba.server.objectserver
// to ariba.util.scheduler. Changed the import statement to reflect this.
//import ariba.util.scheduler.SimpleScheduledTask;
//Ariba 8.0: commented out import SimpleScheduledTask since not using
//Ariba 8.0: added import scheduler *
import ariba.util.scheduler.*;
// Ariba 8.1: Need to import MapUtil
import ariba.util.core.MapUtil;
// Ariba 8.1: need Iterator
import java.util.Iterator;

import org.apache.log4j.Level;

//Ariba 8.0: replaced SimpleScheduledTask with ScheduledTask
//81->822 changed Vector to List
//81->822 changed Hashtable to Map
public class BuysenseCreateApprovableTask extends ScheduledTask
{
    private String    msSchedTaskName = null ;
    private String    msFailedTXNPermission = null ;
    private String    msImportDataTable = BuysenseXMLReader.IMPORT_DATA_TABLE_DEFAULT ;
    // Ariba 8.1: The Map constuctor is deprecated by MapUtil::newTable
    private Map mhConnInfo = MapUtil.map() ;
    private Partition moClassPartition = null ;
    private int       miRetryThreshold = BuysenseXMLReader.RETRY_DEFAULT ;
    private int       miMaxTransactions = BuysenseXMLReader.MAX_TRANSACTIONS_DEFAULT ;
    private long      mlStartRunTime = 0 ;
    // Ariba 8.0: new init is not getting the foPartition
    private Partition foPartition = null ;

    public void init( Scheduler scheduler,
                     String fsScheduledTaskName, Map foArguments )
    //public void init( Partition foPartition, String fsScheduledTaskName, Map foArguments )
    {
        super.init(scheduler, fsScheduledTaskName, foArguments) ;

        // Ariba 8.1: Map::keys() is deprecated by Map.keySet.iterator()
        //for ( Enumeration loEnum = foArguments.keys(); loEnum.hasMoreElements() ; )
        for ( Iterator loItr = foArguments.keySet().iterator(); loItr.hasNext() ; )
        {
            String lsKey = (String)loItr.next() ;

            /*
            if ( lsKey.equals( "ExternalConnection" ) )
            {
               msConnName = (String)foArguments.get( lsKey ) ;
               Log.customer.debug( "Connection parameter specified as " + msConnName + "." ) ;
            }
             **/

            if ( lsKey.equals( "DBType" ) )
            {
                mhConnInfo.put( lsKey, (String)foArguments.get( lsKey ) ) ;
                Log.customer.debug( "Connection parameter specified as " + (String)foArguments.get( lsKey ) + "." ) ;
            }

            if ( lsKey.equals( "DBDriver" ) )
            {
                mhConnInfo.put( lsKey, (String)foArguments.get( lsKey ) ) ;
                Log.customer.debug( "Connection parameter specified as " + (String)foArguments.get( lsKey ) + "." ) ;
            }

            if ( lsKey.equals( "DBPassword" ) )
            {
                mhConnInfo.put( lsKey, (String)foArguments.get( lsKey ) ) ;
                Log.customer.debug( "Connection parameter specified as " + (String)foArguments.get( lsKey ) + "." ) ;
            }

            if ( lsKey.equals( "DBURL" ) )
            {
                mhConnInfo.put( lsKey, (String)foArguments.get( lsKey ) ) ;
                Log.customer.debug( "Connection parameter specified as " + (String)foArguments.get( lsKey ) + "." ) ;
            }

            if ( lsKey.equals( "DBUser" ) )
            {
                mhConnInfo.put( lsKey, (String)foArguments.get( lsKey ) ) ;
                Log.customer.debug( "Connection parameter specified as " + (String)foArguments.get( lsKey ) + "." ) ;
            }

            if ( lsKey.equals( "EmailPermission" ) )
            {
                msFailedTXNPermission = (String)foArguments.get( lsKey ) ;
                Log.customer.debug( "Email permission parameter specified as " + msFailedTXNPermission + "." ) ;
            }

            if ( lsKey.equals( "ImportDataTable" ) )
            {
                msImportDataTable = (String)foArguments.get( lsKey ) ;
                Log.customer.debug( "Import Data Table parameter specified as " + msImportDataTable + "." ) ;
            }

            if ( lsKey.equals( "RetryThreshold" ) )
            {
                String lsRetryValue = (String)foArguments.get( lsKey ) ;

                try
                {
                    miRetryThreshold = Integer.parseInt( lsRetryValue ) ;
                }
                catch ( NumberFormatException loNumExcep )
                {
                    Log.customer.debug( "Invalid retry threshold value specified, resetting to default value." ) ;
                    miRetryThreshold = BuysenseXMLReader.RETRY_DEFAULT ;
                }

                Log.customer.debug( "Retry Threshold = " + miRetryThreshold + "." ) ;
            }

            if ( lsKey.equals( "MaxTransactions" ) )
            {
                String lsMaxTransactionsValue = (String)foArguments.get( lsKey ) ;

                try
                {
                    miMaxTransactions = Integer.parseInt( lsMaxTransactionsValue ) ;
                }
                catch ( NumberFormatException loNumExcep )
                {
                    Log.customer.debug( "Invalid max transactions value specified, resetting to default value." ) ;
                    miMaxTransactions = BuysenseXMLReader.MAX_TRANSACTIONS_DEFAULT ;
                }

                Log.customer.debug( "Max Transactions = " + miMaxTransactions + "." ) ;
            }
        }

        //Ariba 8.0: get foPartition since ScheduledTask init is not getting it

		//rgiesen dev SPL #39
        foPartition = Base.getSession().getPartition();
        //foPartition = Base.getService().getPartition();
        ContentTypeContainer.loadContentType(foPartition);
        Log.customer.debug( "ContentTypeContainer Loaded." ) ;

        moClassPartition = foPartition ;
        msSchedTaskName  = fsScheduledTaskName ;
    }

    public void run()
    {
        try
        {
            // Get system time at start run
            mlStartRunTime = System.currentTimeMillis() ;

            Log.customer.debug( "Calling BuysenseXMLReader" ) ;

            BuysenseXMLReader XMLReader = new BuysenseXMLReader() ;

            XMLReader.setImportDataTable( msImportDataTable ) ;
            XMLReader.setFailedTransactionPermission( msFailedTXNPermission ) ;
            XMLReader.setRetryThreshold( miRetryThreshold ) ;
            XMLReader.setMaxTransactions( miMaxTransactions ) ;
            XMLReader.readXML( mhConnInfo, moClassPartition ) ;

            //Calculate elapsed time
            // Dev SPL #159: Log import statistics always, not only in debug mode
            Logs.buysense.setLevel(Level.DEBUG);
            Logs.buysense.debug( "Elapsed time for the run: " + ( System.currentTimeMillis() - mlStartRunTime ) ) ;
            Logs.buysense.setLevel(Level.DEBUG.OFF);
        }
        catch ( Exception loExcep )
        {
            Log.customer.debug( "BuysenseXMLReader failed" ) ;
            sendMail( msSchedTaskName + " failed during run...",
                     "The task " + msSchedTaskName + " failed during run with the following message: " +
                     loExcep.getMessage() ) ;
        }
    }

    private void sendMail( String fsSubject, String fsBody )
    {
        Permission loPermission = Permission.getPermission( msFailedTXNPermission ) ;

        if ( loPermission == null )
        {
            Log.customer.debug( "Could not retrieve permission " + msFailedTXNPermission + "." ) ;
            return ;
        }

        // Ariba 8.1: Permission::users() is deprecated by Permission::getAllUsers()
        List lvUsers = loPermission.getAllUsers() ;

        if ( lvUsers == null || lvUsers.isEmpty() )
        {
            Log.customer.debug( "No user found that has the " + msFailedTXNPermission + " permission." ) ;
            return ;
        }

        BuysenseEmailAdapter.sendMail( lvUsers, fsSubject, fsBody ) ;
    }
}
