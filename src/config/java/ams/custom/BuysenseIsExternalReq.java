package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.fields.Behavior;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.procure.core.ProcureLineItem;
import ariba.purchasing.core.Requisition;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseIsExternalReq extends Condition{
	


    private static final ValueInfo parameterInfo[];

    public boolean evaluate(Object obj, PropertyTable params) throws ConditionEvaluationException
    {
        Log.customer.debug("Inside BuysenseIsExternalReq" + obj);
        if (obj != null && obj instanceof ProcureLineItem)
        {
            return isNativeReq((ProcureLineItem)obj);
        }
        return false;
    }

    private boolean isNativeReq(ProcureLineItem loPli)
    {
        if (loPli != null && loPli instanceof ProcureLineItem)
        {
            Log.customer.debug("BuysenseIsExternalReq: Inside Product line item" + loPli);
            Approvable loAppr = (Approvable) loPli.getLineItemCollection();
            if(loAppr != null && loAppr instanceof Requisition)
            {
                Log.customer.debug("BuysenseIsExternalReq:Inside Requisition now " + loAppr);
                String transactionSource = (String) loAppr.getFieldValue("TransactionSource");
                boolean bBypassApprovers = (Boolean) loAppr.getFieldValue("BypassApprovers");
                if(transactionSource != null)
                {	
                if (((transactionSource.equals("QQ") || transactionSource.equals("E2E"))) ||(transactionSource.equals("EXT")&& bBypassApprovers) )
                {
                    Log.customer
                            .debug("BuysenseIsExternalReq :External Requisition found"
                                    + loPli);
                    return true;
                }
                }
            }
        }
        return false;
    }

    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    static
    {
        parameterInfo = (new ValueInfo[] { new ValueInfo("SourceObject", 0),
        		new ValueInfo("Message", 0, Behavior.StringClass)});
    }


}
