/*  ROHIT 11/01/2000 - This code will set the supplier location on requisition when the Supplier is selected for 
    catalog or non catalog items. Only those locations with AddressType = O (order) will be allowed                         
    this code will over ride the default trigger code that was provided 
*/

/* 08/11/2003: Updates for Ariba 8.0 (Jeff Namadan) */
/* 08/13/2003: Updates for Ariba 8.0 (David Chamberlain)
   Had to back out previous 8.0 changes. We can't change
   ValueInfo to ValueInfo at this time.      */

package config.java.ams.custom;

import ariba.base.core.BaseVector;
import ariba.base.fields.*;
import ariba.common.core.*;
import ariba.procure.core.*;
import ariba.util.core.PropertyTable;
// Ariba 8.1 deprecated Util added StringUtil
//import ariba.util.core.Util;
import ariba.util.core.StringUtil;
/* Ariba 8.0: Removed netscape.util.Enumeration */
// Ariba 8.1: Use Iterators instead of Enumerations
//import java.util.Iterator;
import java.util.Iterator;
import ariba.util.log.Log;

//81->822 changed Enumeration to Iterator
//81->822 changed ConditionValueInfo to ValueInfo
public class BuysenseSetSupplierLocation extends Action
{
    //  Most of the code is duplicated from the Out-Of-The-Box trigger called ariba.procure.core.action.SetSupplierLocation
    private static final String requiredParameterNames[] = 
        {
        "Target", "Source"
    };
    /* Ariba 8.0: Replaced ValueInfo */
    private static final ValueInfo parameterInfo[] = 
        {
        /* Ariba 8.0: Replaced ValueInfo */
        new ValueInfo("Target",
                               true,
                               0,
                               "ariba.common.core.SupplierLocation"),
            new ValueInfo("Source", 0, "ariba.common.core.Supplier")
    };
    /* Ariba 8.0: Replaced ValueInfo */
    private static final ValueInfo valueInfo = new ValueInfo(0,
                                                                               "ariba.procure.core.ProcureLineItem");
    
    public void fire(ValueSource object, PropertyTable params)
    {
        Log.customer.debug("Calling BuysenseSetSupplierLocation object = " + object);
        ProcureLineItem lineItem = (ProcureLineItem)object;
        String targetFieldName = params.stringPropertyForKey("Target");
        Supplier supplier = (Supplier)params.getPropertyForKey("Source");
        if(lineItem == null)
            return;
        ProcureLineItemCollection collection = (ProcureLineItemCollection)lineItem.getLineItemCollection();
        SupplierLocation location = null;
        if(supplier != null)
            location = bestLocation(collection, lineItem, supplier);
        object.setDottedFieldValue(targetFieldName, location);
    }
    
    protected SupplierLocation bestLocation(ProcureLineItemCollection collection,
                                            ProcureLineItem lineItem,
                                            Supplier supplier)
    {
        String lsSupplierName = (String) supplier.getFieldValue("UniqueName");
        if(!StringUtil.nullOrEmptyOrBlankString(lsSupplierName))
        {
            BaseVector lvSLocations = (BaseVector) supplier.getLocations();
            // CSPL242 Do not find bestLocation if lineitem is adhoc AND SupplierLocations are more than one.
            // CSPL-372; CSPL-4918; added logic to set Supplier Location based on user selection 
            // and to reset DummyStringField, that is set through ARPDefaultNamedChooser  
            SupplierLocation currentSL = null;
            currentSL = lineItem.getSupplierLocation();
            boolean bSelectedValue = false;
            String sDummyStringField = (String) lineItem.getDottedFieldValue("DummyStringField");
            Log.customer.debug("sDummyStringField: "+sDummyStringField);
            if(!StringUtil.nullOrEmptyOrBlankString(sDummyStringField)&& sDummyStringField.equalsIgnoreCase("Selected"))
            {
                bSelectedValue = true;
                lineItem.setDottedFieldValue("DummyStringField",null);
                Log.customer.debug("DummyStringField reset");                
            }
            if(lineItem.getIsAdHoc() && lvSLocations.size() > 1)
            {
                if(bSelectedValue)
                {
                    return currentSL;
                }
                else
                {
                    return null;   
                }
            }
            if(lsSupplierName.equals("[Unspecified]"))
            {
            	 return null;
            }
        }

        String addressType=null;
        // Ariba 8.1: Vector::elements() is deprecated by AbstractList::iterator().
        for(Iterator e = supplier.getLocationsObjects(); e.hasNext();)
        //for(Iterator e = (Iterator)supplier.getLocationsObjects(); e.hasNext();)
        {
            
            // here, we put in the edit so that only those supplier locations that have address type = O are selected
            // Ariba 8.1: changed Enumeration nextElement() to Iterator next().
            //SupplierLocation location = (SupplierLocation)e.next();
            SupplierLocation location = (SupplierLocation)e.next();
            addressType = (String)location.getFieldValue("AddressType");
            if (addressType != null) 
            {
                if (addressType.equals("O"))
                {
                    
                    if(!StringUtil.nullOrEmptyOrBlankString(location.getPreferredOrderingMethod()))
                    {
                        return location;
                    }
                }
            }
        }
        // here, we put in the edit so that only those supplier locations that have address type = O are selected
        if(collection != null)
        {
            SupplierLocation location =  supplier.getBestLocation(collection.getRequester());
            if (location != null )  
            {
                addressType = (String) location.getFieldValue("AddressType");     
                if ((addressType != null) && (addressType.equals("O")))
                {
                    return location;
                }
            }
        }
        else 
        {
            return null;
        }
        return null;
    }
    
    /* Ariba 8.0: Replaced ValueInfo */
    protected ValueInfo getValueInfo()
    {
        return valueInfo;
    }
    
    /* Ariba 8.0: Replaced ValueInfo */
    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }
    
    protected String[] getRequiredParameterNames()
    {
        return requiredParameterNames;
    }
    
    // This method was in the decafed code. However, it seems to have an incorrect signature and is not used as such in the code
    //public SetSupplierLocation()
    //{
    //}
    
}
