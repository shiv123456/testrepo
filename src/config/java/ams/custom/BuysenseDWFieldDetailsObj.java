package config.java.ams.custom;

public class BuysenseDWFieldDetailsObj
{
   
    private String                   objecthierarchyname;
    private BuysenseDWFieldDetails[] objfielddetails;

    public String getObjectHierarchyName()
    {
        return objecthierarchyname;
    }

    public BuysenseDWFieldDetails[] getBuysenseFieldDetails()
    {
        return objfielddetails;
    }
}
