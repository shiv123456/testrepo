package config.java.ams.custom;

    /**

     New class required to implement the new editability condition for ReqHeadCB3Value and ReqHeadCB5Value
                           so that both can not be edited on QQ transactions.
    */
    import ariba.base.fields.*;
    import ariba.util.core.*;
    import ariba.base.core.BaseObject;
import ariba.htmlui.fieldsui.Log;

    //81->822 changed ConditionValueInfo to ValueInfo
    public class BuysenseReqHeadEditability extends Condition
    {

        private static final ValueInfo parameterInfo[];

        public boolean evaluate(Object value, PropertyTable params)
        {
            Log.customer.debug("BuysenseReqHeadEditability: This is the object we get in the evaluate: %s ", value);
            BaseObject obj   = (BaseObject)params.getPropertyForKey("SourceObject");
            boolean lbAllowEdit = true;
            if (obj.instanceOf("ariba.purchasing.core.Requisition"))
            {
               if ((obj.getFieldValue("TransactionSource")) != null)
               {
                      lbAllowEdit = false;
               }
            }
            return lbAllowEdit;
        }

        protected ValueInfo[] getParameterInfo()
        {
           return parameterInfo;
        }

        static
        {
           parameterInfo = (new ValueInfo[] {
              new ValueInfo("SourceObject", 0)
             });
        }
    }

