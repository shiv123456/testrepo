/**
This program is used to write Clusterroots (documents) to the TXN table (as part of the ERP integration).
Note: For each type of Clusterroots (ERP document) to be written to the TXN table,
XML mapping data has to be defined in the xml file that is used by the initMappingData function.

Before write() can be called, the XML mapping data has to be initialized by
calling initMappingData(). This should be done at system start-up time.


pbreuss, AMS, March 2000
CHANGES:

    DATE            DEVELOPER       COMMENTS
    11/10/2000      rohit           Overloaded push method. this method is used to push a dummy transaction to the
                                    integrator database. The transaction data will contain the TIN of a transaction
                                    that was pulled in by the pull adater.Using this new transaction, the integrator
                                    will update the integrator status of the ORIGINAL transaction and this new one to be Completed (CMP).

*/

/* 08/13/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 09/10/2003: Updates for Ariba 8.0 (David Chamberlain) */
/*  rgiesen 02/19/2004 Dev SPL #19 - IntegrationDirectory is no longer used for TXNPublish
*/
/* 03/02/2004: Globally change Log.util.debug to Log.customer.debug (Rob Giesen) */
/* 12/17/2004: Removed the static variable TIN (Ivan Beraha) */

/**
DATE			29/10/2010
DEVELOPER		SRINI
CHANGES			No more TIBCO in play in 9r1 so written a new JDBC class and calling it from all push methods.
				New JDBC class does the same function of TIBCO to push the transactions to BUYINT tables such as
				TXNHEADER, TXNDETAIL, TXNSUBDETAIL and TXNATTACHMENT. See calling class AMSTXNJDBC.java for more details.
*/

package config.java.ams.custom;

import ariba.approvable.core.Comment;
import java.util.Map;
import ariba.approvable.core.Approvable;
import ariba.base.core.BaseObject;
import ariba.base.core.ClusterRoot;
import ariba.util.log.Log;
import ariba.base.core.Partition;
import ariba.app.server.*;
import java.util.List;
// Ariba 8.1: Need to import ListUtil
import ariba.util.core.ListUtil;
import ariba.common.core.*;
import java.io.IOException;
//rgiesen 02/19/2004 Dev SPL #19 - added the following imports
import ariba.util.messaging.CallCenter;
import ariba.util.messaging.MessagingException;
import ariba.util.core.*;
import ariba.base.core.*;
import ariba.util.core.Date;

//81->822 changed Vector to List
public class AMSTXN
{
    public final static int TXNPUSHSUCCESS = 0;
    public final static int XMLMAPPINGERROR = 1;
    public final static int TXNPUSHADAPTERERROR = 2;
    public final static int GENERALTXNPUSHERROR = 3;
    public final static String TXNUPDATESTATUS = "CMP";

    //e2e Start
    // Ariba 8.0: List::List() has been deprecated by List::newVector()
    //public static List vAttachments = new List();
    public static List vAttachments = ListUtil.list();
    //e2e End

    // This is the variable that contains the XML mapping data for all documents that will be written to the TXN table
    static AMSTXNStringFactory m_stringfactory=null;

    private static Partition partition;

    public AMSTXN()
    {


        Log.customer.debug("TXN Constructor called");
    }

    /**
    The XML mapping data has to be initialized by calling initMappingData(). This should be done at system start-up time.
    */
    public static synchronized void initMappingData()
    {
        Log.customer.debug("initXMLMappingData called");

        // here we are initializing the XML data
        if(m_stringfactory==null)
        {
            m_stringfactory=new AMSTXNStringFactory();
            //XMLFilepath is a path variable specified in Parameters.table that gives the path to XML files.
            String XMLFilepath = Base.getService().getParameter(Base.getSession().getPartition(),
                                  "Application.AMSParameters.XMLFilepath");
            m_stringfactory.initialize(XMLFilepath);
            Log.customer.debug("IIB XMLFilepath: %s", XMLFilepath);
            System.out.println("XML Data has been loaded...");
        }
        else
            System.out.println("XML Data has been refreshed...");

    } // end initMappingData


/**
    This function is used to write a new record to the TXN table.
    Parameters:
    String clientName            - The name of the client writes a record to the TXN table
    String docType             - The document type of the record to be created
    String docAgency            - The agency of the entry to be created
    String docNumber            - The doc number of the entry to be created
    String processType,         - "NEW" for a new record
    String integratorStatus,     - "RDY" for ready
    String syncFlag,             - "A" for asynchronous
    String retryNo,             - "0" if the first try
    String submitMode,         - "BATCH" if submitted through a batch program (like ScheduledTask)
    ClusterRoot doc,             - the actual ClusterRoot (like Requision, or Payment)
    String lineItems,            - the name of the line items variable that contains the line items
    String subDetail            -  fully qualified variable name that contains the subdetail.(Relative to lineItems
    Partition part)                - the current partition


    This function then uses the XML mapping data of the document docType (stored in m_stringfactory)
    to create one string out of the document doc that is passed in.

    This string is set to the field TRANSACTIONDATA of the TXN table in the Ariba OM. The XML mapping data mechanism also creates a string out of each line of the document and writes it to the field TRANSATIONDETAIL data of each line of TXN.

    After a document has been written to the TXN table in Ariba OM including all lines it is saved and the AMSTXNPushAdapter is invoked.

    This function then uses the XML mapping data of the document docType (stored in m_stringfactory)
    to create one string out of the document doc that is passed in. This string is set set to the field TRANSACTIONDATA of
    the TXN table in the Ariba OM. The XML mapping data mechanism also creates a string out of each line of the document
    and writes it to the field TRANSATIONDETAIL data of each line of TXN.

    After a document has been written to the TXN table in Ariba OM including all lines it is saved and the AMSTXNPushAdapter is invoked.
    This adapter does a one to one mapping of all data to two physical TXN and TXN Detail tables.
    Afterwards, the TXN record is deleted from the Ariba OM.

*/
    public static int push(    String clientName,
                        String txnType,
                        String erpNumber,
                        String processType,
                        String integratorStatus,
                        String syncFlag,
                        String retryNo,
                        String submitMode,
                        ClusterRoot doc,
                        String lineItems,
                        String subDetail,
                        Partition part)
    {

        try
        {
            // -----------------------------------------------------------------
            // now create new TXN header
            // -----------------------------------------------------------------

            // generate key
            partition = part;
            String TIN = new String() ;
            TIN = getTIN(clientName);

            ClusterRoot    TXN = (ClusterRoot) ClusterRoot.create("ariba.core.Buysense.TXN", part);

            // set fields of TXN

            if (erpNumber.equals(""))
                throw new Exception("ERP Number is not set");

            Log.customer.debug("Creating "+txnType+" "+erpNumber+" ...");

            TXN.setFieldValue("UniqueName", TIN);
            TXN.setFieldValue("TIN", TIN);
            TXN.setFieldValue("CLIENTNAME", clientName); // TODO retrieve client name
            TXN.setFieldValue("TXNTYPE", txnType);
            TXN.setFieldValue("ERPNUMBER", erpNumber);
            TXN.setFieldValue("PROCESSTYPE", processType);
            TXN.setFieldValue("INTEGRATORSTATUS", integratorStatus);
            TXN.setFieldValue("ORMSSTATUS", "");
            TXN.setFieldValue("SEVERITY", "");
            TXN.setFieldValue("SYNCFLAG", syncFlag);
            TXN.setFieldValue("RETRYNO", retryNo);
            TXN.setFieldValue("SUBMITMODE", submitMode);
            TXN.setFieldValue("ORMSUNIQUENAME", doc.getFieldValue("UniqueName"));
            TXN.setFieldValue("PUSHSTATUS", "Sending");
            TXN.setFieldValue("CREATEDATE", new Date());

            Log.customer.debug("IXM At the end of setting values");

            // Mod 050300 pbreuss -
            // if process type is RPE (Re-Pre-Encumbrance), a cancel has to be set to REFDATA
            if (processType.equals("RPE"))
            {
                String cancelDataString=m_stringfactory.generateDataString("header", clientName, "RXC", doc);
                Log.customer.debug("Generated REFDATA: "+cancelDataString);
                TXN.setFieldValue("REFDATA", cancelDataString);
            }

            // create new vector for TXNDETAIL
            //List TXNDetailVector = (List)TXN.fieldValue("TXNDETAIL");


            // -----------------------------------------------------------------
            // set detail data here
            // -----------------------------------------------------------------

            // generate a string that represents that ERP transaction of the document PV.
            String datastring=m_stringfactory.generateDataString("header", clientName, txnType, doc);
            Log.customer.debug("Generated TRANSACTIONDATA: "+datastring);
            TXN.setFieldValue("TRANSACTIONDATA", datastring);

            // ------------------------------------------------------------------
            if (lineItems!=null)//ff: Checking for the cancel doc
            {
                // create new vector for TXNDETAIL
                List TXNDetailVector = (List)TXN.getFieldValue("TXNDETAIL");

                // now deal with line items - retrieve LineItems field
                List lineItemsVector = (List)doc.getDottedFieldValue(lineItems);

                // go through all line items
                for (int i=0;i<lineItemsVector.size();i++)
                {

                    ClusterRoot    TXNDetail = (ClusterRoot) ClusterRoot.create("ariba.core.Buysense.TXNDetail", part);

                    TXNDetail.setFieldValue("TIND", TIN);
                    TXNDetail.setFieldValue("UNID", new Integer(i));

                    // This function uses docType and the line item cluster root to generate
                    // a string containing all data from the cluster root concatenated by offsets
                    // retrieved from the XML information for the passed doctype. This string represents
                    // that ERP transaction of the document.
                    // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                    Object obj = lineItemsVector.get(i);
                    if (obj instanceof ariba.base.core.BaseId)
                    {
                        ariba.base.core.BaseId baseIDObject = (ariba.base.core.BaseId) obj;
                        Log.customer.debug("In the baseID section");
                        datastring=m_stringfactory.generateDataString("detail", clientName, txnType, baseIDObject.get());
                        Log.customer.debug("datastring : %s ", datastring);
                        BaseObject li = baseIDObject.get();
                        List acctgVector = (List) li.getDottedFieldValue(subDetail);
                        //List acctgVector = (List) li.getDottedFieldValue("Accountings.SplitAccountings");
                        setSubDetail(TIN, clientName, txnType, TXNDetail, acctgVector, new Integer(i));
                        Log.customer.debug("datastring after the sub detail call : %s ", datastring);
                    }
                    else if (obj instanceof ariba.base.core.BaseObject)
                    {
                        Log.customer.debug("In the baseObject section");
                        datastring=m_stringfactory.generateDataString("detail", clientName, txnType, (BaseObject) obj);
                        Log.customer.debug("datastring : %s ", datastring);
                        List acctgVector = (List)((BaseObject)obj).getDottedFieldValue(subDetail);
                        // List acctgVector = (List)((BaseObject)obj).getDottedFieldValue("Accountings.SplitAccountings");
                        setSubDetail(TIN, clientName, txnType, TXNDetail, acctgVector, new Integer(i));
                        Log.customer.debug("datastring after setsubdetail call: %s ", datastring);
                    }
                    else
                    {
                        Log.customer.debug("TXN Error: Type %s not recognized as line element", obj);
                    }

                   Log.customer.debug("TXNDetail: %s", TXNDetail);
                   TXNDetail.setFieldValue("TRANSACTIONDETAILDATA", datastring);
                   Log.customer.debug("Generated TRANSACTIONDETAILDATA for detail: "+datastring);

                   Log.customer.debug("IXM Right before the save");

                   // Ariba 8.1: List::addElement() is deprecated by List::add()
                   TXNDetailVector.add(TXNDetail);

                   TXNDetail.save();
                   Log.customer.debug("IXM Right after the save of the detail");



                } // end for each line item
            } // if if lineitems=null
            // ------------------------------------------------------------------

            // save TXN to the object model
            TXN.save();
            Log.customer.debug("IXM Right after the save of the TXN");

            //START :: Added by srini for JDBC PUSH
            AMSTXNJDBC amsTXNJDBC = new AMSTXNJDBC();
            int liResult = amsTXNJDBC.txnPut(TXN);
            Log.customer.debug("AMSTXN :: push :: liResult %s", liResult);
            //END :: Added by srini for JDBC PUSH
        }
        catch (XMLMappingException e)
        {
            e.printStackTrace();
            Logs.buysense.debug("TXN :error: No Records have been pushed to the TXN table: %s", e);
            return XMLMAPPINGERROR;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            Logs.buysense.debug("TXN :error: No Records have been pushed to the TXN table: IO Exception occured - %s", e );
            return TXNPUSHADAPTERERROR;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Logs.buysense.debug("TXN :error: No Records have been pushed to the TXN table: %s" , e);
            return GENERALTXNPUSHERROR;
        }
        finally
          {
            Log.customer.debug("IXM in the finally");
           }
        // TODO deal wirh error codes
        return TXNPUSHSUCCESS;
    } // end write


/**
    This is a modified version of the push method ... only difference from base method
    is that instead of the lineitems identifier String, we will be passing
    the lineTypes String array. Also, no subdetail identifier String.

    Parameters:
    String clientName            - The name of the client writes a record to the TXN table
    String docType             - The document type of the record to be created
    String docAgency            - The agency of the entry to be created
    String docNumber            - The doc number of the entry to be created
    String processType,         - "NEW" for a new record
    String integratorStatus,     - "RDY" for ready
    String syncFlag,             - "A" for asynchronous
    String retryNo,             - "0" if the first try
    String submitMode,         - "BATCH" if submitted through a batch program (like ScheduledTask)
    ClusterRoot doc,             - the actual ClusterRoot (like Requision, or Payment)
    String[] lineTypes,            - DETAIL line types array
    Partition part)                - the current partition

*/
    public static synchronized int push(    String clientName,
                        String txnType,
                        String erpNumber,
                        String processType,
                        String integratorStatus,
                        String syncFlag,
                        String retryNo,
                        String submitMode,
                        ClusterRoot doc,
                        String [] lineTypes,
                        Partition part)
    {

        try
        {
            // -----------------------------------------------------------------
            // now create new TXN header
            // -----------------------------------------------------------------

            // generate key
            partition = part;
            String TIN = new String() ;
            TIN = getTIN(clientName);

            ClusterRoot    TXN = (ClusterRoot) ClusterRoot.create("ariba.core.Buysense.TXN", part);

            // set fields of TXN

            if (erpNumber.equals(""))
                throw new Exception("ERP Number is not set");

            Log.customer.debug("Creating "+txnType+" "+erpNumber+" ...");

            TXN.setFieldValue("UniqueName", TIN);
            TXN.setFieldValue("TIN", TIN);
            TXN.setFieldValue("CLIENTNAME", clientName); // TODO retrieve client name
            TXN.setFieldValue("TXNTYPE", txnType);
            TXN.setFieldValue("ERPNUMBER", erpNumber);
            TXN.setFieldValue("PROCESSTYPE", processType);
            TXN.setFieldValue("INTEGRATORSTATUS", integratorStatus);
            TXN.setFieldValue("ORMSSTATUS", "");
            TXN.setFieldValue("SEVERITY", "");
            TXN.setFieldValue("SYNCFLAG", syncFlag);
            TXN.setFieldValue("RETRYNO", retryNo);
            TXN.setFieldValue("SUBMITMODE", submitMode);
            TXN.setFieldValue("ORMSUNIQUENAME", doc.getFieldValue("UniqueName"));
            TXN.setFieldValue("PUSHSTATUS", "Sending");
            TXN.setFieldValue("CREATEDATE", new Date());

            Log.customer.debug("IXM At the end of setting values");
            // generate the header data string for TRANSACTIONDATA
            String datastring=m_stringfactory.generateDataString("header", clientName, txnType, doc);
            Log.customer.debug("Generated TRANSACTIONDATA: "+datastring);
            TXN.setFieldValue("TRANSACTIONDATA", datastring);

            // ------------------------------------------------------------------
            if (lineTypes.length > 0)     // this will always be true!!
            {
                // create new vector for TXNDETAIL
                 List TXNDetailVector = (List)TXN.getFieldValue("TXNDETAIL");

                // now deal with detail lines ...
                // there are actually no line items, all info is at the header level itself
                // however, instead of using the same xml format for all detail lines,
                // the linetype passed in the array will be used as the current txntype
                // to generate TRANSACTIONDETAILDATA correctly
                for (int i=0;i<lineTypes.length;i++)
                {

                   ClusterRoot    TXNDetail = (ClusterRoot) ClusterRoot.create("ariba.core.Buysense.TXNDetail", part);

                   TXNDetail.setFieldValue("TIND", TIN);
                   TXNDetail.setFieldValue("UNID", new Integer(i));

                   datastring=m_stringfactory.generateDataString("detail", clientName, lineTypes[i], doc);
                   Log.customer.debug("datastring : %s ", datastring);

                   // No subdetail ... so use an empty vector ... ...
                   // Ariba 8.1: List::List() has been deprecated by ListUtil::newVector()
                   setSubDetail(TIN, clientName, txnType, TXNDetail, ListUtil.list(), new Integer(i));
                   Log.customer.debug("datastring after setsubdetail call: %s ", datastring);

                   Log.customer.debug("TXNDetail: %s", TXNDetail);
                   TXNDetail.setFieldValue("TRANSACTIONDETAILDATA", datastring);
                   Log.customer.debug("Generated TRANSACTIONDETAILDATA for detail: "+datastring);

                   Log.customer.debug("IXM Right before the save");

                   // Ariba 8.1: List::addElement() is deprecated by List::add()
                   TXNDetailVector.add(TXNDetail);

                   TXNDetail.save();
                   Log.customer.debug("IXM Right after the save of the detail");

                } // endfor lineTypes length
            } // endif lineTypes.length > 0
            // ------------------------------------------------------------------

            // save TXN to the object model
            TXN.save();
            Log.customer.debug("IXM Right after the save of the TXN");

           //START :: Added by srini for JDBC PUSH
            AMSTXNJDBC amsTXNJDBC = new AMSTXNJDBC();
            int liResult = amsTXNJDBC.txnPut(TXN);
            Log.customer.debug("AMSTXN :: push :: liResult %s", liResult);
            //END :: Added by srini for JDBC PUSH

        }
        catch (XMLMappingException e)
        {
            e.printStackTrace();
            Logs.buysense.debug("TXN :error: No Records have been pushed to the TXN table: %s", e);
            return XMLMAPPINGERROR;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            Logs.buysense.debug("TXN :error: No Records have been pushed to the TXN table: IO Exception occured - %s", e );
            return TXNPUSHADAPTERERROR;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Logs.buysense.debug("TXN :error: No Records have been pushed to the TXN table: %s" , e);
            return GENERALTXNPUSHERROR;
        }
        finally
          {
            Log.customer.debug("IXM in the finally");
           }
        // TODO deal wirh error codes
        return TXNPUSHSUCCESS;
    } // end write

    /**
    This method is used to write a new record to the TXN table.
    It is also specific to e2e processing, as it takes care of pushing
    AMSTXNATTACHMENT objects, if any attachments on Approvable are found.
    */
    public static int push(    String clientName,
                            String txnType,
                            String erpNumber,
                            String processType,
                            String integratorStatus,
                            String syncFlag,
                            String retryNo,
                            String submitMode,
                            ClusterRoot doc,
                            String lineItems,
                            String subDetail,
                            Partition part,
                            boolean sendAttachments)
    {

        try
        {
            // -----------------------------------------------------------------
            // now create new TXN header
            // -----------------------------------------------------------------

            // generate key
            partition = part;
            String TIN = new String() ;
            TIN = getTIN(clientName);

            ClusterRoot    TXN = (ClusterRoot) ClusterRoot.create("ariba.core.Buysense.TXN", part);

            // set fields of TXN

            //e2e Start
            if (sendAttachments == true)
            {

                BuysenseAttachmentFetcher baf = new BuysenseAttachmentFetcher();
                vAttachments = baf.processAttachments((Approvable) doc);
                if ( ! vAttachments.isEmpty() )
                {
                    Log.customer.debug("Attachments total: " + vAttachments.size());
                    List TXNAttacmentVector = (List)TXN.getFieldValue("TXNATTACHMENT");

                    for (int i = 0; i < vAttachments.size(); i++)
                    {

                    Log.customer.debug("IIB Processing Attachments...");

                        // List::elementAt(int) is deprecated by List::get(int)
                        Object[] attachmentCommentArray = (Object[]) vAttachments.get(i);

                    Log.customer.debug("IIB Created the array...");

                        ClusterRoot TXNATTACHMENT = (ClusterRoot) ClusterRoot.create("ariba.core.Buysense.TXNAttachment", part);
                        TXNATTACHMENT.setFieldValue("UniqueName",                   TIN);
                        TXNATTACHMENT.setFieldValue("TIN",                          TIN);
                        TXNATTACHMENT.setFieldValue("LINE_ID_STR",                  attachmentCommentArray[0]);
                        TXNATTACHMENT.setFieldValue("ATTACHMENT_ID_STR",            attachmentCommentArray[1]);
                        TXNATTACHMENT.setFieldValue("STATUS",                       attachmentCommentArray[2]);
                        TXNATTACHMENT.setFieldValue("APPROVABLE_UNIQUE_NAME",       attachmentCommentArray[3]);
                        TXNATTACHMENT.setFieldValue("CREATION_DATE",                attachmentCommentArray[4]);
                        TXNATTACHMENT.setFieldValue("USER_ID",                      attachmentCommentArray[5]);
                        TXNATTACHMENT.setFieldValue("STORED_FILENAME",              attachmentCommentArray[6]);
                        TXNATTACHMENT.setFieldValue("FILENAME",                     attachmentCommentArray[7]);
                        TXNATTACHMENT.setFieldValue("PROPRIETARY_AND_CONFIDENTIAL", attachmentCommentArray[8]);
                        TXNATTACHMENT.save();

                       // Ariba 8.1: List::addElement() is deprecated by List::add()
                       TXNAttacmentVector.add(TXNATTACHMENT);
                    }
                }
            }
            //e2e End

            if (erpNumber.equals(""))
                throw new Exception("ERP Number is not set");

            Log.customer.debug("Creating "+txnType+" "+erpNumber+" ...");

            TXN.setFieldValue("UniqueName", TIN);
            TXN.setFieldValue("TIN", TIN);
            TXN.setFieldValue("CLIENTNAME", clientName); // TODO retrieve client name
            TXN.setFieldValue("TXNTYPE", txnType);
            TXN.setFieldValue("ERPNUMBER", erpNumber);
            TXN.setFieldValue("PROCESSTYPE", processType);
            TXN.setFieldValue("INTEGRATORSTATUS", integratorStatus);
            TXN.setFieldValue("ORMSSTATUS", "");
            TXN.setFieldValue("SEVERITY", "");
            TXN.setFieldValue("SYNCFLAG", syncFlag);
            TXN.setFieldValue("RETRYNO", retryNo);
            TXN.setFieldValue("SUBMITMODE", submitMode);
            TXN.setFieldValue("ORMSUNIQUENAME", doc.getFieldValue("UniqueName"));
            TXN.setFieldValue("PUSHSTATUS", "Sending");
            TXN.setFieldValue("CREATEDATE", new Date());

            Log.customer.debug("IXM At the end of setting values");

            // Mod 050300 pbreuss -
            // if process type is RPE (Re-Pre-Encumbrance), a cancel has to be set to REFDATA
            if (processType.equals("RPE"))
            {
                String cancelDataString=m_stringfactory.generateDataString("header", clientName, "RXC", doc);
                Log.customer.debug("Generated REFDATA: "+cancelDataString);
                TXN.setFieldValue("REFDATA", cancelDataString);
            }

            // create new vector for TXNDETAIL
            //List TXNDetailVector = (List)TXN.fieldValue("TXNDETAIL");


            // -----------------------------------------------------------------
            // set detail data here
            // -----------------------------------------------------------------

            // generate a string that represents that ERP transaction of the document PV.
            String datastring=m_stringfactory.generateDataString("header", clientName, txnType, doc);
            Log.customer.debug("Generated TRANSACTIONDATA: "+datastring);
            TXN.setFieldValue("TRANSACTIONDATA", datastring);

            // ------------------------------------------------------------------
            if (lineItems!=null)//ff: Checking for the cancel doc
            {
                // create new vector for TXNDETAIL
                List TXNDetailVector = (List)TXN.getFieldValue("TXNDETAIL");

                // now deal with line items - retrieve LineItems field
                List lineItemsVector = (List)doc.getDottedFieldValue(lineItems);

                // go through all line items
                for (int i=0;i<lineItemsVector.size();i++)
                {

                    ClusterRoot    TXNDetail = (ClusterRoot) ClusterRoot.create("ariba.core.Buysense.TXNDetail", part);

                    TXNDetail.setFieldValue("TIND", TIN);
                    TXNDetail.setFieldValue("UNID", new Integer(i));

                    // This function uses docType and the line item cluster root to generate
                    // a string containing all data from the cluster root concatenated by offsets
                    // retrieved from the XML information for the passed doctype. This string represents
                    // that ERP transaction of the document.
                    // List::elementAt(int) is deprecated by List::get(int)
                    Object obj = lineItemsVector.get(i);
                    if (obj instanceof ariba.base.core.BaseId)
                    {
                        ariba.base.core.BaseId baseIDObject = (ariba.base.core.BaseId) obj;
                        Log.customer.debug("In the baseID section");
                        datastring=m_stringfactory.generateDataString("detail", clientName, txnType, baseIDObject.get());
                        Log.customer.debug("datastring : %s ", datastring);
                        BaseObject li = baseIDObject.get();
                        List acctgVector = (List) li.getDottedFieldValue(subDetail);
                        //List acctgVector = (List) li.getDottedFieldValue("Accountings.SplitAccountings");
                        setSubDetail(TIN, clientName, txnType, TXNDetail, acctgVector,new Integer(i));
                        Log.customer.debug("datastring after the sub detail call : %s ", datastring);
                    }
                    else if (obj instanceof ariba.base.core.BaseObject)
                    {
                        Log.customer.debug("In the baseObject section");
                        datastring=m_stringfactory.generateDataString("detail", clientName, txnType, (BaseObject) obj);
                        Log.customer.debug("datastring : %s ", datastring);
                        List acctgVector = (List)((BaseObject)obj).getDottedFieldValue(subDetail);
                        // List acctgVector = (List)((BaseObject)obj).getDottedFieldValue("Accountings.SplitAccountings");
                        setSubDetail(TIN, clientName, txnType, TXNDetail, acctgVector, new Integer(i));
                        Log.customer.debug("datastring after setsubdetail call: %s ", datastring);
                    }
                    else
                    {
                        Log.customer.debug("TXN Error: Type %s not recognized as line element", obj);
                    }

                   Log.customer.debug("TXNDetail: %s", TXNDetail);
                   TXNDetail.setFieldValue("TRANSACTIONDETAILDATA", datastring);
                   Log.customer.debug("Generated TRANSACTIONDETAILDATA for detail: "+datastring);

                   Log.customer.debug("IXM Right before the save");

                   // Ariba 8.1: List::addElement() is deprecated by List::add()
                   TXNDetailVector.add(TXNDetail);

                   TXNDetail.save();
                   Log.customer.debug("IXM Right after the save of the detail");



                } // end for each line item
            } // if if lineitems=null
            // ------------------------------------------------------------------

            // save TXN to the object model
            TXN.save();
            Log.customer.debug("IXM Right after the save of the TXN");

            //START :: Added by srini for JDBC PUSH
            AMSTXNJDBC amsTXNJDBC = new AMSTXNJDBC();
            int liResult = amsTXNJDBC.txnPut(TXN);
            Log.customer.debug("AMSTXN :: push :: liResult %s", liResult);
            //END :: Added by srini for JDBC PUSH

        }
        catch (XMLMappingException e)
        {
            e.printStackTrace();
            Logs.buysense.debug("TXN :error: No Records have been pushed to the TXN table: %s", e);
            return XMLMAPPINGERROR;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            Logs.buysense.debug("TXN :error: No Records have been pushed to the TXN table: IO Exception occured - %s", e );
            return TXNPUSHADAPTERERROR;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Logs.buysense.debug("TXN :error: No Records have been pushed to the TXN table: %s" , e);
            return GENERALTXNPUSHERROR;
        }
        finally
          {
            Log.customer.debug("IXM in the finally");
           }
        // TODO deal wirh error codes
        return TXNPUSHSUCCESS;
    } // end write



    // Comment?
    public static int push(    String clientName,
                            String txnType,
                            String erpNumber,
                            String processType,
                            String integratorStatus,
                            String syncFlag,
                            String retryNo,
                            String submitMode,
                            String tableName,
                            Partition part)
    {

        try
            {
            // -----------------------------------------------------------------
            // now create new TXN header
            // -----------------------------------------------------------------

            // generate key
            String TIN = new String() ;
            TIN = getTIN(clientName);

            ClusterRoot    TXN = (ClusterRoot) ClusterRoot.create("ariba.core.Buysense.TXN", part);

            // set fields of TXN

            Log.customer.debug("Creating "+txnType+" "+erpNumber+" ...");

            TXN.setFieldValue("UniqueName", TIN);
            TXN.setFieldValue("TIN", TIN);
            TXN.setFieldValue("CLIENTNAME", clientName); // TODO retrieve client name
            TXN.setFieldValue("TXNTYPE", txnType);
            TXN.setFieldValue("ERPNUMBER", erpNumber);
            TXN.setFieldValue("PROCESSTYPE", processType);
            TXN.setFieldValue("INTEGRATORSTATUS", integratorStatus);
            TXN.setFieldValue("ORMSSTATUS", "");
            TXN.setFieldValue("SEVERITY", "");
            TXN.setFieldValue("SYNCFLAG", syncFlag);
            TXN.setFieldValue("RETRYNO", retryNo);
            TXN.setFieldValue("SUBMITMODE", submitMode);
            TXN.setFieldValue("ORMSUNIQUENAME", "");
            TXN.setFieldValue("PUSHSTATUS", "Sending");
            TXN.setFieldValue("CREATEDATE", new Date());


            TXN.setFieldValue("TRANSACTIONDATA", tableName);


            TXN.save();

            //START :: Added by srini for JDBC PUSH
            AMSTXNJDBC amsTXNJDBC = new AMSTXNJDBC();
            int liResult = amsTXNJDBC.txnPut(TXN);
            Log.customer.debug("AMSTXN :: push :: liResult %s", liResult);
            //END :: Added by srini for JDBC PUSH

        }

        catch (Exception e)
        {
            e.printStackTrace();
            Log.customer.debug("TXN ERROR: No Records have been pushed to the TXN table: %s", e);
            return GENERALTXNPUSHERROR;
        }

        // TODO deal wirh error codes
        return TXNPUSHSUCCESS;
    } // end write



//  Overloaded push method. this method is used to push a dummy transaction to the integrator database. The transaction
//  data will contain the TIN of a transaction that was pulled in by the pull adater.Using this new transaction, the integrator
//  will update the integrator status of the ORIGINAL transaction and this new one to be Completed (CMP).

    public static int push(    String txnType, String integratorStatus, String originalTIN, Partition part)
    {

        try
            {
            // -----------------------------------------------------------------
            // now create new TXN header
            // -----------------------------------------------------------------

            // generate key. Since this is dummy transaction, the clientName is not needed. hence, fudged to be BASELINE
            String TIN = new String() ;
            TIN = getTIN("BASELINE");

            Log.customer.debug("Creating dummy TXN object for DB Update");

            ClusterRoot    TXN = (ClusterRoot) ClusterRoot.create("ariba.core.Buysense.TXN", part);

            // set fields of TXN


            Log.customer.debug("new TIN is %s", TIN);

            TXN.setFieldValue("UniqueName", TIN);
            TXN.setFieldValue("TIN", TIN);
            TXN.setFieldValue("TXNTYPE", txnType);
            TXN.setFieldValue("INTEGRATORSTATUS", integratorStatus);
            TXN.setFieldValue("TRANSACTIONDATA", originalTIN + ";" + TXNUPDATESTATUS);
            TXN.setFieldValue("PUSHSTATUS", "Sent");
            TXN.setFieldValue("CREATEDATE", new Date());


            TXN.save();

            //START :: Added by srini for JDBC PUSH
            AMSTXNJDBC amsTXNJDBC = new AMSTXNJDBC();
            int liResult = amsTXNJDBC.txnPut(TXN);
            Log.customer.debug("AMSTXN :: push :: liResult %s", liResult);
            //END :: Added by srini for JDBC PUSH
        }



        catch (Exception e)
        {
            e.printStackTrace();
            Log.customer.debug("TXN ERROR: No Records have been pushed to the TXN table: %s", e);
            return GENERALTXNPUSHERROR;
        }

        // TODO deal wirh error codes
        return TXNPUSHSUCCESS;
    } // end write

 public static void txnPublish (ClusterRoot    txn)
      throws IOException
    {
      //rgiesen 02/19/2004 Dev SPL #19 - modified this function to
      //push the TXN to the listener
        CallCenter callCenter = CallCenter.defaultCenter();
        String  topicName = "AMSTXNPushEvent";

            // Attempt to send the order to the addapter
        Log.customer.debug("AMSTXN.txnPublish() - Sending transaction %s through the adapter.", txn);

        Map userData = MapUtil.map(3);
        Map userInfo = MapUtil.map(3);

        userData.put("TXN", txn);
      userInfo.put("Partition",txn.getPartition().getName());

        AMSTXNReplyListener listener = new AMSTXNReplyListener(txn.getBaseId(), ObjectServer.objectServer());

        try {
            callCenter.callAsync(topicName, userData, userInfo, listener);
        }
        catch (MessagingException e) {
            Log.customer.debug("MessagingException in AMSTXN.txnpublish()") ;
            throw new IOException();
        }
    }


    /**
        Returns true if a field name was found
    */
    public static boolean isTriggerField(String clientName, String sectionName, String className, String fieldName)
    {
        return m_stringfactory.isTriggerField(clientName, sectionName, className, fieldName);
    }

    public static synchronized String getTIN(String clientName)
    {
       if (clientName != null)
       {
          try
          {
             Thread.sleep(1);
          }
          catch(Exception loEx)
          {
             loEx.printStackTrace();
          }

          return (clientName + System.currentTimeMillis());
       }
       else
          return null;
    }


    /** Goes through accounting vector of the passed in line item and sets the SubDetail record using
    the mapping XML */

    private static synchronized void setSubDetail(String TIN,
                                     String clientName,
                                     String txnType,
                                     ClusterRoot TXNDetail,
                                     List acctgVector,
                                     Integer liNum)
                        throws XMLMappingException
    {
        Log.customer.debug("Buysense:In the setSubDetail");
        String datastring =null;
        List subDetailVector = (List)TXNDetail.getDottedFieldValue("TXNSUBDETAIL");
        Log.customer.debug("This is th reference to the subdetail vector 1: %s",subDetailVector);
        //List acctgVector = (List) lineItem.getDottedFieldValue("Accountings.SplitAccountings");
        Log.customer.debug("This is the accounting vector %s",acctgVector);
        int size = acctgVector.size();
        for(int i=0;i<size;i++) {

            ClusterRoot    TXNSub = (ClusterRoot) ClusterRoot.create("ariba.core.Buysense.TXNSubDetail", partition);
            TXNSub.setFieldValue("UniqueName", TIN);
            TXNSub.setFieldValue("TIND", TIN);
            TXNSub.setFieldValue("UNID", liNum);
            TXNSub.setFieldValue("SUBLINEID", new Integer(i));
            // List::elementAt(int) is deprecated by List::get(int)
            Object obj = acctgVector.get(i);
            if (obj instanceof ariba.base.core.BaseId) {
              ariba.base.core.BaseId baseIDObject = (ariba.base.core.BaseId) obj;
              datastring=m_stringfactory.generateDataString("subdetail", clientName, txnType, baseIDObject.get());
            }
            else if (obj instanceof ariba.base.core.BaseObject) {
              datastring=m_stringfactory.generateDataString("subdetail", clientName, txnType, (BaseObject) obj);
            }
            TXNSub.setFieldValue("TRANSACTIONDETAILDATA", datastring);
            Log.customer.debug("This is the subdetail: %s", datastring);
            Log.customer.debug("This is th reference to the subdetail vector 2: %s",subDetailVector);
            // Ariba 8.1: List::addElement() is deprecated by List::add()
            subDetailVector.add(TXNSub);
            Log.customer.debug("Just before save of TXNSub: %s", TXNSub);
            TXNSub.save();
            Log.customer.debug("Just after save of TXNSub");
        }

  }

}
