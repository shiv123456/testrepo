// Version 1.0

//***************************************************************************/
//  Anup - August 2001 - Trigger for visibility/editability of fields on payment accountings  
//
//***************************************************************************/
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.core.*;
//import ariba.htmlui.fieldsui.Log;
import ariba.util.log.Log;


//81->822 changed ConditionValueInfo to ValueInfo
public class PayAcctgVisibility extends Condition
{

    private static final ValueInfo parameterInfo[];
    private static final String EqualToMsg1 = "EqualToMsg1";

    public boolean evaluate(Object value, PropertyTable params)
    {
       Log.customer.debug("PayAcctgVisibility: This is the object we get in the evaluate: %s ", value);
       BaseObject bo = (BaseObject)params.getPropertyForKey("TargetValue");
       ClusterRoot cr = bo.getClusterRoot();
       
       //Visibility Stuff
       PaymentDefaulter pd = new PaymentDefaulter();
       pd.setAccounting((ValueSource)cr,params);
       
       return true;
             
    }

    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    static
    {
        parameterInfo = (new ValueInfo[] {
            new ValueInfo("TargetValue", 0),new ValueInfo("Message", 0, Behavior.StringClass)
        });
    }
}
