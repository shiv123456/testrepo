package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.BaseId;
import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.core.ClusterRoot;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ConditionResult;
import ariba.base.fields.ValueInfo;
import ariba.base.fields.ValueSource;
import ariba.util.core.Fmt;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseQQRolesValidation extends Condition{
	
	private static final ValueInfo parameterInfo[];
	String msMessage = null;
    private static         String        errorStringTable        = "ariba.procure.core";
    private static         String        qqRoleValidationError   = "QuickQuoteValidationError";
    public ConditionResult evaluateAndExplain(Object object, PropertyTable params)
	{
        Log.customer.debug("Calling BuysenseEmployeeNumberValidation for new User Profile Request Eform.");
        if(!evaluate(object, params))
        {
            String retMsg = null;
            if(!StringUtil.nullOrEmptyOrBlankString(msMessage))
            {
                retMsg = msMessage;
                msMessage = null;
                return new ConditionResult(retMsg);
            }
            else
                return null;
        }
        return null;
    }
	public boolean evaluate(Object obj, PropertyTable params)
	{
		BaseObject baseobj  = (BaseObject)params.getPropertyForKey("SourceObject");	
		Log.customer.debug("Inside BuysenseQQRolesValidation"+baseobj);
		//ValueSource context = getValueSourceContext();
		BaseObject boRoles1 = null;
		String lsRoleName1 = null;
		BaseObject boRoles2 = null;
		String lsRoleName2 = null;		
		
		if(((Approvable)baseobj).instanceOf("ariba.core.BuysenseUserProfileRequest"))
		{
     		BaseVector bvQQRoles = (BaseVector)baseobj.getFieldValue("QQRoles");
     		Log.customer.debug("Inside BuysenseQQRolesValidation : QQRoles is"+bvQQRoles);
			//ClusterRoot lcQQRoles = (ClusterRoot)baseobj.getFieldValue("QQRoles");
			//BaseVector bvQQRoles = (BaseVector)baseobj.getFieldValue("QQRoles");
			if(bvQQRoles!=null && bvQQRoles.size()>1)
			{
				
				for(int i=0;i<(bvQQRoles.size()-1);i++)
				{
					boRoles1 = (BaseObject)Base.getSession().objectFromId((BaseId) bvQQRoles.get(i));
					Log.customer.debug("Inside BuysenseQQRolesValidation : boRoles1 is"+boRoles1);
					if(boRoles1 != null)
					{
					lsRoleName1 = (String)boRoles1.getFieldValue("UniqueName");
					Log.customer.debug("Inside BuysenseQQRolesValidation : lsRoleName1 is"+lsRoleName1);
					
					for(int j=1;j<bvQQRoles.size();j++)
					{
						boRoles2 = (BaseObject)Base.getSession().objectFromId((BaseId) bvQQRoles.get(j));
						Log.customer.debug("Inside BuysenseQQRolesValidation : boRoles2 is"+boRoles2);
						if(boRoles2 != null)
						{
						lsRoleName2 = (String)boRoles2.getFieldValue("UniqueName");
						Log.customer.debug("Inside BuysenseQQRolesValidation : lsRoleName2 is"+lsRoleName2);
						if((lsRoleName1!=null && lsRoleName2 != null && (lsRoleName1.equalsIgnoreCase("Basic") && lsRoleName2.equalsIgnoreCase("Advanced"))) ||(lsRoleName1!=null && lsRoleName2 != null && (lsRoleName1.equalsIgnoreCase("Advanced") && lsRoleName2.equalsIgnoreCase("Basic"))))
						{
							Log.customer.debug("Inside BuysenseQQRolesValidation : Validity condition fails");
							msMessage = Fmt.Sil(Base.getSession().getLocale(),errorStringTable, qqRoleValidationError);
							return false;
						}
						}
					}
					}
				}
			}
		}
		
		return true;
	}
	 protected ValueInfo[] getParameterInfo()
	 {
		 return parameterInfo;
	 }
	 static
     {
		 parameterInfo = (new ValueInfo[] {
         new ValueInfo("SourceObject", 0)
         });
	 }
}
