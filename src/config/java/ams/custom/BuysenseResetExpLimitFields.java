package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.basic.core.Money;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseResetExpLimitFields extends Action
{
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
    	Log.customer.debug("***%s***Called fire:: valuesource obj %s", valuesource);
    	Approvable loAppr = null;
        
        if(valuesource!= null && valuesource instanceof Approvable)
        {
           loAppr = (Approvable)valuesource;
           Money lmExpenditureLimitAmt = (Money)loAppr.getFieldValue("AribaExpenditureLimitAmt");
           if(lmExpenditureLimitAmt == null)
           {
        	   loAppr.setFieldValue("AribaExpenditureLimitType", null);
        	   loAppr.setFieldValue("AribaExpenditureLimitApprover", null);
           }
        }
    }

}
