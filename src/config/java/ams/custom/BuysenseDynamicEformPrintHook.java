package config.java.ams.custom;

import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import ariba.approvable.core.Approvable;
import ariba.approvable.core.Comment;
import ariba.approvable.core.PrintApprovableHook;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseSession;
import ariba.base.core.LongString;
import ariba.base.core.Partition;
import ariba.base.fields.FieldProperties;
import ariba.approvable.core.ApprovalRequest;
import ariba.basic.core.Money;
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;
import ariba.util.formatter.BigDecimalFormatter;
import ariba.util.log.Log;
import ariba.util.net.HTMLPrintWriter;

public class BuysenseDynamicEformPrintHook implements PrintApprovableHook
{
    private static final List NoErrorResult = ListUtil.list(Constants.getInteger(NoError));
    Hashtable hHdrParams = new Hashtable();
    Hashtable hHdrParamsAppr = new Hashtable();

    public List run(Approvable approvable, PrintWriter out, Locale locale, boolean printForEmail)
    {
        HTMLPrintWriter loHout = new HTMLPrintWriter(out);
        String lsHtmlPage=null;
        String lsHtmlApprovalPage = null;
        String lsTotalApprovers = null;
        String lsReqType=approvable.getTypeName();
        if (printForEmail)
        {
            Log.customer.debug("PrintForEmail is true");
            return NoErrorResult;
        }
        try
        {
            if (lsReqType.equals("ariba.core.BuysDynamicEform"))
            {

                lsHtmlPage = PSGFunctions.ReadFile("config/htmlui/resource/ariba.core.BuysDynamicEform.html");
                lsHtmlApprovalPage = PSGFunctions.ReadFile("config/htmlui/resource/ariba.core.BuysDynamicEform.Approvers.html");
                
                //lsHtmlPage = getFinalHTML(lsHtmlPage,approvable);
                
                hHdrParams = getFieldValues(approvable,lsHtmlPage);
                
                lsHtmlPage = PSGFunctions.ReplaceHtml(lsHtmlPage, hHdrParams);
                lsHtmlPage = getFieldValuesAppr(approvable,lsHtmlApprovalPage,lsHtmlPage);
                loHout.println(lsHtmlPage);
            }
        }
        catch (Exception e)
        {
            Log.customer.debug(e);
        }
        return NoErrorResult;
    }
    private String getFieldValuesAppr(Approvable approvable,
			String lsHtmlApprovalPage,String lsHtmlPage) {
		// TODO Auto-generated method stub
    	String sTotalApprovers="";
        int liTag = 0, liTagEnd = -1;
        String lsTag = null;  
        liTag = lsHtmlApprovalPage.lastIndexOf("<!--Appr.");
        Log.customer.debug("The value of liTag is :"+liTag);
        Enumeration eParams = null;
        liTagEnd = -1;
        Object loFieldVal=null;
        while (liTag >= 0)
        {
            liTagEnd = lsHtmlApprovalPage.indexOf("-->", liTag + 1);
            lsTag = lsHtmlApprovalPage.substring(liTag + 4, liTagEnd);
            hHdrParamsAppr.put(lsTag, "[?]");
            liTag = lsHtmlApprovalPage.lastIndexOf("<!--Appr.", liTag - 1);
        }
        List vecApprovalRequests = ListUtil.list();
        List lvecApprovalRequests = ListUtil.list();
        ApprovalRequest appReq = new ApprovalRequest();
        vecApprovalRequests = (List)approvable.getFieldValue("ApprovalRequests");

        lvecApprovalRequests = ListUtil.cloneList(vecApprovalRequests);
        int size = lvecApprovalRequests.size();

        for (int i = 0; i < size; i ++)
        {
            appReq = (ApprovalRequest) lvecApprovalRequests.get(i);
            lvecApprovalRequests = getDependencies(lvecApprovalRequests, appReq);
        }
        size = lvecApprovalRequests.size();
        Log.customer.debug("The size of "+lvecApprovalRequests.toString()+" is :"+size);

        for (int j = (size-1); j >= 0; j--)
        {
            appReq = (ApprovalRequest) lvecApprovalRequests.get(j);

            eParams = hHdrParamsAppr.keys();
            while (eParams.hasMoreElements())
            {
            	lsTag = eParams.nextElement().toString();
            	String sSubString="";
            	sSubString=lsTag.substring(5);
                loFieldVal = appReq.getDottedFieldValue(sSubString); 

                Log.customer.debug("The Field and the field value are:"+sSubString+" values is :"+loFieldVal);
                
                if (loFieldVal == null)
                	loFieldVal = "";

                if (sSubString.equals("ApprovalRequired"))
                {
                    String soValue=String.valueOf(loFieldVal);
                    if (soValue.equals("true"))
                    {
                    	hHdrParamsAppr.put(lsTag, "Yes");
                    }
                    else if (soValue.equals("false"))
                    {
                    	hHdrParamsAppr.put(lsTag, "No");
                    }
                }
                if(sSubString.equals("LastModified"))
                {
                	ariba.util.core.Date loDateValue = (ariba.util.core.Date) appReq.getFieldValue(sSubString);
                	String lsDateValue = loDateValue.toConciseDateTimeString();
                	loFieldVal = lsDateValue;
                	Log.customer.debug("The concise date format for the entered date is:"+loFieldVal);      
                	Object loState = appReq.getFieldValue("State");
                	String lsState = String.valueOf(loState);
                	if(lsState.equals("8"))
                	{
                    	hHdrParamsAppr.put(lsTag, String.valueOf(loFieldVal));
                        Log.customer.debug("APPR: " + lsTag + "(Approved_Date)='" + loFieldVal.toString() + "'");                		
                	}
                	else
                	{
                        String lsFieldVal = "";
                    	hHdrParamsAppr.put(lsTag, lsFieldVal);
                        Log.customer.debug("APPR: " + lsTag + "(Approved_Date)='" + lsFieldVal + "'");                		
                		
                	}
                }
                else if (sSubString.equals("State"))
                {
                    String soValue=String.valueOf(loFieldVal);
                    if (soValue.equals("1"))
                    {
                    	hHdrParamsAppr.put(lsTag, "Pending");
                    }
                    else if (soValue.equals("2"))
                    {
                    	hHdrParamsAppr.put(lsTag, "Pending");
                    }
                    else if (soValue.equals("4"))
                    {
                    	hHdrParamsAppr.put(lsTag, "Denied");
                    }
                    else if (soValue.equals("8"))
                    {
                    	hHdrParamsAppr.put(lsTag, "Approved");
                    	Log.customer.debug("APPR: " + lsTag + "='" + soValue.toString() + "'");
                    }
                }
                else
                {
                	hHdrParamsAppr.put(lsTag, String.valueOf(loFieldVal));
                    Log.customer.debug("APPR: " + lsTag + "='" + loFieldVal.toString() + "'");
                }
            }
            //Log.customer.debug("")
            sTotalApprovers += PSGFunctions.ReplaceHtml(lsHtmlApprovalPage,
            		hHdrParamsAppr);

        }
        lsHtmlPage = PSGFunctions.ReplaceString(lsHtmlPage,"<!--Approvers-->",sTotalApprovers, true);
        return lsHtmlPage;
	}

    private String getFinalHTML(String lsHtmlPage, Approvable loAppr) {
    	String lsApprovableType=loAppr.getTypeName();
    	String str1 = null;
    	String str2 = null;
    	String lsHtmlPageNewLine = lsHtmlPage;
    	
        if (lsApprovableType.equals("ariba.core.BuysDynamicEform"))
        {
        	String lsFormAction = (String) loAppr.getFieldValue("EformChooser.ProfileName");
 		    int n = lsHtmlPage.length();
 		    Log.customer.debug("The total length of the print string is "+n);
 		    if(StringUtil.nullOrEmptyOrBlankString(lsFormAction) || !(lsFormAction.equalsIgnoreCase("A136_DGSSS")))
 		    {
  			   Log.customer.debug("Showing only the header section");
 			   str1 = lsHtmlPage.substring(0, lsHtmlPage.indexOf("<!--SectionDGSEX-->"));
 			   str2 = lsHtmlPage.substring((lsHtmlPage.indexOf("<!--ESectionDGSEX-->")), n);
 			   String lsHtmlPageNew = str1.concat(str2);
 			   return lsHtmlPageNew;
 		    }
 		    
 		   else if((lsFormAction != null && lsFormAction.equalsIgnoreCase("A136_DGSSS")))
 		   {
 			   Log.customer.debug("Now showing sections only associated with DGS eform");

 			   return lsHtmlPage;
 			   
 		   }
	}
        return lsHtmlPage;
    }

	private Hashtable getFieldValues(Approvable approvable, String lsHtmlPage) {
		// TODO Auto-generated method stub
        int liTag = 0, liTagEnd = -1;
        String lsTag = null;        
        liTag = lsHtmlPage.lastIndexOf("<!--Hdr.");
        Log.customer.debug("The value of liTag is :"+liTag);
        liTagEnd = -1;
        Object loFieldVal=null;
        while (liTag >= 0)
        {
            liTagEnd = lsHtmlPage.indexOf("-->", liTag + 1);
            lsTag = lsHtmlPage.substring(liTag + 4, liTagEnd);
            hHdrParams.put(lsTag, "[?]");
            liTag = lsHtmlPage.lastIndexOf("<!--Hdr.", liTag - 1);
        }
        Enumeration loEnumParams = hHdrParams.keys();
        while (loEnumParams.hasMoreElements())
        {
            lsTag = loEnumParams.nextElement().toString();
            String sFieldName = lsTag.substring(4);
            if(sFieldName.contains("#"))
            {
            	Log.customer.debug("Getting the label value");
            	String lsActualFieldName = sFieldName.substring(1);
            	loFieldVal = getLabel(lsActualFieldName,approvable);
            	Log.customer.debug("field value "+loFieldVal);
            }
            else if(sFieldName.contains("$"))
            {
                int liFirstIndex=sFieldName.indexOf('$');
                int liLastIndex=sFieldName.indexOf('$', liFirstIndex+1);
                String lsFieldName=sFieldName.substring(liLastIndex+2);
                String lsStringObj=sFieldName.substring(liFirstIndex+1,liLastIndex);
                loFieldVal = getFieldValue(lsFieldName,lsStringObj,approvable);
                Log.customer.debug("field value "+loFieldVal);
            }
            else if(sFieldName.startsWith("&"))
            {
            	Log.customer.debug("Getting the starting row with the starting element of the field name being &");
            	String lsActualFieldName = sFieldName.substring(1);
        	    ariba.user.core.User RequesterUser = (ariba.user.core.User)approvable.getDottedFieldValue("Requester");
        	    Partition appPartition = approvable.getPartition();
        	    ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,appPartition); 
        	    String lsClientID = (String)RequesterPartitionUser.getDottedFieldValue("ClientName.ClientID");
        	    BaseObject bo = (BaseObject)approvable;
        	    String appType = "Eform";
            	String lsProfileName = (String)approvable.getDottedFieldValue("EformChooser.ProfileName");
            	loFieldVal = approvable.getDottedFieldValue(lsActualFieldName);
                if (lsProfileName != null)
                {
                    Boolean lbVisible = BuysenseEformUIConditions.isFieldVisible(lsClientID, lsProfileName,
                            lsActualFieldName, bo, appType);

                    if (!lsActualFieldName.contains("Radio"))
                    {
                        if (!lsActualFieldName.contains("LargeText"))
                        {
                            if (lbVisible != null && lbVisible.toString().trim().equals("true") && loFieldVal != null)
                            {
                                loFieldVal = "<tr>";
                            }
                        else if(lbVisible != null && lbVisible.toString().trim().equals("true") && loFieldVal == null && lsActualFieldName.equals("EformTableHeaderTxt6"))
                          {
                                loFieldVal = "<tr>";
                          }
                            else
                            {
                                String lsClassValue = "hidden";
                                loFieldVal = "<tr class='" + lsClassValue + "'>";
                            }
                        }

                        else
                        {
                            if (lbVisible != null && lbVisible.toString().trim().equals("true"))

                            {
                                if (lsActualFieldName.equals("EformHeadLargeText"))
                                {
                                    if (approvable.getDottedFieldValue("EformHeadLargeText") == null
                                            && !StringUtil.nullOrEmptyOrBlankString((String) approvable.getDottedFieldValue("EformHeadLargeTextVal")))
                                    {

                                        loFieldVal = "<tr>";

                                    }
                                    else
                                    {
                                        String lsClassValue = "hidden";
                                        loFieldVal = "<tr class='" + lsClassValue + "'>";

                                    }
                                }                                
                                if (lsActualFieldName.equals("EformHeadLargeText1"))
                                {
                                    if (approvable.getDottedFieldValue("EformHeadLargeText1") == null
                                            && !StringUtil.nullOrEmptyOrBlankString((String) approvable.getDottedFieldValue("EformHeadLargeText1Val")))
                                    {

                                        loFieldVal = "<tr>";

                                    }
                                    else
                                    {
                                        String lsClassValue = "hidden";
                                        loFieldVal = "<tr class='" + lsClassValue + "'>";

                                    }
                                }

                                else if (lsActualFieldName.equals("EformHeadLargeText2"))
                                {
                                    if (approvable.getDottedFieldValue("EformHeadLargeText2") == null
                                            && !StringUtil.nullOrEmptyOrBlankString((String) approvable.getDottedFieldValue("EformHeadLargeText2Val")))
                                    {

                                        loFieldVal = "<tr>";

                                    }
                                    else
                                    {
                                        String lsClassValue = "hidden";
                                        loFieldVal = "<tr class='" + lsClassValue + "'>";

                                    }
                                }

                                else if (lsActualFieldName.equals("EformHeadLargeText3"))
                                {
                                    if (approvable.getDottedFieldValue("EformHeadLargeText3") == null
                                            && !StringUtil.nullOrEmptyOrBlankString((String) approvable.getDottedFieldValue("EformHeadLargeText3Val")))
                                    {

                                        loFieldVal = "<tr>";

                                    }
                                    else
                                    {
                                        String lsClassValue = "hidden";
                                        loFieldVal = "<tr class='" + lsClassValue + "'>";

                                    }
                                }
                                else if (lsActualFieldName.equals("EformHeadLargeText4"))
                                {
                                    if (approvable.getDottedFieldValue("EformHeadLargeText4") == null
                                            && !StringUtil.nullOrEmptyOrBlankString((String) approvable.getDottedFieldValue("EformHeadLargeText4Val")))
                                    {

                                        loFieldVal = "<tr>";

                                    }
                                    else
                                    {
                                        String lsClassValue = "hidden";
                                        loFieldVal = "<tr class='" + lsClassValue + "'>";

                                    }
                                }
                                else if (lsActualFieldName.equals("EformAcctLargeText"))
                                {
                                    if (approvable.getDottedFieldValue("EformAcctLargeText") == null
                                            && !StringUtil.nullOrEmptyOrBlankString((String) approvable.getDottedFieldValue("EformAcctLargeTextVal")))
                                    {

                                        loFieldVal = "<tr>";

                                    }
                                    else
                                    {
                                        String lsClassValue = "hidden";
                                        loFieldVal = "<tr class='" + lsClassValue + "'>";

                                    }
                                }
                                else if (lsActualFieldName.equals("EformAcctLargeText1"))
                                {
                                    if (approvable.getDottedFieldValue("EformAcctLargeText1") == null
                                            && !StringUtil.nullOrEmptyOrBlankString((String) approvable.getDottedFieldValue("EformAcctLargeText1Val")))
                                    {

                                        loFieldVal = "<tr>";

                                    }
                                    else
                                    {
                                        String lsClassValue = "hidden";
                                        loFieldVal = "<tr class='" + lsClassValue + "'>";

                                    }
                                }
                                else if (lsActualFieldName.equals("EformAcctLargeText2"))
                                {
                                    if (approvable.getDottedFieldValue("EformAcctLargeText2") == null
                                            && !StringUtil.nullOrEmptyOrBlankString((String) approvable.getDottedFieldValue("EformAcctLargeText2Val")))
                                    {

                                        loFieldVal = "<tr>";

                                    }
                                    else
                                    {
                                        String lsClassValue = "hidden";
                                        loFieldVal = "<tr class='" + lsClassValue + "'>";

                                    }
                                }
                                else if (lsActualFieldName.equals("EformAcctLargeText3"))
                                {
                                    if (approvable.getDottedFieldValue("EformAcctLargeText3") == null
                                            && !StringUtil.nullOrEmptyOrBlankString((String) approvable.getDottedFieldValue("EformAcctLargeText3Val")))
                                    {

                                        loFieldVal = "<tr>";

                                    }
                                    else
                                    {
                                        String lsClassValue = "hidden";
                                        loFieldVal = "<tr class='" + lsClassValue + "'>";

                                    }
                                }
                                else if (lsActualFieldName.equals("EformAcctLargeText4"))
                                {
                                    if (approvable.getDottedFieldValue("EformAcctLargeText4") == null
                                            && !StringUtil.nullOrEmptyOrBlankString((String) approvable.getDottedFieldValue("EformAcctLargeText4Val")))
                                    {

                                        loFieldVal = "<tr>";

                                    }
                                    else
                                    {
                                        String lsClassValue = "hidden";
                                        loFieldVal = "<tr class='" + lsClassValue + "'>";

                                    }
                                }
                                else if (lsActualFieldName.equals("EformAcctLargeText5"))
                                {
                                    if (approvable.getDottedFieldValue("EformAcctLargeText5") == null
                                            && !StringUtil.nullOrEmptyOrBlankString((String) approvable.getDottedFieldValue("EformAcctLargeText5Val")))
                                    {

                                        loFieldVal = "<tr>";

                                    }
                                    else
                                    {
                                        String lsClassValue = "hidden";
                                        loFieldVal = "<tr class='" + lsClassValue + "'>";

                                    }
                                }

                            }
                            else
                            {
                                String lsClassValue = "hidden";
                                loFieldVal = "<tr class='" + lsClassValue + "'>";

                            }
                        }

                    }

                    else
                    {
                        if (lbVisible != null && lbVisible.toString().trim().equals("true"))

                        {
                            if (lsActualFieldName.equals("EformHeadGrp1RadioHeader"))
                            {
                                if (approvable.getDottedFieldValue("EformHeadGrp1RadioHeader") == null
                                        && approvable.getDottedFieldValue("EformHeadGrp1RadioVal") != null)
                                {

                                    loFieldVal = "<tr>";
                                }
                                else
                                {
                                    String lsClassValue = "hidden";
                                    loFieldVal = "<tr class='" + lsClassValue + "'>";
                                }
                            }

                            else if (lsActualFieldName.equals("EformHeadGrp2RadioHeader"))
                            {
                                if (approvable.getDottedFieldValue("EformHeadGrp2RadioHeader") == null
                                        && approvable.getDottedFieldValue("EformHeadGrp2RadioVal") != null)
                                {
                                    loFieldVal = "<tr>";
                                }
                                else
                                {
                                    String lsClassValue = "hidden";
                                    loFieldVal = "<tr class='" + lsClassValue + "'>";
                                }
                            }
                            else if (lsActualFieldName.equals("EformHeadGrp_1RadioHeader"))
                            {
                                if (approvable.getDottedFieldValue("EformHeadGrp_1RadioHeader") == null
                                        && approvable.getDottedFieldValue("EformHeadGrp_1RadioHeaderVal") != null)
                                {
                                    loFieldVal = "<tr>";
                                }
                                else
                                {
                                    String lsClassValue = "hidden";
                                    loFieldVal = "<tr class='" + lsClassValue + "'>";
                                }
                            }
                            else if (lsActualFieldName.equals("EformHeadGrp_2RadioHeader"))
                            {
                                if (approvable.getDottedFieldValue("EformHeadGrp_2RadioHeader") == null
                                        && approvable.getDottedFieldValue("EformHeadGrp_2RadioHeaderVal") != null)
                                {
                                    loFieldVal = "<tr>";
                                }
                                else
                                {
                                    String lsClassValue = "hidden";
                                    loFieldVal = "<tr class='" + lsClassValue + "'>";
                                }
                            }
                            else if (lsActualFieldName.equals("EformHeadGrp_3RadioHeader"))
                            {
                                if (approvable.getDottedFieldValue("EformHeadGrp_3RadioHeader") == null
                                        && approvable.getDottedFieldValue("EformHeadGrp_3RadioHeaderVal") != null)
                                {
                                    loFieldVal = "<tr>";
                                }
                                else
                                {
                                    String lsClassValue = "hidden";
                                    loFieldVal = "<tr class='" + lsClassValue + "'>";
                                }
                            }
                            else if (lsActualFieldName.equals("EformHeadGrp_4RadioHeader"))
                            {
                                if (approvable.getDottedFieldValue("EformHeadGrp_4RadioHeader") == null
                                        && approvable.getDottedFieldValue("EformHeadGrp_4RadioHeaderVal") != null)
                                {
                                    loFieldVal = "<tr>";
                                }
                                else
                                {
                                    String lsClassValue = "hidden";
                                    loFieldVal = "<tr class='" + lsClassValue + "'>";
                                }
                            }
                            else if (lsActualFieldName.equals("EformHeadGrp_5RadioHeader"))
                            {
                                if (approvable.getDottedFieldValue("EformHeadGrp_5RadioHeader") == null
                                        && approvable.getDottedFieldValue("EformHeadGrp_5RadioHeaderVal") != null)
                                {
                                    loFieldVal = "<tr>";
                                }
                                else
                                {
                                    String lsClassValue = "hidden";
                                    loFieldVal = "<tr class='" + lsClassValue + "'>";
                                }
                            }
                            else if (lsActualFieldName.equals("EformHeadGrp_6RadioHeader"))
                            {
                                if (approvable.getDottedFieldValue("EformHeadGrp_6RadioHeader") == null
                                        && approvable.getDottedFieldValue("EformHeadGrp_6RadioHeaderVal") != null)
                                {
                                    loFieldVal = "<tr>";
                                }
                                else
                                {
                                    String lsClassValue = "hidden";
                                    loFieldVal = "<tr class='" + lsClassValue + "'>";
                                }
                            }
                            else if (lsActualFieldName.equals("EformHeadGrp_7RadioHeader"))
                            {
                                if (approvable.getDottedFieldValue("EformHeadGrp_7RadioHeader") == null
                                        && approvable.getDottedFieldValue("EformHeadGrp_7RadioHeaderVal") != null)
                                {
                                    loFieldVal = "<tr>";
                                }
                                else
                                {
                                    String lsClassValue = "hidden";
                                    loFieldVal = "<tr class='" + lsClassValue + "'>";
                                }
                            }

                            else if (loFieldVal != null)
                            {
                                loFieldVal = "<tr>";
                            }
                            else
                            {
                                String lsClassValue = "hidden";
                                loFieldVal = "<tr class='" + lsClassValue + "'>";
                            }

                        }
                        else
                        {
                            String lsClassValue = "hidden";
                            loFieldVal = "<tr class='" + lsClassValue + "'>";
                        }

                    }
                }
                else
                {
                    String lsClassValue = "hidden";
                    loFieldVal = "<tr class='" + lsClassValue + "'>";
                }
            }
            else if((sFieldName.equals("EformTable1Field5") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable1Field6") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable1Field2_3") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformMoney")       && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformMoney1")       && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable2Field2_3") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable2Field5") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable2Field6") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformAcctMoney1")   && approvable.getDottedFieldValue(sFieldName) != null))
                {
                    Money loProcMoney=(Money)approvable.getDottedFieldValue(sFieldName);
                    loProcMoney = new Money(BigDecimalFormatter.round(loProcMoney.getAmount(), 2), loProcMoney.getCurrency());
                    loFieldVal= (loProcMoney.asString()).substring(0, (loProcMoney.asString().indexOf(".")+3)).concat(" ").concat(loProcMoney.getCurrency().getSuffix());
                    Log.customer.debug("Value of ProcurementMoney field  : "+loFieldVal);
                }
            else if((sFieldName.equals("EformTable3Field5") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable3Field2_3") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable3Field6") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable4Field2_3") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable5Field2_3") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable4Field5") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable4Field6") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable5Field5") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable5Field6") && approvable.getDottedFieldValue(sFieldName) != null))
                {
                    Money loProcMoney=(Money)approvable.getDottedFieldValue(sFieldName);
                    loProcMoney = new Money(BigDecimalFormatter.round(loProcMoney.getAmount(), 2), loProcMoney.getCurrency());
                    loFieldVal= (loProcMoney.asString()).substring(0, (loProcMoney.asString().indexOf(".")+3)).concat(" ").concat(loProcMoney.getCurrency().getSuffix());
                    Log.customer.debug("Value of ProcurementMoney field  : "+loFieldVal);
                } 
            else if((sFieldName.equals("EformTable6Field5") && approvable.getDottedFieldValue(sFieldName) != null) ||
                    (sFieldName.equals("EformTable6Field2_3") && approvable.getDottedFieldValue(sFieldName) != null) ||
                    (sFieldName.equals("EformTable7Field2_3") && approvable.getDottedFieldValue(sFieldName) != null) ||
                    (sFieldName.equals("EformTable8Field2_3") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable7Field5") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable7Field6") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable8Field5") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable8Field6") && approvable.getDottedFieldValue(sFieldName) != null))
                {
                    Money loProcMoney=(Money)approvable.getDottedFieldValue(sFieldName);
                    loProcMoney = new Money(BigDecimalFormatter.round(loProcMoney.getAmount(), 2), loProcMoney.getCurrency());
                    loFieldVal= (loProcMoney.asString()).substring(0, (loProcMoney.asString().indexOf(".")+3)).concat(" ").concat(loProcMoney.getCurrency().getSuffix());
                    Log.customer.debug("Value of ProcurementMoney field  : "+loFieldVal);
                } 
            else if((sFieldName.equals("EformTable9Field5")  && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable9Field6")  && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable9Field6")  && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable9Field2_3") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable10Field2_3") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable10Field5") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable10Field6") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("TotalCost") && approvable.getDottedFieldValue(sFieldName) != null))
                {
                    Money loProcMoney=(Money)approvable.getDottedFieldValue(sFieldName);
                    loProcMoney = new Money(BigDecimalFormatter.round(loProcMoney.getAmount(), 2), loProcMoney.getCurrency());
                    loFieldVal= (loProcMoney.asString()).substring(0, (loProcMoney.asString().indexOf(".")+3)).concat(" ").concat(loProcMoney.getCurrency().getSuffix());
                    Log.customer.debug("Value of ProcurementMoney field  : "+loFieldVal);
                }            
            else if((sFieldName.equals("EformTable1Field13") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable2Field13") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable3Field13") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable4Field13") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable5Field13") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable6Field13") && approvable.getDottedFieldValue(sFieldName) != null))
            {
            	ariba.util.core.Date loDateValue = (ariba.util.core.Date) approvable.getFieldValue(sFieldName);
            	String lsDateValue = loDateValue.toDateMonthYearString();
            	loFieldVal = lsDateValue;
            	Log.customer.debug("The concise date format for the entered date is:"+loFieldVal);
            }
            else if((sFieldName.equals("EformTable7Field13") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable8Field13") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable9Field13") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformTable10Field13") && approvable.getDottedFieldValue(sFieldName) != null) ||
            		(sFieldName.equals("EformAcctDate1") && approvable.getDottedFieldValue(sFieldName) != null))
            {
            	ariba.util.core.Date loDateValue = (ariba.util.core.Date) approvable.getFieldValue(sFieldName);
            	String lsDateValue = loDateValue.toDateMonthYearString();
            	loFieldVal = lsDateValue;
            	Log.customer.debug("The concise date format for the entered date is:"+loFieldVal);            	
            }
            else if(sFieldName.contains("Preparer") || sFieldName.contains("Requester"))
                {
                    String lsObjval=sFieldName.substring(0,sFieldName.indexOf("@"));
                    Log.customer.debug("Object value: "+lsObjval);
                    String lsFieldVal=sFieldName.substring(sFieldName.indexOf("@")+1);
                    Log.customer.debug("field value: "+lsFieldVal);
                    loFieldVal=getReqValue(lsObjval,lsFieldVal,approvable);
                }
            else if(sFieldName.equals("username"))
                {
                    String loStatus=(String)approvable.getDottedFieldValue("StatusString");
                    if(loStatus.equals("Denied"))
                    {
                        loFieldVal="Denied by:";
                    }
                    else
                    {
                        loFieldVal="Approved by:";
                    }
                }
            else if(sFieldName.contains("EformChooser.Name"))
            {
            	String lsEformName = (String)approvable.getDottedFieldValue(sFieldName);
            	if(StringUtil.nullOrEmptyOrBlankString(lsEformName))
            	{
            		loFieldVal = "eVA Eform";
            	}
            	else
            	{
            		loFieldVal = lsEformName;
            	}
            }
            else if(sFieldName.equals("date"))
                {
                    String loStatus=(String)approvable.getDottedFieldValue("StatusString");
                    if(loStatus.equals("Denied"))
                    {
                        loFieldVal="Denied Date:";
                    }
                    else
                    {
                        loFieldVal="Approved Date:";
                    }
                }
            else{ 
            	
            	loFieldVal = approvable.getDottedFieldValue(sFieldName);
            	if((sFieldName.equals("EformHeadCB") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
               		(sFieldName.equals("EformHeadCB1") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
               		(sFieldName.equals("EformHeadCB2") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
               		(sFieldName.equals("EformHeadCB3") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
               		(sFieldName.equals("EformHeadCB4") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
               		(sFieldName.equals("EformHeadCB5") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                    (sFieldName.equals("EformHeadCB6") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                    (sFieldName.equals("EformHeadCB7") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
               		(sFieldName.equals("EformAcctCB1") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
               		(sFieldName.equals("EformAcctCB2") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
               		(sFieldName.equals("EformAcctCB3") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
               		(sFieldName.equals("EformAcctCB4") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
               		(sFieldName.equals("EformAcctCB5") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
               		(sFieldName.equals("EformAcctCB6") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
               		(sFieldName.equals("EformAcctCB7") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
               		(sFieldName.equals("EformAcctCB8") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
               		(sFieldName.equals("EformAcctCB9") && loFieldVal != null && loFieldVal.toString().trim().equals("true")))
               {
               	Log.customer.debug("converting boolean true to yes for field name :"+sFieldName);	
                 loFieldVal="Yes";
               }
               else if((sFieldName.equals("EformTable1Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                       (sFieldName.equals("EformTable2Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                       (sFieldName.equals("EformTable3Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                       (sFieldName.equals("EformTable4Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                       (sFieldName.equals("EformTable5Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                       (sFieldName.equals("EformTable6Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                       (sFieldName.equals("EformTable7Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                       (sFieldName.equals("EformTable8Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||
                       (sFieldName.equals("EformTable9Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("true"))||  
               		   (sFieldName.equals("EformTable10Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("true")))
               {
               	  Log.customer.debug("converting boolean true to yes for field name :"+sFieldName);	
                   loFieldVal="Yes";                       	
               }
               	
               else if((sFieldName.equals("EformHeadCB") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
               		(sFieldName.equals("EformHeadCB1") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
               		(sFieldName.equals("EformHeadCB2") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
               		(sFieldName.equals("EformHeadCB3") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
               		(sFieldName.equals("EformHeadCB4") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
               		(sFieldName.equals("EformHeadCB5") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
               		(sFieldName.equals("EformHeadCB6") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
               		(sFieldName.equals("EformHeadCB7") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
               		(sFieldName.equals("EformAcctCB1") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
               		(sFieldName.equals("EformAcctCB2") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
               		(sFieldName.equals("EformAcctCB3") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
               		(sFieldName.equals("EformAcctCB4") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
               		(sFieldName.equals("EformAcctCB5") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
               		(sFieldName.equals("EformAcctCB6") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
               		(sFieldName.equals("EformAcctCB7") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
               		(sFieldName.equals("EformAcctCB8") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
               		(sFieldName.equals("EformAcctCB9") && loFieldVal != null && loFieldVal.toString().trim().equals("false")))
               {
               	 Log.customer.debug("converting boolean false to no for field name :"+sFieldName);	
                  loFieldVal="No";
               }
               else if((sFieldName.equals("EformTable1Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                       (sFieldName.equals("EformTable2Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                       (sFieldName.equals("EformTable3Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                       (sFieldName.equals("EformTable4Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                       (sFieldName.equals("EformTable5Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                       (sFieldName.equals("EformTable6Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                       (sFieldName.equals("EformTable7Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                       (sFieldName.equals("EformTable8Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                       (sFieldName.equals("EformTable9Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("false"))||
                       (sFieldName.equals("EformTable10Field1") && loFieldVal != null && loFieldVal.toString().trim().equals("false")))
               {
               	  Log.customer.debug("converting boolean false to no for field name :"+sFieldName);	
                   loFieldVal="No";                       	
               }                        
            }

            if (loFieldVal == null)
            {
                loFieldVal = "";
            }
            Log.customer.debug("Adding the value to hHdrParams: The value to input now is :" +lsTag+ "and field values is "+loFieldVal);
            hHdrParams.put(lsTag, String.valueOf(loFieldVal));
        }
        return hHdrParams;
        
	}

	private Object getLabel(String lsActualFieldName, Approvable approvable) {

		Log.customer.debug("Inside getLabel method ");
		FieldProperties lfpFieldProperty1 = approvable.getFieldProperties(lsActualFieldName);
		String lsLabel = lfpFieldProperty1.getLabel();
        if(!(StringUtil.nullOrEmptyOrBlankString(lsLabel)))
        {
            Log.customer.debug("Inside the if block " +lsLabel.length());
            if(lsLabel.contains(":"))
            {
            int in = lsLabel.lastIndexOf(":");
            if(in >= 0)
            {
		     String lsLabelWtCln = lsLabel.substring(0,(lsLabel.length() - 1));
	         Log.customer.debug("New label " +lsLabelWtCln);
		     return lsLabelWtCln;
            }
          } 
        }
		return lsLabel;
	}

	public String getFieldValue(String fieldName,String stringObj,Approvable approvable)
    {
        Log.customer.debug("Inside getFieldValue method, fieldName :"+fieldName);
        String lsResultValue="";
        List loObjlist=null;
        Object loObjectValue=null;
        if(stringObj.equalsIgnoreCase("ApprovalRequests"))
        {
            loObjlist = getAllApprovalRequests(approvable);
        }
        else
        {
            loObjlist = (List)approvable.getFieldValue(stringObj);
        }
        Log.customer.debug("list:" +loObjlist);
        BaseSession loSession = Base.getSession();
        for (int i = 0; i < loObjlist.size(); i++)
        {
            if(i>0)
            {
                if(fieldName.equals("Text"))
                {
                    lsResultValue=lsResultValue+"<br>";
                }
                else if(!loObjectValue.equals(""))
                    {
                        lsResultValue=lsResultValue+", ";
                    }
            }
            Object obj = loObjlist.get(i);
            BaseObject loObjectName = null;
            try
            {
                if(obj instanceof Comment)
                {
                    Comment loCmt = (Comment)loObjlist.get(i);
                    loObjectName = loCmt;
                }
                else if(obj instanceof ApprovalRequest)
                    {
                        ApprovalRequest loAppReq=(ApprovalRequest)loObjlist.get(i);
                        loObjectName = loAppReq;
                    }
                else
                    {
                        BaseId loId = (BaseId)loObjlist.get(i);
                        loObjectName = loSession.objectFromId(loId);
                    }
                Log.customer.debug("Inside for of getFieldValue, object:"+loObjectName);

                if(fieldName.equals("Text"))
                {
                    LongString llsText = (LongString) loObjectName.getDottedFieldValue(fieldName);
                    if(llsText != null)
                    {
                        loObjectValue = (String) llsText.string();
                    }
                }
                else
                    {
                        loObjectValue = loObjectName.getDottedFieldValue(fieldName);
                    }
                Log.customer.debug("field name " +loObjectValue);
                if (loObjectValue == null)
                    loObjectValue = ""; // replace NullValue with blank.
                lsResultValue=lsResultValue+String.valueOf(loObjectValue);
            }
            catch(Exception e)
            {
                Log.customer.debug("Excception: "+e.getMessage());
                lsResultValue = "EXCEPTION";
            }
        }
        return lsResultValue;
    }

    public String getReqValue(String objName,String fieldName,Approvable approvable)
    {
        ariba.user.core.User loUser = (ariba.user.core.User)approvable.getDottedFieldValue(objName);
        ariba.common.core.User loPartitionUser = ariba.common.core.User.getPartitionedUser(loUser,approvable.getPartition());
        Object loFieldValue = loPartitionUser.getDottedFieldValue(fieldName);
        if (loFieldValue == null)
        {
            loFieldValue = "";
        }
        Log.customer.debug("Object value in getReqValue mathod: " +loFieldValue);
        String lsResultValue=loFieldValue.toString();
        return lsResultValue;
    }
    public List getAllApprovalRequests(Approvable approvable)
    {
        List lvApprovalRequestsVector;
        List lvecApprovalRequests;
        ApprovalRequest loApprovalRequest;

        lvApprovalRequestsVector = (List)approvable.getFieldValue("ApprovalRequests");
        lvecApprovalRequests = ListUtil.cloneList(lvApprovalRequestsVector);
        for (int i = 0; i < lvecApprovalRequests.size(); i ++)
        {
            loApprovalRequest = (ApprovalRequest) lvecApprovalRequests.get(i);
            lvApprovalRequestsVector = getDependencies(lvecApprovalRequests, loApprovalRequest);
        }
        return lvecApprovalRequests;
    }

    public List getDependencies(List lvec, BaseObject appReq)
    {
        List vecNewDependencies = ListUtil.list();
        ApprovalRequest newDependency = new ApprovalRequest();
        vecNewDependencies = (List)appReq.getFieldValue("Dependencies");
        int size = vecNewDependencies.size();

        for (int i = 0; i < size; i ++)
        {
            newDependency = (ApprovalRequest) vecNewDependencies.get(i);
            if (lvec.contains(newDependency))
            {
                continue;
            }
            else
            {
                lvec.add(newDependency);
                getDependencies(lvec, newDependency);
            }
        }
        return lvec;
    }
}

