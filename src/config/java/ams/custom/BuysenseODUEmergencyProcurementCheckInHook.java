//package config.java.ams.custom;
package config.java.ams.custom;

import java.util.List;

import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableHook;
import ariba.base.fields.FieldProperties;
import ariba.util.core.Constants;
import ariba.util.core.ListUtil;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseODUEmergencyProcurementCheckInHook implements ApprovableHook
{
private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));
public List run(Approvable approvable)
{
    Approvable loAppr = approvable;
    
    Log.customer.debug("Inside BuysenseODUSoleSourceCheckinHook");
    
    Boolean lbRefire = (Boolean)loAppr.getFieldValue("BuysenseReFireWorkflow");
    FieldProperties lfpFieldProperty = loAppr.getFieldProperties("BuysenseReFireWorkflow");
    
    //We are setting the field property 'NULLVALUENAME' for field 'BuysenseReFireWorkflow' based on the value in the 'BuysenseReFireWorkflow'.
    //If the value is 'true' then we are setting the property value to 'Checked:RefireWorkFLow'. If the value is null or 'false' we are setting
    //the value to 'Checked:NoRefireWorkFlow'
    //We are then using this property value to validate whether to re-fire the approval flow or not.
    
    if(lbRefire != null && lbRefire.booleanValue() == true)
    { 
        lfpFieldProperty.setPropertyForKey("nullValueName", "Checked:RefireWorkFLow");
        loAppr.setFieldValue("BuysenseReFireWorkflow", new Boolean("false"));
    }
    else if(lbRefire == null ||(lbRefire != null && lbRefire.booleanValue() == false))
    { 
        lfpFieldProperty.setPropertyForKey("nullValueName", "Checked:NoRefireWorkFlow"); 
    }
    String lsDepartment = (String)approvable.getDottedFieldValue("Department.UniqueName");
    String lsVendor = (String)approvable.getFieldValue("Vendor");
    String lsDescriptionOfEmergencyVal = (String)approvable.getFieldValue("DescriptionOfEmergencyVal");
    String lsDateOfEmergencyVal = (String)approvable.getFieldValue("DateOfEmergencyVal");
    String lsCauseOfEmergencyVal = (String)approvable.getFieldValue("CauseOfEmergencyVal");
    String lsHowEmergencyArisedVal = (String)approvable.getFieldValue("HowEmergencyArisedVal");

    if(StringUtil.nullOrEmptyOrBlankString(lsDepartment) || StringUtil.nullOrEmptyOrBlankString(lsVendor) || StringUtil.nullOrEmptyOrBlankString(lsDescriptionOfEmergencyVal) || StringUtil.nullOrEmptyOrBlankString(lsHowEmergencyArisedVal)|| StringUtil.nullOrEmptyOrBlankString(lsDateOfEmergencyVal) || StringUtil.nullOrEmptyOrBlankString(lsCauseOfEmergencyVal))
    {
        //loAppr.EditRequiresReapprovals;
        return ListUtil.list(Constants.getInteger(-1),
              "The values for the mandatory field(s) must be set.");
    }
  else
  {
      return NoErrorResult;
  }
}
}
