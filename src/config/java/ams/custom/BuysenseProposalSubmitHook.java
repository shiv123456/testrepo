package config.java.ams.custom;

import java.util.List;

import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableHook;
import ariba.base.core.Base;
import ariba.collaboration.core.CollaborationLineItem;
import ariba.collaboration.core.Proposal;
import ariba.procure.core.CategoryLineItemDetails;
import ariba.util.core.Constants;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

/**
 * CSPL #: 1794
 * @author Sarath Babu Garre
 * Date  : 18-May-2010
 * @version 1.0
 * Explanation: In the implementation of ACP Consulting module, we included few required fields in the proposal.
 *              But the field validations are not getting fried for our custom required fields, and supplier is able
 *              to submit the proposal without filling in the required fields.
 *              So, we added a submithook to throw an error if any of the required fields are not filled in.
 *                           
 * CSPL #: 1803
 * @author Sarath Babu Garre
 * Date  : 18-May-2010
 * Explanation: Supplier is able to add non-contatalog items to a proposal. Addded a condition to this class to check
 *              if the proposal contains non-catalog line and throws an error if has.                  
 */
public class BuysenseProposalSubmitHook implements ApprovableHook
{
    private static final List NoErrorResult      = ListUtil.list(Constants.getInteger(0));
    private static String[]   HeaderFields       = { "SupplierContactName", "SupplierTelephone", "SupplierEmail",
            "SupplierTitle", "InsuranceCoverage", "ProposalValidity", "ContractorTermAndCondition" };
    private static final List HeaderFieldsVector = ListUtil.arrayToList(HeaderFields);

    public List run(Approvable approvable)
    {
        Proposal proposal = (Proposal) approvable;
        Log.customer.debug("BuysenseProposalSubmitHook.java called" + proposal);
        List lvCollabLineItems = (List) proposal.getFieldValue("LineItems");
        if (lvCollabLineItems.size() >= 1)
        {
            Log.customer.debug("Inside Collab Line Items" + lvCollabLineItems.get(0));
            for (int p = 0; p < lvCollabLineItems.size(); p++)
            {
                CategoryLineItemDetails cLID = ((CollaborationLineItem) lvCollabLineItems.get(p))
                        .getCategoryLineItemDetails();
                if (cLID != null)
                {
                    String sConsultingLine = cLID.toString();
                    if (sConsultingLine.indexOf("ConsultingLineItemDetails") >= 0)
                    {
                        if ((!proposalRequiredFields((CollaborationLineItem) lvCollabLineItems.get(p),
                                HeaderFieldsVector)))
                        {
                            Log.customer.debug("All Required Proposal Header Fields are not filled");
                            return ListUtil.list(Constants.getInteger(-1), Fmt.Sil(Base.getSession().getLocale(),"aml.Consulting",
                                    "ProposalHeaderValidation"));
                        }
                    }
                }
                else if (((CollaborationLineItem) lvCollabLineItems.get(p)).getIsAdHoc())
                {
                    return ListUtil.list(Constants.getInteger(-1), Fmt.Sil(Base.getSession().getLocale(),"aml.Consulting",
                            "ProposalAdhocLineValidation"));
                }
            }
            return NoErrorResult;
        }
        return NoErrorResult;
    }

    public boolean proposalRequiredFields(CollaborationLineItem lvCollabLineItem, List fieldlist)
    {
        CategoryLineItemDetails cLLD = lvCollabLineItem.getCategoryLineItemDetails();
        Log.customer.debug("collabLineitem " + cLLD);
        for (int i = 0; i < fieldlist.size(); i++)
        {
            Object obj = cLLD.getDottedFieldValue("SharedGlobalProperties." + (String) fieldlist.get(i));
            if (obj == null)
            {
                return false;
            }
            else if (obj != null && obj instanceof String)
            {
                if (StringUtil.nullOrEmptyOrBlankString((String) obj))
                {
                    return false;
                }
            }
            else if (obj != null && obj instanceof Boolean
                    && ((String) fieldlist.get(i)).equals("ContractorTermAndCondition"))
            {
                if (!((Boolean) obj).booleanValue())
                {
                    return false;
                }
            }
        }
        return true;
    }
}
