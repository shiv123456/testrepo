package config.java.ams.custom;

import java.util.List;

import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.fields.ValidChoices;
import ariba.htmlui.baseui.Log;
import ariba.util.core.ListUtil;

public class BuysenseEformHeadGrp3RadioHeaderValidChoices extends ValidChoices
{
    public static final String ClassName = "BuysenseEformHeadGrp3RadioHeaderValidChoices";

    public boolean useOrderedChoices()
    {
        return true;
    }

    @Override
    protected Object choices()
    {
        List<String> choices = ListUtil.list();

        Object cobj = getValueSourceContext();
        Log.customer.debug(ClassName + " cobj " + cobj);
        if (cobj instanceof ClusterRoot)
        {
            ClusterRoot loEfromCR = (ClusterRoot) cobj;
            List<String> loRadioValues = getRadioFieldVAlues(loEfromCR);
            if (loRadioValues != null && !loRadioValues.isEmpty())
            {
                for (int i = 0; i < loRadioValues.size(); i++)
                {
                    String sRadioFieldVal = loRadioValues.get(i);
                    Log.customer.debug(ClassName + " sRaidFieldVal " + sRadioFieldVal);
                    choices.add(sRadioFieldVal);
                }
            }
        }

        Log.customer.debug(ClassName + "choices() " + choices);
        return choices;
    }

    private List<String> getRadioFieldVAlues(ClusterRoot cobj)
    {
        List<String> lsFieldValues = ListUtil.list();
        
        String sClientID = (String) cobj.getDottedFieldValue("EformChooser.ClientID");
        String sProfName = (String) cobj.getDottedFieldValue("EformChooser.ProfileName");
        String sFieldName = "EformHeadGrp_3RadioHeaderVal";
        
        String queryText = "Select Description, FieldName, UniqueName from ariba.core.BuysEformFieldDataTable where " +
        "ClientID = '" + sClientID + "' AND ProfileName='"+ sProfName + "' " +
        "AND FieldName like '" + sFieldName + "%' Order by UniqueName ";
        
        AQLQuery query = AQLQuery.parseQuery(queryText);
        Log.customer.debug(ClassName + " query: " + query);
        Partition pcsv = Base.getService().getPartition("pcsv");
        AQLResultCollection rc = Base.getService().executeQuery(query, new AQLOptions(pcsv));

        if (rc != null && rc.getFirstError() == null)
        {
            while (rc.next())
            {
                String s = rc.getString("Description");
                Log.customer.debug(ClassName + " Name: " + s);
                lsFieldValues.add(s);
            }

        }

        return lsFieldValues;
    }

}
