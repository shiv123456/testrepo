package config.java.ams.custom;

import java.math.BigDecimal;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseContractApprovalFilterRuleJSUtil
{

    public static boolean removeRule(String contractNum, String userUniqueName, String userBSO, String sClientName,
            String reqTotalAmount, String currentRuleToCheck)
    {
        // EVA001:A194DGS:ContractApprovalOverride:ApproverAmount:5000|bsmith|BSO|ContracctNum
        // start get BFTD table data
        try
        {
            String sQuery = "SELECT b.UniqueName,b from ariba.core.BuysenseFieldDataTable b where b.UniqueName like \'"
                    + sClientName + ":ContractApprovalOverride:" + currentRuleToCheck + ":%\' Order by b.Name";
            AQLQuery aqlQuery = AQLQuery.parseQuery(sQuery);
            Partition partition = Base.getService().getPartition("pcsv");
            AQLOptions aqlOptions = new AQLOptions(partition);
            Log.customer.debug("aqlQuery " +aqlQuery);			
            ariba.util.javascript.Log.javascript.debug("aqlQuery: " + aqlQuery);
            AQLResultCollection aqlResults = Base.getService().executeQuery(aqlQuery, aqlOptions);
            if (aqlResults.getErrors() == null)
            {
                while (aqlResults.next())
                {
                    String sUN = aqlResults.getString(0);
                    BaseId sBID = aqlResults.getBaseId(1);
                    ariba.util.javascript.Log.javascript.debug(" query results: sUN=" + sUN + ";sBID" + sBID);
                    String[] splitUniqueName = sUN.split(":");
                    // This will pick only - 5000|bsmith|BSO|ContracctNum
                    // from
                    // EVA001:A194DGS:ContractApprovalOverride:ApproverAmount:5000|bsmith|BSO|ContracctNum
                    String[] splitValues = splitUniqueName[4].split("\\|");
                    if(splitValues.length <3)
                    {
                        Log.customer.warning(8888,"unexpected data and current record is not checked");
                        continue;
                    }
                    // if any of the criteria doesn't match then continue
                    // check amount - splitValues[0] // if amount is null
                    // then it means that no dollar limit applies; same as
                    // '*'
                    // check user - splitValues[1] // if user is null then
                    // it means that same as '*' (any user)
                    // check BSO - splitValues[2] // if BSO is null then it
                    // means same as '*' (any BSO)
                    // check Contract Number - splitValues[3] // if Contract
                    // number is null then it means same as '*' (any
                    // contract number)

                    // check amount
                    // check only if value is NOT (null or '*'). If it is
                    // null or '*' , it means any values is valid
                    if (!(StringUtil.nullOrEmptyOrBlankString(splitValues[0]) || splitValues[0].trim().equals("*")))
                    {
                       BigDecimal loReqTotalAmount = new BigDecimal(reqTotalAmount);
                       BigDecimal loBFDTsplitUniqueName = new BigDecimal(splitValues[0]);

                        if (loReqTotalAmount.compareTo(loBFDTsplitUniqueName) > 0)
                         {
                           continue; // this rule does not match in terms
                           // of amount. Jump to next rule
                         }

                    }
                    // amount criteria passed

                    // user
                    
                    if(!isMatched(userUniqueName, splitValues[1].trim()))
                    {
                        continue;
                    }
                    // user criteria passed

                    // BSO
                    if(!isMatched(userBSO, splitValues[2].trim()))
                    {
                        continue;
                    }
                    // BSO criteria passed

                    // Contract number
                    if(!isMatched(contractNum.toUpperCase(), (splitValues.length==3? "*":splitValues[3].trim().toUpperCase())))
                    {
                        continue;
                    }                    
                    // Contract number criteria passed

                    // if all of them match then return success to remove
                    // this approver
                    // return 'true' in that case. If not returned from here
                    // then return false outside the loop

                    return true;
                }
            }

        }
        catch (Exception e)
        {
            Log.customer.warning(8888,
                    "Exception in BuysenseContractApprovalFilterRuleJSUtil.removeRule() method" + e.getMessage());
            ariba.util.javascript.Log.javascript
                    .debug("Exception in BuysenseContractApprovalFilterRuleJSUtil.removeRule() method "
                            + e.getMessage());
        }
        return false;
    }
//This method is CASE SENSITIVE and returns true if both strings match or if dbString is null or blank or equals to * OR reqString starts with dbString
    private static boolean isMatched(String reqString, String dbString)
    {
        boolean bMatched = false;

        if(reqString.equals(dbString))
        {
            bMatched = true;
        }
        if(!bMatched && (StringUtil.nullOrEmptyOrBlankString(dbString) || (!StringUtil.nullOrEmptyOrBlankString(dbString) && dbString.trim().equals("*")) ))
        {
            bMatched = true;
        }
        //partial wild card condition
        if(!bMatched && dbString.trim().contains("*"))
        {
            String sTemp = dbString.trim().substring(0, dbString.trim().indexOf("*"));
            if(reqString.startsWith(sTemp))
            {
                bMatched = true;
            }            
        }
        return bMatched;
    }
}
