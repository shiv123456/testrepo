package config.java.ams.custom;

import ariba.app.util.Attachment;
import ariba.htmlui.approvableui.fields.*;
import ariba.ui.aribaweb.core.*;

/*

Purpose          : Custom controller of multiple attachment

*/

public class BuysenseARVMultipleAttachments extends ARVAttachments
{
	public static final String ClassName = "config.java.ams.custom.BuysenseARVMultipleAttachments";

    public AWComponent deleteAttachmentAction()
    {
		Attachment att = getCurAttachment();

		valueList().remove(att);
        return null;
    }

}
