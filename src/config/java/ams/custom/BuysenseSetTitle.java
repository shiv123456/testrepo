package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.ClusterRoot;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseSetTitle extends Action
{
	private static String msCN = "BuysenseResetProposedProcurement";
	private static String msSubmitNote = "The following must be submitted and attached to this request: \n" +
                                "- Solicitation, including all addenda and award documents.\n" +
                                   "- Documentation of the solicitation advertisement in VBO or evidence that Virginia vendors received an award.\n" +
                                   "- A memorandum that addresses the following four points:\n" +
                                   "    1. Specifically how does the Scope of Work and pricing of this Cooperative Contract provide for the goods or services you seek to purchase? \n" +  
                                   "    2. Provide evidence the vendor is registered in eVA. \n" + 
                                   "    3. Explain why the use of this cooperative contract is the best option for the Commonwealth, including why the prices offered in the contract are considered fair and reasonable. \n" +
                                   "    4. Provide verification that no state contract exists to satisfy the requirement.";
	
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
    	Log.customer.debug("***%s***Called fire:: valuesource obj %s", msCN, valuesource);
    	Approvable loAppr = null;
        String lsProposedProcurement = null;
        
        if(valuesource!= null && valuesource instanceof Approvable)
        {
           loAppr = (Approvable)valuesource;
           
           Boolean lbContractExemption = (loAppr.getFieldValue("ContractExemption") == null)? false:(Boolean) loAppr.getFieldValue("ContractExemption");
           Boolean lbExceedDelegatedAuthority = (loAppr.getFieldValue("ExceedDelegatedAuthority") == null)? false:(Boolean) loAppr.getFieldValue("ExceedDelegatedAuthority");
           Boolean lbCooperativeContractCB = (loAppr.getFieldValue("CooperativeContractCB") == null)? false:(Boolean) loAppr.getFieldValue("CooperativeContractCB");
           Boolean lbOther = (loAppr.getFieldValue("Other") == null)? false:(Boolean) loAppr.getFieldValue("Other");
         Boolean lbCooperativeContractCB1 = (loAppr.getFieldValue("CooperativeContractCB1") == null)? false:(Boolean) loAppr.getFieldValue("CooperativeContractCB1");
         Boolean lbExceedDelegatedAuthority1 = (loAppr.getFieldValue("ExceedDelegatedAuthority1") == null)? false:(Boolean) loAppr.getFieldValue("ExceedDelegatedAuthority1");
         Boolean lbContractModification1 = (loAppr.getFieldValue("ContractModification1") == null)? false:(Boolean) loAppr.getFieldValue("ContractModification1");
         Boolean lbFederalGrant = (loAppr.getFieldValue("FederalGrant") == null)? false:(Boolean) loAppr.getFieldValue("FederalGrant");
         Boolean lbStateGrant = (loAppr.getFieldValue("StateGrant") == null)? false:(Boolean) loAppr.getFieldValue("StateGrant");
         Boolean lbOtherField = (loAppr.getFieldValue("OtherField") == null)? false:(Boolean) loAppr.getFieldValue("OtherField");

        ariba.user.core.User loRequesterUser = (ariba.user.core.User)loAppr.getRequester();
        if(loRequesterUser != null && proposedProcurement !=null)
        {
        	ariba.common.core.User loRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(loRequesterUser,appr.getPartition());
        	String lsClientName = (String)loRequesterPartitionUser.getDottedFieldValue("ClientName.ClientName");
        	if(lsClientName != null)
        	{
        		appr.setName(lsClientName+ " - " +proposedProcurement);
        	}
        }
    }
}
