package config.java.ams.custom;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.apache.log4j.Level;

import ariba.base.core.Base;
import ariba.base.core.BaseVector;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.common.core.Address;
import ariba.common.core.UserProfile;
import ariba.common.core.UserProfileDetails;
import ariba.util.scheduler.ScheduledTask;
import ariba.util.scheduler.Scheduler;
import ariba.util.log.Log;
import ariba.util.core.Fmt;

public class eVA_UpdateUserProfileFromBSAdmin extends ScheduledTask {

	public static final String ClassName = "config.java.ams.custom.eVA_UpdateUserProfileFromBSAdmin";

	public Connection moConnection;
	java.sql.Statement moUserCSVStatement = null;
	java.sql.Statement moROLEMAPCSVStatement = null;
	java.sql.Statement loUserBuysenseCSVStatement = null;
	
	Partition moPartition = null;
	ClusterRoot moClusterRoot = null;
	
	String msSuccessStatus = "ADDED";
	String msErrorStatus = "ERROR";
	String msStatusField = "ACTION";

	String msUSERBUYSENSEQUERY = null;
	String msUSERQUERY = null;
	String msROLEMAPQUERY = null;
	
	private String msDBPassword = null ;
	private String msDBURL = null ;
	private String msDBUser = null ;

	public void init(Scheduler scheduler, String scheduledTaskName, Map arguments)
	{
        super.init(scheduler, scheduledTaskName, arguments);
        String lsKey = null;
        for(java.util.Iterator e = arguments.keySet().iterator(); e.hasNext();)
        {
           lsKey = (String)e.next();
           if(lsKey.equals("DBPassword"))
           {
        	   msDBPassword = (String) arguments.get(lsKey);
           }else if(lsKey.equals("DBURL"))
           {
        	   msDBURL = (String) arguments.get(lsKey);
           }else if(lsKey.equals("DBUser"))
           {
        	   msDBUser = (String) arguments.get(lsKey);
           }else if(lsKey.equals("USERBUYSENSEQUERY"))
           {
        	   msUSERBUYSENSEQUERY = (String) arguments.get(lsKey);
           }else if(lsKey.equals("USERQUERY"))
           {
        	   msUSERQUERY = (String) arguments.get(lsKey);
           }else if(lsKey.equals("ROLEMAPQUERY"))
           {
        	   msROLEMAPQUERY = (String) arguments.get(lsKey);
           }
        }
	}

	public void run()
	{
		try
		{
		   Log.customer.debug("Class %s on method run", ClassName);
		   moPartition = Base.getSession().getPartition();
		   moConnection = (Connection) DriverManager.getConnection(msDBURL,msDBUser,msDBPassword);
		   moConnection.setAutoCommit (false);
		   loUserBuysenseCSVStatement = moConnection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
		   moUserCSVStatement = moConnection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
		   moROLEMAPCSVStatement = moConnection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
		   Log.customer.debug("Class %s on method run :: variable loReqStatement is %s", ClassName, loUserBuysenseCSVStatement);
		   ResultSet loUserBuysenseResults = loUserBuysenseCSVStatement.executeQuery(msUSERBUYSENSEQUERY);
		   Log.customer.debug("Class %s on method run :: variable loReqResultSet is %s", ClassName, loUserBuysenseResults);
		   while (loUserBuysenseResults != null && loUserBuysenseResults.next())
		   {
			  Log.customer.debug("Class %s on method run ::USER_PROFILE_KEY"+loUserBuysenseResults.getString("USER_PROFILE_KEY").trim());
			  UserProfile loUserProfile = (ariba.common.core.UserProfile)
			                              Base.getService().objectMatchingUniqueName(
			                              "ariba.common.core.UserProfile",moPartition,loUserBuysenseResults.getString("USER_PROFILE_KEY").trim());
			  boolean lbDLMatching = false;
			  if(loUserProfile != null)
			  {
			    lbDLMatching = mcheckIFDLMatching(loUserProfile.getDetails(), loUserBuysenseResults.getString("DRIVER_LICENSE_NUMBER").trim());
			  }
		      if(lbDLMatching)
		      {
		        mUpdateUPFromBSUserCSV(loUserBuysenseResults, loUserProfile, loUserProfile.getDetails());  
		      }
		      else
		      {
		        mUpdateErrorStatus(loUserBuysenseResults);
			    Log.customer.setLevel(Level.WARN);
			    if (loUserProfile == null)
			      Log.customer.warning(10001, loUserBuysenseResults.getString("USER_PROFILE_KEY"));
			    else
			      Log.customer.warning(10002, loUserProfile.getUniqueName());
		      }
		   }
		}
		catch (Exception loEx)
        {
            Log.customer.debug("Class %s on method run :: Exception %s ", ClassName, loEx.toString() );
        }
        finally
        {
          Log.customer.debug("Class %s on method run :: attempting to close moConnection in finally block ", ClassName);
          try
          {
           moConnection.commit(); 
           if (!moConnection.isClosed())
           loUserBuysenseCSVStatement.close();
           moUserCSVStatement.close();
           moROLEMAPCSVStatement.close();
           moConnection.close();
           Log.customer.debug("Class %s on method run :: closed moConnection in finally block of run method ", ClassName);
          }
          catch (Exception loEx)
          {
           Log.customer.debug("Class %s on method run :: failed to close moConnection in finally block of run method ", ClassName);
          }
        }
	}
	
	public void mUpdateUPFromBSUserCSV(ResultSet buysenseUserCSV, UserProfile userProfile, UserProfileDetails userProfileDetails)
	{
	   try
	   {
		  userProfileDetails.setFieldValue("ContractorPersonID", 
				                          buysenseUserCSV.getString("CONTRACTOR_PERSON_ID") == null ? null : buysenseUserCSV.getString("CONTRACTOR_PERSON_ID"));
		  userProfileDetails.setFieldValue("ContractorLoginID", 
				                          buysenseUserCSV.getString("USER_UNIQUE_NAME") == null ? null : buysenseUserCSV.getString("USER_UNIQUE_NAME"));
		  userProfileDetails.setFieldValue("Phone", buysenseUserCSV.getString("PHONE_NUMBER") == null ? null : buysenseUserCSV.getString("PHONE_NUMBER"));
		  moClusterRoot = (ariba.base.core.ClusterRoot)Base.getService().objectMatchingUniqueName
		                                  ("ariba.core.BuySenseCatalogController",moPartition,buysenseUserCSV.getString("CATALOG_CONTROLLER").trim());
		  userProfileDetails.setFieldValue("BuysenseCatalogController", moClusterRoot == null ? null : moClusterRoot);
		  moClusterRoot = (ariba.base.core.ClusterRoot)Base.getService().objectMatchingUniqueName
          							      ("ariba.core.BuysenseOrg", moPartition, buysenseUserCSV.getString("BUYSENSE_ORG").trim());
		  userProfileDetails.setFieldValue("BuysenseOrg", moClusterRoot == null ? null : moClusterRoot);
		  updateROWStatus(buysenseUserCSV, msSuccessStatus);
		  userProfile.save();
		  updateUPFromUSERCSV(userProfile, userProfileDetails, buysenseUserCSV.getString("USER_UNIQUE_NAME"));
	   }
	   catch (SQLException sqlException)
	   {
	      Log.customer.debug("Class %s on method :: mUpdateUPFromBSUserCSV :: Caught with SQL Exception % ", ClassName, sqlException.getMessage()); 	   
	   }
	   catch (Exception exception)
	   {
	      Log.customer.debug("Class %s on method :: mUpdateUPFromBSUserCSV :: Caught with Exception % ", ClassName, exception.getMessage()); 	   
	   }
	}
	
	public void updateUPFromUSERCSV(UserProfile userProfile, UserProfileDetails userProfileDetails, String userUniqueName)
	{
       try
       {
	     Log.customer.debug("Class %s on method updateUPFromUSERCSV :: USER QUERY is %s", ClassName, getFormattedQuery(msUSERQUERY, userUniqueName));
	     //ResultSet loUserCSVResults = moUserCSVStatement.executeQuery(msUSERQUERY+"'"+userUniqueName+"'");
	     ResultSet loUserCSVResults = moUserCSVStatement.executeQuery(getFormattedQuery(msUSERQUERY, userUniqueName));
  	     while (loUserCSVResults != null && loUserCSVResults.next())
		 {
  	       userProfileDetails.setDottedFieldValue("UserProfileDetails.EmailAddress", 
  	    		                           loUserCSVResults.getString("EMAIL_ADDRESS") == null ? null : loUserCSVResults.getString("EMAIL_ADDRESS"));           
  	       Address loShipTo = (ariba.common.core.Address)
  	                           Base.getService().objectMatchingUniqueName( "ariba.common.core.Address",moPartition,loUserCSVResults.getString("SHIPTO").trim());
  	       userProfileDetails.setFieldValue("ShipTo", loShipTo == null ? null : loShipTo);
  	       userProfileDetails.setFieldValue("DeliverTo", 
  	    		                           loUserCSVResults.getString("DELIVERTO") == null ? null : loUserCSVResults.getString("DELIVERTO"));
    	   updateROWStatus(loUserCSVResults, msSuccessStatus);
		 }
  	     userProfile.save();
  	     updateUPFromROLEMAPCSV(userProfile, userProfileDetails, userUniqueName);
       }
	   catch (SQLException sqlException)
	   {
	      Log.customer.debug("Class %s on method :: updateUPFromUSERCSV :: Caught with SQL Exception % ", ClassName, sqlException.getMessage()); 	   
	   }
	   catch (Exception exception)
	   {
	      Log.customer.debug("Class %s on method :: updateUPFromUSERCSV :: Caught with Exception % ", ClassName, exception.getMessage()); 	   
	   }
	}
	
	public void updateUPFromROLEMAPCSV(UserProfile userProfile, UserProfileDetails userProfileDetails, String userUniqueName)
	{
       try
       {
	     Log.customer.debug("Class %s on method updateUPFromROLEMAPCSV::ROLE QUERY is %s", ClassName, getFormattedQuery(msROLEMAPQUERY, userUniqueName));
	     //ResultSet loROLEMAPCSVResults = moROLEMAPCSVStatement.executeQuery(msROLEMAPQUERY+"'"+userUniqueName+"'");
	     ResultSet loROLEMAPCSVResults = moROLEMAPCSVStatement.executeQuery(getFormattedQuery(msROLEMAPQUERY, userUniqueName));
	     ariba.user.core.UserProfileDetails loGeneralUPDetails = (ariba.user.core.UserProfileDetails) userProfileDetails.getUserProfileDetails();
	     BaseVector loUserGroups = (BaseVector) loGeneralUPDetails.getGroups();
  	     while (loROLEMAPCSVResults != null && loROLEMAPCSVResults.next())
		 {
  	    	ariba.user.core.Group loGroupUniqueName = ariba.user.core.Group.getGroup(loROLEMAPCSVResults.getString("ROLE_UNIQUE_NAME"));
  	    	if(loGroupUniqueName != null)
  	    	{
  	          loUserGroups.addElementIfAbsent(loGroupUniqueName);
  	 	      updateROWStatus(loROLEMAPCSVResults, msSuccessStatus);
  	    	}
		 }
  	     userProfile.save();
       }
	   catch (SQLException sqlException)
	   {
	      Log.customer.debug("Class %s on method :: updateUPFromROLEMAPCSV :: Caught with SQL Exception % ", ClassName, sqlException.getMessage()); 	   
	   }
	   catch (Exception exception)
	   {
	      Log.customer.debug("Class %s on method :: updateUPFromROLEMAPCSV :: Caught with Exception % ", ClassName, exception.getMessage()); 	   
	   }
	}
    
	public void mUpdateErrorStatus(ResultSet resultSet)
	{
	  try
	  {
        updateROWStatus(resultSet, msErrorStatus);
        updateErrorStatusForUSERCSV(resultSet.getString("USER_UNIQUE_NAME").trim());
        updateErrorStatusForROLEMAPCSV(resultSet.getString("USER_UNIQUE_NAME").trim());
	  }
      catch (SQLException sqlException)
	  {
	    Log.customer.debug("Class %s on method :: mUpdateErrorStatus :: Caught with SQL Exception % ", ClassName, sqlException.getMessage()); 	   
	  }
      catch (Exception exception)
	  {
	    Log.customer.debug("Class %s on method :: mUpdateErrorStatus :: Caught with Exception % ", ClassName, exception.getMessage()); 	   
	  }
	}
	
	public void updateErrorStatusForUSERCSV(String userUniqueName)
	{
      try
       {
	     //ResultSet loUserCSVResults = moUserCSVStatement.executeQuery(msUSERQUERY+"'"+userUniqueName+"'");
    	 ResultSet loUserCSVResults = moUserCSVStatement.executeQuery(getFormattedQuery(msUSERQUERY, userUniqueName));
         while (loUserCSVResults != null && loUserCSVResults.next())
   	     {
    	   updateROWStatus(loUserCSVResults, msErrorStatus);
	     }
       }
 	   catch (SQLException sqlException)
	   {
	      Log.customer.debug("Class %s on method :: updateErrorStatusForUSERCSV :: Caught with SQL Exception % ", ClassName, sqlException.getMessage()); 	   
	   }
 	   catch (Exception exception)
	   {
	      Log.customer.debug("Class %s on method :: updateErrorStatusForUSERCSV :: Caught with Exception % ", ClassName, exception.getMessage()); 	   
	   }
	}

	public void updateErrorStatusForROLEMAPCSV(String userUniqueName)
	{
      try
       {
	     //ResultSet loROLEMAPCSVResults = moUserCSVStatement.executeQuery(msROLEMAPQUERY+"'"+userUniqueName+"'");
    	 ResultSet loROLEMAPCSVResults = moUserCSVStatement.executeQuery(getFormattedQuery(msROLEMAPQUERY, userUniqueName));
         while (loROLEMAPCSVResults != null && loROLEMAPCSVResults.next())
   	     {
    	   updateROWStatus(loROLEMAPCSVResults, msErrorStatus);
	     }
       }
 	   catch (SQLException sqlException)
	   {
	      Log.customer.debug("Class %s on method :: updateErrorStatusForROLEMAPCSV :: Caught with SQL Exception % ", ClassName, sqlException.getMessage()); 	   
	   }
 	   catch (Exception exception)
	   {
	      Log.customer.debug("Class %s on method :: updateErrorStatusForROLEMAPCSV :: Caught with Exception % ", ClassName, exception.getMessage()); 	   
	   }
	}
	
	public void updateROWStatus(ResultSet resultSet, String status)
	{
	   try
		{
		  Log.customer.debug("Class %s on method run :: updateROWStatus :: %s resultSet, %s status, %s msStatusField", ClassName, resultSet, status, msStatusField); 
		  resultSet.updateString(msStatusField,status);
		  resultSet.updateRow();
		}
	   catch (SQLException sqlException)
	   {
	      Log.customer.debug("Class %s on method :: updateROWStatus :: Caught with SQL Exception %s ", ClassName, sqlException.getMessage()); 	   
	   }
	   catch (Exception exception)
	   {
	      Log.customer.debug("Class %s on method :: updateROWStatus :: Caught with Exception %s ", ClassName, exception.getMessage()); 	   
	   }
	}

	public String getFormattedQuery(String query, String key)
	{
		return Fmt.S(query,key);     	
	}
	
	public boolean mcheckIFDLMatching(UserProfileDetails userProfileDetails, String dlNumber)
	{
		if(dlNumber!=null && userProfileDetails.getFieldValue("DriversLicenseStateNumber") != null)
		{
			return dlNumber.equals(userProfileDetails.getFieldValue("DriversLicenseStateNumber"));	
		}
		else
		{
			return false;
		}
	}
}