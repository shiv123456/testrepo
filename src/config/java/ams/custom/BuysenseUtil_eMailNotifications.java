package config.java.ams.custom;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.StringTokenizer;

import ariba.user.util.mail.Notification;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.LocalizedString;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.user.core.Role;
import ariba.util.core.ListUtil;
import ariba.util.log.Log;
import ariba.util.core.StringUtil;

public class BuysenseUtil_eMailNotifications
{
	/* Call this method and pass fileNameWithPath as config/variants/vcsv/data/AgencySecurityOfficer.csv
	   and sClientName as required.*/
	public static void notifyByMail( String fileNameWithPath,String sClientName, String subject, String body)
	{
		File file = readFile(fileNameWithPath);//Read the file
		Log.customer.debug(" BuysenseUtil_eMailNotifications.notifyByMail()-The file is: "+file);
		//Role role = getRoleFromNameString(getRoleForClientName(file, sClientName));//get the role for the specified Client Name
		Role role = getRoleFromUniqueNameString(getRoleForClientName(file, sClientName));//get the role for the specified Client Name
		Log.customer.debug(" BuysenseUtil_eMailNotifications.notifyByMail()-The role is: "+role);
		List recipients = ListUtil.list();
		if(role == null)
		{
			Log.customer.debug(" BuysenseUtil_eMailNotifications.notifyByMail()- System is unable to get the Role for client: "
					            +sClientName+" From file: "+file+" and hence NO e-Mail notifications will be sent");
		}else
		{
			recipients = role.getAllUsers();//ass all users of role as recipients	
		}
		Log.customer.debug(" Notifying users: "+recipients+" with subject-"+subject);
		LocalizedString localizedSubject = new LocalizedString(subject);
		LocalizedString localizedBody = new LocalizedString(body);
		Notification.emailUsers(recipients, null, localizedSubject, localizedBody);
	}
	
	public static File readFile(String fileNameWithPath)
	{
		File FileToread = new File(fileNameWithPath);
		if (FileToread != null)
		    return FileToread;
		else
			return null;		
	}
	public static String getRoleForClientName(File file, String clientName)
	{
		Log.customer.debug(" BuysenseUtil_eMailNotifications.getRoleForClientName()- file is: "+file); 
		Log.customer.debug(" BuysenseUtil_eMailNotifications.getRoleForClientName()- clientName is: "+clientName); 
        String sRole = null;
        if(file != null)
        try
        {
		  FileReader frDS = new FileReader(file);
          BufferedReader brDS = new BufferedReader(frDS);
          String sInputLine = null;           
          StringTokenizer sTokenizer = null;
          String delimiter = ",";
          String sClientNameFromFile = "";
          while ((sInputLine =brDS.readLine()) != null)
          {  
              sTokenizer = new StringTokenizer(sInputLine,delimiter);                      
				while (sTokenizer.hasMoreTokens())
				{
					// Looping through each column in the file
					sClientNameFromFile = ((String) sTokenizer.nextElement()).trim();
					//Log.customer.debug(" BuysenseUtil_eMailNotifications.getRoleForClientName()- sClientNameFromFile is: "+ sClientNameFromFile);
					String sRoleFromFile = ((String) sTokenizer.nextElement()).trim();
					//Log.customer.debug(" BuysenseUtil_eMailNotifications.getRoleForClientName()- sRoleFromFile is: "+ sRoleFromFile);
					if(sClientNameFromFile.equalsIgnoreCase(clientName))
					{
						sRole = sRoleFromFile;
					}
				}		  
          }

          brDS.close();
        }
        catch(IOException ioe)
        {
        	Log.customer.debug(" BuysenseUtil_eMailNotifications.getRoleForClientName()- exception while reading fiel: "+ioe.getMessage());
        }
		Log.customer.debug(" BuysenseUtil_eMailNotifications.getRoleForClientName()- Role is: "+sRole); 	        
		return sRole;
	}
	
	public static Role getRoleFromUniqueNameString(String sRoleUniqueName)
	{
		if(StringUtil.nullOrEmptyOrBlankString(sRoleUniqueName))
		{
			Log.customer.debug(" BuysenseUtil_eMailNotifications.getRoleFromUniqueNameString()-sRoleUniqueName: "+sRoleUniqueName);
			return null;
		}
		else
		{
		Role role = Role.getRole(sRoleUniqueName);
		Log.customer.debug(" BuysenseUtil_eMailNotifications.getRoleFromUniqueNameString()-role: "+role);		
		return role;
		}
	}
	
	public static Role getRoleFromNameString(String sRoleName)
	{
		Role role = null;
		if(StringUtil.nullOrEmptyOrBlankString(sRoleName))
			return null;
		else
		{
		Partition partition = Base.getSession().getPartition();
		String sAQLQueryText = "SELECT r FROM ariba.user.core.Role r WHERE r.Name = '"+sRoleName+"' AND r.Active='true'";
        AQLOptions aqlOptions = new AQLOptions(partition);
        AQLQuery aqlQuery = AQLQuery.parseQuery(sAQLQueryText);
        AQLResultCollection aqlResults = Base.getService().executeQuery(aqlQuery, aqlOptions);
        
        if(aqlResults.getFirstError()!= null)
        {
        	Log.customer.debug("BuysenseUtil_eMailNotifications.getRoleFromNameString(sRoleName), Errors in resultset: " + " " + aqlResults.getFirstError().toString());
        }
        else
        {
	        while (aqlResults.next())
	        {
	  	    	role = (Role)Base.getSession().objectFromId((BaseId)aqlResults.getObject(0));
	        }
        }
		
		return role;	
		}
	}

}