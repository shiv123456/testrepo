/*
    Copyright (c) 1996-1999 Ariba/Buysense, Inc.
    Falahyar March 2000
    ---------------------------------------------------------------------------------------------------

*/

// 08/07/2003: Updates for Ariba 8.0 (David Chamberlain)

/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.base.core.ClusterRoot;
import ariba.base.core.Base;
import ariba.approvable.core.Approvable;
// Ariba 8.0: replaced 'ariba.approvable.core.ApprovableHook' with 'ariba.approvable.core.ApprovableHook'
import ariba.approvable.core.ApprovableHook;
import ariba.util.log.Log;
import java.util.List;
import ariba.util.core.ListUtil;
import ariba.util.core.Constants;

public class AMSBSOrgSubmitHook implements ApprovableHook
{
    // Ariba 8.1 Changed from Util to ListUtil.vector() and Util to Constants.getInteger()
    // 81->822 changed Vector to List

    public List run (Approvable approvable)
    {

        // 01/18/02 labraham - Added edit to verify that the BuysenseOrg UniqueName does not already exist when adding a
        //                                     new BuysenseOrg
        Boolean newBSOrg = (Boolean)approvable.getDottedFieldValue("NewBuysenseOrg");
        if (newBSOrg.booleanValue() == true)
        {
            String bsorgName = (String)approvable.getDottedFieldValue("KeyID");
            // 08/21/08 manoj gaur -CSPL 590 - trim the space from Key ID to restrict the user from putting duplicate BSO with space.
            bsorgName = bsorgName!= null ? bsorgName.trim() : bsorgName;
            
            ClusterRoot bsorg =
                Base.getService().objectMatchingUniqueName( "ariba.core.BuysenseOrg",approvable.getPartition(),bsorgName);
            Log.customer.debug("AMSBSOrgSubmitHook after looking up BSOrgEform Name in BSOrg %s",bsorgName);

            if (bsorg != null)
            {
                return ListUtil.list(Constants.getInteger(-1),
                                         "Cannot process Submit on the BuysenseOrg Eform " +
                                         "because the BuysenseOrg Name must be unique for New BuysenseOrgs.  " +
                                         "Please modify the name and resubmit.");
            }
        }


        //Anup - Generic invocation of dynamically determined Client/Agency edits. - Dev SPL #173
        List result = EditClassGenerator.invokeHookEdits(approvable,
                                                           this.getClass().getName());
        // Ariba 8.1: elementAt(int) has been deprectated by get(int)
        //int errorCode = ((Integer)result.elementAt(0)).intValue();
        int errorCode = ((Integer)result.get(0)).intValue();
        Log.customer.debug("Validations in " + this.getClass().getName() + ", error Code --> %s",errorCode);
        if(errorCode<0)
        {
            Log.customer.debug("Validations Failed in " + this.getClass().getName() + " - returning error to UI");
            return result;
        }

        return BuysenseRulesEngine.runBSOrgFilteringRules(approvable);

    }

}





