
package config.java.ams.custom;

import ariba.base.core.Partition;
import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.SearchTermQuery;
import ariba.base.core.aql.AQLOrderByElement;
import ariba.base.core.Base;
import ariba.base.core.aql.AQLQuery;
import ariba.base.fields.*;
import ariba.base.core.aql.AQLNameTable;
import ariba.common.core.Supplier;
import java.util.List;
import ariba.util.core.ListUtil;
import ariba.util.log.Log;
import java.lang.String;

public class BuysenseSupplierNameTable extends AQLNameTable
implements InitializeValueSource
{
/****************************************************************************************************************
 *
 * This BuysenseSupplierNameTable is a nameTableClass properties for the ProcureLineItem Supplier field.
 * It extends the AQLNameTable to allow search for supplier by supplier name, supplierlocations name, and
 * supplierlocation TIN number.  The result will always be sorted by supplier name.
 * Ref files: BuysenseSupplierExt.aml and ReqExtrinsicFields.aml
 *
 ****************************************************************************************************************/


    public void initializeValueSource(ValueSource vs)
    {
        ariba.common.core.Supplier loSupplier = (Supplier) vs;
        Log.customer.debug("Calling BuysenseSupplierNameTable vs ="+vs);
    }
    public List matchPattern (String field, String pattern)
    {
        List results = super.matchPattern(field, pattern);
        return results;
    }

    public void addQueryConstraints(AQLQuery query, String field, String pattern) {
        super.addQueryConstraints(query, field, pattern);
        try{
            //Added AQLCondition to where clause for CSPL-5003 to search by�Location name�
            String pattern1=" ";
            if(!pattern.equalsIgnoreCase("null"))
            {
               pattern1=pattern.replace('*', '\'');
            }
            if(field != null && field.contains("All"))
            {
                query.or(AQLCondition.parseCondition("(contains(Supplier.Locations.VendorCustomerCode, "+pattern1+", FALSE)AND Supplier.Creator IS NULL)")); 
                query.or(AQLCondition.parseCondition("(contains(Supplier.Locations.TIN, "+pattern1+", FALSE)AND Supplier.Creator IS NULL)"));
                query.or(AQLCondition.parseCondition("(contains(Supplier.Name, "+pattern1+", FALSE)AND Supplier.Creator IS NULL)"));
                query.or(AQLCondition.parseCondition("(contains(Supplier.Locations.Name, "+pattern1+", FALSE) AND Supplier.Creator IS NULL)"));           
                Log.customer.warning(8000,"New query:"+query);
            }
            else
            {
            query.or(AQLCondition.parseCondition("(contains(Supplier.Locations.Name, "+pattern1+", FALSE) AND Supplier.Creator IS NULL)"));           
            Log.customer.warning(8000,"New query:"+query); 
            }
        }catch(Exception e)
        {
            Log.customer.warning(8000,"Exception in Supplier search:"+e.getMessage());
        }
        finally {
        query.andNotEqual("UniqueName", "[Unspecified]");
        }
    }
    
    protected AQLQuery buildQuery(AQLQuery query, String field, String pattern, SearchTermQuery searchTermQuery) {
        try
        {
            if(hasSortLanguageFields())
            {
                query.addSelectElements(extendSortLanguageFields(fetchFields()));
            }
            else
            {
                query.addSelectElements(fetchFields());
            }
            if(field != null)
            {
                if(hasSortLanguageFields())
                {
                    addSortLanguageOrderByElements(query, field);
                }
                else
                {
                    // Always sort by Supplier Name
                    query.addOrderByElement("Name");
                }
            }
            if(searchTermQuery == null)
            {
                addQueryConstraints(query, field, pattern);
            }
            else
            {
                addQueryConstraints(query, field, searchTermQuery);
            }
        }
        catch (Exception e)
        {
            Log.customer.debug("Exception caught in buildQuery");
            e.printStackTrace();
        }
        return query;
    }
    private final void addSortLanguageOrderByElements(AQLQuery query, String field) 
    {
        List orderBy = ListUtil.list();
        List fields = fetchFields();
        for(int vidx = 0; vidx < fields.size(); vidx++) 
        {
            if(!field.equals(fields.get(vidx)))
            {
                continue;
            }
            if(getSortLanguageFields().contains(field)) 
            {
                orderBy.add(new AQLOrderByElement(vidx + 3));
                orderBy.add(new AQLOrderByElement(vidx + 2));
            } else 
            {
                orderBy.add(field);
            }
            break;
        }
        query.addOrderByElements(orderBy);
    }
}
