/*
    Copyright (c) 1996-2000 Ariba, Inc.
    All rights reserved. Patents pending.

    $Id: //ariba/release/7.0/current/code/sample/procure/misc/java/procure/server/TestPrintHTMLApprovableHook.java#2 $


*/
/****************************************************
*
*
*****************************************************/

/* 08/13/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */
/*
 * rlee, 1/21/2005: Prod SPL 583 - Req Approval flow is displayed multiple times on Req Print
 */

package config.java.ams.custom;
import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.base.core.LongStringElement;
// Ariba 8.0: Commented out BaseObjectOnServer and ApprovableOnServer imports.
//import ariba.base.server.BaseObjectOnServer;
//import ariba.server.ormsserver.ApprovableOnServer;
//Ariba 8.1: Replaced deprecated ariba.approvable.core.PrintApprovableHook with ariba.approvable.core.PrintApprovableHook.
import ariba.approvable.core.PrintApprovableHook;
import ariba.util.log.Log;
//Ariba 8.1: Removed deprecated ariba.util.core.Util class.
//import ariba.util.core.Util;
import java.util.List;
// Ariba 8.1: Various methods have changed from the java.util.List package to the ariba.util.core.ListUtil package.
import ariba.util.core.ListUtil;
// Ariba 8.1: Deprecations, methods have changed packages.
import ariba.util.core.Constants;
import ariba.util.core.StringUtil;
import ariba.util.net.HTMLPrintWriter;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Locale;
import java.io.PrintWriter;
import config.java.ams.custom.PSGException;
import config.java.ams.custom.PSGFunctions;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.LineItemCollection;
import ariba.approvable.core.LineItem;
import ariba.approvable.core.Comment;
import ariba.approvable.core.ApprovalRequest;
//import config.java.procure.server.Log;
import java.text.*;
import ariba.util.core.Fmt;
import ariba.util.formatter.SecureStringFormatter;

/**
    TestPrintHTMLApprovableHook is an example showing how the print hook
    can be used.

    The return results of the PrintApprovableHook is a List with the first
    element the return result. 0 refers to the success of the
    operation and the client should proceed with the operation. Any other
    value need to be interpreted by the client. The convention is that a
    negative value corresponds to an error while a positive value corresponds
    to a warning.

    The second element contains a user-visible string which explains the condition.

    TestPrintHTMLApprovableHook returns 0 to indicate success.
    It returns 1 to indicate there is a warning.
    It returns -1 to indicate there is an error.
*/

/*  6-12-01, eVA Production SPL52, Fix Email notifications contain HTML code ****/
/*  11-01-02, eVA Dev SPL#98, Modify eVA print functionality to label Change Orders as such ****/
//81->822 changed Vector to List
/**
 *  CSPL 920: Search Enhancement
 *  Author  : Sarath Babu Garre
 *  Date:   : 22/05/2009
 *  Explanation:Display billing and shipping address once on printed PR/PO unless the lines contain different addresses.
 *              And display Deliver To with Ship To, EXCEPT in the case when Ship To is the same on all line items and Deliver To is not.
 *              In that case, display Ship To at header and Deliver To at line item *
 */
public class BuysensePrintHTMLApprovableHook implements PrintApprovableHook
{
    // If working is set to true, it will always signal success.
    boolean working = false;

    // Ariba 8.1: Replaced deprecated Util class with ListUtil class and Constants class.
    private static final List NoErrorResult = ListUtil.list(Constants.getInteger(NoError));

    public static final String CRLF = "\r\n";
    boolean bCommonShipping = true;
    boolean bCommonBilling = true;
    boolean bCommonDeliverTo = true;
    String sDeliver;
    String sShipTo;
    String sBillTo;
    String sOrderNo;
    String sERPValue;
    public List run (Approvable approvable, PrintWriter out,
                       Locale locale, boolean printForEmail)
    {

        //6-12-01, Fix Email notifications contain HTML code
        if (printForEmail)
        {
            Log.customer.debug("In if when printForEmail is true");
            return NoErrorResult;
        }

        try
        {
            String sReqType=approvable.getTypeName();


            Log.customer.debug("Calling TestPrintHTMLApprovableHook ..."+sReqType);

            HTMLPrintWriter hout = new HTMLPrintWriter(out);

            Hashtable hHdrParams = new Hashtable();
            Hashtable hCmtParams = new Hashtable();
            Hashtable hComment = null, hApprParams = new Hashtable();
            Hashtable hAppr=null;
            Enumeration eParams = null;
            String snHdrFile = "config/htmlui/resource/" + approvable.getTypeName() + ".New.html";
            String sHdrFile = "config/htmlui/resource/" + approvable.getTypeName() + ".html";
            String sLineFile = "config/htmlui/resource/" + approvable.getTypeName() + ".Line.html";
            String sCmtFile = "config/htmlui/resource/" + approvable.getTypeName() + ".Comment.html";
            String sReqAppr = "config/htmlui/resource/" + approvable.getTypeName() + ".Approvers.html";
            String sHdr = null, sLine = null,sLine1 = null, sCmt = null, sTag = null; 
            String sComment = null, sComments = "";
            String sApprovers ="";
            String sTotalApprovers="";
            String sNewValue ="";
            int iTag = 0, iTagEnd = -1;
            Object oValue = null;
            NumberFormat nf = NumberFormat.getInstance();
            boolean bNewEform = false;

            if (sReqType.equals("ariba.core.BuysenseOrgEform"))
            {
                oValue = approvable.getFieldValue("NewBuysenseOrg");
                if (oValue != null)
                {
                    String sbNewEform = String.valueOf(oValue);
                    if (sbNewEform.equals("true"))
                    {
                        bNewEform = true;
                        Log.customer.debug("line 109, true");
                    }
                    else if (sbNewEform.equals("false"))
                    {
                        bNewEform = false;
                        Log.customer.debug("line 114, false");
                    }
                    else
                    {
                        Log.customer.debug("error in line 118");
                    }


                    if (bNewEform)
                    {
                        sHdrFile=snHdrFile;
                    }
                }
            }

            // Get HTML template files
            sHdr = PSGFunctions.ReadFile(sHdrFile);
            sShipTo = PSGFunctions.ReadFile("config/htmlui/resource/CommShipTo.html");
            sBillTo = PSGFunctions.ReadFile("config/htmlui/resource/CommBillTo.html");
            sDeliver = "<B>Deliver To: </B><!--Line.DeliverTo--><BR>";
            //sOrderNo = "<TR VALIGN=TOP><TD COLSPAN=9> <Font Size=2><B>Order No.: </B><!--Line.Order.UniqueName--><BR></Font></TD></TR>";
            sOrderNo = "<B>Order No.: </B><!--Line.Order.UniqueName--><BR>";

/* CSPL-920:(sarath babu) Display billing and shipping address once on printed PR/PO Header unless the lines contain different addresses */
            if((approvable.instanceOf("ariba.purchasing.core.PurchaseOrder"))||(approvable.instanceOf("ariba.purchasing.core.Requisition")))
            {
            	String sShipToCommon = null, sBillToCommon=null, sShipToTemp =null, sBillToTemp = null,sDeliverToTemp = null;
	 	        String sCommonBillTo = null,sCommonShipTo=null,sDeliverToCommon = null;
	            List vLinesCommon = ((LineItemCollection) approvable).getLineItems();
	            LineItem liCommon = null,liCommonLine = null;
	            if(vLinesCommon.size() >= 1)
	            {
	            	for (int iLine = 0; iLine < vLinesCommon.size(); iLine ++)
	            	{
						liCommon = (LineItem) vLinesCommon.get(iLine);
	 	            	sShipToCommon =(String)liCommon.getDottedFieldValue("ShipTo.UniqueName");
	 	            	sBillToCommon = (String)liCommon.getDottedFieldValue("BillingAddress.UniqueName");
                        Boolean sSmall = (liCommon.getDottedFieldValue("Supplier.SmallBusiness") == null) ? Boolean.FALSE : ((Boolean) liCommon.getDottedFieldValue("Supplier.SmallBusiness"));
                        Boolean	sMinority = (liCommon.getDottedFieldValue("Supplier.MinorityOwnedBusiness") == null) ? Boolean.FALSE : ((Boolean)liCommon.getDottedFieldValue("Supplier.MinorityOwnedBusiness"));
                        Boolean	sWoman = (liCommon.getDottedFieldValue("Supplier.WomanOwnedBusiness") == null) ? Boolean.FALSE : ((Boolean)liCommon.getDottedFieldValue("Supplier.WomanOwnedBusiness"));
                        Boolean sMicro = (liCommon.getDottedFieldValue("Supplier.MicroBusiness") == null) ? Boolean.FALSE : ((Boolean)liCommon.getDottedFieldValue("Supplier.MicroBusiness"));
	 	            	sDeliverToCommon =(String)liCommon.getFieldValue("DeliverTo");
                        String sSWAMType="";
                        if(sSmall.booleanValue())
                        {
                            sSWAMType = "S,";
                        }
                        if(sWoman.booleanValue())
                        {
                            sSWAMType = sSWAMType+ "W,";
                        }
                        if(sMinority.booleanValue())
                        {
                            sSWAMType = sSWAMType+"M,";
                        }
                        if(sMicro.booleanValue())
                        {
                            sSWAMType = sSWAMType+"O,";
                        }
                        if(!StringUtil.nullOrEmptyOrBlankString(sSWAMType))
                        {
                            sSWAMType = sSWAMType.substring(0,sSWAMType.length()-1);
                        }
                        sHdr = PSGFunctions.ReplaceString(sHdr,"<!--SWAMTYPE-->",sSWAMType, true);
                        
	 	            	if(iLine ==0)
	             		{
	             			sShipToTemp = sShipToCommon;
	             			sBillToTemp = sBillToCommon;
	             			sDeliverToTemp = sDeliverToCommon;
	             		}
	             		if(bCommonShipping && !(sShipToTemp.equalsIgnoreCase(sShipToCommon)))
	             		{
	             			bCommonShipping = false;
	             		}
	             		if(bCommonBilling && !(sBillToTemp.equalsIgnoreCase(sBillToCommon)))
	 	            	{
	             			bCommonBilling = false;
	 		            }
	             		if(bCommonDeliverTo && !(sDeliverToTemp.equalsIgnoreCase(sDeliverToCommon)))
	 	            	{
	             			bCommonDeliverTo = false;
	 	            	}
	            	}
	            	Log.customer.debug("bCommonShipping"+bCommonShipping);
	            	Log.customer.debug("bCommonBilling"+bCommonBilling);
	            	Log.customer.debug("bCommonDeliverTo"+bCommonDeliverTo);
		            liCommonLine = (LineItem) vLinesCommon.get(0);
		            sCommonShipTo = shipToBillTo(liCommonLine,sShipTo);
		            sCommonBillTo = shipToBillTo(liCommonLine,sBillTo);
		            String sCommonDeliverTo = shipToBillTo(liCommonLine,sDeliver);
		            if(bCommonShipping== true && bCommonBilling==true)
		            {
		            	Log.customer.debug("Ship To addr and Bill To addr are common");
		            	if(bCommonDeliverTo==true)
		            	{
		            		sCommonShipTo = PSGFunctions.ReplaceString(sCommonShipTo,"<!--DeliverToHdr-->",sCommonDeliverTo, true);
		            	}
		            	sHdr = PSGFunctions.ReplaceString(sHdr,"<!--CommonShipTo-->",sCommonShipTo, true);
		            	sHdr = PSGFunctions.ReplaceString(sHdr,"<!--CommonBillTo-->",sCommonBillTo, true);
		            }
		            if(bCommonShipping== true && bCommonBilling==false)
		            {
		            	Log.customer.debug("shipping is common");
		            	if(bCommonDeliverTo==true)
		            	{
		            		sCommonShipTo = PSGFunctions.ReplaceString(sCommonShipTo,"<!--DeliverToHdr-->",sCommonDeliverTo, true);
		            	}
		            	String sNonCommonBillToHTMLEnd = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","CommonBilling");
		            	sCommonShipTo = sCommonShipTo+sNonCommonBillToHTMLEnd;
		            	sHdr = PSGFunctions.ReplaceString(sHdr,"<!--CommonShipTo-->",sCommonShipTo, true);
		            }
		            if(bCommonShipping== false && bCommonBilling==true)
		            {
		            	Log.customer.debug("Billing is common");
		            	sCommonBillTo = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","NonCommonShipping")+sCommonBillTo;
		            	sHdr = PSGFunctions.ReplaceString(sHdr,"<!--CommonBillTo-->",sCommonBillTo, true);
		            }
	            }
            }
/*(sarath babu) End of Logic for Common billing and Common shipping addresses */

            if (approvable instanceof LineItemCollection)
            {
                sLine = PSGFunctions.ReadFile(sLineFile);
                sCmt = PSGFunctions.ReadFile(sCmtFile);
            }


            // Create Hashtables of the fields we need from the document
            //iTag and iTagEnd as integer with initial values
            iTag = sHdr.lastIndexOf("<!--Hdr.");
            iTagEnd = -1;
            while (iTag >= 0)
            {
                iTagEnd = sHdr.indexOf("-->", iTag + 1);
                sTag = sHdr.substring(iTag + 4, iTagEnd);
                Log.customer.debug("HdrKey: " + sTag);

                hHdrParams.put(sTag, "[?]");
                iTag = sHdr.lastIndexOf("<!--Hdr.", iTag - 1);

            }

            // Put field values into the Hashtables
            eParams = hHdrParams.keys();
            while (eParams.hasMoreElements())
            {
                sTag = eParams.nextElement().toString();
                Log.customer.debug("Print the value of the field sTag" +sTag);
                //oValue = approvable.getDottedFieldValue(sTag.substring(4)); // remove "Hdr." from field name

                /*  11-01-02, eVA Dev SPL#98, Modify eVA print functionality to label Change Orders as such.
                    Check if the field name = 'CHANGEORDER'.  If so, then set the field value based on whether the
                    Order is a Change Order or not.  If not, then set the field value based on the field name passed. */
                String fieldName = sTag.substring(4); // Remove the "Hdr." from the field name

                if (fieldName.compareTo("CHANGEORDER") == 0)
                {
                    oValue = new String();
                    ClusterRoot loCluster = approvable.getPreviousVersion();
                    if (loCluster != null)
                    {
                        if (approvable.instanceOf("ariba.purchasing.core.Requisition"))
                        {
                            String loStatus = (String)loCluster.getFieldValue("StatusString");
                            if (!loStatus.equals("Composing"))
                            {
                                oValue = new String("CHANGE ORDER");
                            }
                        }
                        else
                        {
                            oValue = new String("CHANGE ORDER");
                        }
                    }
                }
                else
                {
                    oValue = approvable.getDottedFieldValue(fieldName); // get the field value for the field name
                    Log.customer.debug("Print the value of the field" +oValue);
                }
                // End of mods for eVA Dev SPL#98

                //if (oValue == null) oValue = "(no value)"; // get NullValueName for field instead?
                if (oValue == null)
                    oValue = ""; // get NullValueName for field instead?

                /*****************************************************
                * Print "null" on the Eform if the new value is
                * null or ""
                ******************************************************/
                if (sReqType.equals("ariba.core.BuysenseOrgEform"))
                {
                    if (oValue == null)
                        sNewValue = "null";
                    else
                        sNewValue = String.valueOf(oValue);

                    if (sNewValue.equals(""))
                        sNewValue = "null";

                    hHdrParams.put(sTag, sNewValue);

                }
                else
                {
                    //ROB Display total cost with 2 decimal places
                    if (sTag.equals ("Hdr.TotalCost.Amount"))
                    {
                        Log.customer.debug("Need to round to 2 decimal places");
                        nf.setMaximumFractionDigits(2);
                        if (oValue == null) oValue = "";
                        hHdrParams.put(sTag, nf.format(oValue));
                        Log.customer.debug("Hdr: " + sTag + "='" + nf.format(oValue) + "'");
                    } //CSPL-4293 Added condition to mask Supplier TIN
                    else
                    if (sTag.equals ("Hdr.LineItems[0].SupplierLocation.TIN"))
                    {
                        Log.customer.debug("Mask Supplier TIN");
                        if (oValue == null)
                        {
                            oValue = "";
                        }else
                        {
                            oValue = SecureStringFormatter.getStringValue(oValue, "XXXXXDDDD", "*");
                        }
                        
                        hHdrParams.put(sTag, oValue);
                        Log.customer.debug("Hdr: " + sTag + "='" + oValue + "'");
                    }
                    else
                    if (sTag.equals ("Hdr.LineItems[0].Accountings.SplitAccountings[0].FieldDefault4.FieldTable.ERPValue"))
                    {
                        Log.customer.debug("Hdr.LineItems[0].Accountings.SplitAccountings[0].FieldDefault4.FieldTable.ERPValue");
                        String sERPValue = ReturnHeaderString(sLine1,approvable);
                        Log.customer.debug("The value return from ReturnHeaderString" +sERPValue );
                        Log.customer.debug("Before assignment" +oValue );
                        if (!StringUtil.nullOrEmptyString(sERPValue) ||  StringUtil.nullOrEmptyString(sTag) )
                        	oValue = sERPValue;
                        Log.customer.debug("After assignment of sERPValue" +oValue );
                        if (sERPValue == null)
                        	oValue = "";
                        hHdrParams.put(sTag, oValue);
                        Log.customer.debug("Hdr: " + sTag + "='" + oValue + "'");
                    }
                    else
                    {
                        // remove "Hdr." from field name
                        if (oValue == null) oValue = "";
                        hHdrParams.put(sTag, String.valueOf(oValue));
                        Log.customer.debug("Hdr: " + sTag + "='" + oValue.toString() + "'");
                    }
                }

            }
            //replace the key with the value in the HTML
            sHdr = PSGFunctions.ReplaceHtml(sHdr, hHdrParams);

            /****************************************************
            * Create Hashtable for the Old Eform values
            *
            *
            *****************************************************/
            if (sReqType.equals("ariba.core.BuysenseOrgEform"))
            {
                // Call the BuysensePrintHTMLEform function
                sHdr = BuysenseOrgEformPrint.Eform(sHdr,
                                                   sReqType,
                                                   approvable,
                                                   eParams,
                                                   hHdrParams, sTag, bNewEform);
                bNewEform = false;
            }//if sReqType


            /****************************************************
            * If the BuysenseEform exist, find and display all
            * the approvers and their approval status
            *
            *****************************************************/


            if ((sReqType.equals("ariba.purchasing.core.Requisition")) ||
                    (sReqType.equals("ariba.core.BuysenseOrgEform")))

            {
                Log.customer.debug("It is a Requisition or BuysenseOrgEform");
                sApprovers=PSGFunctions.ReadFile(sReqAppr);
                Log.customer.debug("File that is read in: "+sApprovers);
                if (sApprovers != null)
                {

                    iTag = sApprovers.lastIndexOf("<!--Appr.");
                    while (iTag >= 0)
                    {
                        iTagEnd = sApprovers.indexOf("-->", iTag + 1);
                        sTag = sApprovers.substring(iTag + 4, iTagEnd);
                        hApprParams.put(sTag, "[?]");
                        iTag = sApprovers.lastIndexOf("<!--Appr.", iTag - 1);
                    }
                    // Ariba 8.1: Replaced deprecated new List() Constructor with ListUtil.newVector();
                    List vecApprovalRequests = ListUtil.list();
                    List lvecApprovalRequests = ListUtil.list();
                    ApprovalRequest appReq = new ApprovalRequest();
                    vecApprovalRequests = (List)approvable.getFieldValue("ApprovalRequests");

                    // Create local vector to avoid modifying real Approval Requests vector
                    // 81->822
                    // lvecApprovalRequests = (Vector) vecApprovalRequests.clone();
                    lvecApprovalRequests = ListUtil.cloneList(vecApprovalRequests);
                    int size = lvecApprovalRequests.size();

                    for (int i = 0; i < size; i ++)
                    {// get all the approval requests and dependencies.
                        // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                        appReq = (ApprovalRequest) lvecApprovalRequests.get(i);
                        lvecApprovalRequests = getDependencies(lvecApprovalRequests, appReq);
                    }
                    size = lvecApprovalRequests.size();

                    for (int j = (size-1); j >= 0; j--)
                    {
                        // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                        appReq = (ApprovalRequest) lvecApprovalRequests.get(j);

                        //hLineItem = new Hashtable();
                        hAppr = new Hashtable();
                        eParams = hApprParams.keys();
                        while (eParams.hasMoreElements())
                        {
                            sTag = eParams.nextElement().toString();
                            oValue = appReq.getDottedFieldValue(sTag.substring(5)); // remove       "Line."                                 from field name
                            String sSubString="";
                            sSubString=sTag.substring(5);
                            //if (oValue == null) oValue = "(no value)"; // get NullValueName       for field instead?
                            if (oValue == null)
                                oValue = ""; // get NullValueName for field         instead?

                            if (sSubString.equals("ApprovalRequired"))
                            {
                                String soValue=String.valueOf(oValue);
                                if (soValue.equals("true"))
                                {
                                    hAppr.put(sTag, "Yes");
                                }
                                else if (soValue.equals("false"))
                                {
                                    hAppr.put(sTag, "No");
                                }
                            }
                            else if (sSubString.equals("State"))
                            {
                                String soValue=String.valueOf(oValue);
                                if (soValue.equals("1"))
                                {
                                    hAppr.put(sTag, "Pending");
                                }
                                else if (soValue.equals("2"))
                                {
                                    hAppr.put(sTag, "Pending");
                                }
                                else if (soValue.equals("4"))
                                {
                                    hAppr.put(sTag, "Denied");
                                }
                                else if (soValue.equals("8"))
                                {
                                    hAppr.put(sTag, "Approved");
                                }
                            }
                            else
                            {
                                hAppr.put(sTag, String.valueOf(oValue));
                                Log.customer.debug("APPR: " + sTag + "='" + oValue.toString() + "'");
                            }
                        }
                        sTotalApprovers += PSGFunctions.ReplaceHtml(sApprovers,
                                                                    hAppr);
                    }
                    sHdr = PSGFunctions.ReplaceString(sHdr,
                                                      "<!--Approvers-->",
                                                      sTotalApprovers, true);
                }
            }
            if (sLine != null)
            {
            	String sLineItemsLine = lineItemsString(sLine,approvable,"line");
            	Log.customer.debug("Get the details for line items and frema the Html ontent");
            	sHdr = PSGFunctions.ReplaceString(sHdr,"<!--LineItems-->",sLineItemsLine, true);
            	
            }

            if (sCmt != null)
            {
                // Now do the same for Comments
                iTag = sCmt.lastIndexOf("<!--Cmt.");
                while (iTag >= 0)
                {
                    iTagEnd = sCmt.indexOf("-->", iTag + 1);
                    sTag = sCmt.substring(iTag + 4, iTagEnd);
                    hCmtParams.put(sTag, "[?]");
                    iTag = sCmt.lastIndexOf("<!--Cmt.", iTag - 1);
                    Log.customer.debug("CmtKey: " + sTag);
                }

                //Log.fixme.M("Casting approvable (" + approvable.typeName() + ") as LineItemCollection...");
                List vComments = ((LineItemCollection) approvable).getComments();
                Comment cm = null;
                // Looping through the vector, convert each element from a Comment to a String of HTML.
                for (int iCmt = 0; iCmt < vComments.size(); iCmt ++)
                {
                    // Ariba 8.1: Replaced deprecated elementAt() method with get() method.
                    //Log.fixme.M("Casting Object (" + vLines.get(iLine).getClass().getName() + ") as LineItem...");
                    cm = (Comment) vComments.get(iCmt);
                    hComment = new Hashtable();
                    eParams = hCmtParams.keys();
                    while (eParams.hasMoreElements())
                    {
                        sTag = eParams.nextElement().toString();

                        // Resolving the issue to allow comments more than 255 character
                        String sLongString = "";
                        if(sTag.substring(4).compareTo("Text.Strings[].String")==0)
                        {
		                    List vCmt = cm.getText().getStrings();
		                    LongStringElement sLongElement=null;
		                    for(int iCmtText = 0; iCmtText < vCmt.size(); iCmtText++)
		                    {
		                        sLongElement = (LongStringElement) vCmt.get(iCmtText);
		                        /*Partho: CSPL-4235 Modified the string to replace "\r\n" with "<BR>" to show line break acurately on HTML Page*/
		                        sLongString += sLongElement.getString().replaceAll("\r\n", "<BR>");
		                    }
		                    oValue = sLongString;
		                }
                        else
                        {
                        	oValue = cm.getDottedFieldValue(sTag.substring(4)); // remove "Cmt." from field name
                        	//if (oValue == null) oValue = "(no value)"; // get NullValueName for field instead?
                        }
                        if (oValue == null)
                            oValue = ""; // get NullValueName for field instead?
                        hComment.put(sTag, String.valueOf(oValue));
                        Log.customer.debug("Comment: " + sTag + "='" + oValue.toString() + "'");
                    }
                    sComment = sCmt;
                    sComments += PSGFunctions.ReplaceHtml(sComment, hComment);
                }

                // Finally, insert the LineItem HTML into the Header HTML
                sHdr = PSGFunctions.ReplaceString(sHdr,
                                                  "<!--Comments-->",
                                                  sComments, true);
            }

            hout.println(sHdr);
            Log.customer.debug("TestPrintHTMLApprovableHook Done.");
            return NoErrorResult;

        }
        catch (PSGException px)
        {
            // Ariba 8.1: Replaced deprecated Util class with ListUtil class and Constants class.
            List vError = ListUtil.list(Constants.getInteger(-1));
            Log.customer.debug("TestPrintHTMLApprovableHook PSGException: " + px);
            // Ariba 8.1: Replaced deprecated addElement() method with add() method.
            vError.add(px);
            px.printStackTrace(System.out);
            return vError;
        }
        catch (Exception xx)
        {
            // Ariba 8.1: Replaced deprecated Util class with ListUtil class and Constants class.
            List vError = ListUtil.list(Constants.getInteger(-2));
            Log.customer.debug("TestPrintHTMLApprovableHook Exception: " + xx);
            // Ariba 8.1: Replaced deprecated addElement() method with add() method.
            vError.add(xx);
            xx.printStackTrace(System.out);
            return vError;
        }
    }
    /**
     * @author sarath.babugarre
     * @date 05/05/2009
     * @param sLine
     * @param approvable
     * @param sValidation
     * @explanation lineItemsString method will get the line item details and format the data in a string,
     *              which can be given as a input to PSGFunctions.ReplaceString method(CSPL-920).
     */
    public String lineItemsString(String sLine, Approvable approvable, String sValidation)
    {
    	Log.customer.debug("Inside lineItems String method");
    	int iTagLine = 0,iTagLineEnd = -1;
    	String sTag=null,sLineItem=null,sLineItems="";
    	Object oValue=null;
    	String sName = null;
    	String sERPval = null;
    	Hashtable hLineParams = new Hashtable(),hLineItem = new Hashtable();
    	Enumeration eParams= null;
    	NumberFormat nf = NumberFormat.getInstance();
        iTagLine = sLine.lastIndexOf("<!--Line.");
        while (iTagLine >= 0)
        {
        	Log.customer.debug("Inside While in line items string");
            iTagLineEnd = sLine.indexOf("-->", iTagLine + 1);
            sTag = sLine.substring(iTagLine + 4, iTagLineEnd);
            hLineParams.put(sTag, "[?]");
            iTagLine = sLine.lastIndexOf("<!--Line.", iTagLine - 1);
            Log.customer.debug("LineKey: " + sTag);
        }

        List vLines = ((LineItemCollection) approvable).getLineItems();
        LineItem li = null;

        // Looping through the vector, convert each element from a LineItem to a String of HTML.
	        for (int iLine = 0; iLine < vLines.size(); iLine ++)
	        {
	        	Log.customer.debug("Inside for of line items string");
	           	li = (LineItem) vLines.get(iLine);
	            hLineItem = new Hashtable();
	            eParams = hLineParams.keys();
	            while (eParams.hasMoreElements())
	            {
	            	Log.customer.debug("Inside While inside while");
	                sTag = eParams.nextElement().toString();
	                Log.customer.debug("Inside stag value" + sTag);
	                oValue = li.getDottedFieldValue(sTag.substring(5)); // remove "Line." from field name
	                Log.customer.debug("Inside eParams value" + oValue);
	                if (oValue == null)
	                    oValue = ""; // replace NullValue with blank
	                //ROB Display total cost with 2 decimal places
	                if (sTag.equals ("Line.Amount.Amount"))
	                {
	                    Log.customer.debug("Need to round to 2 decimal places");
	                    nf.setMaximumFractionDigits(2);
	                    if (oValue == null) oValue = "";
	                    hLineItem.put(sTag, nf.format(oValue));
	                    Log.customer.debug("Line: " + sTag + "='" + nf.format(oValue) + "'");
	                }
	                else if (sTag.equals ("Line.Description.Price.Amount"))
	                {
	                    Log.customer.debug("Need to round to 5 decimal places");
	                    nf.setMaximumFractionDigits(5);
	                    if (oValue == null) oValue = "";
	                    hLineItem.put(sTag, nf.format(oValue));
	                    Log.customer.debug("Line: " + sTag + "='" + nf.format(oValue) + "'");
	                }
	                else if (sTag.equals ("Line.Quantity"))
	                {
	                    nf.setMaximumFractionDigits(5);
	                    if (oValue == null) oValue = "";
	                    hLineItem.put(sTag, nf.format(oValue));
	                    Log.customer.debug("Line: " + sTag + "='" + nf.format(oValue) + "'");
	                }
	                else if ( !StringUtil.nullOrEmptyString(sTag) && sTag.equals ("Line.Accountings.SplitAccountings[0].FieldDefault4.Name") )
	                {

	                    Boolean bActive = (Boolean) li.getDottedFieldValue("Accountings.SplitAccountings[0].FieldDefault4.Active");
	                    Log.customer.debug("Accountings.SplitAccountings[0].FieldDefault4.Active" +bActive);
	                    sName = (String)li.getDottedFieldValue("Accountings.SplitAccountings[0].FieldDefault18.Name");
	                    Log.customer.debug("Accountings.SplitAccountings[0].FieldDefault18.Name" +sName);
	                    if ( bActive == null || oValue == null || !bActive.booleanValue())
	                    	oValue = sName;
                        if (oValue == null)
	                    	oValue = "";
	                    hLineItem.put(sTag,oValue );
	                    //sERPValue = (String) oValue ;
	                    Log.customer.debug("Line: " + sTag + "='" + oValue + "'");
	                }
	                else
	                {
	                    // remove "Hdr." from field name
	                    if (oValue == null)
	                        oValue = ""; // get NullValueName for field instead?
	                    hLineItem.put(sTag, String.valueOf(oValue));
	                    Log.customer.debug("Line: " + sTag + "='" + oValue.toString() + "'");
	                }
	            }
	            if(sValidation.equals("line"))
	            {
	            	Log.customer.debug("Inside if (line)");
	            	String sLineShipTo = shipToBillTo(li,sShipTo);
	                String sLineBillTo = shipToBillTo(li,sBillTo);
	                String sCommonOrderNo = shipToBillTo(li,sOrderNo);
	                String sLineItemCommShipTemp=sLine,sLineItemCommBillTemp=sLine;
	                String sLineDeliverTo = shipToBillTo(li,sDeliver);
	                String sCommonShip = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","NonCommonShipping");
	                String sCommonBillToHTMLEnd = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","CommonBilling");
	                String sCommonShipdiffDeliver = PSGFunctions.ReplaceString(sCommonShip,"<!--DeliverToHdr-->",sLineDeliverTo, true);
	                if(bCommonShipping == true && bCommonBilling == false)
	                {
	                	if (bCommonDeliverTo==false)
	                	{
	                		if(approvable.instanceOf("ariba.purchasing.core.Requisition"))
	                		{
	                			sCommonShipdiffDeliver = PSGFunctions.ReplaceString(sCommonShipdiffDeliver,"<!--OrderNo-->",sCommonOrderNo,true);
	                			sLineBillTo = sCommonShipdiffDeliver+sLineBillTo;
	                		}
	                		else
	                		{
	                			sLineBillTo = sCommonShipdiffDeliver+sLineBillTo;
	                		}
	                	}
	                	else
	                	{
	                		if(approvable.instanceOf("ariba.purchasing.core.Requisition"))
	                		{
	                			sCommonShip = PSGFunctions.ReplaceString(sCommonShip,"<!--OrderNo-->",sCommonOrderNo,true);
	                			sLineBillTo = sCommonShip+sLineBillTo;
	                		}
	                		else
	                		{
	                			sLineBillTo = sCommonShip+sLineBillTo;
	                		}

	                	}
	                	sLineItemCommShipTemp = PSGFunctions.ReplaceString(sLineItemCommShipTemp,"<!--CommonBillTo-->",sLineBillTo, true);
	                	sLineItem =sLineItemCommShipTemp;
	                }
	                else if(bCommonShipping == false && bCommonBilling == true)
	                {
	                	sLineShipTo = PSGFunctions.ReplaceString(sLineShipTo,"<!--DeliverToHdr-->",sLineDeliverTo, true);
	                	if (approvable.instanceOf("ariba.purchasing.core.Requisition"))
	                	{
	                		sLineShipTo = PSGFunctions.ReplaceString(sLineShipTo,"<!--OrderNo-->",sCommonOrderNo, true);
	                		sLineShipTo = sLineShipTo+sCommonBillToHTMLEnd;

	                	}
	                	else
	                	{
	                		sLineShipTo = sLineShipTo+sCommonBillToHTMLEnd;
	                	}
	                	sLineItemCommBillTemp = PSGFunctions.ReplaceString(sLineItemCommBillTemp,"<!--CommonShipTo-->",sLineShipTo, true);
	                	sLineItem =sLineItemCommBillTemp;
	                }
	                else if(bCommonShipping == false && bCommonBilling == false)
	                {
	                	sLineShipTo = PSGFunctions.ReplaceString(sLineShipTo,"<!--DeliverToHdr-->",sLineDeliverTo, true);
	                	if (approvable.instanceOf("ariba.purchasing.core.Requisition"))
	                	{
	                		sLineShipTo = PSGFunctions.ReplaceString(sLineShipTo,"<!--OrderNo-->",sCommonOrderNo, true);
	                	}
	                	sLineItemCommShipTemp = PSGFunctions.ReplaceString(sLineItemCommShipTemp,"<!--CommonShipTo-->",sLineShipTo, true);
	                	sLineItemCommBillTemp = PSGFunctions.ReplaceString(sLineItemCommShipTemp,"<!--CommonBillTo-->",sLineBillTo, true);
	                	sLineItem=sLineItemCommBillTemp;
	                }
	                else if(bCommonShipping == true && bCommonBilling == true && bCommonDeliverTo==false)
	                {
	                	if (approvable.instanceOf("ariba.purchasing.core.Requisition"))
	                	{
	                		sCommonShipdiffDeliver = PSGFunctions.ReplaceString(sCommonShipdiffDeliver,"<!--OrderNo-->",sCommonOrderNo, true);
	                		sCommonShipdiffDeliver=sCommonShipdiffDeliver+sCommonBillToHTMLEnd;
	                	}
	                	else
	                	{
	                		sCommonShipdiffDeliver=sCommonShipdiffDeliver+sCommonBillToHTMLEnd;
	                	}
	                	sLineItemCommBillTemp = PSGFunctions.ReplaceString(sLineItemCommShipTemp,"<!--CommonBillTo-->",sCommonShipdiffDeliver, true);
	                	sLineItem=sLineItemCommBillTemp;
	                }
	                else
	                {
	                	if (approvable.instanceOf("ariba.purchasing.core.Requisition"))
	                	{
	                		sCommonShip = PSGFunctions.ReplaceString(sCommonShip,"<!--OrderNo-->",sCommonOrderNo, true);
	                	    sLineItem = sLine+sCommonShip+sCommonBillToHTMLEnd;
	                	}
	                	else
	                	{
	                		sLineItem = sLine;
	                	}
	                }
	                sLineItems += PSGFunctions.ReplaceHtml(sLineItem,hLineItem);
	            }
	            Log.customer.debug("exiting line items string");
	        }
	        return sLineItems;
    }
    
    public String ReturnHeaderString (String sLine1, Approvable approvable)
    {
    	Log.customer.debug("Inside ReturnHeaderstring class");
    	List vLines = ((LineItemCollection) approvable).getLineItems();
        LineItem li = null;
        String sErpval = "";
        // Looping through the vector, convert each element from a LineItem to a String of HTML.
	        for (int iLine = 0; iLine < vLines.size(); iLine ++)
	        {
	        	Log.customer.debug("Inside for of ReturnHeaderstring class");
	        	
	           	li = (LineItem) vLines.get(iLine);
	           	Boolean bActive = (Boolean) li.getDottedFieldValue("Accountings.SplitAccountings[0].FieldDefault4.Active");
	           	if ( bActive == null || !bActive.booleanValue())
	           		
	           	sErpval = (String) li.getDottedFieldValue("Accountings.SplitAccountings[0].FieldDefault18.FieldTable.ERPValue");
	           	
	           	if (sErpval == null)
	           		sErpval = "";
	           	return sErpval;  	
	        }
    	Log.customer.debug("exiting ReturnHeaderString method");
    	return sErpval;
    }
    /**
     * @author sarath.babugarre
     * @date 05/05/2009
     * @param li
     * @param sFileName
     * @explanation shipToBillTo method will get the required HTML string for the input string with the values (CSPL-929)
     */
    public String shipToBillTo(LineItem li, String sHTMLString)
    {
    	Log.customer.debug("Inside ShipToBillTo");int iTag = 0;
    	int iTagEnd = -1;
        String sTag=null;
        Hashtable hCommonAddress = new Hashtable();
        iTag = sHTMLString.lastIndexOf("<!--Line.");
        while (iTag >= 0)
        {
        	iTagEnd = sHTMLString.indexOf("-->", iTag + 1);
            sTag = sHTMLString.substring(iTag + 4, iTagEnd);
            Log.customer.debug("HdrKey: " + sTag);
            hCommonAddress.put(sTag, "[?]");
            iTag = sHTMLString.lastIndexOf("<!--Line.", iTag - 1);
        }
        Enumeration eParams = hCommonAddress.keys();
        while (eParams.hasMoreElements())
        {
        	sTag = eParams.nextElement().toString();
            String sFieldName = sTag.substring(5);
            Object oValue = li.getDottedFieldValue(sFieldName);
            if (oValue == null)
            {
               oValue = "";
		    }
            if(sFieldName.equalsIgnoreCase("BillingAddress.Lines") || sFieldName.equalsIgnoreCase("ShipTo.Lines") )
            {
                oValue = ((String)oValue).replace("\n", "<BR>");
            }
            hCommonAddress.put(sTag, String.valueOf(oValue));
        }
        sHTMLString = PSGFunctions.ReplaceHtml(sHTMLString, hCommonAddress);
        return sHTMLString;
    }

        public List getDependencies(List lvec, ApprovalRequest appReq)
    {
        // Ariba 8.1: Replaced deprecated new List() Constructor with ListUtil.newVector();
        List vecNewDependencies = ListUtil.list();
        ApprovalRequest newDependency = new ApprovalRequest();
        vecNewDependencies = (List)appReq.getFieldValue("Dependencies");
        int size = vecNewDependencies.size();

        for (int i = 0; i < size; i ++)
        {
            newDependency = (ApprovalRequest) vecNewDependencies.get(i);
            if (lvec.contains(newDependency))
            {
                continue;
            }
            else
            {
                lvec.add(newDependency);
                getDependencies(lvec, newDependency);
            }
        }
        return lvec;
    }

}
