//
//***************************************************************************/
//  Anup - Dec 2003 - Solicitation Line validation on requisition copy.
//
//***************************************************************************/

package config.java.ams.custom;

import ariba.purchasing.core.Requisition;
import ariba.purchasing.core.ReqLineItem;
import ariba.approvable.core.condition.ValidLineItem;
import ariba.base.core.*;
import ariba.base.fields.*;
import ariba.util.core.*;
// eProcure ST SPL #1332 - Added logic to not do the Solicitation Check if the req is a changed req
import ariba.approvable.core.LineItemCollection;
import ariba.util.log.Log;

public class AMSSolLineValidation extends ValidLineItem
{
   boolean mboolSolLine = false;
   String msSolParam = "Application.AMSParameters.SolicitationVendor";

   public boolean evaluate(Object value, PropertyTable params)
   {
      boolean lboolReturn = true;
      if(value == null || ! (value instanceof ReqLineItem) )
          return lboolReturn;

      ReqLineItem loLineItem = (ReqLineItem)value;
      ariba.approvable.core.LineItemCollection loCollection = loLineItem.getLineItemCollection();
      String lsStatus = loCollection.getStatusString();
      if (lsStatus.trim().equalsIgnoreCase("Composing") &&
          isSolicitationVendor(loLineItem, loCollection.getPartition(), msSolParam))
      {
         Log.customer.debug("The Supplier on the ReqLineItem is the SolicitationVendor");
         /* eProcure ST SPL #1332 - Commented out previous logic and added logic as follows to check if the req is a changed req.
            If the Supplier for the ReqLineItem is the SolicitationVendor, check to see if this is a ChangeOrder and issue
            the SolicitationVendorError if any of the following scenarios are met:
            (1) This is not a changed requisition
            (2) This is a changed req but this line item is new
            (3) This is a changed req and the previous line item was not for the SolicationVendor
         */
         //mboolSolLine = true;
         //lboolReturn = false;
         if( !isChangeReq(loLineItem) ||
            (isChangeReq(loLineItem) &&
             !isOrigLineItemSolicitation(loLineItem, loCollection, loCollection.getPartition(), msSolParam)))
         {
             mboolSolLine = true;
             lboolReturn = false;
         }
      }
      return lboolReturn;
    }

    private boolean isSolicitationVendor ( ReqLineItem foReqLine, Partition foPartition, String fsSolParam )
    {
        String lsSolicitationVendor = Base.getService().getParameter(foPartition, fsSolParam);
        String lsSupplier = "";
        if (foReqLine.getSupplier() != null)
        {
            lsSupplier = foReqLine.getSupplier().getUniqueName();
        }
        return lsSupplier.equals(lsSolicitationVendor);
    }

    // eProcure ST SPL #1332 - Added new method to check if the req is a changed req.
    private boolean isChangeReq (ReqLineItem foReqLine)
    {
        ariba.approvable.core.LineItemCollection loCollection = foReqLine.getLineItemCollection();
        ClusterRoot loCluster = loCollection.getPreviousVersion();
        if ( foReqLine.getFieldValue("OldValues") != null && loCluster != null )
        {
            String lsStatus = (String)loCluster.getFieldValue("StatusString");
            if (lsStatus.equals("Composing"))
            {   
               Log.util.debug( "Previous version in composing ... must have had integration errors");
            }
            else
            {
               Log.util.debug( "This is a changed line item") ;
               return true;
            }
        }
        Log.util.debug( "This is not a changed line item - issue SolicitationVendor error") ;
        return false;
    }

    // eProcure ST SPL #1332 - Added new method to check if the original req line item was sent to ADV for solicitation.
    private boolean isOrigLineItemSolicitation (ReqLineItem foReqLine, LineItemCollection foReq, Partition foPartition, String fsSolParam)
    {
        boolean lbSolLine = false;
        Requisition loReq = (Requisition)foReq.getDottedFieldValue("PreviousVersion");
        int liNum = ((Integer)foReqLine.getFieldValue("NumberInCollection")).intValue();
        ariba.approvable.core.LineItem loPrevItem = loReq.getLineItem(liNum);
        if ( loPrevItem != null) {
            Log.customer.debug( "Check to see if the original line item has the SolicitationVendor") ;
            if (isSolicitationVendor((ReqLineItem)loPrevItem, foPartition, fsSolParam)) {
                Log.customer.debug( "This is an existing ADV line item - do not issue SolicitationVendor error") ;
                lbSolLine = true;
            }
        }

        return lbSolLine;
    }
    
    public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
    {
       if (!evaluate(value, params))
       {
          if(mboolSolLine)
          {
             //return new ConditionResult(Fmt.Sil("ariba.procure.core", "ComposingLineWithSolicitationVendor"));
             return new ConditionResult(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core", "ComposingLineWithSolicitationVendor"));
          }
       }
       return super.evaluateAndExplain(value, params);
    }
}
