package config.java.ams.custom;


import ariba.approvable.core.Approvable;
import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.basic.core.Currency;
import ariba.basic.core.Money;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseDynamicEformSetTotalCost extends Action
{
    String msClassName = "BuysenseDynamicEformSetTotalCost";

    public void fire(ValueSource valuesource, PropertyTable param) throws ActionExecutionException
    {
        Approvable loAppr = (Approvable) valuesource;
        Partition partition = Base.getService().getPartition("pcsv");
        Money loTotalCost = new Money(0.0,Currency.getDefaultCurrency(partition));
        
        for(int i=1; i<=10; i++)
        {
            String lsFieldName = "EformTable"+i+"Field6";
            Money loRowAmount = (Money) loAppr.getDottedFieldValue(lsFieldName);
            if(loRowAmount != null)
            {
                loTotalCost=loTotalCost.add(loRowAmount);
            }
        }
        loAppr.setFieldValue("TotalCost", loTotalCost);
    }
}
