package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.BaseObject;
import ariba.base.core.Partition;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseDynamicEformUpdateTitle extends Action{
	
    public void fire(ValueSource valuesource, PropertyTable propertytable)
            throws ActionExecutionException {
    	 
    	 String lsClientName = null;
         Log.customer.debug("BuysenseDynamicEformUpdateTitle: fire method called");
         Approvable loAppr = null;
         if (valuesource != null && valuesource instanceof Approvable) {
         loAppr = (Approvable) valuesource;
         BaseObject loEformObj = (BaseObject)loAppr;
         String lsEformTitle = (String)loAppr.getFieldValue("Name");
         String lsProfileName = (String)loAppr.getDottedFieldValue("EformChooser.ProfileName");
         String lsTitlePrefix = null;
         String lsTitle = null;
         
         Partition loPartition = loAppr.getPartition();
         //User loAribaSystemUser = Core.getService().getAribaSystemUser(loPartition);
         if((ariba.user.core.User)loAppr.getDottedFieldValue("Requester") != null)
         {
 	       ariba.user.core.User RequesterUser = (ariba.user.core.User)loAppr.getDottedFieldValue("Requester");
 	       Partition appPartition = loAppr.getPartition();
 	       ariba.common.core.User loAribaSystemUser = ariba.common.core.User.getPartitionedUser(RequesterUser,appPartition);
 	       lsClientName = (String)loAribaSystemUser.getDottedFieldValue("ClientName.ClientName"); 
 	     }
         if(lsProfileName != null)
         {
         lsTitlePrefix = lsClientName + " - " +lsProfileName;
         if(!(lsEformTitle.contains(lsTitlePrefix)))
         {
        		 Log.customer.debug("Not finding the exact prefix not '-' and '_' exist: so adding the prefix once again");
        	     lsTitle = lsTitlePrefix + "_" + lsEformTitle;         
                 loAppr.setFieldValue("Name", lsTitle);       		 
        	 }
         }
         }
       }
     }
