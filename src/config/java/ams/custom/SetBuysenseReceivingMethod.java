package config.java.ams.custom;

import ariba.base.fields.Action;
import ariba.base.fields.ValueSource;
import ariba.purchasing.core.POLineItem;
import ariba.purchasing.core.ReqLineItem;
import ariba.receiving.core.ReceiptItem;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class SetBuysenseReceivingMethod extends Action
{

    public void fire(ValueSource object, PropertyTable params)
    {
        Log.customer.debug("SetBuysenseReceivingMethod::passed object %s", object);
        if(object != null)
        {
        	int iReceivingType= 2;
        	
        	if(object instanceof ReqLineItem || object instanceof POLineItem)
	        {
	        	iReceivingType = ((Integer) object.getFieldValue("BuysenseReceivingType")).intValue();
	        }
        	else if(object instanceof ReceiptItem)
        	{
        		iReceivingType = ((Integer) object.getFieldValue("ReceivingType")).intValue();
        		object.setFieldValue("BuysenseReceivingType", iReceivingType);
        	}
	
        	if(iReceivingType == 3)
	        {
	        	object.setFieldValue("BuysenseReceivingMethod", "Amount");
	       	}
	        else
	        {
	        	object.setFieldValue("BuysenseReceivingMethod", "Quantity");        		
	        }
	        	Log.customer.debug("SetBuysenseReceivingMethod::final set value %s", object.getFieldValue("BuysenseReceivingMethod"));
        }
	}
}
