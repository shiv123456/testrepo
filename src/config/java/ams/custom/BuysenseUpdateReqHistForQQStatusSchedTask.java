package config.java.ams.custom;

import java.io.FileInputStream;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import com.cgi.evamq.common.EvaQueueMessage;
import com.google.gson.Gson;

import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.common.core.Core;
import ariba.common.core.User;
import ariba.purchasing.core.Requisition;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;
import ariba.util.scheduler.ScheduledTask;
import ariba.util.scheduler.Scheduler;

public class BuysenseUpdateReqHistForQQStatusSchedTask extends ScheduledTask
{
    // fetch data from MQ - EvaMessageQueueNameQQStatus
    // parse the req # and QQ status message
    // update history message for that req # using QQ status message

    private Partition foPartition = null;
    private static boolean bMQThreadStarted = false;

    private ConnectionFactory factory = null;
    private Connection connection = null;
    private Session session = null;
    private Destination destination = null;
    private MessageConsumer consumer = null;
    EvaQueueMessage loQueueMessage;
    Gson gson = new Gson();
    String sClassName = "BuysenseUpdateReqHistForQQStatusSchedTask";
    
    public void init(Scheduler scheduler, String fsScheduledTaskName, Map foArguments)
    {
        super.init(scheduler, fsScheduledTaskName, foArguments);
        foPartition = Base.getSession().getPartition();
    }

    public void run()
    {

        try
        {

            if (!bMQThreadStarted)
            {

                Log.customer.debug(sClassName + " in run() method");

                // Create a ConnectionFactory
                BuysenseUpdateReqHistForQQStatus QQUpdateStatus = new BuysenseUpdateReqHistForQQStatus();
                factory = new ActiveMQConnectionFactory(QQUpdateStatus.getEndPoint());
                connection = factory.createConnection();
                connection.start();
                session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                destination = session.createQueue(QQUpdateStatus.getQueueName());
                consumer = session.createConsumer(destination);

                for (;;)
                {
                    Log.customer.debug(sClassName+ " Waiting for message.");
                    Message message = consumer.receive(1000);
                    if (message == null)
                    {
                        break;
                    }
                    if (message instanceof TextMessage)
                    {
                        TextMessage textMessage = (TextMessage) message;
                        String text = textMessage.getText();
                        Log.customer.debug(sClassName + " Received status message: " + text); // retrieves the message text
                        String reqUniqueName;
                        String qqName;
                        loQueueMessage = gson.fromJson(text, EvaQueueMessage.class);
                        //reqUniqueName = loQueueMessage.getDocSourceID();
                        //qqName = loQueueMessage.getObjOriginID();
                        qqName = loQueueMessage.getDocSourceID();
                        reqUniqueName = loQueueMessage.getObjOriginID();
                        String sOtherPayLoad = loQueueMessage.getEvaOtherPayLoad(); 
                        /*
                         *{"MsgCreator":"QUICKQUOTE","ObjOrigin":"QUICKQUOTE","ObjOriginID":"PR500377","DocSourceID":"EVA001_QQ027894",
                         *"DocUserID":"mmike","EvaOtherPayLoad":"QQCreated;EVA001_QQ027894 has been created for %UID%"}
                         */
                        Log.customer.debug(sClassName + " Message data - reqUniqueName:" + reqUniqueName + ", qqName:" + qqName + ", lsRecordType : " + sOtherPayLoad);
                        //String lsMessage = Fmt.Sil(Base.getSession().getLocale(),"buysense.procure.core", "ReqCreatedTypeRecordDetails",lsReqUN);
                        
                        if(!StringUtil.nullOrEmptyOrBlankString(sOtherPayLoad))
                        {
                            int i = sOtherPayLoad.indexOf(";");
                            String lsRecordType = i<=0?"QQReqStatusRecordType":sOtherPayLoad.substring(0,i);
                            String sRecText = sOtherPayLoad.substring(i+1,sOtherPayLoad.length());
                            Log.customer.debug(sClassName + " lsRecordType:"+lsRecordType+", sRecText: "+sRecText);
                            Requisition req = (Requisition) Base.getService().objectMatchingUniqueName("ariba.purchasing.core.Requisition", foPartition, reqUniqueName);
                            User loAribaSystemUser = Core.getService().getAribaSystemUser(foPartition);
                            BuysenseUtil.createHistory(req, reqUniqueName, lsRecordType, sRecText, loAribaSystemUser);                            
                        }

                    }
                    else
                    {
                        Log.customer.warning(8889, sClassName + " Received a non-text mesage: " + message);
                    }

                }
                consumer.close();
                session.close();
                connection.close();
                
            }
        }
        catch (Exception loExcep)
        {
            Log.customer.error(sClassName + " Exception while consuming or processing QQ status message - "+loExcep.getMessage());
            loExcep.printStackTrace();
        }
        finally
        {
            try
            {
                consumer.close();
                session.close();
                connection.close();
            }
            catch (Exception ex)
            {
            }
        }
    }

}