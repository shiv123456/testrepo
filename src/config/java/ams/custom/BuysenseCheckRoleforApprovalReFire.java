package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseVector;
import ariba.base.core.Partition;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.user.core.Group;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;

import java.util.List;
import ariba.util.core.ListUtil;
import ariba.util.log.Log;
import ariba.base.core.ClusterRoot;

public class BuysenseCheckRoleforApprovalReFire extends Condition
{

    private static String lsGrpToCheck  = "eVA_NoReApprovalwithApproverEdit"; //$NON-NLS-1$
    private static String lsGrpToCheck2 = "eVA-RefireWorkflow_None";         // CSPL-5993 JB //$NON-NLS-1$

    public boolean evaluate(Object paramObject, PropertyTable paramPropertyTable) throws ConditionEvaluationException
    {

        ariba.user.core.User currentEffectiveUser = (ariba.user.core.User) (Base.getSession().getEffectiveUser());
        ariba.common.core.User currentUser = ariba.common.core.User.getPartitionedUser(currentEffectiveUser, Base.getSession().getPartition());
        String currentUserName = (String) currentUser.getFieldValue("UniqueName"); //$NON-NLS-1$

        Partition nonePartition = Base.getService().getPartition("None");
        ClusterRoot oSharedUser = Base.getService().objectMatchingUniqueName("ariba.user.core.User", nonePartition, currentUserName);

        List lAllGroups = (List) oSharedUser.getDottedFieldValue("Groups");

        if(lAllGroups == null)
        {
            return false; // no grps found
        }

        for (int i = 0; i < lAllGroups.size(); i++)
        {
            // Ariba 8.1: List::elementAt() is deprecated by List::get()
            BaseId groupBID = (BaseId) lAllGroups.get(i);
            ClusterRoot group = groupBID.get();
            String grpName = (String) (group.getFieldValue("UniqueName")); //$NON-NLS-1$

            if (grpName.equalsIgnoreCase(lsGrpToCheck) || grpName .equalsIgnoreCase(lsGrpToCheck2))
            {
                Log.customer.debug("In BuysenseCheckRoleforApprovalReFire: role match found for user:" + currentUserName); //$NON-NLS-1$
                return true;
            }
        }

        Log.customer.debug("In BuysenseCheckRoleforApprovalReFire: no role match found for user:" + currentUserName); //$NON-NLS-1$

        return false;
    }

}
