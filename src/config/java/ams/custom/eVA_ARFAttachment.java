package config.java.ams.custom;

import ariba.base.fields.ValueSource;
import ariba.htmlui.workforce.fields.ARFAttachment;
import ariba.util.log.Log;

public class eVA_ARFAttachment extends ARFAttachment 
{
    public String inputView(String group, ValueSource context)

    {    	
    	try
    	{
        	Log.customer.debug("In eVA_ARFAttachment.outputView()- try, returning eVA_ARVAttachments ");
        	// update with proper package name
            return "config.java.ams.custom.eVA_ARVAttachment";
    	}catch(Exception e)
    	{
    		Log.customer.debug("In eVA_ARFAttachment.outputView()- caught Exception "+
    				"while returning config.java.ams.custom.eVA_ARVAttachment"
    				+e.getMessage());
    	}
    	return null;
    }

}
