/*
 * @(#)BuysenseXMLImportException.java  1.00 05/14/2001
 *
 * Copyright (c) 2001 American Management Systems (AMS)
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of AMS
 * ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with AMS.
 */
package config.java.ams.custom ;

import org.xml.sax.* ;

/**
 * Class representing exceptions thrown by the Buysense XML Import/Export classes.
 * 
 * @author Greg Hodum
 * @version 1.00 May 14 2001
 */
public class BuysenseXMLImportException extends SAXException
{
    private int miErrorCode = 0;

    /**
     * This exception is thrown by the Buysense XML Import/Export classes.
     */
    BuysenseXMLImportException( Exception foException )
    {
        super( foException ) ;
    }
    
    
    /**
     * This exception is thrown by the Buysense XML Import/Export classes.
     */
    BuysenseXMLImportException( String fsMessage )
    {
        super( fsMessage ) ;
    }
    
    /**
     * This exception is thrown by the Buysense XML Import/Export classes.
     */
    BuysenseXMLImportException( String fsMessage, Exception foException )
    {
        super( fsMessage, foException ) ;
    }

    /**
     * This exception is thrown by the Buysense XML Import/Export classes.
     */
    BuysenseXMLImportException( String fsMessage, int fiErrorCode )
    {
        super( fsMessage ) ;
        this.miErrorCode = fiErrorCode ;
    }
    
    // Setter for Error Code
    public void setErrorCode(int fiErrorCode)
    {
       this.miErrorCode = fiErrorCode;
    }

    // Getter for Error Code
    public int getErrorCode()
    {
       return this.miErrorCode;
    }
}
