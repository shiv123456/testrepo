/*
    Responsible: imohideen

*/

/* 08/11/2003: Updates for Ariba 8.0 (Jeff Namadan)
   02/13/2004: Dev SPL #10 Updated by Shane Liu to remove set Label operation here.*/


package config.java.ams.custom;

import ariba.base.core.BaseObject;
// Ariba 8.1: not being used
//import ariba.util.core.Util;
import ariba.base.core.ClusterRoot;
import ariba.base.fields.FieldProperties;
import ariba.htmlui.fieldsui.Log;
/* Ariba 8.0: Added ariba.util.core.Assert */
import ariba.util.core.Assert;

public class BuysenseUIConditions
{
    public static boolean  isFieldVisible(String clientUniqueName ,
                                          String fieldName,
                                          BaseObject bo,String appType)
    {
        boolean returnValue = false;
        ClusterRoot fieldTable = BuysenseUtil.findField(clientUniqueName,
                                                        fieldName);
        Log.customer.debug("Found this fieldTable object: %s",fieldTable);
        String visibilityField = getVisibilityField(appType,bo);
        try
        {
            if (!visibilityField.equals("BuysenseOrg"))
            {
                returnValue = ((Boolean)fieldTable.getFieldValue(visibilityField)).booleanValue();
                if (returnValue)
                    Log.customer.debug("*** returnValue: true" );
                else
                    Log.customer.debug("*** returnValue: false");
            }
            else returnValue = true;
        }
        catch (NullPointerException e)
        {
            Log.customer.debug("Could not find the field for client: %s field:%s",clientUniqueName,fieldName);
        }

        if (returnValue)
        {
            //boolean isNA = setLabel(bo,fieldTable,fieldName);
            if (((String)fieldTable.getFieldValue("ERPValue")).trim().equalsIgnoreCase("n/a")) 
               returnValue =  false;
        }
        Log.customer.debug("**** returnValue: "+returnValue );
        return returnValue;
    }

    public static boolean  isFieldEditable(String clientUniqueName ,
                                           String fieldName,
                                           BaseObject bo,String appType)
    {
        boolean returnValue = false;
        ClusterRoot fieldTable = BuysenseUtil.findField(clientUniqueName,
                                                        fieldName);
        String editabilityField = getEditabilityField(appType,bo);
        try
        {
            if (!editabilityField.equals("BuysenseOrg"))
                returnValue = ((Boolean)fieldTable.getFieldValue(editabilityField)).booleanValue();
            else returnValue = true;
        }
        catch (NullPointerException e)
        {
            Log.visibility.debug("Could not find the field for client: %s field:%s",clientUniqueName,fieldName);
            Log.visibility.debug("NullPointerException");
            /* Ariba 8.0: Replaced Util.assertNonFatal */
            Assert.assertNonFatal(false,"Could not find the FieldTable entry");
        }
        return returnValue;
    }
/*  Label is set using FieldPropertiesFilter

    private static boolean setLabel(BaseObject obj,
                                    ClusterRoot fieldTableCR,String fieldName)
    {
        Log.customer.debug("*** In setLabel");
        FieldProperties fp = obj.getFieldProperties(fieldName);
        Log.customer.debug("*** This is the fp: %s", fp);
        String label = (String)fieldTableCR.getFieldValue("ERPValue");
        Log.visibility.debug("Setting %s with the label:%s",fieldName,label);
        Log.customer.debug("*** Setting %s with the label:%s",fieldName,label);
        if (label.equals("n/a"))
        {
            Log.customer.debug("Returning false for n/a");
            Log.visibility.debug("*** Returning false for n/a"); return false ;
        }
        Log.customer.debug("*** Label: %s", label);
        fp.setPropertyForKey("Label",label);
        Log.customer.debug("*** Retreived Label from fp: %s", fp.stringPropertyForKey("Label"));
        Log.customer.debug("*** Leaving setLabel ***");
        return true;
    }
*/
    private static String getVisibilityField(String appType,
                                             BaseObject appObject)
    {
        if (appObject.getClassName().equals("ariba.common.core.Accounting"))
            return "VisibleOnPayment";
        if (appType.equals("Req")) return "VisibleOnReq" ;
        if (appType.equals("Payment")) return "VisibleOnPayment" ;
        if (appType.equals("Receipt")) return "VisibleOnReceipt" ;
        if (appType.equals("BuysenseOrg")) return "BuysenseOrg" ;
        else
        {
            Log.visibility.debug("Unhandled Approvable Type.Assuming BuysenseOrg");
            return "BuysenseOrg";
        }
    }

    private static String getEditabilityField(String appType,
                                              BaseObject appObject)
    {
        if (appObject.getClassName().equals("ariba.common.core.Accounting"))
            return "EditableOnPayment";
        if (appType.equals("Req")) return "EditableOnReq" ;
        if (appType.equals("Payment")) return "EditableOnPayment" ;
        if (appType.equals("Receipt")) return "EditableOnReceipt" ;
        if (appType.equals("BuysenseOrg")) return "BuysenseOrg" ;
        else
        {
            Log.visibility.debug("Unhandled Approvable Type.Assuming BuysenseOrg");
            return "BuysenseOrg";
        }

    }

    private static void setVisibility(BaseObject obj,
                                      ClusterRoot fieldTableCR,
                                      String fieldName,Boolean returnValue)
    {
        FieldProperties fp = obj.getFieldProperties(fieldName);
        //Log.visibility.debug("This is the fp: %s", fp);
        Log.visibility.debug("Setting the Visibility to %s",returnValue);
        fp.setPropertyForKey("Hidden",
                             new Boolean(!(returnValue.booleanValue())));
    }



}
