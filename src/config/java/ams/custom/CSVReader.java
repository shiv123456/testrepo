package config.java.ams.custom;

import java.io.*;
import java.util.Vector;
import java.util.StringTokenizer;


public class CSVReader
{
    
    public Vector readCSV (String filename)
    {
        BufferedReader loInput = null ; 
        Vector appsVector = new Vector();
        try
        {
            loInput = new BufferedReader(new FileReader( filename));
            
            while (loInput.ready()) 
            {
                String in = loInput.readLine();
                Vector fieldsVector = tokenize(in);
                appsVector.addElement(fieldsVector);
                /** Un-comment this if you need to look at what you are reading from the file */
                int fieldcount = fieldsVector.size();
                for (int k=0;k<fieldcount;k++) 
                {
                    System.out.println(fieldsVector.elementAt(k));
                }
            }
            loInput.close();  
        }
        catch (Exception fe)  
        {
            System.out.println("Could not find the file or problem reading the file");
        }
        return appsVector;
    }
    Vector tokenize(String inStr)
    {
        Vector fieldsVector = new Vector();
        
        StringTokenizer str = new StringTokenizer(inStr,",");
        int count = str.countTokens();
        for (int j=0;j<count;j++) 
        {
            fieldsVector.addElement(str.nextToken());
        }
        return fieldsVector ;
    }
    
}
