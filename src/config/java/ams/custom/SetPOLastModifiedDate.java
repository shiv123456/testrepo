package config.java.ams.custom;

import java.util.List;

import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.Requisition;
import ariba.purchasing.core.PurchaseOrder;
import ariba.purchasing.core.POLineItem;
import ariba.base.fields.*;
import ariba.base.core.BaseVector;
import ariba.common.core.SupplierLocation;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

/************************************************************************************************************
 *
 * This class is called by a trigger named, StatusStringChange, in POExtrinsicFields.aml
 * The purpose is to set PO and PO's Requisition LastModified date when the StatusString of the PO changes.
 * The LastModified date is updated for data retention to pick up this PO and Requisition.
 *
 ************************************************************************************************************/

public class SetPOLastModifiedDate extends Action
{
    public void fire(ValueSource object, PropertyTable params)
    {
    	Log.customer.debug("Calling SetPOLastModifiedDate.");

        PurchaseOrder loPO = null;
        Requisition loReq = null;
        BaseVector lvPOLineItems = null;
        POLineItem loPOLineItem = null;
        try
        {
            if(object instanceof PurchaseOrder)
            {
                loPO = (PurchaseOrder)object;
                loPO.setLastModified(new ariba.util.core.Date());
                Log.customer.debug("Updated LastModified for PurchaseOrder %s" + loPO.getFieldValue("UniqueName") +
    	                   " since the status changed.");

                lvPOLineItems = (BaseVector) loPO.getFieldValue("LineItems");

                // Get Requisition object for this PO and update LastModified date
                if(lvPOLineItems.size() > 0)
                {
                    loPOLineItem = (POLineItem)lvPOLineItems.get(0);
                    loReq = (Requisition)loPOLineItem.getFieldValue("Requisition");
	                loReq.setLastModified(new ariba.util.core.Date());
	                Log.customer.debug("Updated LastModified for Requisition %s" + loReq.getFieldValue("UniqueName") +
                           " since the status changed.");
                }
            }
            //SRINI: Start: Added below changes for CSPL-3115
            Log.customer.debug("SetPOLastModifiedDate:Before setting -- StatusString: %s", loPO.getStatusString());
            if(loPO.getStatusString().equals("Shipped") || loPO.getStatusString().equals("Shipping"))
            {
            	//loPO.setStatusString("Ordered");
            	loPO.setAdvancedShipNoticeState(1);
            	loPO.setOrderConfirmationState(1);
            	//loPO.setOrderedState(4);
            	//loReq.setOrderedState(4);
            	Log.customer.debug("SetPOLastModifiedDate:After setting -- StatusString: %s", loPO.getStatusString());
            }
            Log.customer.debug("SetPOLastModifiedDate:After setting1 -- StatusString: %s", loPO.getStatusString());
            //SRINI: End: Added below changes for CSPL-3115

            //SRINI: START: SEV changes for SupplierLocation
        	String lsRegistrationType = "RgstTypeCd";
        	String lsRegistrationTypeDescr = "RgstTypeDesc";
        	String lsRegistrationTypeVal = null;
        	String lsRegistrationTypeDescrVal = null;
            if(loPO.getPreviousVersion() == null)
            {
            	lsRegistrationTypeVal = (String) loPOLineItem.getFieldValue(lsRegistrationType);
            	lsRegistrationTypeDescrVal = (String) loPOLineItem.getFieldValue(lsRegistrationTypeDescr);
            	if(lsRegistrationTypeVal != null && lsRegistrationTypeDescrVal != null)
            	{
            		loPO.setFieldValue(lsRegistrationType, lsRegistrationTypeVal);
            		loPO.setFieldValue(lsRegistrationTypeDescr, lsRegistrationTypeDescrVal);
            	}
            }
            else
            {
            	lsRegistrationTypeVal = (String) loPO.getFieldValue(lsRegistrationType);
            	lsRegistrationTypeDescrVal = (String) loPO.getFieldValue(lsRegistrationTypeDescr);
            	List lvPOLines = loPO.getLineItems();
            	for (int i = 0; i < lvPOLines.size(); i++)
            	{
            		POLineItem loPOLine = (POLineItem) lvPOLines.get(i);
            		if(lsRegistrationTypeVal != null && lsRegistrationTypeDescrVal != null)
            		{
            		   loPOLine.setFieldValue(lsRegistrationType, lsRegistrationTypeVal);
            		   loPOLine.setFieldValue(lsRegistrationTypeDescr, lsRegistrationTypeDescrVal);
            		}
       			}
            }
            //SRINI: START: SEV changes for SupplierLocation
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Logs.customer.debug("error in SetPOLastModifiedDate.java: %s" , e);
        }
    }
}

