/*
By Sunil - September 2000
*/

/* 08/29/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */
/* 03/03/2004 - Dev SPL #37 - Removed system.out.println */
package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLQuery;
import ariba.approvable.core.Approvable;
import ariba.common.core.*;
import ariba.base.fields.*;
// Ariba 8.0: NamedObjectNameTable was deprecated by AQLNameTable.
import ariba.base.core.aql.AQLNameTable;
//import ariba.common.core.nametable.NamedObjectNameTable;
// Ariba 8.1: No longer need Util
//import ariba.util.core.Util;
import java.util.List;
import ariba.base.core.BaseVector;
// Ariba 8.1: Using java's Enueration
import ariba.base.core.ClusterRoot;
import ariba.util.log.Log;
import ariba.util.core.Date;
import ariba.basic.core.DateRange;
// Ariba 8.1: Need ListUtil
import ariba.util.core.ListUtil;


/**
   A NameTable that adds the following constraints on Orders to be paid.
   1. Vendor Constraints.
   2. Date range Constraints.
   3. The current user should only see orders that are having payment inititiator
      as himself(current user) or a role to wich the current user belongs.

*/

// Ariba 8.0: NamedObjectNameTable was deprecated by AQLNameTable
//            so changed class declaration to reflect that.
//81->822 changed Vector to List
public class BuysensePaymentOrderNameTable extends AQLNameTable

{
    /**
        Overridden to check the results to see if the approvable is
        an eform.
    */
    /*
     public void initializeValueSource(ValueSource vs)
     {
         setClassIsLeaf(false);

     }
    */

    public List matchPattern (String field, String pattern)
    {
        setClassIsLeaf(false);
        List results = super.matchPattern(field, pattern);
        setClassIsLeaf(false);
        return results;
    }

    public void addQueryConstraints (AQLQuery query,
                                     String field, String pattern)
    {
        setClassIsLeaf(false);

        // build the default set of field constraints for the pattern
        //default condition1 : select all orders that have a buysense org payment initiator as
        //a user which is the current user or
        //a role which is one of the roles with which the current user is associated.
        // default condition2 : the status of the order should be Ordered,Receiving,Received

        super.addQueryConstraints(query, field, pattern);
        beginSystemQueryConstraints(query);

        //The default condition1
        //Ariba 8.1: Modified logic to get the effectiveUser as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the Roles
        ariba.user.core.User currentEffectiveUser=(ariba.user.core.User)(Base.getSession().getEffectiveUser());
        ariba.common.core.User currentUser = ariba.common.core.User.getPartitionedUser(currentEffectiveUser,Base.getSession().getPartition());
        String currentUserName = (String)currentUser.getFieldValue("UniqueName");
        //BaseVector currentUserRoles= currentUser.getRoles();
        BaseVector currentUserRoles = (BaseVector)currentUser.getFieldValue("Roles");
        // create the list of roles that current user belongs to
        // Ariba 8.1: List constructor is deprecated by ListUtil::newVector()
        List listOfRoles= ListUtil.list();
        for(int i=0;i<currentUserRoles.size();i++)
        {
            // Ariba 8.1: List::elementAt() is deprecated by List::get()
            Object obj = currentUserRoles.get(i);
            if (obj instanceof ariba.base.core.BaseId)
            {
                ariba.base.core.BaseId role = (ariba.base.core.BaseId) obj;
                ClusterRoot cr = role.get();
                String roleName=(String)(cr.getFieldValue("UniqueName"));
                // Ariba 8.1: List::addElement() is deprecated by List::add()
                listOfRoles.add(roleName);
            }
        }
        //Search List is the list of roles that the current user belongs to + the current user
        List searchList = listOfRoles;
        // Ariba 8.1: List::addElement() is deprecated by List::add()
        searchList.add(currentUserName);

        for(int i=0;i<searchList.size();i++)
        {
            // Ariba 8.1: List::elementAt() is deprecated by List::get()
            String name=(String)searchList.get(i);
        }

        AQLCondition defaultCond1 = AQLCondition.buildIn(query.buildField("LineItems[0].Requisition.PaymentInitiatorUniqueName"),searchList);
        query.and(defaultCond1);
        // Ariba 8.1: List constructor is deprecated by ListUtil::newVector()
        List validStatusList=ListUtil.list();
        // Ariba 8.1: List::addElement() is deprecated by List::add()
        validStatusList.add("Ordered");
        // Ariba 8.1: List::addElement() is deprecated by List::add()
        validStatusList.add("Receiving");
        // Ariba 8.1: List::addElement() is deprecated by List::add()
        validStatusList.add("Received");
        AQLCondition defaultCond2 = AQLCondition.buildIn(query.buildField("StatusString"),validStatusList);
        query.and(defaultCond2);

        Supplier supplier = supplier();
        DateRange dr = getOrderDateRange();
        Date fromDate=(Date)dr.getFieldValue("FromDate");
        Date toDate=(Date)dr.getFieldValue("ToDate");
        // Build the supplier condition
        if(supplier!=null)
        {
            BaseId id = supplier == null ? new BaseId(0L,
                                                      0) : supplier.getBaseId();
            AQLCondition supplierCond =
                AQLCondition.buildEqual(query.buildField("Supplier"),id);
            query.and(supplierCond);
        }

        //Build the date range condition
        if(fromDate!=null && toDate!=null)
        {
            AQLCondition dateRangeCond =
                AQLCondition.buildBetween(query.buildField("OrderedDate"),
                                          fromDate,toDate);
            query.and(dateRangeCond);
        }


        query.setDistinct(true);
        //default section - select all orders that have a buysense org payment officer as
        //a user which is the current user or
        //a role which is one of the roles with which the current user is associated.

		Log.customer.debug("BuysensePaymentOrderNameTable - the final query is: "+ query);
        endSystemQueryConstraints(query);
    }

    // Ariba 8.0: This method is no longer valid since NamedObjectNameTable was
    // deprecated by AQLNameTable. There was no custom code in it anyway.
    /* public List constraints (String field, String pattern)
    {
        Log.customer.debug("Getting The Supplier");
        List constraints = super.constraints(field, pattern);
        return constraints;
    }*/


    private Supplier supplier()
    {
        ValueSource context = getValueSourceContext();
        if (((Approvable)context).instanceOf("ariba.core.PaymentEform"))
            return (Supplier)(context).getFieldValue("Vendor");
        else
            return null;
    }
    private DateRange getOrderDateRange()
    {
        ValueSource context = getValueSourceContext();
        if (((Approvable)context).instanceOf("ariba.core.PaymentEform"))
        {
            return (DateRange)(context).getFieldValue("OrderDateRange");
        }
        else
            return null;
    }
}
