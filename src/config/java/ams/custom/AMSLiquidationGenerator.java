/*
    Copyright (c) 1970-1999 AMS, Inc.
*/

/*
03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/04/2004: rgiesen dev SPL #38 - replace getLeadCurrency with Currency.getDefaultCurrency(partition)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/

package config.java.ams.custom;

import ariba.purchasing.core.PurchaseOrder;
import java.math.BigDecimal;
// Ariba 8.1: Money has moved
import ariba.basic.core.Money;
//import ariba.common.core.Money;
import ariba.base.core.ClusterRoot;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.util.log.Log;
import ariba.common.core.SplitAccountingCollection;
import java.util.List;
//Ariba 8.1: Currency has moved
import ariba.basic.core.Currency;
//import ariba.common.core.Currency;


public class AMSLiquidationGenerator
{

    ClusterRoot liquidation;
    PurchaseOrder order;
    ClusterRoot refTranObject;
    BigDecimal quantities[];
    //Constructor
    public AMSLiquidationGenerator(ClusterRoot refTran,
                                   PurchaseOrder po,BigDecimal quant[])
    {
        order=po;
        refTranObject=refTran;
        quantities=quant;
        Log.customer.debug("The Quantities[0]= %s",quantities[0]);
        //Create a liquidation
        liquidation = (ClusterRoot)ClusterRoot.create("ariba.core.Liquidation",
                                                      order.getPartition());
    }

    public boolean generateLiquidation(String txntype)
    {
        //Set refTranObject(the object calling liquidation) and PurchaseOrder on the Liquidation
        liquidation.setFieldValue("Order",order);
        liquidation.setFieldValue("RefTranObject",refTranObject);

        Log.customer.debug("Creating the liquidation transaction");

        BaseVector liquidationLines = (BaseVector)liquidation.getFieldValue("LiquidationItems");
        //Create a dummy liquidation item
        BaseObject liquidationItem = (BaseObject)BaseObject.create("ariba.core.LiquidationItem",order.getPartition());
        //Create a vector of liquidationAccountings from the vector AdvRefAccounting on the order
        //-->get the vector AdvRefAccounting on the order
        BaseVector advRefAccounting= (BaseVector)order.getFieldValue("AdvRefAccounting");
        //Update the advRefAcctg vector quantities.

        //BaseVector liquidationAccountings = new  BaseVector();
        //Clone the AdvRefAccounting vector
        BaseVector liquidationAccountings=(BaseVector)advRefAccounting.clone();
        Log.customer.debug("After Cloning the advRefAccounting vector");


        BaseVector orderItems= (BaseVector)order.getFieldValue("LineItems");
        for(int i=0;i<quantities.length;i++)
        {
            //get the corresponding order line
            // Ariba 8.1: List::elementAt(int) has been deprecated by List::get(int)
            // 81->822 changed Vector to List
            BaseObject orderItem=(BaseObject)orderItems.get(i);
            Log.customer.debug("Order Item is %s",orderItem);
            //BaseVector orderItemAccountings = (BaseVector)orderItem.getFieldValue("Accountings.SplitAccountings");
            //loop thru' the accountings for the order line
            SplitAccountingCollection splitAccountings=(SplitAccountingCollection)orderItem.getFieldValue("Accountings");
            List orderItemAccountings = splitAccountings.getSplitAccountings();
            Log.customer.debug("Before Inner For Loop");
            for(int j=0;j<orderItemAccountings.size();j++)
            {
                // Ariba 8.1: List::elementAt(int) has been deprecated by List::get(int)
                BaseObject orderItemAccounting=(BaseObject)(orderItemAccountings.get(j));
                //BigDecimal accountingPercent=(BigDecimal)(orderItemAccounting).getFieldValue("Percentage");
                //Money orderLineAmt=(Money)orderItem.getFieldValue("Amount");

                updateLiquidationFields(liquidationAccountings,
                                        orderItem,
                                        orderItemAccounting,quantities[i]);
            }
        }
        //Add the liquidationAccountings to the liquidationItem object.
        liquidationItem.setFieldValue("LiquidationAccountings",
                                      liquidationAccountings);
        //Add the liquidationItem to the LiquidationObject
        liquidationLines.add(liquidationItem);

        //Save the liquidation object.
        liquidation.save();
        //TODO:Push the liquidation object.
        Log.customer.debug("ABOUT TO PUSH LIQUIDATION");
        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        //String clientName = (String)order.getDottedFieldValue("LineItems[0].Requisition.Requester.BuysenseOrg.ClientName.ClientName");
        ariba.user.core.User RequesterUser = (ariba.user.core.User)order.getDottedFieldValue("LineItems[0].Requisition.Requester");
        //rgiesen dev SPL #39
        //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,order.getPartition());
        String clientName = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName");
        String clientID = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientID");

        String XMLClientTag = clientID.trim() + "_" + clientName.trim() ;
        Log.customer.debug("XMLClientTag %s",XMLClientTag);
        AMSTXN.initMappingData();
        Log.customer.debug("XMLClientTag %s",XMLClientTag);
        int rt=AMSTXN.push(XMLClientTag,
                           txntype,
                           (String)order.getFieldValue("UniqueName"),
                           "NEW",
                           "RDY",
                           "A",
                           "0",
                           "",
                           liquidation,
                           "LiquidationItems",
                           "LiquidationAccountings",
                           liquidation.getPartition());
        liquidation.save();
        order.setFieldValue("POERPStatus","Paid");
        //order.setFieldvalue("Liquidations",liquidations)
        List liquidations = (List)order.getFieldValue("Liquidations");
        // Ariba 8.1: List::addElement() has been deprecated by List::add()
        liquidations.add(liquidation.getBaseId());
        order.setFieldValue("Liquidations",liquidations);
        order.save();

        //Update the orderRefAccounting vector quantities to reflect the correct NumberToBeLiquidated
        //from the LiquidationAccontings vector

        for(int j=0;j<advRefAccounting.size();j++)
        {
            // Ariba 8.1: List::elementAt(int) has been deprecated by List::get(int)
            BaseObject liquidationAccountingItem=(BaseObject)liquidationAccountings.get(j);
            BaseObject refAccountingItem=(BaseObject)advRefAccounting.get(j);
            refAccountingItem.setFieldValue("NumberToBeLiquidated",
                                            liquidationAccountingItem.getFieldValue("NumberToBeLiquidated"));
            Log.customer.debug("Amt on the refAccounting %s",
                           refAccountingItem.getFieldValue("LiquidationAmount"));
            // Ariba 8.1: Currency.getLeadCurrency() is no longer static.
            //refAccountingItem.setFieldValue("LiquidationAmount",new Money(0.00, Currency.getLeadCurrency()));
            //rgiesen dev SPL #38 - replace getLeadCurrency with Currency.getDefaultCurrency(partition)
            refAccountingItem.setFieldValue("LiquidationAmount",
                                            new Money(0.00,
                                                      Currency.getDefaultCurrency(liquidation.getPartition())));
        }
        return true;
    }

    private void updateLiquidationFields(BaseVector liquidationAccountings,
                                         BaseObject orderItem,
                                         BaseObject orderItemAccounting,
                                         BigDecimal quantity)
    {
        Money orderAccountingLineAmt=(Money)orderItemAccounting.getFieldValue("Amount");
        BigDecimal orderLineQuantity=(BigDecimal)orderItem.getFieldValue("Quantity");
        //Added by Sunil - ST SPL#361
        BigDecimal orderActAmt = orderAccountingLineAmt.getAmount();
        BigDecimal liqActamt = orderActAmt.multiply(quantity);
        liqActamt = liqActamt.divide(orderLineQuantity,
                                     4,BigDecimal.ROUND_HALF_UP);
        // Ariba 8.1: Currency::getLeadCurrency() is no longer static
        //Money liquidationAccountingsAmount= new Money(liqActamt,Currency.getLeadCurrency());
        //rgiesen dev SPL #38 - replace getLeadCurrency with Currency.getDefaultCurrency(partition)
        Money liquidationAccountingsAmount= new Money(liqActamt,
                                                      Currency.getDefaultCurrency(liquidation.getPartition()));

        //Money liquidationAccountingsAmount= orderAccountingLineAmt.multiply(quantity);
        //liquidationAccountingsAmount=Money.divide(liquidationAccountingsAmount,orderLineQuantity);
        Log.customer.debug("orderLineQuantity: %s",orderLineQuantity);
        Log.customer.debug("quantity: %s",quantity);
        Log.customer.debug("orderAccountingLineAmt %s",
                       orderAccountingLineAmt.getAmount());
        Log.customer.debug("Calculated liquidationAccountingsAmount %s",
                       liquidationAccountingsAmount.getAmount());

        //update LiquidationAmount and NumberToBeLiquidated
        String suffix=(String)orderItemAccounting.getFieldValue("RefPCHeaderLineNum");


        Log.customer.debug("In method updateLiquidationAmount");
        Log.customer.debug("Suffix is %s",suffix);
        //Loop thru the liquidationAccountings vector and if a suffix match is found update the liquidationAmount field.
        for(int i=0;i<liquidationAccountings.size();i++)
        {
            // Ariba 8.1: List::elementAt(int) has been deprecated by List::get(int)
            BaseObject liquidationAccountingItem=(BaseObject)liquidationAccountings.get(i);
            if(suffix.equals((String)liquidationAccountingItem.getFieldValue("LineNum")))
            {
                Log.customer.debug("Suffix match found");
                //get the current LiquidationAmt into a temp amt field
                Money tempAmt=(Money)liquidationAccountingItem.getFieldValue("LiquidationAmount");

                Log.customer.debug("tempAmt is %s",tempAmt);
                //update the temp amt field
                tempAmt=tempAmt.add(liquidationAccountingsAmount);
                //set the LiquidationAmt
                liquidationAccountingItem.setFieldValue("LiquidationAmount",
                                                        tempAmt);
                //get the current NumberToBeLiquidated into a temp number field
                BigDecimal tempNum=(BigDecimal)liquidationAccountingItem.getFieldValue("NumberToBeLiquidated");
                //update the temp number field
                tempNum=tempNum.subtract(quantity);
                //set the LiquidationAmt
                liquidationAccountingItem.setFieldValue("NumberToBeLiquidated",
                                                        tempNum);


                //set all the text fields on RefAccounting object
                List AcctgTextFieldsVector = FieldListContainer.getRefAcctgTextFieldValues();
                for (int j=0;j<AcctgTextFieldsVector.size();j++)
                {
                    // Ariba 8.1: List::elementAt(int) has been deprecated by List::get(int)
                    String textField=(String)AcctgTextFieldsVector.get(j);
                    liquidationAccountingItem.setFieldValue(textField,
                                                            (String)orderItemAccounting.getFieldValue(textField));
                }
            }
        }
    }

    public ClusterRoot getLiquidation()
    {
        return liquidation;
    }

    public boolean setLiquidation()
    {
        return true;
    }

    public boolean pushLiquidation()
    {
        return true;
    }
}
