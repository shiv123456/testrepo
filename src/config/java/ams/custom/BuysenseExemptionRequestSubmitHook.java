package config.java.ams.custom;

import java.util.List;

import ariba.admin.core.Log;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableHook;
import ariba.base.core.Base;
import ariba.util.core.Constants;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;

public class BuysenseExemptionRequestSubmitHook implements ApprovableHook
{
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));
    private static         String        errorStringTable            = "buysense.eform";
    private static         String        eVAExemptionReqValidationError  = "TypeOfExemptionRequestError";
    private static         String        eVAExemptionReqValidation  = "ExemptionRequestError";
    
    public List run(Approvable approvable)
    {
        Log.customer.debug("Inside BuysenseUserProfileRequestSubmitHook "+approvable);
        
        //Boolean lbVCERelease = (approvable.getFieldValue("VCERelease") == null)? false:(Boolean) approvable.getFieldValue("VCERelease");
        Boolean lbContractExemption = (approvable.getFieldValue("ContractExemption") == null)? false:(Boolean) approvable.getFieldValue("ContractExemption");
        Boolean lbExceedDelegatedAuthority = (approvable.getFieldValue("ExceedDelegatedAuthority") == null)? false:(Boolean) approvable.getFieldValue("ExceedDelegatedAuthority");
        Boolean lbCooperativeContractCB = (approvable.getFieldValue("CooperativeContractCB") == null)? false:(Boolean) approvable.getFieldValue("CooperativeContractCB");
        Boolean lbOther = (approvable.getFieldValue("Other") == null)? false:(Boolean) approvable.getFieldValue("Other");
      
        if((lbContractExemption == false)&&(lbExceedDelegatedAuthority == false)&&(lbCooperativeContractCB == false)&&(lbOther == false))
        {     
            Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : None of the checkbox have been set");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidationError));
        }       
        
        if(lbContractExemption == true && lbExceedDelegatedAuthority == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbContractExemption == true && lbCooperativeContractCB == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }
        if(lbContractExemption == true && lbOther == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        }

        if(lbExceedDelegatedAuthority == true && lbContractExemption == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        if(lbExceedDelegatedAuthority == true && lbCooperativeContractCB == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        if(lbExceedDelegatedAuthority == true && lbOther == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 

        if(lbCooperativeContractCB == true && lbContractExemption == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        if(lbCooperativeContractCB == true && lbExceedDelegatedAuthority == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        if(lbCooperativeContractCB == true && lbOther == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        
        if(lbOther == true && lbContractExemption == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        if(lbOther == true && lbExceedDelegatedAuthority == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        if(lbOther == true && lbCooperativeContractCB == true)
        {
        	Log.customer.debug("Inside BuysenseExemptionRequestSubmitHook : Only one checkbox should be set at one time");
            return ListUtil.list(Constants.getInteger(-1),Fmt.Sil(Base.getSession().getLocale(),errorStringTable, eVAExemptionReqValidation));
        } 
        
        return NoErrorResult;
        
     }
}


