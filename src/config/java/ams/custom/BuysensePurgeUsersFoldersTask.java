/*******************************************************************************************

DESCRIPTION: Scheduled Task BuysensePurgeUsersFoldersTask
             This task will purge excessive items from users folder to keep user's
             folders under a specified threshold, so they don't grow out of control.
             This task is run for performance reasons.


HOW IT WORKS:After initialization, the task perfoms the following steps in order:
             1.  Purge ALL folder items in the folder of the users stipulated in the
                 UsersForWhomToPurgeAllItems parameter.  If none are specified, it proceeds to
                 the second step.
             2.  Obtain a List of user UniqueNames where the number of items in their PurgeItemsFromFolder/Archive
                 folder has exceeded the limit specified in the NumItemsNotToExceed parameter.
             3.  Using the List of users obtained from the previous step, purge all folder items
                 from those users' Archive folders where the CREATE_DATE on the items is older
                 than the current date minus the number of days stipultated in the NumOfDaysBack
                 parameter.


PARAMETERS:  This task can be configured with 4 OPTIONAL parameters in the ScheduledTasks.table file.
             If these parameters are not set, then default values are used, where applicable.

             Parameter Name               Definition                                      Default Value
             ---------------------------  ----------------------------------------------  -------------
       	     NumItemsNotToExceed          Number to specify how many items a folder       5000
       	                                  can contain before it will be purged.  Folders
       	                                  items are only purged of items that have aged
       	                                  by the next parameter.

    	     NumOfDaysBack                Maximum number of days a folder item can age    365
    	                                  without being purged from the folder when the
    	                                  task is run.  With default value all folder
    	                                  items more than 1 year old will be purged from
    	                                  the user's Archive folder

    	     UsersForWhomToPurgeAllItems  This is an override for use with custom         (no default value)
    	                                  approvers for users whose folders should be
    	                                  purged each time the task is run regardless of
    	                                  the number of items in the folder and
    	                                  regardless of the age of the items in the
    	                                  folder.  This should be stipulated as a
    	                                  comma separated string of user UniqueNames.
    	                                  EXAMPLE:  "customApprover,user1,user2,user3"

    	     TimeZone                     For use with the date comparison, can set the   EDT
    	                                  timezone using the standard 3 character string.
    	                                  
    	     PurgeItemsFromFolder		  This parameter specifies the folder that need to be
    	                                  purged. To purge custom folders the value for this 
    	                                  parameter should be specified as '1'.
    	                                  Ex:'FolderNameArchive','FolderNameStatus','FolderNameWatch','1'...
    	     
    	     PurgeItemsTill				  Date to specify till what date the folder items    (no default value)
    	     							  need to be purged. 
    	     							  This parameter should be specified in the 
    	     							  following format:YYYY-MM-DD
    	     							  Ex: if this parameter is specified as 2010-12-31, 
    	     							  	  Then all the folder items dated on or before 
    	     							  	  2010-12-31 will be purged.

             MaxINcount                   Custom limit on maximum number of expressions for single SQL query IN clause.
                                          This param has to be set if the user count is more than 1000 for a particular NumItemsNotToExceed.
                                          And the value should be set less than the maximum number of expressions allowed for respective DB version.
                                          This is to overcome DB IN restriction. Refer LIMITATIONS section for more info.
                                          Single delete SQL query will be split in to many based on MaxINcount. Default is 1000


SAMPLE ENTRY for this task in ScheduledTasks.table file:

    BuysensePurgeUsersFoldersTask = {
    	ScheduledTaskClassName = "config.java.ams.custom.BuysensePurgeUsersFoldersTask";
    	NumItemsNotToExceed    = "4000";
    	NumOfDaysBack          = "180";
    	UsersForWhomToPurgeAllItems = "atechnic";
    	MaxINcount = "200";
    };


LIMITATIONS:  The way this task builds the IN statement for the DELETE queries could result in large
              numbers of users being added to the IN statement if the NumItemsNotToExceed parameter
              is set to too small a size.  I believe that Oracle has a limit of 200 elements for an IN
              statement in Oracle 9i (I believe this limit is 1000 for 10g and 11g). It is probably best
              to limit the size to no more than 200 elements, regardless.  So before this task is configured,
              one should execute the query below from Inspector to figure out a starting point for where
              to set the NumItemsNotToExceed parameter.  In the query below, it is testing for 6000
              items.  If that query would return more than 200 results, I would run the query again
              at 7000, and keep raising the number until less than 200 results are returned.  After
              executing the query the first time, then I would gradually decrease the number over
              successive executions of the task.

              SELECT fdr.Owner.UniqueName AS UniqueName, count (fdr.Items)
			  FROM   ariba.approvable.core.Folder fdr
			  WHERE  fdr.FolderName = 'FolderNameArchive'
			  GROUP BY fdr.Owner.UniqueName
			  HAVING count (fdr.Items) > 6000


For further clarifications and definitions, please see the comments located throughout the code.

MODIFICATION LOG:

DEVELOPER                        DATE                   MODIFICATION                     App Version
------------------------------   --------------------   ------------------------------   -----------------

Ariba Consulting, Ariba, Inc.       July 27, 2011          Updated task for 9r1         .   Buyer 9r1 SP14


*******************************************************************************************/

package config.java.ams.custom;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLError;
import ariba.base.core.aql.AQLException;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.procure.core.Log;
import ariba.user.core.User;
import ariba.util.core.Date;
import ariba.util.core.ListUtil;
import ariba.util.core.StringUtil;
import ariba.util.scheduler.ScheduledTask;
import ariba.util.scheduler.ScheduledTaskException;
import ariba.util.scheduler.Scheduler;



/**
 * Class BuysensePurgeUsersArchiveFoldersTask is a scheduled task to purge excessive items from users Archive folder.
 * Only the oldest items in the Archive folder will be purged.
 */
public class BuysensePurgeUsersFoldersTask extends ScheduledTask {

	//Define static class Strings
	public  static final String ClassName = "config.java.custom.BuysensePurgeUsersFoldersTask";
	public  static final String SharedUserClassName         = "ariba.user.core.User";
    private static final String NumItemsNotToExceed         = "NumItemsNotToExceed";
    private static final String NumOfDaysBack               = "NumOfDaysBack";
    private static final String UsersForWhomToPurgeAllItems = "UsersForWhomToPurgeAllItems";
    private static final String DesignatedTimeZone          = "TimeZone";
    private static final String PurgeItemsFromFolder        = "PurgeItemsFromFolder";
    private static final String PurgeItemsTill              = "PurgeItemsTill";
    private static final String ansiDate                    = "DATE";
    private static final String dbCreateDateField           = "A.AP_CREATEDATE";
    private static final String dbUniqueNameField           = "cus_uniquename";
    private static final String beginParen                  = "(";
    private static final String comma                       = ",";
    private static final String endParen                    = ")";
    private static final String hyphen                      = "-";
    private static final String lessThan                    = "<";
    private static final String semicolon                   = ";";
    private static final String singleQuote                 = "'";
    private static final String space                       = " ";
    private static final String MaxINcount                  = "MaxINcount";  
    //Define static class variables for parameter Default Values
    private static final int NumItemsNotToExceedDefaultValue = 5000;
    private static final int NumOfDaysBackDefaultValue       = 365;
    private static final String DefaultTimeZone              = "EDT";
    private static final String DefaultPurgeItemsFromFolder   = "FolderNameArchive";
    private static final int NumMaxINcountDefaultValue       = 1000;    

    //Define class variables
    public  Partition partition                     = null;
    public  Partition partitionNone                 = null;
    private String paramTimeZone                    = null;
    private String paramUsersForWhomToPurgeAllItems = null;
    private String paramPurgeItemsFromFolder     	= null;
    private String paramPurgeItemsTill		     	= null;
    private String folderTabQueryFiled                        = null;
    private int paramNumItemsNotToExceed            = 0;
    private int paramNumOfDaysBack                  = 0;
    private int paramMaxINcount                     = 0;

    int totalNumItemsUpdatedByFirstQuery            = 0;
    int totalNumItemsUpdatedBySecondQuery           = 0;    

    /**
     * init method - reads/defaults values for parameters set in the ScheduledTasks.table for this task
     */
    public void init(Scheduler scheduler, String scheduledTaskName, Map arguments) {

		String methodName = "init";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
		super.init(scheduler, scheduledTaskName, arguments);
        Iterator iterator = arguments.keySet().iterator();
        while (iterator.hasNext()) {
            String key = (String)iterator.next();
            String value = ((String)arguments.get(key)).trim();
			Log.customer.debug("%s.%s - key is \"%s\" and value is \"%s\"", ClassName, methodName, key, value);
            if (key.equals(NumItemsNotToExceed)) {
				if(StringUtil.isAllDigits(value)){
	                paramNumItemsNotToExceed = Integer.parseInt(value);
					Log.customer.debug("%s.%s - paramNumItemsNotToExceed is set to \"%s\"", ClassName, methodName, value);
				}//end inner if
				else{
					Log.customer.debug("%s.%s - paramNumItemsNotToExceed was not set to \"%s\", was not all digits", ClassName, methodName, value);
				}//end else
            }//end if
            if (key.equals(NumOfDaysBack)) {
				if(StringUtil.isAllDigits(value)){
	                paramNumOfDaysBack = Integer.parseInt(value);
					Log.customer.debug("%s.%s - paramNumOfDaysBack is set to \"%s\"", ClassName, methodName, value);
				}//end inner if
				else{
					Log.customer.debug("%s.%s - paramNumOfDaysBack was not set to \"%s\", was not all digits", ClassName, methodName, value);
				}//end else
            }//end if
            if (key.equals(UsersForWhomToPurgeAllItems)) {
                paramUsersForWhomToPurgeAllItems = value;
				Log.customer.debug("%s.%s - paramUsersForWhomToPurgeAllItems is set to \"%s\"", ClassName, methodName, value);
            }//end if
            if (key.equals(DesignatedTimeZone)) {
                paramTimeZone = value;
				Log.customer.debug("%s.%s - paramTimeZone is set to \"%s\"", ClassName, methodName, value);
            }//end if
            if (key.equals(PurgeItemsFromFolder)) {
            	if(StringUtil.isAllDigits(value)){
            		folderTabQueryFiled = "Type";
            		paramPurgeItemsFromFolder = value;
            	}
            	else
            	{
            		folderTabQueryFiled = "FolderName";
            		paramPurgeItemsFromFolder = singleQuote+value+singleQuote;
            	}            		
				Log.customer.debug("%s.%s - paramPurgeItemsFromFolder is set to \"%s\"", ClassName, methodName, value);
            }//end if
            if (key.equals(PurgeItemsTill)) {
            	paramPurgeItemsTill = value;
				Log.customer.debug("%s.%s - paramPurgeItemsTill is set to \"%s\"", ClassName, methodName, value);
            }//end if
            if (key.equals(MaxINcount)) {
                if(StringUtil.isAllDigits(value)){
                    paramMaxINcount = Integer.parseInt(value);
                    Log.customer.debug("%s.%s - paramMaxINcount is set to \"%s\"", ClassName, methodName, value);
                }//end inner if
                else{
                    Log.customer.debug("%s.%s - paramMaxINcount was not set to \"%s\", was not all digits", ClassName, methodName, value);
                }//end else
            }//end if            
        }//end while
        //If parameters were not stipulated in ScheduledTasks.table, set to default values
        if(paramNumItemsNotToExceed == 0){
			paramNumItemsNotToExceed = NumItemsNotToExceedDefaultValue;
			Log.customer.debug("%s.%s - paramNumItemsNotToExceed was set to default value of %s", ClassName, methodName, String.valueOf(NumItemsNotToExceedDefaultValue));
		}//end if
        if(paramNumOfDaysBack == 0){
			paramNumOfDaysBack = NumOfDaysBackDefaultValue;
			Log.customer.debug("%s.%s - paramNumOfDaysBack was set to default value of %s", ClassName, methodName, String.valueOf(NumOfDaysBackDefaultValue));
		}//end if
        if(StringUtil.nullOrEmptyOrBlankString(paramTimeZone)){
			paramTimeZone = DefaultTimeZone;
			Log.customer.debug("%s.%s - paramTimeZone was set to default value of \"%s\"", ClassName, methodName, DefaultTimeZone);
		}//end if
        if(StringUtil.nullOrEmptyOrBlankString(paramPurgeItemsFromFolder)){
        	paramPurgeItemsFromFolder = singleQuote+DefaultPurgeItemsFromFolder+singleQuote;
			Log.customer.debug("%s.%s - paramPurgeItemsFromFolder was set to default value of \"%s\"", ClassName, methodName, DefaultPurgeItemsFromFolder);
		}//end if
        if(paramMaxINcount == 0){
            paramMaxINcount = NumMaxINcountDefaultValue;
            Log.customer.debug("%s.%s - paramMaxINcount was set to default value of %s", ClassName, methodName, String.valueOf(NumMaxINcountDefaultValue));
        }//end if        
		Log.customer.debug("%s.%s - ended", ClassName, methodName);
    }//end method init


    /**
     * run method - executes the logic for this task to clean up user's Archive folders
     */
    public void run() throws ScheduledTaskException {

		String methodName = "run";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
        partition = Base.getSession().getPartition();
        partitionNone = Base.getService().getPartition("None");
        
        Log.customer.debug("%s.%s - begin cleaning Archive folders", ClassName, methodName);
		this.removeItemsFromArchiveFolders();
		Log.customer.debug("%s.%s - ended", ClassName, methodName);
        return;
    }//end method run


    /**
     * This method performs the lions share of the business logic for this scheduled task by invoking the business logic
     * to determine which users' Archive folders are to be cleaned or purged.
     */
    private void removeItemsFromArchiveFolders(){

		String methodName = "removeItemsFromArchiveFolders";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);

		/* *** STEP 1 ***
		   Empty all folder items from the Archive folders of the users explicitly specified in the parameter
		   UsersForWhomToPurgeAllItems parameter in ScheduledTasks.table.  This is an optional parameter.
		*/
		List purgeAllItemsUserList = ListUtil.list();
		//Get List of users for who to purge all items if parameter is NOT NULL
		if (!StringUtil.nullOrEmptyOrBlankString(paramUsersForWhomToPurgeAllItems)){
			purgeAllItemsUserList = this.getListOfUsersForWhomToPurgeAllItems();
			if (ListUtil.getListSize(purgeAllItemsUserList) > 0){
				this.emptyUsersArchiveFolders(purgeAllItemsUserList);
			}//end if
		}//end if

		/* *** STEP 2 ***
		   Obtain the list of user's UniqueNames whose Archive folders exceed the maximum number of items
		   as dictated by the NumItemsNotToExceed parameter.
		*/
		List listOfUserFolders = this.getListOfUsersWhoNeedTheirFoldersCleaned();
		Log.customer.debug("%s.%s - listOfUserFolders contains %s elements", ClassName, methodName, ListUtil.getListSize(listOfUserFolders));

		/* *** STEP 3 ***
		   Execute the SQL statement to remove aged items from the Archive folder
		*/
		if (ListUtil.getListSize(listOfUserFolders) > 0){
			this.purgeAgingItemsFromUsersArchiveFolders(listOfUserFolders);
		}//end if

		Log.customer.debug("%s.%s - ended", ClassName, methodName);
        return;
	}//end method removeItemsFromArchiveFolders


    /**
     * Void method purges only the items that were created more than NumOfDaysBack prior to the current date
     * from the Archive folders of the user's with UniqueNames in the List passed to this method.
     * This method uses the parameter NumOfDaysBack and to detemine which items to purge.
     */
    private void purgeAgingItemsFromUsersArchiveFolders(List userList){

		String methodName = "purgeAgingItemsFromUsersArchiveFolders";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
		String pastDateString = null;
		if(StringUtil.nullOrEmptyOrBlankString(paramPurgeItemsTill))
		{
			pastDateString = this.getFormattedDateStringForDaysBack();
		}
		else
		{
			pastDateString = paramPurgeItemsTill+space+paramTimeZone;
		}			
		Log.customer.debug("%s.%s - pastDateString %s", ClassName, methodName,pastDateString);		
		this.purgeUsersArchiveFolders(userList, pastDateString);
		Log.customer.debug("%s.%s - ended", ClassName, methodName);
		return;
	}//end method purgeAgingItemsFromUsersArchiveFolders


    /**
     * Void method purges ALL items from the Archive folders of the user's with UniqueNames in the
     * List passed to this method.  This method ignores the parameter NumOfDaysBack and removes all
     * folder items regardless of age.
     */
    private void emptyUsersArchiveFolders(List userList){

		String methodName = "emptyUsersArchiveFolders";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
		String formattedDateString = new String(); //create empty string to pass to purge method
		this.purgeUsersArchiveFolders(userList, formattedDateString);
		Log.customer.debug("%s.%s - ended", ClassName, methodName);
		return;
	}//end method emptyUsersArchiveFolders


    /**
     * Void method purges items from the Archive folders of the user's with UniqueNames in the
     * List passed to this method.  Folder items are removed that were created prior to the day
     * of the formatted Date String passed into the method.
     */
    private void purgeUsersArchiveFolders(List userList, String formattedDateString){

		String methodName = "purgeUsersArchiveFolders";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
		String dateAndClause   = new String();
		String userWhereClause = new String();
		if(StringUtil.nullOrEmptyOrBlankString(formattedDateString)){
			//Don't add AND condition to where clause for date - purge everything
		}//end if
		else{
			//Add AND condition to where clause for date to only purge aged items
			dateAndClause = this.getAndClauseForDateLessThan(formattedDateString, dbCreateDateField);
		}//end else
		int numUsers = ListUtil.getListSize(userList);
        if(numUsers == 0){
			//No users in list
			Log.customer.debug("%s.%s - ended - no users in List passed to method", ClassName, methodName);
			return;
		}
		else{

		// Add a for loop and just pass 200 values at a time
		for(int i=0;i<numUsers;i+=paramMaxINcount)
		{
		    List tempUserList = ListUtil.list();
		    tempUserList = userList.subList(i, Math.min(numUsers, i + paramMaxINcount));

	      if (numUsers == 1)
             {
               userWhereClause = this.getWhereClauseForEqualityStatement((String) userList.get(0),dbUniqueNameField);
             }
             else{
               userWhereClause = this.getWhereClauseForINStatement(tempUserList, dbUniqueNameField);
             }
		    String firstQuery = this.buildQueryForPurgeFromBaseIdTab(dateAndClause, userWhereClause);
		    String secondQuery = this.buildQueryForPurgeFromFolderItemTab(dateAndClause, userWhereClause);
		    //Execute queries to delete the items
		    AQLOptions aqlOptions = new AQLOptions(partition);
		    aqlOptions.setSQLAccess(AQLOptions.AccessReadWrite);
		    int numItemsUpdatedByFirstQuery  = 0;
		    int numItemsUpdatedBySecondQuery = 0;
		    try{
		 	   Log.customer.debug("%s.%s - Executing 1st query", ClassName, methodName);
			   numItemsUpdatedByFirstQuery = (int)Base.getService().executeUpdate(firstQuery,aqlOptions);
			   totalNumItemsUpdatedByFirstQuery += numItemsUpdatedByFirstQuery;
			   Log.customer.debug("%s.%s - Executing 2nd query", ClassName, methodName);
			   numItemsUpdatedBySecondQuery = (int)Base.getService().executeUpdate(secondQuery,aqlOptions);
			   totalNumItemsUpdatedBySecondQuery += numItemsUpdatedBySecondQuery;
		    }//end try
		    catch (AQLException aqlException){
		      	Log.customer.debug("%s.%s - caught AQLException: %s", ClassName, methodName, aqlException.toString());
		    }//end catch
		    Log.customer.debug("%s.%s - %s item(s) updated by the first query", ClassName, methodName, String.valueOf(numItemsUpdatedByFirstQuery));
		    Log.customer.debug("%s.%s - %s item(s) updated by the second query", ClassName, methodName, String.valueOf(numItemsUpdatedBySecondQuery));
		    Log.customer.debug("%s.%s - ended", ClassName, methodName);
	    }
        Log.customer.debug("%s.%s - %s total item(s) updated by the first query", ClassName, methodName, String.valueOf(totalNumItemsUpdatedByFirstQuery));
        Log.customer.debug("%s.%s - %s total item(s) updated by the second query", ClassName, methodName, String.valueOf(totalNumItemsUpdatedBySecondQuery));		
		}//end else

		return;
	}//end method purgeUsersArchiveFolders


	/**
	 * Returns the dynamically generated SQL String to be used to purge the Archive folders from the baseidtab.
	 */
    private String buildQueryForPurgeFromBaseIdTab(String dateLessThanAndClause, String userNameWhereClause){

		String methodName = "buildQueryForPurgeFromBaseIdTab";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
		String deleteQuery =
			" DELETE " +
			" FROM   baseidtab " +
			" WHERE  rootid in ( " +
			"   SELECT fi.fi_parentfolder " +
			"   FROM   folderitemtab fi " +
			"   WHERE  fi.fi_parentfolder in ( " +
			"     SELECT rootid " +
			"     FROM   foldertab " +
			"     WHERE  fo_"+folderTabQueryFiled+" = "+ paramPurgeItemsFromFolder +" "+
			"     AND    fo_owner in ( " +
			"       SELECT rootid " +
			"       FROM   us_usertab " +
			"       " + userNameWhereClause + " ))) " +
			" AND    val in ( " +
			"   SELECT fi.rootid " +
			"   FROM   folderitemtab fi,APPROVABLETAB A " +
			"   WHERE  fi.FI_ITEM = A.ROOTID " +
			"   " + dateLessThanAndClause +
			"   AND    fi.fi_parentfolder in ( " +
			"     SELECT rootid " +
			"     FROM   foldertab " +
			"     WHERE  fo_"+folderTabQueryFiled +"= "+ paramPurgeItemsFromFolder +" "+
			"     AND    fo_owner in ( " +
			"       SELECT rootid " +
			"       FROM   us_usertab" +
			"       " + userNameWhereClause + " ))) ";
		Log.customer.debug("%s.%s - query is \"%s\"", ClassName, methodName, deleteQuery);
		Log.customer.debug("%s.%s - ended", ClassName, methodName);
		return deleteQuery;
	}//end method buildQueryForPurgeFromBaseIdTab


	/**
	 * Returns the dynamically generated SQL String to be used to purge the Archive folders from the folderitemtab.
	 */
    private String buildQueryForPurgeFromFolderItemTab(String dateLessThanAndClause, String userNameWhereClause){

		String methodName = "buildQueryForPurgeFromFolderItemTab";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
		String deleteQuery =
			" DELETE " +
			" FROM   folderitemtab fi " +
			" WHERE  fi.fi_parentfolder in ( " +
			"   SELECT rootid from foldertab " +
			"   WHERE  fo_"+ folderTabQueryFiled +"= "+ paramPurgeItemsFromFolder +" " +
			"   AND    fo_owner in ( " +
			"     SELECT rootid " +
			"     FROM   us_usertab" +
			"     " + userNameWhereClause + " )) " +
			" AND FI_ITEM IN ( " +
			"   SELECT A.ROOTID " +
			"   FROM   APPROVABLETAB A " +
			"   WHERE  A.ROOTID = FI_ITEM " +
			"   " + dateLessThanAndClause + " ) ";
		Log.customer.debug("%s.%s - query is \"%s\"", ClassName, methodName, deleteQuery);
		Log.customer.debug("%s.%s - ended", ClassName, methodName);
		return deleteQuery;
	}//end method buildQueryForPurgeFromFolderItemTab


	/**
	 * Returns the AQL String to be used to identify all users who posess
	 * Archive folders that contain more items than the number contained
	 * in the threshold passed into this method.
	 */
	private String buildQueryToCountUsersFolderItems(int itemNumberThreshold){

		String methodName = "buildQueryToCountUsersFolderItems";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
		String countUsersFolderItemsQuery =
			" SELECT fdr.Owner.UniqueName AS UniqueName " +
			" FROM   ariba.approvable.core.Folder fdr " +
			" WHERE  fdr."+ folderTabQueryFiled +"="+ paramPurgeItemsFromFolder +" " +
			" GROUP BY fdr.Owner.UniqueName " +
			" HAVING count (fdr.Items) > " + String.valueOf(itemNumberThreshold) + space;
		Log.customer.debug("%s.%s - ended", ClassName, methodName);
		return countUsersFolderItemsQuery;
	}//end method buildQueryToCountUsersFolderItems


	/**
	 * Method getListOfUsersWhoNeedTheirFoldersCleaned returns a List of String elements (User UniqueNames)
	 * for user's whose Archive folder contains more items than the threshold defined in NumItemsNotToExceed
	 * set in the ScheduledTasks.table.
	 */
	private List getListOfUsersWhoNeedTheirFoldersCleaned(){

		String methodName = "getListOfUsersWhoNeedTheirFoldersCleaned";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
		List listOfUsers = ListUtil.list();
		String aqlQueryString = this.buildQueryToCountUsersFolderItems(paramNumItemsNotToExceed);
		Log.customer.debug("%s.%s -  AQL query string is: %s", ClassName, methodName, aqlQueryString );
		AQLResultCollection aqlResultCollection = this.getAQLCollection(aqlQueryString);
		//make sure that aqlCollection returns data, if not - return null
		if (aqlResultCollection.getFirstError() != null){
			AQLError error = aqlResultCollection.getFirstError();
			Log.customer.debug("%s.%s -  AQL error encountered: %s", ClassName, methodName, error.toString());
			aqlResultCollection.close();
			Log.customer.debug("%s.%s - ended; returning empty List due to AQL error", ClassName, methodName);
			return listOfUsers; //return an empty list
		}//end if
		int collectionSize = aqlResultCollection.getSize();
		//Get records from AQLResultCollection
		int recordCounter = 0;
		while (aqlResultCollection.next()){
			String userName = aqlResultCollection.getString(0);
			listOfUsers.add(userName);
		}//end while
		aqlResultCollection.close();
		int numUsersInList = ListUtil.getListSize(listOfUsers);
		Log.customer.debug("%s.%s - ended - return List containing %s elements", ClassName, methodName, String.valueOf(numUsersInList));
		return listOfUsers;
	}//end method getListOfUsersWhoNeedTheirFoldersCleaned


	/**
	 * Returns a date String representation (of the current date minus paramNumOfDaysBack)
	 * formatted as DD-MON-YYYY.  This format is used since this is what Oracle expects
	 * for use with the TO_DATE.
	 */
	private String getFormattedDateStringForDaysBack(){

		String methodName = "getFormattedDateStringForDaysBack";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
		//Set paramNumOfDaysBack to a negative value so we can subtract the number of days from the current date
		if (paramNumOfDaysBack > 0){
			paramNumOfDaysBack = paramNumOfDaysBack * -1;
			Log.customer.debug("%s.%s - paramNumOfDaysBack set to %s", ClassName, methodName, String.valueOf(paramNumOfDaysBack));
		}//end if
		Date daysBackDate = new Date();
		Log.customer.debug("%s.%s - current date is %s", ClassName, methodName, daysBackDate.toString());
		Date.addDays(daysBackDate, paramNumOfDaysBack);
		Log.customer.debug("%s.%s - Days Back date set to %s", ClassName, methodName, daysBackDate.toString());
		String formattedDateString = this.getFormattedDateString_YYYY_MM_DD(daysBackDate);
		Log.customer.debug("%s.%s - ended", ClassName, methodName);
		return formattedDateString;
	}//end method getFormattedDateStringForDaysBack


	/**
	 * Returns an date String representation (of the Date object passed to the method)
	 * formatted as YYYY-MM-DD, which is the ANSI default for the DATE function.
	 */
	private String getFormattedDateString_YYYY_MM_DD(Date date){

		String methodName = "getFormattedDateString_YYYY_MM_DD";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
		String formattedDateString = new String();
		int dayOfMonth  = 0;
		int monthOfYear = 0;
		int year        = 0;
		if(date.isNull()){
			Log.customer.debug("%s.%s - DATE passed to method is NULL - cannot format a NULL date", ClassName, methodName);
			Log.customer.debug("%s.%s - ended - return EMPTY String", ClassName, methodName);
			return formattedDateString;
		}//end if
		dayOfMonth  = (int)Date.getDayOfMonth(date);
		monthOfYear = (int)Date.getMonth(date) + 1; //Month returned is zero based so add 1 (i.e. January is returned as 0)
		year        = (int)Date.getYear((java.util.Date)date);
		//Build the formatted date string (add some extra checking just to be safe)
		//Build string in format of YYYY-MM-DDB
		if (dayOfMonth > 0 && monthOfYear > 0 && year > 0) {
			if (dayOfMonth < 32 && monthOfYear < 13 && year > 1969){
				//String for year in YYYY format
				String yearString = String.valueOf(year);
				//String for month in MM format
				String monthOfYearString = String.valueOf(monthOfYear);
				//Get String for day of month in DD format
				String dayOfMonthString = String.valueOf(dayOfMonth);
				if (monthOfYear < 10){
					monthOfYearString = "0" + monthOfYearString; //place in leading 0 for date format
				}//end if
				if (dayOfMonth < 10){
					dayOfMonthString = "0" + dayOfMonthString; //place in leading 0 for date format
				}//end if
				//Make the formatted date String
				formattedDateString = yearString + hyphen + monthOfYearString + hyphen + dayOfMonthString + space + paramTimeZone;
			}//end if
		}//end if
		if(StringUtil.nullOrEmptyOrBlankString(formattedDateString)){
			Log.customer.debug("%s.%s - formatted date String IS NULL or EMPTY", ClassName, methodName);
		}//end if
		else{
			Log.customer.debug("%s.%s - formatted date String = \"%s\"", ClassName, methodName, formattedDateString);
		}//end else
		Log.customer.debug("%s.%s - ended", ClassName, methodName);
		return formattedDateString;
	}//end method getFormattedDateString_YYYY_MM_DD


	/**
	 * Returns a String to be used as the AND clause to compare a specified field name that represents a date
	 * where the field is less than the date String passed to the method.
	 * Date String must be formatted as DD-MON-YYYY for Oracle.
	 */
    private String getAndClauseForDateLessThan(String dateString, String fieldName){

		String methodName = "getAndClauseForDateLessThan";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
		String andClause = " AND " + fieldName + space + lessThan + space + ansiDate + beginParen + singleQuote + dateString + singleQuote + endParen + space;
		Log.customer.debug("%s.%s - ended; return String andClause = \"%s\"", ClassName, methodName, andClause);
		return andClause;
	}//end method getAndClauseForDateLessThan


	/**
	 * Returns a String to be used as the WHERE clause to stipulate that the value of a specified field
	 * is equal to the value of the String passed to the method.
	 */
    private String getWhereClauseForEqualityStatement(String elementForEquality, String fieldName){

		String methodName = "getWhereClauseForEqualityStatement";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
		String whereClause = " WHERE " + fieldName + " = " + singleQuote + elementForEquality + singleQuote + space;
		Log.customer.debug("%s.%s - ended; return String whereClause = \"%s\"", ClassName, methodName, whereClause);
		return whereClause;
	}//end method getWhereClauseForEqualityStatement


	/**
	 * Returns a String to be used as the WHERE clause to stipulate that the value of a specified field
	 * is IN the List of String values passed to the method.  This method, builds the IN statement, by
	 * inserting all of the values passed in the List into the IN statement, enclosing the values in SingleQuotes
	 * and separating the values with commas.
	 */
    private String getWhereClauseForINStatement(List elementsForINStatement, String fieldName){

		String methodName = "getWhereClauseForINStatement";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
		// Build the WHERE clause adding all of the elements to the IN statement
		int listSize = ListUtil.getListSize(elementsForINStatement);
		String whereClause = " WHERE " + fieldName + " IN " + beginParen;
		for (int i=0; i < listSize; i++){
			//Add commas where appropriate
			if (i > 0){
				whereClause = whereClause + comma;
			}//end if
			String element = (String)elementsForINStatement.get(i);
			whereClause = whereClause + singleQuote + element + singleQuote;
		}//end for
		//Close the parenthesis of the IN statement in WHERE clause
		whereClause = whereClause + endParen + space;
		Log.customer.debug("%s.%s - ended; return String whereClause = \"%s\"", ClassName, methodName, whereClause);
		return whereClause;
	}//end method getWhereClauseForINStatement


    /**
     * Returns a list of VALID user UniqueNames for whom all items are to be purged from their folder.
     */
    private List getListOfUsersForWhomToPurgeAllItems(){

		String methodName = "getListOfUsersForWhomToPurgeAllItems";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
		List userNamesList = this.parseCommaSeparatedString(paramUsersForWhomToPurgeAllItems);
		List validUserList = this.getListOfValidUserUniqueNames(userNamesList);
		Log.customer.debug("%s.%s - ended; return List of %s elements", ClassName, methodName, String.valueOf(ListUtil.getListSize(validUserList)));
		return validUserList;
	}//end method getListOfUsersForWhomToPurgeAllItems


    /**
     * Returns a list of VALID user UniqueNames.  Method verifies that each String UniqueName in the passed-in List
     * corresponds to valid ariba.user.core.User object.  Expects a List of String elements.
     */
    private List getListOfValidUserUniqueNames(List userNamesList){

		String methodName = "getListOfValidUserUniqueNames";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
		List userList = ListUtil.list();
		int userNamesListSize = ListUtil.getListSize(userNamesList);
		if(userNamesListSize < 1){
			Log.customer.debug("%s.%s - List of user names contains no elements - return empty List of Users", ClassName, methodName);
			return userList;
		}//end if
		for(int i=0; i < userNamesListSize; i++){
			String userName = (String)userNamesList.get(i);
			if (Base.getService().objectMatchingUniqueName(SharedUserClassName, partitionNone, userName) != null){
				Log.customer.debug("%s.%s - Add UniqueName for SharedUser %s to userList", ClassName, methodName, userName);
				userList.add(userName);
			}//end if
			else{
				Log.customer.debug("%s.%s - No ACTIVE SharedUser exists for UniqueName = \"%s\"", ClassName, methodName, userName);
			}//end else
		}//end for
		int userListSize = ListUtil.getListSize(userList);
		Log.customer.debug("%s.%s - userNamesList contained %s elements and %s Users were added to userList", ClassName, methodName, String.valueOf(userNamesListSize), String.valueOf(userListSize));
		Log.customer.debug("%s.%s - ended; return List of %s User UniqueName(s)", ClassName, methodName, userListSize);
		return userList;
	}//end method getListOfValidUserUniqueNames


    /**
     * Returns a list of String objects parsed from a passed-in comma separated String
     */
    private List parseCommaSeparatedString(String argument) {

		String methodName = "parseCommaSeparatedString";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
        List identifiers = ListUtil.list();
        if (!StringUtil.nullOrEmptyOrBlankString(argument)){
            int end = 0;
            int begin = 0;
            int i = 0;
            String identifier = null;
            do {
                end = argument.indexOf(",", begin);
                if (end != -1) {
                    identifier = argument.substring(begin, end);
                    begin = end + 1;
                }//end if
                else {
                    identifier = argument.substring(begin, argument.length());
                }//end else
                identifier = identifier.trim();
                if (!StringUtil.nullOrEmptyOrBlankString(identifier)) {
                    identifiers.add(identifier);
                    Log.customer.debug("%s.%s - Add String element \"%s\" to list", ClassName, methodName, identifier);
                }//end if
            }//end do
            while ((end != -1) && !StringUtil.nullOrEmptyOrBlankString(identifier));
        }//end if
		int identifiersSize = ListUtil.getListSize(identifiers);
		Log.customer.debug("%s.%s - ended; return List of %s String elements", ClassName, methodName, identifiersSize);
        return identifiers;
    }//end method parseCommaSeparatedString


	/**
	 * Method <b>getAQLCollection()</b> gets the AQLCollection
	 * results for the associated AQL query string.
	 *
	 * @param String aqlQueryString
	 * @return AQLResultCollection aqlResultCollection
	 */
	private AQLResultCollection getAQLCollection(String aqlQueryString) {

		String methodName = "getAQLCollection";
		Log.customer.debug("%s.%s - invoked", ClassName, methodName);
		AQLQuery aqlQuery = AQLQuery.parseQuery(aqlQueryString);
		AQLOptions aqlOptions = new AQLOptions(partition);
		aqlOptions.setSQLAccess(AQLOptions.AccessReadWrite);
		AQLResultCollection aqlResultCollection = Base.getService().executeQuery(aqlQuery, aqlOptions);
		if(aqlResultCollection.getFirstError() == null){
			Log.customer.debug("%s.%s - AQLCollection contains %s elements", ClassName, methodName, String.valueOf(aqlResultCollection.getSize()));
		}//end if
		Log.customer.debug("%s.%s - ended", ClassName, methodName);
		return aqlResultCollection;
	}//end method getAQLCollection

}//end class BuysensePurgeUsersArchiveFoldersTask
