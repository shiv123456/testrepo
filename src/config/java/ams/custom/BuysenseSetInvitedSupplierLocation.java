package config.java.ams.custom;

import java.util.Iterator;

import ariba.base.core.BaseVector;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.common.core.Supplier;
import ariba.common.core.SupplierLocation;
import ariba.procure.core.ProcureLineItemCollection;
import ariba.procure.core.RankedSupplier;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;
import ariba.workforce.core.LaborLineItemDetails;
/**
 * 
 * @author sarath.babugarre
 * Trigger to default Suppplier Contact using best supplier location logic. 
 * 
 */
public class BuysenseSetInvitedSupplierLocation extends Action
{

	public void fire(ValueSource object, PropertyTable params) throws ActionExecutionException
	{
		LaborLineItemDetails laborLine = (LaborLineItemDetails)object;
		Log.customer.debug("BuysenseSetInvitedSupplierLocation Clusterroot returned  = " + laborLine);
		if(((Boolean)laborLine.getFieldValue("Collaborate")).booleanValue())
		{
			BaseVector invitedlsupplierList = laborLine.getInvitedSuppliers();
			if(invitedlsupplierList.size()>=1)
	    	{
				Log.customer.debug("inside if");
	    		for(int i=0;i<invitedlsupplierList.size();i++)	    			
	    		{
	    			ariba.procure.core.RankedSupplier ranksupplier = (RankedSupplier) invitedlsupplierList.get(i);	    			  
	    		    Supplier partsupplier = ranksupplier.getPartitionedSupplier();	    			  
	    			SupplierLocation location = null;
	    			if(partsupplier != null)
	    			{
	    				location = bestLocation(partsupplier,laborLine);
	 	                Log.customer.debug("inside seconf if location "+location);
	    			}
	    			ranksupplier.setFieldValue("SupplierLocation", location);	    			  	    			 
	    		}
	    	}
		}
		else
	    {
			return;		
		}
	}
	protected SupplierLocation bestLocation(Supplier supplier, LaborLineItemDetails llid)
	{
		Log.customer.debug("inside bestLocation");
		String lsSupplierName = (String) supplier.getFieldValue("UniqueName");
		if(!StringUtil.nullOrEmptyOrBlankString(lsSupplierName))
        {
            BaseVector lvSLocations = (BaseVector) supplier.getLocations();            
            if(lvSLocations.size() > 1)
            {
                return null;
            }
            if(lsSupplierName.equals("[Unspecified]"))
            {
            	 return null;
            }
        }
        String addressType=null;
        for(Iterator e = supplier.getLocationsObjects(); e.hasNext();)        
        {                
            SupplierLocation location = (SupplierLocation)e.next();            
            addressType = (String)location.getFieldValue("AddressType");
            if (addressType != null) 
            {
                if (addressType.equals("O"))
                {                    
                    if(!StringUtil.nullOrEmptyOrBlankString(location.getPreferredOrderingMethod()))
                    {                    	
                        return location;
                    }
                }
            }
        }       
        ProcureLineItemCollection collection = (ProcureLineItemCollection)llid.getDottedFieldValue("LineItem.LineItemCollection");        
        if(collection != null)
        {
            SupplierLocation location =  supplier.getBestLocation(collection.getRequester());            
            if (location != null )  
            {
                addressType = (String) location.getFieldValue("AddressType");     
                if ((addressType != null) && (addressType.equals("O")))
                {
                    return location;
                }
            }
        }
		return null;
	}
}
