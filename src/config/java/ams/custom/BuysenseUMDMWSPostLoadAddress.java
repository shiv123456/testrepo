package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.util.core.StringUtil;

public class BuysenseUMDMWSPostLoadAddress extends BuysenseUMDMWSPostLoad
{
    // any special checks specifically for Address web service
    // return true if instance of Address is found
    boolean checkIfUniqueAribaInstanceExists(ClusterRoot oCustomObj)
    {
        boolean bFoundRecord = false;

        if (oCustomObj == null)
        {
            return false;
        }
        Object oUniquename = oCustomObj.getDottedFieldValue("UniqueName");
        if (oUniquename != null && !StringUtil.nullOrEmptyOrBlankString((String) oUniquename))
        {
            // get ariba.common.core.Address with same uniquename
            Partition partition = Base.getService().getPartition("pcsv");
            ClusterRoot clr = Base.getService().objectMatchingUniqueName("ariba.common.core.Address", partition, (String) oUniquename);
            if (clr != null)
            {
                bFoundRecord = true;
            }
        }
        return bFoundRecord;
    }
}
