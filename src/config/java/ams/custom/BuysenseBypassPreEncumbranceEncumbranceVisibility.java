package config.java.ams.custom;
//Checks the field visibility of BypassPreEncumbranceEncumbrance and ReqHeadCB5Value.
import ariba.base.core.BaseObject;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.purchasing.core.Requisition;
import ariba.user.core.User;
import ariba.util.core.PropertyTable;

import ariba.util.log.Log;

public class BuysenseBypassPreEncumbranceEncumbranceVisibility extends Condition{

        private static final ValueInfo parameterInfo[];
        
        public boolean evaluate(Object value, PropertyTable params) throws ConditionEvaluationException
        {
            BaseObject obj   = (BaseObject)params.getPropertyForKey("SourceObject");
            Log.customer.debug("inside BuysenseBypassPreEncumbranceEncumbranceVisibility SourceObject"+obj);
            
            Log.customer.debug("inside BuysenseBypassPreEncumbranceEncumbranceVisibility");

            
            if(obj.instanceOf("ariba.purchasing.core.Requisition"))
            {
                Requisition req = (Requisition)obj;
                User requester = req.getRequester();
                ariba.common.core.User partitionedUser = ariba.common.core.User.getPartitionedUser(requester, req
                        .getPartition());
                Log.customer.debug("inside BuysenseBypassPreEncumbranceEncumbranceVisibility");
                
                Boolean oEncumbrance =  (Boolean)partitionedUser.getDottedFieldValue("ClientName.Encumbrance");
                Log.customer.debug("inside BuysenseBypassPreEncumbranceEncumbranceVisibility" +oEncumbrance);
                Boolean bIsReqHeadcb5 = (Boolean) obj.getFieldValue("ReqHeadCB5Value");
                Log.customer.debug("inside BuysenseBypassPreEncumbranceEncumbranceVisibility" +bIsReqHeadcb5);
                
                if(oEncumbrance!=null && oEncumbrance.booleanValue() || bIsReqHeadcb5!=null && bIsReqHeadcb5.booleanValue())
                { 
                    Log.customer.debug("inside bIsReqHeadcb!=null && bIsReqHeadcb.booleanValue()");
                    return true;
                }
                else
                  {    
                    Log.customer.debug("inside else of BuysenseBypassPreEncumbranceEncumbranceVisibility::bIsReqHeadcb!=null && bIsReqHeadcb.booleanValue() ");
                      return false;
                  }                  
            }
            else
            {
                Log.customer.debug("inside ariba.purchasing.core.Requisition");
                return false;
            } 
        }            
        protected ValueInfo[] getParameterInfo()
        {
           return parameterInfo;
        }   
        static
        {
             parameterInfo = (new ValueInfo[] {new ValueInfo("SourceObject", 0)});
        }
    }
