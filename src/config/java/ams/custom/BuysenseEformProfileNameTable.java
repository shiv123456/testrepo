package config.java.ams.custom;

import java.util.List;
import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLNameTable;
import ariba.base.core.aql.AQLQuery;
import ariba.base.fields.ValueSource;
import ariba.util.core.ListUtil;
import ariba.util.log.Log;

public class BuysenseEformProfileNameTable extends AQLNameTable
{
    String    sClassName    = "BuysenseEformProfileNameTable";
    @SuppressWarnings("rawtypes")
    List      lUserProfiles = ListUtil.list();

    Partition partition     = Base.getService().getPartition("pcsv");

    //ariba.user.core.User   loSharedUser  = (ariba.user.core.User) User.getEffectiveUser();

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void addQueryConstraints(AQLQuery query, String field, String pattern)
    {
        try
        {

            ValueSource context = getValueSourceContext();
            Object loSharedUser = context.getDottedFieldValue("Requester");

            if (loSharedUser == null)
            {
                //Dummy query to return no results of Requester (On Behalf of) is null
                query.and(AQLCondition.parseCondition("1=2"));
            }
            else
            {
                ariba.common.core.User currUsr = ariba.common.core.User.getPartitionedUser(
                        (ariba.user.core.User) loSharedUser, partition);

                if (currUsr != null)
                {

                    if (currUsr.getDottedFieldValue("BuysEformProfiles") != null)
                    {
                        lUserProfiles = (List) (currUsr.getDottedFieldValue("BuysEformProfiles"));
                    }
                    //If User doesnt have any Profile then we need to add a query constraint such that no Profile is displayed
                    //For that, just passing user baseid to BuysEformProf IN Condition to avoid null exception and intern that returns no results
                    if (lUserProfiles == null || (lUserProfiles != null && lUserProfiles.isEmpty()))
                    {
                        lUserProfiles.add(currUsr.id);
                    }

                    query.andIn("this", lUserProfiles);

                }
            }
            Log.customer.debug( sClassName + " query: " + query);
        }
        catch (Exception e)
        {
            Log.customer.warning(8000, sClassName + " Exception: " + e.getStackTrace());
        }

    }
}
