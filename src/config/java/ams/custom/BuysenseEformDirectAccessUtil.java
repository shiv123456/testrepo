package config.java.ams.custom;

import ariba.util.core.GrowOnlyHashtable;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseEformDirectAccessUtil
{
    private static BuysenseEformDirectAccessUtil m_instance = null;
    private GrowOnlyHashtable                    m_userEforms;

    private BuysenseEformDirectAccessUtil()
    {
        m_userEforms = new GrowOnlyHashtable();
    }

    public static synchronized BuysenseEformDirectAccessUtil getInstance()
    {
        if (m_instance == null)
            m_instance = new BuysenseEformDirectAccessUtil();
        return m_instance;
    }

    protected GrowOnlyHashtable userEformTable()
    {
        return m_userEforms;
    }

    public String lookupEformProf(String userUName, String profileID)
    {
        Log.customer.debug("BuysenseEformDirectAccessUtil looking up Eform profile" + profileID + " for user " + userUName);
        String userProf = lookupProf(userUName);
        if (userProf == null)
        {
            Log.customer.debug("BuysenseEformDirectAccessUtil setting a new profile " + userProf + " for user " + userProf);
            putProfId(userUName, userProf);
        }
        return userProf;
    }

    public String lookupProf(String userId)
    {
        Log.customer.debug("BuysenseEformDirectAccessUtil looking up Eform profile for User: " + userId);
        if (!StringUtil.nullOrEmptyOrBlankString(userId))
        {
            Log.customer.debug("BuysenseEformDirectAccessUtil User up Eform profile " + m_userEforms.get(userId));
            return (String) m_userEforms.get(userId);
        }
        else
            return null;
    }

    public void putProfId(String userId, String profID)
    {
        Log.customer.debug("BuysenseEformDirectAccessUtil putProfId() userId: " + userId + ", profID: " + profID);
        if (!StringUtil.nullOrEmptyOrBlankString(userId))
            m_userEforms.put(userId, profID);
    }
}
