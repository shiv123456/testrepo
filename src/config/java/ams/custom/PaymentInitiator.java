package config.java.ams.custom;

/*03/02/2004: Dev SPL #37 - Create custom PaymentInitiator trigger (Shane Liu)
This trigger is called from ReqExtrinsicFields.aml as follows:
	<trigger event="FieldChange" field="Requester" name="InitializePaymentInitiator">
		<action implementation="config.java.ams.custom.PaymentInitiator">
			<parameter name="SourcePath" value="Requester.PartitionedUser.BuysenseOrg.PaymentInitiator.UniqueName" />
			<parameter name="Target" value="PaymentInitiatorUniqueName"/>
		</action>
	</trigger>
It copies the value of the BSOrg's PaymentInitiator to the Req.

It is used in the BuysensePaymentOrderNameTable to restrict the orders that the user can see.
*/

import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.fields.Action;
import ariba.purchasing.core.Requisition;

//81->822 changed ConditionValueInfo to ValueInfo
public class PaymentInitiator extends Action
{
    private static final ValueInfo[] parameterInfo =
        {
        new ValueInfo("Target", false, IsScalar, StringClass),
        new ValueInfo("SourcePath", false, IsScalar, StringClass),
    };
    private static ValueInfo valueInfo = new ValueInfo(0, "ariba.base.core.BaseObject");

    public void fire(ValueSource object, PropertyTable params)
    {
        Requisition source;
        if (object instanceof Requisition)
        	source = (Requisition)object;
        else
        	source = null;

		String sourcePath = params.stringPropertyForKey("SourcePath");
        String target = (String)params.getPropertyForKey("Target");
        String sUniqueName=null;
        if (source != null && target != null)
        {
        	sUniqueName = resolveSource(source, sourcePath);
			source.setFieldValue(target, sUniqueName);
			Log.customer.debug("PaymentInitiator.fire()::source: " + source + " sourcePath: " + sourcePath + " sUniqueName: " + sUniqueName + " target: " +target);
		}
    }

    protected String resolveSource(Requisition source, String path)
    {
		String ret = null;
		if (path.startsWith("Requester.PartitionedUser.")) {
			ariba.user.core.User user = (ariba.user.core.User)source.getFieldValue("Requester");
			if (user != null) {
				ariba.common.core.User cuser = ariba.common.core.User.getPartitionedUser(user, source.getPartition());
				Log.customer.debug("PaymentInitiator.resolveSource()::Partitioned User"+cuser.getUniqueName());
				//Strip away the string "Requester.PartitionedUser.", which is 26 characters, to just get the field
				ret = (String)cuser.getDottedFieldValue(path.substring(26));
			}
			return ret;
		}
        return ret;
    }

    public PaymentInitiator()
    {
    }


    /**
        Return the list of valid value types.
    */
    protected ValueInfo getValueInfo ()
    {
        return valueInfo;
    }

    /**
        Return the parameter names.
    */
    protected ValueInfo[] getParameterInfo ()
    {
        return parameterInfo;
    }

}
