package config.java.ams.custom;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import org.codehaus.jackson.JsonGenerator;

import ariba.app.util.Attachment;
import ariba.approvable.core.ApprovalRequest;
import ariba.approvable.core.Comment;
import ariba.base.core.BaseObject;
import ariba.base.core.LongString;
import ariba.base.core.LongStringElement;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseDW2ExtractUtils
{
    private static String msCN = "BuysenseDW2ExtractUtils";

    public static void getFieldsForObjectLevel(JsonGenerator jsonGen, BuysenseDWFieldDetails[] fieldList, BaseObject poObj, BuysenseDWHierarchyDetailsObj objLevel) throws Exception
    {
        Log.customer.debug("*****%s*****  getFieldsForObjectLevel poObj:%s", msCN, poObj);

        // work on it later
        // BuysenseDWFieldDetails[] fieldList = aribaFieldDetailsMap.get(objLevel.getBuysenseAribaObjHierarchy().getObjectHierarchyName());
        if (fieldList == null)
        {
            Log.customer.debug("*****%s***** found obj ObjTagLevel:%s list of fields is null", msCN, objLevel.getBuysenseAribaObjHierarchy().getAribaObjname());
            return;
        }
        Log.customer.debug("*****%s***** found obj ObjTagLevel:%s list of fields:%s", msCN, objLevel.getBuysenseAribaObjHierarchy().getAribaObjname(), fieldList);

        BuysenseDWFieldDetails fldDetail;
        for (int i = 0; i < fieldList.length; i++)
        { // put more checks later so that it doesn't blow up -JB
            fldDetail = fieldList[i];
            String fieldVal = "";
            int fieldValInt = 0;
            BigDecimal fieldBigDecimal = new BigDecimal(0);
            boolean fieldValBool = false;
            String eol = System.getProperty("line.separator");

            Log.customer.debug("*****%s***** fldDetail.getAribaFieldName:" + fldDetail.getAribaFieldName(), msCN);
            Log.customer.debug("*****%s***** fldDetail.getAribaGetMethod():" + fldDetail.getAribaGetMethod(), msCN);
            if (fldDetail.getAribaFieldName().equalsIgnoreCase("CommentText"))
            {
                Log.customer.debug("*****%s***** in CommentText", msCN);
                jsonGen.writeArrayFieldStart("COMMENTTEXTVECTOR");
                Comment loCmt = (Comment) poObj;
                LongString ls = loCmt.getText();
                if(ls == null)
                {
                        jsonGen.writeStartObject();
                        jsonGen.writeEndObject();
                }else
                {
                    for (Iterator ie = ls.getStringsIterator(); ie.hasNext();)
                    {
                        LongStringElement lse = (LongStringElement) ie.next();
                        jsonGen.writeStartObject();
                        jsonGen.writeNumberField("COMMENTINDEX", lse.getIndex());
                        String sCmtTxt = lse.getString();
                        sCmtTxt = sCmtTxt.replaceAll("\r\n", eol); // replace with correct newline character for all String type fields
                        jsonGen.writeStringField("COMMENTTEXT", sCmtTxt);
                        jsonGen.writeEndObject();
                    }
                }

                jsonGen.writeEndArray();

                // Comment Attachments
                jsonGen.writeArrayFieldStart("ATTACHMENTSVECTOR");
                for (Iterator attIterator = loCmt.getAttachmentsIterator(); attIterator.hasNext();)
                {
                    Attachment att = (Attachment) attIterator.next();
                    jsonGen.writeStartObject();
                    jsonGen.writeStringField("FILENAME", att.getFilename());
                    jsonGen.writeNumberField("CONTENTLENGTH", att.getContentLength());
                    jsonGen.writeEndObject();
                }
                jsonGen.writeEndArray();
                continue;
            }
            if (fldDetail.getAribaGetMethod() != null)
            {
                Method fieldMethod = poObj.getClass().getMethod(fldDetail.getAribaGetMethod(), new Class[] {});
                fieldVal = getFieldValue(fieldMethod.invoke(poObj, new Object[] {}), fldDetail.getAribaFieldType());
                Log.customer.debug("*****%s***** invoking field method:" + fieldVal, msCN);
            }
            else if (fldDetail.getAribaFieldName().equalsIgnoreCase("BaseId"))
            {
                fieldVal = poObj.getBaseId().toDBString();
                Log.customer.debug("*****%s***** invoking baseid method:" + fieldVal, msCN);
            }
            else if (fldDetail.getAribaFieldName().equalsIgnoreCase("COVAParentBaseId"))
            {
                fieldVal = getParentBaseIDs(poObj);
                Log.customer.debug("*****%s***** invoking parent baseid method:" + fieldVal, msCN);
            }
            else
            { // poObj is null then just return empty string - TBD
                fieldVal = getFieldValue(poObj.getDottedFieldValue(fldDetail.getAribaFieldName()), fldDetail.getAribaFieldType());
            }
            Log.customer.debug("*****%s***** field value returned from getFieldValue:" + fieldVal, msCN);

            if (StringUtil.equalsIgnoreCase("int", fldDetail.getAribaFieldType()) || StringUtil.equalsIgnoreCase("java.lang.Integer", fldDetail.getAribaFieldType()))
            {
                if (!StringUtil.nullOrEmptyOrBlankString(fieldVal))
                {
                    fieldValInt = Integer.parseInt(fieldVal);
                    jsonGen.writeNumberField(fldDetail.getDwOutputStr(), fieldValInt);
                }
                else
                {
                    jsonGen.writeNullField(fldDetail.getDwOutputStr());
                }
            }
            else if (StringUtil.equalsIgnoreCase("boolean", fldDetail.getAribaFieldType()) || StringUtil.equalsIgnoreCase("java.lang.Boolean", fldDetail.getAribaFieldType()))
            {
                if (!StringUtil.nullOrEmptyOrBlankString(fieldVal))
                {
                    fieldValBool = Boolean.parseBoolean(fieldVal);
                    if (fieldValBool)
                    {
                        jsonGen.writeNumberField(fldDetail.getDwOutputStr(), 1);
                    }
                    else
                    {
                        jsonGen.writeNumberField(fldDetail.getDwOutputStr(), 0);
                    }
                }
                else
                {
                    jsonGen.writeNullField(fldDetail.getDwOutputStr());
                }
            }
            else if (StringUtil.equalsIgnoreCase("java.math.BigDecimal", fldDetail.getAribaFieldType()))
            {
                if (!StringUtil.nullOrEmptyOrBlankString(fieldVal))
                {
                    fieldBigDecimal = new BigDecimal(fieldVal);
                    jsonGen.writeNumberField(fldDetail.getDwOutputStr(), fieldBigDecimal);
                }
                else
                {
                    jsonGen.writeNullField(fldDetail.getDwOutputStr());
                }
            }
            else if (StringUtil.equalsIgnoreCase("ariba.basic.core.Money", fldDetail.getAribaFieldType()))
            {
                if (!StringUtil.nullOrEmptyOrBlankString(fieldVal))
                {
                    fieldBigDecimal = new BigDecimal(fieldVal);
                    jsonGen.writeNumberField(fldDetail.getDwOutputStr(), fieldBigDecimal);
                }
                else
                {
                    jsonGen.writeNullField(fldDetail.getDwOutputStr());
                }
            }
            /*
             * else if (StringUtil.equalsIgnoreCase("ariba.base.core.LongString", fldDetail.getAribaFieldType())) { if (!StringUtil.nullOrEmptyOrBlankString(fieldVal)) { fieldBigDecimal = new BigDecimal(fieldVal); } jsonGen.writeNumberField(fldDetail.getDwOutputStr(), fieldBigDecimal); }
             */
            else
            {
                jsonGen.writeStringField(fldDetail.getDwOutputStr(), fieldVal);
            }
        }
    }

    private static String getFieldValue(Object obj, String fieldType)
    {
        String lsResult = "";
        Object loValue;
        SimpleDateFormat sdf_ET2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String eol = System.getProperty("line.separator");

        Log.customer.debug(msCN + "::getFieldValue(); obj=" + obj + "; fieldType=" + fieldType);
        if (obj != null)
        {
            if (fieldType.equals("java.lang.String"))
            {
                lsResult = (String) BuyintXWalk.XWalk.changeNewToOld((String) obj);
                lsResult = lsResult.replaceAll("\r\n", eol); // replace with correct newline character for all String type fields
            }
            else if (fieldType.equals("java.lang.Boolean"))
            {
                lsResult = ((Boolean) obj).toString();
            }
            else if (fieldType.equals("java.lang.Integer"))
            {
                lsResult = ((Integer) obj).toString();
            }
            else if (fieldType.equals("ariba.basic.core.Money"))
            {
                loValue = ((BaseObject) obj).getDottedFieldValue("Amount");
                if (loValue != null)
                {
                    lsResult = (((BigDecimal) loValue).setScale(5, BigDecimal.ROUND_HALF_UP)).toString();
                }

            }
            else if (fieldType.equals("java.math.BigDecimal"))
            {
                lsResult = ((BigDecimal) obj).toString();
            }
            else if (fieldType.equals("ariba.base.core.LongString"))
            {
                lsResult = ((ariba.base.core.LongString) obj).string();
                lsResult = lsResult.replaceAll("\r\n", eol); // replace with correct newline character for all String type fields
            }
            else if (fieldType.equals("ariba.base.core.MultiLingualString"))
            {
                lsResult = ((ariba.base.core.MultiLingualString) obj).getPrimaryString();
                lsResult = lsResult.replaceAll("\r\n", eol); // replace with correct newline character for all String type fields
            }
            else if (fieldType.equals("ariba.util.core.Date"))
            {
                lsResult = sdf_ET2.format((ariba.util.core.Date) obj);
            }

            else
            {
                lsResult = obj.toString();
            }
        }
        return lsResult;
    }

    private static String getParentBaseIDs(BaseObject foApprovalRequest)
    {
        String parentVec = "";
        Log.customer.debug("*****%s***** invoked getParentBaseIDs" , msCN);
        
        if (foApprovalRequest instanceof ApprovalRequest)
        {
            List vecParents = (List) foApprovalRequest.getFieldValue("Dependencies");
            ApprovalRequest parentApprReq;
            int size = vecParents.size();

            for (int i = 0; i < size; i++)
            {
                parentApprReq = (ApprovalRequest) vecParents.get(i);
                if (i == 0)
                {
                    parentVec = parentApprReq.getBaseId().toDBString();
                }
                else
                {
                    parentVec = parentVec + "," + parentApprReq.getBaseId().toDBString();
                }
            }
        }
        return parentVec;
    }
}
