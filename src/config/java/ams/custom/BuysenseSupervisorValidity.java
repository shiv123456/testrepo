package config.java.ams.custom;

import java.util.List;

import ariba.admin.core.Log;
import ariba.approvable.core.Approvable;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ConditionResult;
import ariba.base.fields.ValueInfo;
import ariba.base.fields.ValueSource;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.workforce.core.LaborLineItemDetails;


public class BuysenseSupervisorValidity extends Condition{

	
	private static final ValueInfo parameterInfo[];
	String msMessage = null;
    private static         String        errorStringTable        = "ariba.procure.core";
    private static         String        supervisorValidationError   = "SupervisorValidationError";
    public ConditionResult evaluateAndExplain(Object object, PropertyTable params)
    {
        Log.customer.debug("Calling BuysenseEmployeeNumberValidation for new User Profile Request Eform.");
        if(!evaluate(object, params))
        {
            String retMsg = null;
            if(!StringUtil.nullOrEmptyOrBlankString(msMessage))
            {
                retMsg = msMessage;
                msMessage = null;
                return new ConditionResult(retMsg);
            }
            else
                return null;
        }
        return null;
    }
	public boolean evaluate(Object object, PropertyTable params)
	{
		String lsRoleCheck = "eVA-No Supervisor";
		BaseObject baseobj  = (BaseObject)params.getPropertyForKey("SourceObject");	
		Log.customer.debug("Inside BuysenseSupervisorEditability "+baseobj);
		BaseObject boAdditionalRoles = null;
		String lsRoleUniqueName = null;
		Boolean lbreturnvalue = false;
		if(((Approvable)baseobj).instanceOf("ariba.core.BuysenseUserProfileRequest"))
		{
     		BaseVector bvAdditionalRoles = (BaseVector)baseobj.getFieldValue("AribaAdditionalRoles");
     		Log.customer.debug("Inside BuysenseSupervisorEditability : AdditionalRoles is"+bvAdditionalRoles);

			if(bvAdditionalRoles!=null)
			{
				Log.customer.debug("Inside BuysenseSupervisorEditability");
				
				for(int i=0;i<bvAdditionalRoles.size();i++)
				{
					boAdditionalRoles = (BaseObject)Base.getSession().objectFromId((BaseId) bvAdditionalRoles.get(i));
					Log.customer.debug("Inside BuysenseSupervisorEditability : boRoles1 is"+boAdditionalRoles);
						lsRoleUniqueName = (String)boAdditionalRoles.getFieldValue("UniqueName");
					    if(lsRoleUniqueName.equalsIgnoreCase(lsRoleCheck))
						{
							Log.customer.debug("Inside BuysenseSupervisorEditability : Additional Role is"+lsRoleUniqueName);
							lbreturnvalue = true;
						}
				}
				
			}
			ariba.user.core.User loSupervisor = (ariba.user.core.User)baseobj.getFieldValue("AribaSupervisor");
			if(lbreturnvalue == false && loSupervisor == null)
			{
				msMessage = Fmt.Sil(Base.getSession().getLocale(),errorStringTable, supervisorValidationError);
					return false;
			}
			else
			{
				return true;
			}
	    }
	
	return true;

}
	 protected ValueInfo[] getParameterInfo()
	 {
		 return parameterInfo;
	 }
	 static
     {
		 parameterInfo = (new ValueInfo[] {
         new ValueInfo("SourceObject", 0)
         });
	 }
	
}
