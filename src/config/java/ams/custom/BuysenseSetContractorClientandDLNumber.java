package config.java.ams.custom;

import ariba.approvable.core.Comment;
import ariba.base.core.aql.*;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.user.core.Group;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;
import ariba.workforce.core.Contractor;
import ariba.common.core.UserProfile;
import ariba.common.core.UserProfileDetails;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.BaseVector;
import ariba.base.core.LongStringElement;
import ariba.base.core.Partition;
/**
 *  Author: Sarath Babu Garre.
 *  Date : 24-Aug-2009
 *  ACP Requirement: Trigger to set field values for UserProfileDetails object before 
 *  submitting a User Profile.*  
 *  
**/

public class BuysenseSetContractorClientandDLNumber extends Action
{

	public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
	{			
		UserProfile userProfile = (UserProfile)valuesource;
		Log.customer.debug("BuysenseSetContractorClientandDLNumber Clusterroot returned  = " + userProfile);		
		ariba.common.core.User partUser = (ariba.common.core.User)userProfile.getDottedFieldValue("User");		
		if(partUser !=null)
		{			
			ariba.user.core.User user = (ariba.user.core.User)partUser.getUser();		
			Boolean BisContractor =(Boolean)user.getFieldValue("IsContractor");
			if (BisContractor==null || BisContractor.booleanValue()== false)
			{
				Log.customer.debug("BuysenseSetContractorClientandDLNumber user is not a contractor");
				return;
			}		
			else if (BisContractor.booleanValue()== true)
			{
				String sDriversLicenseStateNumber=null;
				Log.customer.debug("BuysenseSetContractorClientandDLNumber user is a contractor");
				Partition partition = Base.getSession().getPartition();
				AQLOptions options = new AQLOptions(partition);
				AQLQuery queryContractor = new AQLQuery("ariba.workforce.core.Contractor");			
				queryContractor.andEqual("User", user);
				Log.customer.debug("BuysenseSetContractorClientandDLNumber Contractor query"+queryContractor);
				UserProfileDetails profileDetails = (UserProfileDetails)userProfile.getFieldValue("Details");				
				AQLResultCollection results = Base.getService().executeQuery(queryContractor, options);
				if(results.getErrors() != null)
		        {	            
		            return;
		        }
				while(results.next()) 
		        {
					BaseId id = (BaseId)results.getObject(0);
		            Contractor contractor = (Contractor)id.get();		            
		            BaseObject lOClientName = null;
		            lOClientName = (BaseObject)contractor.getFieldValue("ClientName");		            
		            sDriversLicenseStateNumber = (String)contractor.getFieldValue("DriversLicenseStateNumber");	            
				    profileDetails.setFieldValue("ClientName", lOClientName);
				    profileDetails.setFieldValue("DriversLicenseStateNumber", sDriversLicenseStateNumber);
				    ariba.user.core.UserProfileDetails commonProfileDetails = (ariba.user.core.UserProfileDetails)profileDetails.getUserProfileDetails();
	                BaseVector groups = (BaseVector)commonProfileDetails.getFieldValue("Groups");
	                AQLQuery queryGroup = new AQLQuery("ariba.user.core.Group");
	                queryGroup.andEqual("UniqueName", "Create Time Sheet");
	                Log.customer.debug("BuysenseSetContractorClientandDLNumber Contractor query"+queryGroup);
	                AQLOptions groupOptions = new AQLOptions(Partition.None);
	                AQLResultCollection groupResults = Base.getService().executeQuery(queryGroup, groupOptions);
	                if(groupResults.getErrors() != null)
			        {	            
			            return;
			        }
					while(groupResults.next()) 
			        {					
						BaseId groupId = (BaseId)groupResults.getObject(0);
			            Group group = (Group)groupId.get();		            					
			            groups.add(group);		            
			            commonProfileDetails.setFieldValue("Groups", groups);
			        }						    
			    }
				BaseVector vComments=(BaseVector)userProfile.getComments();
				if(vComments.size()==0)
				{
					Log.customer.debug("BuysenseSetContractorClientandDLNumber No Comments.");
					return;
				}
				Comment comment = null;
				comment = (Comment) vComments.get(0);			
				BaseVector vcommentsString = (BaseVector)comment.getDottedFieldValue("Text.Strings");
				LongStringElement commentLongString = (LongStringElement)vcommentsString.get(0);
				String commentSting  =(String) commentLongString.getFieldValue("String");				
				Log.customer.debug("BuysenseSetContractorClientandDLNumber Comments."+commentSting);
				int i= commentSting.indexOf("Driver's License Number:");
				if(i>=0)
				{
					String Substring = commentSting.substring(0,i+25);
					Substring = Substring+sDriversLicenseStateNumber;
					commentLongString.setFieldValue("String", Substring);
				}
			}		
	}
	}
}
