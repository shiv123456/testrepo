package config.java.ams.custom;

import ariba.basic.core.Money;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.condition.ValidReqAmount;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

/**
 * CSPL-6106: Ariba 9r1 -  Enable negative Price line items in Requisition.
 * 
 * @author Pavan Aluri
 * @Date 11-Apr-2014
 * @version 1.0
 * @Explanation minValue() method of ValidReqAmount (SP30) is overridden to pass negative min value for Requisition Line Amount field in order to allow negative Priced line items.
 * Note: For Amount field, min property is not defined and during run time system takes it as Amount Accepted, which is defaulted to 0.0D
 * 
 */

public class BuysenseValidReqAmount extends ValidReqAmount
{

    private String sClass = "BuysenseValidReqAmount";

    /*/
     * For reference, SP30 OOTB version of minValue() method is commented as below.
     */
    /*protected Money minValue(PropertyTable params)
    {
        Money minValue = super.minValue(params);
        ReqLineItem rli = (ReqLineItem)params.getPropertyForKey(ReqLineItemParam);
        LineItemCollection lic = rli.getLineItemCollection();
        if(lic == null || lic.getPreviousVersion() == null || !lic.isCreationInProgress() && lic.getApprovedState() != 2)
            return null;
        if(rli.getEditableAccess() != Access.Now)
            return null;
        ReqLineItem oldValue = (ReqLineItem)rli.getOldLineItemValuesIfAny();
        if(oldValue != null && rli.getAmount().compareTo(oldValue.getAmount()) == 0)
            return null;
        else
            return minValueInternal(rli, minValue);
    }*/

    @Override
    protected Money minValue(PropertyTable params)
    {
        Money minValue = super.minValue(params);
        ReqLineItem rli = (ReqLineItem) params.getPropertyForKey(ReqLineItemParam);
        ReqLineItem oldValue = (ReqLineItem) rli.getOldLineItemValuesIfAny();
        Log.customer.debug(sClass + "::minValue() - oldValue: " + oldValue);
        if (oldValue == null && (rli.getAmount().getSign() == -1))// this is to skip validation for new items
        {
            Log.customer.debug(sClass + "::minValue() returning null for new -ve line");
            return null;
        }
        else if (oldValue != null && oldValue.getAmount().isNegative())// this is to skip validation on changed orders
        {
            //Current version Amount should be greater than or equals to previous Amount accepted OR previous line Amount
            //AmountAccepted is defaulted to $0.0 and for negative lines AmountAccepted gets updated as negative value.
            //Hence, Min value is negative AmountAccepted OR negative Previous Amount. 
            Money accepted = rli.getAmountAccepted();
            Log.customer.debug(sClass + "::minValue() - accepted: " + accepted + "; oldValue.getAmount(): "
                    + oldValue.getAmount());
            if (accepted != null && accepted.isNegative())
            {
                return accepted;
            }
            else
            {
                return oldValue.getAmount();
            }

        }

        Log.customer.debug(sClass + "::minValue() - returning minValue: " + minValue);
        return minValue;

    }

}
