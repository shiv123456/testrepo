package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.ClusterRoot;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseDefaultLogiRTLimit extends Action{
	
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        Approvable loAppr = null;
        AQLOptions opt = new AQLOptions();
        opt.setUserLocale(Base.getSession().getLocale());
        opt.setUserPartition(Base.getSession().getPartition());
        if(valuesource!= null && valuesource instanceof Approvable)
        {
           loAppr = (Approvable)valuesource;
           String lsQuery = "Select s from ariba.core.BuysenseLogiRTLimit s where Name = '5K'";
           AQLQuery query = AQLQuery.parseQuery(lsQuery);
           AQLResultCollection resultsCollection =
               Base.getService().executeQuery(query, opt);
           while(resultsCollection.next())
           {
        	   BaseId id = resultsCollection.getBaseId(0);
        	   loAppr.setFieldValue("LogiReportThresholdLimit", id);
        	   Log.customer.debug("The LogiReportThresholdLimit is now"+loAppr.getFieldValue("LogiReportThresholdLimit.Name"));
           }
        }
    }

}
