package config.java.ams.custom;

import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLNameTable;
import ariba.base.core.aql.SearchTermQuery;
import ariba.base.fields.ValueSource;
import ariba.purchasing.core.ReqLineItem;
import ariba.util.log.Log;


public class BuysenseStandardRoleNameTable extends AQLNameTable{
	
    public static final String ClassName="config.java.ams.custom.BuysenseStandardRoleNameTable ";
    public static final String FieldName="AribaStandardRoles";

    
    /* Ariba 8.1 - As part of rewriting the class to only return Approvers that are of type ariba.user.core.User or
                   ariba.user.core.Group, replaced the matchPattern() and addQueryConstraints() methods with buildQuery().  */
    public void addQueryConstraints(AQLQuery query, String field, String pattern, SearchTermQuery searchTermQuery)
    {
    	Log.customer.debug( "Inside BuysenseStandardRoleNameTable") ;
    	ValueSource valueSourceCurrent = getValueSourceContext();
    	super.addQueryConstraints(query, field, pattern, searchTermQuery);
    	String lsAgency = null;
    	String lsRole1 = "eVA-CreateRequisition";
    	String lsRole2 = "eVA-Rpt-Hier";
    	if (valueSourceCurrent instanceof ariba.approvable.core.Approvable && valueSourceCurrent.getTypeName().equals("ariba.core.BuysenseUserProfileRequest"))
        { 
    	
    		lsAgency = (String)valueSourceCurrent.getFieldValue("Agency");
            if(lsAgency == null)
            {
                query.andFalse();
                return;
            }
            else
            {
        		String lsAgencyName = lsAgency.substring(0, 4);
        	    String lsRole3 = lsAgencyName + "-" + "Query All";
                Log.customer.debug("The initial query is"+query);
                query.and(AQLCondition.parseCondition(" UniqueName IN ('" + lsRole1 + "','" + lsRole2 + "','" + lsRole3 + "')"));
                Log.customer.debug("The final query is"+query);            	
            }

        }
    	else
    	{
            query.andFalse();
            Log.customer.debug(ClassName + " No constraints are added to the Query : ");
    	}
    }

}
