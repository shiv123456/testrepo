/*
 * @(#)BuyinEncumbranceAction.java      1.0 2004/08/18
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information"). You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *
 *
 * PVCS Modification :
 *
 * $Log:   R:\Software\development\archives\Ariba 8.x Customizations\app\buyer\Server\classes\extensions\config\java\ams\custom\BuyintEncumbranceAction.java-arc  $
 *
 *    Rev 1.8   04 Jul 2006 09:09:34   ngrao
 * VEPI Dev SPL #670: INT - Updates to fix due to functional clarifications.
 *
 *    Rev 1.7   06 Mar 2006 13:52:36   ngrao
 * VEPI Dev #670: INT - new cancel transaction to differentiate between pre-encumbrance negation due to order cancel vs. change order.
 *
 *    Rev 1.6   01 Feb 2006 16:40:52   rlee
 * Dev SPL 719 - INT - Set EncumbranceStatusChangeDate with trigger.
 *
 *    Rev 1.5   07 Nov 2005 16:52:54   rlee
 * Dev SPL 655 - Update BuyintEncumbranceAction.java
 *
 *
 */

package config.java.ams.custom;

import ariba.purchasing.core.Requisition;
import ariba.purchasing.core.PurchaseOrder;
import java.util.Map;
import ariba.pcard.core.PCardOrder;
import java.util.List;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.ERPOrder;
import ariba.purchasing.core.POLineItem;
import ariba.base.core.*;
import ariba.common.core.*;
import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.purchasing.core.action.IntegrationPostUpdateERPOrder;
import ariba.purchasing.core.action.IntegrationPostLoadPOError;
import java.util.Iterator;

//81->822 changed Vector to List
//81->822 changed Hashtable to Map
public class BuyintEncumbranceAction implements BuyintConstants
{
   private Partition moPartition = null;
   private PropertyTable moPropTable = null;
   private Map moParams = MapUtil.map();
   private String msResponseType = null;
   private String msAribaObjectUN = null;
   private String msMessage = null;
   private String msRecordType = null;
   private String msIntegrationRule = null;

   User moAribaSystemUser = null;

   public void fire (ClusterRoot foObject, Map foParams)
   {
      Log.customer.debug("BuyintEncumbranceAction: fire() - Received this object %s", foObject);

      moParams = foParams;
      moPropTable = new PropertyTable(moParams);

      //Get the partition
      PurchaseOrder loOrder = (PurchaseOrder)foObject;
      moPartition = loOrder.getPartition();

      //Get the aribasystem user at the partition level
      moAribaSystemUser = Core.getService().getAribaSystemUser(moPartition);

      // Get approvable UniqueName
      msAribaObjectUN = (String)loOrder.getUniqueName();

      Requisition loRequisition=null;

      try
      {

         // Dev SPL 655 loRequisition = (Requisition)(((POLineItem)(loOrder.getLineItem(1))).getRequisition());
         loRequisition = (Requisition)((POLineItem)loOrder.getDottedFieldValue("LineItems[0]")).getRequisition();

         Log.customer.debug("BuyintEncumbranceAction: calling processPO");
         processPO(loOrder, loRequisition);
      }
      catch (Exception loE)
      {
         Log.customer.debug("BuyintEncumbranceAction: Caught exception in fire().");
         Log.customer.debug(SystemUtil.stackTrace(loE));
      }
  }

  public void processPO(PurchaseOrder foOrder,
                        Requisition foRequisition)
  {
     Log.customer.debug("BuyintEncumbranceAction: In processPO(): " + " po: " + foOrder +
     " requisition: " + foRequisition);

     for ( Iterator loItr = moParams.keySet().iterator(); loItr.hasNext() ; )
     {
        String key = (String) loItr.next();
        if(key.equalsIgnoreCase("response_type"))
        {
           msResponseType = (String)moParams.get(key);
        }
        if(key.equalsIgnoreCase("erp_message"))
        {
           msMessage = (String)moParams.get(key);
        }
        if(key.equalsIgnoreCase("integration_signer_rule"))
        {
           msIntegrationRule = (String)moParams.get(key);
        }
     }

     if(msResponseType == null ||
        !(msResponseType.equalsIgnoreCase(STATUS_ERP_APPROVE) || msResponseType.equalsIgnoreCase(STATUS_ERP_DENY))
       )
     {
        Log.customer.debug("BuyintEncumbranceAction: Response Type is not valid.");
        return;
     }

     if (msResponseType.equalsIgnoreCase(STATUS_ERP_APPROVE))
     {
        msRecordType = RECORD_ENCUMBER_SUCCESS;
        foOrder.setFieldValue("EncumbranceStatus", STATUS_ERP_APPROVE);
        msIntegrationRule = null;
     }
     else
     {
        msRecordType = RECORD_ENCUMBER_FAIL;
        foOrder.setFieldValue("EncumbranceStatus", STATUS_ERP_DENY);
        if (foOrder instanceof PCardOrder)
        {
           msIntegrationRule = null;
        }
     }

     ariba.user.core.User loRequesterUser = (ariba.user.core.User)foRequisition.getDottedFieldValue("Requester");
     ariba.common.core.User loRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(loRequesterUser,moPartition);

     //cast the req/po to a baseobject as createHistroy takes only BaseObject, create history
     BaseObject loReqObject = (BaseObject) foRequisition;
     BuysenseUtil.createHistory(loReqObject, msAribaObjectUN,  msRecordType, msMessage, moAribaSystemUser);
     BaseObject loPoObject = (BaseObject) foOrder;
     BuysenseUtil.createHistory(loPoObject, msAribaObjectUN,  msRecordType, msMessage, moAribaSystemUser);
     updateApprovables(foOrder,foRequisition);

     if (foOrder instanceof ERPOrder)
     {
        if (msResponseType.equalsIgnoreCase(STATUS_ERP_APPROVE))
        {
           postPOProcess((ERPOrder)foOrder);
           cancelPreviousVersion(foOrder);
        }
        else
        {
           failedPOProcess((ERPOrder)foOrder);
        }
     }

  }

  private void updateApprovables(PurchaseOrder foOrder,
                                 Requisition foRequisition)
  {
     // Update PO/Req

     if (foOrder instanceof ERPOrder)
     {
     foOrder.setFieldValue("ERPPONumber", msAribaObjectUN);
     }
     if (msResponseType.equalsIgnoreCase(STATUS_ERP_APPROVE))
     {
        foOrder.setFieldValue("Encumbered", new Boolean(true));
     }
     else
     {
        foOrder.setFieldValue("Encumbered", new Boolean(false));
     }

     updateReqLines(foOrder, foRequisition);

  }

  private void postPOProcess(ERPOrder foOrder)
  {
     try
     {
        IntegrationPostUpdateERPOrder postUpdateOrder
                = new IntegrationPostUpdateERPOrder();
        postUpdateOrder.fire(foOrder,moPropTable) ;
     }
     catch (Exception loE)
     {
        Log.customer.debug("BuyintEncumbranceAction: Caught exception in postPOProcess().");
        Log.customer.debug(SystemUtil.stackTrace(loE));
     }
  }

  private void failedPOProcess(ERPOrder foOrder)
  {
      Log.customer.debug("BuyintEncumbranceAction: Hit the ERP error section");

      /****************************************************************************************
       * CSPL-912 COVA agreed that should not be sending PCNCL after ERP Deny in PO Integration
       * Therefore removed the codes to send PCNCL to ERP here.
       * The Requisition PreEncumbraceStatus will not be updated, since it is possible for the
       * requisition to have two PO integration orders where ERP could APPROVE one and DENY the
       * the other one.  In this case, it would not be correct to update the PreEncumbranceStatus
       * to DENY and PreEncumbered to false, since one of the orders is APPROVE and in ordered status.
       ****************************************************************************************/

      ClusterRoot loPOE = (ClusterRoot) ClusterRoot.create("ariba.integration.core.PurchaseOrderError", moPartition);
      loPOE.setFieldValue("AddedToReq",new Boolean(false));
      loPOE.setFieldValue("Id",msAribaObjectUN);
      loPOE.setFieldValue("Type","ERP Encumbrance Error");
      loPOE.setFieldValue("Date",new ariba.util.core.Date());

      BaseObject loIEI = (BaseObject)BaseObject.create("ariba.integration.core.IntegrationErrorItem",moPartition);

      //START: Added by srini for comment break issue for CSPL - 3068
      String lsNullValue = null;
      loIEI.setDottedFieldValue("DataSource",lsNullValue);
      loIEI.setDottedFieldValue("DataColumn",lsNullValue);
      loIEI.setDottedFieldValue("DataFieldName",lsNullValue);
      loIEI.setDottedFieldValue("DataValue",lsNullValue);
      loIEI.setDottedFieldValue("ErrorCode",lsNullValue);
      //END: Added by srini for comment break issue for CSPL - 3068
      
      loIEI.setDottedFieldValue("ItemId","1");
      loIEI.setDottedFieldValue("ErrorMessage",
                                "Please refer to the history for details");
      List loItemVector = (List)loPOE.getDottedFieldValue("Items");
      loItemVector.add(loIEI);

      loPOE.save();
      Log.customer.debug("BuyintEncumbranceAction: Successfully saved loPOE");

      IntegrationPostLoadPOError loPOError = new IntegrationPostLoadPOError();
      Log.customer.debug("BuyintEncumbranceAction: Before firing poError method: %s",loPOError);
      try
      {
         loPOError.fire((ariba.integration.core.PurchaseOrderError)loPOE,moPropTable);
      }
      catch (Exception loE)
      {
         Log.customer.debug("BuyintEncumbranceAction: Error occured while calling the fire method of PurchaseOrderError");
         Log.customer.debug(SystemUtil.stackTrace(loE));
      }
  }

  private void updateReqLines(PurchaseOrder foOrder,
                              Requisition foRequisition)
  {
     String lsEncumNumber = null;
     List loPOLIVector = foOrder.getLineItems();
     List loReqLIVector = foRequisition.getLineItems();
     int liPOLISize = loPOLIVector.size();
     int liReqLISize = loReqLIVector.size();
     int liNumberOnReq = 0;
     POLineItem loPOLine = null;
     ReqLineItem loReqLine = null;

     // We set the Encumbrance number on Req Line even on integration denial
     lsEncumNumber = (String)foOrder.getFieldValue("EncumbranceNumber");

     for (int liPOCount = 0 ;liPOCount < liPOLISize; liPOCount++)
     {
         loPOLine = (POLineItem)loPOLIVector.get(liPOCount);

         liNumberOnReq = ((Integer) loPOLine.getFieldValue("NumberOnReq")).intValue();

         for (int liReqCount = 0; liReqCount < liReqLISize; liReqCount++)
         {
             loReqLine = (ReqLineItem)loReqLIVector.get(liReqCount);
             if (liNumberOnReq == loReqLine.getNumberInCollection())
             {
                loReqLine.setFieldValue("EncumbranceNumber", lsEncumNumber);
                loReqLine.setFieldValue("LineIntSignerRule", msIntegrationRule);
                break;
             }
         }
     }
  }

    public void cancelPreviousVersion(PurchaseOrder po)
    {
       // Ariba 8.1 Upgrade - Send encumbrance cancellation transaction, if necessary
       PurchaseOrder previousPO = (PurchaseOrder) po.getPreviousVersion();
       if (previousPO != null)
       {
          if (((Boolean)previousPO.getFieldValue("Encumbered")).booleanValue() &&
                ((String)previousPO.getFieldValue("EncumbranceStatus")).trim().equalsIgnoreCase(STATUS_ERP_APPROVE))
          {
             // Previous version encumbered, send cancellation transaction
             Log.customer.debug("BuyintEncumbranceAction: Encumbrance cancellation needed.");

             Requisition prevRequisition = null;
             try
             {
                List prevVector = (List) previousPO.getFieldValue("LineItems") ;
                POLineItem prevLine = (POLineItem)prevVector.get(0);

                prevRequisition = (Requisition)prevLine.getFieldValue("Requisition");
             }
             catch (Exception loE)
             {
                Log.customer.debug("BuyintEncumbranceAction: Caught exception while getting a handle to Requisition.");
                Log.customer.debug(SystemUtil.stackTrace(loE));
             }
             previousPO.setFieldValue("EncumbranceStatus", STATUS_ERP_CANCELLED);
             previousPO.setFieldValue("Encumbered", new Boolean(false));
             String lsPoUniqueName = (String)previousPO.getUniqueName();
             BuysenseUtil.createHistory(previousPO, lsPoUniqueName, RECORD_ENC_CANCEL, null, moAribaSystemUser);
             BuysenseUtil.createHistory(prevRequisition, lsPoUniqueName, RECORD_ENC_CANCEL, null, moAribaSystemUser);
             BuyintXMLFactory.submit(TXNTYPE_ENC_CANCEL_ON_CHG, previousPO);
          }
       }
    }

    public Map testPOOrdering()
    {
       Log.customer.debug("BuyintEncumbranceAction: testPOOrdering got called.");
       Map loHashtable = MapUtil.map();
       loHashtable.put("response_type", "APPROVE");
       loHashtable.put("erp_message", "ERP encumbrance approved.");
       loHashtable.put("integration_signer_rule", "555555");

       return loHashtable;
    }

    public ClusterRoot getOrder()
    {
       Map loHash = testPOOrdering();
       ClusterRoot loOrder = Base.getService().objectMatchingUniqueName("ariba.purchasing.core.ERPOrder", Base.getService().getPartition("pcsv"), "EP96");
       return loOrder;
    }
}

