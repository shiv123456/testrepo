package config.java.ams.custom;

import ariba.app.util.Attachment;
import ariba.base.core.Base;
import ariba.htmlui.workforce.fields.ARVAttachment;
import ariba.util.core.ResourceService;
import ariba.util.log.Log;

public class eVA_ARVAttachment extends ARVAttachment 
{

    public String curAttachmentName()
    {
        //return ResourceService.getService().getLocalizedString("ariba.html.workforceui", "ResumeLink", Base.getSession().getLocale());
    	// HERE RETURN THE RESUME FILE NAME.
    	Log.customer.debug("Inside eVA_ARVAttachment.curAttachmentName() ");
    	Log.customer.debug("Inside eVA_ARVAttachment.curAttachmentName(), value()is: "+value());
    	Attachment attachment = (Attachment)value();
    	if(attachment != null)
    	{
    	String fileName = attachment.getFilename();
    	Log.customer.debug("Inside eVA_ARVAttachment attachment file name is: "+fileName);
    	return fileName;
    	}else
    	{
    	Log.customer.debug("Inside eVA_ARVAttachment.curAttachmentName(), there is no attachment and returning default file name.");
    	 return ResourceService.getService().getLocalizedString("ariba.html.workforceui", "ResumeLink", Base.getSession().getLocale());
    	}
    	
    }
}
