/************************************************************************************
 * Author:  Richard Lee
 * Date:    June 2005
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 06/08/2005        Richard Lee            CR# 22 - Punchout get Contract Number
 *
 *
 * @(#)ParsePunchoutContractNum.java     1.0 06/08/2005
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/

package config.java.ams.custom ;

import java.util.List;
import java.io.* ;

import org.apache.log4j.Level;

import ariba.base.core.*;
import ariba.util.log.Log;

//81->822 changed Vector to List
public class ParsePunchoutContractNum extends BuysenseXMLImport
{
	protected BaseObject moBaseObj = null;
	String lsErrorMessage = null;
	String msXML = null;
	String msReqUN = null;
	String msFieldName = null;

        /**
         *  Constructor.
         */
        public ParsePunchoutContractNum()
        {
            super() ;
            moPartition = Base.getSession().getPartition() ;
        }
        /**
        *  Constructor.
        */
        public ParsePunchoutContractNum( Partition foPartition )
        {
            super() ;
            moPartition = foPartition ;
        }
	protected void parseAndSetValue (BaseObject foBaseObj, ByteArrayInputStream larrayXML, String lsString)
	{
		msXML = lsString;
		Logs.buysense.setLevel(Level.DEBUG);
                ClusterRoot cr = (ClusterRoot)foBaseObj.getClusterRoot();
                msReqUN = (String) cr.getDottedFieldValue("UniqueName");
		try
		{
			moBaseObj = foBaseObj;
			this.importFromXML(larrayXML);
		}
		catch ( Exception loExcep )
		{
			lsErrorMessage = loExcep.getMessage() ;
			Logs.buysense.debug("ParsePunchoutError for Requisition " + msReqUN +
				       " " + lsErrorMessage);
			Logs.buysense.debug("This is the XML received : " + lsString ) ;
		}
                finally
                {
		        Logs.buysense.setLevel(Level.DEBUG.OFF);
                }
	}
       /**
	* This method processes the start of the BuysenseImport.
	*
	* @param foAttrList The XML tag attributes parsed
	* @exception BuysenseXMLImportException
	*/
	protected void startBuysenseImport( ContextObject foMyContext )
	throws BuysenseXMLImportException
	{
		try
		{
			Log.customer.debug("startBuysenseImport. ");
		}
		catch (Exception loExcep)
		{
			lsErrorMessage= "startBuysenseImport exception: " + loExcep.getMessage();
			addToErrorList(lsErrorMessage);
		}
	}

	/**
	* This method processes the start of the field element.
	*
	* @param foAttrList The XML tag attributes parsed
	* @exception BuysenseXMLImportException
	*/
	protected void startField( ContextObject foMyContext )
	throws BuysenseXMLImportException
	{
		try
		{
			Log.customer.debug("Inside startField.");
			return;
		}
		catch (Exception loExcep)
		{
			lsErrorMessage= "startField exception: " + loExcep.getMessage();
			addToErrorList(lsErrorMessage);
		}
	}

	/**
	* This method processes the end of the field element.
	*
	* @param foAttrList The XML tag attributes parsed
	* @exception BuysenseXMLImportException
	*/
	protected void finishField( ContextObject foMyContext )
	throws BuysenseXMLImportException
	{
		try
		{
			Log.customer.debug("Calling finishField method to update " + moBaseObj);
			Object loValueObj = null;
			String lsAribaVal  = foMyContext.getText() ;
			String lsAribaName = foMyContext.getAttrValue( ARIBA_FIELD_NAME ) ;
			String lsAribaTyp  = foMyContext.getAttrValue( ARIBA_TYPE ) ;
			String lsAribaRef  = foMyContext.getAttrValue( ARIBA_REFERENCE ) ;
			String lsValPrefix = foMyContext.getAttrValue( ARIBA_VAL_PREFIX ) ;
			msFieldName = lsAribaName;

			if ((lsAribaVal == null)||(lsAribaVal.length()<1))
			{
				// There is no value to set from XML,
				// so make sure value is reset in BaseObject
				if ( BuysenseXMLFieldParser.resetFieldValue(moBaseObj, lsAribaName, lsAribaTyp))
				{
					Log.customer.debug("Calling resetFieldValue method.");
				}
				else
				{
					Log.customer.debug( "Error: could not reset field: " + lsAribaName);
					// Instead of throwing an Exception, start accumulating Errors
					addToErrorList( "ParsePunchoutError when resetting field: " + lsAribaName);
				}
			}
			if ((lsValPrefix != null)&& (lsValPrefix.length()>0))
			{
				lsAribaVal = lsValPrefix + lsAribaVal ;
			}

			if ((lsAribaRef != null)&&(lsAribaRef.equals("true")))
			{
				loValueObj = BuysenseXMLFieldParser.lookupFieldValue( lsAribaVal, lsAribaTyp, moPartition);
			}
			else
			{
				loValueObj = BuysenseXMLFieldParser.parseFieldValue( lsAribaVal, lsAribaTyp);
				Log.customer.debug("lsAribaRef is null or true. loValueObj = " + loValueObj);

			}
			if (loValueObj != null)
			{
				moBaseObj.setDottedFieldValue( lsAribaName, loValueObj ) ;
			}
			else
			{
				Log.customer.debug( "Error: could not set field: " +
							   lsAribaName + " with value: " +
							   lsAribaVal  ) ;

				// Instead of throwing an Exception, start accumulating Errors
				addToErrorList("Could not set field: " + lsAribaName + " with value: " + lsAribaVal +
								" Data Type: " + lsAribaTyp);
			}
		}
		catch (Exception loExcep)
		{
			Log.customer.debug("Inside finishField exception. " + msFieldName);
			lsErrorMessage= "Requisition: " + msReqUN + ", field: " + msFieldName +
                                                         ", error related to: " + loExcep.getMessage();
			addToErrorList(lsErrorMessage);
		}
	}
	/**
         * This method processes the end of the BuysenseImport element.
	 *
	 * @param foAttrList The XML tag attributes parsed
	 * @exception BuysenseXMLImportException
	 */
	protected void finishBuysenseImport( ContextObject foMyContext )
	throws BuysenseXMLImportException
	{
		try
		{
			Log.customer.debug( "Inside finishBuysenseImport.");
		}
		catch ( Exception loExcep )
		{
			lsErrorMessage= "finishBuysenseImport exception: " + loExcep.getMessage();
			addToErrorList(lsErrorMessage);
		}
		try
		{
			List tempVectorOfErrors = getErrorList() ;
			StringBuffer lsbErr = new StringBuffer() ;
			/*
			 * start with 1 to skip the error severity rating.
			 */
                        if(tempVectorOfErrors.isEmpty() || tempVectorOfErrors.size() < 2)
                        {
                                return;
                        }
                        int liSize = tempVectorOfErrors.size();
                        Logs.buysense.debug("Error vector size = " + liSize);

			for ( int i = 1 ; i < liSize ; i++ )
			{
				String tempString = ( String ) tempVectorOfErrors.get(i) ;
				lsbErr.append( tempString ) ;
				lsbErr.append( ";" ) ;
			}

			if( lsbErr.length() > 0)
			{
				Logs.buysense.debug( "ParsePunchoutError was encountered: " + lsbErr.toString() );
				Logs.buysense.debug( "This is the XML: " + msXML );
			}
		}
		catch ( Exception loExcep )
		{
			lsErrorMessage = loExcep.getMessage() ;
			Logs.buysense.debug( "getErrorList encountered error: " + lsErrorMessage );
		}
	}
}

