package config.java.ams.custom;

import org.w3c.dom.*;
import org.xml.sax.helpers.*;

import java.io.*;
import java.lang.reflect.*;
import java.math.*;
import java.util.*;

import ariba.purchasing.core.Requisition;
import ariba.user.core.Group;
import ariba.user.core.Permission;
import ariba.user.core.Role;
import ariba.util.log.Log;
import ariba.approvable.core.ApprovalRequest;
import ariba.approvable.core.AttachmentWrapper;
import ariba.approvable.core.CustomApprover;
import ariba.approvable.core.Record;
import ariba.user.core.Approver;
import ariba.base.core.*;
import ariba.util.core.*;
import com.megginson.sax.DataWriter;

public class BuysenseWebserviceXMLWriter implements BuyintConstants
{
   /*
    * BEGIN Constants
    */

   /* Element Tags */
    public static final String DOC = "buysensewsDoc";
    public static final String HEADER = "header";
    public static final String LINEITEMS = "lineitems";
    public static final String LINEITEM = "lineitem";
    public static final String ACCOUNTINGS = "accountings";
    public static final String SPLITACCOUNTINGS = "splitaccountings";
    public static final String SPLITACCOUNTING = "splitaccounting";
    public static final String COMMENTS = "comments";
    public static final String COMMENT = "comment";
    public static final String ATTACHMENTS = "attachments";
    public static final String ATTACHMENT = "attachment";
    public static final String APPROVALFLOWS = "approvalflows";
    public static final String APPROVALFLOW = "approvalflow";
    public static final String HISTORYS = "historys";
    public static final String HISTORY = "history";
    public static final String FIELD = "field";

    public static final String MSG = "buysenseMsg";
    public static final String REQUEST = "request";
    public static final String RESPONSE = "response";

    public static final String CDATA_PFX = "<![CDATA[";
    public static final String CDATA_SFX = "]]>";

   /* Attribute Names */
    public static final String ARIBA_TYPE = "ariba_type";
    public static final String ARIBA_FIELD_NAME = "ariba_field_name";
    public static final String FIELD_LABEL = "field_label";
    public static final String FIELD_VALUE_DESC = "field_value_description";

   /* Special purpose tags */
   /*
    * TEXT_DATA: During initial parsing in BuyintXMLContainer, non-empty Field/Element
    * are saved along with the Attributes of the Field/Element against the TEXT_DATA tag.
    * While rendering XML, the reverse is done. The value of the TEXT_DATA Attribute
    * is interpreted at runtime so that it appears as a Field/Element value rather than
    * as an attribute of the Field/Element.
    *
    */
    private static final String TEXT_DATA = "@TEXT_DATA@";

   /*
    * METHOD_INDICATOR: If the value of an attribute or field in the template starts with
    * the METHOD_INDICATOR prefix, then the remaining part of the
    * value is treated as a method name, and that method is invoked using the
    * current BaseObject (or ClusterRoot) as parameter.
    * is treated as a a method call.
    *
    */
    public static final String METHOD_INDICATOR = "@@METHOD@@";
    public static final String GET_OBJECT = "@@GET@@";
    public static final String SET_OBJECT = "@@SET@@";

   /*
    * EMPTY_ATTS: Predefined empty AttributesImpl object.
    */
    private static final AttributesImpl EMPTY_ATTS = new AttributesImpl();

   /*
    * Miscellaneous
    */
    private static final String LINEFEED = System.getProperty("line.separator");

   /*
    * END Constants
    */

    private ClusterRoot moAribaDoc = null;
    private Properties moLabels = null;

    private BuyintXMLContainer moContainer = null;
    private String msLevel = "";
    private String msClientID = "";
    private String msClientName = "";
    private String msClientNameUniqueName = "";
    private String msDocumentType = "";
    private String msDocumentID = "";
    private String msRequesterName = "";
    private String msPreparerName = "";

    private ariba.common.core.User moRequester = null;
    private ariba.common.core.User moPreparer = null;

    private static final String REQ_REQUESTER = "Requester";
    private static final String REQ_PREPARER = "Preparer";
    private static final String PO_REQUESTER = "LineItems[0].Requisition.Requester";
    private static final String PO_PREPARER = "LineItems[0].Requisition.Preparer";
    private static final String USER = "User";

    // for method invocation ... all reflection invokable methods use a single parameter ... BaseObject
    private Class[] moMethodParmTypes = null;

    private java.util.Map moAIHash = new java.util.HashMap(5);
    private java.util.Map moSavedObjects = new java.util.HashMap(5);
    // for PCards
    private static final String SKIP_LINE = "SKIPLINE";
    private static final String PCARD_TO_USE = "PCardToUse";
    private static final String PCARD = "PCard";
    private ariba.common.core.PCard moPCardToUse = null;
    boolean lbUseCard = false;
    String msPCardAlias = null;

    public BuysenseWebserviceXMLWriter(ClusterRoot foAribaDoc)
    {
        moLabels = new Properties();
        moAribaDoc = foAribaDoc;
        initData();
    }

    private void initData()
    {
        ariba.user.core.User loUser = null;

        // Set the current document type and ID
        msDocumentType = (String)moAribaDoc.getTypeName();
        msDocumentID = moAribaDoc.getUniqueName();

        // Set the Requester/Preparer based on object type
        if (moAribaDoc.instanceOf("ariba.purchasing.core.PurchaseOrder"))
        {
           loUser = (ariba.user.core.User)moAribaDoc.getDottedFieldValue(PO_REQUESTER);
           moRequester = ariba.common.core.User.getPartitionedUser(loUser,moAribaDoc.getPartition());
           loUser = (ariba.user.core.User)moAribaDoc.getDottedFieldValue(PO_PREPARER);
           moPreparer = ariba.common.core.User.getPartitionedUser(loUser,moAribaDoc.getPartition());
        }
        else
        {
           loUser = (ariba.user.core.User)moAribaDoc.getDottedFieldValue(REQ_REQUESTER);
           moRequester = ariba.common.core.User.getPartitionedUser(loUser,moAribaDoc.getPartition());
           loUser = (ariba.user.core.User)moAribaDoc.getDottedFieldValue(REQ_PREPARER);
           moPreparer = ariba.common.core.User.getPartitionedUser(loUser,moAribaDoc.getPartition());
        }

        // Set requester and preparer names
        msRequesterName = ((MultiLingualString)moRequester.getDottedFieldValue("Name")).getPrimaryString();
        msPreparerName = ((MultiLingualString)moPreparer.getDottedFieldValue("Name")).getPrimaryString();

        // Set clientid, clientname, etc. based on requester
        msClientID = (String)moRequester.getDottedFieldValue("BuysenseOrg.ClientName.ClientID");
        msClientName = (String)moRequester.getDottedFieldValue("BuysenseOrg.ClientName.ClientName");
        msClientNameUniqueName = (String)moRequester.getDottedFieldValue("BuysenseOrg.ClientName.UniqueName");
    }

    public void setContainer(BuyintXMLContainer foContainer)
    {
        moContainer = foContainer;
    }

    public BuyintXMLContainer getContainer()
    {
        return moContainer;
    }

    public StringBuffer renderDoc()
        throws BuyintException
    {
    	debug("XML WRITER CALLED");
        debug("Entered renderDoc ... ");
        StringWriter loSW = new StringWriter(2048);
        DataWriter loDW = new DataWriter(loSW);
        Properties loProps = null;
        AttributesImpl loAI = null;

        try
        {
            loDW.setIndentStep(2);
            loDW.startDocument();

            renderDTD(loDW);

            setLevel(DOC);
            loProps = moContainer.getAtts(msLevel);
            /* buysensewsDoc level always exists */
            loAI = this.getAttsImpl(loProps, moAribaDoc);
            loDW.startElement("",msLevel,"",loAI);

            moAIHash.clear();

            // do header
            renderHeader(moAribaDoc, loDW);

            loDW.endElement(DOC);
            loDW.endDocument();
        }
        catch(Exception loEx)
        {
            loEx.printStackTrace();
            throw new BuyintException("Error during renderDoc",BuyintException.EXCPT_XMLGEN_ERROR);
        }

        debug("Leaving renderDoc ... ");
        debug("FINISHED RENDERING XML");
        return loSW.getBuffer();
    }

    private void renderDTD(DataWriter foDW)
        throws BuyintException
    {
        debug("Entered renderDTD ... ");
        try
        {
            // Write the DTD into the output file
            Document loDom = moContainer.getDoc();
            String lsType = loDom.getDoctype().getName();     // This would match the DOC constant defined earlier
            String lsFile = loDom.getDoctype().getSystemId(); // This is the dtd name
            String lsDTD = BuyintXMLFactory.getDTD(lsFile);
            debug("Doctype      : " + lsType);
            debug("DTD File Name: " + lsFile);
            debug("DTD Contents : " + lsDTD);
            //Commented by SRINI to skip DTD for web services
            //foDW.startElement("","!DOCTYPE " + lsType + " [" + LINEFEED + lsDTD + LINEFEED + "]");
            // We do not end the element used for rendering DTD
        }
        catch(Exception loEx)
        {
            loEx.printStackTrace();
            throw new BuyintException("Error while rendering DTD",BuyintException.EXCPT_XMLGEN_ERROR);
        }
        debug("Leaving renderDTD ... ");
    }

    private void renderHeader(BaseObject foHeader, DataWriter foDW)
        throws BuyintException
    {
        Log.customer.debug("Calling BuysenseWebserviceXMLWriter: renderHeader");

        debug("Entered renderHeader ... ");
        java.util.List lvLIVector;
        java.util.List lvUnorderedCOVector;
        java.util.List lvCOVector;
        java.util.List lvApprovalRequestsVector;
        java.util.List lvecApprovalRequests;
        java.util.List lvAttachVector;
        BaseObject loAttach;
        java.util.List lvRecordsVector;
        BaseObject loRecord;
        int liRecordSize;
        int liRecordIndex;
        int liAttachSize;
        int liAttachIndex;
        
        BaseObject loLine;
        BaseObject loComment;
        BaseObject loApprovalRequest;
        int liLineSize;
        int liCommentSize;
        int liLineIndex;
        int liCommentIndex;
        Properties loProps;
        AttributesImpl loAI;

        try
        {
            setLevel(HEADER);
            loProps = moContainer.getAtts(msLevel);
            /* header level always exists */
            loAI = this.getAttsImpl(loProps, foHeader);
            foDW.startElement("",msLevel,"",loAI);

            renderFields(foHeader, foDW);

            setLevel(LINEITEMS);
            loProps = moContainer.getAtts(msLevel);

            if (loProps != null)       // lineitems level exists
            {
                // do lines
                lvLIVector = (java.util.List)foHeader.getDottedFieldValue("LineItems");
                liLineSize = lvLIVector.size();
                loAI = this.getAttsImpl(loProps, foHeader);
                foDW.startElement("",msLevel,"",loAI);

                for (liLineIndex = 0 ;liLineIndex < liLineSize; liLineIndex++)
                {
                    loLine = (BaseObject)lvLIVector.get(liLineIndex);
                    renderLine(loLine, foDW);
                }
                foDW.endElement(LINEITEMS);
                // done lines
            }

            setLevel(COMMENTS);
            loProps = moContainer.getAtts(msLevel);

            if (loProps != null)       // comments level exists
            {
                debug("Comments exist ... ");
                // do comments
                lvUnorderedCOVector = (java.util.List)foHeader.getDottedFieldValue("Comments");
                debug("Number of comments=" + lvUnorderedCOVector.size());
                lvCOVector = orderComments(lvUnorderedCOVector);
                liCommentSize = lvCOVector.size();
                debug("Number of comments after ordering=" + liCommentSize);
                loAI = this.getAttsImpl(loProps, foHeader);
                foDW.startElement("",msLevel,"",loAI);

                for (liCommentIndex = 0 ;liCommentIndex < liCommentSize; liCommentIndex++)
                {
                    loComment = (BaseObject)lvCOVector.get(liCommentIndex);
                    renderComment(loComment, foDW);
                }
                foDW.endElement(COMMENTS);
                // done comments
            }
            
            setLevel(ATTACHMENTS);
            loProps = moContainer.getAtts(msLevel);
            if (loProps != null)      // this level exists
            {
                lvAttachVector = (java.util.List)foHeader.getDottedFieldValue("Attachments");
                liAttachSize = lvAttachVector.size();
                // do attachments
                loAI = this.getAttsImpl(loProps, foHeader);
                foDW.startElement("",msLevel,"",loAI);
                debug("Number of attachments =" + liAttachSize);
                for (liAttachIndex = 0 ; liAttachIndex < liAttachSize; liAttachIndex++)
                {
                    loAttach = (BaseObject)lvAttachVector.get(liAttachIndex);
                    renderAttachment(loAttach, foDW);
                }

                foDW.endElement(ATTACHMENTS);
                // done attachments
            }            
            
            setLevel(APPROVALFLOWS);
            loProps = moContainer.getAtts(msLevel);
            if (loProps != null)       // comments level exists
            {
                debug("Approvalflows exist ... ");
                // do approval flows
                if(foHeader instanceof Requisition)
                {
                	Requisition loReq = (Requisition) foHeader;
                	//lvApprovalRequestsVector = (java.util.List)loReq.getApprovalRequests();
                	lvApprovalRequestsVector = (java.util.List)loReq.getFieldValue("ApprovalRequests");
                	lvecApprovalRequests = ListUtil.cloneList(lvApprovalRequestsVector);
                    for (int i = 0; i < lvecApprovalRequests.size(); i ++)
                    {
                    	loApprovalRequest = (ApprovalRequest) lvecApprovalRequests.get(i);
                    	lvApprovalRequestsVector = getDependencies(lvecApprovalRequests, loApprovalRequest);
                    }

                	debug("Number of approval requests =" + lvecApprovalRequests.size());	
                    loAI = this.getAttsImpl(loProps, foHeader);
                    foDW.startElement("",msLevel,"",loAI);
                    for (int i = 0; i < lvecApprovalRequests.size(); i++)
                    {
                    	loApprovalRequest = (BaseObject)lvecApprovalRequests.get(i);
                    	renderApprovalRequest(loApprovalRequest, foDW);
                    }
                }
                foDW.endElement(APPROVALFLOWS);
                // done approval requests
            }

            setLevel(HISTORYS);
            loProps = moContainer.getAtts(msLevel);
            if (loProps != null)      // this level exists
            {
            	lvRecordsVector = (java.util.List)foHeader.getDottedFieldValue("Records");
            	liRecordSize = lvRecordsVector.size();
                // do records
                loAI = this.getAttsImpl(loProps, foHeader);
                foDW.startElement("",msLevel,"",loAI);
                debug("Number of records =" + liRecordSize);
                for (liRecordIndex = 0 ; liRecordIndex < liRecordSize; liRecordIndex++)
                {
                    loRecord = (BaseObject)lvRecordsVector.get(liRecordIndex);
                    renderRecord(loRecord, foDW);
                }
                foDW.endElement(HISTORYS);
                // done records
            }            
                // done record requests
            
            foDW.endElement(HEADER);
        }
        catch(Exception loEx)
        {
            loEx.printStackTrace();
            throw new BuyintException("Error while rendering Header",BuyintException.EXCPT_XMLGEN_ERROR);
        }
        debug("Leaving renderHeader ... ");
    }
    
    private void renderRecord(BaseObject foApprovalFlow, DataWriter foDW)
    throws BuyintException
    {
	    debug("Entered render record ... ");
   	    AttributesImpl loAI;
	    Properties loProps;

	    try
	    {
	        setLevel(HISTORY);
	        loProps = moContainer.getAtts(msLevel);
	        loAI = this.getAttsImpl(loProps, foApprovalFlow);
	        foDW.startElement("",msLevel,"",loAI);
	        renderFields(foApprovalFlow, foDW);
	        foDW.endElement(HISTORY);
	        // done Approval Flow
	    }
	    catch(Exception loEx)
	    {
	        loEx.printStackTrace();
	        throw new BuyintException("Error while rendering record",BuyintException.EXCPT_XMLGEN_ERROR);
	    }
	    debug("Leaving render record ... ");
	}

    public String getSummary(BaseObject foRecord)
    {
    	debug("Entered getSummary ... ");
    	if(foRecord instanceof Record && ((Record)foRecord).getTextSummary() != null)
    	{
    		return ((Record)foRecord).getTextSummary();
    	}
    	else 
    	{
    		return null;
    	}
    }
    
    public ariba.user.core.User setRecordUser(BaseObject foRecord)
    {
        debug("Entered setRecordUser with object=" + foRecord);
    	if(foRecord instanceof Record && ((Record)foRecord).getUser() != null)
    	{
    		return ((Record)foRecord).getUser();
    	}
    	else 
    	{
    		return null;
    	}
    }

    public List getDependencies(List lvec, BaseObject appReq)
    {
        List vecNewDependencies = ListUtil.list();
        ApprovalRequest newDependency = new ApprovalRequest();
        vecNewDependencies = (List)appReq.getFieldValue("Dependencies");
        int size = vecNewDependencies.size();

        for (int i = 0; i < size; i ++)
        {
            newDependency = (ApprovalRequest) vecNewDependencies.get(i);
            if (lvec.contains(newDependency))
            {
                continue;
            }
            else
            {
                lvec.add(newDependency);
                getDependencies(lvec, newDependency);
            }
        }
        return lvec;
    }

    
    private void renderApprovalRequest(BaseObject foApprovalFlow, DataWriter foDW)
    throws BuyintException
    {
	    debug("Entered renderApprovalRequest ... ");
   	    AttributesImpl loAI;
	    Properties loProps;

	    try
	    {
	        setLevel(APPROVALFLOW);
	        loProps = moContainer.getAtts(msLevel);
	        loAI = this.getAttsImpl(loProps, foApprovalFlow);
	        foDW.startElement("",msLevel,"",loAI);
	        renderFields(foApprovalFlow, foDW);
	        foDW.endElement(APPROVALFLOW);
	        // done Approval Flow
	    }
	    catch(Exception loEx)
	    {
	        loEx.printStackTrace();
	        throw new BuyintException("Error while rendering Approval Flow",BuyintException.EXCPT_XMLGEN_ERROR);
	    }
	    debug("Leaving render Approval Flow ... ");
	}

    public String getAttachmentLevel(BaseObject foAttachmentWrapper)
    {
    	debug("Entered getAttachmentLevel ... ");
    	return String.valueOf(getAttachmentLevelInt(foAttachmentWrapper));
    }

    public int getAttachmentLevelInt(BaseObject foAttachmentWrapper)
    {
   		if(((AttachmentWrapper)foAttachmentWrapper).getFirstLineItem() == null)
   		{
   			return 0;
   		}
   		else
   		{
   			return ((AttachmentWrapper)foAttachmentWrapper).getFirstLineItem().getNumberInCollection();
   		}
    }
    
    public String getApprover(BaseObject foApprovalRequest)
    {
    	debug("Entered getApprover ... ");
    	String lsApprover = null;
    	if(foApprovalRequest instanceof ApprovalRequest)
    	{
    		lsApprover = getApproverName(((ApprovalRequest)foApprovalRequest).getApprover(), (ApprovalRequest)foApprovalRequest);
    	}
    	return lsApprover;
    }
    
    public String getApprovedBy(BaseObject foApprovalRequest)
    {
    	debug("Entered getApprovedBy ... ");
    	String lsApprover = null;
    	if(foApprovalRequest instanceof ApprovalRequest)
    	{
    		lsApprover = getApproverName(((ApprovalRequest)foApprovalRequest).getApprovedBy(), (ApprovalRequest)foApprovalRequest);
    	}
    	return lsApprover;
    }

    public String getApprovalRequired(BaseObject foApprovalRequest)
    {
    	debug("Entered getApprovalRequired ... ");
    	String lsApprovalRequired = null;
    	if(foApprovalRequest instanceof ApprovalRequest)
    	{
    		lsApprovalRequired = ((ApprovalRequest)foApprovalRequest).getDerivedApprovalRequired();
    	}
    	return lsApprovalRequired;
    }

    public String getApprovalState(BaseObject foApprovalRequest)
    {
    	debug("Entered getApprovalState ... ");
    	String lsApprovedState = null;
    	if(foApprovalRequest instanceof ApprovalRequest)
    	{
    		lsApprovedState = ((ApprovalRequest)foApprovalRequest).getDerivedApprovalState();
    	}
    	return lsApprovedState;
    }

    public String getApproverName(Approver foApprover, ApprovalRequest foApprovalRequest)
    {
    	String lsApproverName = null;
    	if(foApprover != null)
    	{
    		if(foApprover instanceof Role)
    		{
    			lsApproverName = (String)((Role)foApprover).getName().getPrimaryString();
    		}
    		else if(foApprover instanceof Group)
    		{
    			lsApproverName = (String)((Group)foApprover).getName().getPrimaryString();
    		}
    		else if(foApprover instanceof ariba.user.core.User)
    		{
    			lsApproverName = (String)((ariba.user.core.User)foApprover).getName().getPrimaryString();
    		}
    		else if(foApprover instanceof ariba.approvable.core.ApproverList)
    		{
    			lsApproverName = (String) foApprovalRequest.getReason();
    		}
    		else if(foApprover instanceof ariba.approvable.core.CustomApprover)
    		{
    			lsApproverName = (String)((CustomApprover) foApprover).getUniqueName();
    			debug("Entered getApproverName::Custom ApproverName " +lsApproverName);
    			if(lsApproverName.equals("Requisition Custom Approver") || lsApproverName.equals("Collaboration Custom Approver"))
    			{
    				debug("Entered getApproverName::Do Nothing::Returning custom approver unique name");
    			}
    			else
    			{
    				debug("Entered getApproverName::Returning reason");
    				lsApproverName = (String) foApprovalRequest.getReason();
    			}
    		}
    	}
    	return lsApproverName;
    }

    private void renderLine(BaseObject foLineItem, DataWriter foDW)
        throws BuyintException
    {
        debug("Entered renderLine ... ");
        java.util.List lvAcctVector;
        AttributesImpl loAI;
        BaseObject loAcct;
        Properties loProps;
        int liAcctSize;
        int liAcctIndex;

        try
        {
            setLevel(LINEITEM);
            loProps = moContainer.getAtts(msLevel);
            loAI = this.getAttsImpl(loProps, foLineItem);
            foDW.startElement("",msLevel,"",loAI);

            renderFields(foLineItem, foDW);

            setLevel(ACCOUNTINGS);
            loProps = moContainer.getAtts(msLevel);

            if (loProps != null)      // this level exists
            {
                lvAcctVector = (java.util.List)foLineItem.getDottedFieldValue("Accountings.SplitAccountings");
                liAcctSize = lvAcctVector.size();
                // do accounting
                loAI = this.getAttsImpl(loProps, foLineItem);
                foDW.startElement("",msLevel,"",loAI);

               /*
                * If we ever have to render fields inside the Accountings object,
                * we will need one more level rendered (renderAcctg?) before this point.
                * Until then, "current assumption" below holds.
                */

                setLevel(SPLITACCOUNTINGS);
                loProps = moContainer.getAtts(msLevel);
                /* splitaccountings always exists (current assumption) */
                loAI = this.getAttsImpl(loProps, foLineItem);
                foDW.startElement("",msLevel,"",loAI);

                for (liAcctIndex = 0 ; liAcctIndex < liAcctSize; liAcctIndex++)
                {
                    loAcct = (BaseObject)lvAcctVector.get(liAcctIndex);
                    renderSplitAcctg(loAcct, foDW);
                }
                foDW.endElement(SPLITACCOUNTINGS);
                foDW.endElement(ACCOUNTINGS);
                // done accounting
            }

            foDW.endElement(LINEITEM);
        }
        catch(Exception loEx)
        {
            loEx.printStackTrace();
            throw new BuyintException("Error while rendering LineItem",BuyintException.EXCPT_XMLGEN_ERROR);
        }
        debug("Leaving renderLine ... ");
    }

    private void renderSplitAcctg(BaseObject foAcctg, DataWriter foDW)
        throws BuyintException
    {
        debug("Entered renderSplitAcctg ... ");
        AttributesImpl loAI = null;
        Properties loProps = null;
        try
        {
            setLevel(SPLITACCOUNTING);
            loProps = moContainer.getAtts(msLevel);
            loAI = this.getAttsImpl(loProps, foAcctg);
            foDW.startElement("",msLevel,"",loAI);

            renderFields(foAcctg, foDW);

            foDW.endElement(SPLITACCOUNTING);
        }
        catch(Exception loEx)
        {
            loEx.printStackTrace();
            throw new BuyintException("Error while rendering SplitAccounting",BuyintException.EXCPT_XMLGEN_ERROR);
        }
        debug("Leaving renderSplitAcctg ... ");
    }

    private java.util.List orderComments(java.util.List fvComments)
    {
        java.util.List lvOrderedVector = ListUtil.list();
        java.util.List lvSortingVector = ListUtil.list();
        String lsPos = null;
        int liPos = 0;
        int liLine = 0;
        for (liPos = 0 ; liPos < fvComments.size(); liPos++)
        {
            ariba.approvable.core.Comment loComment = (ariba.approvable.core.Comment)fvComments.get(liPos);
                liLine = getCommentLevelInt(loComment);
                // string together comment level times 1000 and position in vector to get a sortable string
                lvSortingVector.add(String.valueOf(1000*(liLine + 1) + liPos));
            }

        // sort in ascending order of comment level and arrival sequence
        ListUtil.sortStrings(lvSortingVector,true);

        for (liLine = 0 ; liLine < lvSortingVector.size(); liLine++)
        {
            // get back the position
            lsPos = (String)lvSortingVector.get(liLine);
            lsPos = lsPos.substring(lsPos.length() - 3);    // strips out all but 3 digits
            try
            {
                liPos = Integer.parseInt(lsPos);
            }
            catch (NumberFormatException loEx)
            {
                debug("Being here is theoretically impossible ... ");
                liPos = 0;
            }
            ariba.approvable.core.Comment loComment = (ariba.approvable.core.Comment)fvComments.get(liPos);
            lvOrderedVector.add(loComment);
        }
        return lvOrderedVector;
    }

    private void renderComment(BaseObject foComment, DataWriter foDW)
        throws BuyintException
    {
        debug("Entered renderComment ... ");
        java.util.List lvAttachVector;
        AttributesImpl loAI;
        BaseObject loAttach;
        Properties loProps;
        int liAttachSize;
        int liAttachIndex;

        try
        {
            setLevel(COMMENT);
            loProps = moContainer.getAtts(msLevel);
            loAI = this.getAttsImpl(loProps, foComment);
            foDW.startElement("",msLevel,"",loAI);
            renderFields(foComment, foDW);
            foDW.endElement(COMMENT);
            // done comments
        }
        catch(Exception loEx)
        {
            loEx.printStackTrace();
            throw new BuyintException("Error while rendering Comment",BuyintException.EXCPT_XMLGEN_ERROR);
        }
        debug("Leaving renderComment ... ");
    }

    private void renderAttachment(BaseObject foAttach, DataWriter foDW)
        throws BuyintException
    {
        debug("Entered renderAttachment ... ");
        AttributesImpl loAI = null;
        Properties loProps = null;
        try
        {
            setLevel(ATTACHMENT);
            loProps = moContainer.getAtts(msLevel);
            loAI = this.getAttsImpl(loProps, foAttach);
            foDW.startElement("",msLevel,"",loAI);

            renderFields(foAttach, foDW);

            foDW.endElement(ATTACHMENT);
        }
        catch(Exception loEx)
        {
            loEx.printStackTrace();
            throw new BuyintException("Error while rendering Attachment",BuyintException.EXCPT_XMLGEN_ERROR);
        }
        debug("Leaving renderAttachment ... ");
    }

    private void renderFields(BaseObject foBase, DataWriter foDW)
        throws BuyintException
    {
        debug("Entered renderFields for " + msLevel + " level ... ");
        java.util.List lvFieldAtts = moContainer.getFieldAtts(msLevel);
        String lsFldName = null;
        String lsMethod = null;
        String lsObjectName = null;
        String lsFldLabel = null;
        int liFldLabelIndex = 0;
        String lsFldValDesc = null;
        int liFldValDescIndex = 0;
        String lsFldValue = null;
        String lsFldType = null;
        AttributesImpl loAI = null;
        AttributesImpl loAITemp = null;
        java.util.List loAIVector;
        BaseObject loBase = null;

        try
        {
            // set up field labels ... save in hash table for current level
            if (moAIHash.containsKey(msLevel))
            {
                debug("Already have field attributes List");
                loAIVector = (java.util.List)moAIHash.get(msLevel);
            }
            else
            {
                debug("About to build field attributes List and set field labels");
                loAIVector = loadFieldAttsImpl(lvFieldAtts);
                for (int lix = 0; lix < loAIVector.size(); lix++)
                {
                    loBase = foBase;
                    loAI = (AttributesImpl)loAIVector.get(lix);
                    // get the ariba field name
                    lsFldName = loAI.getValue("", ARIBA_FIELD_NAME);
                    // get the ariba field type ... default is String
                    lsFldType = loAI.getValue("", ARIBA_TYPE);
                    debug("renderFields field_name=" + lsFldName + ", field_type=" + lsFldType);
                    if (lsFldType == null)
                    {
                       lsFldType = "java.lang.String";
                    }
                    // get any value specified in the data area ... this is stored in the TEXT_DATA attribute
                    // when loading XML in BuyintXMLContainer.
                    
                    lsFldValue = loAI.getValue("", TEXT_DATA);
                    if (lsFldValue != null)
                    {
                        debug("renderFields TEXT_DATA contents=" + lsFldValue);
                        if (lsFldValue.startsWith(METHOD_INDICATOR))
                        {
                            // no relevance to label processing ... must hardcode label or have valid reference
                        }
                        else if (lsFldValue.startsWith(SET_OBJECT))
                        {
                            // SET_OBJECT saves object reference for subsequent retrieval by GET_OBJECT.
                            // Custom method identified by method name loads object identified by field name.
                            // Just like METHOD_INDICATOR, java reflection is used to run appropriate load method.
                            debug("It is a SET ... will save the object for reference");
                            lsObjectName = lsFldName;
                            setReference(lsObjectName, loBase);
                            lsFldName = null;
                        }
                        else if (lsFldValue.startsWith(GET_OBJECT))
                        {
                            // GET_OBJECT switches reference from base object to internally maintained object
                            // Object name is 1st part of fld name, rest is dotted field notation for field content
                            debug("It is a GET ... will use referenced object instead of base object");
                            lsObjectName = lsFldName.substring(0, lsFldName.indexOf('.'));
                            lsFldName = lsFldName.substring(lsFldName.indexOf('.') + 1);
                            loBase = getReference(lsObjectName);
                            debug("Retrieved this object, name=" + lsObjectName + ", object=" + loBase);
                        }
                    }
                    if (lsFldName == null || lsFldName.trim().length() == 0 || lsFldName.equalsIgnoreCase("NULL"))
                    {
                        // ignore label retrieval
                        debug("Field name is null or empty ... ignore label retrieval");
                    }
                    else if(loBase!=null)
                    {
                        // get the label if necessary
                        liFldLabelIndex = loAI.getIndex("",FIELD_LABEL);
                        if (liFldLabelIndex == -1)
                        {
                           // get label for field ... no label provided in template
                           debug("About to call getLabel for " + lsFldName + " ... no label provided in template");
                           lsFldLabel = getLabel(loBase, lsFldName);
                        }
                        else
                        {
                           debug("Field label provided ... will delete and re-add");
                           lsFldLabel = loAI.getValue("", FIELD_LABEL);
                           // remove for later readd so field ordering remains same on output xml
                           loAI.removeAttribute(liFldLabelIndex);
                        }
                        // add the field label attribute ... always
                        debug("About to add field label attribute, value is " + lsFldLabel);
                        loAI.addAttribute("",FIELD_LABEL,"","",lsFldLabel);

                        // get the field BFDT description if necessary
                        liFldValDescIndex = loAI.getIndex("",FIELD_VALUE_DESC);
                        if (liFldValDescIndex == -1)
                        {
                           // get BuysenseFieldDataTable value description for field ... no overrride provided in template
                           debug("About to call getValueDesc for " + lsFldName + " ... no override in template");
                           lsFldValDesc = getValueDesc(loBase, lsFldName);
                        }
                        else
                        {
                           debug("Field value description provided ... will delete and re-add");
                           lsFldValDesc = loAI.getValue("", FIELD_VALUE_DESC);
                           // remove for later readd so field ordering remains same on output xml
                           loAI.removeAttribute(liFldValDescIndex);
                        }
                        // add the field value desc attribute ... relevant if BSO fld or explicitly specified
                        if (lsFldValDesc != null && lsFldValDesc.trim().length() > 0)
                        {
                           debug("About to add field value index attribute, value is " + lsFldValDesc);
                           loAI.addAttribute("",FIELD_VALUE_DESC,"","",lsFldValDesc);
                        }
                    }
                    else
                    {
                    	debug(" set object is null");
                    }
                }
                // Save the initial image of populated attributes vector which includes labels.
                // This allows reuse over multiple occurrences of current level eg. multiple lines, comments, etc..
                moAIHash.put(msLevel, loAIVector);
            }

            // now set up field data and put out
            debug("Labels are all fine ... about to print data now ... ");
            for (int lix = 0; lix < loAIVector.size(); lix++)
            {
                loBase = foBase;
                loAITemp = (AttributesImpl)loAIVector.get(lix);
                loAI = new AttributesImpl(loAITemp);
                // get the ariba field name
                lsFldName = loAI.getValue("", ARIBA_FIELD_NAME);
                // get the ariba field type ... default is String
                lsFldType = loAI.getValue("", ARIBA_TYPE);
                if (lsFldType == null)
                {
                   lsFldType = "java.lang.String";
                }
                // get any value specified in the data area ... this is stored in the TEXT_DATA attribute
                // when loading XML in BuyintXMLContainer.
                lsFldValue = loAI.getValue("", TEXT_DATA);

                if (lsFldValue != null)
                {
                    // interpret method call
                	debug("renderFields value processing field_name=" + lsFldName + ", field_type=" + lsFldType + ", lsFldValue=" +lsFldValue);
                	if (lsFldValue.startsWith(METHOD_INDICATOR))
                    {
                        // Presence of METHOD_INDICATOR implies field requires custom processing
                        // and would not be derivable from dotted field notation.
                        // Field value is just returned by custom method (invoked by interpret).
                        // Also field_type attribute takes on special meaning herein
                        lsMethod = lsFldValue.substring(METHOD_INDICATOR.length());
                        lsFldValue = interpret(lsMethod, loBase);
                    }
                    else if (lsFldValue.startsWith(SET_OBJECT))
                    {
                        lsObjectName = lsFldName;
                        setReference(lsObjectName, loBase);
                        lsFldName = null;                      // force ignore ... SET fields not intended for output
                    }
                    else if (lsFldValue.startsWith(GET_OBJECT))
                    {
                        lsObjectName = lsFldName.substring(0,lsFldName.indexOf('.'));
                        lsFldName = lsFldName.substring(lsFldName.indexOf('.') + 1);
                        loBase = getReference(lsObjectName);
                        lsFldValue = null;                     // force GET of field based on prior SET
                    }
                    loAI.removeAttribute(loAI.getIndex("",TEXT_DATA));
                }
                // CSPL-761 if field value is SKIP_LINE, then skip the entire field in XML output
                if(lsFldValue != null && lsFldValue.indexOf(SKIP_LINE) >= 0)
                {
                	debug("field_name=" + lsFldName + ", field_type=" + lsFldType + ", field_value=" + lsFldValue);
                	lsFldName = null;
                }
                //Commented by Srini add METHOD_INDICATOR fields too
                /*else
                {
                    lsFldValue = null;
                }*/

                if (lsFldName == null)
                {
                    debug("Field name set to null ... will ignore");
                }
                else
                {
                    // use the ariba field name to get the value from the object model
                    debug("Field name=" + lsFldName + ",type=" + lsFldType);
                    //SRINI::START
                    boolean lbVisibleOnReq = FieldLabelGetter.getFieldVisibility(msClientNameUniqueName, lsFldName, foBase, "Req");
                    debug("Visible for Web Services " +lbVisibleOnReq);
                    //SRINI::END
                	if((lsFldValue != null) && (lsFldValue.trim().length() > 0) && lbVisibleOnReq)
                    {
                        debug("First if about to write data element with field value=" + lsFldValue);
                        foDW.dataElement("",FIELD,"",loAI,lsFldValue);
                    }
                    else if (lsFldValue == null && loBase!=null)
                    {
                        lsFldValue = getFieldValue(loBase, lsFldName, lsFldType);           // ***** this is where field is got
                        if ((lsFldValue != null) && (lsFldValue.trim().length() > 0) && lbVisibleOnReq)
                        {
                            debug("About to write data element with field value=" + lsFldValue);
                            foDW.dataElement("",FIELD,"",loAI,lsFldValue);
                        }
                        else
                        {
                            debug("Field " + lsFldName + " has no value ... skipping");
                        }
                    }
                    else
                    {
                    	debug(" set object is null");
                    }
                }
            }
        }
        catch(Exception loEx)
        {
            loEx.printStackTrace();
            throw new BuyintException("Error while rendering fields at " + msLevel + " level",BuyintException.EXCPT_XMLGEN_ERROR);
        }
        debug("Leaving renderFields for " + msLevel + "level ... ");
    }

    private void setLevel(String fsLevel)
    {
        msLevel = fsLevel;
    }

    private String interpret(String fsMethodName, BaseObject foBase)
        throws Exception
    {
        debug("Entered interpret with method name=" + fsMethodName);
        if (moMethodParmTypes == null)
        {
           moMethodParmTypes = new Class[1];
           moMethodParmTypes[0] = Class.forName("ariba.base.core.BaseObject");
        }

        BaseObject [] loObjArray = {foBase};
        Method loMethod = this.getClass().getMethod(fsMethodName, moMethodParmTypes);
        String lsValue = (String)loMethod.invoke(this,loObjArray);
        debug("Leaving interpret with value=" + lsValue);
        return lsValue;
    }

    private void setReference(String fsObjectName, BaseObject foBase)
        throws Exception
    {
        debug("Entered setReference with object name=" + fsObjectName + ", base object=" + foBase);

        String lsSaveName = fsObjectName;
        BaseObject loSaveObject = null;

        if (moSavedObjects == null)
        {
            moSavedObjects = new java.util.HashMap(5);
        }
        if (moSavedObjects.containsKey(lsSaveName))
        {
            debug("Object " + lsSaveName + "exists in SavedObjects Map ... will resave value");
        }
        else
        {
            debug("Object " + lsSaveName + "does not exist in SavedObjects Map ... will save value");
        }

        if (moMethodParmTypes == null)
        {
           moMethodParmTypes = new Class[1];
           moMethodParmTypes[0] = Class.forName("ariba.base.core.BaseObject");
        }

        BaseObject [] loObjArray = {foBase};
        // Prefix "set" to @@SET@@ object reference to obtain method name
        String lsMethodName = "set" + fsObjectName;
        Method loMethod = this.getClass().getMethod(lsMethodName, moMethodParmTypes);

        loSaveObject = (BaseObject)loMethod.invoke(this,loObjArray);
        debug("About to save name=" + lsSaveName + ", object=" + loSaveObject);
        moSavedObjects.put(lsSaveName, loSaveObject);
    }

    public BaseObject getReference(String fsObjectName)
    {
        return (BaseObject)moSavedObjects.get(fsObjectName);
    }
    public String getPCardAlias(BaseObject foObj)
    {
        boolean lbUsePCard = ((Boolean)foObj.getFieldValue("UsePCardBool")).booleanValue();
        moPCardToUse = (ariba.common.core.PCard)foObj.getDottedFieldValue(PCARD_TO_USE);

        if(moPCardToUse != null && lbUsePCard)
        {
            msPCardAlias = (String)moPCardToUse.getDottedFieldValue("Alias");
        }
        else
        {
            msPCardAlias = SKIP_LINE;
        }
        return msPCardAlias;
    }

    public String getClientID(BaseObject foObj)
    {
        return msClientID;
    }

    public String getClientName(BaseObject foObj)
    {
        return msClientName;
    }

    public String getClientNameUniqueName(BaseObject foObj)
    {
        return msClientNameUniqueName;
    }

    public String getDocumentType(BaseObject foObj)
    {
        return msDocumentType;
    }

    public String getDocumentID(BaseObject foObj)
    {
        return msDocumentID;
    }

    public String getRequester(BaseObject foObj)
    {
        return msRequesterName;
    }

    public ariba.common.core.User setRequester(BaseObject foBase)
    {
        debug("Entered setRequester with object=" + foBase);
        ariba.user.core.User loUser = null;
        ariba.common.core.User loPartUser = null;

        // Set the Requester/Preparer based on object type
        if (moAribaDoc.instanceOf("ariba.purchasing.core.PurchaseOrder"))
        {
           loUser = (ariba.user.core.User)moAribaDoc.getDottedFieldValue(PO_REQUESTER);
           loPartUser = ariba.common.core.User.getPartitionedUser(loUser,moAribaDoc.getPartition());
        }
        else
        {
           loUser = (ariba.user.core.User)moAribaDoc.getDottedFieldValue(REQ_REQUESTER);
           loPartUser = ariba.common.core.User.getPartitionedUser(loUser,moAribaDoc.getPartition());
        }

        return loPartUser;
    }

    public ariba.common.core.User setPreparer(BaseObject foBase)
    {
        debug("Entered setPreparer with object=" + foBase);
        ariba.user.core.User loUser = null;
        ariba.common.core.User loPartUser = null;

        // Set the Requester/Preparer based on object type
        if (moAribaDoc.instanceOf("ariba.purchasing.core.PurchaseOrder"))
        {
           loUser = (ariba.user.core.User)moAribaDoc.getDottedFieldValue(PO_PREPARER);
           loPartUser = ariba.common.core.User.getPartitionedUser(loUser,moAribaDoc.getPartition());
        }
        else
        {
           loUser = (ariba.user.core.User)moAribaDoc.getDottedFieldValue(REQ_PREPARER);
           loPartUser = ariba.common.core.User.getPartitionedUser(loUser,moAribaDoc.getPartition());
        }

        return loPartUser;
    }

    public ariba.common.core.User setCommentUser(BaseObject foBase)
    {
        debug("Entered setCommentUser with object=" + foBase);
        ariba.user.core.User loUser = null;
        ariba.common.core.User loPartUser = null;

        loUser = (ariba.user.core.User)foBase.getDottedFieldValue(USER);
        loPartUser = ariba.common.core.User.getPartitionedUser(loUser,moAribaDoc.getPartition());

        return loPartUser;
    }


    /** This method set PCard reference in saved object list
     *
     * @param foBase
     * @return
     */
    public ariba.common.core.PCard setPCard(BaseObject foBase)
    {
    	return (ariba.common.core.PCard)foBase.getDottedFieldValue(PCARD);
    }

    public String getPreparer(BaseObject foObj)
    {
        return msPreparerName;
    }

    public  ariba.common.core.User getPartitionedRequester(BaseObject foObj)
    {
        return moRequester;
    }

    public  ariba.common.core.User getPartitionedPreparer(BaseObject foObj)
    {
        return moPreparer;
    }

    public String getCommentLevel(BaseObject foObj)
    {
        return String.valueOf(getCommentLevelInt(foObj));
    }
    public int getCommentLevelInt(BaseObject foObj)
    {
        // Incoming baseObject must be a Comment element
        ariba.approvable.core.Comment foComment = (ariba.approvable.core.Comment)foObj;
        if (foComment.getDottedFieldValue("LineItem") == null)
        {
            return 0;
        }
        else
        {
            return ((Integer)foComment.getDottedFieldValue("LineItem.NumberInCollection")).intValue();
        }
    }

    private String getLabel(BaseObject foBase, String fsAribaName)
    {
        String lsLabel = null;
        if (moLabels.containsKey(fsAribaName))
        {
            lsLabel = moLabels.getProperty(fsAribaName);
        }
        else
        {
            lsLabel = FieldLabelGetter.getLabel(foBase, msClientNameUniqueName, fsAribaName);
            if (lsLabel != null)
            {
                moLabels.setProperty(fsAribaName, lsLabel);
            }
        }
        return lsLabel;
    }

    private String getValueDesc(BaseObject foBase, String fsAribaName)
    {
       // not all fields have value descriptions ... hence no need for hashtable logic
       return FieldLabelGetter.getValueDescription(foBase, fsAribaName);
    }

    private AttributesImpl loadAttsImpl(Properties foProps)
    {
        debug("Entered loadAttsImpl ...");
        AttributesImpl loAI = new AttributesImpl();
        if (foProps.size() == 0)
        {
            return loAI;
        }

        Enumeration loEE = foProps.propertyNames();
        String lsKey = null;
        String lsValue = null;

        for (loEE = foProps.propertyNames(); loEE.hasMoreElements() ;)
        {
            lsKey = loEE.nextElement().toString();
            lsValue = foProps.getProperty(lsKey);
            if (lsValue != null)
            {
                loAI.addAttribute("",lsKey,"","",lsValue);
            }
        }
        debug("Leaving loadAttsImpl ...");
        return loAI;
    }

    private java.util.List loadFieldAttsImpl(java.util.List fvFldAtts)
    {
        debug("Entered loadFieldAttsImpl method ... ");
        java.util.List lvAll = ListUtil.list();
        Properties loProps;
        for (int lix = 0; lix < fvFldAtts.size(); lix++)
        {
            loProps = (Properties) fvFldAtts.get(lix);
            lvAll.add(loadAttsImpl(loProps));
        }
        debug("Leaving loadFieldAttsImpl method ... ");
        return lvAll;
    }

    private AttributesImpl getAttsImpl(Properties foProps, BaseObject foBase)
        throws Exception
    {
        debug("Entered getAttsImpl ...");
        if (foProps == null || foProps.size() == 0)
        {
            return EMPTY_ATTS;
        }

        AttributesImpl loAI = new AttributesImpl();
        Enumeration loEE = foProps.propertyNames();
        String lsKey = null;
        String lsValue = null;
        String lsMethod = null;

        for (loEE = foProps.propertyNames(); loEE.hasMoreElements() ;)
        {
            lsKey = loEE.nextElement().toString();
            lsValue = foProps.getProperty(lsKey);
            debug("lsKey=" + lsKey + ", lsValue=" + lsValue);
            if (lsValue != null)
            {
                // interpret method call
                if (lsValue.startsWith(METHOD_INDICATOR))
                {
                    lsMethod = lsValue.substring(METHOD_INDICATOR.length());
                    lsValue = interpret(lsMethod, foBase);
                }
                debug(" loading lsKey=" + lsKey + ", lsValue=" + lsValue);
                loAI.addAttribute("",lsKey,"","",lsValue);
            }
        }
        debug("Leaving getAttsImpl ...");
        return loAI;
    }

    public String getFieldValue(BaseObject foBase, String fsFldName, String fsFldType)
    {
        String lsResult = null;
        String lsSaveName = msLevel + "_" + fsFldName + "_" + fsFldType;
        Object loValue = null;
        if (moSavedObjects.containsKey(lsSaveName))
        {
            // if the reference object matches a saved Object from an earlier "set" method ...
            // ... use that object
            loValue = ((BaseObject)moSavedObjects.get(lsSaveName)).getDottedFieldValue(fsFldName);
        }
        else
        {
            loValue = foBase.getDottedFieldValue(fsFldName);
        }

        if (loValue != null)
        {
            if (fsFldType.equals("java.lang.String"))
            {
                lsResult = (String)BuyintXWalk.XWalk.changeNewToOld((String)loValue);
                lsResult = wrapCDATA(lsResult);
            }
            else if (fsFldType.equals("java.lang.Boolean"))
            {
                lsResult = ((Boolean)loValue).toString();
            }
            else if (fsFldType.equals("java.lang.Integer"))
            {
                lsResult = ((Integer)loValue).toString();
            }
            else if (fsFldType.equals("ariba.basic.core.Money"))
            {
                loValue = foBase.getDottedFieldValue(fsFldName + ".Amount");
                lsResult = (((BigDecimal)loValue).setScale(5, BigDecimal.ROUND_HALF_UP)).toString();  
            }
            else if (fsFldType.equals("java.math.BigDecimal"))
            {
                lsResult = ((BigDecimal)loValue).toString();
            }
            else if (fsFldType.equals("ariba.base.core.LongString"))
            {
                lsResult = ((ariba.base.core.LongString)loValue).string();
                lsResult = wrapCDATA(lsResult);
            }
            else if (fsFldType.equals("ariba.base.core.MultiLingualString"))
            {
                lsResult = ((ariba.base.core.MultiLingualString)loValue).getPrimaryString();
                lsResult = wrapCDATA(lsResult);
            }
            else                            // default ... same as java.lang.String
            {
                lsResult = loValue.toString();
                lsResult = wrapCDATA(lsResult);
            }
        }
        return lsResult;
    }

    private static String wrapCDATA(String fsData)
    {
        if (fsData != null && fsData.trim().length() > 0)
        {
            return CDATA_PFX + fsData + CDATA_SFX;
        }
        else
        {
            return fsData;
        }
    }

    private static int getIntValue(BaseObject foBase, String fsField, int fiDefault)
    {
        int liRet = fiDefault;
        String lsFldVal;
        try
        {
            lsFldVal = getStringValue(foBase, fsField);
            if (lsFldVal != null)
            {
                liRet = Integer.parseInt(lsFldVal);
            }
        }
        catch (NumberFormatException loEx)
        {
            // liRet = fiDefault;      redundant
        }
        return liRet;
    }

    private static boolean getBooleanValue(BaseObject foBase, String fsField)
    {
        boolean lboolRet = false;
        String lsFldVal;
        lsFldVal = getStringValue(foBase, fsField);
        if (lsFldVal != null)
        {
            lboolRet = lsFldVal.equalsIgnoreCase("TRUE");
        }
        return lboolRet;
    }

    private static BigDecimal getBigDecimalValue(BaseObject foBase, String fsField, BigDecimal foDefault)
    {
        BigDecimal loBD = foDefault;
        String lsFldVal;
        try
        {
            lsFldVal = getStringValue(foBase, fsField);
            if (lsFldVal != null)
            {
                loBD = new BigDecimal(lsFldVal.trim());
            }
        }
        catch (NumberFormatException loEx)
        {
            // loBD = foDefault;      redundant
        }
        return loBD;
    }

    private static String getStringValue(BaseObject foBase, String fsField)
    {
        String lsFldVal;
        lsFldVal = (String)foBase.getDottedFieldValue(fsField);
        return lsFldVal;
    }

   /**
    * Convenience method for debug message output.
    *
    * @param  fsMsg The text of the message to be printed.
    *
    */
    public static void debug(String fsMsg)
    {
        Log.customer.debug("BuysenseWebserviceXMLWriter: " + fsMsg);     // ONLINE
    }
}
