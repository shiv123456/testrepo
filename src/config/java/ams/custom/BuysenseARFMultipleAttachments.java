package config.java.ams.custom;

import ariba.base.fields.ValueSource;
import ariba.htmlui.approvableui.fields.*;

/*
Purpose   : Custom controller of multiple attachments
*/

public class BuysenseARFMultipleAttachments extends ARFAttachments
{
	public static final String ClassName = "config.java.ams.custom.BuysenseARFMultipleAttachments";
    public String outputView(String group, ValueSource context)
    {
        return "config.java.ams.custom.BuysenseARVMultipleAttachments";
    }
}
