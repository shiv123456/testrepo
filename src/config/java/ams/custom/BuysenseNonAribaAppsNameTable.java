package config.java.ams.custom;

import ariba.base.core.aql.AQLNameTable;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.SearchTermQuery;
import ariba.base.fields.ValueSource;
import ariba.util.log.Log;

public class BuysenseNonAribaAppsNameTable extends AQLNameTable
{

    String sClassName = "BuysenseNonAribaAppsNameTable";

    public void addQueryConstraints(AQLQuery query, String field, String pattern)
    {
        Log.customer.debug(sClassName + "::addQueryConstraints query: " + query + ",field " + field + ", pattern " + pattern);
        boolean bReqtoADV = false;
        boolean bReqtoQQ = false;

        try
        {

            //based on the selected app flag (ADV or QQ) add condition to filter users who has access to that respective app
            //APPGROUP ex: 010200000000000000001100000000
            //We get this data from BUYINT<-BPORTAL.BUYSENSEUSER. As of today (05-Apr-2017), there are 17 apps in BPORTAL.
            // Quick Quote app id 08
            // Full Advantage app-id 12
            // APPGROUP id framed and set based on the app-level access as a sequence of app-id #; no-access is represented as 00
            // Suppose if a use has access to only QQ then APPGROUP would be  000000000000000800000000000000
            //Suppose if a use has access to only ADV then APPGROUP would be  000000000000000000000012000000

            ValueSource context = getValueSourceContext();
            Log.customer.debug(sClassName + " context - " + context);
            if (context instanceof ariba.purchasing.core.Requisition)
            {

                bReqtoADV = (Boolean) (context.getDottedFieldValue("ReqHeadCB3Value") != null ? context.getDottedFieldValue("ReqHeadCB3Value")
                        : false);
                bReqtoQQ = (Boolean) (context.getDottedFieldValue("ReqHeadCB5Value") != null ? context.getDottedFieldValue("ReqHeadCB5Value") : false);

            }

            if (bReqtoADV)
            {
                query.andLike("AppGroup", "%12%");
            }

            if (bReqtoQQ)
            {
                query.andLike("AppGroup", "%08%");
            }

            Log.customer.debug(sClassName + " final query - " + query);
        }
        catch (Exception e)
        {
            Log.customer.debug(sClassName + " Exception: " + e.getStackTrace());
        }

    }

    public AQLQuery buildQuery(AQLQuery query, String field, String pattern, SearchTermQuery searchTermQuery)
    {
        Log.customer.debug(sClassName + "::buildQuery query: " + query + ", field: " + field + ", pattern: " + pattern + ", searchTermQuery: "
                + searchTermQuery);

        query = super.buildQuery(query, field, pattern, searchTermQuery);

        if (pattern != null)
        {
            if (pattern.equalsIgnoreCase("*"))
            {
                addQueryConstraints(query, field, pattern);
            }
            else
            {
                query.andLike(field, pattern);
            }

        }
        Log.customer.debug(sClassName + "::buildQuery returning query: " + query);
        return query;

    }

}
