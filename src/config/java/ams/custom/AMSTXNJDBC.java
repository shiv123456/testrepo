
/**
DATE			29/10/2010
AUTHOR			SRINI
CHANGES			NEW CLASS: TIBCO is removed in 9r1 and written a new class to achieve the same functionality of
				AMSTXNPushEvent (TIBCO integration event). Each variable and method contains detailed description
				to explain its role and responsibility.
*/

package config.java.ams.custom;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseVector;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.util.log.Log;
import java.sql.ResultSet;

public class AMSTXNJDBC
{
    /**
     * SQL Related Parameters
     */
    private static Connection        moConn                        = null;
    private static PreparedStatement moPrep                        = null;
    private static java.sql.Statement moStatusPullStatement        = null;


    /**
     * BUYINT DB Connection information Parameters. These variables are used to get and store DB connection parameters
     * from P.table
     */
    private static String            msDBURL                       = null;
    private static String            msDBUser                      = null;
    private static String            msDBPassword                  = null;

    /**
     * Partition object used to get connection parameters from P.table
     */
    private static Partition         moPart                        = null;

    /**
     * Indicators to indicate success or failure of TXN (transaction) put.
     */
    public final static int          TXN_PUT_SUCCESS               = 0;
    public final static int          TXN_PUT_FAILED                = 1;

    /**
     * Indicators to indicate success or failure of TXN (transaction) get.
     */
    public final static int          TXN_GET_SUCCESS               = 0;
    public final static int          TXN_GET_FAILED                = 1;


    /**
     * Variable to store DB execute update result of Prepared Statement
     */
    int                              liResult                      = 0;

    /**
     * Class Name used to print in log statements
     */
    public final static String       ClassName                     = "config.java.ams.custom.AMSTXNJDBC";

    /**
     * Variables used for formatting DB columns and its related values
     */
    String                           msColumnsResult               = "";
    String                           msCommaSpace                  = ", ";
    boolean                          mbCommaSpaceRqrd              = false;

    /**
     * Variables used for multi purposes so declared them as global. Destroy these variables at end of each transaction
     * put as well as end of code or in case of exception.
     */
    private  BaseVector        moListElements                = null;
    private  BaseVector        moListTXNSUBDETAIL            = null;
    private  BaseId            moBaseIdObj                   = null;
    private  ClusterRoot       moCRObj                       = null;


    public static String[]                         ls_BUYSENSE_TXN_Fields           = { "UniqueName", "TIN", "Type", "ERP",
            "txntype", "ErrorCode", "ORMSUniqueName", "ErrorValue", "resultstring", "erpnumber", "ErrorColumn", "RESULTSEXTN1",
            "RESULTSEXTN2", "RESULTSEXTN3", "RESULTSEXTN4", "RESULTSEXTN5", "RESULTSEXTN6", "RESULTSEXTN7", "RESULTSEXTN8",
            "RESULTSEXTN9"};


    /** ls_Header_Fields are the list of fields used for mapping between Ariba TXN object and
     * BUYINT.TXNHEADER tables. Both the tables -- TXN header field names in Ariba and BUYINT.TXNHEADER
     * column names in BUYINT -- are same.
     */
    String[]                         ls_TXNHEADER_Fields           = { "TIN", "CLIENTNAME", "TXNTYPE", "ERPNUMBER",
            "PROCESSTYPE", "INTEGRATORSTATUS", "ORMSSTATUS", "SEVERITY", "SYNCFLAG", "RETRYNO", "SUBMITMODE",
            "ORMSUNIQUENAME", "TRANSACTIONDATA"                   };

    /**
     * ls_TXNHEADER_UPD_Fields are the fields used to pushed dummy final transaction from ariba to BUYINT.TXNHEADER.
     * Only four fields are required to push for dummy transaction so created separate String array and don't want to
     * mess with original HEADER fields
     */
    String[]                         ls_TXNHEADER_UPD_Fields       = { "TIN", "TXNTYPE", "INTEGRATORSTATUS",
            "TRANSACTIONDATA"                                     };

    /** ls_TXNDETAIL_Ariba_Fields and  ls_TXNDETAIL_DB_Fields are the list of fields used for mapping between Ariba
     * TXNDetail object and BUYINT.TXNDETAiL tables. InOrder to reduce the lengthy code and execution, created
     * two set of String arrays. ls_TXNDETAIL_DB_Fields is used to create insert query and ls_TXNDETAIL_Ariba_Fields
     * is used to retrieve and create values for DataBase column input. Even though both the tables -- TXNDetail field
     * names in Ariba and BUYINT.TXNDETAiL column names in BUYINT -- are same, created two different set of arrays
     * to make insert and value retrieval more flexible. For example INTR_UNID and UNID have same field and column names
     * in ariba (UNID) and in DB (UNID) but suffixed with INTR_ for ariba UNID to identify it as Integer field.
     * Always use meaningful suffix only with 5 character length in order to not get conflict with existing methods
     * See setInsertColumns, getDBINTValue, getDBDATEValue and getDBSTRValue for more details
     */
    String[]                         ls_TXNDETAIL_Ariba_Fields     = { "TIND", "INTR_UNID", "TRANSACTIONDETAILDATA" };

    String[]                         ls_TXNDETAIL_DB_Fields        = { "TIN", "UNID", "TRANSACTIONDETAILDATA" };

    /** ls_TXNSUBDETAIL_Ariba_Fields and  ls_TXNSUBDETAIL_DB_Fields are the list of fields used for mapping between
     * Ariba TXNSUBDETAIL object and BUYINT.TXNSUBDETAIL tables. InOrder to reduce the lengthy code and execution, created
     * two set of String arrays. ls_TXNSUBDETAIL_DB_Fields is used to create insert query and ls_TXNSUBDETAIL_Ariba_Fields
     * is used to retrieve and create values for DataBase column input. Even though both the tables TXNSUBDETAIL field
     * names in Ariba and BUYINT.TXNSUBDETAIL column names in BUYINT are same, created two different set of arrays
     * to make insert and value retrieval more flexible. For example INTR_SUBLINEID and SUBLINEID have same field and
     * column names in Ariba (SUBLINEID) and DB (SUBLINEID) but suffixed with INTR_ for ariba SUBLINEID to identify
     * it as Integer field. Always use meaningful suffix only with 5 character length in order to not get conflict with
     * existing methods. See setInsertColumns, getDBINTValue, getDBDATEValue and getDBSTRValue for more details
     */
    String[]                         ls_TXNSUBDETAIL_Ariba_Fields  = { "TIND", "INTR_UNID", "INTR_SUBLINEID",
            "TRANSACTIONDETAILDATA"                               };

    String[]                         ls_TXNSUBDETAIL_DB_Fields     = { "TIN", "UNID", "SUBLINEID",
            "TRANSACTIONDETAILDATA"                               };

    /** ls_TXNATTACHMENT_Ariba_Fields and  ls_TXNATTACHMENT_DB_Fields are the list of fields used for mapping between
     * Ariba TXNATTACHMENT object and BUYINT.TXNATTACHMENT tables. InOrder to reduce the lengthy code and execution, created
     * two set of String arrays. ls_TXNATTACHMENT_DB_Fields is used to create insert query and ls_TXNATTACHMENT_Ariba_Fields
     * is used to retrieve and create values for DataBase column input. This set of tables have different field and column
     * names in ariba and DB. Examples -- LINE_ID_STR and ATTACHMENT_ID_STR. For example DATE_CREATION_DATE and CREATION_DATE
     * have same field and column names in ariba (CREATION_DATE) and DB (CREATION_DATE) but suffixed with INTR_ for ariba
     * CREATION_DATE to identify it as DATE field. Always use meaningful suffix only with 5 character length in order to not
     * get conflict with existing methods. See setInsertColumns, getDBINTValue, getDBDATEValue and getDBSTRValue for more
     * details
     */
    String[]                         ls_TXNATTACHMENT_Ariba_Fields = { "TIN", "INTR_LINE_ID_STR",
            "INTR_ATTACHMENT_ID_STR", "STATUS", "APPROVABLE_UNIQUE_NAME", "DATE_CREATION_DATE", "USER_ID",
            "STORED_FILENAME", "FILENAME", "INTR_PROPRIETARY_AND_CONFIDENTIAL" };

    String[]                         ls_TXNATTACHMENT_DB_Fields    = { "TIN", "LINE_ID", "ATTACHMENT_ID", "STATUS",
            "APPROVABLE_UNIQUE_NAME", "CREATION_DATE", "USER_ID", "STORED_FILENAME", "FILENAME",
            "PROPRIETARY_AND_CONFIDENTIAL"                        };

    /**
     * This is the main method get called from AMSTXN.java using amsTXNJDBC.txnPut(TXN) using ClusterRoot object
     * (ariba.core.Buysense.TXN) as input parameter. This method first writes TXN ariba object to BUYINT.TXNHEADER then
     * checks if TXN has TXNDETAIL, TXNSUBDETAIL and TXNATTACHMENT, if there (List size is more than 1) then writes
     * writes them to respective BUYINT tables such as BUYINT.TXNDETAIL, BUYINT.TXNSUBDETAIL and BUYINT.TXNATTACHMENT.
     * @param txn - ClusterRoot object (ariba.core.Buysense.TXN) from AMSTXN.java
     * @return TXN_PUT_SUCCESS or  TXN_PUT_FAILED - Returns whether transaction is successfully written to BUYINT DB
     * @throws BuyintException
     */

    public int txnPut(ClusterRoot txn) throws BuyintException
    {
        try
        {
            getDBConnection();

            putTXNHEADER(txn);

            moListElements = (BaseVector) txn.getFieldValue("TXNDETAIL");
            Log.customer.debug(" %s :: txnPut :: TXNDETAIL.size() %s", ClassName, moListElements.size());
            if (moListElements.size() > 0)
            {
                Log.customer.debug(" %s :: txnPut :: putTXNDETAILS called ", ClassName);
                putTXNDETAILS(moListElements);
            }

            /*
             * Destroy moListElements global variable before assigning to another vector\list
             */
            moListElements = null;

            moListElements = (BaseVector) txn.getFieldValue("TXNATTACHMENT");
            Log.customer.debug(" %s :: txnPut :: TXNATTACHMENT.size() %s", ClassName, moListElements.size());
            if (moListElements.size() > 0)
            {
                putTXNATTACHMENTS(moListElements);
            }
        }
        catch (Exception loEx2)
        {
            Log.customer.debug(" %s :: Exception occured in txnPut %s", ClassName, loEx2.getLocalizedMessage());
            return TXN_PUT_FAILED;
        }
        finally
        {
            try
            {
                /*
                 * Commit the DB and close the connection and prepared statement
                 */
                moConn.commit();
                moPrep.close();
                BuyintDBIO.closeConnection(moConn);
                /*
                 * Destroy all global variable at the end of execution
                 */
                moCRObj = null;
                moBaseIdObj = null;
                moListElements = null;
                moListTXNSUBDETAIL = null;
                msDBURL = null;
                msDBUser = null;
                msDBPassword = null;
            }
            catch (SQLException loEx1)
            {
                Log.customer.debug(" %s :: Exception occured in txnPut %s", ClassName, loEx1.getLocalizedMessage());
                return TXN_PUT_FAILED;
            }

        }
        return TXN_PUT_SUCCESS;
    }

    /**
     * This method get called from txnPut and used for writing transaction header (TXN) from ariba to BUYINT.TXNHEADER
     * @param txn - ClusterRoot object (ariba.core.Buysense.TXN)
     * @throws BuyintException
     */
    public void putTXNHEADER(ClusterRoot txn) throws BuyintException
    {
        try
        {
            String lsTXNTYPE = (String) txn.getFieldValue("TXNTYPE");
            Log.customer.debug(" %s :: putTXNHEADER :: lsTXNTYPE %s", ClassName, lsTXNTYPE);
            if (lsTXNTYPE != null && lsTXNTYPE.equals("UPD"))
            {
                Log.customer.debug(" %s :: putTXNHEADER :: Caling UPD-Dummy put %s", ClassName, lsTXNTYPE);
                setInsertColumns("TXNHEADER", ls_TXNHEADER_UPD_Fields, ls_TXNHEADER_UPD_Fields, txn);
            }
            else
            {
                Log.customer.debug(" %s :: putTXNHEADER :: Caling normal-HEADER put %s", ClassName, lsTXNTYPE);
                setInsertColumns("TXNHEADER", ls_TXNHEADER_Fields, ls_TXNHEADER_Fields, txn);
            }
            liResult = moPrep.executeUpdate();
        }
        catch (SQLException loEx1)
        {
            Log.customer
                    .debug(" %s :: SQLException occured in putTXNHEADER %s", ClassName, loEx1.getLocalizedMessage());
        }
        catch (Exception loEx2)
        {
            Log.customer.debug(" %s :: Exception occured in putTXNHEADER %s", ClassName, loEx2.getLocalizedMessage());
        }
    }

    /**
     * This method get call from txnPut and is used for writing transaction detail (TXNDetail) from ariba to
     * BUYINT.TXNDETAIL. Each TXNDetail may\can contain more than one TXNSubDetail so for each TXNDetail,
     * putTXNSUBDETAILS is called to put TXNSubDetail to BUYINT.TXNSUBDETAIL based on availability. putTXNDETAILS
     * and putTXNSUBDETAILS will get called only if the field (TXNDETAIL from TXN and TXNSUBDETAIL field from TXNDETAIL
     * in ariba) List size is more than zero.
     * @param txnDetails - List field (ariba.core.Buysense.TXNDetail) in ariba.core.Buysense.TXN object
     * @throws BuyintException
     */
    public void putTXNDETAILS(BaseVector txnDetails) throws BuyintException
    {
        try
        {
            Log.customer.debug(" %s :: putTXNDETAILS :: txnDetails.size() %s", ClassName, txnDetails.size());
            for (int k = 0; k < txnDetails.size(); k++)
            {
                moBaseIdObj = (BaseId) txnDetails.get(k);
                moCRObj = (ClusterRoot) moBaseIdObj.getValueSource();
                Log.customer.debug(" %s :: putTXNDETAILS :: moCRObj %s", ClassName, moCRObj);
                setInsertColumns("TXNDETAIL", ls_TXNDETAIL_Ariba_Fields, ls_TXNDETAIL_DB_Fields, moCRObj);
                liResult = moPrep.executeUpdate();
                moListTXNSUBDETAIL = (BaseVector) moCRObj.getFieldValue("TXNSUBDETAIL");
                Log.customer.debug(" %s :: putTXNDETAILS :: txnSubDetails.size() %s", ClassName, moListTXNSUBDETAIL
                        .size());
                if (moListTXNSUBDETAIL.size() > 0)
                {
                    putTXNSUBDETAILS(moListTXNSUBDETAIL);
                }
            }
            /*
             * Destroy the global variables after putting TXNDETAILS
             */
            destroyGlobalVariables(moCRObj, moBaseIdObj, moListElements);
        }
        catch (SQLException loEx1)
        {
            Log.customer.debug(" %s :: SQLException occured in putTXNDETAILS %s", ClassName, loEx1
                    .getLocalizedMessage());

            /*
             * Destroy the global variables in case of exception
             */
            destroyGlobalVariables(moCRObj, moBaseIdObj, moListElements);
        }
        catch (Exception loEx2)
        {
            Log.customer.debug(" %s :: Exception occured in putTXNDETAILS %s", ClassName, loEx2.getLocalizedMessage());

            /*
             * Destroy the global variables in case of exception
             */
            destroyGlobalVariables(moCRObj, moBaseIdObj, moListElements);
        }
    }

    /**
     * This method get called from putTXNDETAILS if TXNDETAIL contains value for TXNSUBDETAIL and its size is more
     * than zero and used to write transaction sub details (TXNSubDetail) from ariba to BUYINT.TXNSUBDETAIL.
     * @param txnSubDetails - List field (ariba.core.Buysense.TXNSubDetail) in ariba.core.Buysense.TXNDetail object
     * @throws BuyintException
     */
    public void putTXNSUBDETAILS(BaseVector txnSubDetails) throws BuyintException
    {
        try
        {
            Log.customer.debug(" %s :: In putTXNSUBDETAILS", ClassName);
            for (int i = 0; i < txnSubDetails.size(); i++)
            {
                moBaseIdObj = (BaseId) txnSubDetails.get(i);
                moCRObj = (ClusterRoot) moBaseIdObj.getValueSource();
                setInsertColumns("TXNSUBDETAIL", ls_TXNSUBDETAIL_Ariba_Fields, ls_TXNSUBDETAIL_DB_Fields, moCRObj);
                liResult = moPrep.executeUpdate();
            }
            /*
             * Destroy the global variables after putting putTXNSUBDETAILS
             */
            destroyGlobalVariables(moCRObj, moBaseIdObj, moListTXNSUBDETAIL);
        }
        catch (SQLException loEx1)
        {
            Log.customer.debug(" %s :: SQLException occured in putTXNSUBDETAILS %s", ClassName, loEx1
                    .getLocalizedMessage());
            /*
             * Destroy the global variables in case of exception
             */
            destroyGlobalVariables(moCRObj, moBaseIdObj, moListTXNSUBDETAIL);
        }
        catch (Exception loEx2)
        {
            Log.customer.debug(" %s :: Exception occured in putTXNSUBDETAILS %s", ClassName, loEx2
                    .getLocalizedMessage());

            /*
             * Destroy the global variables in case of exception
             */
            destroyGlobalVariables(moCRObj, moBaseIdObj, moListTXNSUBDETAIL);
        }
    }

    /**
     * This method get called from txnPut if TXNATTACHMENT size from TXN is greater than zero. It writes Attachments
     * one by one to BUYINT.TXNATTACHMENT table.
     * @param txnAttachments - List field (ariba.core.Buysense.TXNAttachment) in ariba.core.Buysense.TXN object
     * @throws BuyintException
     */
    public void putTXNATTACHMENTS(BaseVector txnAttachments) throws BuyintException
    {
        try
        {
            for (int i = 0; i < txnAttachments.size(); i++)
            {
                moBaseIdObj = (BaseId) txnAttachments.get(i);
                moCRObj = (ClusterRoot) moBaseIdObj.getValueSource();
                setInsertColumns("TXNATTACHMENT", ls_TXNATTACHMENT_Ariba_Fields, ls_TXNATTACHMENT_DB_Fields, moCRObj);
                liResult = moPrep.executeUpdate();
            }

            /*
             * Destroy the global variables after putting TXNATTACHMENTS
             */
            destroyGlobalVariables(moCRObj, moBaseIdObj, moListElements);
        }
        catch (SQLException loEx1)
        {
            Log.customer.debug(" %s :: SQLException occured in putTXNATTACHMENTS %s", ClassName, loEx1
                    .getLocalizedMessage());

            /*
             * Destroy the global variables in case of exception
             */
            destroyGlobalVariables(moCRObj, moBaseIdObj, moListElements);
        }
        catch (Exception loEx2)
        {
            Log.customer.debug(" %s :: Exception occured in putTXNATTACHMENTS %s", ClassName, loEx2
                    .getLocalizedMessage());

            /*
             * Destroy the global variables in case of exception
             */
            destroyGlobalVariables(moCRObj, moBaseIdObj, moListElements);
        }
    }

    public void destroyGlobalVariables(ClusterRoot crObj, BaseId bIdObj, BaseVector elements)
    {
        crObj = null;
        bIdObj = null;
        elements = null;
    }

    /**
     * This is the generic method get called from putTXNHEADER, putTXNDETAILS, putTXNSUBDETAILS and putTXNATTACHMENTS. This method
     * automatically prepares Insert SQL and Prepared statement based on input parameters. During the preparation of Prepared
     * statement it will checks whether aribaFields starts with lsInteger or lsDate, if so it will does special handling while
     * preparing DB values. See getDBSTRValue, getDBDATEValue and getDBINTValue methods for more details for preparing DB values.
     * Also see declaration of String arrays for field declaration naming conventions. For example take ls_TXNATTACHMENT_Ariba_Fields
     * array value, INTR_LINE_ID_STR is int value in BUYINT DB and it is String in ariba so start the variable name with "INTR_"
     * which is 5 character in length and matches lsInteger in setInsertColumns and substring calculation in getDBSTRValue and
     * getDBDATEValue methods.
     * @param sTableName - Table name to create\insert rows. Expected values are TXNHEADER, TXNDETAILS, TXNSUBDETAILS and TXNATTACHMENTS
     * @param aribaFields - Array of String fields to create DB values for BUYINT tables
     * @param dbFields - Array of String fields to create Insert SQL for BUYINT tables
     * @param obj - ClusterRoot ariba object to create DB values for BUYINT tables
     * @throws BuyintException
     */
    protected void setInsertColumns(String sTableName, String[] aribaFields, String[] dbFields, ClusterRoot obj)
            throws BuyintException
    {
        String lsInteger = "INTR_";
        String lsDate = "DATE_";
        int j = 0;
        try
        {
            /*
             * Check for connection before preparing doing Prepared Statement
             */
            this.checkConnection(obj);
            moPrep = moConn.prepareStatement(getInsertSQL(sTableName, dbFields));
            //Log.customer.debug(" %s :: Started Prepared Statment :: Insert SQL %s", ClassName, getInsertSQL(sTableName, dbFields));
            for (int i = 0; i < aribaFields.length; i++)
            {
                if (aribaFields[i].startsWith(lsInteger))
                {
                    moPrep.setInt(++j, getDBINTValue(obj, aribaFields[i]));
                }
                else if (aribaFields[i].startsWith(lsDate))
                {
                    moPrep.setDate(++j, getDBDATEValue(obj, aribaFields[i]));
                }
                else
                {
                    moPrep.setString(++j, getDBSTRValue(obj, aribaFields[i]));
                    //Log.customer.debug(" %s :: After setString in setInsertColumns ", ClassName);
                }
            }
            j = 0;
        }
        catch (SQLException loEx1)
        {
            Log.customer.debug(" %s :: SQLException occured in setInsertColumns %s", ClassName, loEx1
                    .getLocalizedMessage());
        }
        catch (Exception loEx2)
        {
            Log.customer.debug(" %s :: Exception occured in setInsertColumns %s", ClassName, loEx2
                    .getLocalizedMessage());
        }
    }

    /**
     * This method get called from setInsertColumns method to get respective DB value from ariba ClusterRoot object
     * @param obj - ClusterRoot object and it can be TXN, TXNDETAIL, TXNSUBDETAIL or TXNATTACHMENT
     * @param fieldName - Associated field of ClusterRoot object to get DB value
     * @return Respective DB value for column in the form of String object
     */
    public String getDBSTRValue(ClusterRoot obj, String fieldName)
    {
        Log.customer.debug("Class %s on method getDBSTRValue:: Value %s ", ClassName, obj.getFieldValue(fieldName));
        return (String) obj.getFieldValue(fieldName);
    }

    /**
     * This method get called from setInsertColumns method to get respective DB value from ariba ClusterRoot object
     * @param obj - ClusterRoot object and it can be TXN, TXNDETAIL, TXNSUBDETAIL or TXNATTACHMENT
     * @param fieldName - Associated field of ClusterRoot object to get DB value
     * @return Respective DB value for column in the form of SQL Date object
     */
    public java.sql.Date getDBDATEValue(ClusterRoot obj, String fieldName)
    {
        ariba.util.core.Date loAribaDate = (ariba.util.core.Date) obj.getFieldValue(fieldName.substring(5));
        Log.customer.debug("Class %s on method getDBINTValue:: Value %s ", ClassName, loAribaDate);
        return new java.sql.Date(loAribaDate.getTime());
    }

    /**
     * This method get called from setInsertColumns method to get respective DB value from ariba ClusterRoot object
     * @param obj - ClusterRoot object and it can be TXN, TXNDETAIL, TXNSUBDETAIL or TXNATTACHMENT
     * @param fieldName - Associated field of ClusterRoot object to get DB value. Input can come in the form of String or int.
     * @return Respective DB value for column in the form of int
     */
    public int getDBINTValue(ClusterRoot obj, String fieldName)
    {
        Integer loINTValue = null;
        Object lsAribaField = (Object) obj.getFieldValue(fieldName.substring(5));
        if (lsAribaField instanceof String)
        {
            loINTValue = new Integer((String) lsAribaField);
        }
        else
        {
            loINTValue = (Integer) obj.getFieldValue(fieldName.substring(5));
        }

        Log.customer.debug("Class %s on method getDBINTValue:: Value %s ", ClassName, loINTValue.intValue());
        return loINTValue.intValue();
    }

    /**
     * This is first method called from txnPut. This gets connection info parameters from Parameters.table and opens
     * connection using BuyintDBIO object\class
     */
    private static void getDBConnection()
    {
        try
        {
            /**
             *get the partition object from session else with service
             */
            moPart = Base.getService().getPartition("pcsv");
            if (moPart == null)
            {
                moPart = Base.getSession().getPartition();
            }
            /**
             *get values from Parameter.table for DB connection
             */
            msDBURL = Base.getService().getParameter(moPart, "Application.AMSParameters.BuyintDBaseURL");
            msDBUser = Base.getService().getParameter(moPart, "Application.AMSParameters.BuyintDBaseUser");
            msDBPassword = Base.getService().getParameter(moPart, "Application.AMSParameters.BuyintDBasePassword");

            moConn = BuyintDBIO.getConnection(msDBURL, msDBUser, msDBPassword);
        }
        catch (Exception loEx)
        {
            Log.customer.debug("Class %s :: Exception occured in getDBConnection ", ClassName);
        }
    }

    /**
     * This is always called from setInsertColumns method before creating PreparedStatement to check the connection is open
     * if not creates new connection else throws SQLException
     * @throws SQLException
     */
    public void checkConnection(ClusterRoot obj) throws SQLException
    {
        try
        {
            /**
             *get the partition object from session else with service
             */
            moPart = Base.getService().getPartition("pcsv");
            if (moPart == null)
            {
                moPart = obj.getPartition();
            }

            /**
             *Get values from Parameter.table for DB connection
             */
            msDBURL = Base.getService().getParameter(moPart, "Application.AMSParameters.BuyintDBaseURL");
            msDBUser = Base.getService().getParameter(moPart, "Application.AMSParameters.BuyintDBaseUser");
            msDBPassword = Base.getService().getParameter(moPart, "Application.AMSParameters.BuyintDBasePassword");

            if (moConn == null || moConn.isClosed())
            {
                Log.customer.debug("Class %s :: Connection is closed and re-establishing ", ClassName);
                moConn = BuyintDBIO.getConnection(msDBURL, msDBUser, msDBPassword);
                Log.customer.debug("Class %s :: Connection re-established ", ClassName);
            }
        }
        catch (SQLException loEx)
        {
            /**
             * Get the connection in case of Exception too
             */
            Log.customer.debug("Class %s :: SQLException occured in checkConnection %s", ClassName, loEx
                    .getLocalizedMessage());
        }
        catch (Exception loEx1)
        {
            /**
             * Get the connection in case of Exception too
             */
            Log.customer.debug("Class %s :: Exception occured in checkConnection %s", ClassName, loEx1
                    .getLocalizedMessage());
        }
    }

    /**
     * This method creates Insert SQL based on input parameters, using TableName and BUYINT DB fields. This method is called
     * from setInsertColumns. See getColumns and getValues for more details used to formulate Insert SQL.
     * @param sTableName - TableName to create Insert SQL
     * @param fields - Array of String fields to create Insert SQL
     * @return - String of Insert SQL
     */
    protected String getInsertSQL(String sTableName, String[] fields)
    {
        return "INSERT INTO " + sTableName + "(" + getColumns(fields) + ")" + " VALUES (" + getValues(fields) + ")";
    }

    /**
     * This method creates BUYINT DB table column names using input array of String fields. This is called from getInsertSQL
     * method.
     * @param fields - String array of BUYINT DB field names
     * @return - String of BUYINT DB fields (Each field is separated by comma)
     */
    public String getColumns(String[] fields)
    {
        for (int i = 0; i < fields.length; i++)
        {
            if (mbCommaSpaceRqrd)
            {
                msColumnsResult = msColumnsResult + msCommaSpace + fields[i];
            }
            else
            {
                msColumnsResult = fields[i];
                mbCommaSpaceRqrd = true;
            }
        }
        mbCommaSpaceRqrd = false;
        Log.customer.debug("Class %s on method getValues:: msColumnsResult %s ", ClassName, msColumnsResult);
        return msColumnsResult;
    }

    /**
     * This method creates BUYINT DB table column values using length of array String fields. This is called from getInsertSQL
     * method.
     * @param fields - String array of BUYINT DB field names
     * @return - String of BUYINT DB values nothing but questions marks separated with comma
     */
    public String getValues(String[] fields)
    {
        String lsQuestion = "?";

        for (int i = 0; i < fields.length; i++)
        {
            if (mbCommaSpaceRqrd)
            {
                msColumnsResult = msColumnsResult + msCommaSpace + lsQuestion;
            }
            else
            {
                msColumnsResult = lsQuestion;
                mbCommaSpaceRqrd = true;
            }
        }
        Log.customer.debug("Class %s on method getValues:: msColumnsResult %s ", ClassName, msColumnsResult);
        mbCommaSpaceRqrd = false;
        return msColumnsResult;
    }

    /**
     * This is the main calling method from AMXTXPULL.java. This method gets query as input to pull status pending records
     * from BUYINT.
     * @param pullQuery - Query input from AMXTXPULL.java
     * @return - int result after status processing of the record
     */
    public static int getTXN(String pullQuery)
    {
        Log.customer.debug("Class %s :: Inside getTXN ", ClassName);
        ResultSet loResultSet = getTXNStatusResults(pullQuery);
        int liResult = 0;
        if(loResultSet != null)
        {
            liResult =  createBuysenseTXNStatusObj(loResultSet);
        }
        Log.customer.debug("Class %s :: getTXN :: liResult %s", ClassName, liResult);
        return liResult;
    }

    /**
     * This method creates the BuysenseTXNStatus ClusterRoot objects as per results fetched in getTXN method
     * @param loResultSet - ResultSet from getTXN method
     * @return - int result after creating and setting field values in BuysenseTXNStatus ClusterRoot
     */
    private static int createBuysenseTXNStatusObj(ResultSet loResultSet)
    {
        try
        {
            Log.customer.debug("Class %s :: Inside createBuysenseTXNStatusObj ", ClassName);
            if(moPart == null)
            {
               moPart = Base.getService().getPartition("pcsv");
            }

            while (loResultSet != null && loResultSet.next())
            {
                Log.customer.debug("Class %s :: createBuysenseTXNStatusObj::moPart %s", ClassName, moPart);
                if(!objectAlreadyExists(loResultSet, moPart))
                {
                   ClusterRoot    loBuysenseTXNStatusObj = (ClusterRoot) ClusterRoot.create("ariba.core.BuysenseTXNStatus", moPart);
                   loBuysenseTXNStatusObj.save();
                   setFieldValues(loResultSet, loBuysenseTXNStatusObj, ls_BUYSENSE_TXN_Fields);
                }
            }
            moStatusPullStatement.close();
            BuyintDBIO.closeConnection(moConn);
        }
        catch (SQLException loEx)
        {
           Log.customer.debug("Class %s :: SQLException occured in createBuysenseTXNStatusObj %s", ClassName, loEx
                        .getLocalizedMessage());
           return TXN_GET_FAILED;
        }
        catch (Exception loEx1)
        {
           Log.customer.debug("Class %s :: Exception occured in createBuysenseTXNStatusObj %s", ClassName, loEx1
                        .getLocalizedMessage());
           return TXN_GET_FAILED;
        }
        return TXN_GET_SUCCESS;
    }

    /**
     * This method checks if BuysenseTXNStatus ClusterRoot already exists in ariba
     * @param loResultSet - ResultSet from getTXN method
     * @param part - Partiton from createBuysenseTXNStatusObj method
     * @return - boolean result whether this object already exist in ariba or not
     */
    public static boolean objectAlreadyExists(ResultSet loResultSet, Partition part)
    {
        boolean lbAlreadyExists = false;
        try
        {
           ClusterRoot loTXNStatusObj  = (ClusterRoot) Base.getService().objectMatchingUniqueName
                                         ("ariba.core.BuysenseTXNStatus", part,loResultSet.getString("UniqueName"));
           if(loTXNStatusObj != null)
           {
               lbAlreadyExists = true;
           }
        }
        catch (SQLException loEx)
        {
           Log.customer.debug("Class %s :: SQLException occured in objectAlreadyExists %s", ClassName, loEx
                        .getLocalizedMessage());
        }
        catch (Exception loEx1)
        {
           Log.customer.debug("Class %s :: Exception occured in objectAlreadyExists %s", ClassName, loEx1
                        .getLocalizedMessage());
        }
        return lbAlreadyExists;
    }

    /**
     * This method sets  matching value of BUYINT resultset row with BuysenseTXNStatus ClusterRoot in ariba
     * @param loResultSet - ResultSet to get values from
     * @param crObj - BuysenseTXNStatus ClusterRoot object to set values
     */
    public static void setFieldValues(ResultSet loResultSet, ClusterRoot crObj, String[] objFields)
    {
        try
        {
           for (int i = 0; i < objFields.length; i++)
           {
              Log.customer.debug("Class %s :: createBuysenseTXNStatusObj::objFields[i] %s", ClassName, objFields[i]);
              //crObj.setFieldValue(objFields[i], loResultSet.getString(objFields[i]));
              crObj.setFieldValue(objFields[i], loResultSet.getString(objFields[i]) == null? null : loResultSet.getString(objFields[i]));
           }
           crObj.save();
        }
           catch (SQLException loEx)
           {
              Log.customer.debug("Class %s :: SQLException occured in setFieldValues %s", ClassName, loEx
                           .getLocalizedMessage());
           }
           catch (Exception loEx1)
           {
              Log.customer.debug("Class %s :: Exception occured in setFieldValues %s", ClassName, loEx1
                           .getLocalizedMessage());
           }
    }

	/**
     * This method fetches rows from BUYINT for the input query of AMSTXNPULL.java
     * @param pullQuery - input query from AMSTXNPULL.java
     * @return - ResultSet of rows fetched from BUYINT
     */
    private static ResultSet getTXNStatusResults(String pullQuery)
    {
        ResultSet loPullResults = null;
        try
        {
           getDBConnection();
           moStatusPullStatement = moConn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);;
           loPullResults = moStatusPullStatement.executeQuery(pullQuery);
        }
        catch (SQLException loEx)
        {
            Log.customer.debug("Class %s :: SQLException occured in getTXNStatusResults %s", ClassName, loEx
                    .getLocalizedMessage());
            return loPullResults;
        }
        catch (Exception loEx1)
        {
            Log.customer.debug("Class %s :: Exception occured in getTXNStatusResults %s", ClassName, loEx1
                    .getLocalizedMessage());
            return loPullResults;
        }
        return loPullResults;
    }
}