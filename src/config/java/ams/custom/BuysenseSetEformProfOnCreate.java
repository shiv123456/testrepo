package config.java.ams.custom;

import java.util.List;

import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.user.core.User;
import ariba.util.core.ListUtil;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseSetEformProfOnCreate extends Action
{

    public String sClassName = "BuysenseSetEformProfOnCreate";

    public void fire(ValueSource valueSource, PropertyTable arg1) throws ActionExecutionException
    {
        String sEformProfUN = "";
        User user = (User) Base.getSession().getEffectiveUser();
        Log.customer.debug(sClassName + " user: " + user);

        BuysenseEformDirectAccessUtil loUserProf = BuysenseEformDirectAccessUtil.getInstance();
        sEformProfUN = loUserProf.lookupProf(user.getUniqueName());
        Log.customer.debug(sClassName + " sEformProfUN: " + sEformProfUN);

        ClusterRoot loEform = (ClusterRoot) valueSource;
        Partition partition = Base.getService().getPartition("pcsv");
        ClusterRoot loEformProf = Base.getService().objectMatchingUniqueName("ariba.core.BuysEformProf", partition, sEformProfUN);
        Log.customer.debug(sClassName + " loEformProf: " + loEformProf);
        //CHECK IF USER has access to this profile before setting it

        if (loEformProf != null && isValidUserProfile(user, sEformProfUN))
        {
            Log.customer.debug(sClassName + " Setting loEformProf: " + loEformProf);
            loEform.setFieldValue("EformChooser", loEformProf);
        }

        if(!StringUtil.nullOrEmptyOrBlankString(sEformProfUN))
        {
            Log.customer.debug(sClassName + " resetting user profile mapping for "+user.getUniqueName()+", "+sEformProfUN);
            loUserProf.putProfId(user.getUniqueName(), "");
        }
    }

    private boolean isValidUserProfile(User loUser, String sEformProfUN)
    {
        boolean bValidProf = false;
        List<?> loCurrentUserProfiels = ListUtil.list();
        ariba.common.core.User loPartUser = ariba.common.core.User.getPartitionedUser(loUser, Base.getSession().getPartition());
        loCurrentUserProfiels = (List<?>) loPartUser.getDottedFieldValue("BuysEformProfiles");
        if (loCurrentUserProfiels != null && !loCurrentUserProfiels.isEmpty())
        {
            for (int i = 0; i < loCurrentUserProfiels.size(); i++)
            {
                ClusterRoot loUserProf = (ClusterRoot) ((BaseId) loCurrentUserProfiels.get(i)).get();
                boolean bActive = ((Boolean) loUserProf.getDottedFieldValue("Active")).booleanValue();
                Log.customer.debug(sClassName +" loUserProf: "+loUserProf+", bActive: " + bActive);
                if (bActive && loUserProf.getUniqueName().equalsIgnoreCase(sEformProfUN))
                {
                    bValidProf = true;
                    break;
                }

            }

        }
        Log.customer.debug(sClassName + " bValidProf: " + bValidProf);
        return bValidProf;
    }

}
