
/**
DATE			29/10/2010
AUTHOR			SRINI
CHANGES			NEW CLASS: TIBCO is removed in 9r1 and written a new class to achieve the same functionality of
				BuySenseTxnStatusEvent (TIBCO integration event). This class just calls AMSTXNJDBC using getTXN
				and all processing is done in calling class.
*/

package config.java.ams.custom;

import java.util.Map;

import ariba.util.log.Log;
import ariba.util.scheduler.ScheduledTask;
import ariba.util.scheduler.Scheduler;

public class AMSTXNPULL extends ScheduledTask {

	public static final String ClassName = "config.java.ams.custom.AMSTXNPULL";

	String msPULLQUERY = null;

	public void init(Scheduler scheduler, String scheduledTaskName, Map arguments)
	{
        super.init(scheduler, scheduledTaskName, arguments);
        String lsKey = null;
        for(java.util.Iterator e = arguments.keySet().iterator(); e.hasNext();)
        {
           lsKey = (String)e.next();
           if(lsKey.equals("TXN_STATUS_PULL"))
           {
               msPULLQUERY = (String) arguments.get(lsKey);
           }
        }
	}

	public void run()
	{
	    int liStatusResult = AMSTXNJDBC.getTXN(msPULLQUERY);
	    Log.customer.debug("Class %s :: createBuysenseTXNStatusObj::liStatusResult %s", ClassName, liStatusResult);
	}
}