package config.java.ams.custom;

import ariba.base.fields.Behavior;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ConditionResult;
import ariba.base.fields.ValueInfo;
import ariba.procure.core.ProcureLineItem;
import ariba.purchasing.core.Requisition;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseReqLineNIGPCommCodeValidation extends Condition {

	private static final ValueInfo parameterInfo[];

	public boolean evaluate(Object obj, PropertyTable params){
		Log.customer
				.debug("Inside BuysenseReqLineNIGPCommCodeValidation" + obj);
		if (obj != null && obj instanceof ProcureLineItem) {
			return isValidNIGPCommodityCodeReq((ProcureLineItem) obj);
		}
		return true;
	}

	private boolean isValidNIGPCommodityCodeReq(ProcureLineItem loPli) {
		//boolean bValidValue = true;
		if (loPli != null && loPli instanceof ProcureLineItem) {
			Object loNIGPCommodityCode;
			Log.customer
					.debug("BuysenseReqLineNIGPCommCodeValidation: Inside Product line item"
							+ loPli);
			Requisition loReq = (Requisition) loPli.getLineItemCollection();
			boolean bisAdHoc = (Boolean) loPli.getFieldValue("IsAdHoc");
			loNIGPCommodityCode = (Object) loPli
					.getDottedFieldValue("ReqLineNIGPCommodityCode");
			if (loReq != null) {
				Log.customer
						.debug("BuysenseCheckTransactionSource:Inside Requisition now "
								+ loReq);
				String transactionSource = (String) loReq
						.getFieldValue("TransactionSource");
				boolean bBypassApprovers = (Boolean) loReq.getFieldValue("BypassApprovers");
				if(loNIGPCommodityCode == null)
				{	
				if((transactionSource == null && bisAdHoc)||(!StringUtil.nullOrEmptyOrBlankString(transactionSource)) && transactionSource.equals("EXT")&& !bBypassApprovers)
				{
					Log.customer
							.debug("BuysenseReqLineNIGPCommCodeValidation:Inside BuysenseReqLineNIGPCommCodeValidation:Native Non-Catalog line found"
									+ loPli);
					return false;
				}
				}

			}
		}
		return true;
	}

	public ConditionResult evaluateAndExplain(Object value, PropertyTable params) {
		if (!evaluate(value, params)) {
			String msg = params.stringPropertyForKey("Message");
			Log.customer.debug("BuysenseReqLineNIGPCommCodeValidation:msg %s ", msg);
			return new ConditionResult(msg);
		} else {
			return null;
		}
	}

	protected ValueInfo[] getParameterInfo() {
		return parameterInfo;
	}

	static {
		parameterInfo = (new ValueInfo[] { new ValueInfo("SourceObject", 0),
				new ValueInfo("Message", 0, Behavior.StringClass) });
	}

}
