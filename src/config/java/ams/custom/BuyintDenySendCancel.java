/************************************************************************************
 * Author:  Richard Lee
 * Date:    July 30, 2004
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 07/30/2004        Richard Lee            Integration Interface
 * 10/14/2004        Richard Lee            UAT SPL 26 - Errors from PreEncumbrance logic
 *
 * @(#)BuyintDenySendCancel.java     1.0 08/24/2004
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/

/*
 * rlee, 10/12/2004: UAT SPL 26, 36... check null values for Pre-Integration requisitions.
 *
 * rlee August 2004: XML Integration Interface - PreEncumbrance
 * Since there is no Ariba Deny state, StatusString field change is used to trigger this program.
 * If the StatusString is not = Denied, then get out.
 * If it is Denied Status, check if this Requisition is being or has been sent to ERP
 * If this Req is being sent or has been sent, issue a Cancel req to ERP.
 * If PreEncumbrance has not been sent, do nothing.
 *
 */
package config.java.ams.custom;

import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.core.*;
import ariba.common.core.*;
import ariba.util.log.Log;
import ariba.purchasing.core.Requisition;
import ariba.base.fields.Action;
import ariba.util.core.StringUtil;
import ariba.util.formatter.BooleanFormatter;

public class BuyintDenySendCancel extends Action implements BuyintConstants
{
    public void fire (ValueSource object, PropertyTable parameters)
        throws ActionExecutionException
    {
        Log.customer.debug("Calling BuyintDenySendCancel");
        Requisition loReq = (Requisition) object;
        String lsStatusString = (String) loReq.getFieldValue("StatusString");
        // Should never required to send PCNCL unless Req is in Denied or Submitted status.
        if (!((lsStatusString.equalsIgnoreCase("Denied"))||(lsStatusString.equalsIgnoreCase("Submitted"))))
        {
           Log.customer.debug("Leaving BuyintDenySendCancel since StatusString is not Denied or Submitted");
           return;
        }

        boolean lbFinallyApproved = false;
        String lsPECheck = (String) loReq.getFieldValue("PreEncumbranceStatus");
        Boolean lbPreEncumbered = (Boolean)loReq.getFieldValue("PreEncumbered");
        Date loSubDate = (Date) loReq.getFieldValue("SubmitDate");
        Date loAppDate = (Date) loReq.getFieldValue("ApprovedDate");

        if(StringUtil.nullOrEmptyOrBlankString(lsPECheck))
        {
            loReq.setFieldValue("PreEncumbranceStatus", STATUS_ERP_READY);
        }
        if(StringUtil.nullOrEmptyOrBlankString(BooleanFormatter.getStringValue(lbPreEncumbered)))
        {
            loReq.setFieldValue("PreEncumbered", new Boolean(false));
            lbPreEncumbered = new Boolean(false);
        }

        // If ApprovedDate is later than SubmittedDate, the req has been approved and
        // going to be in ordering or ordered status
        if ( loAppDate != null && loSubDate != null && loAppDate.after(loSubDate))
        {   lbFinallyApproved = true;
        }

        /***********************************************************************************************
        * Only send Req Cancel(PCNCL) to ERP if req has not been finally approved
        * by all approvers, but has been APPROVE by ERP.  So when one of the approvers editing the requisition,
        * this would send the req back to the beginning of the approval flow; therefore, need to send Req Cancel here.
        ***********************************************************************************************/

        if (lsPECheck.equals(STATUS_ERP_APPROVE) && lbPreEncumbered.booleanValue() && !lbFinallyApproved)
        {
           BuyintXMLFactory.submit(TXNTYPE_PREENC_CANCEL,loReq);
           BaseObject loReqObject = (BaseObject) loReq;
           String lsReqUN = (String) loReq.getDottedFieldValue("UniqueName");
           String lsRecordType = "PreEncCRecord";
           String lsMessage = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","PreEncDeniedMessage");
           Partition loPartition = loReq.getPartition();
           User loAribaSystemUser = Core.getService().getAribaSystemUser(loPartition);

           BuysenseUtil.createHistory(loReqObject, lsReqUN,  lsRecordType, lsMessage, loAribaSystemUser);
           loReq.setFieldValue("PreEncumbranceStatus", STATUS_ERP_CANCELLED);
           loReq.setFieldValue("PreEncumbered", new Boolean(false));
        }
        return;
    }
}
