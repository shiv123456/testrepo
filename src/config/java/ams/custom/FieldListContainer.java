
/**
This program is designed to run in a nightly cycle or on demand.
imohideen, AMS, August 2000
*/

/**
WA ER #75: Add new ReqHeadCB3Value field to HeaderFieldValues list and
PaymentHeadText5Value and PaymentHeadText6Value fields to PaymentHeaderFieldValues list
*/

/**
ST SPL 414 :imohideen:  Created a new list for copying fields from Req to PO which includes
                        Billing Address.

rlee, 2/28/03, eProc ST SPL 895 - Order does not versioned if Pcard changed.
Added PCard vector

rgiesen, 2/19/2004 Dev SPL #27 - Java Null Errors - the vector variables are now defined as
    private final

*/

/*
VEPI Prod #727: Incorrect ShipTo address on POB orders. Reapplied fix corresponding
                to VEPI Prod #287 which should have had matching 8.1 SPL.
*/

/*
VEPI Prod #812: Change Order was getting created with original Needby date. Added NeedBy into ReqToPOCopyFieldValues array. 
*/


package config.java.ams.custom;

import java.util.List;
// Ariba 8.1: Various methods have changed from the java.util.List package to the ariba.util.core.ListUtil package.
import ariba.util.core.ListUtil;



//81->822 changed Vector to List
public class FieldListContainer
{
private static String [] RefAcctgTextFieldValues = {"LineAcctText1Value","LineAcctText2Value","LineAcctText3Value","LineAcctText4Value","LineAcctText5Value",
                                        "LineAcctText6Value","LineAcctText7Value","LineAcctText8Value","LineAcctText9Value",
                                        "LineAcctText10Value"};
private static String [] AcctgFields =
{ "Field1","Field2","Field3","Field4","Field5","Field6","Field7","Field8",
"Field9","Field10","Field11","Field12","Field13","Field14","Field15",
"Field16","Field17","Field18","Field19","Field20","Field21","Field22","Field23",
"Field24","Field25","LineAcctText1","LineAcctText2",
"LineAcctText3","LineAcctText4","LineAcctText5","LineAcctText6",
"LineAcctText7","LineAcctText8","LineAcctText9","LineAcctText10","LineAcctCB1",
"LineAcctCB2","LineAcctCB3","LineAcctCB4","LineAcctCB5"};

private static String [] AcctgFieldValues =
{"FieldDefault1","FieldDefault2","FieldDefault3","FieldDefault4","FieldDefault5",
"FieldDefault6","FieldDefault7","FieldDefault8","FieldDefault9","FieldDefault10",
"FieldDefault11","FieldDefault12","FieldDefault13","FieldDefault14","FieldDefault15",
"FieldDefault16","FieldDefault17","FieldDefault18","FieldDefault19","FieldDefault20",
"FieldDefault21","FieldDefault22","FieldDefault23","FieldDefault24","FieldDefault25",
"LineAcctText1Value","LineAcctText2Value","LineAcctText3Value","LineAcctText4Value","LineAcctText5Value",
"LineAcctText6Value","LineAcctText7Value","LineAcctText8Value","LineAcctText9Value",
"LineAcctText10Value","LineAcctCB1Value","LineAcctCB2Value","LineAcctCB3Value","LineAcctCB4Value",
"LineAcctCB5Value"};

private static String [] RefAcctgFields = { "Field1","Field2","Field3","Field4","Field5","Field6","Field7","Field8",
"Field9","Field10","Field11","Field12","Field13","Field14","Field15",
"Field16","Field17","Field18","Field19","Field20","Field21","Field22","Field23",
"Field24","Field25"};
private static String [] ChooserFields = { "Field1","Field2","Field3","Field4","Field5","Field6","Field7","Field8",
"Field9","Field10","Field11","Field12","Field13","Field14","Field15",
"Field16","Field17","Field18","Field19","Field20","Field21","Field22","Field23",
"Field24","Field25","ReqHeadField1","ReqHeadField2","ReqHeadField3",
"ReqHeadField4","ReqHeadField5","ReqHeadField6","ReqLineField1","ReqLineField2","ReqLineField3","ReqLineField4",
"PaymentHeadField1","PaymentHeadField2","PaymentHeadField3","PaymentHeadField4",
 "PaymentLineField1","PaymentLineField2","PaymentLineField3","PaymentLineField4"};

private static String [] ChooserFieldValues = { "FieldDefault1","FieldDefault2","FieldDefault3","FieldDefault4","FieldDefault5",
"FieldDefault6","FieldDefault7","FieldDefault8","FieldDefault9","FieldDefault10",
"FieldDefault11","FieldDefault12","FieldDefault13","FieldDefault14","FieldDefault15",
"FieldDefault16","FieldDefault17","FieldDefault18","FieldDefault19","FieldDefault20",
"FieldDefault21","FieldDefault22","FieldDefault23","FieldDefault24","FieldDefault25",
"ReqHeadFieldDefault1","ReqHeadFieldDefault2","ReqHeadFieldDefault3",
"ReqHeadFieldDefault4","ReqHeadFieldDefault5","ReqHeadFieldDefault6","ReqLineFieldDefault1","ReqLineFieldDefault2","ReqLineFieldDefault3",
 "ReqLineFieldDefault4","PaymentHeadFieldDefault1","PaymentHeadFieldDefault2","PaymentHeadFieldDefault3",
 "PaymentHeadFieldDefault4","PaymentLineFieldDefault1","PaymentLineFieldDefault2","PaymentLineFieldDefault3",
 "PaymentLineFieldDefault4"};
private static String [] ReqHeaderChooserFieldValues = { "ReqHeadFieldDefault1","ReqHeadFieldDefault2","ReqHeadFieldDefault3",
"ReqHeadFieldDefault4","ReqHeadFieldDefault5","ReqHeadFieldDefault6",};

     private static String [] ReqLineChooserFieldValues = { "ReqLineFieldDefault1","ReqLineFieldDefault2","ReqLineFieldDefault3",
"ReqLineFieldDefault4"};

private static String [] AcctgChooserFieldValues = { "FieldDefault1","FieldDefault2","FieldDefault3","FieldDefault4","FieldDefault5",
"FieldDefault6","FieldDefault7","FieldDefault8","FieldDefault9","FieldDefault10",
"FieldDefault11","FieldDefault12","FieldDefault13","FieldDefault14","FieldDefault15",
"FieldDefault16","FieldDefault17","FieldDefault18","FieldDefault19","FieldDefault20",
"FieldDefault21","FieldDefault22","FieldDefault23","FieldDefault24","FieldDefault25"};


private static String [] HeaderFields = {"ReqHeadField1","ReqHeadField2","ReqHeadField3","ReqHeadField4","ReqHeadField5","ReqHeadField6","ReqHeadText1",
"ReqHeadText2","ReqHeadText3","ReqHeadText4","ReqHeadCB1","ReqHeadCB2","ReqHeadCB3","ReqHeadCB4","ReqHeadCB5","BypassPreEncumbranceEncumbrance"};

private static String [] HeaderFieldValues = {"ReqHeadFieldDefault1","ReqHeadFieldDefault2","ReqHeadFieldDefault3",
                                    "ReqHeadFieldDefault4","ReqHeadFieldDefault5","ReqHeadFieldDefault6","ReqHeadText1Value","ReqHeadText2Value","ReqHeadText3Value",
                                    "ReqHeadText4Value","ReqHeadCB1Value","ReqHeadCB2Value","ReqHeadCB3Value","ReqHeadCB4Value","ReqHeadCB5Value","BypassPreEncumbranceEncumbrance" };

    private static String [] LineFields = { "ReqLineField1","ReqLineField2","ReqLineField3","ReqLineField4","ReqLineText1",
                                    "ReqLineText2","ReqLineText3","ReqLineText4","ReqLineCB1","ReqLineCB2"};

    private static String [] LineFieldValues = { "ReqLineFieldDefault1","ReqLineFieldDefault2","ReqLineFieldDefault3",
                                        "ReqLineFieldDefault4","ReqLineText1Value","ReqLineText2Value","ReqLineText3Value",
                                        "ReqLineText4Value","ReqLineCB1Value","ReqLineCB2Value"};
    private static String [] TestFields = { "Field1","Field2","Field3","Field4","Field5"};
    private static String [] TestFieldValues ={"FieldDefault1","FieldDefault2","FieldDefault3",
                                         "FieldDefault4","FieldDefault5"};
    private static String [] PaymentHeaderFields = {"PaymentHeadField1","PaymentHeadField2","PaymentHeadField3","PaymentHeadField4","PaymentHeadText1",
                                    "PaymentHeadText2","PaymentHeadText3","PaymentHeadText4","PaymentHeadText5","PaymentHeadText6","PaymentHeadCB1","PaymentHeadCB2"};

private static String [] PaymentHeaderFieldValues = {"PaymentHeadFieldDefault1","PaymentHeadFieldDefault2","PaymentHeadFieldDefault3",
                                    "PaymentHeadFieldDefault4","PaymentHeadText1Value","PaymentHeadText2Value","PaymentHeadText3Value",
                                    "PaymentHeadText4Value","PaymentHeadText5Value","PaymentHeadText6Value","PaymentHeadCB1Value","PaymentHeadCB2Value" };
 private static String [] PaymentLineFields = { "PaymentLineField1","PaymentLineField2","PaymentLineField3","PaymentLineField4","PaymentLineText1",
                                    "PaymentLineText2","PaymentLineText3","PaymentLineText4","PaymentLineCB1","PaymentLineCB2"};

    private static String [] PaymentLineFieldValues = { "PaymentLineFieldDefault1","PaymentLineFieldDefault2","PaymentLineFieldDefault3",
                                        "PaymentLineFieldDefault4","PaymentLineText1Value","PaymentLineText2Value","PaymentLineText3Value",
                                        "PaymentLineText4Value","PaymentLineCB1Value","PaymentLineCB2Value"};

private static String [] ApproversAmounts = {"SupervisorFirstApprover","Approver","ApproverAmt","ProcurementOfficer",
                                    "ProcurementOfficerAmt","FinOfficer","FinOfficerAmt","ReceiptApprover1","ReceiptApprover2",
                                    "ReceiptApprover3","PaymentInitiator","PaymentApprover","NonCatalogApprover","NonCatalogProcessor","OOTPaymentApprover",
                                    "OOTAmount","OOTPercent","CommCodeCat1","CommCodeCat1","CommCodeCat1","CommCodeCat1","CommCodeCat1",
                                    "CommCodeCat1","CommCodeCat2","CommCodeCat3","CommCodeCat4","CommCodeCat5","CommCodeCat6","CommCodeCat7",
                                    "CommCodeCat8","CommCodeCat9","CommCodeCat10","CommCodeCat11","CommCodeCat12","CommCodeCat13","CommCodeCat14","CommCodeCat15",
                                    "CommCodeCat16","CommCodeCat17","CommCodeCat18","CommCodeCat19","CommCodeCat20","CommCodeCat21","CommCodeCat22",
                                    "CommCodeCat23","CommCodeCat24","CommCodeCat25","CommCodeCat26","CommCodeCat27","CommCodeCat28","CommCodeCat29",
                                    "CommCodeCat30","CommCodeCat31","CommCodeCat32","CommCodeCat33","CommCodeCat34","CommCodeCat35","CommCodeCat36",
                                    "CommCodeCat37","CommCodeCat38","CommCodeCat39","CommCodeCat40",
                                    "CommCodeCatAppr1","CommCodeCatAppr2","CommCodeCatAppr3","CommCodeCatAppr4","CommCodeCatAppr5","CommCodeCatAppr6","CommCodeCatAppr7",
                                    "CommCodeCatAppr8","CommCodeCatAppr9","CommCodeCatAppr10","CommCodeCatAppr11","CommCodeCatAppr12","CommCodeCatAppr13","CommCodeCatAppr14","CommCodeCatAppr15",
                                    "CommCodeCatAppr16","CommCodeCatAppr17","CommCodeCatAppr18","CommCodeCatAppr19","CommCodeCatAppr20","CommCodeCatAppr21","CommCodeCatAppr22",
                                    "CommCodeCatAppr23","CommCodeCatAppr24","CommCodeCatAppr25","CommCodeCatAppr26","CommCodeCatAppr27","CommCodeCatAppr28","CommCodeCatAppr29",
                                    "CommCodeCatAppr30","CommCodeCatAppr31","CommCodeCatAppr32","CommCodeCatAppr33","CommCodeCatAppr34","CommCodeCatAppr35","CommCodeCatAppr36",
                                    "CommCodeCatAppr37","CommCodeCatAppr38","CommCodeCatAppr39","CommCodeCatAppr40",
                                    "WatchCommCodeCat1","WatchCommCodeCat2","WatchCommCodeCat3","WatchCommCodeCat4","WatchCommCodeCat5","WatchCommCodeCat6",
                                    "WatchCommCodeCat7","WatchCommCodeCat8","WatchCommCodeCat9","WatchCommCodeCat10","CommCodeCatWatcher1",
                                    "CommCodeCatWatcher2","CommCodeCatWatcher3","CommCodeCatWatcher4","CommCodeCatWatcher5","CommCodeCatWatcher6","CommCodeCatWatcher7",
                                    "CommCodeCatWatcher8","CommCodeCatWatcher9","CommCodeCatWatcher10"};
private static String [] PaymentHeaderChooserFieldValues = { "PaymentHeadFieldDefault1","PaymentHeadFieldDefault2","PaymentHeadFieldDefault3",
"PaymentHeadFieldDefault4",};

     private static String [] PaymentLineChooserFieldValues = { "PaymentLineFieldDefault1","PaymentLineFieldDefault2","PaymentLineFieldDefault3",
"PaymentLineFieldDefault4"};
private static String [] ReqToPOCopyFieldValues = { "ReqLineFieldDefault1","ReqLineFieldDefault2","ReqLineFieldDefault3",
                                        "ReqLineFieldDefault4","ReqLineText1Value","ReqLineText2Value","ReqLineText3Value",
                                        "ReqLineText4Value","ReqLineCB1Value","ReqLineCB2Value","BillingAddress","ShipTo","NeedBy"};


// rlee, 5/15/2003 ST SPL 899 - POBs are imported with blank Deliver To field
// rlee, added DeliverTo in LineFieldDefaultingValues
// LineFieldValues + ShipTo + Billingaddress
private static String [] LineFieldDefaultingValues = { "ReqLineFieldDefault1","ReqLineFieldDefault2","ReqLineFieldDefault3",
                                        "ReqLineFieldDefault4","ReqLineText1Value","ReqLineText2Value","ReqLineText3Value",
                                        "ReqLineText4Value","ReqLineCB1Value","ReqLineCB2Value","ShipTo","BillingAddress", "DeliverTo"};

// AcctgFieldValues + ClientName
private static String [] AcctgFieldDefaultingValues = {"FieldDefault1","FieldDefault2","FieldDefault3","FieldDefault4","FieldDefault5",
                                        "FieldDefault6","FieldDefault7","FieldDefault8","FieldDefault9","FieldDefault10",
                                        "FieldDefault11","FieldDefault12","FieldDefault13","FieldDefault14","FieldDefault15",
                                        "FieldDefault16","FieldDefault17","FieldDefault18","FieldDefault19","FieldDefault20",
                                        "FieldDefault21","FieldDefault22","FieldDefault23","FieldDefault24","FieldDefault25",
                                        "LineAcctText1Value","LineAcctText2Value","LineAcctText3Value","LineAcctText4Value","LineAcctText5Value",
                                        "LineAcctText6Value","LineAcctText7Value","LineAcctText8Value","LineAcctText9Value",
                                        "LineAcctText10Value","LineAcctCB1Value","LineAcctCB2Value","LineAcctCB3Value","LineAcctCB4Value",
                                        "LineAcctCB5Value","ClientName"};


private static String [] HeaderPCardFieldValues = {"ReqHeadFieldDefault1","ReqHeadFieldDefault2","ReqHeadFieldDefault3",
                                    "ReqHeadFieldDefault4","ReqHeadFieldDefault5","ReqHeadFieldDefault6","ReqHeadText1Value","ReqHeadText2Value","ReqHeadText3Value",
                                    "ReqHeadText4Value","ReqHeadCB1Value","ReqHeadCB2Value","ReqHeadCB3Value","ReqHeadCB4Value","ReqHeadCB5Value","UsePCardBool","PCardToUse","BypassPreEncumbranceEncumbrance" };

//rgiesen, 2/19/2004 Dev SPL #27 - define them as private static final
private static final List  RefAcctgTextFieldValuesVector = ListUtil.arrayToList(RefAcctgTextFieldValues);
private static final List  AcctgFieldsVector = ListUtil.arrayToList( AcctgFields);
private static final List  RefAcctgFieldsVector = ListUtil.arrayToList( RefAcctgFields);
private static final List  AcctgFieldValuesVector = ListUtil.arrayToList(AcctgFieldValues);
private static final List  ReqHeaderChooserFieldValuesVector = ListUtil.arrayToList(ReqHeaderChooserFieldValues);
private static final List  ReqLineChooserFieldValuesVector = ListUtil.arrayToList(ReqLineChooserFieldValues);
private static final List  HeaderFieldsVector = ListUtil.arrayToList(HeaderFields);
private static final List  HeaderFieldValuesVector = ListUtil.arrayToList(HeaderFieldValues);
private static final List  HeaderFieldValuesOnlyVector = ListUtil.arrayToList(HeaderFieldValues);
private static final List  LineFieldsVector = ListUtil.arrayToList(LineFields);
private static final List  LineFieldValuesVector = ListUtil.arrayToList(LineFieldValues);
private static final List  TestFieldsVector = ListUtil.arrayToList(TestFields);
private static final List  TestFieldValuesVector = ListUtil.arrayToList(TestFieldValues);
private static final List  ChooserFieldsVector = ListUtil.arrayToList(ChooserFields);
private static final List  ChooserFieldValuesVector = ListUtil.arrayToList(ChooserFieldValues);
private static final List  AcctgChooserFieldValuesVector = ListUtil.arrayToList(AcctgChooserFieldValues);
private static final List  PaymentHeaderFieldsVector = ListUtil.arrayToList(PaymentHeaderFields);
private static final List  PaymentHeaderFieldValuesVector = ListUtil.arrayToList(PaymentHeaderFieldValues);
private static final List  PaymentLineFieldsVector = ListUtil.arrayToList(PaymentLineFields);
private static final List  PaymentLineFieldValuesVector = ListUtil.arrayToList(PaymentLineFieldValues);
private static final List  ApproversAmountsVector = ListUtil.arrayToList(ApproversAmounts);
private static final List  PaymentHeaderChoosers = ListUtil.arrayToList(PaymentHeaderChooserFieldValues);
private static final List  PaymentLineChoosers = ListUtil.arrayToList(PaymentLineChooserFieldValues);
private static final List  ReqToPOCopyFieldValuesVector = ListUtil.arrayToList(ReqToPOCopyFieldValues);
private static final List  LineFieldDefaultingValuesVector = ListUtil.arrayToList(LineFieldDefaultingValues);
private static final List  AcctgFieldDefaultingValuesVector = ListUtil.arrayToList(AcctgFieldDefaultingValues);
private static final List  HeaderPCardFieldVector = ListUtil.arrayToList(HeaderPCardFieldValues);

public static void loadList()
{
  // Ariba 8.1: Replaced deprecated new List() Constructor with ListUtil.newVector();
  // The following 27 lines were modified accordingly.
  //rgiesen, 2/19/2004 Dev SPL #27 moved the definition of these vectors to be private static final
 /* RefAcctgTextFieldValuesVector = ListUtil.newVector(RefAcctgTextFieldValues);
  AcctgFieldsVector = ListUtil.newVector( AcctgFields);
  RefAcctgFieldsVector = ListUtil.newVector( RefAcctgFields);
  AcctgFieldValuesVector = ListUtil.newVector(AcctgFieldValues);
  ReqHeaderChooserFieldValuesVector = ListUtil.newVector(ReqHeaderChooserFieldValues);
  ReqLineChooserFieldValuesVector = ListUtil.newVector(ReqLineChooserFieldValues);
  HeaderFieldsVector = ListUtil.newVector(HeaderFields);
  HeaderFieldValuesVector = ListUtil.newVector(HeaderFieldValues);
  HeaderFieldValuesOnlyVector = ListUtil.newVector(HeaderFieldValues);
  LineFieldsVector = ListUtil.newVector(LineFields);
  LineFieldValuesVector = ListUtil.newVector(LineFieldValues);
  TestFieldsVector = ListUtil.newVector (TestFields);
  TestFieldValuesVector = ListUtil.newVector(TestFieldValues);
  ChooserFieldsVector = ListUtil.newVector (ChooserFields);
  ChooserFieldValuesVector = ListUtil.newVector (ChooserFieldValues);
  AcctgChooserFieldValuesVector = ListUtil.newVector(AcctgChooserFieldValues);
  PaymentHeaderFieldsVector = ListUtil.newVector(PaymentHeaderFields);
  PaymentHeaderFieldValuesVector = ListUtil.newVector(PaymentHeaderFieldValues);
  PaymentLineFieldsVector = ListUtil.newVector(PaymentLineFields);
  PaymentLineFieldValuesVector = ListUtil.newVector(PaymentLineFieldValues);
  ApproversAmountsVector = ListUtil.newVector(ApproversAmounts);
  PaymentHeaderChoosers = ListUtil.newVector(PaymentHeaderChooserFieldValues);
  PaymentLineChoosers = ListUtil.newVector(PaymentLineChooserFieldValues);
  ReqToPOCopyFieldValuesVector = ListUtil.newVector(ReqToPOCopyFieldValues);
  LineFieldDefaultingValuesVector = ListUtil.newVector(LineFieldDefaultingValues);
  AcctgFieldDefaultingValuesVector = ListUtil.newVector(AcctgFieldDefaultingValues);
  HeaderPCardFieldVector = ListUtil.newVector(HeaderPCardFieldValues); */

}
    public static List getRefAcctgTextFieldValues()
    {
        return RefAcctgTextFieldValuesVector;
    }
    public static List getAcctgFields()
    {
        return AcctgFieldsVector;
    }
    public static List getAcctgFieldValues()
    {
        return AcctgFieldValuesVector;
    }

    public static List getHeaderFields()
    {
        return HeaderFieldsVector;
    }
    public static List getHeaderFieldValues()
    {
        return HeaderFieldValuesVector;
    }
    public static List getHeaderFieldValuesOnly()
    {
        return HeaderFieldValuesOnlyVector;
    }
    public static List getLineFields()
    {
        return LineFieldsVector;
    }
    public static List getLineFieldValues()
    {
        return LineFieldValuesVector;
    }
    public static List getRefAcctgFields()
    {
        return RefAcctgFieldsVector;
    }
    public static List getAcctgChooserFieldValues()
    {
        return AcctgChooserFieldValuesVector;
    }
    public static List getReqHeaderChooserFieldValues()
    {
        return ReqHeaderChooserFieldValuesVector;
    }
    public static List getReqLineChooserFieldValues()
    {
        return ReqLineChooserFieldValuesVector;
    }
    public static List getTestFields()
    {
        return TestFieldsVector;
    }
    public static List getTestFieldValues()
    {
        return TestFieldValuesVector;
    }
    public static List getChooserFields()
    {
        return ChooserFieldsVector;
    }
    public static List getChooserFieldValues()
    {
        return ChooserFieldValuesVector;
    }
    public static List getPaymentHeaderFields()
    {
        return PaymentHeaderFieldsVector;
    }
    public static List getPaymentHeaderFieldValues()
    {
        return PaymentHeaderFieldValuesVector;
    }
    public static List getPaymentLineFields()
    {
        return PaymentLineFieldsVector;
    }
    public static List getPaymentLineFieldValues()
    {
        return PaymentLineFieldValuesVector;
    }
     public static List getApproversAmounts()
    {
        return ApproversAmountsVector;
    }
    public static List getPaymentHeaderChoosers()
    {
        return PaymentHeaderChoosers;
    }
    public static List getPaymentLineChoosers()
    {
        return PaymentLineChoosers;
    }
    public static List getReqToPOFields()
    {
        return ReqToPOCopyFieldValuesVector;
    }
    public static List getLineFieldDefaultingValues()
    {
        return LineFieldDefaultingValuesVector;
    }
    public static List getAcctgFieldDefaultingValues()
    {
        return AcctgFieldDefaultingValuesVector;
    }
    public static List getHeaderPCardFields()
    {
        return HeaderPCardFieldVector;
    }
}

