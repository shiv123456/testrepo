package config.java.ams.custom;

import ariba.base.fields.ValueSource;
import ariba.htmlui.approvableui.fields.*;

public class BuysenseARFMultipleAttachment extends ARFAttachments
{
	public static final String ClassName = "config.java.ams.custom.BuysenseARFMultipleAttachment";
    public String outputView(String group, ValueSource context)
    {
        return "config.java.ams.custom.BuysenseARVMultipleAttachment";
    }	

}
