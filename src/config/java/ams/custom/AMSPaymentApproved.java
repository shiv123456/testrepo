/*
    Copyright (c) 1970-1999 AMS, Inc.
    WA ST SPL # 475/Baseline Dev SPL # 229 - PaymentAccountings --> Accountings.PaymentAccountings
*/

/* 08/13/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 09/23/2003: Updates for Ariba 8.0 (Richard Lee) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/

package config.java.ams.custom;

// Ariba 8.0: Commented out the workflow and core imports; they aren't needed.
//import ariba.server.objectserver.workflow.*;
//import ariba.server.objectserver.core.*;
import ariba.base.core.ClusterRoot;
import ariba.base.core.BaseVector;
import ariba.base.core.BaseObject;
import java.math.BigDecimal;
import ariba.base.core.BaseId;
import ariba.base.core.Base;
import ariba.base.core.BaseSession;
import ariba.approvable.core.Approvable;

// Ariba 8.0: added the 5 import below
import ariba.base.fields.Action;
import ariba.util.core.PropertyTable;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.server.workflowserver.*;

/* TODO integrate this with other hooks
*/
/* Ariba 8.0: can not use ActionInterface and execute any more
public class AMSPaymentApproved implements ActionInterface
{
     public boolean execute( WorkflowState workflowState,
                            ClusterRoot cr,
                            WorkflowParameter[] parameters)
                            throws WorkflowException
    {
*/
public class AMSPaymentApproved extends Action
{
    // Ariba 8.0: replaced public boolean execute
    public void fire (ValueSource object,
                      PropertyTable parameters)
        throws ActionExecutionException
    {
        ClusterRoot cr = (ClusterRoot)object;

        Log.customer.debug("AMSPaymentApproved.execute called...");
        //Get the order with write lock.

        ClusterRoot order = (ClusterRoot)cr.getFieldValue("Order");
        BaseId refBaseId = (BaseId)order.getBaseId();
        BaseSession baseSession = Base.getSession();
        order = baseSession.objectForWrite(refBaseId);

        Log.customer.debug("Finished locking the order");

        //Get the order and payment lines.
        BaseVector polines = (BaseVector)order.getFieldValue("LineItems");
        BaseVector pvlines = (BaseVector)cr.getFieldValue("PaymentItems");

        //Update the order lines NumberPaid with PayingQuantity from payment
        //and decrease the NumberInPaymentProcess by PayingQuantity from payment
        for(int i=0;i<polines.size();i++)
        {
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            BaseObject item = (BaseObject)polines.get(i);
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            BaseObject paymentItem = (BaseObject)pvlines.get(i);
            BigDecimal payingQty = (BigDecimal)paymentItem.getFieldValue("CurPayingQuantity");
            BigDecimal inPorcessQty = (BigDecimal)item.getFieldValue("NumberInPaymentProcess");
            BigDecimal numberPaid = (BigDecimal)item.getFieldValue("NumberPaid");
            BigDecimal numberAccepted = (BigDecimal)item.getFieldValue("NumberAccepted");

            inPorcessQty=inPorcessQty.subtract(payingQty);
            numberPaid=numberPaid.add(payingQty);
            Log.customer.debug("The new Inprocess qty is %s",inPorcessQty);
            item.setFieldValue("NumberInPaymentProcess",inPorcessQty);
            item.setFieldValue("NumberPaid",numberPaid);
        }

        //Anup - Generic invocation of dynamically determined Client/Agency edits. - Dev SPL #173
        Boolean result = EditClassGenerator.invokeStatusEdits((Approvable)cr,
                                                              this.getClass().getName());
        if(!result.booleanValue())
        {
            Log.customer.debug("Validations Failed in " + this.getClass().getName());
            // Ariba 8.0: fire can not use return false
            //return false;
            return;
        }

        //Push the payment transaction followed immediately by the liquidation transaction.
        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        ariba.user.core.User RequesterUser = (ariba.user.core.User)cr.getDottedFieldValue("Requester");
        //rgiesen dev SPL #39
        //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,cr.getPartition());
        String clientName = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName");
        String clientID = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientID");
        String XMLClientTag = clientID.trim() + "_" + clientName.trim() ;

        AMSTXN.initMappingData();
        int rt=AMSTXN.push(XMLClientTag,
                           "PV",
                           (String)cr.getFieldValue("UniqueName"),
                           "NEW",
                           "RDY",
                           "A",
                           "0",
                           "",
                           cr,
                           "PaymentItems",
                           "Accountings.PaymentAccountings", cr.getPartition());
        Log.customer.debug("value of rt %s",rt);
        //add the payment object to the Payments vector on the order.

        /*
        //get a lock on the payment eform.
        BaseId refBaseId = (BaseId)cr.getBaseId();
        BaseSession baseSession = Base.getSession();
        cr = baseSession.objectForWrite(refBaseId);
        order.save();
        */

        BaseVector payments = ((BaseVector)order.getFieldValue("Payments"));
        Log.customer.debug("The vector is %s",payments);
        Log.customer.debug("The size of the vector is %s",payments.size());
        payments.add(cr.getBaseId());
        Log.customer.debug("After addelement");
        order.setFieldValue("Payments",payments);
        order.save();
        // Ariba 8.0: fire can not use return false
        //return true;
        return;
    }
}
