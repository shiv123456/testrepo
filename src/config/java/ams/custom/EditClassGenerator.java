/*
   Anup - A class for constructing client and agency edit classes based on Parameter.table
   setting for Client and source class invoking this class.
   May 2001
*/

/* 09/03/2003: Updates for Ariba 8.0 (David Chamberlain)
   Replaced all Util.integer()'s with Constants.getInteger()
   Replaced all Util.vector()'s with ListUtil.vector()
   Replaced all Util.getInteger()'s with Constants.getInteger()
*/
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
*/

package config.java.ams.custom;

import ariba.approvable.core.Approvable;

import java.util.List;
import ariba.util.core.*;
import ariba.base.core.*;
import ariba.util.log.Log;

public class EditClassGenerator
{
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));
    private static final Boolean NoError = new Boolean(true);

    public static List invokeHookEdits(Approvable approvable,
                                         String sourceClassName)
    {
		//rgiesen dev SPL #39
		Partition appPartition = approvable.getPartition();
        //String classExt = Base.getService().getParameter(Base.getSession().getPartition(), "Application.AMSParameters.ClientExt");
        String classExt = Base.getService().getParameter(appPartition, "Application.AMSParameters.ClientExt");
        // Ariba 8.1: changed List to ListUtil
        List warningVector = ListUtil.list();

        Log.customer.debug("EditClassGenerator, classExt is --> " + classExt);

        Log.customer.debug("EditClassGenerator, sourceClassName is --> " + sourceClassName);

        if (!(classExt == null) && !(classExt.trim().equals("")))
        {
            String clientClassName = (String)(classExt.trim() + "_" + (sourceClassName.substring(sourceClassName.lastIndexOf('.')+1)).trim());
            Log.customer.debug("EditClassGenerator, clientClassName is --> " + clientClassName);
            List result = (List)CustomClassRouter.routeHookRequest(approvable, "config.java.ams.custom.HookEdits." + clientClassName.trim(), "run");

            // Ariba 8.1: elementAt has been deprecated by get()
            int errorCode = ((Integer)result.get(0)).intValue();

            if(errorCode<0)
            {
                Log.customer.debug("Client Validations Failed --> returning error to UI");
                return result;
            }
            //Capture Warnings here if present
            else if (errorCode > 0)
            {
                Log.customer.debug("Client Validations Encountered a Warning --> returning error to UI");
                warningVector.clear();
                // 81->822                 warningVector = (Vector)result.clone();
                warningVector = ListUtil.cloneList(result);
            }

            //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
            //Partitioned User to obtain the extrinsic BuysenseOrg field value.
            //String clientName = (String)approvable.getDottedFieldValue("Requester.BuysenseOrg.ClientName.ClientName");
            ariba.user.core.User RequesterUser = (ariba.user.core.User)approvable.getDottedFieldValue("Requester");
            //rgiesen dev SPL #39
            //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
            ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,appPartition);
            String clientName = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName");

            String agencyClassName = (String)(classExt.trim() + "_" + clientName.trim() + "_" + (sourceClassName.substring(sourceClassName.lastIndexOf('.')+1)).trim());
            Log.customer.debug("EditClassGenerator, agencyClassName is --> " + agencyClassName);
            result = (List)CustomClassRouter.routeHookRequest(approvable,
                                                                "config.java.ams.custom.HookEdits." + agencyClassName.trim(), "run");
            // Ariba 8.0: elementAt() has been deprecated by get()
            errorCode = ((Integer)result.get(0)).intValue();

            if(errorCode<0)
            {
                Log.customer.debug("Agency Validations Failed --> returning error to UI");
                return result;
            }
            //Capture Warnings here if present
            else if (errorCode > 0)
            {
                Log.customer.debug("Agency Validations Encountered a Warning --> returning error to UI");
                warningVector.clear();
                // 81->822  warningVector = (Vector)result.clone();
                warningVector = ListUtil.cloneList(result);
            }

        }
        else
        {
            Log.customer.debug("Application.AMSParameters.ClassExt in Parameters.table is undefined, spaces or empty string");

        }

        // If warning is present, show it to the user
        if ( ! warningVector.isEmpty() )
        {
            return warningVector;
        }

        return NoErrorResult;
    }

    public static Boolean invokeStatusEdits(Approvable approvable,
                                            String sourceClassName)
    {
		//rgiesen dev SPL #39
		Partition appPartition = approvable.getPartition();
        //String classExt = Base.getService().getParameter(Base.getSession().getPartition(), "Application.AMSParameters.ClientExt");
        String classExt = Base.getService().getParameter(appPartition, "Application.AMSParameters.ClientExt");
        Log.customer.debug("EditClassGenerator, classExt is --> " + classExt);

        Log.customer.debug("EditClassGenerator, sourceClassName is --> " + sourceClassName);

        if (!(classExt == null) && !(classExt.trim().equals("")))
        {
            String clientClassName = (String)(classExt.trim() + "_" + (sourceClassName.substring(sourceClassName.lastIndexOf('.')+1)).trim());
            Log.customer.debug("EditClassGenerator, clientClassName is --> " + clientClassName);
            Boolean retValue = (Boolean)CustomClassRouter.routeStatusRequest(approvable, "config.java.ams.custom.HookEdits." + clientClassName.trim(), "run");

            if(!retValue.booleanValue())
            {
                Log.customer.debug("Client Validations returned false.");
                return retValue;
            }

            //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
            //Partitioned User to obtain the extrinsic BuysenseOrg field value.
            //String clientName = (String)approvable.getDottedFieldValue("Requester.BuysenseOrg.ClientName.ClientName");
            ariba.user.core.User RequesterUser = (ariba.user.core.User)approvable.getDottedFieldValue("Requester");
            //rgiesen dev SPL #39
            //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
            ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,appPartition);
            String clientName = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName");

            String agencyClassName = (String)(classExt.trim() + "_" + clientName.trim() + "_" + (sourceClassName.substring(sourceClassName.lastIndexOf('.')+1)).trim());
            Log.customer.debug("EditClassGenerator, agencyClassName is --> " + agencyClassName);
            retValue = (Boolean)CustomClassRouter.routeStatusRequest(approvable,
                                                                     "config.java.ams.custom.HookEdits." + agencyClassName.trim(), "run");

            if(!retValue.booleanValue())
            {
                Log.customer.debug("Agency Validations returned false.");
                return retValue;
            }

        }
        else
        {
            Log.customer.debug("Application.AMSParameters.ClassExt in Parameters.table is undefined, spaces or empty string");

        }

        return NoError;
    }

}

