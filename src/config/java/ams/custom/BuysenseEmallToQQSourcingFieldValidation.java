package config.java.ams.custom;
//Checks the field validity of ReqHeadCB5Value and BuysenseNonAribaAppUsers. If ReqHeadCB5Value is true we have to choose one user cannot submit without selecting the user.
import ariba.base.core.Base;
import ariba.base.core.BaseObject;
import ariba.base.core.ClusterRoot;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ConditionResult;
import ariba.base.fields.ValueInfo;
import ariba.util.core.Fmt;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseEmallToQQSourcingFieldValidation extends Condition
{
    private static final ValueInfo parameterInfo[];
    protected String             sResourceFile = "ariba.procure.core";
    private String                 msMessage     = null;
    public String sClassName = "BuysenseEmallToQQSourcingFieldValidation";
    
    public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
    {
        Log.customer.debug(sClassName +"::evaluateAndExplain() validation for ReqHeadCB5Value");
        try
        {
            if(!evaluate(value, params))
            {
                String retMsg = null;
                if(!StringUtil.nullOrEmptyOrBlankString(msMessage))
                {
                    retMsg = msMessage;
                    msMessage = null;
                    return new ConditionResult(retMsg);
                }
                else
                    return null;
            }
        }
        catch (ConditionEvaluationException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
    public boolean evaluate(Object value, PropertyTable params) throws ConditionEvaluationException
    {

        BaseObject obj = (BaseObject) params.getPropertyForKey("SourceObject");
        Log.customer.debug(sClassName + "::evaluate() SourceObject " + obj);
        boolean bValidSourcingBuyer = true;
        if (obj.instanceOf("ariba.purchasing.core.Requisition"))
        {

            Log.customer.debug(sClassName + "::evaluate() TransactionSource: " + obj.getFieldValue("TransactionSource"));
            if ((obj.getFieldValue("TransactionSource")) != null)
            {

                return true;
            }

            boolean bSendReqToADV = obj.getDottedFieldValue("ReqHeadCB3Value") == null ? false : ((Boolean) obj.getDottedFieldValue("ReqHeadCB3Value")).booleanValue();
            boolean bSendReqtoQQ = obj.getDottedFieldValue("ReqHeadCB5Value") == null ? false : ((Boolean) obj.getDottedFieldValue("ReqHeadCB5Value")).booleanValue();

            Log.customer.debug(sClassName + "::evaluate() bSendReqToADV: " + bSendReqToADV + ", bSendReqtoQQ: " + bSendReqtoQQ);

            if (bSendReqToADV || bSendReqtoQQ)
            {
                Object loBuysenseNonAribaAppUsers = obj.getFieldValue("BuysenseNonAribaAppUsers");

                Log.customer.debug(sClassName + "::evaluate() BuysenseNonAribaAppUsers: " + loBuysenseNonAribaAppUsers);

                if (loBuysenseNonAribaAppUsers == null)
                {
                    msMessage = Fmt.Sil(Base.getSession().getLocale(), sResourceFile, "QQSourcing");
                    return false;
                }
                else
                {
                    ClusterRoot loSourcingUser = (ClusterRoot) loBuysenseNonAribaAppUsers;
                    String sAppGroup = (String) loSourcingUser.getDottedFieldValue("AppGroup");
                    if (!loSourcingUser.getActive() && bSendReqToADV)
                    {
                        msMessage = Fmt.Sil(Base.getSession().getLocale(), sResourceFile, "InActiveADVSourcingBuyer");
                        return false; 
                    }
                    
                    if (!loSourcingUser.getActive() && bSendReqtoQQ)
                    {
                        msMessage = Fmt.Sil(Base.getSession().getLocale(), sResourceFile, "InActiveQQSourcingBuyer");
                        return false; 
                    }
                    if (!StringUtil.nullOrEmptyOrBlankString(sAppGroup))
                    {
                        //APPGROUP ex: 010200000000000000001100000000
                        // Quick Quote app id 08
                        // Full Advantage app-id 12
                        if (bSendReqToADV)
                        {
                            bValidSourcingBuyer = sAppGroup.contains("12") ? true : false;
                        }

                        if (bSendReqtoQQ)
                        {
                            bValidSourcingBuyer = sAppGroup.contains("08") ? true : false;
                        }

                        if (bValidSourcingBuyer)
                        {
                            return true;
                        }
                        else
                        {
                            msMessage = Fmt.Sil(Base.getSession().getLocale(), sResourceFile, "InvalidSourcingBuyerMsg");
                            return false;
                        }
                    }
                    else
                    {
                        Log.customer.debug(sClassName + " " + loSourcingUser + " object AppGroup field value is null or blank");
                    }
                }
            }
            Log.customer.debug(sClassName + "  bSendReqToADV: " + bSendReqToADV + ", bSendReqToADV: " + bSendReqtoQQ);
        }
        else
        {
            Log.customer.debug(sClassName + "::evaluate() object is NOT a Requisition");
            return bValidSourcingBuyer;
        }
        return bValidSourcingBuyer;
    }
    

    
    protected ValueInfo[] getParameterInfo()
    {
       return parameterInfo;
    }   
    static
    {
         parameterInfo = (new ValueInfo[] {new ValueInfo("SourceObject", 0)});
    }
}

