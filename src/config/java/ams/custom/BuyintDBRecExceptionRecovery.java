package config.java.ams.custom;

import java.sql.*;
import java.util.*;

public class BuyintDBRecExceptionRecovery extends BuyintDBRec
{
    private static final String EXCEPTION_RECOVERY = "EXCEPTION_RECOVERY";

    public BuyintDBRecExceptionRecovery(BuyintDBIO foDBIO)
    {
        super(foDBIO, EXCEPTION_RECOVERY);
    }

    public BuyintDBRecExceptionRecovery(BuyintDBIO foDBIO, Connection foConn)
    {
        super(foDBIO, foConn, EXCEPTION_RECOVERY);
    }

    protected static String getSeqSQL()
    {
        return "SELECT SEQ_EXCEPTION_RECOVERY_TIN.nextval from DUAL";
    }

   /**
    * Retrieves multiple recs based on the passed where clause,
    * upto the max specified. If no max is specified, all qualifying
    * records are returned.
    *
    */
    public BuyintDBRecExceptionRecovery[] getRecs(String fsWhereClause, int fiMax)
    {
        return getRecs(fsWhereClause, fiMax, "TIN");
    }

   /**
    * Retrieves multiple records based on where clause passed
    *
    */
    public BuyintDBRecExceptionRecovery[] getRecs(String fsWhereClause, int fiMax, String fsOrderBy)
    {
        Statement loStmt = null;
        ResultSet loResult = null;
        String lsQuery = null;
        BuyintDBRecExceptionRecovery[] loArray = null;

        try
        {
            this.checkConnection();
            lsQuery = getSelectString(msTableName) + fsWhereClause;

            if (fiMax > 0)
            {
               lsQuery += " and rownum <= " + String.valueOf(fiMax);
            }

            if (fsOrderBy != null)
            {
               lsQuery += " order by " + fsOrderBy;
            }

            debug("About to execute query: " + lsQuery);
            loStmt = moConn.createStatement();
            loResult = loStmt.executeQuery(lsQuery);
            loArray = getResultSetAsInfoArray(loResult);
            debug("Query execution complete ... returned array of length " + loArray.length);
        }
        catch(SQLException loEx1)
        {
            loEx1.printStackTrace();
            // no point throwing exception or calling ExceptionHandler at this time
        }
        finally
        {
           try
           {
               if (loResult != null)
               {
                  loResult.close();
               }
               if (loStmt != null)
               {
                  loStmt.close();
               }
           }
           catch (SQLException loEx2)
           {
              // nothing to do
           }
        }
        return loArray;
    }

    /**
     * Returns the ResultSet as an Info Array. If ResultSet is empty, returns null
     *
     * @param rs ResultSet to be converted
     * @return BuyintDBRecExceptionRecovery[]
     */
    public BuyintDBRecExceptionRecovery[] getResultSetAsInfoArray(ResultSet results) throws SQLException {
        Vector vInf = getResultSetAsInfoVector(results);
        int elements = vInf.size();
        if (elements > 0) {
            BuyintDBRecExceptionRecovery[] infoArray = new BuyintDBRecExceptionRecovery[elements];
            for (int i=0; i < elements ; i++)
            {
                infoArray[i]= (BuyintDBRecExceptionRecovery)vInf.elementAt(i);
            }
            return infoArray;
        }
        else
        {
            return new BuyintDBRecExceptionRecovery[0];
        }
    }
   /**
    * Returns the ResultSet as a Vector of BuyintDBRec.
    *
    * @param rs ResultSet to be converted
    * @return Vector
    */
    public Vector getResultSetAsInfoVector (ResultSet results) throws SQLException {
        ResultSetMetaData rsMeta = results.getMetaData();
        Vector vInf = new Vector();
        BuyintDBRecExceptionRecovery inf;

        int cols = rsMeta.getColumnCount();          // initialize nbr of columns
        String colsValue, colsName;
        while (results.next())
        {
            inf = new BuyintDBRecExceptionRecovery(moDBIO, moConn);
            for (int j=1; j <= cols; j++)
            {   // database column identifier is 1-based
                colsName = rsMeta.getColumnLabel(j);
                colsValue = getColValueAsString(results, rsMeta, j);
                inf.setValue(colsName,colsValue);
            }
            vInf.addElement(inf);
        }
        return vInf;
    }

}
