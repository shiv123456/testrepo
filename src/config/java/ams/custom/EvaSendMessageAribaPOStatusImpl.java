package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class EvaSendMessageAribaPOStatusImpl extends EvaSendMessageAribaImpl {
    
    private static String msCN = "EvaSendMessageAribaPOStatusImpl";
    private static String queueName = null;
    
    protected String getQueueName() throws Exception
    {
        if(!StringUtil.nullOrEmptyOrBlankString(queueName))
        {
            return queueName;
        }

        // for ariba get the Endpoint from P.table
        Log.customer.debug(msCN + ": fetching QueueName from P.table");

        Partition moPart = null;
        moPart = Base.getService().getPartition("pcsv");
        if (moPart == null)
        {
            moPart = Base.getSession().getPartition();
        }

        queueName  = (String) Base.getService().getParameter(moPart, "Application.AMSParameters.EvaMessageQueueNamePOStatus");
        return queueName;        
    }
}
