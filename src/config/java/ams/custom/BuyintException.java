//************************************************************************************
// Name:             BuyintException
// Description: Exception Object For Integration.
// Author:  David Chamberlain
// Date:    July 30, 2004
// Revision History:
//
//  Date              Responsible            Description
//  ----------------------------------------------------------------------------------
//  07/30/2004        David Chamberlain      Initial Version. Ariba 8.1 Upgrade
//											 Dev SPL 88 and 89.
//
//
// Copyright 2001 by American Management Systems, Inc.,
// 4050 Legato Road, Fairfax, Virginia, U.S.A.
// All rights reserved.
//
// This software is the confidential and proprietary information
// of American Management Systems, Inc. ("Confidential Information"). You
// shall not disclose such Confidential Information and shall use
// it only in accordance with the terms of the license agreement
// you entered into with American Management Systems, Inc.
//*************************************************************************************

/* DETAILED DESCRIPTION:
   This is the exception object for Integration. This exception will be thrown in
   situations where custom integration code has created or brought about an error
   in processing. That is, areas of custom coded processing that something goes wrong
   but no other type of exception is thrown. For instance, if the XMLFactory is
   generating an XML but can't find a template. No java exception would be thrown, but
   the error situation would be accounted for and BuyintException will be thrown.
*/

package config.java.ams.custom;

public class BuyintException extends Exception implements BuyintConstants
{
	//Continue exception types...
	private final int EXCPT_DEFAULT = 0;
    public static final int EXCPT_XMLGEN_ERROR = 10;
    public static final int EXCPT_DBIO_ERROR = 20;

	private static int miExceptionType;

	//This Exception is thrown by Integration modules
	BuyintException()
	{
		super();
		miExceptionType = this.EXCPT_DEFAULT;
	}

	//This Exception is thrown by Integration modules
	BuyintException(int fiExceptionType)
	{
		super();
		miExceptionType = fiExceptionType;
	}

	//This Exception is thrown by Integration modules
	BuyintException(String fsException, int fiExceptionType)
	{
		super(fsException);
		miExceptionType = fiExceptionType;
	}

	//This Exception is thrown by Integration modules
	BuyintException(String fsException)
	{
		super(fsException);
		miExceptionType = this.EXCPT_DEFAULT;
	}

	//Get the exception type
	public int getType()
	{
		return miExceptionType;
	}

	//Set the exception type
	public void setType(int fiType)
	{
		miExceptionType = fiType;
	}
}
