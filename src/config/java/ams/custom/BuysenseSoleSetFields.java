package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseSoleSetFields extends Action
{
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
        Log.customer.debug("fire method called");
        Approvable loAppr = null;        
        if(valuesource!= null && valuesource instanceof Approvable)
        {
            loAppr = (Approvable)valuesource;                    
            loAppr.setDottedFieldValue("ProcurementDescription"," ");
            loAppr.setDottedFieldValue("OnlyMeetPurchasingNeed"," ");
            loAppr.setDottedFieldValue("OnlySource"," ");
            loAppr.setDottedFieldValue("ReasonablePrice"," ");
            loAppr.setDottedFieldValue("NoncompetitiveNego"," ");
            loAppr.setDottedFieldValue("Note","Agencies and institutions are delegated authority to make contract award only after the appropriate approval has been obtained on this form.  This Sole Source form number must be entered as a comment on the final eVA Requisition Purchase using noncompetitive negotiation.");
            loAppr.setDottedFieldValue("AttachFileText","If necessary attach file on next screen.");
            loAppr.setDottedFieldValue("NIGPLookup","https://logi.epro.cgipdc.com/External/rdPage.aspx?rdReport=Public.Reports.Report9004_Data");
        }
    }
}
