package config.java.ams.custom;

import ariba.base.core.Partition;
import ariba.base.core.aql.*;
import ariba.base.fields.ValueSource;
import ariba.procure.server.CustomCatalog;
import ariba.util.log.Log;
import ariba.base.core.LongString;
import ariba.common.core.CommonSupplier;

// eVA Dev SPL # 22 - anup - Added a false condition to where clause.

/* 08/13/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

/*rgiesen Jan 16, 2004 - Dev SPL#3
For Ariba 8.1, this program was significantly modified/cleaned up
It still functions in the same way, but changes were made to it for accessing
the user object correctly, as well as general clean-up of old code
*/

public class BuySenseCustomCatalog implements CustomCatalog
{

    public BuySenseCustomCatalog()
    {
    }

    public AQLCondition customCatalogConstraint(AQLQuery catalogQuery, Partition partition, ValueSource user,
            ValueSource req)
    {

        Log.customer.debug("BuySenseCustomCatalog - Start");

        //START::Added by SRINI for CSPL-1768/CSPL-1803 not to display any catalogs for Supplier Users
        if (user != null)
        {
            /* Pavan: In 9r1, user ValueSource object is of ariba.common.core.User, which was ariba.user.core.User
               Modified the code accordingly.*/
            //ariba.user.core.User loEffectiveUser = (ariba.user.core.User) user;
            ariba.common.core.User commonUser = (ariba.common.core.User) user;
            ariba.user.core.User loEffectiveUser = commonUser.getUser();
            //check whether user is a supplier user
            if (loEffectiveUser.getOrganization() instanceof CommonSupplier)
            {
                //Create a dummy/junk filter so that system will not return any catalogs for supplier users
                String lsHardWhereClause = "Filter2 IN ('EndaroMahanubhavulu-dummy')";
                Log.customer.debug("BuySenseCustomCatalog::returning supplier user condition");
                return AQLCondition.parseCondition(lsHardWhereClause);
            }
        }
        //END::Added by SRINI for CSPL-1768/CSPL-1803 not to display any catalogs for Supplier Users

        ValueSource catalogController = null;
        
        /* Pavan: In 9r1, Users can directly go to Catalog screen (instead of Title) while creating Requisition, hence below
         if condition is modified.*/
        if (user != null)
        {
            //Ariba 8.1 get the partitioned user
            /*ariba.user.core.User PreparerUser = (ariba.user.core.User) req.getDottedFieldValue("Preparer");
            ariba.common.core.User PreparerPartitionUser = ariba.common.core.User.getPartitionedUser(PreparerUser,
                    partition);*/
            ariba.common.core.User partitionUser = (ariba.common.core.User) user;
            if (partitionUser == null)
            {
                Log.customer.debug("BuySenseCustomCatalog - Could not get the User");
                //A return value of null means that there is no constraint to add.
                return null;
            }

            catalogController = (ValueSource) partitionUser.getDottedFieldValue("BuysenseCatalogController");
        }
        else
        {
            Log.customer.debug("BuySenseCustomCatalog - the user is null.  No filter.");
            //A return value of null means that there is no constraint to add.
            return null;
        }

        if (catalogController == null)
        {
            Log.customer.debug("BuySenseCustomCatalog - The user's catalog controller is null  No filter.");
            //A return value of null means that there is no constraint to add.
            return null;
        }

        Log.customer.debug("BuySenseCustomCatalog - got the BuysenseCatalogController: " + catalogController);

        LongString lsWhereClause = (LongString) catalogController.getFieldValue("WhereClause");

        /* rgiesen Dev SPL#3 The WhereClause field within the BuysenseCatalogController might be something like the following 4 examples:
         "Filter1 IN ('xpedx_64601-01_eVAcatalog')"
         "Filter2 IN ('ContractAll')"
         "Filter2 IN ('ContractAll') OR Filter3 IN ('ContractAllPcard')"
         "Filter1 IN ('VehicleT','aaa_cova','abswelding_eVAcatalog') OR Filter2 IN ('VIB') OR Filter3 IN ('Pcard','VDCPcard') "
        The WhereClause field could be quite long.  It is stored in the database as a LongString, which
         breaks the string into 255 character pieces.  So, we retrieve it as a LongString and then
         convert it to a regular string to make up our whereClause.
        */

        if (lsWhereClause == null)
        {
            Log.customer.debug("BuySenseCustomCatalog - The catalog controller whereClauseis null  No filter.");
            //A return value of null means that there is no constraint to add.
            return null;
        }

        String whereClause = new String();

        whereClause = lsWhereClause.string();
        //eVA Dev SPL # 22 - added next line.
        /*rgiesen Dev SPL#3 According to Anup, this line was added for performance, and Ariba
         told them to do this.  However, now in 8.1, it is complaining about the left side ("1") of the
         condition has to be a valid catalog field.
         Therefore, for Ariba 8.1 this was removed, and performance will be re-evaluated in 8.1
         If necessary, there is a buildFalse() method for AQLCondition which should
         probably be used instead of 1=2.
        */
        //whereClause = whereClause.concat(" OR 1=2");
        Log.customer.debug("BuySenseCustomCatalog - the where clause is: " + whereClause);

        return AQLCondition.parseCondition(whereClause);
    }

    public Object customCatalogHash(Partition partition, ValueSource user, ValueSource req)
    {
        //rgiesen Dev SPL#3 This method will be deprecated in the near future, so do not use it.
        //The warning message for this can be ignored
        //If, in the future, they deprecate this method, then it can be removed
        return null;
    }

    public void initialize()
    {
    }
}