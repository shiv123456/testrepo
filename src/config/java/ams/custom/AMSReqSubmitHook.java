/*
    Copyright (c) 1996-1999 Ariba/Buysense, Inc.
    Falahyar March 2000
    ---------------------------------------------------------------------------------------------------

   rlee, 10/14/2004: UAT SPL 26 and 36 - Errors from PreEncumbrance logic not handling null values from Ariba 7.1


   09/03/2003: Updates for Ariba 8.0 (David Chamberlain)
   Replaced all Util.integer()'s by Constants.getInteger()
   Replaced all Util.vector()'s by ListUtil.vector()
   Replaced import of ApprovableHook to it's deprecated API

   03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
   03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method


   rlee, Aug 2004: Dev SPL 84 - Implement ERP PreEncumbrance functionality.
 */

package config.java.ams.custom;


import ariba.procure.core.LineItemProductDescription;
import ariba.procure.core.Procure;
import ariba.purchasing.core.DirectOrder;
import ariba.purchasing.core.Requisition;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.Comment;
import java.util.List;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.ordering.OrderMethod;
import ariba.purchasing.core.ordering.OrderMethodException;
import ariba.base.core.*;
import ariba.common.core.*;
// Ariba 8.1: Added new ariba.user.core.User class.
import ariba.user.core.User;
import ariba.util.core.*;
// Ariba 8.0: Replaced deprecated ApprovableHook
import ariba.approvable.core.ApprovableHook;
//import ariba.approvable.core.ApprovableHook;
import ariba.util.log.Log;
// Ariba 8.0: Deprecations, methods have changed packages.
import ariba.util.core.ClassUtil;
import ariba.util.core.Constants;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;
import ariba.util.formatter.BooleanFormatter;
import config.java.ams.custom.BuysenseRulesEngine;
import java.util.regex.*;
import java.util.*;

//81->822 changed Vector to List
public class AMSReqSubmitHook implements ApprovableHook, BuyintConstants
{

    // Ariba 8.0: Replaced deprecated method
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));

    Boolean b1,b2,b3;

    // Ariba 8.1: List::List() is deprecated by ListUtil::newVector()
    private List warningVector = ListUtil.list();
    public  static List lineItemsChecked = ListUtil.list();
    public List retVector;
    public List valVector;
    private static         Comment       loComment               = null;
    private static         String        errorStringTable        = "ariba.procure.core";
    private static         String        sErrorMessage           = null;
    private static         String        sensitiveData           = "SensitiveDataMsg";
    private static         String        patternToMatch          = "PatternToCheck";
    private static         String        orderConfCheckedErr     = "BuysenseOrderConfCheckedErr";
    private static         String        orderConfCheckedMultiErr= "BuysenseOrderConfCheckedMultiErr";
    private static         String        orderConfUncheckedErr   = "BuysenseOrderConfUncheckedErr";
    private static         String        orderConfUncheckedMultiErr  = "BuysenseOrderConfUncheckedMultiErr";
    private static         String        orderConfComment        = "BuysenseOrderConfComment";
    public  static         Comment       oReplycomment           = null;
    public  static         String        lsCommentString         = null;
    public  static         LongStringElement lsCommentLongString = null;
    private static         BaseVector    bvCommentsString        = null;
    public  static         List          llCommentString         = ListUtil.list();
    private static         String        lsLeftBrace             = "(";
    private static         String        lsRightBrace            = ")";
    private static         String        lsDotSpace              = " ";
    public  static         int           lineNo                  = 0;
    public  static         boolean       lbIsLineVerified        = false;

    public List run (Approvable approvable)
    {

        Log.customer.debug("AMSReqSubmitHook.java called");

        //SRINI: Added for CSPL-7067 - To align with Mass Edit Functionality
        alignMassEditFunctionality((Requisition) approvable);
        //SRINI: Ended for CSPL-7067 - To align with Mass Edit Functionality
     
        //SRINI: START: SEV changes for SupplierLocation
        setRegistrationType((Requisition) approvable);
        //SRINI: END: SEV changes for SupplierLocation

        // CSPL-593 (Manoj) Nullify the objects with n/a ERP value
        // validate PR for each BSO fields
        performFieldValidation(approvable);
        boolean activeFlag;


        /* CSPL-2409, Pavan Aluri, 24-Jan-2011
         * Explanation: validate for expired/inactive lines*/

        retVector = isValidCatalogLine(approvable);
        if (((Integer)retVector.get(0)).intValue() == -1)
        {
           return retVector;
        }

        // BEGIN CSPL-5525 'BuysenseOrderCOnfirm' check JB
        retVector = checkBuysenseOrderConfirm(approvable);
        if (((Integer)retVector.get(0)).intValue() == -1)
        {
           return retVector;
        }
        // END CSPL-5525 'BuysenseOrderCOnfirm' check JB 
        
        // Validate Supplier/Location
        retVector = performSSLValidations(approvable);
        if (((Integer)retVector.get(0)).intValue() == -1)
        {
            return retVector;
        }
        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        //ClusterRoot bso = (ClusterRoot)approvable.getDottedFieldValue("Requester.BuysenseOrg");
        ariba.user.core.User RequesterUser = (ariba.user.core.User)approvable.getDottedFieldValue("Requester");
        //rgiesen dev SPL #39
        //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,approvable.getPartition());
        ClusterRoot bso = (ClusterRoot)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg");

        if (bso == null)
            return ListUtil.list(Constants.getInteger(-1),
                                     "BuysenseOrg for a requisition must not be blank.");

        activeFlag = bso.getActive();
        if (!activeFlag)
            return ListUtil.list(Constants.getInteger(-1),
                                     "BuysenseOrg for a requisition must be valid.");

        ClusterRoot bsoClientObject = (ClusterRoot)bso.getDottedFieldValue("ClientName");
        // Ariba 8.0: Replaced deprecated method
        if (bsoClientObject == null)
            return ListUtil.list(Constants.getInteger(-1),
                                     "ClientName for your BuysenseOrg must not be blank.");

        activeFlag = bsoClientObject.getActive();
        // Ariba 8.0: Replaced deprecated method
        if (!activeFlag)
            return ListUtil.list(Constants.getInteger(-1),
                                     "ClientName for your BuysenseOrg must be valid.");

        String clientUniqueName1 = (String)bso.getDottedFieldValue("ClientName.UniqueName");
        // Ariba 8.0: Replaced deprecated method
        if (clientUniqueName1 == null)
            return ListUtil.list(Constants.getInteger(-1),
                                     "ClientName/UniqueName for a BuysenseOrg must not be blank.");

        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        //ClusterRoot bso = (ClusterRoot)approvable.getDottedFieldValue("Requester.BuysenseOrg");
        ClusterRoot clientObject = (ClusterRoot)RequesterPartitionUser.getDottedFieldValue("ClientName");
        // Ariba 8.0: Replaced deprecated method
        if (clientObject == null)
            return ListUtil.list(Constants.getInteger(-1),
                                     "ClientName for the requester must not be blank.");

        activeFlag = clientObject.getActive();
        // Ariba 8.0: Replaced deprecated method
        if (!activeFlag)
            return ListUtil.list(Constants.getInteger(-1),
                                     "ClientName for a requester must be valid.");
        String clientUniqueName2 = (String)clientObject.getFieldValue("UniqueName");
        // Ariba 8.0: Replaced deprecated method
        if (clientUniqueName2 == null)
            return ListUtil.list(Constants.getInteger(-1),
                                     "ClientName/UniqueName for a requester must not be blank.");

        // Ariba 8.0: Replaced deprecated method
        if (!clientUniqueName1.equals(clientUniqueName2))
            return ListUtil.list(Constants.getInteger(-1),
                                     "BuysenseOrg/ClientName objects for the requester do not match.");


        //Set the changed flag on the requisition to be false at this point.

        approvable.setFieldValue("Changed",new Boolean(false));
        Log.customer.debug("Set the Changed Flag to be : %s",
                       approvable.getFieldValue("Changed"));


        //Joe Perry, July 2001
        //Call the Validation Rule engine to validate all chooser field selections
        //if we get a -1 that means we failed the validation. so we return without proceeding

        BuysenseValidationEngine eVAValEng = new BuysenseValidationEngine();

        valVector = eVAValEng.runReqHeaderValidation(approvable);
        // Ariba 8.0: List::elementAt(int) is deprecated by List::get(int)
        if (((Integer)valVector.get(0)).intValue() == -1)
        {
            return valVector;
        }

        //Anup - Generic invocation of dynamically determined Client/Agency edits. - Dev SPL #173
        List result = EditClassGenerator.invokeHookEdits(approvable,
                                                           this.getClass().getName());
        // Ariba 8.0: List::elementAt(int) is deprecated by List::get(int)
        int errorCode = ((Integer)result.get(0)).intValue();
        Log.customer.debug("Validations in " + this.getClass().getName() + ", error Code --> %s",errorCode);
        if(errorCode<0)
        {
            Log.customer.debug("Validations Failed in " + this.getClass().getName() + " - returning error to UI");
            return result;
        }
        //Capture Warnings here if present
        else if (errorCode > 0)
        {
            Log.customer.debug("Validations encountered a Warning in " + this.getClass().getName() + " - returning error to UI");
            warningVector = ListUtil.cloneList(result);
        }


        // BEGIN WA ST SPL#179: Check for approvers in approval flow
        BaseVector ApprovalRequests = (BaseVector)approvable.getFieldValue("ApprovalRequests");
        boolean isApprovalRequest = false;

        if (ApprovalRequests.size() > 0)
            isApprovalRequest = true;

        // retrieve line items from RX
        BaseVector ReqLineItems = (BaseVector)approvable.getFieldValue("LineItems");

        // ST SPL # 367 - Modified amount logic to pick up cents and modified error messages
        // If there are no approvers in the approval flow check the line items for error situations
        if(!isApprovalRequest)
        {
            // Ariba 8.1: BaseVector::count() is deprecated by BaseVector::size()
            for (int i=0; i<(ReqLineItems.size()); i++)
            {
                // Ariba 8.0: List::elementAt(int) is deprecated by List::get(int)
                ReqLineItem RXLineItems =(ReqLineItem)ReqLineItems.get(i);

                // If the Supplier is null and no approvers are in the approval flow,
                // output a user error to that affect.
                if (RXLineItems.getFieldValue("Supplier") == null)
                {
                    // Ariba 8.0: Replaced deprecated method
                    return ListUtil.list(Constants.getInteger(-1),
                                             "Cannot submit this requisition. " +
                                             "It contains at least one line " +
                                             "item with no supplier. Please click the " +
                                             "approval flow tab to verify that approvers " +
                                             "are present. If you are the final approver, " +
                                             "please complete the appropriate line item supplier.");
                }

                        /* 01/16/02 - labraham - eVA ST SPL#162: Commented out the code below that checks if there
                                                 is a zero dollar line item.
                        // BEGIN WA ST SPL#128
                        // Output a user error if there is at least one zero dollar line item and no
                        // approvers in the approval flow.
                        BigDecimal amount = (BigDecimal)RXLineItems.getDottedFieldValue("Amount.Amount");
                        double amountValue = amount.doubleValue();

                        if (amountValue <= 0)
                        {
                            // Ariba 8.0: Replaced deprecated method
                            return ListUtil.vector(Constants.getInteger(-1), "Cannot submit this requisition. " +
                                                                             "It contains at least one " +
                                                                             "zero dollar line item. Please click the " +
                                                                             "approval flow tab to verify that approvers " +
                                                                            "are present. If you are the final approver, " +
                                                                            "please complete the appropriate line item amount.");
                        }
                        // END WA ST SPL#128
                        */
            }
        }
        // END WA ST SPL#179


        //Call the Business Rule engine to validate all chooser field selections
        //if we get a -1 that means we failed the validation. so we return without proceeding
        retVector = BuysenseRulesEngine.runReqFilteringRules(approvable);
        // Ariba 8.0: List::elementAt(int) is deprecated by List::get(int)
        if (((Integer)retVector.get(0)).intValue() == -1)
        {
            return retVector;
        }

        // first retrieve client name
        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        String clientName = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.Name");
        //String agency = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.FieldDefault1.Name");
        // Ariba 8.0: Replaced deprecated method
        if (clientName==null)
             return ListUtil.list(Constants.getInteger(-1),
                        "Cannot do Submit because the User is not assosiated with a BuysenseOrg");

        if (approvable instanceof ariba.purchasing.core.Requisition)
        {
            /*
             * rlee: In case a ReqIntTxn was sent and approved, and a cancel should have been sent to ERP
             * due to withdraw, Edit, or Deny by other approvers; however, something happen and a
             * cancel was never sent; so this is the chance to sent out a cancel ReqIntTxn to ERP.
             */

            Requisition loReq = (ariba.purchasing.core.Requisition)approvable;
            Boolean lbPreEncumbered = (Boolean)loReq.getFieldValue("PreEncumbered");
            String lsPECheck = (String) loReq.getFieldValue("PreEncumbranceStatus");

            if( StringUtil.nullOrEmptyOrBlankString(lsPECheck))
            {
                loReq.setFieldValue("PreEncumbranceStatus", STATUS_ERP_READY);
                lsPECheck = STATUS_ERP_READY;
            }

            if(StringUtil.nullOrEmptyOrBlankString(BooleanFormatter.getStringValue(lbPreEncumbered)))
            {
                loReq.setFieldValue("PreEncumbered", new Boolean(false));
                lbPreEncumbered = new Boolean(false);
            }
            // CSPL-912 Should be sending PCNCL and not CNCL in this case;
            if(lsPECheck.equals(STATUS_ERP_APPROVE) && lbPreEncumbered.booleanValue())
            {
                Log.customer.debug("Sending out a Cancel ReqIntTxn to ERP.");
                BuyintXMLFactory.submit(TXNTYPE_PREENC_CANCEL, loReq);
                BaseObject loReqObject = (BaseObject) loReq;
                String lsReqUN = (String) loReq.getDottedFieldValue("UniqueName");
                String lsRecordType = "PreEncCRecord";
                Partition loPartition = loReq.getPartition();
                ariba.common.core.User loAribaSystemUser = Core.getService().getAribaSystemUser(loPartition);
                BuysenseUtil.createHistory(loReqObject, lsReqUN,  lsRecordType, null, loAribaSystemUser);
                loReq.setFieldValue("PreEncumbranceStatus", STATUS_ERP_CANCELLED);
                loReq.setFieldValue("PreEncumbered", new Boolean(false));
            }

        }

        // If warning is present, show it to the user
        if ( ! warningVector.isEmpty() )
        {
            return warningVector;
        }


        return NoErrorResult;
    }
    /***********************************************************************************************
     *
     * CSPLU-52 add PO Category Ad-Hoc Supplier Warning
     * After looping through all the line items, this CheckVendorPay method will
     * be called. If any one of the line items has a newly added supplierlocation, the lbIsAdhoc
     * will be true. If the vendorPay has been set to true or false AND user/approver decides to
     * delete the newly added supplierlocation afterwards, then VendorPay field will be reset to "null".
     * See the first "if" statement below.
     * Rest of the logic is to issue error statement if the PO category selected does not match
     * the selected VendorPay value. See CSPLU-52 SOW for more details.
     *
     **********************************************************************************************/
    public String CheckVendorPay(boolean lbIsAdhoc, Requisition loApprovable, String lsMessage)
    {
        Log.customer.debug("Calling CheckVendorPay method.");
        Requisition loReq = loApprovable;
        if(!lbIsAdhoc && loReq.getFieldValue("VendorPay") != null)
        {
            loReq.setFieldValue("VendorPay", null);
            return lsMessage;
        }

     if(lbIsAdhoc)
     {
        if(loReq.getFieldValue("VendorPay") == null)
        {
           lsMessage += Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","VendorPayNull");
           return lsMessage;
        }
        boolean lbVendorPay = ((Boolean)loReq.getFieldValue("VendorPay")).booleanValue();
        String lsPOCatValue = (String) loReq.getDottedFieldValue("ReqHeadFieldDefault4.Name");
        if(lbVendorPay)
        {
            if ( StringUtil.nullOrEmptyOrBlankString(lsPOCatValue) || !((lsPOCatValue.trim()).endsWith("2")) )
            {
                return lsMessage;
            }
            else
            {
                if(StringUtil.nullOrEmptyOrBlankString(lsMessage))
                    lsMessage = "";
                lsMessage += Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","VendorPayError");
                return lsMessage;
            }
        }
        else
        {
            if (lsPOCatValue != null && (lsPOCatValue.trim()).endsWith("2"))
            {
                return lsMessage;
            }
            else
            {
                if(StringUtil.nullOrEmptyOrBlankString(lsMessage))
                    lsMessage = "";
                lsMessage += Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","AgencyPayError");
                return lsMessage;
            }
        }
     }
        return lsMessage;
    }
    public List performSSLValidations(Approvable loApprovable)
    {
       BaseVector loLineItems = (BaseVector)loApprovable.getFieldValue("LineItems");
       SupplierSupplierLocationValidation loSSLValidations = new SupplierSupplierLocationValidation();
       PropertyTable loTable = new PropertyTable();
       String lsMessage = null;
       String lsAdapterSource = null;
       boolean lbIsAdhoc = false;
       Requisition loReq = (Requisition) loApprovable;

       for (int i=0; i<(loLineItems.size()); i++)
       {
          ReqLineItem loLineItem =(ReqLineItem)loLineItems.get(i);
          lsAdapterSource = (String) loLineItem.getDottedFieldValue("SupplierLocation.AdapterSource");
          if( StringUtil.nullOrEmptyOrBlankString(lsAdapterSource))
          {
             lbIsAdhoc = true;
          }

          loTable.setPropertyForKey("TargetValue", loLineItem);
          loTable.setPropertyForKey("TargetValue1", "SupplierField");
          if (! loSSLValidations.evaluate(loLineItem.getSupplier(), loTable))
          {
             if (lsMessage == null)
             {
                lsMessage = "Error :";
             }
             lsMessage += " Line # " + (i + 1) + " : " + loSSLValidations.message;
             continue;
          }
          loTable.setPropertyForKey("TargetValue1", "LocationField");
          if (! loSSLValidations.evaluate(loLineItem.getSupplierLocation(), loTable))
          {
             if (lsMessage == null)
             {
                lsMessage = "Error :";
             }
             lsMessage += " Line # " + (i + 1) + " : " + loSSLValidations.message;
          }
       }
       /********************************************************************************************************
       *
       * The following line is commented in accordance with CSPL-3741. COVA has decided to no longer
       * check for Ad-hoc line items in the requisition. Thus the check for Ad-hoc line items
       * and subsequently the validation of PO Category value against the VendorPay value is no longer required.
       *
       *********************************************************************************************************/
      //lsMessage = CheckVendorPay(lbIsAdhoc, loReq, lsMessage);

       if (lsMessage != null)
       {
          return ListUtil.list(Constants.getInteger(-1), lsMessage);
       }

       return NoErrorResult;
    }

    /**
     * CSPL-593 (Manoj) 10/03/08
     * @param appr
     * @explanation performing field validation for each BSO fields
     */

    /**
     * CSPL-752 (Srini) 11/30/08
     * @param appr
     * @Added additional parameters to nullifyObj method to get field values dynamically
     */

    public void performFieldValidation(Approvable appr)
	{
		Requisition objRequisition = null;
		if (appr != null && appr instanceof Requisition)
			objRequisition = (Requisition) appr;
		if(objRequisition != null)
		{
			Log.customer.debug(" Req Object: %s",objRequisition);
			String sBuysenseClient = (String)objRequisition.getDottedFieldValue("LineItems[0].ClientName.UniqueName");
			Log.customer.debug(" Client UniqueName : %s",sBuysenseClient);
			nullifyObj("HeaderDetailsEditable",FieldListContainer.getHeaderFieldValuesOnly(),FieldListContainer.getHeaderFields(),objRequisition,sBuysenseClient);
			nullifyObj("LineItemGeneralFields",FieldListContainer.getLineFieldValues(),FieldListContainer.getLineFields(),objRequisition,sBuysenseClient);
			nullifyObj("ProcureAccountingFields",FieldListContainer.getAcctgFieldValues(),FieldListContainer.getAcctgFields(),objRequisition,sBuysenseClient);
		}
	}

    /**
     * CSPL-593 (Manoj) 1/03/08
     * @param Group
     * @param fields
     * @param objAppr
     */

    /**
     * CSPL-752 (Srini) 11/30/08
     * @param appr
     * @Added additional parameters to get field values dynamically. Added null check if(objAppr.getDottedFieldValue(sFullFieldName) != null)
     * so that if getDottedFieldValue from obj is null there is no need to do further process.
     */

	public void nullifyObj(String Group,List fields,List getFields,Approvable objAppr,String sBuysenseClient)
	{
		Log.customer.debug("Entered in nullifyObj method for "+Group+" and Approvable "+objAppr);
		if(objAppr!=null && objAppr instanceof Requisition)
		{
			Requisition objReq = (Requisition) objAppr;
			List fieldNames = fields;

			String sFieldName;
			String sFullFieldName="";
			String sGetFieldName="";

			for (int i = 0; i < fieldNames.size(); i++)
			{
				sFieldName = (String) fieldNames.get(i);
				sGetFieldName = (String) getFields.get(i);
				if(Group.equals("LineItemGeneralFields"))
				{
					List lineItems = objReq.getLineItems();
					for(int j=0; j<lineItems.size();j++)
					{
						sFullFieldName = Fmt.S("%s.%s","LineItems["+new Integer(j).toString()+"]",sFieldName);
						if(objAppr.getDottedFieldValue(sFullFieldName) != null)
							setNull(objAppr,sFullFieldName,sGetFieldName,sBuysenseClient);
					}
				}
				else if (Group.equals("ProcureAccountingFields"))
				{
					List lineItems = objReq.getLineItems();
					for(int j=0; j<lineItems.size();j++)
					{
						ReqLineItem objReqLineItem = (ReqLineItem)lineItems.get(j);
						List accountings = objReqLineItem.getAccountings().getAllSplitAccountings();
						for(int k=0; k<accountings.size();k++)
						{
							sFullFieldName = Fmt.S("%s.%s.%s","LineItems["+new Integer(j).toString()+"]","Accountings.SplitAccountings["+new Integer(k).toString()+"]",sFieldName);
							if(objAppr.getDottedFieldValue(sFullFieldName) != null)
								setNull(objAppr,sFullFieldName,sGetFieldName,sBuysenseClient);
						}
					}
				}
				else
				{
					sFullFieldName = sFieldName;
					if(objAppr.getDottedFieldValue(sFullFieldName) != null)
						setNull(objAppr,sFullFieldName,sGetFieldName,sBuysenseClient);
				}
			}
		}
		else
		{
			Log.customer.debug("Object is not instance of Requisition");
		}
	}

	/**
	 * CSPL-593 (Manoj) 10/03/08
	 * @param appr
	 * @param fieldName
	 * @explanation setting null for fieldName as per condition ERPValue has n/a
	 */

    /**
     * CSPL-752 (Srini) 10/30/08
     * @param appr
     * @Changed the logic to get BFT directly from sGetField and check for ERPValue == n/a and set null. Please CSPL-752 for full dscription.
     */

	public static void setNull(Approvable appr,String fieldName,String sGetField,String sBuysenseClient)
	{
		try
		{
			String sBFTUniqueName = (sBuysenseClient+":"+sGetField);
			Log.customer.debug("sBFTUniqueName = %s",sBFTUniqueName);
			ClusterRoot fieldTable = Base.getService().objectMatchingUniqueName("ariba.core.BuysenseFieldTable", appr.getPartition(), sBFTUniqueName);
			if(fieldTable != null)
			{
				String sERPVal = (String)fieldTable.getFieldValue("ERPValue");
				if(sERPVal!= null && sERPVal.equals("n/a"))
				{
					// CSPL-752 (Manoj) 11/20/08 Added logic to set false for boolean fields
					Object obj = appr.getDottedFieldValue(fieldName);
					if(obj != null && obj instanceof Boolean)
					{
						appr.setDottedFieldValue(fieldName, new Boolean(false));
						Log.customer.debug(" For %s has set false",fieldName);
					}
					else
					{
					    appr.setDottedFieldValue(fieldName, null);
					    Log.customer.debug(" For %s has set null",fieldName);
					}
				}
			}
		}
		catch(Exception e)
		{
			Log.customer.debug(" Exception from AMSReqSubmitHook.setNull()");
			e.printStackTrace();
		}
	}

    public AMSReqSubmitHook()
    {
    }
    public static List isValidCatalogLine(Approvable approvable)
    {
        Requisition req = (Requisition) approvable;
        List reqLines = req.getLineItems();
        StringBuffer sb = new StringBuffer();
        String slines = " ";
        for (int i = 0; i < reqLines.size(); i++)
        {
            Log.customer.debug("AMSReqSubmitHook - Line: %s validation", i);
            ReqLineItem reqLine = (ReqLineItem) reqLines.get(i);
            if (reqLine.getOldValues() == null)
            {
                Log.customer.debug("AMSReqSubmitHook - Line: %s is not changed order line ", i);
                label1:
                {
                    ariba.base.core.Partition partition = reqLine.getPartition();

                    /**
                     * Added by Srini for CSPL-2904. There will be no session available for Custom Collaboration Approver
                     * while validating warnings or errors from Rules. Checking for User.getEffectiveUser() if it is null
                     * then replace the value from REQ Preparer. REQ preparer will always be effective user in case of
                     * Requisitions and will satisfy the below requirement.
                     */

                    /* START: Code changes for CSPL-2904 */
                    ariba.user.core.User loSharedUser = (ariba.user.core.User) User.getEffectiveUser();
                    if(loSharedUser == null)
                    {
                    	Log.customer.debug("AMSReqSubmitHook - user is null due to no session");
                    	loSharedUser = req.getPreparer();
                    }
                    ariba.common.core.User currUsr = ariba.common.core.User.getPartitionedUser(loSharedUser,
                            partition);
                    /* END: Code changes for CSPL-2904 */

                    if (reqLine.getIsAdHoc() || reqLine.isPunchOut())
                        break label1;
                    ariba.catalog.base.CatalogItemRef cir = reqLine.getDescription().getCatalogItemRef();
                    if (cir == null)
                        break label1;
                    ariba.catalog.base.search.ResultRow rr = Procure.getService().getResultRowForUser(partition,
                            currUsr, req.getBaseId(), cir);
                    Log.customer.debug("AMSReqSubmitHook - rr: %s: ", rr);
                    if (rr != null)
                        break label1;
                    sb.append(i+1);
                    sb.append(",");
                }
            }
        }
        slines = sb.toString();
        Log.customer.debug("AMSReqSubmitHook - slines:%s: ", slines);
        if (!StringUtil.nullOrEmptyOrBlankString(slines))
        {
            String error = ResourceService.getString("ariba.procure.core", "InvalidCatalogItemMsg");
            return ListUtil.list(Constants.getInteger(-1), "At line(s) " + slines.substring(0, slines.length() - 1)
                    + " - " + error);
        }
        else
        {
            return ListUtil.list(Constants.getInteger(0));
        }
    }
    
    /* BEGIN CSPL-5525 'BuysenseOrderCOnfirm' check JB */
    public static List checkBuysenseOrderConfirm(Approvable approvable)
    {
        Requisition loReq = (Requisition) approvable;
        BaseVector lvComments = (BaseVector) loReq.getFieldValue("Comments");
        Log.customer.debug("AMSReqSubmitHook::checkBuysenseOrderConfirm:Getting into the comments: comment basevector is " + lvComments);
        sErrorMessage = ResourceService.getString(errorStringTable, sensitiveData);
        Log.customer.debug("AMSReqSubmitHook::checkBuysenseOrderConfirm:Getting the pattern");
        Boolean bBuysenseOrderConfirmVal = (Boolean) loReq.getDottedFieldValue("BuysenseOrderConfirm");
        // Count # of confirm comments
        int iNumConfirmingComments = 0;

        Log.customer.debug("AMSReqSubmitHook::checkBuysenseOrderConfirm:Checking all the comments for buysense order confirm comments data");

        for (int i = 0; i < lvComments.size(); i++)
        {
            loComment = (Comment) lvComments.get(i);
            BaseVector vcommentsString = (BaseVector) loComment.getDottedFieldValue("Text.Strings");
            ReqLineItem loReqLi = (ReqLineItem) loComment.getFieldValue("LineItem");
            Log.customer.debug("AMSReqSubmitHook::checkBuysenseOrderConfirm:Getting into the comments: comment basevector is " + vcommentsString);

            try
            {
                lsCommentString = getCommentString(loComment);
                Log.customer.debug("AMSReqSubmitHook::checkBuysenseOrderConfirm:The comments string is " + lsCommentString);

                if (lsCommentString != null)
                {
                    // Order confirmation change - start JB BBB
                    // If 'BuysenseOrderConfirm' is checked the we should find 'CONFIRMING ORDER; DO NOT DUPLICATE'. Else display warning message*/
                    // If 'BuysenseOrderConfirm' is unchecked the we should NOT find 'CONFIRMING ORDER; DO NOT DUPLICATE'. Else display warning message*/
                    /* Retrieve confirming order warning messages from resource files JB BBB */

                    String lsOrderConfComment = ResourceService.getString(errorStringTable, orderConfComment);
                    String lsOrderConfPattern = ResourceService.getString(errorStringTable, lsOrderConfComment);
                    Pattern patternOrderConf = Pattern.compile(lsOrderConfPattern);
                    Matcher orderConfMatcher = patternOrderConf.matcher(lsCommentString);

                    // check only for header comments && comment found 
                    if (loReqLi == null && (orderConfMatcher.find() || orderConfMatcher.matches() || orderConfMatcher.lookingAt()))
                    {
                        iNumConfirmingComments++;
                    }                    
                }
            }
            catch (ArrayIndexOutOfBoundsException e)
            {
                Log.customer.debug("We hit this on %s", lsCommentLongString);
                Log.customer.debug(e.getMessage());
                continue;
            }
            catch (Exception e)
            {
                Log.customer.debug("Error retrieving vector element");
                Log.customer.debug(e.getMessage());
                continue;
            }
        }

        try
        {
            // If 'BuysenseOrderConfirm' is unchecked and you find 1 or >1 order comments
            if (!bBuysenseOrderConfirmVal.booleanValue())
            {
                Log.customer.debug("AMSReqSubmitHook:: BuysenseOrderConfirm is unchecked");
                // If there are no comments then it is correct
                if (iNumConfirmingComments >= 1)
                {
             /* Start CSPL-7529 'BuysenseOrderConfirm' unchecked, remove automatically added comment */
                    for (int i = 0; i < lvComments.size(); i++)
                    {

                        String lsOrderConfComment = ResourceService.getString(errorStringTable, orderConfComment);
                        String lsOrderConfPattern = ResourceService.getString(errorStringTable, lsOrderConfComment);
                        Log.customer.debug("Inside for if:: BuysenseOrderConfirm one comment");
                        loComment = (Comment) lvComments.get(i);
                        lsCommentString = getCommentString(loComment);
                        if ((lsOrderConfPattern.trim()).equals(lsCommentString.trim()))
                        {
                            loReq.removeComment(loComment);
                            loReq.save();
                        }
                    }   
                }
            }
             /* End CSPL-7529 'BuysenseOrderConfirm' unchecked, remove automatically added comment */
            // If 'BuysenseOrderConfirm' is checked and you find 0 or >1 order comments
            if (bBuysenseOrderConfirmVal.booleanValue())
            {
                Log.customer.debug("AMSReqSubmitHook:: BuysenseOrderConfirm is checked");
                if (iNumConfirmingComments == 0)
                {
                    Log.customer.debug("AMSReqSubmitHook:: BuysenseOrderConfirm no comments");
                    String lsOrderConfCheckedErr = ResourceService.getString(errorStringTable, orderConfCheckedErr);
                    return ListUtil.list(Constants.getInteger(-1), lsOrderConfCheckedErr);
                }
                else if (iNumConfirmingComments > 1)
                {
                    Log.customer.debug("AMSReqSubmitHook:: BuysenseOrderConfirm more than 1 comments");
                    String lsOrderConfCheckedMultiErr = ResourceService.getString(errorStringTable, orderConfCheckedMultiErr);
                    return ListUtil.list(Constants.getInteger(-1), lsOrderConfCheckedMultiErr);
                }
                // If there is just one comment, then it is correct
            }
        }
        catch (Exception e)
        {
            Log.customer.debug("AMSReqSubmitHook:: Exception in");
        }
        return ListUtil.list(Constants.getInteger(0));
    }
    /* END CSPL-5525 'BuysenseOrderConfirm' check JB*/
    
    /* BEGIN::PARTHO: Fix For CSPL-4053 sensitive data check */
	public static String checkSensitiveDataPattern(Approvable approvable) throws Exception
    {
    	Requisition loReq = (Requisition) approvable;
    	List lvReqLines = loReq.getLineItems();

    	BaseVector lvComments = (BaseVector) loReq.getFieldValue("Comments");
    	Log.customer.debug("AMSReqSubmitHook::checkSensitiveDataPattern:Getting into the comments: comment basevector is " +lvComments);
    	List errorLineList=ListUtil.list();

    	boolean       lbCommentsFlag          = false;
    	boolean       lbDescriptionFlag       = false;
    	boolean       lbHeaderCommentFlag     = false;
    	boolean       lbReplyHeaderComment    = false;
    	boolean       lbIsAdHoc               = false;
    	boolean       lbIsAdHocTrue           = false;
    	StringBuffer lsbSensitiveDataLineMsg = new StringBuffer();
    	StringBuffer lsbSensitiveDataOnHeaderCommentMsg = new StringBuffer();
    	sErrorMessage = ResourceService.getString(errorStringTable, sensitiveData);
    	String lsLineMsg = "Description/Comment(s) on Line(s)";

        
    	
    	lsbSensitiveDataLineMsg.append(lsLineMsg);

    	ArrayList lineNoCheckForDescription = null;
    	ArrayList lineNoCheckForComments = null;



    	/*Following is the pattern to check sensitive data(SSN).
    	 This can be in the form of XXX-XX-XXXX or XXX.XX.XXXX or XXX XX XXXX or XXXXXXXXX */

		Log.customer.debug("AMSReqSubmitHook::checkSensitiveDataPattern:Getting the pattern");
		String lsPattern = ResourceService.getString(errorStringTable, patternToMatch);
	    Pattern pattern = Pattern.compile(lsPattern);
	    String lsPattern1 = pattern.toString();

	    Log.customer.debug("The pattern is :"+pattern);
        
	    for(int i=0; i < lvReqLines.size(); i++)
    	{

    		Log.customer.debug("AMSReqSubmitHook::checkSensitiveDataPattern:Checking all the line item description for sensitive data");

    		ReqLineItem loReqLi = (ReqLineItem) lvReqLines.get(i);
    		lbIsAdHoc = (Boolean)loReqLi.getFieldValue("IsAdHoc");
    		Log.customer.debug("The current line item is an AdHoc line is "+lbIsAdHoc);

    		String lsDescription = (String) loReqLi.getDottedFieldValue("Description.Description");
    		Log.customer.debug("AMSReqSubmitHook::checkSensitiveDataPattern:The Description value to check is  "+lsDescription);
    		Matcher matcher = pattern.matcher(lsDescription);
    		boolean lbPatternMatcher = lsDescription.matches(lsPattern1);

    		lsDescription.matches(lsPattern1);
    		if(lbIsAdHoc)
    		{
    		lbIsAdHocTrue = true;
    		if(lsDescription != null && (matcher.find() || matcher.matches() || matcher.lookingAt()))
    		{
    			lbDescriptionFlag = true;
    			Log.customer.debug("The current value for lbIsLineVerified is "+lbIsLineVerified);
    			int liLineNo = (i+1);
    			Log.customer.debug("The Requisition contains sensitive data at line"+liLineNo);
				Log.customer.debug("We are currently verifying line" +lineNo);
				Log.customer.debug("We are currently verifying line" +liLineNo);
				lineItemsChecked.add(liLineNo);
    			continue;
    		}
    		}
    	}

    	for(int i=0; i < lvComments.size(); i++)
    	{
    		Log.customer.debug("AMSReqSubmitHook::checkSensitiveDataPattern:Checking all the comments for sensitive data");
    		loComment = (Comment) lvComments.get(i);
    		BaseVector vcommentsString = (BaseVector)loComment.getDottedFieldValue("Text.Strings");
    		ReqLineItem loReqLi = (ReqLineItem) loComment.getFieldValue("LineItem");
    		Log.customer.debug("AMSReqSubmitHook::checkSensitiveDataPattern:Getting into the comments: comment basevector is "+vcommentsString);

    		try
    			{

					lsCommentString = getCommentString(loComment);
					Log.customer.debug("AMSReqSubmitHook::checkSensitiveDataPattern:The comments string is "+lsCommentString);
					
					if(lsCommentString != null){
					// Order confirmation change - start JB BBB 
					// If 'BuysenseOrderConfirm' is checked the we should find 'CONFIRMING ORDER; DO NOT DUPLICATE'. Else display warning message*/					    
                    // If 'BuysenseOrderConfirm' is unchecked the we should NOT find 'CONFIRMING ORDER; DO NOT DUPLICATE'. Else display warning message*/
			        /* Retrieve confirming order warning messages from resource files JB BBB */
			        /* Need one warning message if checkbox is checked and corresponding comment is removed */
			        /* Need one warning message if checkbox is unchecked and corresponding comment is NOT removed */
				     
					Matcher matcher = pattern.matcher(lsCommentString);
					if(matcher.find() || matcher.matches() || matcher.lookingAt())
    					{
    						lbCommentsFlag = true;
    						Log.customer.debug("The Comments flag for Comment line " +(i+1) +"is set to " +lbCommentsFlag);
    						if(loReqLi != null)
    						{
    							int liLineNo = (Integer)loReqLi.getDottedFieldValue("NumberInCollection");
    							Log.customer.debug("We are currently verifying line" +lineNo);
    							Log.customer.debug("We are currently verifying line" +liLineNo);
    							lineItemsChecked.add(liLineNo);

    						}
    						else
    						{
    							if(!lbHeaderCommentFlag)
    							{
    								lbHeaderCommentFlag = true;
    								lsbSensitiveDataOnHeaderCommentMsg.append("Header Comment(s)");
    								lsbSensitiveDataOnHeaderCommentMsg.append(",");

    							}
    						}
    						continue;
    					}
    				}
    				if (loComment.getComments() != null && !loComment.getComments().isEmpty()
    						&& loComment.getComments().size() > 0)
    				{
    					try {
							lbCommentsFlag = verifyReplyComments(loComment.getComments(),pattern,loComment,lsbSensitiveDataLineMsg,lsbSensitiveDataOnHeaderCommentMsg,lbReplyHeaderComment);
						} catch (NoProblemException e) {
							e.printStackTrace();
						}
    				}
    			}
    			catch (ArrayIndexOutOfBoundsException e)
    			{
    					Log.customer.debug("We hit this on %s", lsCommentLongString);
    					Log.customer.debug(e.getMessage());
    					continue;
    			}
    			catch (Exception e)
    			{
    				Log.customer.debug("Error retrieving vector element");
    				Log.customer.debug(e.getMessage());
    				continue;
    			}
    	}

    	Log.customer.debug("AMSReqSubmitHook::the values of Description flag and Comments flag is DescriptionFlag:" +lbDescriptionFlag +" And Comments flag is:" +lbCommentsFlag);

    	if((lbCommentsFlag || lbDescriptionFlag))
    	{
    		Log.customer.debug("Either of lbCommentsFlag or lbDescriptionFlag is set to true");

    		Log.customer.debug("The elements in the unsorted Line Item list are " +lineItemsChecked.toString());
    		HashSet lLineItemChecked = new HashSet(lineItemsChecked);
    		Log.customer.debug("The elements in the hashset are " +lLineItemChecked.toString());

    		lineItemsChecked.clear();
    		lineItemsChecked.addAll(lLineItemChecked);

    		lLineItemChecked.clear();
    		Collections.sort(lineItemsChecked);


    		if(lineItemsChecked.isEmpty())
    		{
    			lsbSensitiveDataOnHeaderCommentMsg.setCharAt((lsbSensitiveDataOnHeaderCommentMsg.length()-1), '.');
    			String lsFinalWarningMsg = sErrorMessage + "The Requisition contains sensitive data at " + lsbSensitiveDataOnHeaderCommentMsg.toString();
    			lineItemsChecked.clear();
    			return lsFinalWarningMsg;
    		}
    		else
    		{
        	String lslineItemsChecked = lineItemsChecked.toString();
        	Log.customer.debug("The sorted and duplicate removed list of line items is "+lslineItemsChecked);
    		String lsFinalWarningMsg = sErrorMessage + "The Requisition contains sensitive data at " + lsbSensitiveDataOnHeaderCommentMsg.toString() +lsLineMsg+ lslineItemsChecked;
    		Log.customer.debug("The final message to show is" +lsFinalWarningMsg);
    		lineItemsChecked.clear();
    		return lsFinalWarningMsg;
    		}


    	}
    	return null;
    }

	public static boolean verifyReplyComments(BaseVector comments,Pattern pattern,Comment loComment,StringBuffer lsbSensitiveDataLineMsg,StringBuffer lsbSensitiveDataOnHeaderCommentMsg,boolean lbReplyHeaderComment) throws NoProblemException
	{
        Log.customer.debug("AMSReqSubmitHook::getReplyComments::Inside getReplyComments");
        boolean lbCommentsFlag = false;
        if (!comments.isEmpty())
        {
            for (int i = 0; i < comments.size(); i++)
            {
                oReplycomment = (Comment) comments.get(i);
                ReqLineItem loReqLi = (ReqLineItem) loComment.getFieldValue("LineItem");
           		try
            		{
           			lsCommentString = getCommentString(oReplycomment);
           			Log.customer.debug("AMSReqSubmitHook::checkSensitiveDataPattern:The reply comments string is "+lsCommentString);
					
					if(lsCommentString != null)
            		{
         				
         				Matcher matcher = pattern.matcher(lsCommentString);
    					if(lsCommentString != null && (matcher.find() || matcher.matches() || matcher.lookingAt()))
    					{
    						lbCommentsFlag = true;
    						Log.customer.debug("The Comments flag for Reply Comment line is set to " +lbCommentsFlag);
    						if(loReqLi != null)
    						{
    							boolean lbIsAdHoc = (Boolean)loReqLi.getFieldValue("IsAdHoc");
    							Log.customer.debug("The current line item is an AdHoc line is: "+lbIsAdHoc);
    							if(lbIsAdHoc)
    							{
    							int liLineNo = (Integer)loReqLi.getDottedFieldValue("NumberInCollection");
    							Log.customer.debug("We are currently verifying line" +lineNo);
    							Log.customer.debug("We are currently verifying line" +liLineNo);
    							lineItemsChecked.add(liLineNo);
    							}

    						}
    						else
    						{
    							if(!lbReplyHeaderComment)
    							{
    								lbReplyHeaderComment = true;
        							lsbSensitiveDataOnHeaderCommentMsg.append("Reply to Header Comment(s)");
        							lsbSensitiveDataOnHeaderCommentMsg.append(",");
    							}

    						}
    						continue;
    					}


            		}
            		if (oReplycomment.getComments() != null && !oReplycomment.getComments().isEmpty()
                            && oReplycomment.getComments().size() > 0)
                    	{
            				lbCommentsFlag = verifyReplyComments(oReplycomment.getComments(),pattern,loComment,lsbSensitiveDataLineMsg,lsbSensitiveDataOnHeaderCommentMsg,lbReplyHeaderComment);
                    	}
        			}
                    catch (ArrayIndexOutOfBoundsException e)
                    {
                        Log.customer.debug("We hit this on %s", lsCommentLongString);
    					Log.customer.debug(e.getMessage());
    					continue;
                    }
                    catch (Exception e)
                    {
        				Log.customer.debug("Error retrieving vector element");
        				Log.customer.debug(e.getMessage());
        				continue;
                    }

            }
            return lbCommentsFlag;
        }
        return lbCommentsFlag;
	}

	/*END::PARTHO: CSPl-4053*/

    //SRINI: START: SEV changes for SupplierLocation
    public static void setRegistrationType(Requisition req)
    {
    	String lsRegistrationType = "RgstTypeCd";
    	String lsRegistrationTypeDescr = "RgstTypeDesc";
    	String lsRegistrationTypeVal = null;
    	String lsRegistrationTypeDescrVal = null;

    	List lvReqLines = req.getLineItems();
    	for (int i = 0; i < lvReqLines.size(); i++)
    	{
    		ReqLineItem loReqLine = (ReqLineItem) lvReqLines.get(i);
    		SupplierLocation loLocation = (SupplierLocation) loReqLine.getSupplierLocation();
    		if((isLocationChanged(loReqLine) || loReqLine.getOldValues() == null) && loLocation != null)
    		{
    			lsRegistrationTypeVal = (String) loLocation.getFieldValue(lsRegistrationType);
    			lsRegistrationTypeDescrVal = (String) loLocation.getFieldValue(lsRegistrationTypeDescr);
    			if(lsRegistrationTypeVal != null && lsRegistrationTypeDescrVal != null)
    			{
    				loReqLine.setFieldValue(lsRegistrationType, lsRegistrationTypeVal);
    				loReqLine.setFieldValue(lsRegistrationTypeDescr, lsRegistrationTypeDescrVal);
    			}
    		}
    	}
    }
    
    public static boolean isLocationChanged(ReqLineItem reqLine)
    {
    	if(reqLine.getOldValues() != null && reqLine.getPreviousRLIWithValidOrder() != null)
    	{
    		ReqLineItem loOldRLi =  (ReqLineItem) reqLine.getPreviousRLIWithValidOrder();
    		if(loOldRLi.getSupplierLocation() != null && reqLine.getSupplierLocation() != null &&
    		   !loOldRLi.getSupplierLocation().getUniqueName().equals(reqLine.getSupplierLocation().getUniqueName()))
    		{
    			return true;
    		}
    	}
    	return false;
    }
    //SRINI: END: SEV changes for SupplierLocation


    //SRINI: Added for CSPL-7067
    public static void alignMassEditFunctionality(Requisition req)
    {
    	int iReceivingType = 2;
    	ReqLineItem loReqLineItem = null;
    	List lvReqLines = req.getLineItems();
    	for (int i = 0; i < lvReqLines.size(); i++)
    	{
    		loReqLineItem = (ReqLineItem) lvReqLines.get(i);
    		iReceivingType = ((Integer) loReqLineItem.getFieldValue("BuysenseReceivingType")).intValue();
    		if(iReceivingType == 0)
    		{
    			loReqLineItem.setFieldValue("BuysenseReceivingType", Integer.valueOf(2));
    		}
    		else if(iReceivingType == 3)
    		{
    			loReqLineItem.setFieldValue("BuysenseReceivingMethod", "Amount");
    		}
	        else
	        {
	        	loReqLineItem.setFieldValue("BuysenseReceivingMethod", "Quantity");        		
	        }
    	}
    }
    //SRINI: Added for CSPL-7067
  
    //JB: START: update line item SWAM values CSPL-6684
    public static void setSWAMValues(Requisition req)
    {
        Boolean lbSupplierSmall = new Boolean("false");
        Boolean lbSupplierWoman = new Boolean("false");
        Boolean lbSupplierMinority = new Boolean("false");
        Boolean lbSupplierMicroBusiness = new Boolean("false");
        
    	List lvReqLines = req.getLineItems();
    	for (int i = 0; i < lvReqLines.size(); i++)
    	{
    		ReqLineItem loReqLine = (ReqLineItem) lvReqLines.get(i);
            lbSupplierSmall = (Boolean) loReqLine.getDottedFieldValue("Supplier.SmallBusiness");
            lbSupplierWoman = (Boolean) loReqLine.getDottedFieldValue("Supplier.WomanOwnedBusiness");
            lbSupplierMinority = (Boolean) loReqLine.getDottedFieldValue("Supplier.MinorityOwnedBusiness");
            lbSupplierMicroBusiness = (Boolean) loReqLine.getDottedFieldValue("Supplier.MicroBusiness");

            loReqLine.setDottedFieldValue("Description.SmallBusiness", lbSupplierSmall);
            loReqLine.setDottedFieldValue("Description.WomanOwnedBusiness", lbSupplierWoman);
            loReqLine.setDottedFieldValue("Description.MinorityOwnedBusiness", lbSupplierMinority);
            loReqLine.setDottedFieldValue("Description.MicroBusiness", lbSupplierMicroBusiness);
        	}
    }


    
    
    public static boolean isLineItemSWAMChanged(ReqLineItem reqLine)
    {
        Boolean lbSupplierSmall = (Boolean) reqLine.getDottedFieldValue("Supplier.SmallBusiness");
        Boolean lbSupplierWoman = (Boolean) reqLine.getDottedFieldValue("Supplier.WomanOwnedBusiness");
        Boolean lbSupplierMinority = (Boolean) reqLine.getDottedFieldValue("Supplier.MinorityOwnedBusiness");
        Boolean lbSupplierMicroBusiness = (Boolean) reqLine.getDottedFieldValue("Supplier.MicroBusiness");

        Boolean lbReqLineSmall = (Boolean) reqLine.getDottedFieldValue("Description.SmallBusiness");
        Boolean lbReqLineWoman = (Boolean) reqLine.getDottedFieldValue("Description.WomanOwnedBusiness");
        Boolean lbReqLineMinority = (Boolean) reqLine.getDottedFieldValue("Description.MinorityOwnedBusiness");
        Boolean lbReqLineMicroBusiness = (Boolean) reqLine.getDottedFieldValue("Description.MicroBusiness");

        
        if(isSWAMChanged(lbSupplierSmall,lbReqLineSmall) || isSWAMChanged(lbSupplierWoman,lbReqLineWoman) 
        		|| isSWAMChanged(lbSupplierMinority,lbReqLineMinority) || isSWAMChanged(lbSupplierMicroBusiness,lbReqLineMicroBusiness))
        {
        	return true;        	
        }
        
        return false;
    }

    private static boolean isSWAMChanged(Boolean supplierSWAM, Boolean lineItemSWAM)
    {
    	if(((supplierSWAM == null) && (lineItemSWAM != null)) || ((supplierSWAM != null) && (lineItemSWAM == null))) // one of the values is null but not the other one
    	{
    		return true;
    	}
    	if((supplierSWAM == null) && (lineItemSWAM == null)) // both are null
    	{
    		return false;
    	}
    	// both values are non-null
    	return !(supplierSWAM.booleanValue() == lineItemSWAM.booleanValue());    	
    }

    //JB: END: update line item SWAM values

    //Partho: Code fix to read complete content from comment string having more than 254 characters.
    public static String getCommentString(Comment cmtStr)
    {
    	String sLongString = "";
     
    	List vCmt = cmtStr.getText().getStrings();
    	LongStringElement sLongElement=null;
    						
    	for(int iCmtText = 0; iCmtText < vCmt.size(); iCmtText++)
    	   {
    			sLongElement = (LongStringElement) vCmt.get(iCmtText);
    			sLongString += sLongElement.getString();
    			Log.customer.debug("The value of sLongString is :"+sLongString);
    	   }
	    Log.customer.debug("The value os sLongString is :"+sLongString);
	    return sLongString;
  
    }

}
