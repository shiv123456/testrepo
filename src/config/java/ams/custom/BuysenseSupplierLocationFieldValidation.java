//
//***************************************************************************/
//  Anup - July 2003 - validate Supplier Location fields.
//
//  12/02/2003 JN - Apply changes for Ariba 8.1
//
//***************************************************************************/

/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */
/* 09/18/2004: Ariba 8.x ST SPL#91 Check for null Creator instead of object (David Chamberlain) */

package config.java.ams.custom;

//import ariba.basic.util.Log;
import java.util.Map;
import ariba.util.log.Log;
import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.base.core.*;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
/* Ariba 8.1: Added new package ariba.basic.core */
import ariba.basic.core.*;
//jackie - need to ask Ariba consultant or in house ariba experts. THere are several Log classes in 822. Need to figure out
// which on we need
//import ariba.util.log.Log;
import ariba.common.core.*;


//Rob Giesen, Feb 3, 2004 Dev SPL#21 - Country changed object
// Changed the country object for Ariba 8.1 to ariba.basic.core.Country
// from the old 7.X
// Also changed to the setCountry() function, instead of setFieldValue

//81->822 changed Hashtable to Map
//81->822 changed ConditionValueInfo to ValueInfo
public class BuysenseSupplierLocationFieldValidation extends Condition
{

    private static final ValueInfo parameterInfo[];
    private static final String defaultCountry = "US";
    String message = null;
    //Changed to global variable by SRINI for SEV TIN
    Object Obj = null;

    public boolean evaluate(Object value, PropertyTable params)
    {
       String testValue = null;
       String field = params.stringPropertyForKey("TargetValue");
       String linkField = params.stringPropertyForKey("TargetValue1");
       Obj = (Object)params.getPropertyForKey("TargetValue2");
       String conditionField = null;

       // Ariba 8.x ST SPL#91 09/18/2004: Check for null creator instead of null object
       // Get out if not ad-hoc location
       Object loUser = (Object)(((SupplierLocation)Obj).getCreator());
       if (loUser == null)
       {
          return true;
       }

            // Set/Default country
            Log.customer.debug("BuysenseSupplierLocationFieldValidation : Obj = %s ", Obj);
            Country cntry = (Country)((SupplierLocation)Obj).getCountry();
            Log.customer.debug("BuysenseSupplierLocationFieldValidation : Country is = %s ", cntry);
            if(cntry != null)
            {
               /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
               if(StringUtil.nullOrEmptyOrBlankString((String)cntry.getFieldValue("UniqueName")))
               {       cntry.setFieldValue("UniqueName", defaultCountry);
                       cntry.setDottedFieldValue("Name.PrimaryString", "United States");
               }
            }
            else
            {
                //rgiesen Dev SPL#21 - changed the country object
               cntry = (Country)Base.getService().objectMatchingUniqueName("ariba.basic.core.Country",
                                   Base.getSession().getPartition(), defaultCountry);
                //rgiesen Dev SPL#21 - changed the method to setCountry()
               //((SupplierLocation)Obj).setFieldValue("Country", cntry);
               ((SupplierLocation)Obj).setCountry(cntry);
            }
            conditionField = (String)cntry.getFieldValue("UniqueName");

       // Get field value
       if (! field.equals("Country"))
       {
          testValue = (String)value;
       }
       Log.customer.debug("BuysenseSupplierLocationFieldValidation : value = %s ", value);

       //Validations
       return validateFields(testValue, field, linkField, conditionField, (SupplierLocation)Obj, true);

    }

    public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
    {

        if(!evaluate(value, params))
        {
            String retMsg = null;
            /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
            if(!StringUtil.nullOrEmptyOrBlankString(message))
            {
                retMsg = message;
                message = null;
                return new ConditionResult(retMsg);
            }
            else
                return new ConditionResult(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SLDefaultMessage"));
        }
        return null;
    }

    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    static
    {
        parameterInfo = (new ValueInfo[] {
            new ValueInfo("TargetValue", 0),
            new ValueInfo("TargetValue1", 0),
            new ValueInfo("TargetValue2", 0),
            new ValueInfo("Message", 0, Behavior.StringClass)
        });
    }

    public String validateLocation(SupplierLocation location)
    {
        String returnString = null;
        String[] fields = {"EmailAddress", "Phone", "Lines", "City", "State",
                           "PostalCode", "Contact", "Fax", "TIN"};

        String countryCode = (String)location.getDottedFieldValue("Country.UniqueName");
        String targetValue = null;
        String linkValue = null;
        for (int i=0; i<fields.length; i++)
        {
            linkValue = null;
            targetValue = (String)location.getFieldValue(fields[i]);
            if (i == 0)
                linkValue = (String)location.getFieldValue(fields[i + 1]);
            else if (i == 1)
                linkValue = (String)location.getFieldValue(fields[i - 1]);

            if(!validateFields(targetValue, fields[i], linkValue, countryCode, location, false))
            {
                if(returnString == null)
                    returnString = fields[i];
                else
                    returnString = returnString + ", " + fields[i];
            }
        }

        return returnString;
    }


    private boolean validateFields(String testValue, String field, String linkField, String conditionField, SupplierLocation location, boolean bypassSubmihook)
    {
        if(field.equals("TIN"))
        {
                        /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
            if(StringUtil.nullOrEmptyOrBlankString(testValue))
            {
                message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SLDefaultMessage");
                return false;
            }

                        /* Ariba 8.1: Replaced Util.isAllDigits */
            if(testValue.length() != 9 || !StringUtil.isAllDigits(testValue))
            {
                message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SLTINDigits");
                return false;
            }
            
            //SRINI: START: Added for SEV TIN validation
            TransientData td = Base.getSession().getTransientData();
            Log.customer.debug("BuysenseSupplierLocationFieldValidation : TD TIN = %s ", td.get("TIN"));
            //if(duplicateTIN(testValue) && td.get("TIN") == null && td.get("TINValidated") == null)
            if(duplicateTIN(testValue, location) && td.get("TIN") == null)
            {
            	td.put("TIN", testValue);
            	//td.put("TINValidated", "TRUE");
            	message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","DuplicateTIN");
            	Log.customer.debug("BuysenseSupplierLocationFieldValidation : TD TIN first if = %s ", td.get("TIN"));
            	return false;
            }

            if(duplicateTIN(testValue, location) && td.get("TIN") != null && !((String)td.get("TIN")).equals(testValue))
            {
            	td.put("TIN", testValue);
            	message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","DuplicateTIN");
            	Log.customer.debug("BuysenseSupplierLocationFieldValidation : TD TIN second if = %s ", td.get("TIN"));
            	return false;
            	//td.put("TINValidated", null);
            }

            if(duplicateTIN(testValue, location) && td.get("TIN") != null && ((String)td.get("TIN")).equals(testValue))
            {
            	td.put("TIN", null);
            	Log.customer.debug("BuysenseSupplierLocationFieldValidation : TD TIN third if = %s ", td.get("TIN"));
            	//td.put("TINValidated", null);
            }
            //SRINI: END: Added for SEV TIN validation
        }
        else if (field.equals("Lines")      ||
                 field.equals("City")       ||
                 field.equals("PostalCode"))
        {
               /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
           if(StringUtil.nullOrEmptyOrBlankString(testValue))
           {
               message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SLDefaultMessage");
               return false;
           }
        }
        else if (field.equals("State"))
        {
            if( StringUtil.nullOrEmptyOrBlankString( testValue ) )
            {
                return false ;
            }
            if( StringUtil.nullOrEmptyOrBlankString( conditionField ) )
            {
                conditionField = defaultCountry ;
            }
                return validateState( testValue , conditionField ) ;
        }
        else if (field.equals("Fax"))
        {
                        /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
            if(StringUtil.nullOrEmptyOrBlankString(testValue))
                return true;
                        /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
            if(StringUtil.nullOrEmptyOrBlankString(conditionField))
                        conditionField = defaultCountry;

            return validatePhoneOrFax(testValue, conditionField);

        }
        else if (field.equals("Phone"))
        {
                        /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
            if(StringUtil.nullOrEmptyOrBlankString(testValue))
            {
                                /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
                if(StringUtil.nullOrEmptyOrBlankString(linkField))
                {
                    message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SLPhoneOrEmail");
                    return false;
                }
            }
            else
            {
                        /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
            if(StringUtil.nullOrEmptyOrBlankString(conditionField))
                               conditionField = defaultCountry;

                return validatePhoneOrFax(testValue, conditionField);

            }
        }
        else if (field.equals("EmailAddress"))
        {
                /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
            if(StringUtil.nullOrEmptyOrBlankString(testValue))
            {
                   /* Ariba 8.1: Replaced Util.nullOrEmptyOrBlankString */
               if(StringUtil.nullOrEmptyOrBlankString(linkField))
               {
                   message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SLPhoneOrEmail");
                   return false;
               }
            }
            else
            {
                 return validateEmail(testValue);
            }
        }
        //SRINI: START: Added for SEV Name validation
        else if(field.equals("Contact"))
        {
            String lsSpace = " ";
        	if(StringUtil.nullOrEmptyOrBlankString(testValue))
            {
                message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SLDefaultMessage");
                return false;
            }
        	if(!testValue.trim().contains(lsSpace) && bypassSubmihook)
            {
        		message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","ContactNameValidation");
                return false;
            }
        }
        else if(field.equals("LLCClassification"))
        {
        	String lsOrgType = (String) location.getFieldValue("OrganizationType");
        	if(lsOrgType != null && StringUtil.nullOrEmptyOrBlankString(testValue) && lsOrgType.startsWith("Limited Liability Co."))
            {
        		message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","LLCValidation");
                return false;
            }
        }
        else if(field.equals("OrderDUNS"))
        {
        	if(!StringUtil.nullOrEmptyOrBlankString(testValue) && (!StringUtil.isAllDigits(testValue) || !(testValue.length() == 9)))
            {
        		message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","DUNSValidation");
                return false;
            }
        }
        //SRINI: End: Added for SEV Name validation        
       return true;
    }
    
    //SRINI: START: Added for SEV TIN validation
    public static boolean duplicateTIN(String tinValue, SupplierLocation location)
    {
    	String lsQuery = "SELECT sl from ariba.common.core.SupplierLocation sl " +
    					 "WHERE TIN = :TIN AND Creator IS NULL";
		AQLOptions loOptions;
	    AQLQuery loQuery;
	    AQLResultCollection loResults;
		Map loParams = MapUtil.map();
		loParams.put("TIN", tinValue);
		loOptions = new AQLOptions(((SupplierLocation)location).getPartition());
		loOptions.setActualParameters(loParams);
		loQuery = AQLQuery.parseQuery(lsQuery);
		loResults = Base.getService().executeQuery(loQuery, loOptions);
		if (loResults.next()) 
	    {
			return true;
	    }
		else
		{
			return false;
		}
    }
    //SRINI: END: Added for SEV TIN validation

        public boolean validateState( String stateCode, String countryCode )
        {
            Log.customer.debug("stateCode = %s , countryCode = %s", stateCode, countryCode);
            if( !countryCode.equals( "US" ) )
            {
                Log.customer.debug("Country is not US - abend evaluation");
                return true ;
            }
            else
            {
                Log.customer.debug("Country is US - evaluate against the Map");
                if ( USStatesAndTerritories.isValidUSStatesAndTerritoriesCode( stateCode ) )
                {
                    return true ;
                }
                else
                {
                    message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SLStateCodeValue") ;
                    return false ;
                }
            }
        }

    public boolean validatePhoneOrFax(String telNumber, String countryCode)
    {
        if(!(countryCode.equals("CA") || countryCode.equals("US")))
        {
                        /* Ariba 8.1: Replaced Util.isAllDigits */
            if(StringUtil.isAllDigits(telNumber))
                return true;
            else
            {
                message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SLAllDigits");
                return false;
            }
        }

        if(telNumber.length() == 12)
        {
            int index1 = telNumber.indexOf("-", 0);
            int index2 = telNumber.indexOf("-", index1 + 1);
            if(index1 == 3 &&
               index2 == 7 &&
                           /* Ariba 8.1: Replaced Util.isAllDigits */
               StringUtil.isAllDigits(telNumber.substring(0, index1 - 1)) &&
               StringUtil.isAllDigits(telNumber.substring(index1 + 1, index2 - 1)) &&
               StringUtil.isAllDigits(telNumber.substring(index2 + 1)))
                return true;
            else
            {
                message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SLPhoneFaxFormat");
                return false;
            }
        }
        else
        {
            message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SLPhoneFaxFormat");
            return false;
        }
    }

    public boolean validateEmail(String email)
    {
        if ((email.indexOf("@") >= 0) && (email.indexOf(".") >= 0))
            return true;
                else
                {
                message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SLInvalidValue");
                    return false;
            }
        }
}

