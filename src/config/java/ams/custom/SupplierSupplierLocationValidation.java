/*
 * @(#)SupplierSupplierLocationValidation.java      1.0 2005/03/14
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information"). You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 */

package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.common.core.*;
import ariba.purchasing.core.ReqLineItem;

//81->822 changed ConditionValueInfo to ValueInfo
public class SupplierSupplierLocationValidation extends Condition
{

    private static final ValueInfo parameterInfo[];
    String message = null;

    public boolean evaluate(Object value, PropertyTable params)
    {
       ReqLineItem loReqLine = (ReqLineItem)(params.getPropertyForKey("TargetValue"));
       String lsSource = (String)(params.getPropertyForKey("TargetValue1"));
       
       if (loReqLine == null)
          return true;
    // CSPL-7968 - Mass edit functionality was not working and updated the code as below as part of CSPL7968.
       if (lsSource.equalsIgnoreCase("LineItemSimpleGeneralFields"))
       {
          return true;
       } 
       if (lsSource.equalsIgnoreCase("SupplierField") && loReqLine.getSupplier() == null)
       {
          message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SSLSupplierNull"); 
          return false;
       } 

       if (lsSource.equalsIgnoreCase("LocationField") && loReqLine.getSupplierLocation() == null)
       {
          message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SSLLocationNull"); 
          return false;
       }

       if (value instanceof Supplier)
       {
          if (! loReqLine.getSupplier().getActive() )
          {
             message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SSLInactiveSupplier");
             return false;
          }
       }

       if (value instanceof SupplierLocation)
       {
          if (! loReqLine.getSupplierLocation().getActive() )
          {
             message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SSLInactiveLocation");
             return false;
          }
       }

       if (loReqLine.getSupplier() != null &&
           loReqLine.getSupplier().getActive() && 
           loReqLine.getSupplierLocation() != null &&
           loReqLine.getSupplierLocation().getActive())
       {
          if (! ((loReqLine.getSupplierLocation().getSupplier()).equals(loReqLine.getSupplier())) )
          {
             message = Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SSLUnrelated");
             return false;
          }
       }

       return true;

    }

    public ConditionResult evaluateAndExplain(Object value, PropertyTable params)
    {

        if(!evaluate(value, params))
        {
            if(!StringUtil.nullOrEmptyOrBlankString(message))
            {
                String retMsg = message;
                message = null;
                return new ConditionResult(retMsg);
            }
            else
                return new ConditionResult(Fmt.Sil(Base.getSession().getLocale(),"ariba.procure.core","SSLDefaultMessage"));
        }
        return null;
    }

    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }

    static
    {
        parameterInfo = (new ValueInfo[] {
            new ValueInfo("TargetValue", 0),
            new ValueInfo("TargetValue1", 0),
            new ValueInfo("Message", 0, Behavior.StringClass)
        });
    }

}

