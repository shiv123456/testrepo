package config.java.ams.custom;

/*
Purpose          : Custom controller of Invited Suppliers
Change Log	  :
*/

import ariba.htmlui.procure.fields.*;
import ariba.procure.core.CategoryUtil;
import ariba.procure.core.ProcureLineItem;
import ariba.util.log.Log;

public class eVA_ARVInvitedSuppliersTable extends ARVInvitedSuppliersTable
{
	public static final String ClassName = "config.java.ams.custom.eVA_ARVInvitedSuppliersTable";
	
	public void updateInvitedSuppliers()
	{
		Log.customer.debug("eVA_ARVInvitedSuppliersTable::updateInvitedSuppliers called");
		
		ProcureLineItem loPLI = (ProcureLineItem) super.pli();
		ariba.procure.core.CategoryLineItemDetails slid = CategoryUtil.getSLID(loPLI);
		if (slid.getCollaborate())
		{
			slid.setCollaborate(Boolean.FALSE);
			slid.setCollaborate(Boolean.TRUE);
		}
	}
	
	public boolean updateButtonVisible()
	{
		Log.customer.debug("eVA_ARVInvitedSuppliersTable::updateButtonVisible called");
		
		ProcureLineItem loPLI = (ProcureLineItem) super.pli();
		ariba.procure.core.CategoryLineItemDetails slid = CategoryUtil.getSLID(loPLI);
		if (slid.getCollaborate())
		{
			slid.setCollaborate(Boolean.FALSE);
			slid.setCollaborate(Boolean.TRUE);
		}
		return false;
	}
}
