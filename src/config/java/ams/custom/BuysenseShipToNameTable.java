// Decompiled by Decafe PRO - Java Decompiler
// Classes: 1   Methods: 4   Fields: 1

/* 08/11/2003: Updates for Ariba 8.0 (Jeff Namadan) */
/* 09/05/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Locale;

import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLNameTable;
import ariba.base.core.aql.AQLQuery;
import ariba.base.fields.ValueSource;
import ariba.common.core.Address;
import ariba.common.core.UserProfile;
import ariba.common.core.UserProfileDetails;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;
import ariba.workforce.core.LaborLineItemDetails;

// import ariba.util.log.Log;
// Referenced classes of package ariba.common.core.nametable:
// NamedObjectNameTable

/* Ariba 8.0: Replaced NamedObjectNameTable */
// 81->822 changed Vector to List
public class BuysenseShipToNameTable extends AQLNameTable
{

    public static final String ClassName = "ariba.common.core.nametable.AddressNameTable";

    public BuysenseShipToNameTable()
    {
        Log.customer.debug("In constructor for BuysenseAddressNameTable");
    }

    /* Ariba 8.0: Removed deprecated getTitleForObject(Named object, Locale locale) method */
    public String getTitleForObject(ValueSource object, Locale locale)
    {
        Log.customer.debug("In getTitleForObject- valuesource");
        String title = super.getTitleForObject(object, locale);
        /* Ariba 8.0: Replaced Util.nullOrEmptyOrBlankString */
        if ((object instanceof Address) && StringUtil.nullOrEmptyOrBlankString(title))
        {
            Log.customer.debug("In getTitleForObject- valuesource TITLE:%s", getDerivedTitle((Address) object));
            return getDerivedTitle((Address) object);
        }
        else
        {
            Log.customer.debug("In getTitleForObject- valuesource TITLE:%s", title);
            return title;
        }
    }

    private String getDerivedTitle(Address address)
    {
        if (address == null)
            return null;
        String name = address.getName();
        /* Ariba 8.0: Replaced Util.nullOrEmptyOrBlankString */
        if (!StringUtil.nullOrEmptyOrBlankString(name))
            return name;
        String lines = address.getLines();
        /* Ariba 8.0: Replaced Util.nullOrEmptyOrBlankString */
        if (!StringUtil.nullOrEmptyOrBlankString(lines))
        {
            BufferedReader reader = new BufferedReader(new StringReader(lines));
            if (reader != null)
                try
                {
                    name = reader.readLine();
                }
                catch (IOException _ex)
                {
                    // Log.constraints.warning(4000, lines);
                }
            else
            // Log.constraints.warning(4000, lines);
            /* Ariba 8.0: Replaced Util.nullOrEmptyOrBlankString */
            if (!StringUtil.nullOrEmptyOrBlankString(name))
                return name;
        }
        return address.getCity();
    }

    // *************************************************************************************
    public List matchPattern(String field, String pattern)
    {
        Log.customer.debug("Hello from BUYSENSEADDRESS NAME TABLE CLASS 1");
        List results = super.matchPattern(field, pattern);
        return results;
    }

    public void addQueryConstraints(AQLQuery query, String field, String pattern)
    {
        Log.customer.debug("Hello from BUYSENSEADDRESS NAME TABLE CLASS 2");

        // build the default set of field constraints for the pattern
        super.addQueryConstraints(query, field, pattern);
        beginSystemQueryConstraints(query);

        // add the constraints
        String addressType = "S";
        ValueSource approvable = getValueSourceContext();
        // ER #94
        BaseObject clientName = (BaseObject) clientName();
        if (clientName != null)
        {
            BaseId id = clientName.getBaseId();
            Log.customer.debug("Hello Washington -  from BuysenseShipToNameTable");

            // Create the AQL based on the ClientName that was returned
            AQLCondition clientNameCond = AQLCondition.buildEqual(query.buildField("ClientName"), id);
            query.and(clientNameCond);
        }
        AQLCondition addressTypeCond = AQLCondition.buildEqual(query.buildField("ShipToAddressType"), addressType);
        query.and(addressTypeCond);
        endSystemQueryConstraints(query);
    }

    /*
     * Ariba 8.0: NamedObjectNameTable was deprecated by AQLNameTable. This class was changed to
     * derive from AQLNameTable and the method constraints() is not in this class and isn't needed.
     * public List constraints (String field, String pattern) { Log.customer.debug("Hello from
     * BuysenseAddress NAME TABLE CLASS 3"); List constraints = super.constraints(field, pattern);
     * return constraints; }
     */

    private ValueSource clientName()
    {
        ValueSource context = getValueSourceContext();
        Log.customer.debug("Washington - This is the ValueSource Context:%s", context);
        if (context instanceof UserProfile)
        {
            return (ValueSource) context.getFieldValue("UserProfileDetails.ClientName");
        }
        if (context instanceof UserProfileDetails)
        {
            return (ValueSource) context.getFieldValue("ClientName");
        }
        if (context instanceof ariba.purchasing.core.ReqLineItem)
        {
            return (ValueSource) context.getFieldValue("ClientName");
        }
        if (context instanceof LaborLineItemDetails)
        {
            return (ValueSource) context.getDottedFieldValue("LineItem.ClientName");
        }
        if (context != null && context.toString().indexOf("ConsultingLineItemDetails") >= 0)
        {
            Log.customer.debug("Inside Categorylineitem Details Check");
            return (ValueSource) context.getDottedFieldValue("LineItem.ClientName");
        }
        if (context instanceof ariba.purchasing.core.Requisition || 
        		(context instanceof ariba.approvable.core.Approvable && context.getTypeName().equals("ariba.core.BuysenseUserProfileRequest")))
        {
            /*Log.customer.debug("Getting client name where context is BuysenseUserProfileRequest");
            ariba.user.core.User RequesterUser = (ariba.user.core.User)context.getDottedFieldValue("Requester");
            ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
            return (ValueSource)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName");*/
            return (ValueSource) context.getDottedFieldValue("Client");
        }
        else
            return null;

    }

}
