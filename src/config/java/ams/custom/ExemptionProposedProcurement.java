package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.ClusterRoot;
import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class ExemptionProposedProcurement extends Action
{
	private static String msCN = "ExemptionProposedProcurement";
	private static String msSubmitNote = "The following must be submitted and attached to this request: \n" +
            " A. If the following documents were not posted in VBO, provide a copy of the solicitation, including all addenda, and the contract award documents, including all modifications. \n" +
            " B. If solicitation was not posted in VBO, provide documentation that Virginia vendors were given an opportunity to participate in the solicitation and/or received an award. \n" + 
            " C. Verification that no Mandatory Source or State Contract exists to satisfy the requirement. \n" +
            " D. A memorandum that addresses the following: \n" +
                                            "a. Specifically how does the scope of work and pricing of this Cooperative Contract \n" +
												 "provide for the goods or services the agency is seeking to purchase? \n" +
                                            "b. Explain why the use of this Cooperative Contract is the best option for the \n" +
												 "Commonwealth, including why the prices offered in the Cooperative Contract are \n" + 
												 "considered fair and reasonable."; 
	
    public void fire(ValueSource valuesource, PropertyTable propertytable) throws ActionExecutionException
    {
    	Log.customer.debug("***%s***Called fire:: valuesource obj %s", msCN, valuesource);
    	Approvable loAppr = null;
       
        if(valuesource!= null && valuesource instanceof Approvable)
        {
           loAppr = (Approvable)valuesource;         
                  
           Boolean bCooperativeContract = (Boolean) loAppr.getDottedFieldValue("CooperativeContractCB1");
           
           if(bCooperativeContract != null && bCooperativeContract)
           {
        	   Log.customer.debug("***%s***fire::Setting submit note %s", msCN, msSubmitNote);
        	   loAppr.setFieldValue("SubmitNote", msSubmitNote);
        	   loAppr.setFieldValue("ContactEx", "For Example: Contact (firstname lastname, phone, email)");
        	  // loAppr.setFieldValue("OtherProcurement", null);
           }
           else
           {
        	   Log.customer.debug("***%s***fire::Re-Setting values to null", msCN);
        	   loAppr.setFieldValue("SubmitNote", null);
        	   loAppr.setFieldValue("ContactEx", null);
        	   loAppr.setFieldValue("Contact", null);
        	   loAppr.setFieldValue("Issuedby", null);
        	   loAppr.setFieldValue("CooperativeContract", null);
        	  // loAppr.setFieldValue("OtherProcurement", null);
           }
        }
    }
}
