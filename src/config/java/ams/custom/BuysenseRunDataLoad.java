package config.java.ams.custom;

import java.io.*;
import java.util.Iterator;
import java.util.Map;

import ariba.base.core.Base;
import ariba.base.core.Partition;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;
import ariba.util.scheduler.ScheduledTask;
import ariba.util.scheduler.Scheduler;

public class BuysenseRunDataLoad extends ScheduledTask implements BuyintConstants {
	private static String msCN = "BuysenseRunDataLoad";
	private static Partition moPart = Base.getSession().getPartition();
	private String msDataLoadScript = null;

	public void run() {
		String s = null;

		try {

			// run the Unix "ps -ef" command
			// using the Runtime exec method:
			// Process p = Runtime.getRuntime().exec("ps -ef");
			if (!StringUtil.nullOrEmptyOrBlankString(msDataLoadScript)) {
				Process p = Runtime.getRuntime().exec(msDataLoadScript);

				BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

				BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

				// read the output from the command
				Log.customer.debug(msCN + ": Here is the standard output of the command:\n");
				while ((s = stdInput.readLine()) != null) {
					Log.customer.debug(s);
				}
				Log.customer.debug(msCN + ": DONE printing standard output of the command:\n");

				// read any errors from the attempted command
				Log.customer.debug(msCN + ": Here is the standard error of the command (if any):\n");
				while ((s = stdError.readLine()) != null) {
					Log.customer.debug(s);
				}
				Log.customer.debug(msCN + ": DONE printing standard error of the command:\n");
			} else {
				Log.customer.debug(msCN + ": msDataLoadScript is null: ");
			}

				Log.customer.debug(msCN + ": DONE all processig and now can exit\n");
		} catch (IOException e) {
			Log.customer.debug(msCN + ": exception happened - here's what I know: ");
			Log.customer.error(7000, "::run() exception4 in processing cause = " + e.getCause());
			Log.customer.error(7000, "::run() exception4 in processing mesg = " + e.getMessage());
			Log.customer.error(7000, "::run() exception4 = " + getStackMessg(e));
			//e.printStackTrace();
		}
				Log.customer.debug(msCN + ": DONE all processing and now can exit\n");
	}

	public void init(Scheduler scheduler, String scheduledTaskName, Map arguments) {
		super.init(scheduler, scheduledTaskName, arguments);

		if (moPart == null) {
			moPart = Base.getService().getPartition("pcsv");
		}

		for (Iterator loItr = arguments.keySet().iterator(); loItr.hasNext();) {
			String lsKey = (String) loItr.next();
			if (lsKey.equals("DataLoadScript")) {
				msDataLoadScript = (String) arguments.get(lsKey);
				Log.customer.debug(msCN + " msDataLoadScript Connection parameter specified as " + msDataLoadScript + ".");
			}
		}

	}

	private String getStackMessg(Exception ex)
    {
        StackTraceElement[] arr = ex.getStackTrace();
        String stackMessg = "";

        ex.printStackTrace();

        if (ex.getMessage() != null)
        {
            stackMessg = stackMessg + ex.getMessage() + "\n";
        }
        if (ex.getCause() != null)
        {
            stackMessg = "cause:" + stackMessg + ex.getCause() + "\n";
        }
        for (int i = 0; i < arr.length; i++)
        {
            stackMessg = stackMessg + (arr[i].toString()) + "\n";
        }
        return stackMessg;
    }
}
