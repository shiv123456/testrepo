/*
 * @(#)SetPOFields.java     1.0 01/21/2004
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 * PVCS Modification :
 *
 * $Log:   R:\Software\development\archives\Ariba 8.x Customizations\app\buyer\Server\classes\extensions\config\java\ams\custom\SetPOFields.java-arc  $
 * 
 *    Rev 1.2   18 Aug 2005 14:41:48   rlee
 * ER#119 - P&C - Ariba changes.
 *
 *
 */
/* Responsible Ismail Mohideen */

// 10/10/2003: Updates for Ariba 8.1 (Richard Lee)
// Replaced all the elementAt with get
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

/**
    This class gets called from AMSPOPrep. Gets the associated Req and goes through the each line
    and each accounting in the line and copies it over to the PO.
*/
/**
ST SPL 414 :imohideen:  We get the list of line fields to be copied from the getReqToPOFields now
                        which includes Billing Address
rlee: 4/23/03: NumberOnReq causes stack trace when an item is deleted during Change Order.
      Better to use LineItems.NumberInCollection instead
*/
package config.java.ams.custom;

import ariba.purchasing.core.PurchaseOrder;
//Deprecated import ariba.base.fields.ValueInfo;
import ariba.util.core.Date;
import java.util.List;
import ariba.base.core.BaseObject;
import ariba.util.log.Log;
import ariba.approvable.core.Comment;

//81->822 changed Vector to List
public class SetPOFields
{
    static List actgFieldsVector = FieldListContainer.getAcctgFieldValues();
    static List lineFieldsVector = FieldListContainer.getReqToPOFields();
    public static void copyFields (PurchaseOrder po)
    {
        Log.customer.debug("Calling SetPOFields. In copyFields method");
        List POLIVector = po.getLineItems();
        List ReqLIVector = (List)((BaseObject)POLIVector.get(0)).getDottedFieldValue("Requisition.LineItems");
        int POliSize = POLIVector.size();
        int ReqLiSize = ReqLIVector.size();
        List POCommentsVector = po.getComments();
        List ReqCommentsVector = (List)((BaseObject)POLIVector.get(0)).getDottedFieldValue("Requisition.Comments");
        int liPOCommentSize = POCommentsVector.size();
        int liReqCommentSize = ReqCommentsVector.size();
        Boolean lbPandC = null;
        Comment POCommentObject = null;
        Comment ReqCommentObject = null;
        Date ldPOTime = null;
        Date ldReqTime = null;

        for (int liCount = 0 ;liCount < POliSize;liCount++)
        {
            BaseObject poLI = (BaseObject)POLIVector.get(liCount);

            /* This is important as NumberOnReq tells what is the right ReqLineItem object is */
            int numberonReq = ((Integer) poLI.getFieldValue("NumberOnReq")).intValue();

            for (int rliCount = 0; rliCount < ReqLiSize; rliCount++)
            {
                BaseObject reqLI = (BaseObject)ReqLIVector.get(rliCount);
                int ReqNIC = ((Integer) reqLI.getFieldValue("NumberInCollection")).intValue();
                if (ReqNIC == numberonReq)
                {
                    Log.customer.debug("rlee68, inside ReqNIC = numberonReq");
                    copyLineFields(poLI,reqLI);
                    copyAcctgFields(poLI,reqLI);
                }
            }
        }
        /*
         * CR 119 Proprietary and Confidential.  Match Req and PO Comment Date to update PandC field
         */
        for(int liPOCount =0 ; liPOCount < liPOCommentSize; liPOCount++)
        {
            POCommentObject = (Comment)POCommentsVector.get(liPOCount);
            ldPOTime = (Date) POCommentObject.getDottedFieldValue("Date");

            for(int liReqCount = 0; liReqCount < liReqCommentSize; liReqCount++)
            {
                ReqCommentObject = (Comment)ReqCommentsVector.get(liReqCount);
                ldReqTime = (Date) ReqCommentObject.getFieldValue("Date");
                if((ldReqTime != null) && (ldPOTime != null) && (ldReqTime.compareTo(ldPOTime) == 0))
                {
                    lbPandC = (Boolean) ReqCommentObject.getDottedFieldValue("ProprietaryAndConfidential");
                    POCommentObject.setFieldValue("ProprietaryAndConfidential", lbPandC);
                    break;
                }
            }
        }
    }
    private static void copyAcctgFields (BaseObject poLIbo,BaseObject reqLIbo)
    {
        List POActgVector =(List)poLIbo.getDottedFieldValue("Accountings.SplitAccountings");
        List ReqActgVector = (List)reqLIbo.getDottedFieldValue("Accountings.SplitAccountings");
        int actgSize = POActgVector.size();
        for (int actgCount = 0; actgCount < actgSize; actgCount++)
        {
            BaseObject POActg = (BaseObject)POActgVector.get(actgCount);
            BaseObject ReqActg = (BaseObject)ReqActgVector.get(actgCount);
            int actgFieldSize = actgFieldsVector.size();
            for (int actgFieldsCount = 0; actgFieldsCount < actgFieldSize; actgFieldsCount++)
            {
                POActg.setDottedFieldValue((String)actgFieldsVector.get(actgFieldsCount),ReqActg.getDottedFieldValue((String)actgFieldsVector.get(actgFieldsCount)));
            }
        }
    }

    private static void copyLineFields (BaseObject poLIbo,BaseObject reqLIbo)
    {
        int lineFieldSize = lineFieldsVector.size();
        for (int lineFieldsCount = 0;lineFieldsCount<lineFieldSize;lineFieldsCount++)
        {
            poLIbo.setDottedFieldValue((String)lineFieldsVector.get(lineFieldsCount),reqLIbo.getDottedFieldValue((String)lineFieldsVector.get(lineFieldsCount)));
        }
    }


}
