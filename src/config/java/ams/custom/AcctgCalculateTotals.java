// Version 1.1

// Anup - July 2001

// Dev SPL 219 - ahiranandani - Call to PVDetailDefaulter replaced by CalculateTotals


/* 08/27/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* As of release 8.0 ConditionValueInfo is deprecated by ValueInfo. However, Ariba has
   decided to keep ConditionValueInfo for now for backward compatibility. Wherever possible
   We have replaced ConditionValueInfo by ValueInfo. That can't be done here because the
   overridden methods in this module have a return type of ConditionValueInfo at a higher
   level. When Ariba decided to remove ConditionValueInfo completely, this module can be
   updated to have ValueInfo instead of ConditionValueInfo. */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.base.fields.*;
import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.base.core.*;


// 81->822 changed from ConditionValueInfo to ValueInfo
public class AcctgCalculateTotals extends Action
{
    static final String Value = "Value";
    private static final ValueInfo[] parameterInfo =
        {
        new ValueInfo(TargetParam, true, IsScalar, StringClass)
    };
    
    private static final String[] requiredParameterNames = 
        {
        TargetParam 
    };
    
    private static final ValueInfo valueInfo =
        new ValueInfo(IsScalar, "ariba.core.AcctLink");
    
    
    public void fire (ValueSource object, PropertyTable params)
    {
        Log.customer.debug("Inside fire of Acctg Calculate Totals");
        BaseObject bo = (BaseObject)object;
        ClusterRoot cr = (ClusterRoot)bo.getClusterRoot();
        CalculateTotals ct = new CalculateTotals();
        ct.fire((ValueSource)cr, params);
    }
    
    /**
        Return the list of valid value types.
    */
    protected ValueInfo getValueInfo ()
    {
        return valueInfo;
    }
    
    /**
        Return the parameter names.
    */    
    protected ValueInfo[] getParameterInfo ()
    {
        return parameterInfo;
    }
    
    /**
        Return the required parameter names.
    */
    protected String[] getRequiredParameterNames ()
    {
        return requiredParameterNames;
    }
    
    
}
