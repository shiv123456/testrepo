    package config.java.ams.custom;
//Checks the field visibility of SendEmallReqToQQ. If true ReqHeadCB5 will be visible on Req.
    import ariba.base.core.BaseObject;
    import ariba.base.fields.Condition;
    import ariba.base.fields.ConditionEvaluationException;
    import ariba.base.fields.ValueInfo;
import ariba.purchasing.core.Requisition;
import ariba.user.core.User;
    import ariba.util.core.PropertyTable;

import ariba.util.log.Log;

public class BuysenseCheckEmallToQQ extends Condition
{
    
    private static final ValueInfo parameterInfo[];
    private String                 sClassName = "BuysenseCheckEmallToQQ";
    public boolean evaluate(Object value, PropertyTable params) throws ConditionEvaluationException
    {
        BaseObject obj1   = (BaseObject)params.getPropertyForKey("SourceObject");
        Log.customer.debug(sClassName + "::evaluate() SourceObject " + obj1);
        
        if (obj1 instanceof ariba.purchasing.core.Requisition)
        {
            Log.customer.debug(sClassName + "::evaluate() Requisition");
            Requisition req = (Requisition)obj1;
            User requester = req.getRequester();
            ariba.common.core.User partitionedUser = ariba.common.core.User.getPartitionedUser(requester, req.getPartition());
            Boolean oClientSendEmallReqToQQ = (Boolean) partitionedUser.getDottedFieldValue("ClientName.SendEmallReqToQQ");
            Log.customer.debug(sClassName + "::evaluate() lbSendEmallReqToQQ: " + oClientSendEmallReqToQQ);

            if (req.getFieldValue("TransactionSource") != null)
            {
                Log.customer.debug("inside if of TransactionSource :: BuysenseCheckEmallToQQ");

                    return false;
            }

            if (oClientSendEmallReqToQQ != null)
            {
                if (oClientSendEmallReqToQQ.booleanValue())
                {
                    return true;
                }
                else
                {
                Log.customer.debug("inside else of ::BuysenseCheckEmallToQQ oSendEmallReqToQQ!=null && oSendEmallReqToQQ.booleanValue() ");
                   req.setDottedFieldValueWithoutTriggering("ReqHeadCB5Value", Boolean.FALSE);
                  return false;
                }
              }                  
        }
        else
        {
            Log.customer.debug(sClassName + "::evaluate() object is NOT a Requisition");
            return false;
        }
        return false; 
    }            
    protected ValueInfo[] getParameterInfo()
    {
       return parameterInfo;
    }   
    static
    {
         parameterInfo = (new ValueInfo[] {new ValueInfo("SourceObject", 0)});
    }
}

            