/*



    Responsible: imohideen

*/



package config.java.ams.custom.NameTables;



import ariba.base.core.aql.AQLQuery;
import config.java.ams.custom.FieldDataNameTable;

public class FD16NameTable extends FieldDataNameTable
{
    public void addQueryConstraints (AQLQuery query,
                                     String field, String pattern)
    
    {
        super.setFieldName("FieldDefault16");
        super.addQueryConstraints (query, field, pattern);
    }
    
}

