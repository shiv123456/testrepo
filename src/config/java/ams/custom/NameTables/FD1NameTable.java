/*



    Responsible: imohideen

*/



package config.java.ams.custom.NameTables;


import ariba.base.core.aql.AQLQuery;
import config.java.ams.custom.FieldDataNameTable;

public class FD1NameTable extends FieldDataNameTable
{
    public void addQueryConstraints (AQLQuery query,
                                     String field, String pattern)
    
    {
        super.setFieldName("FieldDefault1");
        super.addQueryConstraints (query, field, pattern);
    }
    
}

