package config.java.ams.custom;

import java.util.ArrayList;

import ariba.base.core.BaseId;

public class BuysenseDW2ControlObj
{

    private ArrayList<BaseId> loBaseIdsList;
    private boolean           bEXTRACT_CRITERIA_Null_Flag;

    public ArrayList<BaseId> getBaseIdsList()
    {
        return loBaseIdsList;
    }

    public void setBaseIDsList(ArrayList<BaseId> loBaseIDs)
    {
        this.loBaseIdsList = loBaseIDs;
    }

    public boolean getEXTRACT_CRITERIA_Null_Flag()
    {
        return bEXTRACT_CRITERIA_Null_Flag;
    }

    public void setEXTRACT_CRITERIA_Null_Flag(boolean bEXTRACT_CRITERIA_Null_Flag)
    {
        this.bEXTRACT_CRITERIA_Null_Flag = bEXTRACT_CRITERIA_Null_Flag;
    }

    @Override
    public String toString()
    {
        return "BuysenseDW2ControlObj [loBaseIdsList=" + loBaseIdsList + ", bEXTRACT_CRITERIA_Null_Flag=" + bEXTRACT_CRITERIA_Null_Flag + "]";
    }

    public BuysenseDW2ControlObj()
    {
        this.loBaseIdsList = new ArrayList<BaseId>();
        this.bEXTRACT_CRITERIA_Null_Flag = false;
    }

}
