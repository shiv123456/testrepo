/*
 * @(#)AMSPOPrep.java     1.0 05/14/2001
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 * PVCS Modification :
 *
 * $Log:   R:\Software\development\archives\Ariba 8.x Customizations\app\buyer\Server\classes\extensions\config\java\ams\custom\AMSPOPrep.java-arc  $
 * 
 *    Rev 1.17   08 Jun 2007 14:34:34   arohaly
 * VEPI Prod #1112: Increment POB UniqueName to prevent dups
 * 
 *    Rev 1.16   17 Apr 2006 11:18:18   rlee
 * Dev SPL 732 - INT - New Flag for ByPass Integration.
 * 
 *    Rev 1.15   02 Mar 2006 13:55:56   rlee
 * Dev SPL 732 - INT - New Flag for ByPass Integration.
 * 
 *    Rev 1.14   01 Feb 2006 16:38:46   rlee
 * Dev SPL 719 - INT - Set EncumbranceStatusChangeDate with trigger.
 * 
 *    Rev 1.13   06 Oct 2005 15:27:28   rlee
 * Prod SPL 819 - Update to AMSPOPrep.java
 *
 *    Rev 1.12   28 Sep 2005 14:23:26   rlee
 * Prod SPL 819 - Update to AMSPOPrep.java
 *
 *
 *
 * Responsible : imohideen
*/

/* 09/10/2003: Updates for Ariba 8.0 (David Chamberlain) */
/*
03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan)
03/04/2004: rgiesen dev SPL #38 - replace getLeadCurrency with Currency.getDefaultCurrency(partition)
03/11/2004: rgiesen dev SPL #39 - replaced Base.getSession().getPartition() with a better method
09/04/2004: Ahiranan Dev SPL #85 - Ariba 8.1 Upgrade for Encumbrance.
*/

package config.java.ams.custom;

import ariba.purchasing.core.Requisition;
import ariba.purchasing.core.PurchaseOrder;
import ariba.purchasing.core.OrderRecipient;
import ariba.purchasing.core.ReqLineItem;
import ariba.purchasing.core.POLineItem;
import ariba.base.core.Base;
import ariba.util.log.Log;
import ariba.base.core.Partition;
import ariba.base.core.ClusterRoot;
import ariba.base.core.*;
import java.math.BigDecimal;
// Ariba 8.1: Util has been deprecated
//import ariba.util.core.Util;
import java.util.List;
import org.apache.log4j.Level;
// Ariba 8.1: Money has moved
import ariba.basic.core.Money;
//import ariba.common.core.Money;
// Ariba 8.1: Currency has moved
import ariba.basic.core.Currency;
//import ariba.common.core.Currency;
// Ariba 8.0: These classes have been removed in Ariba 8.0.
//import ariba.procure.server.OrderRecipientOnServer;
//import ariba.procure.server.ERPOrderOnServer;
import ariba.approvable.core.Comment;
import ariba.approvable.core.LineItem;
import ariba.common.core.User;
import ariba.common.core.Core;
import ariba.util.core.SystemUtil;


public class AMSPOPrep implements AMSE2EConstants, BuyintConstants
{
    static int lineNum=0;
    static ariba.purchasing.core.POLineItem li=null;
    static PurchaseOrder PurchOrder =null;
    static final String msSuccess = "SUCCESS";
    static User moAribaSystemUser = null;

    /**
    Load the Buysense-related fields from vectors maintained in the FieldListContainer
    */
    // 81->822 changed Vector to List
    private static final List RefAcctgFieldsVector = (List)FieldListContainer.getRefAcctgFields();
    private static final List AcctgFieldsVector = (List)FieldListContainer.getAcctgChooserFieldValues();
    /**
    Sets up the PO extrinsics and calls the TXN.Write
    */
    public static int send( PurchaseOrder  po,
                           String tranType,OrderRecipient recipient)
    {
        // first retrieve client name
        List lineItems = (List)po.getFieldValue("LineItems");

        //We need to change the Requester from AribaSystem to the Requester of the Requisition
        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
        BaseObject userObject = (BaseObject)((BaseObject)lineItems.get(0)).getDottedFieldValue("Requisition.Requester");
        Log.customer.debug("This is the user:%s", userObject);
        if (userObject!= null) po.setDottedFieldValue("Requester",userObject);
        po.save();

        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        //String clientName = (String)((BaseObject)lineItems.get(0)).getDottedFieldValue("Requisition.Requester.BuysenseOrg.ClientName.ClientName");
        ariba.user.core.User RequesterUser = (ariba.user.core.User)(userObject);
        //rgiesen dev SPL #39
        //ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,Base.getSession().getPartition());
        Partition poPartition = po.getPartition();
        ariba.common.core.User RequesterPartitionUser = ariba.common.core.User.getPartitionedUser(RequesterUser,poPartition);
        String clientName = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientName");

        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
        String clientID = (String)RequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.ClientID");

        //The XML file's client tag is expected to be in the following format
        //  clientID_ClientName

        String XMLClientTag = clientID.trim() + "_" + clientName.trim() ;

        //Setting the POERPStatus attribute for the Purchase Order to "Encumbering"
        Log.customer.debug("Setting the POERPStatus");
        po.setFieldValue("POERPStatus","Sent to BI");

        Log.customer.debug("AMSPOPrep was called . In send method");

        int result=0;
        moAribaSystemUser = Core.getService().getAribaSystemUser(poPartition);
        String erpNumber = (String)po.getFieldValue("UniqueName");

        Requisition loRequisition=null;
        // CSPL-302 SetOrderID on ERPOrder push
        String lsPOUniqueName = (String) po.getFieldValue("UniqueName");
        po.setFieldValue("OrderID",lsPOUniqueName);
        List lvPOLineItems = (List) po.getFieldValue("LineItems");
        List lvLineItems = null;
        POLineItem loPOLineItem = null;
        ReqLineItem loLineItem = null;

        try
        {
            for (int i=0; i<(lvPOLineItems.size()); i++)
            {
                loPOLineItem = (POLineItem)lvPOLineItems.get(i);
                loPOLineItem.setFieldValue("OrderID",lsPOUniqueName);
            }
            lvLineItems = (List) loPOLineItem.getDottedFieldValue("Requisition.LineItems");
            for (int j=0; j<(lvLineItems.size()); j++)
            {
                loLineItem = (ReqLineItem)lvLineItems.get(j);
                if( ((PurchaseOrder)loLineItem.getFieldValue("Order")) != null)
                {
                    lsPOUniqueName = (String) loLineItem.getDottedFieldValue("Order.UniqueName");
                    loLineItem.setDottedFieldValue("OrderID",lsPOUniqueName);
                }
            }
            loPOLineItem = (POLineItem)lvPOLineItems.get(0);
            loRequisition = (Requisition)loPOLineItem.getFieldValue("Requisition");
        }
        catch (Exception loE)
        {
            Log.customer.debug("AMSPOPrep: Caught exception while setting LineItems vector OrderID field. " );
            Log.customer.debug(SystemUtil.stackTrace(loE));
        }

        if (tranType.equals("cancel"))
        {


            //As we have not implemented a pullback for Liqudation. We will go ahead and move the recipient to
            //canceled at this point.
            if (recipient.isERPSender())
            {
                Log.customer.debug("Canceling the Recipient: %s",recipient);
                /* Ariba 8.0: The OnServer Classes are no longer in Ariba 8.0. Ariba tech
                   support and the migration pdf say they must be removed. */
                //OrderRecipientOnServer.onServer(recipient).canceledOrderSent(recipient);
                // Ariba 8.1: OrderRecipient::canceledOrderSent is no an opent API. The method signature has changed.
                // to accomodate this change, the 'recipient' parameter has to be removed from the method call.
                recipient.canceledOrderSent();
                // Ariba 8.1 Upgrade - Send encumbrance cancellation transaction
                if(((Boolean)po.getFieldValue("Encumbered")).booleanValue())
                {
                po.setFieldValue("EncumbranceStatus", STATUS_ERP_CANCELLED);
                po.setFieldValue("Encumbered", new Boolean(false));
                BuysenseUtil.createHistory(po, erpNumber, RECORD_ENC_CANCEL, null, moAribaSystemUser);
                BuysenseUtil.createHistory(loRequisition, erpNumber, RECORD_ENC_CANCEL, null, moAribaSystemUser);
                BuyintXMLFactory.submit(TXNTYPE_CANCEL, po);
                }
            }
        }
        else
        {
            // Temporary code for PVPush to pick up the Orders
            po.setFieldValue("PaidState", new Integer(1));
            po.save();

            List LineItems = (List)po.getFieldValue("LineItems");
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            ariba.purchasing.core.POLineItem line = (ariba.purchasing.core.POLineItem)LineItems.get(0);
            System.out.println("PoLine Item : "+ line.toString());


            //ff String erpNumber = "PC" + (String) line.dottedFieldValue("Accounting.Field1.Name")
            //ff  + (String)po.fieldValue("UniqueName");
            //String erpNumber = (String)po.getFieldValue("UniqueName");
            System.out.println("ERPnumber " + erpNumber);
            /** set up the accounting reference vector */
            boolean linkingOption = Base.getService().getBooleanParameter(po.getPartition(), "Application.AMSParameters.EPSLinkingOption");

            SetPOFields.copyFields(po);

            if (linkingOption)
            {
                setActgHeaderReference(po);
            }

            // Take care of copying the comments to Header and Line placeholders
            Log.customer.debug("Copying Order comments to placeholders...");
            List poComments = po.getComments();
            // Ariba 8.1: List::count() is deprecated by List::size()
            for ( int i = 0; i < poComments.size(); i++ )
            {
                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                Comment orderComment = (Comment)poComments.get(i);
                Log.customer.debug("Got comment #: " + i);
                if ( orderComment.getFieldValue("LineItem") == null )
                {
                    //Got a Header comment
                    if ( po.getFieldValue("HeaderCommentText") == null )
                    {
                        //The first Header comment, so save it to a header comment placeholder
                        LongString ls = (LongString)(orderComment.getFieldValue("Text"));
                        String s1 = ls.string();
                        Log.customer.debug("Save the Header comment: " + s1);
                        po.setFieldValue( "HeaderCommentText", s1 );
                    }
                }
                else
                {
                    //Got a Line comment, so capture the line
                    LineItem commentLineItem = (LineItem)orderComment.getFieldValue("LineItem");
                    if ( commentLineItem.getFieldValue("LineCommentText") == null )
                    {
                        //The first Line comment, so save it to a line comment placeholder
                        LongString ls2 = (LongString)(orderComment.getFieldValue("Text"));
                        String s2 = ls2.string();
                        Log.customer.debug("Save the Line comment: " + s2);
                        commentLineItem.setFieldValue( "LineCommentText", s2 );
                    }
                }
            }
            //e2e End:

            po.save();

            //e2e Start:
            //rgiesen dev SPL #39
            //String solicitationVendor = Base.getService().getParameter(Base.getSession().getPartition(),

            //Logs.buysense.setDebugOn();
            Logs.buysense.setLevel(Level.DEBUG);
            String solicitationVendor = Base.getService().getParameter(poPartition,
                                                                       "Application.AMSParameters.SolicitationVendor");
            if (((String) po.getDottedFieldValue("LineItems[0].Supplier.UniqueName")).compareTo
                    (solicitationVendor) == 0)
            {
                po.setFieldValue("EncumbranceStatus", STATUS_ERP_SENT_TO_EPRO);
                po.setFieldValue("Encumbered", new Boolean(false));
                Log.customer.debug("Pushing the UR");
                AMSTXN.initMappingData();
                result = AMSTXN.push(XMLClientTag,
                                     "UR",
                                     erpNumber,
                                     "NEW",
                                     "ATT",
                                     "A",
                                     "0",
                                     "BATCH",
                                     po,
                                     "LineItems",
                                     "Accountings.SplitAccountings",
                                     po.getPartition(),
                                     true);
            }
            else
            {
                // AKH - Send encumbrance creation transaction, if necessary

                if (isEncumbranceRequired(po))
                {
                   po.setFieldValue("POERPStatus","Sent to ERP");
                   po.setFieldValue("EncumbranceStatus", STATUS_ERP_INPROGRESS);
                   Log.customer.debug("AKH - Encumbrance is required.");
                   po.setFieldValue("Encumbered", new Boolean(false));
                   BuysenseUtil.createHistory(po, erpNumber, RECORD_ENCUMBERING, null, moAribaSystemUser);
                   BuysenseUtil.createHistory(loRequisition, erpNumber, RECORD_ENCUMBERING, null, moAribaSystemUser);
                   BuyintXMLFactory.submit(TXNTYPE_SEND, po);
                }
                else
                {
                AMSTXN.initMappingData();
                result = AMSTXN.push(XMLClientTag,
                                     "PC",
                                     erpNumber,
                                     "NEW",
                                     "RDY",
                                     "A",
                                     "0",
                                     "BATCH",
                                     po,
                                     "LineItems",
                                     "Accountings.SplitAccountings",
                                     po.getPartition());
            }
            }
            //Logs.buysense.setDebugOff();
            Logs.buysense.setLevel(Level.DEBUG.OFF);
            //e2e End:
        }

        return result;
    }


    //Method to determine if encumbrance is required for a po
    public static boolean isEncumbranceRequired(PurchaseOrder loPO)
    {
       boolean lboolIntegrate = false;
       ariba.user.core.User loUser = (ariba.user.core.User)(((POLineItem)loPO.getDottedFieldValue("LineItems[0]")).getRequisition().getRequester());
       ariba.common.core.User loPartUser = ariba.common.core.User.getPartitionedUser(loUser, loPO.getPartition());

       //Encumbrance flag and limit
       boolean lboolEnc = ((Boolean)loPartUser.getDottedFieldValue("BuysenseOrg.ClientName.Encumbrance")).booleanValue();
       BigDecimal loLimit = (BigDecimal)loPartUser.getDottedFieldValue("BuysenseOrg.EncumbranceLimit");

       // Transaction source
       String lsTranSrc = (String)loPO.getDottedFieldValue("LineItems[0].Requisition.TransactionSource");
       boolean lboolPass = ((Boolean)loPO.getDottedFieldValue("LineItems[0].Requisition.BypassApprovers")).booleanValue();

       // BypassPreEncumbranceEncumbrance
       Partition poPartition = loPO.getPartition();
       String lsBypassEncumbranceField = Base.getService().getParameter(poPartition,"Application.AMSParameters.BypassEncumbranceField");
       lsBypassEncumbranceField = "LineItems[0].Requisition."+lsBypassEncumbranceField;
       Boolean lboolBypassERP = (Boolean)loPO.getDottedFieldValue(lsBypassEncumbranceField);

       if (!lboolEnc)
       {
          loPO.setFieldValue("EncumbranceStatus", STATUS_ERP_NOT_ENABLED);
       }
       else if ((loPO.getTotalCost().getAmount()).compareTo(loLimit) < 0)
       {
          loPO.setFieldValue("EncumbranceStatus", STATUS_ERP_BELOW_THRESHOLD);
       }
       else if( (lboolBypassERP != null) && lboolBypassERP.booleanValue())
       {
          loPO.setFieldValue("EncumbranceStatus", STATUS_ERP_USER_BYPASSED);
       }
       else if (lsTranSrc == null                                        ||
                (lsTranSrc.trim()).equals("")                            ||
                lsTranSrc.trim().equalsIgnoreCase(TXNSOURCE_E2EPROCURE)  ||
                lsTranSrc.trim().equalsIgnoreCase(TXNSOURCE_QUICKQUOTE)  ||
                (lsTranSrc.trim().equalsIgnoreCase(TXNSOURCE_INTERFACES) && ! lboolPass)
          )
       {
          lboolIntegrate = true;
       }
       else if (lsTranSrc.trim().equalsIgnoreCase(TXNSOURCE_INTERFACES) && lboolPass)
       {
          loPO.setFieldValue("EncumbranceStatus", STATUS_ERP_EXTERNAL_ORDER);
       }

       //Null out PO Integration # and ISR in case integration is not required
       if(! lboolIntegrate)
       {
          loPO.setFieldValue("EncumbranceNumber", null);
          List loPOLIVector = loPO.getLineItems();
          List loReqLIVector = ((POLineItem)loPO.getDottedFieldValue("LineItems[0]")).getRequisition().getLineItems();
          int liPOLISize = loPOLIVector.size();
          int liReqLISize = loReqLIVector.size();
          int liNumberOnReq = 0;
          POLineItem loPOLine = null;
          ReqLineItem loReqLine = null;

          //Set the Encumbrance number and Signer Rule on Req Line
          for (int liPOCount = 0 ;liPOCount < liPOLISize; liPOCount++)
          {
             loPOLine = (POLineItem)loPOLIVector.get(liPOCount);

             liNumberOnReq = ((Integer) loPOLine.getFieldValue("NumberOnReq")).intValue();

             for (int liReqCount = 0; liReqCount < liReqLISize; liReqCount++)
             {
                loReqLine = (ReqLineItem)loReqLIVector.get(liReqCount);
                if (liNumberOnReq == loReqLine.getNumberInCollection())
                {
                   loReqLine.setFieldValue("EncumbranceNumber", null);
                   loReqLine.setFieldValue("LineIntSignerRule", null);
                   break;
                }
             }
          }
       }

       return lboolIntegrate;

    }
    private static void  setActgHeaderReference(PurchaseOrder  order)
    {

        PurchOrder = order;
        int i;
        int numOfUniqueLines = 0;
        List LineItems = (List)PurchOrder.getFieldValue("LineItems");
        // Ariba 8.1: List::count() is deprecated by List::size()
        for (i=0;i<(LineItems.size());i++)
        {
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            li = (ariba.purchasing.core.POLineItem) LineItems.get(i);
            //Get SplitAccounting vector
            List SplitAccounting = (List)li.getDottedFieldValue("Accountings.SplitAccountings");
            Log.customer.debug("SplitAccounting vector created!");
            // Ariba 8.1: List::count() is deprecated by List::size()
            for (int acctLinePerLineItem = 0; acctLinePerLineItem < (SplitAccounting.size()); acctLinePerLineItem++)
            {
                if (isLineUnique( (ariba.purchasing.core.POLineItem)li,
                                 acctLinePerLineItem))
                {
                    numOfUniqueLines++;

                    Log.customer.debug("numOfUniqueLines: %s", numOfUniqueLines );
                    Log.customer.debug("Matching Element: %s",  lineNum );

                    //ClusterRoot refActg = (ClusterRoot)ClusterRoot.create("ariba.core.RefAccounting", PurchOrder.getPartition());
                    BaseObject refActg = (BaseObject)BaseObject.create("ariba.core.ReferenceAccounting", PurchOrder.getPartition());

                    /**
                    Set the Accounting field values in the refAccounting object and
                    push it into vector
                    */
                    Log.customer.debug("Line Item %s", li.toString() );

                    refActg.setFieldValue("LineNum",
                                          new String(""+lineNum));
                    refActg.setFieldValue("RXRefLineNum",
                                          (String)li.getDottedFieldValue("Accountings.SplitAccountings[" + acctLinePerLineItem + "].RefRXHeaderLineNum"));

                    /**
                    Loop through static vectors and pick-up Buysense-related attribute names; Set field values on refActg by
                    dynamically creating the following command:
                    refActg.setFieldValue("Field1",(String)li.getDottedFieldValue
                                               ("Accountings.SplitAccountings[" + acctLinePerLineItem + "].FieldDefault1.Name"));
                    */
                    Log.customer.debug("IIB Finished creating RefAcctgFieldsVector and AcctgFieldsVector");
                    // Ariba 8.1: List::count() is deprecated by List::size()
                    for (int x = 0; x < RefAcctgFieldsVector.size(); x++)
                    {
                        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                        String RefAcctgField = (String)RefAcctgFieldsVector.get(x);
                        // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                        String AcctgField = (String)AcctgFieldsVector.get(x);
                        refActg.setFieldValue(RefAcctgField,
                                              (String)li.getDottedFieldValue
                                              ("Accountings.SplitAccountings[" + acctLinePerLineItem + "]." + AcctgField + ".Name"));
                    }

                    refActg.setFieldValue("LineAmount",
                                          (BigDecimal)li.getDottedFieldValue
                                          ("Accountings.SplitAccountings[" + acctLinePerLineItem + "].Amount.Amount"));
                    //added by Sunil - set the quantityRemaining field on the referenceAccounting class
                    setQuantityRemaining(li,refActg);
                    refActg.setFieldValue("RefReqUniqueName",
                                          (String)li.getDottedFieldValue("Requisition.UniqueName"));

                    //Log.customer.debug("IXM Got past setting values");
                    //refActg.save();

                    // Ariba 8.1: List::addElement() is deprecated by List::add()
                    ((List) PurchOrder.getFieldValue("AdvRefAccounting")).add(refActg);

                    li.setDottedFieldValue("Accountings.SplitAccountings[" + acctLinePerLineItem + "].RefPCHeaderLineNum",new String("" + lineNum));
                    //Log.customer.debug("IXM Got past setting the vector "  );

                }
                else
                {
                    li.setDottedFieldValue("Accountings.SplitAccountings[" + acctLinePerLineItem + "].RefPCHeaderLineNum",new String("" + lineNum));
                }
            }
        }
    }

    /**
    This method ensures the uniqueness of AdvRefAccounting elements
    */
    private static boolean isLineUnique(ariba.purchasing.core.POLineItem bo,
                                        int acctLinePerLineItem)
    {

        Log.customer.debug("At the begining of isLineUnique");
        List RefActgVector = (List) PurchOrder.getFieldValue("AdvRefAccounting");
        Log.customer.debug("At the begining of isLineUnique 2 ");
        int j;

        //String actgLine = (String)bo.getFieldValue("RefRXHeaderLineNum") ;
        //Retrieve the SplitAccountings amount information from the LineItems
        BigDecimal actgAmount = (BigDecimal)bo.getDottedFieldValue
            ("Accountings.SplitAccountings[" + acctLinePerLineItem + "].Amount.Amount");

        //Create actgLine string for comparisson with the existing refLine string

        /**
        Loop through static vectors and pick-up Buysense-related attribute names; Create actgLine string by
        dynamically creating the following command:
        String actgLine = (String)bo.getDottedFieldValue
                              ("Accountings.SplitAccountings[" + acctLinePerLineItem + "].FieldDefault1.Name") + [...]
        */
        ClusterRoot cr = (ClusterRoot)bo.getClusterRoot();
        //Ariba 8.1: Modified logic to get the Requester as ariba.user.core.User and then get the ariba.common.core.User
        //Partitioned User to obtain the extrinsic BuysenseOrg field value.
        //String clientUniqueName = (String)cr.getDottedFieldValue("Requester.BuysenseOrg.ClientName.UniqueName");
        ariba.user.core.User poRequesterUser = (ariba.user.core.User)cr.getDottedFieldValue("Requester");
        //rgiesen dev SPL #39
        Partition crPartition = cr.getPartition();
        //ariba.common.core.User poRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(poRequesterUser,Base.getSession().getPartition());
        ariba.common.core.User poRequesterPartitionUser = ariba.common.core.User.getPartitionedUser(poRequesterUser,crPartition);

        String clientUniqueName = (String)poRequesterPartitionUser.getDottedFieldValue("BuysenseOrg.ClientName.UniqueName");
        String UniqueName, targetFieldName;
        Boolean reqVisible;
        Log.customer.debug("IIB isLineUnique actgLine");
        String actgLine = "";
        // Ariba 8.1: List::count() is deprecated by List::size()
        for (int z = 0; z < RefAcctgFieldsVector.size(); z++)
        {
            // Summary Fix
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            targetFieldName = (String)RefAcctgFieldsVector.get(z);
            UniqueName = (clientUniqueName+":"+targetFieldName);
            //rgiesen dev SPL #39
            ClusterRoot fieldTable = Base.getService().objectMatchingUniqueName
                ("ariba.core.BuysenseFieldTable",
                crPartition, UniqueName);
            //     Base.getSession().getPartition(), UniqueName);
            reqVisible = ((Boolean)fieldTable.getFieldValue("VisibleOnReq"));
            if (reqVisible.booleanValue())
            {
                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                String tempZ = (String)AcctgFieldsVector.get(z);
                actgLine += (String)bo.getDottedFieldValue("Accountings.SplitAccountings[" + acctLinePerLineItem + "]." + tempZ + ".Name");
            }
        }

        Log.customer.debug("At the begining of isLineUnique 3 ");
        Log.customer.debug("This is the actgline : %s ", actgLine);

        // Ariba 8.1: List::count() is deprecated by List::size()
        for (j=0 ; (j < RefActgVector.size()); j++)
        {
            //BaseId refBaseId = (BaseId)RefActgVector.elementAt(j);
            //ClusterRoot crRefActg = Base.getService().getObjectWithReadLock(refBaseId);
            // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
            BaseObject crRefActg = (BaseObject)RefActgVector.get(j);
            Log.customer.debug("After casting");

            //Create refLine string

            /**
            Loop through static vectors and pick-up Buysense-related attribute names; Create refLine string by
            dynamically creating the following command:
            String refLine = (String)crRefActg.getDottedFieldValue("Field1") + [...]
            */
            Log.customer.debug("IIB refLine");
            String refLine = "";
            // Ariba 8.1: List::count() is deprecated by List::size()
            for (int y = 0; y < RefAcctgFieldsVector.size(); y++)
            {
                // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                targetFieldName = (String)RefAcctgFieldsVector.get(y);
                UniqueName = (clientUniqueName+":"+targetFieldName);
                //rgiesen dev SPL #39
                ClusterRoot fieldTable = Base.getService().objectMatchingUniqueName
                    ("ariba.core.BuysenseFieldTable",
                       crPartition, UniqueName);
                //     Base.getSession().getPartition(), UniqueName);
                reqVisible = ((Boolean)fieldTable.getFieldValue("VisibleOnReq"));
                if (reqVisible.booleanValue())
                {
                    // Ariba 8.1: List::elementAt(int) is deprecated by List::get(int)
                    String tempW = (String)RefAcctgFieldsVector.get(y);
                    refLine += (String)crRefActg.getDottedFieldValue(tempW);
                }
            }

            Log.customer.debug("This is the refline : %s", refLine);

            /**
            If the actgLine is the same as an existing refLine, its dollar
            amount will be added to the existing refLine, and no new refLine
            will be created
            */
            if (refLine.equals(actgLine))
            {
                lineNum = j+1;
                java.math.BigDecimal totalAmount = ((java.math.BigDecimal)crRefActg.getDottedFieldValue("LineAmount")).add(
                                                                                                                           (actgAmount));
                //added by Sunil - set the quantityRemaining field on the referenceAccounting class
                setQuantityRemaining(bo,crRefActg);
                Log.customer.debug("This is the total amount 1: %s", totalAmount);
                crRefActg.setFieldValue("LineAmount",totalAmount);
                return false;
            }
        }
        lineNum = j+1;
        Log.customer.debug("This is the line num in the method  : %s", lineNum);
        return true;
    }

    //added by Sunil - set the quantityRemaining field on the referenceAccounting class
    private static void setQuantityRemaining(BaseObject bo,
                                             BaseObject crRefActg)
    {
        BigDecimal numberToBeLiquidatedQty=(BigDecimal)crRefActg.getFieldValue("NumberToBeLiquidated");

        if (numberToBeLiquidatedQty == null)
        {
            numberToBeLiquidatedQty=new BigDecimal("0");
            // Ariba 8.1: Currency::getLeadCurrency() is no longer static. Changed to non-static call
         //rgiesen dev SPL #38 - replace getLeadCurrency with Currency.getDefaultCurrency(partition)
            crRefActg.setFieldValue("LiquidationAmount",
                                    new Money(0.00,
                                              Currency.getDefaultCurrency(bo.getPartition())));
        }

        numberToBeLiquidatedQty=numberToBeLiquidatedQty.add((BigDecimal)bo.getFieldValue("Quantity"));
        crRefActg.setFieldValue("NumberToBeLiquidated",numberToBeLiquidatedQty);


    }

}
