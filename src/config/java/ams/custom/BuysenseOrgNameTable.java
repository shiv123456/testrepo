package config.java.ams.custom;

import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLNameTable;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.SearchTermQuery;
import ariba.base.fields.ValueSource;
import ariba.util.log.Log;

public class BuysenseOrgNameTable extends AQLNameTable
{
    public static final String ClassName="config.java.ams.custom.BuysenseOrgNameTable";
    
    public void addQueryConstraints(AQLQuery query, String field, String pattern, SearchTermQuery searchTermQuery)
    {
    	Log.customer.debug( "Inside BuysenseOrgNameTable") ;
    	ValueSource valueSourceCurrent = getValueSourceContext();
    	super.addQueryConstraints(query, field, pattern, searchTermQuery);
    	String lsAgency = null;
    	if (valueSourceCurrent instanceof ariba.approvable.core.Approvable && valueSourceCurrent.getTypeName().equals("ariba.core.BuysenseUserProfileRequest"))
        { 
    	
    		lsAgency = (String)valueSourceCurrent.getFieldValue("Agency");
            if(lsAgency == null)
            {
                query.andFalse();
                return;
            }
            else
            {
        		String lsAgencyName = lsAgency.substring(0, 4);
                Log.customer.debug("The initial query is"+query);
                query.and(AQLCondition.parseCondition("  UniqueName like '" + lsAgencyName + "%' "));
                Log.customer.debug("The final query is"+query);            	
            }

        }
    	else
    	{
            query.andFalse();
            Log.customer.debug(ClassName + " No constraints are added to the Query : ");
    	}
    }

}
