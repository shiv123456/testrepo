
package config.java.ams.custom;

import ariba.base.fields.ValueSource;
import ariba.htmlui.workforce.fields.ARFPurchaseOrderChooser;
import ariba.purchasing.core.PurchaseOrder;
import ariba.util.core.ListUtil;
import ariba.util.core.MapUtil;
import ariba.util.log.Log;
import ariba.workforce.core.*;
import java.util.List;
import java.util.Map;


// 81->822 changed from ConditionValueInfo to ValueInfo
public class BuysenseARFOrder extends ARFPurchaseOrderChooser
{
    protected Object findChoices(ValueSource valueSourceContext, boolean returnOrderedList)
    {
        TimeSheet cr = (TimeSheet)valueSourceContext;
        List llidList = Contractor.getCurrentLaborLineItemDetails(cr.getRequester());
        Map poMap = MapUtil.map();
        List sortList = ListUtil.list();
        boolean isLastItem = false; // JB added item to track whether it is last order item in the drop-down list
        boolean isItemSelected = false; // JB to indicate whether item is selected

        // JB -- added another condition to verify that StatuString is not set to received. If order is received then we need not show it on the UI

        if(cr.getOrder() != null && (!cr.getOrder().getActive() || cr.getOrder().getOrderedState() != 4) && (!cr.getOrder().getStatusString().equals("Received")))
        {
        	Log.customer.debug("BuysenseARFOrder: query ");
            poMap.put(cr.getOrder().getUniqueName(), cr.getOrder());
            sortList.add(cr.getOrder().getUniqueName());
        }
        if(llidList != null )
        {
            for(int i = 0; i < llidList.size(); i++)
            {
                LaborLineItemDetails llid = (LaborLineItemDetails)llidList.get(i);
                PurchaseOrder po = (PurchaseOrder)llid.getLineItem().getLineItemCollection();
                // JB set 'isLastItem' to true for last order in case all orders are in 'received' state
                // JB We want to make sure that we display at least one item in the drop-down irrespective of the status
                if(i == (llidList.size() -1))
                {
                    isLastItem = true;
                }
                if ((isLastItem && !isItemSelected) || !po.getStatusString().equals("Received"))
                {
                	if(po.getStatusString().equals("Received"))
                	{
                		//if all the orders are in Received state then we will leave the OOTB order to be displayed in the UI.
                		Log.customer.debug("BuysenseARFOrder: All orders are in Received State. ");
                		PurchaseOrder polast = (PurchaseOrder)cr.getOrder();
                		if(polast!=null)
                		{
                			poMap.put(polast.getOrderID(), polast);                			
                		}                		
                	}
                	else
                	{
                		poMap.put(po.getOrderID(), po);
                        ListUtil.addElementIfAbsent(sortList, po.getOrderID());
                        isItemSelected = true;                		
                	}
                    
                }
            }

        }
        if(returnOrderedList)
        {
            ListUtil.sortStrings(sortList, true);
            List sortedObjList = ListUtil.list();
            for(int j = 0; j < sortList.size(); j++)
                sortedObjList.add(poMap.get(sortList.get(j)));

            return sortedObjList;
        } else
        {
            return poMap;
        }
    }


}
