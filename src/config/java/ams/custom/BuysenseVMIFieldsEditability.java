package config.java.ams.custom;

import ariba.base.core.BaseObject;
import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.base.fields.ValueInfo;
import ariba.util.core.PropertyTable;
import ariba.approvable.core.Approvable;
import ariba.util.core.ResourceService;
import ariba.base.core.*;

/**
 *  Date : 04-June-2009
 *  CER-8: eMall: Unfiltered "On behalf of" field
 *  Description : If the transaction is a POB the On Behalf Of field will not be editable.           
 */

public class BuysenseVMIFieldsEditability extends Condition 
{
	  private static final ValueInfo parameterInfo[];
	   
	  public boolean evaluate(Object value, PropertyTable params)throws ConditionEvaluationException 
	  {
		   BaseObject obj   = (BaseObject)params.getPropertyForKey("SourceObject");
		   String lsRadio1 = ResourceService.getString("buysense.eform", "Radio1", Base.getSession().getLocale());
		   String lsRadio2 = ResourceService.getString("buysense.eform", "Radio2", Base.getSession().getLocale());
		   Boolean lbItemITrelated1 = false;
		   Boolean lbItemITrelated2 = false;
		   Boolean lbItemITrelated3 = false;
		   Boolean lbItemITrelated4 = false;
		   Boolean lbItemITrelated5 = false;
		   String lsFormAction = null;
     	   if (obj != null && ((Approvable)obj).instanceOf("ariba.core.BuysenseVMIAssetTracking"))
			   {
				   lbItemITrelated1 = ((Boolean)obj.getFieldValue("ItemITrelated1"));
				   lbItemITrelated2 = ((Boolean)obj.getFieldValue("ItemITrelated2"));
				   lbItemITrelated3 = ((Boolean)obj.getFieldValue("ItemITrelated3"));
				   lbItemITrelated4 = ((Boolean)obj.getFieldValue("ItemITrelated4"));
				   lbItemITrelated5 = ((Boolean)obj.getFieldValue("ItemITrelated5"));
				   lsFormAction = (String)obj.getFieldValue("FormAction");
				   
				   if(((lbItemITrelated1 != null && lbItemITrelated1) || (lbItemITrelated2 != null && lbItemITrelated2) || (lbItemITrelated3 != null && lbItemITrelated3) || (lbItemITrelated4 != null && lbItemITrelated4) || (lbItemITrelated5 != null && lbItemITrelated5)) && ((lsFormAction!=null && lsFormAction.equalsIgnoreCase(lsRadio1))||(lsFormAction != null && lsFormAction.equalsIgnoreCase(lsRadio2))))
				   {
				   return true;   
			       }
			   }
			   else
			   {
				   return false;   
			   }			   
		   return false;
   }
	  protected ValueInfo[] getParameterInfo()
	  {
	        return parameterInfo;
	  }
	
	  static
      {
          parameterInfo = (new ValueInfo[] {
             new ValueInfo("SourceObject", 0)
            });
       }
  }