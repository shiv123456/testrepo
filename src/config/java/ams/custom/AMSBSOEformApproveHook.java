/************************************************************************************
 * Author:  Richard Lee
 * Date:    November 5, 2004
 * Revision History:
 *
 * Date              Responsible            Description
 * ----------------------------------------------------------------------------------
 * 11/05/2007        Richard Lee            CSPL-165
 *
 * @(#)AMSBSOEformApprove.java v.1.0   11/5/2007
 *
 * Copyright 2001 by American Management Systems, Inc.,
 * 4050 Legato Road, Fairfax, Virginia, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of American Management Systems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with American Management Systems, Inc.
 *
 *************************************************************************************/

/*
 * This program is called when user approves a BuysenseOrgEform.
 * This will prevent user from approving BuysenseOrgEform, if it is in Denied status.
 */

package config.java.ams.custom;
import ariba.approvable.core.Approvable;
import ariba.approvable.core.ApprovableHook;
import ariba.util.core.Constants;
import java.util.List;
import ariba.util.core.ListUtil;
import ariba.util.log.Log;

public class AMSBSOEformApproveHook implements ApprovableHook
{
    private static final List NoErrorResult=ListUtil.list(Constants.getInteger(0));

    // 81->822 changed Vector to List
    public List run (Approvable approvable)
    {
        Log.customer.debug("Calling AMSBSOEformApproveHook");
        if (approvable.instanceOf("ariba.core.BuysenseOrgEform"))
        {

            String BSOEformStatus = (String)approvable.getStatusString();
            if (BSOEformStatus.equals("Denied"))
            {
                return ListUtil.list(Constants.getInteger(-1),
                                         "Can not Approve " +
                                         "this BuysenseOrgEform. " +
                                         "It is already in Denied Status");
            }
        }
        Log.customer.debug("Done with AMSBSOEformApproveHook");
        return NoErrorResult;
    }
}
