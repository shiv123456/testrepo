package config.java.ams.custom;

import ariba.base.fields.Condition;
import ariba.base.fields.ConditionEvaluationException;
import ariba.receiving.core.ReceiveRecord;
import ariba.util.core.PropertyTable;
import ariba.util.log.Log;

public class BuysenseHistoryDateVisibility extends Condition
{

    public boolean evaluate(Object obj, PropertyTable propertytable) throws ConditionEvaluationException
    {
        Log.customer.debug("BuysenseHistoryDateVisibility: obj: %s ", obj);
        if(obj instanceof ReceiveRecord)
        {
            Log.customer.debug("BuysenseHistoryDateVisibility returning false for ReceiveRecord ");
            return false;
        }
        return true;
    }

}
