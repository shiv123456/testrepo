package config.java.ams.custom;

import ariba.base.fields.Action;
import ariba.base.fields.ActionExecutionException;
import ariba.base.fields.ValueSource;
import ariba.basic.core.Currency;
import ariba.basic.core.Money;
import ariba.procure.core.ContractibleProperties;
import ariba.procure.core.QualifiedMoney;
import ariba.procure.core.ValueQualifier;
import ariba.util.core.PropertyTable;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;
/**
 * Date     : 07-May-2010
 * CSPL-1792: ACP Consulting - Bill rate set to Zero for consulting Line
 *            This is a trigger to set 0 value for Bill rate if the Billrate is Blank
**/
public class BuysenseSetRateForConsultingLine extends Action
{
    public void fire(ValueSource object, PropertyTable params) throws ActionExecutionException
    {
        Log.customer.debug("Calling BuysenseSetRateForConsultingLine.java" + object);
        if (object != null && object.toString().indexOf("ConsultingContractibleProperties") >= 0)
        {
            if ((object.getFieldValue("Rate")) != null
                    && (StringUtil.nullOrEmptyOrBlankString(object.getFieldValue("Rate").toString())))
            {
                ContractibleProperties Conprop = (ContractibleProperties) object;
                QualifiedMoney rate = new QualifiedMoney(Conprop.getPartition());
                rate.setValue(new Money(0.00, Currency.getDefaultCurrency(Conprop.getPartition())));
                rate.setQualifier(ValueQualifier.getNegotiableQualifier(Conprop.getPartition()));
                object.setFieldValue("Rate", rate);
            }
        }
    }
}
