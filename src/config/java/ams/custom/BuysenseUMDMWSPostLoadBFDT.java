package config.java.ams.custom;

import ariba.base.core.Base;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;

public class BuysenseUMDMWSPostLoadBFDT extends BuysenseUMDMWSPostLoad
{

    @Override
    boolean checkIfUniqueAribaInstanceExists(ClusterRoot oCustomObj)
    {
        //check if BFDT object exists with the same uniquename as custom object.
        Object cObjUN = oCustomObj.getFieldValue("UniqueName");
        if (cObjUN != null && (cObjUN instanceof String))
        {
            String sUniqueName = (String) cObjUN;
            Partition pcsvPartition = Base.getService().getPartition("pcsv");
            Object oBFDT = Base.getService().objectMatchingUniqueName("ariba.core.BuysenseFieldDataTable",
                    pcsvPartition, sUniqueName);
            if (oBFDT == null)
            {
                log("BuysenseFieldDataTable do not exist in Ariba");
                return false; //Role object do not exist in Ariba. 
            }
            // need to check Group?? Think, not required.
        }
        return true;
    }

}
