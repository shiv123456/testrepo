package config.java.ams.custom;

import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import ariba.approvable.core.Approvable;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.ClusterRoot;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.util.core.Constants;
import ariba.util.core.Fmt;
import ariba.util.core.ListUtil;
import ariba.util.core.ResourceService;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseEformValidationEngine {

	private String msClassName = this.getClass().getName();
	protected String sResourceFile = "buysense.DynamicEform";
	private static final List NoErrorResult = ListUtil.list(Constants
			.getInteger(0));

	String clientName = null;

	String errorMessage = "";

	String errorMessage1 = "";

	boolean AccCBFlag = true;   
	boolean HeadCBFlag = true;
	    
	List fieldTableVector = ListUtil.list();

	// Private strings for local FieldList Containers
    /*   CSPL-8526: Updated the missing fields that were added as part of "eForm Phase 3" (CSPL-8348) changes.
         And added getFormattedLable() method to remove punctuation(s) at the end of Field Label for error message.*/

	private static String[] Local_eformFields = { 
	    "EformHeadPckList", "EformHeadPckList1","EformHeadPckList2", "EformHeadPckList3", "EformHeadPckList4", "EformHeadPckList5", "EformHeadPckList6", "EformHeadPckList7", "EformHeadPckList8",
	    "EformHeadLargeText", "EformHeadLargeText1", "EformHeadLargeText2", "EformHeadLargeText3", "EformHeadLargeText4","EformHeadLargeText3_1", "EformHeadLargeText3_2", "EformHeadLargeText3_3",
	    "EformHeadLongTextField", "EformHeadLongTextField1","EformHeadLongTextField2", "EformHeadLongTextField_1",
	    "EformHeadText", "EformHeadText1", "EformHeadText2", "EformHeadText3", "EformHeadText4", "EformHeadText5", "EformHeadText_1",
	    "EformHeadDate1", "EformHeadDate2", "EformHeadDate3",
	    "EformIntegerText1", "EformIntegerText2", "EformIntegerText3", "EformIntegerText4",
		"EformHeadCB", "EformHeadCB1", "EformHeadCB2", "EformHeadCB3","EformHeadCB4", "EformHeadCB5","EformHeadCB6", "EformHeadCB7",
		"EformHeadGrp1RadioHeader", "EformHeadGrp1RadioVal","EformHeadGrp2RadioHeader","EformHeadGrp2RadioVal",
		"EformHeadGrp_1RadioHeader","EformHeadGrp_1RadioHeaderVal","EformHeadGrp_2RadioHeader","EformHeadGrp_2RadioHeaderVal",
		"EformHeadGrp_3RadioHeader","EformHeadGrp_3RadioHeaderVal","EformHeadGrp_4RadioHeader","EformHeadGrp_4RadioHeaderVal",
		"EformHeadGrp_5RadioHeader","EformHeadGrp_5RadioHeaderVal","EformHeadGrp_6RadioHeader","EformHeadGrp_6RadioHeaderVal","EformHeadGrp_7RadioHeader","EformHeadGrp_7RadioHeaderVal",
		"EformMoney", "EformMoney1", 
		"EformTable1Field1", "EformTable1Field1_1", "EformTable1Field2","EformTable1Field2_1","EformTable1Field2_2","EformTable1Field2_3", "EformTable1Field3", "EformTable1Field4", "EformTable1Field5","EformTable1Field5_1","EformTable1Field5_2", "EformTable1Field6", "EformTable1Field7", "EformTable1Field7_1", "EformTable1Field8", "EformTable1Field9", "EformTable1Field10","EformTable1Field11", "EformTable1Field12", "EformTable1Field13","EformTable1Field14","EformTable1Field14_1",
		"EformTable2Field1", "EformTable2Field1_1", "EformTable2Field2","EformTable2Field2_1","EformTable2Field2_2","EformTable2Field2_3", "EformTable2Field3", "EformTable2Field4", "EformTable2Field5","EformTable2Field5_1","EformTable2Field5_2", "EformTable2Field6", "EformTable2Field7", "EformTable2Field7_1", "EformTable2Field8", "EformTable2Field9", "EformTable2Field10","EformTable2Field11", "EformTable2Field12", "EformTable2Field13","EformTable2Field14","EformTable2Field14_1",
		"EformTable3Field1", "EformTable3Field1_1", "EformTable3Field2","EformTable3Field2_1","EformTable3Field2_2","EformTable3Field2_3", "EformTable3Field3", "EformTable3Field4", "EformTable3Field5","EformTable3Field5_1","EformTable3Field5_2", "EformTable3Field6", "EformTable3Field7", "EformTable3Field7_1", "EformTable3Field8", "EformTable3Field9", "EformTable3Field10","EformTable3Field11", "EformTable3Field12", "EformTable3Field13","EformTable3Field14","EformTable3Field14_1",
		"EformTable4Field1", "EformTable4Field1_1", "EformTable4Field2","EformTable4Field2_1","EformTable4Field2_2","EformTable4Field2_3", "EformTable4Field3", "EformTable4Field4", "EformTable4Field5","EformTable4Field5_1","EformTable4Field5_2", "EformTable4Field6", "EformTable4Field7", "EformTable4Field7_1", "EformTable4Field8", "EformTable4Field9", "EformTable4Field10","EformTable4Field11", "EformTable4Field12", "EformTable4Field13","EformTable4Field14","EformTable4Field14_1",
		"EformTable5Field1", "EformTable5Field1_1", "EformTable5Field2","EformTable5Field2_1","EformTable5Field2_2","EformTable5Field2_3", "EformTable5Field3", "EformTable5Field4", "EformTable5Field5","EformTable5Field5_1","EformTable5Field5_2", "EformTable5Field6", "EformTable5Field7", "EformTable5Field7_1", "EformTable5Field8", "EformTable5Field9", "EformTable5Field10","EformTable5Field11", "EformTable5Field12", "EformTable5Field13","EformTable5Field14","EformTable5Field14_1",
		"EformTable6Field1", "EformTable6Field1_1", "EformTable6Field2","EformTable6Field2_1","EformTable6Field2_2","EformTable6Field2_3", "EformTable6Field3", "EformTable6Field4", "EformTable6Field5","EformTable6Field5_1","EformTable6Field5_2", "EformTable6Field6", "EformTable6Field7", "EformTable6Field7_1", "EformTable6Field8", "EformTable6Field9", "EformTable6Field10","EformTable6Field11", "EformTable6Field12", "EformTable6Field13","EformTable6Field14","EformTable6Field14_1",
		"EformTable7Field1", "EformTable7Field1_1", "EformTable7Field2","EformTable7Field2_1","EformTable7Field2_2","EformTable7Field2_3", "EformTable7Field3", "EformTable7Field4", "EformTable7Field5","EformTable7Field5_1","EformTable7Field5_2", "EformTable7Field6", "EformTable7Field7", "EformTable7Field7_1", "EformTable7Field8", "EformTable7Field9", "EformTable7Field10","EformTable7Field11", "EformTable7Field12", "EformTable7Field13","EformTable7Field14","EformTable7Field14_1",
		"EformTable8Field1", "EformTable8Field1_1", "EformTable8Field2","EformTable8Field2_1","EformTable8Field2_2","EformTable8Field2_3", "EformTable8Field3", "EformTable8Field4", "EformTable8Field5","EformTable8Field5_1","EformTable8Field5_2", "EformTable8Field6", "EformTable8Field7", "EformTable8Field7_1", "EformTable8Field8", "EformTable8Field9", "EformTable8Field10","EformTable8Field11", "EformTable8Field12", "EformTable8Field13","EformTable8Field14","EformTable8Field14_1",
		"EformTable9Field1", "EformTable9Field1_1", "EformTable9Field2","EformTable9Field2_1","EformTable9Field2_2","EformTable9Field2_3", "EformTable9Field3", "EformTable9Field4", "EformTable9Field5","EformTable9Field5_1","EformTable9Field5_2", "EformTable9Field6", "EformTable9Field7", "EformTable9Field7_1", "EformTable9Field8", "EformTable9Field9", "EformTable9Field10","EformTable9Field11", "EformTable9Field12", "EformTable9Field13","EformTable9Field14","EformTable9Field14_1",
		"EformTable10Field1", "EformTable10Field1_1", "EformTable10Field2","EformTable10Field2_1","EformTable10Field2_2","EformTable10Field2_3", "EformTable10Field3", "EformTable10Field4", "EformTable10Field5","EformTable10Field5_1","EformTable10Field5_2", "EformTable10Field6", "EformTable10Field7", "EformTable10Field7_1", "EformTable10Field8", "EformTable10Field9", "EformTable10Field10","EformTable10Field11", "EformTable10Field12", "EformTable10Field13","EformTable10Field14","EformTable10Field14_1",
		"EformAcctPckList1", "EformAcctPckList2", "EformAcctPckList3", "EformAcctPckList4", "EformAcctPckList5", "EformAcctPckList6", "EformAcctPckList7", "EformAcctPckList8", "EformAcctPckList9", "EformAcctPckList10",
		"EformAcctText1", "EformAcctText2", "EformAcctText3", "EformAcctText4","EformAcctText5",
		"EformAcctLargeText", "EformAcctLargeText1", "EformAcctLargeText2", "EformAcctLargeText3", "EformAcctLargeText4", "EformAcctLargeText5", 
		"EformAcctDate1", "EformAcctMoney1",
	    "EformAcctCB1", "EformAcctCB2", "EformAcctCB3", "EformAcctCB4", "EformAcctCB5", "EformAcctCB6", "EformAcctCB7", "EformAcctCB8","EformAcctCB9",
	    "EformAcctLongTextField1", "EformAcctLongTextField2", "EformAcctLongTextField3"
	};

	private static String[] Local_eformBuysEformFieldDataTableFields = {
			"EformHeadPckList", "EformHeadPckList1", "EformHeadPckList2", "EformHeadPckList3", "EformHeadPckList4", "EformHeadPckList5", "EformHeadPckList6", "EformHeadPckList7", "EformHeadPckList8",
			"EformTable1Field2_1","EformTable1Field2_2", "EformTable1Field9", "EformTable1Field10",
			"EformTable2Field2_1","EformTable2Field2_2", "EformTable2Field9", "EformTable2Field10", 
			"EformTable3Field2_1","EformTable3Field2_2", "EformTable3Field9", "EformTable3Field10",
			"EformTable4Field2_1","EformTable4Field2_2", "EformTable4Field9", "EformTable4Field10",
			"EformTable5Field2_1","EformTable5Field2_2", "EformTable5Field9", "EformTable5Field10", 
			"EformTable6Field2_1","EformTable6Field2_2", "EformTable6Field9", "EformTable6Field10",
			"EformTable7Field2_1","EformTable7Field2_2", "EformTable7Field9", "EformTable7Field10", 
			"EformTable8Field2_1","EformTable8Field2_2", "EformTable8Field9", "EformTable8Field10", 
			"EformTable9Field2_1","EformTable9Field2_2", "EformTable9Field9", "EformTable9Field10",
			"EformTable10Field2_1","EformTable10Field2_2", "EformTable10Field9", "EformTable10Field10",
			"EformAcctPckList1", "EformAcctPckList2", "EformAcctPckList3", "EformAcctPckList4", "EformAcctPckList5", "EformAcctPckList6", "EformAcctPckList7", "EformAcctPckList8", "EformAcctPckList9", "EformAcctPckList10" };

	private static String[] Local_eformBooleanFields = {
			"EformTable1Field1", "EformTable2Field1", "EformTable3Field1",
			"EformTable4Field1", "EformTable5Field1", "EformTable6Field1",
			"EformTable7Field1", "EformTable8Field1", "EformTable9Field1",
			"EformTable10Field1" };

	private static String[] Local_eformMoneyFields = { 
	        "EformTable1Field5", "EformTable1Field6", "EformTable1Field2_3",
	        "EformTable2Field5", "EformTable2Field6", "EformTable2Field2_3",
			"EformTable3Field5", "EformTable3Field6", "EformTable3Field2_3",
			"EformTable4Field5", "EformTable4Field6", "EformTable4Field2_3",
			"EformTable5Field5", "EformTable5Field6", "EformTable5Field2_3",
			"EformTable6Field5", "EformTable6Field6", "EformTable6Field2_3",
			"EformTable7Field5", "EformTable7Field6", "EformTable7Field2_3",
			"EformTable8Field5", "EformTable8Field6", "EformTable8Field2_3",
			"EformTable9Field5", "EformTable9Field6", "EformTable9Field2_3",
			"EformTable10Field5", "EformTable10Field6", "EformTable10Field2_3",
			"EformMoney", "EformMoney1", "EformAcctMoney1" };

	private static String[] Local_eformBooleanAccCbFields = { 
           "EformAcctCB1", "EformAcctCB2", "EformAcctCB3", "EformAcctCB4",
           "EformAcctCB5", "EformAcctCB6", "EformAcctCB7","EformAcctCB8","EformAcctCB9" };

    private static String[] Local_eformBooleanHeadCbFields = { "EformHeadCB1",
           "EformHeadCB2", "EformHeadCB3", "EformHeadCB4", "EformHeadCB5"
            };

	private static String[] local_eformDateFields = { "EformTable1Field13",
			"EformTable2Field13", "EformTable3Field13", "EformTable4Field13",
			"EformTable5Field13", "EformTable6Field13", "EformTable7Field13",
			"EformTable8Field13", "EformTable9Field13", "EformTable10Field13",
			"EformAcctDate1" };

	private static String[] Local_eformLargeTextFields = {
	        "EformHeadLargeText","EformHeadLargeText1", "EformHeadLargeText2", "EformHeadLargeText3", "EformHeadLargeText3_1", "EformHeadLargeText3_2", "EformHeadLargeText3_3", "EformHeadLargeText4",
			"EformAcctLargeText1", "EformAcctLargeText2", "EformAcctLargeText3", "EformAcctLargeText4", "EformAcctLargeText5" };
	private static String[] Local_eformLargeTextFieldsVal = {
	        "EformHeadLargeTextVal","EformHeadLargeText1Val", "EformHeadLargeText2Val", "EformHeadLargeText3Val", "EformHeadLargeText3_1Val", "EformHeadLargeText3_2Val", "EformHeadLargeText3_3Val", "EformHeadLargeText4Val",
			"EformAcctLargeText1Val", "EformAcctLargeText2Val", "EformAcctLargeText3Val", "EformAcctLargeText4Val", "EformAcctLargeText5Val"};
    private static String[] Local_eformHeadGrp1RadioFields = {
        "EformHeadLargeText1", "EformHeadLargeText2",
        "EformHeadLargeText3", "EformAcctLargeText1",
        "EformAcctLargeText2", "EformAcctLargeText3",
        "EformAcctLargeText4", "EformAcctLargeText5", "EformHeadLargeText4"};
    private static String[] Local_eformHeadGrp1RadioFieldsVal = {
        "EformHeadLargeText1Val", "EformHeadLargeText2Val",
        "EformHeadLargeText3Val", "EformAcctLargeText1Val",
        "EformAcctLargeText2Val", "EformAcctLargeText3Val",
        "EformAcctLargeText4Val", "EformAcctLargeText5Val", "EformHeadLargeText4Val"};
    private static String[] Local_eformIntegerFields = {
            "EformIntegerText1","EformIntegerText2", 
            "EformIntegerText3", "EformIntegerText4"};
    private static String[] Local_eformNigpFields = {
        "NIGPCommodityCodes","EformAcctNIGPCommodityCodes"};
	public List runEformValidation(Approvable approvable, String lsProfileName) {

		Log.customer
				.debug("In runEformHeaderValidation of BuysenseEformValidationEngine");
		
        if(approvable.getDottedFieldValue("Name") == null || StringUtil.nullOrEmptyOrBlankString((String)approvable.getDottedFieldValue("Name")))
        {
            errorMessage = errorMessage+"Title"+ ", ";
        }
		String lsProfileN = (String)approvable.getDottedFieldValue("EformChooser.Name");

		ariba.user.core.User RequesterUser = (ariba.user.core.User) approvable
				.getDottedFieldValue("Requester");

		Partition appPartition = approvable.getPartition();

		ariba.common.core.User RequesterPartitionUser = ariba.common.core.User
				.getPartitionedUser(RequesterUser, appPartition);
		clientName = (String) RequesterPartitionUser
				.getDottedFieldValue("BuysenseOrg.ClientName.Name");
		String sClientID = (String) RequesterPartitionUser
				.getDottedFieldValue("BuysenseOrg.ClientName.ClientID");

		AQLOptions fieldTableOptions = new AQLOptions(appPartition);
		AQLQuery fieldTableQuery = new AQLQuery(
				"ariba.core.BuysEformFieldTable", true);
		fieldTableQuery.andEqual("RequiredNotRequired", new Boolean(true));
		fieldTableQuery.andEqual("ProfileName", lsProfileName);

		Log.customer.debug("The query to fetch the fields is:"
				+ fieldTableQuery);
		
		

		AQLResultCollection fieldTableResults = Base.getService().executeQuery(
				fieldTableQuery, fieldTableOptions);

/*		boolean bAQLResult = fieldTableResults.isEmpty();

		if (bAQLResult) {
			Log.customer.debug("AQL fieldTableQuery RETURNED NO ROWS");
			return NoErrorResult;

		}*/
        if(fieldTableResults != null && fieldTableResults.getFirstError() == null && !fieldTableResults.isEmpty())
		{

		while (fieldTableResults.next()) {
			BaseId bid = (BaseId) fieldTableResults.getBaseId(0);
			ClusterRoot fieldTable = (ClusterRoot) bid.get();

			fieldTableVector.add(fieldTable);
			String fieldName = (String) fieldTable.getFieldValue("FieldName");
			Log.customer.debug("Adding it to the vector: %s",
					fieldTable.getFieldValue("FieldName"));
		}

		// All Fields List
		List eformHeaderFieldsVector = ListUtil.arrayToList(Local_eformFields);
		// Amount Fields List
		List eformAmountFields = ListUtil.arrayToList(Local_eformMoneyFields);
		// Boolean Fields List
		List eformBooleanFields = ListUtil
				.arrayToList(Local_eformBooleanFields);
        List eformBooleanAccCbFields = ListUtil
                .arrayToList(Local_eformBooleanAccCbFields);
        List eformBooleanHeadCbFields = ListUtil
                .arrayToList(Local_eformBooleanHeadCbFields);
	     List eformIntegerFields = ListUtil
	                .arrayToList(Local_eformIntegerFields);
		// Date Fields List
		List eformDateFields = ListUtil.arrayToList(local_eformDateFields);
		// Large Text Field
		List eformLargeTextFields = ListUtil
				.arrayToList(Local_eformLargeTextFields);

		List eformLargeTextFieldsVal = ListUtil
				.arrayToList(Local_eformLargeTextFieldsVal);

	      List eformHeadGrp1RadioFields = ListUtil.arrayToList(Local_eformHeadGrp1RadioFields);
	      List eformHeadGrp1RadioFieldsVal = ListUtil.arrayToList(Local_eformHeadGrp1RadioFieldsVal);
	       List eformNigpFields = ListUtil.arrayToList(Local_eformNigpFields);
		// for (Enumeration fieldTableEnum =
		// fieldTableVector.elements();fieldTableEnum.hasMoreElements();)
		for (Iterator fieldTableEnum = fieldTableVector.iterator(); fieldTableEnum
				.hasNext();) {
			// Get the field we are looking for from the AQL query
			ClusterRoot fieldTable = (ClusterRoot) fieldTableEnum.next();
			String fieldName = (String) fieldTable.getFieldValue("FieldName");
			Log.customer.debug("Looking for the AQL field: %s",
					fieldTable.getFieldValue("FieldName"));

			// Loop thru the reqHeaderFieldsVector
			int vectorSize = eformHeaderFieldsVector.size();
			for (int i = 0; i < vectorSize; i++) {

				String eformHeaderFieldName = (String) eformHeaderFieldsVector
						.get(i);

				// Log.customer.debug("Checking on the eform field: %s",
				// eformHeaderFieldName );

				if (eformHeaderFieldName.equals(fieldName)) {
					Log.customer.debug("reqHeaderFieldName: %s",
							eformHeaderFieldName);
					Log.customer.debug("reqHeaderFieldValue: %s", (approvable
							.getDottedFieldValue(eformHeaderFieldName)));
					Log.customer.debug("fieldname: %s", fieldName);

					if (eformAmountFields.contains(eformHeaderFieldName)
							&& approvable.getFieldValue(eformHeaderFieldName) == null) {
						Log.customer.debug("The field %s :"
								+ eformHeaderFieldName + "is empty");
						errorMessage = errorMessage
								+ getFormattedLable(fieldTable)
								+ ", ";
					}

					else if (eformBooleanFields.contains(eformHeaderFieldName)
							&& approvable.getFieldValue(eformHeaderFieldName) == null) {
						Log.customer.debug("The field %s :"
								+ eformHeaderFieldName + "is empty");
						errorMessage = errorMessage
								+ getFormattedLable(fieldTable)
								+ ", ";
					}
                        else if (eformBooleanAccCbFields.contains(eformHeaderFieldName)
                                && (approvable.getFieldValue(eformHeaderFieldName) == null || approvable.getFieldValue(eformHeaderFieldName)
                                        .toString().trim().equals("false")))
                        {

                            boolean EformAcctCB1 = (approvable.getDottedFieldValue("EformAcctCB1") == null) ? false : ((Boolean) approvable
                                    .getDottedFieldValue("EformAcctCB1")).booleanValue();
                            boolean EformAcctCB2 = (approvable.getDottedFieldValue("EformAcctCB2") == null) ? false : ((Boolean) approvable
                                    .getDottedFieldValue("EformAcctCB2")).booleanValue();
                            boolean EformAcctCB3 = (approvable.getDottedFieldValue("EformAcctCB3") == null) ? false : ((Boolean) approvable
                                    .getDottedFieldValue("EformAcctCB3")).booleanValue();
                            boolean EformAcctCB4 = (approvable.getDottedFieldValue("EformAcctCB4") == null) ? false : ((Boolean) approvable
                                    .getDottedFieldValue("EformAcctCB4")).booleanValue();
                            boolean EformAcctCB5 = (approvable.getDottedFieldValue("EformAcctCB5") == null) ? false : ((Boolean) approvable
                                    .getDottedFieldValue("EformAcctCB5")).booleanValue();
                            boolean EformAcctCB6 = (approvable.getDottedFieldValue("EformAcctCB6") == null) ? false : ((Boolean) approvable
                                    .getDottedFieldValue("EformAcctCB6")).booleanValue();
                            boolean EformAcctCB7 = (approvable.getDottedFieldValue("EformAcctCB7") == null) ? false : ((Boolean) approvable
                                    .getDottedFieldValue("EformAcctCB7")).booleanValue();
                            boolean EformAcctCB8 = (approvable.getDottedFieldValue("EformAcctCB8") == null) ? false : ((Boolean) approvable
                                    .getDottedFieldValue("EformAcctCB8")).booleanValue();
                            boolean EformAcctCB9 = (approvable.getDottedFieldValue("EformAcctCB9") == null) ? false : ((Boolean) approvable
                                    .getDottedFieldValue("EformAcctCB9")).booleanValue();

                            if (!(EformAcctCB1 || EformAcctCB2 || EformAcctCB3 || EformAcctCB4 || EformAcctCB5 || EformAcctCB6 || EformAcctCB7
                                    || EformAcctCB8 || EformAcctCB9))
                            {

                                if (AccCBFlag)
                                {
                                    errorMessage = errorMessage + ResourceService.getString(sResourceFile, "EformAcctCBErrorMsg") + ", ";
                                    AccCBFlag = false;
                                }

                            }
                        }
                        else if (eformBooleanHeadCbFields.contains(eformHeaderFieldName)
                                && (approvable.getFieldValue(eformHeaderFieldName) == null || approvable.getFieldValue(eformHeaderFieldName)
                                        .toString().trim().equals("false")))
                        {
                            boolean EformHeadCB1 = (approvable.getDottedFieldValue("EformHeadCB1") == null) ? false : ((Boolean) approvable
                                    .getDottedFieldValue("EformHeadCB1")).booleanValue();
                            boolean EformHeadCB2 = (approvable.getDottedFieldValue("EformHeadCB2") == null) ? false : ((Boolean) approvable
                                    .getDottedFieldValue("EformHeadCB2")).booleanValue();
                            boolean EformHeadCB3 = (approvable.getDottedFieldValue("EformHeadCB3") == null) ? false : ((Boolean) approvable
                                    .getDottedFieldValue("EformHeadCB3")).booleanValue();
                            boolean EformHeadCB4 = (approvable.getDottedFieldValue("EformHeadCB4") == null) ? false : ((Boolean) approvable
                                    .getDottedFieldValue("EformHeadCB4")).booleanValue();
                            boolean EformHeadCB5 = (approvable.getDottedFieldValue("EformHeadCB5") == null) ? false : ((Boolean) approvable
                                    .getDottedFieldValue("EformHeadCB5")).booleanValue();

                            if (!(EformHeadCB1 || EformHeadCB2 || EformHeadCB3 || EformHeadCB4 || EformHeadCB5))
                            {
                                if (HeadCBFlag)
                                {
                                    errorMessage = errorMessage + ResourceService.getString(sResourceFile, "EformHeadCBErrorMsg") + ", ";
                                    HeadCBFlag = false;
                                }

                            }
                        }
                    else if (eformIntegerFields.contains(eformHeaderFieldName)
                            && approvable.getFieldValue(eformHeaderFieldName) == null) {
                        Log.customer.debug("The field %s :"
                                + eformHeaderFieldName + "is empty");
                        errorMessage = errorMessage
                                + getFormattedLable(fieldTable)
                                + ", ";
                    }
					else if (eformDateFields.contains(eformHeaderFieldName)
							&& approvable.getFieldValue(eformHeaderFieldName) == null) {
						Log.customer.debug("The field %s :"
								+ eformHeaderFieldName + "is empty");
						errorMessage = errorMessage
								+ getFormattedLable(fieldTable)
								+ ", ";
					}

	                   else if (eformNigpFields.contains(eformHeaderFieldName))
	                   {
	                       List<?> lo = (List<?>) approvable.getFieldValue(eformHeaderFieldName);
	                       if(lo != null && lo.isEmpty())
	                       {
                                Log.customer.debug("The field %s :" + eformHeaderFieldName + "is empty");
                                errorMessage = errorMessage + fieldTable.getDottedFieldValue("FieldLabel") + ", ";
	                       }

	                    }
					else if (eformLargeTextFields
							.contains(eformHeaderFieldName)) {
						eformHeaderFieldName = eformHeaderFieldName + "Val";
						if (StringUtil
								.nullOrEmptyOrBlankString((String) (approvable
										.getFieldValue(eformHeaderFieldName)))) {
							Log.customer.debug("The field %s :"
									+ eformHeaderFieldName + "is empty");
							errorMessage = errorMessage
									+ fieldTable
											.getDottedFieldValue("FieldLabel")
									+ ", ";
						}
					} else if (eformLargeTextFieldsVal
							.contains(eformHeaderFieldName)) {
						if (StringUtil
								.nullOrEmptyOrBlankString((String) (approvable
										.getFieldValue(eformHeaderFieldName)))) {
							Log.customer.debug("The field %s :"
									+ eformHeaderFieldName + "is empty");
							errorMessage = errorMessage
									+ fieldTable
											.getDottedFieldValue("FieldLabel")
									+ ", ";
						}
					}else if (eformHeadGrp1RadioFields.contains(eformHeaderFieldName)) {
	                        eformHeaderFieldName = eformHeaderFieldName + "Val";
	                        if (StringUtil.nullOrEmptyOrBlankString((String) (approvable .getFieldValue(eformHeaderFieldName)))) {
	                            Log.customer.debug("The field %s :" + eformHeaderFieldName + "is empty");
	                            errorMessage = errorMessage + getFormattedLable(fieldTable)+ ", ";
	                        }
	                  }else if (eformHeadGrp1RadioFieldsVal.contains(eformHeaderFieldName)) {
                        if (StringUtil.nullOrEmptyOrBlankString((String) (approvable .getFieldValue(eformHeaderFieldName)))) {
                            Log.customer.debug("The field %s :" + eformHeaderFieldName + "is empty");
                            errorMessage = errorMessage+ getFormattedLable(fieldTable)+ ", ";
                        }
                    }

					else if ((approvable
							.getDottedFieldValue(eformHeaderFieldName) instanceof String)
							&& StringUtil
									.nullOrEmptyOrBlankString((String) approvable
											.getDottedFieldValue(eformHeaderFieldName))) {
						Log.customer
								.debug("regHeaderFieldValue is instance of String: %s",
										eformHeaderFieldName);
						Log.customer.debug("%s is a Empty String ",
								(String) eformHeaderFieldName);
						errorMessage = errorMessage
								+ getFormattedLable(fieldTable)
								+ ", ";
					}

					else if (approvable
							.getDottedFieldValue(eformHeaderFieldName) == null) {
						Log.customer.debug("The field " + eformHeaderFieldName
								+ " is empty but requires a value");
						errorMessage = errorMessage
								+ getFormattedLable(fieldTable)
								+ ", ";
					}

					break;
				}

			}
		}
	}
        //call below method each time for each series field rule (Rule1PL, Rule2PL etc) passing the rule name
        String sGroupValidationError = validateGroupFieldsBasedonOtherField(approvable, "Rule1PL",sClientID,lsProfileName);
        if(!StringUtil.nullOrEmptyOrBlankString(sGroupValidationError))
        {
            errorMessage = errorMessage +sGroupValidationError;   
        }

		Log.customer.debug("The profile name is :"+lsProfileName);
        List eformFieldDataTableFields = ListUtil.arrayToList(Local_eformBuysEformFieldDataTableFields);

		errorMessage1 = VerifyFieldValues(approvable, lsProfileName,eformFieldDataTableFields);

		
		Log.customer.debug("The error message is :"+errorMessage);
		
		Log.customer.debug("The second message is:"+errorMessage1);
		
		
		
		if (!(StringUtil.nullOrEmptyOrBlankString(errorMessage))) {
			Log.customer
					.debug("Checking errorMessage : ErrorMessage.length: %s", errorMessage.length());
			errorMessage = errorMessage.substring(0,
					(errorMessage.length() - 2));

			return  ListUtil
					.list(Constants.getInteger(-1), lsProfileN
							+ " requires the following field(s): "
							+ errorMessage + errorMessage1);			
			
		}
		
		else if(!(StringUtil.nullOrEmptyOrBlankString(errorMessage1)) & (StringUtil.nullOrEmptyOrBlankString(errorMessage)))
		{
			Log.customer.debug("The error message is :"+errorMessage1);
			//return errorMessage1;
			return  ListUtil
					.list(Constants.getInteger(-1), errorMessage1 +  ".");	
		}
		
		else{
		return NoErrorResult;}

	}

    private String VerifyFieldValues(Approvable approvable,
			String lsProfileName, List eformFieldDataTableFields) {
		
		String lsNullString = "";

		/*for (Iterator fieldTableEnum = fieldTableVector.iterator(); fieldTableEnum
				.hasNext();) {
			// Get the field we are looking for from the AQL query
			ClusterRoot fieldTable = (ClusterRoot) fieldTableEnum.next();
			String fieldName = (String) fieldTable.getFieldValue("FieldName");
			Log.customer.debug("Looking for the AQL field: %s",
					fieldTable.getFieldValue("FieldName"));*/
			
			
			// Loop thru the reqHeaderFieldsVector
			int vectorSize = eformFieldDataTableFields.size();
			for (int i = 0; i < vectorSize; i++) {
				String eformFieldDataTableFieldFieldName = (String) eformFieldDataTableFields
						.get(i);

				// Log.customer.debug("Checking on the eform field: %s",
				// eformHeaderFieldName );

				//if (eformFieldDataTableFieldFieldName.equals(fieldName)) {
					Log.customer.debug("eformFieldDataTableFieldFieldName: %s",
							eformFieldDataTableFieldFieldName);
					Log.customer
							.debug("eformFieldDataTableFieldFieldName: %s",
									(approvable
											.getDottedFieldValue(eformFieldDataTableFieldFieldName)));
					//Log.customer.debug("fieldname: %s", fieldName);

					if (approvable
							.getDottedFieldValue(eformFieldDataTableFieldFieldName) != null) {
						Partition boPartition = approvable.getPartition();
						
						String fieldNameValue = (String) approvable
								.getDottedFieldValue(eformFieldDataTableFieldFieldName
										+ ".Name");
						Log.customer.debug("The field value is :"
								+ fieldNameValue);
						ariba.user.core.User RequesterUser = (ariba.user.core.User) approvable
								.getDottedFieldValue("Requester");
						ariba.common.core.User RequesterPartitionUser = ariba.common.core.User
								.getPartitionedUser(RequesterUser, boPartition);
						String clientID = (String) RequesterPartitionUser
								.getDottedFieldValue("ClientName.ClientID");

						String lsProfileUN = (String)approvable.getDottedFieldValue("EformChooser.UniqueName");
						
						String fieldLabel = BuysenseEformUtil.findFieldLabel(lsProfileUN, eformFieldDataTableFieldFieldName);
						List llNames = ListUtil.list();
						String lsFieldDataTableQuery = "Select Name from ariba.core.BuysEformFieldDataTable AS BuysEformFieldDataTable where BuysEformFieldDataTable.Creator IS NULL AND BuysEformFieldDataTable.FieldName = '"
								+ eformFieldDataTableFieldFieldName
								+ "' AND BuysEformFieldDataTable.ClientID = '"
								+ clientID
								+ "' AND BuysEformFieldDataTable.ProfileName = '"
								+ lsProfileName
								+ "' ORDER BY BuysEformFieldDataTable.Name ASC";

						Partition pcsvPartition = Base.getService()
								.getPartition("pcsv");
						AQLOptions loOptions = new AQLOptions(pcsvPartition);
						try {
							AQLQuery loQuery = AQLQuery
									.parseQuery(lsFieldDataTableQuery);
							Log.customer.debug("%s - executing query -%s",
									msClassName, loQuery);
							AQLResultCollection loResults = Base.getService()
									.executeQuery(loQuery, loOptions);
							if (loResults.getFirstError() != null) {
								Log.customer.warning(8000,
										"%s - error in AQL results - %s",
										msClassName, loResults.getFirstError()
												.toString());
							} else {
								if (loResults != null) {
									while (loResults.next()) {

										String lsName = (String) loResults
												.getString("Name");
										Log.customer.debug("The Name is :"+lsName);
										llNames.add(lsName);
									}

									if (llNames.contains(fieldNameValue)) {
										//return lsNullString;
									    continue;
									} else {
										errorMessage1 = errorMessage1
												+ fieldLabel
												+ ", ";
										Log.customer.debug("The error is :"
												+ errorMessage1);
									}
									
								}
							}

						} catch (Exception e) {
							Log.customer.warning(8000,
									"%s - Exception in AQL execution -%s",
									msClassName, e.getMessage());
							return lsNullString;
						}

					}
					
				//}

			}
			/*return lsNullString;
		}*/
		
            if (!(StringUtil.nullOrEmptyOrBlankString(errorMessage1)) && errorMessage1 != null)
            {
                Log.customer.debug("ErrorMessage.length: %s", errorMessage1.length());
                errorMessage1 = errorMessage1.substring(0,(errorMessage1.length() - 2));
                String lsError = ResourceService.getString(sResourceFile, "FieldValuesMisMatchError");
                String lsErrorHeader = ResourceService.getString(sResourceFile, "FieldValuesMisMatchErrorHeader");
                return lsErrorHeader + errorMessage1+ lsError;
            }
            return lsNullString;

	}

	public String validateGroupFieldsBasedonOtherField(Approvable loEformObj, String sValidationRuleName, String clientID, String lsProfileName)
    {
	    if(loEformObj== null)
	    {
	        return null;
	    }
	    String sErrorMessage = "";
	    
        //UniqueName: EVA001:QR:Rule1PL-EformAcctPckList4-No:EformAcctCB1
        String lsFieldDataTableQuery = "Select b.FieldName as FieldName, b.UniqueName as UniqueName, b.Description as Description  from ariba.core.BuysEformFieldDataTable AS b where b.Creator IS NULL "
               + " AND b.ClientID = '" + clientID+ "' AND b.ProfileName = '" + lsProfileName
               + "' AND b.FieldName like '" + sValidationRuleName +"%' ORDER BY b.Name ASC";

        Partition pcsvPartition = Base.getService().getPartition("pcsv");

        AQLOptions loOptions = new AQLOptions(pcsvPartition);
        try
        {
            AQLQuery loQuery = AQLQuery.parseQuery(lsFieldDataTableQuery);
            Log.customer.debug("%s - executing query -%s", msClassName, loQuery);
            AQLResultCollection loResults = Base.getService().executeQuery(loQuery, loOptions);
            if (loResults.getFirstError() != null)
            {
                Log.customer.warning(8000, "%s - error in AQL results - %s", msClassName, loResults.getFirstError().toString());
            }
            else
            {
                if (loResults != null)
                {
                    while (loResults.next())
                    {


                        String sFieldToCheck = null;
                        String sFieldValueToCheck = null;
                        String lsFieldName = loResults.getString("FieldName");
                        StringTokenizer st = new StringTokenizer(lsFieldName,"-");
                        Log.customer.debug(" token count "+st.countTokens());
                        if(st.countTokens()<3)
                        {
                            Log.customer.debug("Incorrect BuysEformFieldDataTable.FieldName data setup for "+ "Rule1PL"+" rule");
                            continue;
                        }
                        String sFieldValue = lsFieldName.substring(lsFieldName.indexOf("-")+1, lsFieldName.length());
                        sFieldToCheck = sFieldValue.substring(0, sFieldValue.indexOf("-"));
                        sFieldValueToCheck = sFieldValue.substring(sFieldValue.indexOf("-")+1, sFieldValue.length());

                        Log.customer.debug("sFieldToCheck:"+sFieldToCheck+", sFieldValueToCheck:"+sFieldValueToCheck);
                        if(!StringUtil.nullOrEmptyOrBlankString(sFieldToCheck) && !StringUtil.nullOrEmptyOrBlankString(sFieldValueToCheck))
                        {
                            //assuming field type as ariba.core.BuysEformFieldDataTable (EformAcctPckList4)
                            Object loActualFieldValue =  loEformObj.getDottedFieldValue(sFieldToCheck+".Name");
                            if(loActualFieldValue == null)
                            {
                                Log.customer.warning(8000,"The loActualFieldValue is null not checking for "+sValidationRuleName);
                                continue;
                            }else
                            {
                                String lsActualFieldValue = (String) loActualFieldValue;
                                if(lsActualFieldValue.equals(sFieldValueToCheck))
                                {
                                    // get group field to validate
                                    //EVA001:QR:Rule1PL-EformAcctPckList4-No:EformAcctCB1
                                    String sUniqueName = loResults.getString("UniqueName");
                                    Log.customer.debug("The sUniqueName is :" + sUniqueName);
                                    StringTokenizer st2 = new StringTokenizer(sUniqueName,":");
                                    
                                    if(st2.countTokens()<4)
                                    {
                                        Log.customer.warning(8000,"Incorrect BuysEformFieldDataTable (UniqueName) data setup for "+ sValidationRuleName+" rule");
                                        continue;
                                    }
                                    
                                    String sGroupFieldsToValidate = "";
                                    String[] lsCBsToValidate = {};
                                    boolean bValidSeries = false;
                                    int j=0;
                                    while(st2.hasMoreTokens())
                                    {
                                        String sToken = st2.nextToken().trim();
                                        if(j==3)
                                        {
                                            sGroupFieldsToValidate = sToken;
                                        }
                                        j++;
                                    }
                                    if (sGroupFieldsToValidate.startsWith("EformHeadCB"))
                                    {
                                        lsCBsToValidate = Local_eformBooleanHeadCbFields;
                                    }
                                    else if (sGroupFieldsToValidate.startsWith("EformAcctCB"))
                                    {
                                        lsCBsToValidate = Local_eformBooleanAccCbFields;
                                    }
                                    
                                    if(lsCBsToValidate.length == 0)
                                    {
                                        continue;
                                    }else
                                    {
                                        for(int k =0 ; k< lsCBsToValidate.length; k++)
                                        {
                                            Object loSeriesFieldValue =  loEformObj.getDottedFieldValue(lsCBsToValidate[k]);
                                            if(loSeriesFieldValue != null && loSeriesFieldValue.toString().trim().equals("true"))
                                            {
                                                bValidSeries =  true;
                                            }
                                        }
                                       
                                    }
                                    Log.customer.debug("bValidSeries - "+bValidSeries);
                                    if(bValidSeries)
                                    {
                                        // for valid condition we return null since we care about only error
                                        continue;
                                    }else
                                    {
                                        //sErrorMessage =  sErrorMessage + Fmt.Sil(Base.getSession().getLocale(), "buysense.DynamicEform", sValidationRuleName+"ErrorMsg",sGroupFieldsToValidate,sFieldToCheck,sFieldValueToCheck) + ",";
                                        String lsDesc = loResults.getString("Description");
                                        sErrorMessage =  sErrorMessage + lsDesc + ",";
                                    }
                                }
                            }
                        }
                        
                    }

                }
            }

        }
        catch (Exception e)
        {
            Log.customer.warning(8000, "%s - Exception in AQL execution -%s", msClassName, e.getMessage());
            return null;
        }
        sErrorMessage = StringUtil.nullOrEmptyOrBlankString(sErrorMessage)?" ":sErrorMessage +".";
        // Note: Added one extra period at the end to negate the truncation of last two chars of errorMessage as per current code.
        return sErrorMessage;
    }

    private String getFormattedLable(ClusterRoot fieldTable)
    {
        String sFieldLabel = (String) fieldTable.getDottedFieldValue("FieldLabel");

        if (sFieldLabel.endsWith("::") || sFieldLabel.endsWith("??"))
        {
            sFieldLabel = sFieldLabel.substring(0, sFieldLabel.length() - 2);
        }

        if (sFieldLabel.endsWith(":") || sFieldLabel.endsWith("?"))
        {
            sFieldLabel = sFieldLabel.substring(0, sFieldLabel.length() - 1);
        }
        return sFieldLabel;
    }
}
