package config.java.ams.custom;

import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLNameTable;
import ariba.base.core.aql.SearchTermQuery;
import ariba.base.fields.ValueSource;
import ariba.util.log.Log;

public class BuysenseLogiAdvanceRoleNameTable extends AQLNameTable{
	
    public static final String ClassName="config.java.ams.custom.BuysenseLogiAdvanceRoleNameTable";
    public static final String FieldName="LogiAdvancedReportNeeds";

    
    /* Ariba 8.1 - As part of rewriting the class to only return Approvers that are of type ariba.user.core.User or
                   ariba.user.core.Group, replaced the matchPattern() and addQueryConstraints() methods with buildQuery().  */
    public void addQueryConstraints(AQLQuery query, String field, String pattern, SearchTermQuery searchTermQuery)
    {
        Log.customer.debug( "Inside BuysenseLogiAdvanceRoleNameTable") ;
        String lsAgency = null;
        ValueSource valueSourceCurrent = getValueSourceContext();
    	if (valueSourceCurrent instanceof ariba.approvable.core.Approvable && valueSourceCurrent.getTypeName().equals("ariba.core.BuysenseUserProfileRequest"))
        {
    		lsAgency = (String)valueSourceCurrent.getFieldValue("Agency");
            if(lsAgency == null)
            {
                query.andFalse();
                return;
            }
            else
            {
            	String lsAgencyName = lsAgency.substring(0, 4);
                query.and(AQLCondition.parseCondition("  Name like '%" + lsAgencyName + "%' "));
                Log.customer.debug("The final query is"+query);   
            }
            
        }


    }

}
