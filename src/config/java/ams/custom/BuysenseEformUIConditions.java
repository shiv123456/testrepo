
package config.java.ams.custom;

import ariba.base.core.BaseObject;
import ariba.base.core.ClusterRoot;
import ariba.htmlui.fieldsui.Log;
import ariba.util.core.Assert;

public class BuysenseEformUIConditions
{
public static boolean  isFieldVisible(String sClientID ,String sProfName,
                                      String sFieldName,
                                      BaseObject bo,String appType)
{
    boolean returnValue = false;
    ClusterRoot fieldTable = BuysenseEformUtil.findField(sClientID,sProfName,
                                                    sFieldName);
    Log.customer.debug("Found this fieldTable object: %s",fieldTable);
    String visibilityField = getVisibilityField(appType,bo);
    try
    {
            returnValue = ((Boolean)fieldTable.getFieldValue(visibilityField)).booleanValue();
            if (returnValue)
                Log.customer.debug("*** returnValue: true" );
            else
                Log.customer.debug("*** returnValue: false");
    }
    catch (NullPointerException e)
    {
        Log.customer.debug("Could not find the field for client: %s profName: %s field:%s",sClientID,sProfName,sFieldName);
    }

    if (returnValue)
    {
        if (((String)fieldTable.getFieldValue("FieldLabel")).trim().equalsIgnoreCase("n/a")) 
           returnValue =  false;
    }
    Log.customer.debug("**** returnValue: "+returnValue );
    return returnValue;
}

public static boolean  isFieldEditable(String sClientID ,String sProfName,
                                       String sFieldName,
                                       BaseObject bo,String appType)
{
    boolean returnValue = false;
    ClusterRoot fieldTable = BuysenseEformUtil.findField(sClientID,sProfName,
                                                    sFieldName);
    String editabilityField = getEditabilityField(appType,bo);
    try
    {
            returnValue = ((Boolean)fieldTable.getFieldValue(editabilityField)).booleanValue();
    }
    catch (NullPointerException e)
    {
        Log.customer.debug("Could not find the field for client: %s profName: %s field:%s",sClientID,sProfName,sFieldName);
        Log.customer.debug("NullPointerException");
        Assert.assertNonFatal(false,"Could not find the FieldTable entry");
    }
    return returnValue;
}

private static String getVisibilityField(String appType,
                                         BaseObject appObject)
{
    if (appType.equals("Eform")) return "VisibleOnEform" ;
    else
    	return "NoField";
}

private static String getEditabilityField(String appType,
                                          BaseObject appObject)
{
    if (appType.equals("Eform")) return "EditableOnEform" ;
    else
    	return "NoField";
}

}
