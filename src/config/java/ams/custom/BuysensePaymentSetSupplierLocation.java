/*  ROHIT 11/01/2000 - This code will set the supplier location on Payment when the Supplier is selected for 
    catalog or non catalog items. Only those locations with AddressType = P (Payment) will be allowed                         
   
*/

/* 08/29/2003: Updates for Ariba 8.0 (David Chamberlain) */
/* 03/01/2004: Globally change Log.util.debug to Log.customer.debug (Jeff Namadan) */

package config.java.ams.custom;

import ariba.base.fields.*;
import ariba.common.core.*;
import ariba.util.core.PropertyTable;
import java.util.Iterator;

import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.base.core.aql.AQLScalarExpression;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.util.log.Log;
import ariba.util.core.Fmt;

//81->822 changed Vector to List
//81->822 changed Enumeration to Iterator
//81->822 changed ConditionValueInfo to ValueInfo
public class BuysensePaymentSetSupplierLocation extends Action
{
    //  Most of the code is duplicated from the Out-Of-The-Box trigger called ariba.procure.core.action.SetSupplierLocation
    private static final String requiredParameterNames[] = 
        {
        "Target", "Source"
    };
    private static final ValueInfo parameterInfo[] = 
        {
        new ValueInfo("Target",
                               true,
                               0,
                               "ariba.common.core.SupplierLocation"),
            new ValueInfo("Source", 0, "ariba.common.core.Supplier")
    };
    private static final ValueInfo valueInfo = new ValueInfo(0,
                                                                               "ariba.core.PaymentEform");
    
    // string variable for AddressType field
    
    public void fire(ValueSource object, PropertyTable params)
    {
        //ProcureLineItem lineItem = (ProcureLineItem)object;
        String targetFieldName = params.stringPropertyForKey("Target");
        Supplier supplier = (Supplier)params.getPropertyForKey("Source");
        if(object == null)
            return;
        //  ProcureLineItemCollection collection = (ProcureLineItemCollection)lineItem.getLineItemCollection();
        SupplierLocation location = null;
        if(supplier != null)
            //    location = getLocation(object, supplier);
            location = (SupplierLocation)getCorrectLocation(supplier);
        object.setDottedFieldValue(targetFieldName, location);
    }
    
    protected SupplierLocation getLocation(ValueSource object,
                                           Supplier supplier)
    {
        String addressType=null;
        // Ariba 8.1: Removed ariba.util.core.Enumeration because it's moved and Enumeration is imported above
        for(Iterator e = supplier.getLocationsObjects(); e.hasNext();)
        {
            
            // here, we put in the edit so that only those supplier locations that have address type = P are selected
            SupplierLocation location = (SupplierLocation)e.next();
            addressType = (String)location.getFieldValue("AddressType");
            if (addressType != null) 
            {
                if (addressType.equals("P"))
                {
                    
                    return location;
                    
                }
            }
        }
        
        return null;
    }
    
    protected SupplierLocation getCorrectLocation(Supplier supplier)
    {
        String addressType=null;
        
        AQLOptions opt = new AQLOptions();
        opt.setUserLocale(Base.getSession().getLocale());
        opt.setUserPartition(Base.getSession().getPartition());
        String supplierName = (String)supplier.getFieldValue("UniqueName");
        Log.customer.debug("Supplier UniqueName is %s",supplierName);
        
        String queryText = Fmt.S("SELECT Locations FROM Supplier where Supplier.UniqueName = " +
                                 "%s ORDER BY Locations.ContactID",
                                 AQLScalarExpression.buildLiteral(supplierName).toString()); 
        
        AQLQuery query = AQLQuery.parseQuery(queryText);
        
        AQLResultCollection resultsCollection =
            Base.getService().executeQuery(query, opt);
        
        SupplierLocation location = null;
        
        while (resultsCollection.next())
        {
            BaseId id = resultsCollection.getBaseId(0);               
            location = (SupplierLocation)id.get();
            addressType = (String)location.getFieldValue("AddressType");
            if (addressType != null) 
            {
                if (addressType.equals("P"))
                {
                    return location;
                }
            }
        }
        
        return null;
    }
    
    protected ValueInfo getValueInfo()
    {
        return valueInfo;
    }
    
    protected ValueInfo[] getParameterInfo()
    {
        return parameterInfo;
    }
    
    protected String[] getRequiredParameterNames()
    {
        return requiredParameterNames;
    }
    
    // This method was in the decafed code. However, it seems to have an incorrect signature and is not used as such in the code
    //public SetSupplierLocation()
    //{
    //}
    
}
