/**
 * @author  Andrius Karpavicius
 * @version 1.0
 * Defines web utility functions for Buysense.com user management
 */


package com.buysense.dw;

import javax.servlet.http.*;

public class BuysenseUtils
{
    
    // Retrieve a parameter from request and return a default if one not found
    public static int getParameter(HttpServletRequest request, String name, int def)
    {
        
        Object obj = request.getParameter(name);
        if (obj==null)
        {
            return def;
        } else
        {
            return Integer.valueOf((String)obj).intValue();
        }
    }
    
    // Retrieve a parameter from request and return a default if one not found
    public static String getParameter(HttpServletRequest request, String name, String def)
    {
        
        Object obj[] = request.getParameterValues(name);
        if (obj==null)
        {
            return def;
        } else
        {
            String value = (String)obj[0];
            for (int i=1; i<obj.length; i++)
            {
                value +=","+ (String)obj[i];
            }
            return value;
        }
    }
    
    // Print and manage errors
    public static void logError(String className, Exception e)
    {
        System.err.println(className + ": " + e);
    }
    
    public static void logError(String className, String e)
    {
        System.err.println(className + ": " + e);
    }
    
    
}
