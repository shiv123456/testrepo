/**
 * This class provides a message storage object.
 * @author AMS Inc. All rights reserved.
 *
 * @version 1.1.1
 * @since Ver 1.0.0
 */

package com.buysense.dw;

import javax.mail.internet.*;


public class BuysenseMessage
{
    
    // Sender address
    private InternetAddress from = null;
    
    // Recipient addresses
    private InternetAddress[] to = null;
    private InternetAddress[] cc = null;
    private InternetAddress[] bcc = null;
    
    private String subject = null;
    private String message = null;
    private Object attachment = null;
    
    
    public void setFrom(InternetAddress ia)
    {
        from = ia;
    }
    
    public InternetAddress getFrom()
    {
        return from;
    }
    
    public void setRecipientsTO(InternetAddress[] ia)
    {
        to = ia;
    }
    
    public InternetAddress[] getRecipientsTO()
    {
        return to;
    }
    
    public void setRecipientsCC(InternetAddress[] ia)
    {
        cc = ia;
    }
    
    public InternetAddress[] getRecipientsCC()
    {
        return cc;
    }
    
    public void setRecipientsBCC(InternetAddress[] ia)
    {
        bcc = ia;
    }
    
    public InternetAddress[] getRecipientsBCC()
    {
        return bcc;
    }
    
    public void setSubject(String s)
    {
        subject = s;
    }
    
    public String getSubject()
    {
        return subject;
    }
    
    public void setMessage(String s)
    {
        message = s;
    }
    
    public String getMessage()
    {
        return message;
    }
    
    
    public void setAttachment(Object obj)
    {
        attachment = obj;
    }
    
    public Object getAttachment()
    {
        return attachment;
    }
}
