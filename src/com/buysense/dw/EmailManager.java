/**
 * This class manages the email queue - places messages in the queue and retrieves messages from the queue.
 * @author AMS Inc. All rights reserved.
 *
 * @version 1.1.1
 * @since Ver 1.0.0
 */

package com.buysense.dw;


import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
//import com.buysense.lang.BuysenseException;
//import com.buysense.utils.BuysenseUtils;
import ariba.util.log.Log;

public class EmailManager
{
    
    private static Vector messages=new Vector();
    
    /**
     * Default Constructor
     */
    public EmailManager()
    {
        
        
    }
    
    /**
     * Adds the passed message into the queue.
     */
    public static void sendEmail(String sendTO, String sendCC, String sendBCC, String fromEmail, String fromName, String subject, String message, Object attachment) throws BuysenseException
    {
        
        // Create a message storage object and append to the message que
        try
        {
            
            // Construct a message frame
            BuysenseMessage msg = new BuysenseMessage();
            
            // Set sender if applicable. Else use a default email address
            if ((fromEmail!=null) && (fromName!=null))
            {
                msg.setFrom(new InternetAddress(fromEmail, fromName));
            } else if (fromEmail!=null)
            {
                msg.setFrom(new InternetAddress(fromEmail));
            } else
            {
                msg.setFrom(new InternetAddress("support@buysense.com", "Buysense.com Support"));
            }
            
            // Prepare To recipient addresses
            InternetAddress[] recipients = InternetAddress.parse(sendTO);
            msg.setRecipientsTO(recipients);
            
            // Prepare CC recipient addresses
            if (sendCC!=null)
            {
                recipients = InternetAddress.parse(sendCC);
                msg.setRecipientsCC(recipients);
            }
            
            // Prepare BCC recipient addresses
            if (sendBCC!=null)
            {
                recipients = InternetAddress.parse(sendBCC);
                msg.setRecipientsBCC(recipients);
            }
            
            // Set up message subject, message and attachments
            msg.setSubject(subject);
            msg.setMessage(message);
            msg.setAttachment(attachment);
            
            // Append message to the que
            messages.addElement(msg);
            
        } catch (Exception e)
        {
           // Log.debug.utilsBuysenseUtils.logError("com.buysense.dw.EmailManager.sendEmail()",e);
           // throw new BuysenseException(e);
           Log.util.debug("EmailManager:sendEmail also failed to send Email" , e.toString());
        }
        
    }
    
    
     /*
      * Simplified send email invocation.
      */
    public static void sendEmail(String sendTO, String fromEmail, String fromName, String subject, String message) throws BuysenseException
    {
        sendEmail(sendTO, null, null, fromEmail, fromName, subject, message, null);
    }
    
     /*
      * Simplified send email invocation.
      */
    public static void sendEmail(String sendTO, String subject, String message) throws BuysenseException
    {
        sendEmail(sendTO, null, null, null, null, subject, message, null);
    }
    
    /**
     * Checks if the queue is empty.
     * @return boolean
     */
    public static boolean isMailQueueEmpty()
    {
        boolean result = false;
        
        if (messages.size()==0)
        {
            result=true;
        }
        return result;
    }
    
    /**
     * Retrieves one message from the queue.
     */
    public static synchronized BuysenseMessage getMessage()
    {
        BuysenseMessage message = (BuysenseMessage)messages.elementAt(0);
        messages.removeElementAt(0);
        messages.trimToSize();
        return message;
    }
}
