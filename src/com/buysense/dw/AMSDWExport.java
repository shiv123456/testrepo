package com.buysense.dw;

/*

jnamadan, 11/05/03 Dev SPL 189 - Comment out calls to ComposeEmailMessage as they are no longer needed

*/

/** AMSDWExport gets AQL for the specified table and database connection and uses the metadata 
   from the AQL to dynamically build the INSERT statement. Then it will populate the prepared
   statement as per their data types and finally creates the records into the staging area 
*/

import java.util.Map;

import ariba.base.core.Base;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
// Ariba 8.1 Changed Map to *
import ariba.util.core.*;
import ariba.util.log.Log;
import ariba.base.core.Partition;

import java.sql.*;
import java.util.Calendar;
import java.util.logging.Level;


public class AMSDWExport
{
    // Variables for sending emails
    // these will be set to correct values when they are available
    // private static java.util.Date ReqTimeStamp = null;
    private static String BuysenseClientId = "Not Available";
    private static String ETLNumber = "Not Available";
    private static String TableName = " Not available";
    private static String RowCount = " Not available";
    private static String ErrMessage = " Not available";
    private static String EmailSubject = "Subject not set";
    // end varibales for sending emails
    
    public AMSDWExport ()
    {
    }
   // Ariba 8.1 include Partition as argumant 
   // public static boolean export(String fsTableName, String fsTableAQL, Connection foConnection, ariba.util.core.Date foAribaFromDate, ariba.util.core.Date foAribaToDate, int fiRunNumber, String fsCLIENT_ID)
    public static boolean export(String fsTableName, String fsTableAQL, Connection foConnection, ariba.util.core.Date foAribaFromDate, ariba.util.core.Date foAribaToDate, int fiRunNumber, String fsCLIENT_ID, Partition foPartition)
    {
       String lsTableName = fsTableName;
	   String lsTableAQL  = fsTableAQL;
       java.sql.Connection loConnection = foConnection;
       ariba.util.core.Date loAribaFromDate =  foAribaFromDate;
       ariba.util.core.Date loAribaToDate =  foAribaToDate;
       int liRunNumber = fiRunNumber;
       String lsBUYSENSE_CLIENT_ID = fsCLIENT_ID;
       
       PreparedStatement  loPreparedStatement = null;
       PreparedStatement loLogPreparedStatement = null;
       
       String lsSQLInsert;
       String lsSQLValues;
       String msExtractMessage = "Run Started";
       String msKeys = "";
       int liAQLColCount;
       int liRowCount = 0;
       int liCommitTime = 0;
       boolean lbFirstRow = true; 
	   AQLResultCollection loResultSet;

       Calendar loCalendar = Calendar.getInstance();
       
       java.util.Date loTodaysUtilDate = loCalendar.getTime();
       long loLocalTime = loTodaysUtilDate.getTime(); // time since 1,1,1970
       java.sql.Date loTodaysDate = new java.sql.Date(loLocalTime); //converted to java.sql.date
       
       //Level currLevel = Log.util.getLevel(); 
       //Log.util.setLevel(Level.FINEST);
     org.apache.log4j.Level currLevel = Log.util.getLevel();
       Log.util.setLevel(Log.DebugLevel.DEBUG);

       // Set available variables for email messages
       // ReqTimeStamp = loTodaysUtilDate;
       BuysenseClientId = lsBUYSENSE_CLIENT_ID;
       ETLNumber = String.valueOf(liRunNumber);
       TableName = lsTableName;
       // End set available varibales for email messages
       
	   //Log.util.debug("AMSDWExport started for table = %s ", lsTableName );
       Log.util.debug("EVA AMSDWExport started for table = %s ", lsTableName );
       try
       {
          //clean the stage table first
          Log.util.debug("AMSDWExport: clean the tables first");
          PreparedStatement l_pst = loConnection.prepareStatement("delete from " + lsTableName);
          int li_stat = l_pst.executeUpdate();
          Log.util.debug("AMSDWExport: clean the table return code ", li_stat);
          
          //delete the old log entry for this table too
          String lsDelete = "DELETE FROM DW_ETL_LOGS WHERE TABLE_NAME = '" + lsTableName + "' AND BUYSENSE_CLIENT_ID = '" + lsBUYSENSE_CLIENT_ID + "'";
          Log.util.debug("AMSDWExport: lsDelete = %s ", lsDelete);
          l_pst = loConnection.prepareStatement(lsDelete);
          Log.util.debug("AMSDWExport: prepared");
          li_stat = l_pst.executeUpdate();
          Log.util.debug("AMSDWExport: executed");
          l_pst.close();
          Log.util.debug("AMSDWExport: clean the tables statement closed");
          
          // Build the shell columns for DW_ETL_LOGS
	      lsSQLInsert = "INSERT INTO DW_ETL_LOGS (BUYSENSE_CLIENT_ID, ETL_NUM, TABLE_NAME, ETL_DATE, RECS_EXTRACTED_FROM_ARIBA, EXTRACT_MESSAGE, RUN_FLAG) VALUES (?, ?, ?, ?, ?, ?, ?)";
          loLogPreparedStatement = loConnection.prepareStatement(lsSQLInsert);
          loLogPreparedStatement.setString(1, lsBUYSENSE_CLIENT_ID);              //BUYSENSE_CLIENT_ID
          loLogPreparedStatement.setInt(2, liRunNumber);                          //ETL_NUM
          loLogPreparedStatement.setString(3, lsTableName);                       //TABLE_NAME
          loLogPreparedStatement.setDate(4, loTodaysDate);                        //ETL_DATE
          loLogPreparedStatement.setInt(5, 0);                                    //RECS_EXTRACTED_FROM_ARIBA
          loLogPreparedStatement.setString(6, msExtractMessage);                  //EXTRACT_MESSAGE
          loLogPreparedStatement.setString(7, "N");                               //RUN_FLAG
       }
       catch (SQLException loEx)
       {
          msExtractMessage = "AMSDWExport:SQL Exception during Delete table in stage area DW_ETL_LOGS " + loEx.toString(); 
/* Dev #189 - Comment out call to ComposeEmailMessage          
          // set email variables and send email
          
          RowCount = String.valueOf(liRowCount);
          EmailSubject = "EVA AMSDWExport SQL Exception during Delete table in stage area DW_ETL_LOGS ";
          AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExport","Failed", BuysenseClientId, ETLNumber,TableName, RowCount, msExtractMessage);
          // end send email
*/          
          Log.util.setLevel(currLevel);
          return statusFailed(loConnection, loLogPreparedStatement, lsBUYSENSE_CLIENT_ID, liRunNumber, lsTableName,loTodaysDate, liRowCount, msExtractMessage);
       } //end try

       AQLQuery loWhereQuery = AQLQuery.parseQuery(lsTableAQL);

       AQLOptions loOptions = new AQLOptions();
       /*
         In case a Date range is to be used for the AQL then we need to (dynamically) populate 
         those dates from DW_DATE_CTRL :FROM_DATE and :TO_DATE (both) have to be present in the AQL.
       */
       if (lsTableAQL.indexOf(":FROM_DATE") > 0)  
       {
          Map loParamHashtable = MapUtil.map();
          loParamHashtable.put("FROM_DATE", loAribaFromDate); //pass From_Date to the parameter
          Log.util.debug("AMSDWExport:insertParameters: From AribaDate is = %s  ", loAribaFromDate);
     
          loParamHashtable.put("TO_DATE", loAribaToDate); //pass To_Date to the parameter
          Log.util.debug("AMSDWExport:insertParameters: To AribaDate is = %s  ", loAribaToDate);
               
          loOptions.setActualParameters(loParamHashtable);
               
         Log.util.debug("AMSDWExport: lsAQL's date parameters are set for %s", lsTableAQL);
       } //end :FROM_DATE

       loOptions.setUserLocale(Base.getSession().getLocale());

       // Ariba 8.1 Partition from Driver
       //loOptions.setUserPartition(Base.getService().getPartition());
       loOptions.setUserPartition(foPartition);

       Log.util.debug("AMSDWExport: Ready to execute the AQL query");

       loResultSet =  (AQLResultCollection) Base.getService().executeQuery(loWhereQuery, loOptions);

       if (loResultSet.getFirstError() != null) 
       {
          msExtractMessage = "AMSDWExport: got error in execute AQL = " + loResultSet.getFirstError().toString();
/* Dev #189 - Comment out call to ComposeEmailMessage          
          // send email
          RowCount = String.valueOf(liRowCount);
          EmailSubject = "EVA AMSDWExport got error in execute AQL";
          AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExport","Failed", BuysenseClientId, ETLNumber,TableName, RowCount, msExtractMessage);
          // end send email
*/          
          Log.util.setLevel(currLevel);
          return statusFailed(loConnection, loLogPreparedStatement, lsBUYSENSE_CLIENT_ID, liRunNumber, lsTableName, loTodaysDate, liRowCount, msExtractMessage);
       }//end-if first error
        
       liAQLColCount = loResultSet.getResultFieldCount();
       Log.util.debug("AMSDWExport: liAQLColCount = %s" , liAQLColCount);
       
       if (loResultSet.next() == false) 
       {
          msExtractMessage = "AMSDWExport: No Records to Process - loResultSet.next is null";
          Log.util.debug(msExtractMessage); 
          try 
          {          
             Log.util.debug("and that is OOKKKKK");
             loLogPreparedStatement.setString(1, lsBUYSENSE_CLIENT_ID);  //BUYSENSE_CLIENT_ID 
                loLogPreparedStatement.setInt(2, liRunNumber);           //ETL_NUM
             loLogPreparedStatement.setString(3, lsTableName);           //TABLE_NAME
               loLogPreparedStatement.setDate(4, loTodaysDate);          //ETL_DATE
                loLogPreparedStatement.setInt(5, 0);                     //RECS_EXTRACTED_FROM_ARIBA
             loLogPreparedStatement.setString(6, msExtractMessage);      //EXTRACT_MESSAGE
             loLogPreparedStatement.setString(7, "Y");                   //RUN_FLAG
             loLogPreparedStatement.executeUpdate();
             Log.util.debug("so return true");
             Log.util.setLevel(currLevel);
             return true; //statusFailed(loConnection, loLogPreparedStatement, lsBUYSENSE_CLIENT_ID, liRunNumber, lsTableName, loTodaysDate, liRowCount, msExtractMessage);
          }
          catch (SQLException loEx)
          {
             msExtractMessage = "AMSDWExport:SQL Exception in writing to the log for zero rows = " + loEx.toString();
/* Dev #189 - Comment out call to ComposeEmailMessage             
             // set email variables and send email
          
             RowCount = String.valueOf(liRowCount);
             EmailSubject = "EVA AMSDWExport:SQL Exception in writing to the log for zero rows";
             AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExport","Failed", BuysenseClientId, ETLNumber,TableName, RowCount, msExtractMessage);
             // end send email
*/             
             Log.util.setLevel(currLevel);
             //get out
             
             return statusFailed(loConnection, loLogPreparedStatement, lsBUYSENSE_CLIENT_ID, liRunNumber, lsTableName, loTodaysDate, liRowCount, msExtractMessage);
          } //end try1
       }
       else       //got the first row
       {
          Log.util.debug("AMSDWExport: We have the first Row");                    
          //At the first row you can get metadata to prepare JDBC INSERT statement

          lsSQLInsert = new String("INSERT INTO " + lsTableName + " ( ");
          lsSQLValues = new String(" , DW_ETLDATE, DW_ETLNUM) VALUES ( ");

          try 
          {
             for (int li = 0; li <liAQLColCount ; li++) 
             {
                Log.util.debug("AMSDWExport: Get the details for column = %s", li);
                if (li != 0) // for the first column you do not need a comma prefix
                {
                   lsSQLInsert = lsSQLInsert + ", ";
                   lsSQLValues = lsSQLValues + ", ";
                } //end-if (li != 0)

                // Get the column�s name
                Log.util.debug("AMSDWExport: the getFieldName = %s ", loResultSet.getFieldName(li));
                Log.util.debug("AMSDWExport: the getFieldClassName = %s ", loResultSet.getFieldClassName(li));
                lsSQLInsert = lsSQLInsert + loResultSet.getFieldName(li);
                lsSQLValues = lsSQLValues + " ? "; // corresponding Parameter marker

             } // end-for all columns

             lsSQLValues = lsSQLValues + " ,? , ? )";  //for DW_ETLDATE and DW_ETLNUM

             Log.util.debug("AMSDWExport:PreparedStatement for lsSQLInsert = %s " , lsSQLInsert);
             Log.util.debug("and msSQLValue = %s " , lsSQLValues);
                
             loPreparedStatement = loConnection.prepareStatement(lsSQLInsert + lsSQLValues);

             // You already have the first row so insert that first

             liRowCount = 1;
             
             if ( insertParameters(loResultSet, loPreparedStatement, liAQLColCount) )  // for this row
	         {
	            loPreparedStatement.setDate(liAQLColCount + 1 , loTodaysDate);
	            loPreparedStatement.setInt(liAQLColCount + 2, liRunNumber);
	            loPreparedStatement.executeUpdate();
	            //rest of the rows will be handled in the WHILE loop below
	         }
	         else
	         {
	            //else give error and get out
	            msExtractMessage = "AMSDWExport: Error in insertParameters. Null data or invalid data type, check AQL - 1";
/* Dev #189 - Comment out call to ComposeEmailMessage                
                // set email variables and send email
              
                RowCount = String.valueOf(liRowCount);
                EmailSubject = "EVA AMSDWExport:Error in insertParameters. Null data or invalid data type, check AQL - 1";
                AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExport","Failed", BuysenseClientId, ETLNumber,TableName, RowCount, msExtractMessage);
                // end send email
*/                    
	            Log.util.setLevel(currLevel);
                return statusFailed(loConnection, loLogPreparedStatement, lsBUYSENSE_CLIENT_ID, liRunNumber, lsTableName, loTodaysDate, liRowCount, msExtractMessage);
			 } 
          }
          catch (Exception loEx) 
          {
             msExtractMessage = "AMSDWExport: could not build INSERT this row in staging are due to = " + loEx.toString();
/* Dev #189 - Comment out call to ComposeEmailMessage             
             // set email variables and send email

             RowCount = String.valueOf(liRowCount);
             EmailSubject = "EVA AMSDWExport: could not build INSERT this row ";
             AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExport","Failed", BuysenseClientId, ETLNumber,TableName, RowCount, msExtractMessage);
             // end send email
*/             
             Log.util.setLevel(currLevel);
             //get out
             return statusFailed(loConnection, loLogPreparedStatement, lsBUYSENSE_CLIENT_ID, liRunNumber, lsTableName, loTodaysDate, liRowCount, msExtractMessage);
          }// Try build the INS SQL
       } //end-if empty result-set or one found
       
       try 
       {
          Log.util.debug("AMSDWExport: loop for all the rows");
          
          while (loResultSet.next() ) // loop for the remaining rows - starting from 2nd row
          {
             liRowCount++;
             msKeys = ""; //start fresh for each row
             //Log.util.debug("AMSDWExport: got some thing");
             //debug info: In case of the failure, if possible report the first three column values too.
             for (int li = 0 ; li <= 2 && li < liAQLColCount ; li++)
             {
                 //Log.util.debug ( "AMSDWExport:insertParameters: loop count = %s ", li ) ;

                if (loResultSet.getObject(li) == null) // a AQL Null Check required
                {
                    msKeys = msKeys + " : This column is NULL ";
                }
                else
                {
                   if (loResultSet.getFieldClassName(li).equals("java.lang.String")) 
                   {
                      msKeys =  msKeys.concat(" : " + loResultSet.getString(li) ).trim();
                   }
                   else
                   {
                      msKeys = msKeys + " : This column is NON_STRING ";
                   }
                }
             }//end for debug info

             try
             {
	            if ( insertParameters(loResultSet, loPreparedStatement, liAQLColCount) )  // for this row
	            {
	               loPreparedStatement.setDate(liAQLColCount + 1 , loTodaysDate);
	               loPreparedStatement.setInt(liAQLColCount + 2, liRunNumber);
	               loPreparedStatement.executeUpdate();
	               liCommitTime++;
	               //Log.util.debug("AMSDWExport:################# one more row inserted - commit count = %", liCommitTime);
	               /*
	               if (liCommitTime > 999) 
	               {
	                 Log.util.debug("AMSDWExport:################# NO commit after EvErY 1000 rows - temp");
	                  //Log.util.debug("AMSDWExport:################# commit after EvErY 1000 rows - temp");
	                  //loConnection.commit();  //for huge catalogs - temp.
	                  liCommitTime = 0;
	                  //Log.util.debug("AMSDWExport:################# just committed so reset counter to 0");
	               }
	               else
	               {
	                Log.util.debug("AMSDWExport:################# dont commit yet");
	               }
	               */
	            }
	            else
	            {
	               //else give error and get out
	               msExtractMessage = "AMSDWExport: Error in insertParameters: Null data or invalid data type, check AQL";
/* Dev #189 - Comment out call to ComposeEmailMessage	               
	               // set email variables and send email
                  
                   RowCount = String.valueOf(liRowCount);
                   EmailSubject = "EVA AMSDWExport: Error in insertParameters: Null data or invalid data type, check AQL";
                   AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExport","Failed", BuysenseClientId, ETLNumber,TableName, RowCount, msExtractMessage);
                   // end send email
*/                   
	               Log.util.setLevel(currLevel);
                   return statusFailed(loConnection, loLogPreparedStatement, lsBUYSENSE_CLIENT_ID, liRunNumber, lsTableName, loTodaysDate, liRowCount, msExtractMessage);
			    } 
		     }			
             catch (SQLException loEx)
             {
                msExtractMessage = "AMSDWExport:SQL Exception in insert while working with the keys = " + msKeys + ":" + loEx.toString();
/* Dev #189 - Comment out call to ComposeEmailMessage                
                // set email variables and send email
              
                RowCount = String.valueOf(liRowCount);
                EmailSubject = "EVA AMSDWExport:SQL Exception in insert while working with the keys";
                AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExport","Failed", BuysenseClientId, ETLNumber,TableName, RowCount, msExtractMessage);
                // end send email
*/
                Log.util.setLevel(currLevel);
                //get out
                return statusFailed(loConnection, loLogPreparedStatement, lsBUYSENSE_CLIENT_ID, liRunNumber, lsTableName, loTodaysDate, liRowCount, msExtractMessage);
             } //end try1
          }// end While loop for all the rows
          Log.util.debug("processed %s rows", liRowCount);
        }
		catch ( Exception loEx )
		{
            msExtractMessage = "AMSDWExport: Failed:  = " + loEx.toString();
/* Dev #189 - Comment out call to ComposeEmailMessage           
            // set email variables and send email
              
            RowCount = String.valueOf(liRowCount);
            EmailSubject = "EVA AMSDWExport Failed ";
            AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExport","Failed", BuysenseClientId, ETLNumber,TableName, RowCount, msExtractMessage);
            // end send email
*/               
            Log.util.setLevel(currLevel);
            return statusFailed(loConnection, loLogPreparedStatement, lsBUYSENSE_CLIENT_ID, liRunNumber,lsTableName, loTodaysDate, liRowCount, msExtractMessage);
		} 
		
        try
        {
	       Log.util.debug("AMSDWExport:going to insert the values ---- success ---- so write it to log");

           loLogPreparedStatement.setString(1, lsBUYSENSE_CLIENT_ID);//BUYSENSE_ID
              loLogPreparedStatement.setInt(2, liRunNumber);         //ETL_NUM
           loLogPreparedStatement.setString(3, lsTableName);         //TABLE_NAME
             loLogPreparedStatement.setDate(4, loTodaysDate);        //ETL_DATE
              loLogPreparedStatement.setInt(5, liRowCount);          //RECS_EXTR
           loLogPreparedStatement.setString(6, "SUCCESSFUL EXTRACT");//EXTRACT_MSG
           loLogPreparedStatement.setString(7, "Y");                 //RUN_FLAG

           loLogPreparedStatement.executeUpdate();
           Log.util.setLevel(currLevel);
           return true;
        }			
        catch (SQLException loEx)
        {
            msExtractMessage = "AMSDWExport:SQL Exception in updating the log = " + loEx.toString();
/* Dev #189 - Comment out call to ComposeEmailMessage           
            // set email variables and send email
          
            RowCount = String.valueOf(liRowCount);
            EmailSubject = "EVA AMSDWExport:SQL Exception in updating the log";
            AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExport","Failed", BuysenseClientId, ETLNumber,TableName, RowCount, msExtractMessage);
            // end send email
*/
            Log.util.setLevel(currLevel);

            //get out
            return statusFailed(loConnection, loLogPreparedStatement, lsBUYSENSE_CLIENT_ID, liRunNumber, lsTableName, loTodaysDate, liRowCount, msExtractMessage);
        } //end try
           
    } // end export

    //        Other methods

    //----------------------------------------------------------------
    // update values 
    //----------------------------------------------------------------
    private static boolean insertParameters (AQLResultCollection foResultSet, PreparedStatement foPreparedStatement, int fiAQLColCount) 
    { 
       AQLResultCollection loResultSet = foResultSet;
       PreparedStatement loPreparedStatement = foPreparedStatement;
       int liAQLColCount = fiAQLColCount;

       boolean lbReturn = true;
        //Log.util.debug ( "AMSDWExport:insertParameters starting loop") ;
       for (int li = 0 ; li <= ( liAQLColCount - 1)  ; li++) //loop through all the columns except last two
       {
           

          try 
          {
             //----------- Integer -------/
             if (loResultSet.getFieldClassName(li).equals("java.lang.Integer") )//|| loResultSet.getFieldClassName(li).equals("java.lang.int?") ) 
             {
                 //Log.util.debug("AMSDWEexport:insertParameters: Integer Column Number = %s  ", li);
                loPreparedStatement.setInt(li + 1 , loResultSet.getInteger(li) );
             } 
             //----------- String -------/
             else if (loResultSet.getFieldClassName(li).equals("java.lang.String")) 
             {
                 //Log.util.debug("AMSDWExport:insertParameters: string Column Number = %s  ", li);
                loPreparedStatement.setString(li + 1 , loResultSet.getString(li) );
             } 
             //----------- Boolean -------/
             else if (loResultSet.getFieldClassName(li).equals("java.lang.Boolean")) 
             {
                 //Log.util.debug("AMSDWExport:insertParameters: Boolean Column Number = %s  ", li);
                if ( loResultSet.getBoolean(li) )   //Boolean to Int
                {
                    //Log.util.debug("AMSDWExport:insertParameters: Boolean going for true");
                  loPreparedStatement.setInt(li + 1 , 1 );
                }
                else
                {
                   loPreparedStatement.setInt(li + 1 , 0 );
                    //Log.util.debug("AMSDWExport:insertParameters: Boolean going for false");
                }
             } 
             //----------- Double -------/
             else if (loResultSet.getFieldClassName(li).equals("java.lang.Double")) //why li +1?
             {
                 //Log.util.debug("AMSDWExport:insertParameters: double Column Number = %s  ", li);
                loPreparedStatement.setDouble(li + 1, loResultSet.getDouble(li) );
             } 
             //----------- Date -------/
             else if (loResultSet.getFieldClassName(li).equals("ariba.util.core.Date")) 
             {
                 //Log.util.debug("AMSDWExport:insertParameters: date Column Number = %s  ", li);
                 //Log.util.debug("AMSDWExport:insertParameters: date is = %s  ", loResultSet.getDate(li));
                if (loResultSet.getDate(li) != null)
                {
                   ariba.util.core.Date loAribaDate = loResultSet.getDate(li);
                    //Log.util.debug("AMSDWExport:insertParameters: AribaDate is = %s  ", loAribaDate);
                   long llGetTime = loAribaDate.getTime();
                    //Log.util.debug("AMSDWExport:insertParameters: AribaDate long(int) time since 1/1/70 is = %s  ", (int)llGetTime);
                   java.sql.Date loSQLDate = new java.sql.Date(llGetTime);
                   java.sql.Timestamp loSQLTimestamp = new java.sql.Timestamp(llGetTime);
                    //Log.util.debug("AMSDWExport:insertParameters: loSQLDate using long time since 1/1/70 is = %s  ", loSQLDate);
                   loPreparedStatement.setDate(li + 1 , loSQLDate);
                   loPreparedStatement.setTimestamp(li + 1 , loSQLTimestamp);
                }
                else
                {
                   //Log.util.debug("AMSDWExport:insertParameters: java SQL date is = %s  ", (java.sql.Date)((java.util.Date) loResultSet.getDate(li)));
                   loPreparedStatement.setDate(li + 1 , (java.sql.Date)((java.util.Date) loResultSet.getDate(li)) ); //ariba.util.core.Date
                }
             } 
             //----------- Long -------/
             else if (loResultSet.getFieldClassName(li).equals("java.lang.Long")) 
             {
                 //Log.util.debug("AMSDWExport:insertParameters: Long Column Number = %s  ", li); 
                loPreparedStatement.setLong(li + 1 , loResultSet.getLong(li) );
             }
             //----------BigDecimal---/
             else if (loResultSet.getFieldClassName(li).equals("java.math.BigDecimal")) 
             {
                 //Log.util.debug("AMSDWExport:insertParameters: BigDecimal Column Number = %s  ", li);
                loPreparedStatement.setBigDecimal(li + 1 , loResultSet.getBigDecimal(li));
             }
             //----- unfamiliar type ---/
             else 
             {
                Log.util.debug("AMSDWExport: Check Field Name = %s ", loResultSet.getFieldName(li));
                Log.util.debug("AMSDWExport:insertParameters:ERROR: - Check AQL - Column type not recognized = %s", loResultSet.getFieldClassName(li) );
                //Please make sure you are getting any Ariba specific elements like BaseIDs etc.
                lbReturn = false;
/* Dev #189 - Comment out call to ComposeEmailMessage                
                // set email variables and send email
              
                RowCount = "Not avilable in this function";
                EmailSubject = "EVA AMSDWExport:insertParameters:Error";
                ErrMessage = " EVA AMSDWExport:insertParameters:ERROR: - Check AQL - Column type not recognized = " + loResultSet.getFieldClassName(li);
                AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExport","Insert Paramters Error", BuysenseClientId, ETLNumber,TableName, RowCount, ErrMessage);
                // end send email
*/                
             } // end-if setxxx
          } 
          catch (Exception loExp) 
          {
             
             Log.util.debug("AMSDWExport:insertParameters:Error AMSResultSet.setParamValues could not set values in the PreparedStatement for the INSERT=  %s" , loExp.getMessage());
             lbReturn = false;
/* Dev #189 - Comment out call to ComposeEmailMessage             
             // set email variables and send email
          
             RowCount = "Not avilable in this function";
             EmailSubject = "EVA AMSDWExport:insertParameters:Error";
             ErrMessage = " EVA AMSDWExport:insertParameters:Error AMSResultSet.setParamValues could not set values in the PreparedStatement for the INSERT= " + loExp.getMessage();
             AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExport","Insert Parameters Error", BuysenseClientId, ETLNumber,TableName, RowCount, ErrMessage);
             // end send email
*/             
             
          }

       } // end loop for all the columns ----- end
       return lbReturn;
    } // end insertParameters

    private static boolean statusFailed(java.sql.Connection foConnection, PreparedStatement foLogPreparedStatement, String fsClient, int fiETLNum, String fsTableName, java.sql.Date foTodaysDate, int fiRowCount, String fsExtractMessage)
    {  
       PreparedStatement loLogPreparedStatement = foLogPreparedStatement;
       Log.util.debug("*****************************************************************");
       Log.util.debug("*****AMSDWExport:statusFailed for client = %s", fsClient);
       Log.util.debug("*****                             etlnum = %s", fiETLNum);
       Log.util.debug("*****                              table = %s", fsTableName);
       Log.util.debug("*****                               date = %s ",foTodaysDate);
       Log.util.debug("*****                               rows = %s ",fiRowCount);
       Log.util.debug("*****                            message = %s", fsExtractMessage);
       Log.util.debug("*****************************************************************");
       try
       {
        Log.util.debug("AMSDWExport:statusFailed attempting to rollback" );
        //first thing first, rollback
        foConnection.rollback();
        Log.util.debug("AMSDWExport:statusFailed attempting to preparestatement" );
        loLogPreparedStatement.setString(1, fsClient);                 //BUYSENSE_CLIENT_ID 
        loLogPreparedStatement.setInt(2, fiETLNum);                 //ETL_NUM
        loLogPreparedStatement.setString(3, fsTableName);           //TABLE_NAME
        loLogPreparedStatement.setDate(4, foTodaysDate);            //ETL_DATE
        loLogPreparedStatement.setInt(5, fiRowCount);               //RECS_EXTRACTED_FROM_ARIBA
        loLogPreparedStatement.setString(6, fsExtractMessage);      //EXTRACT_MESSAGE
        loLogPreparedStatement.setString(7, "N");                   //RUN_FLAG
         
/* Dev #189 - Comment out call to ComposeEmailMessage       
        // set email variables and send email
          
        RowCount = String.valueOf(fiRowCount);
        EmailSubject = "EVA AMSDWExport Message from statusFailed()";
        AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExport","Failed", BuysenseClientId, ETLNumber,TableName, RowCount, fsExtractMessage);
        // end send email
*/
        loLogPreparedStatement.executeUpdate();
        foConnection.commit(); //so that this entry goes in the DW_ETL_LOGS table
      }
      catch (SQLException loEx)
      {
        Log.util.debug("AMSDWExport:statusFailed also failed to update the DW_ETL_LOGS =  %s" , loEx.toString()); 
/* Dev #189 - Comment out call to ComposeEmailMessage        
        // send email
        
        RowCount = String.valueOf(fiRowCount);
        EmailSubject = "EVA AMSDWExport:statusFailed also failed to update the DW_ETL_LOGS";
        ErrMessage = " EVA AMSDWExport:statusFailed also failed to update the DW_ETL_LOGS =  " + loEx.toString();
        AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExport","Failed", BuysenseClientId, ETLNumber,TableName, RowCount, ErrMessage);
        // end send email
*/        
      } 
      catch (Exception loNonSQLEx)
      {
        Log.util.debug("AMSDWExport:statusFailed caught non SQLException = %s", loNonSQLEx.toString());
/* Dev #189 - Comment out call to ComposeEmailMessage        
        // send email
        
        RowCount = String.valueOf(fiRowCount);
        EmailSubject = "EVA AMSDWExport:statusFailed caught non SQLException";
        ErrMessage = " EVA AMSDWExport:statusFailed caught non SQLException =  " + loNonSQLEx.toString();
        AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExport","Failed", BuysenseClientId, ETLNumber,TableName, RowCount, ErrMessage);
        // end send email
*/
      }
      //end try
    
      return false;
   }
   
  
} //end class
