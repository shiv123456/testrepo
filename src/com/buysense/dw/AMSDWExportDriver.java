/*
Anup - DEV SPL # 11 - 7.1a sync up : Defined ClassName and invoked super.init(), and
        get connection info components from 'arguments' in init() and use these class
        vars for DB connection in initConnection().

rlee, 5/23/03 Dev SPL 112 - DW Phase 2 Modify Extract Engine

jnamadan, 11/05/03 Dev SPL 189 - Comment out calls to ComposeEmailMessage as they are no longer needed

dchamber, 07/22/05 VEPI Dev SPL 612 - Modified to get clob object for AQL column

*/
package com.buysense.dw;

//@author Surendra Rajak, buysense, August 2000
import java.util.Map;
import java.sql.*;
import java.util.Calendar;
import ariba.base.core.Base;
import ariba.util.log.Log;
import ariba.server.objectserver.*;
import ariba.base.core.Partition;
// Ariba 8.1 not needed
//import java.util.Map;
// Ariba 8.1 Code using TableEdit commented out
//import ariba.util.core.TableEdit;

import ariba.base.core.Base;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
//import ariba.base.core.aql.AQLScalarExpression;
// Ariba 8.1 User of AdapterDirectory commented out.
//import ariba.server.objectserver.adapter.AdapterDirectory;
import ariba.util.core.*;
// Ariba 8.1: Added Iterator
import java.util.Iterator;
//Ariba 8.0: added import scheduler classes
import ariba.util.scheduler.*;

/**
This is the driver program that is picked by Ariba's scheduler. In order for this to happen
there has to be an entry in ScheduledTasks.table (under the proper partition folder). e.g.

AMSDWExport =
{
   ScheduledTaskClassName = "com.buysense.dw.AMSDWExportDriver";
   Schedules =
   {
      Schedule1 =
      {
         DayOfWeek = Weekday;
         Hour = 0;
      };
   };
};

This program is designed to run in everyday at midnight nightly cycle or on demand.

*/

//Ariba 8.0: replaced SimpleScheduledTask with ScheduledTask
public class AMSDWExportDriver extends ScheduledTask
{
    private final String ClassName = this.getClass().getName();
    private String    msDBType = null ;
    private String    msDBDriver = null ;
    private String    msDBPassword = null ;
    private String    msDBURL = null ;
    private String    msDBUser = null ;
    private String    msPartition = null;
    public Partition  moPartition = null;
   public Connection moConnection;
   private static EmailThread emailThread = null;

    // Variables for sending emails
    // these will be set to correct values when they are available
    private static java.util.Date DriverReqTimeStamp = null;
    private static String BuysenseClientId = "Not Available";
    private static String ETLNumber = "Not Available";
    private static String TableName = " Not available";
    private static String RowCount = " Not available";
    private static String ErrMessage = " Not available";
    private static String EmailSubject = "Subject not set";
    // end varibales for sending emails

   /**
   This init function gets automatoically invoked by Ariba on server start-up time.
   */

   // Ariba 8.1 init() signature changed.
   //public void init(Partition partition, String scheduledTaskName, Map arguments)
   public void init( Scheduler scheduler, String scheduledTaskName, Map arguments)
   {
      Log.util.debug("AMSDWExportDriver could call export during the init..");
      // DEV SPL # 11 - call super.init() and get connection info components from arguments
      // into class vars.
      super.init( scheduler, scheduledTaskName, arguments ) ;

      // Ariba 8.1: Map::keys() is deprecated by Map.keySet.iterator()
      // for ( ariba.util.core.Enumeration loEnum = arguments.keys(); loEnum.hasMoreElements() ; )
      for ( Iterator loItr = arguments.keySet().iterator(); loItr.hasNext() ; )
      {
            String lsKey = (String)loItr.next() ;

            if ( lsKey.equals( "DBType" ) )
            {
                msDBType = (String)arguments.get( lsKey );
                Log.util.debug( "Connection parameter specified as " + msDBType + "." ) ;
            }

            if ( lsKey.equals( "DBDriver" ) )
            {
                msDBDriver = (String)arguments.get( lsKey );
                Log.util.debug( "Connection parameter specified as " + msDBDriver + "." ) ;
            }

            if ( lsKey.equals( "DBPassword" ) )
            {
                msDBPassword = (String)arguments.get( lsKey );
                Log.util.debug( "Connection parameter specified as " + msDBPassword + "." ) ;
            }

            if ( lsKey.equals( "DBURL" ) )
            {
                msDBURL = (String)arguments.get( lsKey );
                Log.util.debug( "Connection parameter specified as " + msDBURL + "." ) ;
            }

            if ( lsKey.equals( "DBUser" ) )
            {
                msDBUser = (String)arguments.get( lsKey );
                Log.util.debug( "Connection parameter specified as " + msDBUser + "." ) ;
            }
            if ( lsKey.equals( "Partition" ) )
            {
                msPartition = (String)arguments.get( lsKey );
                Log.util.debug( "Connection parameter specified as " + msPartition + "." ) ;
                moPartition = Base.getService().getPartition(msPartition);
            }
        }

      Log.util.debug("AMSDWExportDriver starting email Thread..");
      // Start Email Thread
      try
      {
        if ((emailThread==null) || (!emailThread.isAlive()))
        {
            //System.err.println("thread init");
            emailThread = new EmailThread();
            emailThread.start();
            Log.util.debug("AMSDWExportDriver started email Thread successfully..");
        }
      }
      catch (Exception e)
      {
        Log.util.debug("AMSDWExportDriver could not start email Thread..");

      }
   }

   /**
    This is the method that gets "run" either via serverMonitor command option or when the scheduler picks up its
    entry at the slated time slot or started from the Enterprise Manager.
   */

   public void run() throws ScheduledTaskException
   {
      Log.util.debug("AMSDWExportDriver STARTED, see if you need the dB connection");
      boolean lbGoodDBStatus = true;

      // Set available varibales for sending emails
      Calendar loCalendar = Calendar.getInstance();
      DriverReqTimeStamp = loCalendar.getTime();
      // End set available email variables

      if (initConnection())
      {
         Log.util.debug("AMSDWExportDriver: dB connection done now get AQLs");

         // Ariba 8.1 MapUtil used
         Map loParamHashtable = MapUtil.map(); //use it to pass parms to export method
         String lsCLIENT_ID = "WKFLD";
         int liRunNumber = 0;
         ariba.util.core.Date moAribaFromDate = null;
         ariba.util.core.Date moAribaToDate = null;

         try
         {
            AQLResultCollection loBuysenseClientResultSet;
            AQLQuery loWhereQuery = AQLQuery.parseQuery("select ClientID from ariba.core.BuysenseClient");//
            AQLOptions loOptions = new AQLOptions();
            loOptions.setUserLocale(Base.getSession().getLocale());
            //loOptions.setUserPartition(Base.getService().getPartition());
            loOptions.setUserPartition(moPartition);
            Log.util.debug("AMSDWExportDriver: Ready to execute the AQL query");
            loBuysenseClientResultSet =  (AQLResultCollection) Base.getService().executeQuery(loWhereQuery, loOptions);

            if (loBuysenseClientResultSet.getFirstError() != null)
            {
               String lsExtractMessage = "AMSDWExportDriver: got error in ClientID AQL retrieve = " + loBuysenseClientResultSet.getFirstError().toString();
               Log.util.debug(lsExtractMessage);
/* Dev #189 - Comment out call to ComposeEmailMessage
               // send email
               EmailSubject = " EVA AMSDWExportDriver: WARNING!!!";
               ErrMessage = lsExtractMessage;
               AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExportDriver","Warning!!!", BuysenseClientId, ETLNumber,TableName, RowCount, ErrMessage);
               // end sending email
*/
            }//end-if first error

            if (loBuysenseClientResultSet.next() == false)
            {
               Log.util.debug("AMSDWExportDriver: WARNING: No Records in the BuysenseClient");
               lbGoodDBStatus = false;
/* Dev #189 - Comment out call to ComposeEmailMessage
               // send email
               EmailSubject = " EVA AMSDWExportDriver: WARNING!!!";
               ErrMessage = " EVA AMSDWExportDriver: WARNING: No Records in the BuysenseClient";
               AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExportDriver","Warning!!!", BuysenseClientId, ETLNumber,TableName, RowCount, ErrMessage);
               // end sending email
*/

               return;
            }

            lsCLIENT_ID = loBuysenseClientResultSet.getString(0).trim();
            Log.util.debug("AMSDWExportDriver: client id from the buysense org is = %s", lsCLIENT_ID);

            // set available email variables
            BuysenseClientId = lsCLIENT_ID;
            // end set available email variables

            String lsSQL = "SELECT * FROM DW_DATE_CTRL WHERE DW_TYPE = 'EXP01' AND CLIENT_ID = '" + lsCLIENT_ID + "'";

            Log.util.debug("AMSDWExportDriver: create statement for getting the stuff off DW_DATE_CTRL table");
            java.sql.Statement loDateStatement = moConnection.createStatement();

            Log.util.debug("AMSDWExportDriver: now get the Date details");

            ResultSet loDateResultSet = loDateStatement.executeQuery(lsSQL);

            if (loDateResultSet!=null && loDateResultSet.next() )
            {
               //lsCLIENT_ID = loDateResultSet.getString("CLIENT_ID");
               liRunNumber = loDateResultSet.getInt("RUN_NUMBER");  //later bump it by one and add todays date

               java.sql.Date loSQLDate = loDateResultSet.getDate("FROM_DATE"); //take this SQL date
               long llGetTime = loSQLDate.getTime(); //and convert it to standard millisecond since 1,1,1970
               moAribaFromDate = new ariba.util.core.Date(llGetTime); //and use it to get the Ariba Date

               loSQLDate = loDateResultSet.getDate("TO_DATE"); //take this SQL date
               llGetTime = loSQLDate.getTime(); //and convert it to standard millisecond since 1,1,1970
               moAribaToDate = new ariba.util.core.Date(llGetTime); //and use it to get the Ariba Date
            }
            else
            {
               String msExtractMessage = "AMSDWExportDriver: WARNING!!! data missing on DW_DATE_CTRL for " + lsCLIENT_ID + " client and DW_TYPE = 'EXP01'";
               Log.util.debug(msExtractMessage);
               lbGoodDBStatus = false;
/* Dev #189 - Comment out call to ComposeEmailMessage
               // send email
               EmailSubject = " EVA AMSDWExportDriver: WARNING!!!";
               ErrMessage = msExtractMessage;
               ETLNumber = String.valueOf(liRunNumber);
               AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExportDriver","WARNING!!!", BuysenseClientId, ETLNumber,TableName, RowCount, ErrMessage);
               // end sending email
*/

               return;
            }   //end-if there is data on dw_date_ctrl

            //The AQLs are on DW_AQLs so get them and process each one of them (only the flagged ones.)
            Log.util.debug("AMSDWExportDriver: create to statement to retrieve AQL");
            java.sql.Statement loAQLStatement = moConnection.createStatement();
            Log.util.debug("AMSDWExportDriver: now get AQLs for the active tables order by AQL_RUN_SEQ");

            // rlee 5/23/03: Dev SPL 112 Added Source = ARIBA
            ResultSet loResultSet = loAQLStatement.executeQuery("SELECT * FROM DW_AQLS WHERE EXPORT_FLAG = 1 AND SOURCE = 'ARIBA' ORDER BY AQL_RUN_SEQ");

            lbGoodDBStatus = true;  //start with a true flag

            while (loResultSet!=null && loResultSet.next())
            {
               String lsTableName = loResultSet.getString("EXPORT_TABLE_NAME");
               //eye catchers
               Log.util.debug("==================================================================");
               Log.util.debug("AMSDWExportDriver: got entry for table %s", lsTableName);
               Log.util.debug("==================================================================");

               //VEPI Dev SPL 612: AQL field in DB was changed to clob type. So, modified code to get it out.
               Clob loClob = loResultSet.getClob("AQL");
               String lsAQL = loClob.getSubString(1, (int)loClob.length());
               Log.util.debug("AMSDWExportDriver: its AQL %s", lsAQL);

               if (AMSDWExport.export(lsTableName, lsAQL, moConnection, moAribaFromDate, moAribaToDate, liRunNumber, lsCLIENT_ID, moPartition))
               {
                  Log.util.debug("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                  Log.util.debug("AMSDWExportDriver: commit the SUCCESS :-) for table %s", lsTableName);
                  Log.util.debug("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                  moConnection.commit();
/* Dev #189 - Comment out call to ComposeEmailMessage
                  // set email variables and send email
                  EmailSubject = " EVA AMSDWExportDriver: Incremental Success " ;
                  ETLNumber = String.valueOf(liRunNumber);
                  ErrMessage = " EVA AMSDWExportDriver: The Export SUCCEEDED for " + lsTableName ;
                  AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExportDriver"," Incremental Success", BuysenseClientId, ETLNumber, lsTableName, RowCount, ErrMessage);
                  // end send email
*/
               }
               else
               {
                  Log.util.debug("--------------------------------------------------------------------");
                  Log.util.debug("AMSDWExportDriver: NO SUCCESS :-( for table %s", lsTableName);
                  Log.util.debug("--------------------------------------------------------------------");
                  lbGoodDBStatus = false;
/* Dev #189 - Comment out call to ComposeEmailMessage
                  // set email variables and send email
                  EmailSubject = " EVA AMSDWExportDriver: Incremental Failure " ;
                  ETLNumber = String.valueOf(liRunNumber);
                  ErrMessage = "EVA AMSDWExportDriver: The Export Failed for " + lsTableName ;
                  AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExportDriver"," Incremental Failure", BuysenseClientId, ETLNumber, lsTableName, RowCount, ErrMessage);
                  // end send email
*/
               } //end one export call
            }//end while for all the exports

            if (lbGoodDBStatus)  //all the exports were good
            {
               Log.util.debug("AMSDWExportDriver: commit the SUCCESS :-) update DW_DATE_CTRL");
               //updateDateCTRL putting todays date and adding one to the Date ranges and the Run Number
               // EVA  needs to run daily. So we add 1

               // rlee 5/23/03: Dev SPL 112 commented out the lsUpdate
               //String lsUpdate = "UPDATE DW_DATE_CTRL SET RUN_NUMBER = RUN_NUMBER + 1, FROM_DATE = TO_DATE, TO_DATE = TO_DATE + 1, RUN_DATE = SYSDATE WHERE DW_TYPE = 'EXP01' AND CLIENT_ID = '" + lsCLIENT_ID + "'";
               //int li_stat = loDateStatement.executeUpdate(lsUpdate);
               //Log.util.debug("AMSDWExportDriver: status for update of DW_DATE_CTRL, rows = %s " ,li_stat);

               loDateStatement.close();
               Log.util.debug("AMSDWExportDriver: closed loDateStatement " );
               moConnection.commit();
               Log.util.debug("AMSDWExportDriver: committed " );

                // everything ran successfully
/* Dev #189 - Comment out call to ComposeEmailMessage
                // send a Success Email

                EmailSubject = " EVA AMSDWExportDriver SUCCEEDED";
                ETLNumber = String.valueOf(liRunNumber);
                TableName = " All Tables";
                ErrMessage = "EVA AMSDWExportDriver: The Export SUCCEEDED for all tables" ;
                AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExportDriver","SUCCESS", BuysenseClientId, ETLNumber,TableName, RowCount, ErrMessage);
                // end send email
*/
            }
            else
            {
                Log.util.debug("AMSDWExportDriver: At least one export failed so no update to DW_DATE_CTRL");
/* Dev #189 - Comment out call to ComposeEmailMessage
                // send email
                EmailSubject = " EVA AMSDWExportDriver FAILED";
                ETLNumber = String.valueOf(liRunNumber);
                TableName = " Not Applicable";
                ErrMessage = " EVA AMSDWExportDriver: Atleast one Export Failed so no update to DW_DATE_CTRL" ;
                AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExportDriver","FAILED", BuysenseClientId, ETLNumber,TableName, RowCount, ErrMessage);
                // end send email
*/
                return;
            }

            Log.util.debug("AMSDWExportDriver run method: attempting to close moConnection " );
            try
            {
                moConnection.close();
                Log.util.debug("AMSDWExportDriver run method: closed moConnection  " );


            }
            catch (Exception loEx)
            {
                Log.util.debug("AMSDWExportDriver run method: failed to close moConnection  " );
/* Dev #189 - Comment out call to ComposeEmailMessage
                // send email
                EmailSubject = " EVA AMSDWExportDriver CONNECTION CLOSE ERROR after Exports succeeded";
                ETLNumber = String.valueOf(liRunNumber);
                TableName = " Not Applicable";
                ErrMessage = "EVA AMSDWExportDriver Could not close connection after Exports succeeded" ;
                AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExportDriver"," CONNECTION CLOSE ERROR", BuysenseClientId, ETLNumber,TableName, RowCount, ErrMessage);
                // end send email
*/
            }
            //moConnection.close(); //for now when we are running multple runs, leave it open

         }
         catch ( Exception loEx )
         {
            Log.util.debug ( "AMSDWExportDriver: AQL access Failed: = %s " , loEx.toString() ) ;
/* Dev #189 - Comment out call to ComposeEmailMessage
            // send email
            EmailSubject = " Cust -5 AMSDWExportDriver AQL Access Failed";
            ErrMessage = " EVA AMSDWExportDriver: AQL access Failed:  " + loEx.toString() ;
            ETLNumber = String.valueOf(liRunNumber);
            AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExportDriver"," FAILED", BuysenseClientId, ETLNumber,TableName, RowCount, ErrMessage);

            // end send email
*/
         }
         finally
         {
           Log.util.debug("AMSDWExportDriver run method: attempting to close moConnection in finally block " );
           try
           {
            if (! moConnection.isClosed())
            moConnection.close();
            Log.util.debug("AMSDWExportDriver run method: closed moConnection in finally block of run method" );
           }
           catch (Exception loEx)
           {
            Log.util.debug("AMSDWExportDriver run method: failed to close moConnection in finally block of run method" );
/* Dev #189 - Comment out call to ComposeEmailMessage
            // send email
            EmailSubject = " EVA AMSDWExportDriver CONNECTION CLOSE ERROR in finally block";
            ETLNumber = String.valueOf(liRunNumber);
            TableName = " Not Applicable";
            ErrMessage = "EVA AMSDWExportDriver Could not close connection in finally block of run method" ;
            AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExportDriver"," CONNECTION CLOSE ERROR", BuysenseClientId, ETLNumber,TableName, RowCount, ErrMessage);
            // end send email
*/
           }
         }// end try
      }
      else
      {
         Log.util.debug ( "AMSDWExportDriver: Error : Can not get to staging database, STOP " ) ;
/* Dev #189 - Comment out call to ComposeEmailMessage
         // set email variables and send
         EmailSubject = " EVA AMSDWExportDriver STOPPED";
         ErrMessage = "EVA AMSDWExportDriver: Error : Can not get to staging database, hence STOPPING" ;
         AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExportDriver","STOPPED", BuysenseClientId, ETLNumber,TableName, RowCount, ErrMessage);
         // end send email
*/

      } //end-if initConnection

   }// end run()

   private boolean initConnection( )
   {
      Log.util.debug("AMSDWExportDriver:initConnection ---------- ");
      try
      {
         if (  moConnection == null || moConnection.isClosed())
         {
            Log.util.debug("AMSDWExportDriver:initConnection is null/closed so get one ---------- ");
            try
            {
                /* DEV SPL # 11
               String lsConnkey = "DW_ConnectionInfo";

               Log.util.debug("AMSDWExportDriver:initConnection: Parameter Connection key is %s", lsConnkey);

               Map loDWConnect = AdapterDirectory.getExternalConnectionInfo(lsConnkey);
               */

/*
        String filename = "ConnectionInfo.table";
        Map parametersTable = Util.loadHashtable(Util.url(filename));

        if(parametersTable == null)
            Util.assert(false, "No configuration file %s.", filename);

        String lsPassword = null;
        try {
            lsPassword = (String)TableEdit.decode(parametersTable, "DW_ConnectionInfo.Password");
            Util.assert(lsPassword != null, "Unable to find %s in Parameters.table", "DW_ConnectionInfo.Password");
        }
        catch(ParameterException _ex) {
            Util.assert(false, "Unable to find %s in ConnectionInfo.table", "DW_ConnectionInfo.Password");
        }
*/
                // DEV SPL # 11 - use class vars to get connection info components.
               String lsDriver   = msDBDriver ;
               String lsPassword = msDBPassword ;
               String lsUrl      = msDBURL ;
               String lsUser     = msDBUser ;
               //String lsdBType   = (String)loDWConnect.get("DBType");

               Log.util.debug("Driver = %s, URL = %s, User = %s password = %s", lsDriver, lsUrl, lsUser, lsPassword);

               //Log.util.debug("AMSExport getConnection to staging db@:1521");
               moConnection = (Connection) DriverManager.getConnection(lsUrl,lsUser,lsPassword);

               Log.util.debug("AMSDWExportDriver:initConnection: connected. now setAutoCommit to false.");

               moConnection.setAutoCommit (false) ;  // by default

            }
            catch (SQLException loExp)
            {
               Log.util.debug("AMSDWExportDriver:initConnection:SQL Exception in connection = %s " , loExp.toString());
/* Dev #189 - Comment out call to ComposeEmailMessage
                // send email
                EmailSubject = " EVA AMSDWExportDriver SQL Exception in connection ";
                ErrMessage = "EVA AMSDWExportDriver SQL Exception in connection "  + loExp.toString();
                AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExportDriver"," CONNECTION OPEN ERROR", BuysenseClientId, ETLNumber,TableName, RowCount, ErrMessage);
                // end send email
*/

               return false;
            }
            return true;
         }
         else
         {
           return true; //you have the connection
         } // end-if not null
      }
      catch ( Exception loEx )
      {
         Log.util.debug ( "AMSDWExportDriver:initConnection: exception = %s " , loEx.toString() ) ;
/* Dev #189 - Comment out call to ComposeEmailMessage
         // send email
         EmailSubject = " EVA AMSDWExportDriver CONNECTION OPEN ERROR ";
         ErrMessage = "EVA AMSDWExportDriver:initConnection: exception = " + loEx.toString() ;
         AMSDWExportDriver.ComposeEmailMessage(EmailSubject, "AMSDWExportDriver"," CONNECTION OPEN ERROR", BuysenseClientId, ETLNumber,TableName, RowCount, ErrMessage);
         // end send email
*/
         return false;
      } // end try1
      //return true;
   } // end initConnection

  public static void ComposeEmailMessage( String fsSubject, String fsProgName, String fsStatus, String fsClient, String fsETLNum, String fsTableName, String fsRowCount,String fsErrMessage)
   {




    String lsSendEmailTo = "srajak@ams.com";

    StringBuffer lsbMessage = new StringBuffer();

    java.util.Date ErrorTimeStamp = null;
    // Set time when this message is being sent
    Calendar loCalendar = Calendar.getInstance();
    ErrorTimeStamp = loCalendar.getTime();
    // End set available email variables

    lsbMessage.append("\n\n******************Data warehouse extract statistical report******** \n\n\n");

    lsbMessage.append("**************************" + fsProgName + " " + fsStatus + " " +
                               "****************************\n\n");
    lsbMessage.append('\n');
    lsbMessage.append("Start Time of Export Driver:\t" + DriverReqTimeStamp + "\n");
    lsbMessage.append("This message was composed at:\t" + ErrorTimeStamp + "\n");
    lsbMessage.append("Buysense Client ID:\t" + fsClient + "\n");
    lsbMessage.append("RunNumber:\t" + fsETLNum + "\n");
    lsbMessage.append("Table:\t" + fsTableName + "\n");
    lsbMessage.append("Rows:\t" + fsRowCount + "\n");
    lsbMessage.append("Error Message:\t" + fsErrMessage + "\n");

    try
    {
        Log.util.debug("Attempting to send email with subject: %s ", fsSubject );
        EmailManager.sendEmail(lsSendEmailTo, fsSubject, lsbMessage.toString());
        Log.util.debug("Email sent" );
    }
    catch (Exception e)
    {
        Log.util.debug("Failed to send Email" , e.toString());
    }

   }
} // end AMSDWExportDriver
