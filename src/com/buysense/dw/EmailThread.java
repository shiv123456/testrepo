/**
 * This class is responsible for retrieving the messages from the e-mail message queue
 * and sending them to the given SMTP server.
 *
 * @author AMS Inc.. All rights reserved.
 * @version 1.1.1
 * @since Ver 1.0.0
 */

package com.buysense.dw;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.Properties;
import java.util.Date;
//import com.buysense.utils.BuysenseUtils;


public class EmailThread extends Thread
{
    
    private final long waitingtime=300000;
    private Session session = null;
    private Properties props = null;
    
    /*
     * Default constructor. Initializes SMTP server particulars from system initialization parameters.
     */
    public EmailThread()
    {
        
        // Define the mail server
        props = new Properties();
        props.put("mail.smtp.host", "ams-sun-ftp-1-hme1");
        
    }
    
    /*
     * Run method inherited from "Runnable".
     * Retrieves a message from the queue created by the EmailManager, and sends it out.
     */
    public void run()
    {
        BuysenseMessage message = null;
        
        while (true)
        {
            while(!EmailManager.isMailQueueEmpty())
            {
                // Retrieve next message and send it out
                message = EmailManager.getMessage();
                sendMessage(message);
            }
            try
            {
                Thread.sleep(waitingtime);
            }catch (InterruptedException e)
            {
            }
        }
    }
    
    /*
     * Prepare message and send it out via email server
     */
    private void sendMessage(BuysenseMessage message)
    {
        
        MimeMessage msg = null;
        
        try
        {
            
            // Establish a session
            Session session = (Session.getDefaultInstance(props,null));
            session.setDebug(true);
            
            // Create Mime type message and populate required fields
            
            msg = new MimeMessage(session);
            
            // Set sender, and recipients if applicable.
            msg.setFrom(message.getFrom());
            
            msg.setRecipients(Message.RecipientType.TO, message.getRecipientsTO());
            InternetAddress[] recipients = null;
            recipients = message.getRecipientsCC();
            if (recipients!=null)
            {
                msg.setRecipients(Message.RecipientType.CC, recipients);
            }
            recipients = message.getRecipientsBCC();
            if (recipients!=null)
            {
                msg.setRecipients(Message.RecipientType.BCC, recipients);
            }
            
            // Set up message subject and date
            msg.setSubject(message.getSubject());
            msg.setSentDate(new Date());
            
            // Set up message body and attachments if applicable
            // Plain text
            Object attachment = message.getAttachment();
            String msgText = message.getMessage();
            if (attachment == null)
            {
                msg.setText(msgText);
                
                // Create a multipart body with text and attachment included
            } else
            {
                BodyPart body = new MimeBodyPart();
                body.setText(msgText);
                MimeMultipart multipart = new MimeMultipart();
                multipart.addBodyPart(body);
                multipart.addBodyPart((BodyPart)attachment);
                msg.setContent(multipart);
            }
            
            // Send message
            Transport.send(msg);
            
        } catch (Exception e)
        {
            BuysenseUtils.logError("com.buysense.mail.EmailThread.sendMessage()",e);
            msg = null;
        }
    }
    
}

