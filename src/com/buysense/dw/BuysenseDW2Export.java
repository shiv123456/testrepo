package com.buysense.dw;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import config.java.ams.custom.BuysenseDW2AddExtractCriteria;
import config.java.ams.custom.BuysenseDW2ControlObj;
import config.java.ams.custom.BuysenseDW2ExtractEngine;
import ariba.base.core.Base;
import ariba.base.core.BaseId;
import ariba.base.core.BaseObject;
import ariba.base.core.Partition;
import ariba.base.core.aql.AQLCondition;
import ariba.base.core.aql.AQLOptions;
import ariba.base.core.aql.AQLQuery;
import ariba.base.core.aql.AQLResultCollection;
import ariba.util.core.MapUtil;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;
import ariba.util.scheduler.ScheduledTask;
import ariba.util.scheduler.ScheduledTaskException;
import ariba.util.scheduler.Scheduler;

public class BuysenseDW2Export extends ScheduledTask
{
    private final String              ClassName                  = "BuysenseDW2Export";
    private String                    msDBType                   = null;
    private String                    msDBDriver                 = null;
    private String                    msDBPassword               = null;
    private String                    msDBURL                    = null;
    private String                    msDBUser                   = null;
    private String                    msPartition                = null;
    public Partition                  moPartition                = null;
    public Connection                 moConnection;
    private String                    lsCLIENT_ID                = "";

    private static int                OUTPUTJSONSIZE             = 3000;
    private static String             DW_TYPE_ARIBA              = "ARIBA";
    private static String             MONITORING_MESSAGE         = "Failed to run BuysenseDW2Export task. Error in processing the documents";
    private static String             ETL_LOGS_MAIN_PROC_START   = "RUNNING";
    private static String             ETL_LOGS_MAIN_PROC_SUCCESS = "READY FOR DW";
    private static String             ETL_LOGS_MAIN_PROC_ERROR   = "EXTRACT FAILED";
    private static String             ETL_LOGS_ALL_DOC_NAME      = "EXTRACT STATUS";
    private static String             ETL_LOGS_DOC_PROC_START    = "RUNNING";
    private static String             ETL_LOGS_DOC_PROC_SUCCESS  = "EXTRACT SUCCESSFUL";
    private static String             ETL_LOGS_DOC_PROC_ERROR    = "EXTRACT FAILED";
    private static java.sql.Timestamp extractStartTime           = null;

    private static int                DB_TYPE_STRING             = 0;
    private static int                DB_TYPE_INT                = 1;
    private static int                DB_TYPE_DATE               = 2;

    SimpleDateFormat                  sdf_ET                     = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
    SimpleDateFormat                  sdf_ET2                    = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static int                 iMaxINcount                = 900; // defaulting to 1000 if it is not set in ScheduledTasks.table

    @SuppressWarnings("rawtypes")
    public void init(Scheduler scheduler, String scheduledTaskName, Map arguments)
    {
        Log.customer.warning(7000, ClassName + ":: init()");

        for (Iterator loItr = arguments.keySet().iterator(); loItr.hasNext();)
        {
            String lsKey = (String) loItr.next();

            if (lsKey.equals("DBType"))
            {
                msDBType = (String) arguments.get(lsKey);
                Log.customer.warning(7000, ClassName + " Connection parameter specified as " + msDBType + ".");
            }

            if (lsKey.equals("DBDriver"))
            {
                msDBDriver = (String) arguments.get(lsKey);
                Log.customer.warning(7000, ClassName + " Connection parameter specified as " + msDBDriver + ".");
            }

            if (lsKey.equals("DBPassword"))
            {
                msDBPassword = (String) arguments.get(lsKey);
                Log.customer.warning(7000, ClassName + " Connection parameter specified as " + msDBPassword + ".");
            }

            if (lsKey.equals("DBURL"))
            {
                msDBURL = (String) arguments.get(lsKey);
                Log.customer.warning(7000, ClassName + " Connection parameter specified as " + msDBURL + ".");
            }

            if (lsKey.equals("DBUser"))
            {
                msDBUser = (String) arguments.get(lsKey);
                Log.customer.warning(7000, ClassName + " Connection parameter specified as " + msDBUser + ".");
            }
            if (lsKey.equals("Partition"))
            {
                msPartition = (String) arguments.get(lsKey);
                Log.customer.warning(7000, ClassName + " Connection parameter specified as " + msPartition + ".");
                moPartition = Base.getService().getPartition(msPartition);
            }
            if (lsKey.equals("MaxINcount"))
            {
                iMaxINcount = Integer.parseInt(((String) arguments.get(lsKey)).trim());
                Log.customer.warning(7000, ClassName + " MaxINcount parameter specified as " + iMaxINcount + ".");
            }
        }

    }

    public void run() throws ScheduledTaskException
    {
        Log.customer.debug(ClassName + "::run() start");

        if (initConnection())
        {
            int liDWETLNum = 0;

            String lsRunType = null;
            String lsTaskRunning = null;
            String lsStopSchedTask = null;

            java.sql.Timestamp loDBFromDateTime = null;
            java.sql.Timestamp loDBToDateTime = null;
            java.util.Calendar loToDate = Calendar.getInstance();
            java.util.Calendar loFromDate = Calendar.getInstance();

            PreparedStatement preStatement = null;
            PreparedStatement preStatement1 = null;
            ResultSet rs = null;
            boolean oneDocSuccess = true;
            boolean allDocSuccess = true;
            boolean oneDocFailed = false;
            ResultSet docRS = null;
            boolean bIsAdhocDateExtraction = false;
            boolean bIsScheduledExtracction = false;
            Map<String, BuysenseDW2ControlObj> loextractCrlObjMap = MapUtil.map();
            ResultSet loCtrlRecRS = null;
            boolean bSkipExecution = false;
            try
            {
                AQLResultCollection loBuysenseClientResultSet;
                AQLQuery loWhereQuery = AQLQuery.parseQuery("select distinct ClientID from ariba.core.BuysenseClient order by ClientID");
                AQLOptions loOptions = new AQLOptions();
                loOptions.setUserLocale(Base.getSession().getLocale());
                loOptions.setUserPartition(moPartition);

                loBuysenseClientResultSet = (AQLResultCollection) Base.getService().executeQuery(loWhereQuery, loOptions);

                if (loBuysenseClientResultSet.getFirstError() != null)
                {
                    Log.customer.warning(7000, ClassName + "::run() AQL execution error: " + loBuysenseClientResultSet.getFirstError().toString());
                }

                if (loBuysenseClientResultSet.next() == false)
                {
                    Log.customer.warning(7000, ClassName + "::run() WARNING: No Records in the BuysenseClient, returning");
                    bSkipExecution = true;
                    return;
                }

                lsCLIENT_ID = loBuysenseClientResultSet.getString(0).trim();
                Log.customer.debug(ClassName + "::run() client id from the buysense org is = " + lsCLIENT_ID);
                // note extractStartTime
                extractStartTime = new java.sql.Timestamp(Calendar.getInstance().getTimeInMillis());

                // update if task running is 'n' and stop sched task is 'n'
                String lsSQL = "UPDATE DW2_DATE_CTRL SET TASK_RUNNING = ? WHERE TASK_RUNNING=? AND STOP_SCHED_TASK=? AND DW_TYPE = ? AND CLIENT_ID = ?";
                preStatement = moConnection.prepareStatement(lsSQL);
                preStatement.setString(1, "Y");
                preStatement.setString(2, "N");
                preStatement.setString(3, "N");
                preStatement.setString(4, DW_TYPE_ARIBA);
                preStatement.setString(5, lsCLIENT_ID);

                int iSqlRetVal = preStatement.executeUpdate();

                if (iSqlRetVal == 0) // no row was updated. so return without any changes - one of the where conditions failed
                {
                    Log.customer.warning(7000, ClassName + "::DW2_DATE_CTRL table not being updated as conditions are not met");
                    bSkipExecution = true;
                    return;
                }

                // CSPL-8305
                // Append data from ADHOC_EXTRACT_LIST_ETL to ADHOC_EXTRACT_LIST in DW2_ADHOC_EXTR_DOCLIST table and then set ADHOC_EXTRACT_LIST_ETL = NULL
                lsSQL = "UPDATE DW2_ADHOC_EXTR_DOCLIST SET ADHOC_EXTRACT_LIST = NVL2 (ADHOC_EXTRACT_LIST, ADHOC_EXTRACT_LIST||','||ADHOC_EXTRACT_LIST_ETL, ADHOC_EXTRACT_LIST_ETL), " +
                		"ADHOC_EXTRACT_LIST_ETL = NULL WHERE SOURCE='"+DW_TYPE_ARIBA+"' AND ADHOC_EXTRACT_LIST_ETL IS NOT NULL";
                Log.customer.debug(ClassName + "::run() lsSQL: " + lsSQL);
                preStatement = moConnection.prepareStatement(lsSQL);
                iSqlRetVal = preStatement.executeUpdate();
                
                // select other values from the table DW2_DATE_CTRL
                lsSQL = "SELECT * FROM DW2_DATE_CTRL WHERE DW_TYPE = ? AND CLIENT_ID = ?";
                Log.customer.debug(ClassName + "::run() lsSQL: " + lsSQL);
                preStatement = moConnection.prepareStatement(lsSQL);
                preStatement.setString(1, DW_TYPE_ARIBA);
                preStatement.setString(2, lsCLIENT_ID);
                rs = preStatement.executeQuery();

                if (rs != null && rs.next())
                {

                    liDWETLNum = rs.getInt("DW_ETLNUM"); // later bump it by one and add todays date
                    lsRunType = rs.getString("RUN_TYPE"); // S:scheduled OR A:ad-hoc
                    lsTaskRunning = rs.getString("TASK_RUNNING"); // used only for logging after this point
                    lsStopSchedTask = rs.getString("STOP_SCHED_TASK"); // used only for logging after this point

                    if (lsRunType != null && lsRunType.trim().equalsIgnoreCase("AD")) // ad-hoc task
                    {
                        loDBFromDateTime = rs.getTimestamp("ADHOC_FROM_DATE");
                        loFromDate.setTimeInMillis(loDBFromDateTime.getTime());
                        loDBToDateTime = rs.getTimestamp("ADHOC_TO_DATE");
                        loToDate.setTimeInMillis(loDBToDateTime.getTime());
                        bIsAdhocDateExtraction =true;
                    }
                    else if (lsRunType != null && lsRunType.trim().equalsIgnoreCase("S")) // pick regular scheduled run times
                    {
                        loDBFromDateTime = rs.getTimestamp("FROM_DATE");
                        loDBToDateTime = rs.getTimestamp("TO_DATE");
                        bIsScheduledExtracction =true;
                    }
                    else
                    {
                        Log.customer.error(7000, ClassName + "::run() invalid run type");
                        Log.customer.error(7000, ClassName + "::run " + MONITORING_MESSAGE);
                        bSkipExecution = true;
                        return;
                    }
                    Log.customer.debug(ClassName + "::run() results: " + liDWETLNum + "," + loDBFromDateTime + "," + loDBToDateTime + "," + lsRunType + "," + lsTaskRunning + "," + lsStopSchedTask);
                }
                else
                {
                    String msExtractMessage = ClassName + "::run() WARNING!!! data missing on DW2_DATE_CTRL for DW_TYPE = 'ARIBA' AND CLIENT_ID = " + lsCLIENT_ID;
                    Log.customer.error(7000, msExtractMessage);
                    Log.customer.error(7000, ClassName + "::run " + MONITORING_MESSAGE);
                    bSkipExecution = true;
                    return;
                }

                // JB - If last run was unsuccessful then we won't increment the ETL#
                // Got to do some cleanup before running the current cycle as there are entries for same old ETL # that failed
                // Move logs to DW2_ETL_LOGS_ERRORS for ETL# and Clear logs from DW2_ETL_LOGS table for current ETL # records if they exist
                // remove all entries in DW2_EXTRACTED_DOCS for current ETL#
                WriteToHistAndClearETLLogs(liDWETLNum); // writes to DW2_ETL_LOGS_HIST and removes entries from DW2_ETL_LOGS for # liDWETLNum
                RemoveExtractedDOCS(liDWETLNum); // remove entries from DW2_EXTRACTED_DOCS for # liDWETLNum

                if (lsRunType != null && lsRunType.trim().equalsIgnoreCase("S"))
                { // set TO_DATE = current date (rounded off to last 5 min)
                    loToDate = Calendar.getInstance();
                    loToDate.set(Calendar.SECOND, 0);
                    loToDate.set(Calendar.MILLISECOND, 0);
                    int modulo = loToDate.get(Calendar.MINUTE) % 5;
                    if (modulo > 0)
                    {
                        loToDate.add(Calendar.MINUTE, -modulo);
                    }
                    loFromDate.setTimeInMillis(loDBFromDateTime.getTime());
                    Log.customer.debug(ClassName + "::from date:" + loFromDate.toString() + " ::to date:" + loToDate.toString());
                }

                // start fetching the documents from DW2_DOC_CTRL table
                String lsSelect ="";
                if(bIsScheduledExtracction)
                {
                       lsSelect = "Select EXTRACT_DOC_NAME,APP_SOURCE_OBJ,EXTRACT_CRITERIA,IDENTIFY_CHANGE_CRITERIA,INPUT_OBJ_JSON,INPUT_FIELD_JSON,CONTROL_RECORD,EXTRACT_DOC_TYPE from DW2_DOC_CTRL " +
                       "where SOURCE = 'ARIBA' AND EXTRACT_FLAG=?  ORDER BY EXTRACT_SEQ";
                }else if(bIsAdhocDateExtraction)
                {
                    lsSelect = "Select EXTRACT_DOC_NAME,APP_SOURCE_OBJ,EXTRACT_CRITERIA,IDENTIFY_CHANGE_CRITERIA,INPUT_OBJ_JSON,INPUT_FIELD_JSON,CONTROL_RECORD,EXTRACT_DOC_TYPE from DW2_DOC_CTRL " +
                    "where SOURCE = 'ARIBA' AND ADHOC_EXTRACT_FLAG=?  ORDER BY EXTRACT_SEQ";
                }
                
                preStatement = moConnection.prepareStatement(lsSelect);
                preStatement.setInt(1, 1);
                docRS = preStatement.executeQuery();

                String sCtrlRecordsQuery = "SELECT DOCCTRL.EXTRACT_DOC_TYPE,DOCCTRL.EXTRACT_DOC_NAME,DOCCTRL.APP_SOURCE_OBJ,DOCCTRL.EXTRACT_CRITERIA,DOCCTRL.IDENTIFY_CHANGE_CRITERIA," +
                       "DOCCTRL.INPUT_OBJ_JSON,DOCCTRL.INPUT_FIELD_JSON, DOCCTRL.CONTROL_RECORD, ADDOCS.ADHOC_EXTRACT_LIST,ADDOCS.IDENTIFIER_FIELD " +
                       "FROM DW2_DOC_CTRL DOCCTRL, DW2_ADHOC_EXTR_DOCLIST ADDOCS " +
                       "WHERE DOCCTRL.SOURCE = 'ARIBA' AND DOCCTRL.CONTROL_RECORD = 1 AND DOCCTRL.EXTRACT_DOC_TYPE=ADDOCS.EXTRACT_DOC_TYPE AND DOCCTRL.SOURCE = ADDOCS.SOURCE " +
                       "ORDER BY DOCCTRL.EXTRACT_DOC_TYPE, DOCCTRL.CONTROL_RECORD, DOCCTRL.EXTRACT_SEQ ";
                Log.customer.debug("The sCtrlRecordsQuery is " +sCtrlRecordsQuery);
                preStatement1 = moConnection.prepareStatement(sCtrlRecordsQuery);

                loCtrlRecRS = preStatement1.executeQuery(sCtrlRecordsQuery);
                while (loCtrlRecRS.next())
                {
                    String sCtrlRecDocType = loCtrlRecRS.getString("EXTRACT_DOC_TYPE");
                    String sCtrlRecAppSource = loCtrlRecRS.getString("APP_SOURCE_OBJ");
                    String sCtrlRecDocName = loCtrlRecRS.getString("EXTRACT_DOC_NAME");
                    String sCtrlRecExtractCriteria = loCtrlRecRS.getString("EXTRACT_CRITERIA");
                    String sCtrlRecIdentifierField = loCtrlRecRS.getString("IDENTIFIER_FIELD");
                    String sCtrlRecAdhocIdList = loCtrlRecRS.getString("ADHOC_EXTRACT_LIST");
                    Log.customer.debug("The vale of EXTRACT_DOC_TYPE= " +sCtrlRecDocType+", EXTRACT_DOC_NAME="+sCtrlRecDocName+", IDENTIFIER_FIELD= "+sCtrlRecIdentifierField+", ADHOC_EXTRACT_LIST="+sCtrlRecAdhocIdList);
                    String lsExtractDocQuery = "";
                    ArrayList<BaseId> loAdhocBaseIDs = new ArrayList<BaseId>();
                    if(StringUtil.nullOrEmptyOrBlankString(sCtrlRecDocType))
                    {
                        Log.customer.debug(ClassName + "::loCtrlRecRS NOT processed as DocType is null or empty");
                        WriteErrorLogs(lsCLIENT_ID, liDWETLNum, sCtrlRecDocName, "DOC_TYPE NOT FOUND FOR " + sCtrlRecDocName);
                        continue;
                    }
                    else if (!loextractCrlObjMap.containsKey(sCtrlRecDocType))
                    {
                        BuysenseDW2ControlObj extractCrlObj = new BuysenseDW2ControlObj();
                        if(sCtrlRecIdentifierField.equalsIgnoreCase("BaseID"))
                        {
                            if (!StringUtil.nullOrEmptyOrBlankString(sCtrlRecAdhocIdList))
                            {
                                StringUtil.splitWithQuotes(sCtrlRecAdhocIdList, true);
                                String[] lsBaseids = sCtrlRecAdhocIdList.split(",");
                                for (int i = 0; i < lsBaseids.length; i++)
                                {
                                    String sUnQuoteBaseId = lsBaseids[i].substring(1, lsBaseids[i].length() - 1);
                                    /*for malformated baseid like 663d'4.a
                                    BaseId.parse() fails with exception - Assert failed: The passed in string '663d'4.' is not of the expected format

                                    for non-existent basid like 663d.a, 
                                    BaseId.parse() returns BasId object [[BaseId 287833 663d.a ]]*/
                                    Log.customer.debug("::run() parsing BaseId " + sUnQuoteBaseId);
                                    if ((BaseId.parse(sUnQuoteBaseId) != null))
                                    {
                                        loAdhocBaseIDs.add((BaseId.parse(sUnQuoteBaseId)));
                                    }
                                    else
                                    {
                                        //Just log for invalid BaseId's
                                        Log.customer.warning(7000, "::run() Base id " + sUnQuoteBaseId + " in ADHOC_EXTRACT_LIST for "
                                                + sCtrlRecDocType + " seems invalid");
                                    }
                                }
                            }

                        }
                        else
                        {
                            if (!StringUtil.nullOrEmptyOrBlankString(sCtrlRecAdhocIdList))
                            {
                                //directly pass IDENTIFIER_FIELD and quoted ADHOC_EXTRACT_LIST to the AQL
                                String sAdHocExtractDocQuery = "SELECT DISTINCT(A) FROM " + sCtrlRecAppSource + "  A INCLUDE INACTIVE " + 
                                       "WHERE " + sCtrlRecIdentifierField + " IN (" + sCtrlRecAdhocIdList + ")";
                                Partition partition = Base.getSession().getPartition();
                                AQLOptions aqlOptions = new AQLOptions(partition);
                                AQLQuery aqlQuery = AQLQuery.parseQuery(sAdHocExtractDocQuery);
                                Log.customer.debug(ClassName + "::  DW2_ADHOC_EXT_DOCLIST extraction aqlQuery: " + aqlQuery);
                                // Should not catch exceptions as we need to fail the task in case of invalid data in IDENTIFIER_FIELD or ADHOC_EXTRACT_LIST
                                AQLResultCollection aqlResults = Base.getService().executeQuery(aqlQuery, aqlOptions);
                                while (aqlResults.next())
                                {
                                    loAdhocBaseIDs.add(aqlResults.getBaseId(0));
                                }
                            }
                        }
                        if(!StringUtil.nullOrEmptyOrBlankString(sCtrlRecExtractCriteria))
                        {
                            sCtrlRecExtractCriteria = sCtrlRecExtractCriteria.replace(":FROM_DATE", "Date('" + sdf_ET.format(loFromDate.getTime()) + "')"); // time formatted to ET (EST/EDT)
                            sCtrlRecExtractCriteria = sCtrlRecExtractCriteria.replace(":TO_DATE", "Date('" + sdf_ET.format(loToDate.getTime()) + "')");
                            sCtrlRecExtractCriteria = "(" + sCtrlRecExtractCriteria + ")";

                            lsExtractDocQuery = "Select distinct(doc) from " + sCtrlRecAppSource + "  doc include inactive where " + sCtrlRecExtractCriteria;

                            Partition partition = Base.getSession().getPartition();
                            AQLOptions aqlOptions = new AQLOptions(partition);
                            AQLQuery aqlQuery = AQLQuery.parseQuery(lsExtractDocQuery);
                            AQLResultCollection aqlResults = Base.getService().executeQuery(aqlQuery, aqlOptions);
                            ArrayList<BaseId> loBaseIDs = new ArrayList<BaseId>();
                            while (aqlResults.next())
                            {
                                loBaseIDs.add(aqlResults.getBaseId(0));
                            }
                            if(loAdhocBaseIDs != null && !loAdhocBaseIDs.isEmpty() )
                            {
                                loBaseIDs.addAll(loAdhocBaseIDs);
                            }
                            extractCrlObj.setBaseIDsList(loBaseIDs);
                            extractCrlObj.setEXTRACT_CRITERIA_Null_Flag(false);
                            loextractCrlObjMap.put(sCtrlRecDocType, extractCrlObj);
                        }// if extract criteria is null, even then add the list to the map. This means there is a control record that has no extract criteria
                        else
                        {
                            extractCrlObj.setBaseIDsList(null);
                            extractCrlObj.setEXTRACT_CRITERIA_Null_Flag(true);
                            loextractCrlObjMap.put(sCtrlRecDocType, extractCrlObj);
                        }

                    }

                }

                StringBuffer strInputObjJson = new StringBuffer();
                StringBuffer strInputFieldJson = new StringBuffer();
                String aux;
                BufferedReader br;

                InsertIntoDW2ETLLogs(lsCLIENT_ID, liDWETLNum, ETL_LOGS_ALL_DOC_NAME, ETL_LOGS_MAIN_PROC_START);

                String sDocType = null;
                String sDocName = null;
                String appSource = null;

                String extractAddCriteriaMethod = null;
                String sAddExtractCriteria = null;
                int countDocs = 0;
                int countAllDocs = 0;
                int iOutputJsonSize = 0;
                // select other values from the table DW2_DATE_CTRL
                lsSQL = "SELECT VALUE FROM D_PARAMETERS WHERE PARAMETER = ?";
                preStatement = moConnection.prepareStatement(lsSQL);
                preStatement.setString(1, "DW2_EXTRACTED_CLOB_LENGTH"); // ok to hard code as it's being used only here -JB
                rs = preStatement.executeQuery();

                if (rs != null && rs.next())
                {

                    String strVal = rs.getString("VALUE");
                    iOutputJsonSize = (new Integer(strVal)).intValue();
                    Log.customer.debug(ClassName + "::run() got value from D_PARAMETERS for iOutputJsonSize: " + strVal);
                }
                else
                {
                    iOutputJsonSize = OUTPUTJSONSIZE; // put the default value if not found
                    Log.customer.warning(7000, ClassName + "::setting  iOutputJsonSize to default value in ariba as param in D_PARAMETERS not found");
                }

                while (docRS.next())
                {
                    // process one document at a time. If there is an error, log message and fetch the next document
                    try
                    {
                        sDocType = docRS.getString("EXTRACT_DOC_TYPE");
                        sDocName = docRS.getString("EXTRACT_DOC_NAME");
                        appSource = docRS.getString("APP_SOURCE_OBJ");
                        extractAddCriteriaMethod = docRS.getString("IDENTIFY_CHANGE_CRITERIA");
                        sAddExtractCriteria = null;
                        // form the next query to fetch the documents - AQL query

                        countAllDocs = countAllDocs + countDocs;

                        strInputObjJson.setLength(0);
                        strInputFieldJson.setLength(0);

                        br = new BufferedReader(docRS.getClob("INPUT_OBJ_JSON").getCharacterStream());
                        while ((aux = br.readLine()) != null)
                        {
                            strInputObjJson.append(aux);
                            strInputObjJson.append(System.getProperty("line.separator"));
                        }

                        br = new BufferedReader(docRS.getClob("INPUT_FIELD_JSON").getCharacterStream());
                        while ((aux = br.readLine()) != null)
                        {
                            strInputFieldJson.append(aux);
                            strInputFieldJson.append(System.getProperty("line.separator"));
                        }

                        /*if (extractCriteria != null)
                        {
                            Log.customer.debug(ClassName + "::run() results2: " + appSource + "," + extractCriteria + "," + strInputObjJson + "," + strInputFieldJson);
                            extractCriteria = extractCriteria.replace(":FROM_DATE", "Date('" + sdf_ET.format(loFromDate.getTime()) + "')"); // time formatted to ET (EST/EDT)
                            extractCriteria = extractCriteria.replace(":TO_DATE", "Date('" + sdf_ET.format(loToDate.getTime()) + "')");
                            extractCriteria = "(" + extractCriteria + ")";
                        }

                        // AQL query to extract document list
                        String lsExtractDocQuery = "";
                        Log.customer.debug(ClassName + "::run() sAddExtractCriteria: " + sAddExtractCriteria);

                        if (extractCriteria != null)
                        {
                            if (!StringUtil.nullOrEmptyOrBlankString(sAddExtractCriteria))
                            {
                                extractCriteria = extractCriteria + " " + sAddExtractCriteria;
                            }
                            lsExtractDocQuery = "Select doc from " + appSource + "  doc include inactive where " + extractCriteria; // check for null extract criteria
                        }
                        else
                        {
                            if (!StringUtil.nullOrEmptyOrBlankString(sAddExtractCriteria))
                            {
                                lsExtractDocQuery = "Select doc from " + appSource + "  doc include inactive where " + sAddExtractCriteria;
                            }
                            else
                            {
                                lsExtractDocQuery = "Select doc from " + appSource + "  doc include inactive";
                            }
                        }*/
                        if (extractAddCriteriaMethod != null)
                        {
                            Class[] paramString = new Class[3];
                            paramString[0] = String.class;
                            paramString[1] = String.class;
                            paramString[2] = Class.forName("java.util.ArrayList");
                            String sFromDate = "Date('" + sdf_ET.format(loFromDate.getTime()) + "')";
                            String sToDate = "Date('" + sdf_ET.format(loToDate.getTime()) + "')";

                            // call the printIt method
                            Method method = BuysenseDW2AddExtractCriteria.class.getDeclaredMethod(extractAddCriteriaMethod, paramString);
                            BuysenseDW2ControlObj loCrlObj = loextractCrlObjMap.get(sDocType);
                            ArrayList<BaseId> loBaseIDs = (loCrlObj!=null)?loCrlObj.getBaseIdsList():null;
                            Log.customer.debug(ClassName + "::run() sDocType: " + sDocType+", loCrlObj: "+loCrlObj);
                            sAddExtractCriteria = (String) method.invoke(BuysenseDW2AddExtractCriteria.class, sFromDate, sToDate, loBaseIDs);
                            Log.customer.debug(ClassName + "::run() sAddExtractCriteria: " + sAddExtractCriteria);

                        }                       
                        String sAQLQuery = "Select doc from " + appSource + "  doc include inactive ";                        
                        if(appSource.equals("ariba.common.core.Address"))
                        {
                            sAQLQuery = sAQLQuery +" SUBCLASS NONE ";
                        }  
                        AQLQuery aqlQuery = AQLQuery.parseQuery(sAQLQuery);

                        if(StringUtil.nullOrEmptyOrBlankString(sDocType))
                        {
                            WriteErrorLogs(lsCLIENT_ID, liDWETLNum, sDocName, "DOC_TYPE NOT FOUND FOR " + sDocName);
                            continue;
                        }else if(!loextractCrlObjMap.containsKey(sDocType))
                        {
                            WriteErrorLogs(lsCLIENT_ID, liDWETLNum, sDocName, "CTRL_RECORD object NOT FOUND FOR " + sDocType);
                            continue;
                        }else
                        {
                            BuysenseDW2ControlObj loCrlObj = loextractCrlObjMap.get(sDocType);
                            ArrayList<BaseId> loUniqueKeys = loCrlObj.getBaseIdsList();
                            boolean bExtractCriteriaNull = loCrlObj.getEXTRACT_CRITERIA_Null_Flag();

                            if ((loUniqueKeys == null) || (loUniqueKeys != null && loUniqueKeys.isEmpty()))
                            {
                                if (!bExtractCriteriaNull)
                                {
                                    if (StringUtil.nullOrEmptyOrBlankString(sAddExtractCriteria))
                                    {
                                        aqlQuery.setWhere(AQLCondition.parseCondition("1=2"));
                                    }
                                    else
                                    {
                                        aqlQuery.setWhere(AQLCondition.parseCondition(sAddExtractCriteria));
                                    }
                                }
                                else
                                {
                                    if (StringUtil.nullOrEmptyOrBlankString(extractAddCriteriaMethod))
                                    {
                                        WriteErrorLogs(lsCLIENT_ID, liDWETLNum, sDocName, "both extract criteria of control record (" + sDocType + ") and IDENTIFY_CHANGE_CRIETERIA of " + sDocName + " are null");
                                        continue;
                                    }
                                    else
                                    {
                                        if (StringUtil.nullOrEmptyOrBlankString(sAddExtractCriteria))
                                        {
                                            aqlQuery.setWhere(AQLCondition.parseCondition("1=2"));
                                        }
                                        else
                                        {
                                            aqlQuery.setWhere(AQLCondition.parseCondition(sAddExtractCriteria));
                                        }

                                    }
                                }
                            }else
                            {


                                // identify change criteria will take the list of unique names and spit out all of the where clause --JB
                                // Example: for REQ_COMMENTS 'getAPPLNS_POChangeCriterea' will take UniqueName[PO] and spit out relevant req Unique names
                                // Not going to add unique names sUNsExtractCriteria in case identify change criteria is defined
                                if(extractAddCriteriaMethod!=null)
                                {
                                   if (!StringUtil.nullOrEmptyOrBlankString(sAddExtractCriteria))
                                   {
                                       //lsExtractDocQuery = "Select doc from " + appSource + "  doc include inactive where " + sAddExtractCriteria;
                                       aqlQuery.setWhere(AQLCondition.parseCondition(sAddExtractCriteria));
                                   }
                                   else
                                   {
                                       //lsExtractDocQuery = "Select doc from " + appSource + "  doc include inactive where 1=2";
                                       aqlQuery.setWhere(AQLCondition.parseCondition("1=2"));
                                   }
                                }
                                else
                                {
                                    Log.customer.debug(ClassName + "::building IN clause - list: " + loUniqueKeys.size() + "; iMaxINcount: " + iMaxINcount);
                                    for (int i = 0; i < loUniqueKeys.size(); i += iMaxINcount)
                                    {
                                        List<BaseId> tempINList;
                                        tempINList = loUniqueKeys.subList(i, Math.min(loUniqueKeys.size(), i + iMaxINcount));
                                        if (i == 0)
                                        {
                                            aqlQuery.andIn("this", tempINList);
                                        }
                                        else
                                        {
                                            aqlQuery.or(AQLCondition.buildIn(aqlQuery.buildField("this"), tempINList));
                                        }
                                    }

                                }
                            }

                        }

                        Log.customer.debug(ClassName + "::fetching from COVASTAGE DOC_CTRL aqlQuery: " + aqlQuery);

                        Partition partition = Base.getSession().getPartition();
                        AQLOptions aqlOptions = new AQLOptions(partition);
                        AQLResultCollection aqlResults = Base.getService().executeQuery(aqlQuery, aqlOptions);

                        BaseObject approvableBaseObj;
                        BuysenseDW2ExtractEngine extractEngine = new BuysenseDW2ExtractEngine();
                        String outputJsonStr = "";
                        String outJsonStrForOneObj = "";

                        if (aqlResults.getFirstError() != null)
                        {
                            Log.customer.debug("*****%s***** did not find ariba object for given criteria", ClassName);
                            // return; -- don't return as there might be other objects that need to be processed
                        }
                        else
                        {
                            ArrayList<String> lsOutJSON = new ArrayList<String>();
                            String tempOutput = "";
                            int objNum = 1;
                            boolean objBiggerThanOutJSON = false;
                            countDocs = 0;

                            InsertIntoDW2ETLLogs(lsCLIENT_ID, liDWETLNum, sDocName, ETL_LOGS_DOC_PROC_START);

                            while (aqlResults.next())
                            {
                                countDocs++;
                                approvableBaseObj = Base.getSession().objectFromId((BaseId) aqlResults.getObject(0));
                                Log.customer.debug("*****%s***** found approvable base id: %s", ClassName, approvableBaseObj);

                                outJsonStrForOneObj = extractEngine.extractObj(strInputObjJson.toString(), strInputFieldJson.toString(), approvableBaseObj);
                                Log.customer.debug("*****%s***** outJsonStrForOneObj: %s", ClassName, outJsonStrForOneObj);

                                objBiggerThanOutJSON = false;

                                // we should always allow to write an object if - outJsonStrForOneObj.length() > OUTPUTJSONSIZE
                                if ((tempOutput.length() + outJsonStrForOneObj.length()) > iOutputJsonSize)
                                {
                                    if (tempOutput.length() == 0) // this is the first object
                                    {
                                        tempOutput = "[";
                                        tempOutput = tempOutput + System.getProperty("line.separator");
                                        tempOutput = tempOutput + outJsonStrForOneObj;
                                        tempOutput = tempOutput + System.getProperty("line.separator");
                                        tempOutput = tempOutput + "]";
                                        lsOutJSON.add(tempOutput);
                                        objBiggerThanOutJSON = true;
                                        objNum = 0;
                                        tempOutput = ""; // clear temp object as it is already processed
                                    }
                                    else
                                    {
                                        // close obj and add to array list
                                        tempOutput = tempOutput + System.getProperty("line.separator");
                                        tempOutput = tempOutput + "]";
                                        lsOutJSON.add(tempOutput);
                                        tempOutput = ""; // clear temp object as it is already processed
                                        // reset the obj #
                                        objNum = 1;
                                    }
                                }

                                // object is already processed if it is bigger
                                if (!objBiggerThanOutJSON)
                                {
                                    if (objNum == 1)
                                    {
                                        tempOutput = "["; // begin JSON array
                                        tempOutput = tempOutput + System.getProperty("line.separator");
                                        tempOutput = tempOutput + outJsonStrForOneObj;
                                    }
                                    else
                                    {
                                        tempOutput = tempOutput + ","; // add comma for previous object
                                        tempOutput = tempOutput + System.getProperty("line.separator");
                                        tempOutput = tempOutput + outJsonStrForOneObj;
                                    }
                                }
                                objNum++;
                            }

                            Log.customer.debug("*****%s***** writing to output table: %s", ClassName, outputJsonStr);

                            // if last output is not bigger that size limit, close the JSON object and add to the array
                            if (!objBiggerThanOutJSON && (countDocs > 0))
                            {
                                tempOutput = tempOutput + "]";
                                lsOutJSON.add(tempOutput);
                            }
                            oneDocSuccess = writetoExtractedDocs(lsCLIENT_ID, sDocName, lsOutJSON, liDWETLNum);
                            if(!oneDocSuccess) // If any one doc fails, mark it as failed
                            {
                            	oneDocFailed = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        
                        //CSPL-8485 - Send out email notification when Ariba extract task fails
                        Partition partition = Base.getService().getPartition("pcsv");
                        String sFromEmailID = (String) Base.getService().getParameter(partition, "Application.Base.NotificationFromAddress");
                        String sFromName = (String) Base.getService().getParameter(partition, "Application.Base.NotificationFromNameKey");
                        String sSendToID = (String) Base.getService().getParameter(partition, "Application.AMSParameters.DWExportEmailTo");
                        String sENVName = (String) Base.getService().getParameter(partition, "Application.AMSParameters.EnvironmentName"); 
                        String sSubject = "FAILURE: "+sENVName+" DW Ariba Extract Task Failed";  

                        Log.customer.debug(ClassName + "::run() - sending exception email");
                        EmailManager.sendEmail(sSendToID, sFromEmailID, sFromName, sSubject, ClassName + "::run() exception in processing mesg = " + getStackMessg(ex));
                        Log.customer.debug(ClassName + "::run() - sent exception email");
                        
                        // error processing docRS. Catch exception and continue processing
                        // Log.customer.error(7000, "::run() exception in processing doc = " + sDocName);
                        Log.customer.error(7000, ClassName+"::run() exception in processing mesg = " + ex.getMessage());
                        Log.customer.error(7000, ClassName+"::run() exception in processing cause = " + ex.getCause());
                        Log.customer.error(7000, ClassName+"::run() exception = " + getStackMessg(ex));
                        Log.customer.debug(ClassName + "::run() exception in processing mesg = " + ex.getMessage());
                        Log.customer.debug(ClassName + "::run() exception in processing cause = " + ex.getCause());
                        Log.customer.debug(ClassName + "::run() exception = " + getStackMessg(ex));
                        oneDocSuccess = false;
                        allDocSuccess = false; // something went wrong in processing one of the documents
                        oneDocFailed = true;
                        // write to LOG DB that processing was not successful for this docRS
                        // but continue to process - JB
                    }

                    if (oneDocSuccess)
                    {
                        // write to LOG DB that processing was successful for this docRS
                        UpdateFinishMsgDW2ETLLogs(lsCLIENT_ID, liDWETLNum, sDocName, ETL_LOGS_DOC_PROC_SUCCESS, countDocs, "Y");
                    }
                    else
                    {
                        UpdateFinishMsgDW2ETLLogs(lsCLIENT_ID, liDWETLNum, sDocName, ETL_LOGS_DOC_PROC_ERROR, countDocs, "N");
                    }

                    // set value to true for next doc
                    oneDocSuccess = true;

                    // exit loop if allDocSuccess is false. We won't process other docs in case of error
                    if (!allDocSuccess)
                    {
                        break;
                    }
                } // end-of-while for each doc processing

                if (allDocSuccess && !oneDocFailed)// both conditions need to be true for success
                {
                    // moConnection.commit(); // JB_notes : why are we committing here?
                    // final message the all docs were extracted successfully in LOG DB
                    UpdateFinishMsgDW2ETLLogs(lsCLIENT_ID, liDWETLNum, ETL_LOGS_ALL_DOC_NAME, ETL_LOGS_MAIN_PROC_SUCCESS, countAllDocs, "Y");
                    
                    // After successfully extracting all docs, write to DW2_ADHOC_EXTR_DOCLIST_HIST and then reset DW2_ADHOC_EXTR_DOCLIST
                    writeToADHOCETLLogs(liDWETLNum);
                }
                else
                {
                    // what to do if extraction for one or more document has failed
                    // don't increment DW_ETLNUM ??
                    UpdateFinishMsgDW2ETLLogs(lsCLIENT_ID, liDWETLNum, ETL_LOGS_ALL_DOC_NAME, ETL_LOGS_MAIN_PROC_ERROR, countAllDocs, "N");
                }
                loextractCrlObjMap.clear();
                Log.customer.debug(ClassName + "::run()-end of try");
            }
            catch (Exception loEx)
            {
                Log.customer.error(7000, ClassName + "::run() in try/catch " + MONITORING_MESSAGE);
                Log.customer.error(7000, "::run() exception2 = " + loEx);
                Log.customer.error(7000, "::run() exception2 in processing mesg = " + loEx.getMessage());
                Log.customer.error(7000, "::run() exception2 in processing cause = " + loEx.getCause());
                Log.customer.error(7000, "::run() exception2 = " + getStackMessg(loEx));
                oneDocSuccess = false;
                allDocSuccess = false; // something went wrong in processing one or more documents

            }
            finally
            {

                try
                {
                    // update TO_DATE before writing to DATECTRLHIST table. So all correct info is there for current run before updating the record for next run
                    // update TO_DATE whether successful or not
                    try
                    {
                        if (lsRunType != null && lsRunType.trim().equalsIgnoreCase("S"))
                        {
                            Log.customer.debug(ClassName + "::updating TO_DATE for current run:" + loToDate.toString());
                            if(!bSkipExecution)
                            {
                            setOrUpdateDW2DateCtrl("TO_DATE", new Long(loToDate.getTimeInMillis()).toString(), DB_TYPE_DATE); // redundant values for string and int
                            }
                        }
                    }
                    catch (Exception loEx)
                    {
                        Log.customer.error(7000, ClassName + "::run() failed to update from date for next run:\n" + getStackMessg(loEx));
                        Log.customer.error(7000, "::run() exception3 in processing mesg = " + loEx.getMessage());
                        Log.customer.error(7000, "::run() exception3 in processing cause = " + loEx.getCause());
                        Log.customer.error(7000, "::run() exception3 = " + getStackMessg(loEx));

                    }
                    // At this point all updates are made except RUN_TYPE and TASK_RUNNING
                    // write the values to DW2_DATE_CTRL_HIST from DW2_DATE_CTRL --JB
                    try
                    {
                        WriteToDATECTRLHIST();
                    }
                    catch (Exception loEx)
                    {
                        Log.customer.error(7000, ClassName + "::run() failed to write to WriteToDATECTRLHIST\n" + getStackMessg(loEx));
                        Log.customer.error(7000, "::run() exception4 in processing mesg = " + loEx.getMessage());
                        Log.customer.error(7000, "::run() exception4 in processing cause = " + loEx.getCause());
                        Log.customer.error(7000, "::run() exception4 = " + getStackMessg(loEx));

                    }

                    try
                    {
                        if (allDocSuccess && !oneDocFailed)
                        {
                            // set FROM_DATE = current date if RUN_TYPE = S
                            if (lsRunType != null && lsRunType.trim().equalsIgnoreCase("S"))
                            {
                                Log.customer.debug(ClassName + "::updating FROM_DATE for next run:" + loToDate.toString());
                                if(!bSkipExecution)
                                {
                                setOrUpdateDW2DateCtrl("FROM_DATE", new Long(loToDate.getTimeInMillis()).toString(), DB_TYPE_DATE); // redundant values for string and int
                                }
                            }
                        }
                    }
                    catch (Exception loEx)
                    {
                        Log.customer.error(7000, ClassName + "::run() failed to update from date for next run:\n" + getStackMessg(loEx));
                        Log.customer.error(7000, "::run() exception5 in processing mesg = " + loEx.getMessage());
                        Log.customer.error(7000, "::run() exception5 in processing cause = " + loEx.getCause());
                        Log.customer.error(7000, "::run() exception5 = " + getStackMessg(loEx));

                    }

                    try
                    {
                        if (allDocSuccess && !oneDocFailed) // we want to process all docs, so we ignore if one doc fails but continue processing. hence need 2 conditions
                        {
                            // Increment DW_ETLNUM (liDWETLNum)
                            if(!bSkipExecution)
                            {
                            setOrUpdateDW2DateCtrl("DW_ETLNUM", String.valueOf(liDWETLNum + 1), DB_TYPE_INT);
                            }
                        }
                    }
                    catch (Exception loEx)
                    {
                        Log.customer.error(7000, ClassName + "::run() failed to update etl num for next run:\n" + getStackMessg(loEx));
                        Log.customer.error(7000, "::run() exception6 in processing mesg = " + loEx.getMessage());
                        Log.customer.error(7000, "::run() exception6 in processing cause = " + loEx.getCause());
                        Log.customer.error(7000, "::run() exception6 = " + getStackMessg(loEx));

                    }

                    // set RUN_TYPE = S if lsRunType=A
  
                         if(allDocSuccess && !oneDocFailed && lsRunType != null && lsRunType.trim().equalsIgnoreCase("AD"))
                          {
                           Log.customer.debug(ClassName + "::setting RUN_TYPE to S");
                           if(!bSkipExecution)
                           {
                           setOrUpdateDW2DateCtrl("RUN_TYPE", "S", DB_TYPE_STRING);
                           }
                          }
                       else if(oneDocFailed && lsRunType != null && lsRunType.trim().equalsIgnoreCase("AD"))
                        {
                        Log.customer.debug(ClassName + "::setting RUN_TYPE to AD");
                        if(!bSkipExecution)
                        {
                        setOrUpdateDW2DateCtrl("RUN_TYPE", "AD", DB_TYPE_STRING);
                        }
                        }
                    // set TASK_RUNNING = N
                    Log.customer.debug(ClassName + "::setting TASK_RUNNING to N");
                    if(!bSkipExecution)
                    {
                    setOrUpdateDW2DateCtrl("TASK_RUNNING", "N", DB_TYPE_STRING);
                    }
                }
                catch (Exception loEx)
                {
                    // MAJOR ERROR writing to the DB
                    Log.customer.error(7000, ClassName + "::run() failed to close moConnection in finally block " + getStackMessg(loEx));
                    Log.customer.error(7000, "::run() exception7 in processing mesg = " + loEx.getMessage());
                    Log.customer.error(7000, "::run() exception7 in processing cause = " + loEx.getCause());
                    Log.customer.error(7000, "::run() exception7 = " + getStackMessg(loEx));

                }

                try
                {
                    if (docRS != null)
                    {
                        docRS.close();
                    }
                }
                catch (SQLException e)
                {
                    Log.customer.error(7000, ClassName + "::cannot close resultset docRS = " + getStackMessg(e));
                    Log.customer.error(7000, "::run() exception8 in processing mesg = " + e.getMessage());
                    Log.customer.error(7000, "::run() exception8 in processing cause = " + e.getCause());
                    Log.customer.error(7000, "::run() exception8 = " + getStackMessg(e));

                }
                try
                {
                    if (rs != null)
                    {
                        rs.close();
                    }
                }
                catch (SQLException e)
                {
                    Log.customer.error(7000, ClassName + "::cannot close resultset rs = " + getStackMessg(e));
                    Log.customer.error(7000, "::run() exception9 in processing mesg = " + e.getMessage());
                    Log.customer.error(7000, "::run() exception9 in processing cause = " + e.getCause());
                    Log.customer.error(7000, "::run() exception9 = " + getStackMessg(e));
                }
                try
                {
                    if (preStatement != null)
                    {
                        preStatement.close();
                    }
                }
                catch (SQLException e)
                {
                    Log.customer.error(7000, ClassName + "::cannot close statement prestatment = " + getStackMessg(e));
                    Log.customer.error(7000, "::run() exception10 in processing mesg = " + e.getMessage());
                    Log.customer.error(7000, "::run() exception10 in processing cause = " + e.getCause());
                    Log.customer.error(7000, "::run() exception10 = " + getStackMessg(e));
                }
                try
                {
                    if (loCtrlRecRS != null)
                    {
                        loCtrlRecRS.close();
                    }
                }
                catch (SQLException e)
                {
                    Log.customer.error(7000, ClassName + "::cannot close connection = " + getStackMessg(e));
                    Log.customer.error(7000, "::run() exception11 in processing mesg = " + e.getMessage());
                    Log.customer.error(7000, "::run() exception11 in processing cause = " + e.getCause());
                    Log.customer.error(7000, "::run() exception11 = " + getStackMessg(e));

                }
                try
                {
                    if (preStatement1 != null)
                    {
                        preStatement1.close();
                    }
                }
                catch (SQLException e)
                {
                    Log.customer.error(7000, ClassName + "::cannot close connection = " + getStackMessg(e));
                    Log.customer.error(7000, "::run() exception11 in processing mesg = " + e.getMessage());
                    Log.customer.error(7000, "::run() exception11 in processing cause = " + e.getCause());
                    Log.customer.error(7000, "::run() exception11 = " + getStackMessg(e));

                }
                try
                {
                    if (moConnection != null)
                    {
                        moConnection.close();
                    }
                }
                catch (SQLException e)
                {
                    Log.customer.error(7000, ClassName + "::cannot close connection = " + getStackMessg(e));
                    Log.customer.error(7000, "::run() exception11 in processing mesg = " + e.getMessage());
                    Log.customer.error(7000, "::run() exception11 in processing cause = " + e.getCause());
                    Log.customer.error(7000, "::run() exception11 = " + getStackMessg(e));

                }
                Log.customer.warning(7000, ClassName + "::run() closed moConnection in finally block");
            }
        }
        else
        {
            Log.customer.error(7000, ClassName + "::run() COULDN'T establish a connection");
            Log.customer.error(7000, ClassName + "::run() " + MONITORING_MESSAGE);
        }
        Log.customer.debug(ClassName + "::run() end");
    }

    private void writeToADHOCETLLogs(int liDWETLNum) throws Exception
    {
        int liUpdateCount = -1;
        PreparedStatement preparedStatement = null;
        PreparedStatement preparedStatement2 = null;

        try
        {
            if (initConnection())
            {
                //CSPL-8305. Setting newly added DW_ETLDATE column.
                String sInsertSQL = "INSERT INTO DW2_ADHOC_EXTR_DOCLIST_HIST (SOURCE, EXTRACT_DOC_TYPE,ADHOC_EXTRACT_LIST,DW_ETLNUM,DW_ETLDATE) " +
                       "(SELECT SOURCE,EXTRACT_DOC_TYPE, ADHOC_EXTRACT_LIST, ?, ? FROM DW2_ADHOC_EXTR_DOCLIST WHERE SOURCE = 'ARIBA' AND ADHOC_EXTRACT_LIST IS NOT NULL)";
                preparedStatement = moConnection.prepareStatement(sInsertSQL);
                Log.customer.debug("::writeToADHOCETLLogs() - adding record to DW2_ADHOC_EXT_DOCLIST_HIST lsUpdateSQL= " + sInsertSQL);
                preparedStatement.setInt(1, liDWETLNum);
                preparedStatement.setTimestamp(2, new java.sql.Timestamp(Calendar.getInstance().getTimeInMillis()));
                liUpdateCount = preparedStatement.executeUpdate();

                if (liUpdateCount > 0)
                {
                    //resetting DW2_ADHOC_EXTR_DOCLIST 
                    String sUpdateAdocListSQL = "UPDATE DW2_ADHOC_EXTR_DOCLIST SET ADHOC_EXTRACT_LIST = NULL WHERE ADHOC_EXTRACT_LIST IS NOT NULL " +
                           "AND SOURCE = 'ARIBA' ";
                    preparedStatement2 = moConnection.prepareStatement(sUpdateAdocListSQL);
                    Log.customer.debug("::writeToADHOCETLLogs() sUpdateAdocListSQL = " + sUpdateAdocListSQL);
                    int liUpdateCount2 = preparedStatement2.executeUpdate();
                    Log.customer.debug("::writeToADHOCETLLogs() liUpdateCount2 = " + liUpdateCount2);
                    moConnection.commit();
                }
                else
                {
                    Log.customer.warning(7000, "::writeToADHOCETLLogs() failed writing to DW2_ADHOC_EXTR_DOCLIST_HIST");
                }
            }
            else
            {
                Log.customer.error(7000, "::writeToADHOCETLLogs()::failed writing to DW2_ADHOC_EXTR_DOCLIST - connection not established");
            }
        }
        finally
        {
            try
            {
                if (preparedStatement != null)
                {
                    preparedStatement.close();
                }
                if (preparedStatement2 != null)
                {
                    preparedStatement2.close();
                }
            }
            catch (SQLException e)
            {
                Log.customer.error(7000, "::writeToADHOCETLLogs() exception in closing preparedStatement(s) - " + getStackMessg(e));
            }
        }
    }

    private boolean writetoExtractedDocs(String sClientId, String sDocName, ArrayList<String> loOutJsons, int iETLno)
    {
        Log.customer.debug(ClassName + "::writetoExtractedDocs() start");

        boolean bSuccess = true;
        int j;
        int retVal;

        PreparedStatement loInsertStatement = null;
        PreparedStatement loUpdateClobStatement = null;
        ResultSet loUpdateRecRS = null;

        BufferedReader      inputBufferedReader     = null;
        int                 chunkSize;
        char[]              textBuffer;
        long                position;
        int                 charsRead               = 0;
        oracle.sql.CLOB loCLOB = null;

        try
        {
            if (initConnection())
            {
                // JB
                // Unique key is combination of SOURCE, EXTRACTED_DOC_NAME, DOC_SEQ_NUM, DW_ETLNUM; hence you need to make sure this combination is unique
                // for now added query so that DW_ETLNUM keeps increasing, making unique entries for each run; Need to check with DW team for unique combination

                //String lsInsert = "INSERT INTO DW2_EXTRACTED_DOCS (CLIENT_ID, SOURCE, EXTRACTED_DOC_NAME, DOC_SEQ_NUM, EXTRACTED_DOCS, DW_ETLNUM, DW_ETLDATE) VALUES" + "(?,?,?,?,?,?,?)";
            	String lsInsert = "INSERT INTO DW2_EXTRACTED_DOCS (CLIENT_ID, SOURCE, EXTRACTED_DOC_NAME, DOC_SEQ_NUM, EXTRACTED_DOCS, DW_ETLNUM, DW_ETLDATE) VALUES" + "(?,?,?,?,empty_clob(),?,?)";
                loInsertStatement = moConnection.prepareStatement(lsInsert);
                for (int i = 0; i < loOutJsons.size(); i++)
                {
                    j = i + 1;// we want to start the DOC_SEQ_NUM with 1 instead of 0. DW wants to consume starting # 1
                    loInsertStatement.setString(1, sClientId);
                    loInsertStatement.setString(2, DW_TYPE_ARIBA); // JB_notes : ARIBA
                    loInsertStatement.setString(3, sDocName);
                    loInsertStatement.setInt(4, j);
                    loInsertStatement.setInt(5, iETLno);
                    loInsertStatement.setTimestamp(6, new java.sql.Timestamp(Calendar.getInstance().getTimeInMillis()));
                    Log.customer.debug(ClassName + "::writetoExtractedDocs() lsInsert: " + lsInsert);
                    retVal = loInsertStatement.executeUpdate();
                    if (retVal <= 0)
                    {
                        bSuccess = false;
                        Log.customer.warning(7000, ClassName + "::writetoExtractedDocs() failed to insert ");
                    }
                    else
                    {
                    	moConnection.commit();
                    	String lsUpdate = "SELECT EXTRACTED_DOCS FROM DW2_EXTRACTED_DOCS WHERE CLIENT_ID = '" + sClientId
                    			+ "' AND SOURCE = '" + DW_TYPE_ARIBA + "' AND EXTRACTED_DOC_NAME = '" + sDocName + "' AND DOC_SEQ_NUM=" + (new Integer(j)).toString()
                    			+ " AND DW_ETLNUM =" + (new Integer(iETLno)).toString()
                    			+ " FOR UPDATE";
                    	Log.customer.debug( ClassName + "::writetoExtractedDocs() lsUpdate record" + lsUpdate);
                        loUpdateClobStatement = moConnection.prepareStatement(lsUpdate);
                        loUpdateRecRS=loUpdateClobStatement.executeQuery();

                       
                        inputBufferedReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(loOutJsons.get(i).getBytes("UTF-8")),"UTF-8"));                        
                        
                        if (loUpdateRecRS.next())
                        {

                        	  //Log.customer.warning(7000, ClassName + "::writetoExtractedDocs() found record for updating clob");
                              loCLOB = (oracle.sql.CLOB)loUpdateRecRS.getObject( 1 ) ;

                              chunkSize = loCLOB.getChunkSize();
                              textBuffer = new char[chunkSize];

                              position = 1;
                              while ((charsRead = inputBufferedReader.read(textBuffer)) != -1) {
                                   loCLOB.putChars(position, textBuffer, charsRead);
                                  position        += charsRead;
                              }
                              if(inputBufferedReader!=null)
                              {
                                 inputBufferedReader.close();
                              }
                        }
                        else
                        {
                            // error method
                            bSuccess = false;
                            Log.customer.warning(7000, ClassName + "::writetoExtractedDocs() failed to write to clob dw2_extracted_docs ");
                        }
                        if(loUpdateRecRS!=null)
                        {
                        	loUpdateRecRS.close();
                        }
                        if (loUpdateClobStatement != null)
                        {
                        	loUpdateClobStatement.close();
                        }

                    }
                }
            }
        }
        catch (Exception loEx)
        {
            Log.customer.error(7000, ClassName + "::writetoExtractedDocs() exception (xxx) = " + getStackMessg(loEx));
            bSuccess = false;
            Log.customer.error(7000, "::run() exception12 in processing mesg = " + loEx.getMessage());
            Log.customer.error(7000, "::run() exception12 in processing cause = " + loEx.getCause());
            Log.customer.error(7000, "::run() exception12 = " + getStackMessg(loEx));

        }
        finally
        {
            try
            {
                if(inputBufferedReader!=null)
                {
                   inputBufferedReader.close();
                }
                if (loUpdateClobStatement != null)
                {
                	loUpdateClobStatement.close();
                }
                if (loUpdateRecRS != null)
                {
                	loUpdateRecRS.close();
                }
                if (loInsertStatement != null)
                {
                    loInsertStatement.close();
                }
                if (moConnection != null)
                {
                    moConnection.commit();
                }
                Log.customer.debug(ClassName + "::writetoExtractedDocs() closed loInsertStatement and committed moConnection2");
            }
            catch (Exception loEx)
            {
                Log.customer.error(7000, ClassName + "::writetoExtractedDocs() failed to close loInsertStatement or commit");
                Log.customer.error(7000, "::run() exception13 in processing mesg = " + loEx.getMessage());
                Log.customer.error(7000, "::run() exception13 in processing cause = " + loEx.getCause());
                Log.customer.error(7000, "::run() exception13 = " + getStackMessg(loEx));

            }
        }
        Log.customer.debug(ClassName + "::writetoExtractedDocs() end - bSuccess" + bSuccess);
        return bSuccess;
    }

    private boolean setOrUpdateDW2DateCtrl(String sColumn, String sValue, int type) throws Exception
    {
        int liUpdateCount = -1;
        boolean bUpdtSuccessful = false;

        PreparedStatement preparedUpdateSQLStatement = null;
        try
        {
            if (initConnection())
            {
                // String lsUpdate = "UPDATE DW2_DATE_CTRL SET " + sColumn + " = " + sValue + " WHERE DW_TYPE = 'ARIBA' AND CLIENT_ID = '" + lsCLIENT_ID + "'";
                String lsUpdate = "UPDATE DW2_DATE_CTRL SET " + sColumn + " = ? " + " WHERE DW_TYPE = ? AND CLIENT_ID = ?";
                preparedUpdateSQLStatement = moConnection.prepareStatement(lsUpdate);
                if (type == DB_TYPE_STRING)
                {
                    preparedUpdateSQLStatement.setString(1, sValue);
                }
                else if (type == DB_TYPE_INT)
                {
                    preparedUpdateSQLStatement.setInt(1, new Integer(sValue));
                }
                else if (type == DB_TYPE_DATE)
                {
                    // convert string to long
                    Calendar tempCal = Calendar.getInstance();
                    long lTime = new Long(sValue);
                    tempCal.setTimeInMillis(lTime);
                    preparedUpdateSQLStatement.setTimestamp(1, new java.sql.Timestamp(tempCal.getTimeInMillis()));
                }
                preparedUpdateSQLStatement.setString(2, DW_TYPE_ARIBA);
                preparedUpdateSQLStatement.setString(3, lsCLIENT_ID);

                Log.customer.debug(ClassName + "::setOrUpdate() lsUpdate: " + lsUpdate);
                liUpdateCount = preparedUpdateSQLStatement.executeUpdate();

                Log.customer.debug(ClassName + "::setOrUpdate() liUpdateCount: " + liUpdateCount);
                if (liUpdateCount > 0)
                {
                    bUpdtSuccessful = true;
                }
                else
                {
                    bUpdtSuccessful = false;
                }
            }
        }// JB - catch above in main run method and write a message to monitor!
        finally
        {
            try
            {
                if (preparedUpdateSQLStatement != null)
                {
                    preparedUpdateSQLStatement.close();
                }
            }
            catch (SQLException e)
            {
                Log.customer.error(7000, ClassName + "::error closing preparedUpdateSQLStatement: " + getStackMessg(e));
                Log.customer.error(7000, "::run() exception14 in processing mesg = " + e.getMessage());
                Log.customer.error(7000, "::run() exception14 in processing cause = " + e.getCause());
                Log.customer.error(7000, "::run() exception14 = " + getStackMessg(e));
            }

        }
        return bUpdtSuccessful;
    }

    private void InsertIntoDW2ETLLogs(String sClientId, int etlNum, String docName, String sMessage) throws Exception
    {
        int liUpdateCount = -1;
        PreparedStatement preparedInsertSQLStatement = null;

        try
        {
            if (initConnection())
            {

                String lsSQLInsert = "INSERT INTO DW2_ETL_LOGS " + "(CLIENT_ID, DW_ETLNUM, EXTRACTED_DOC_NAME, DOC_START_TIME, EXTRACT_MESSAGE) VALUES " + "(?,?,?,?,?)";

                Log.customer.debug(ClassName + "::success writing in DB");
                preparedInsertSQLStatement = moConnection.prepareStatement(lsSQLInsert);
                preparedInsertSQLStatement.setString(1, sClientId);
                preparedInsertSQLStatement.setInt(2, etlNum);
                preparedInsertSQLStatement.setString(3, docName);
                preparedInsertSQLStatement.setTimestamp(4, new java.sql.Timestamp(Calendar.getInstance().getTimeInMillis()));
                preparedInsertSQLStatement.setString(5, sMessage);
                liUpdateCount = preparedInsertSQLStatement.executeUpdate();

                if (liUpdateCount > 0)
                {
                    // log messages
                    Log.customer.debug(ClassName + "::success writing in DB");
                }
                else
                {
                    // log messages
                    Log.customer.error(ClassName + "::failed writing to DW2 LOG message DB - insert failed");
                }
            }
            else
            {
                Log.customer.error(ClassName + "::failed writing to DW2 LOG message DB - connection not established");
            }
        } // JB - catch above in main run method and write a message to monitor!
        finally
        {
            try
            {
                if (preparedInsertSQLStatement != null)
                {
                    preparedInsertSQLStatement.close();
                }
            }
            catch (SQLException e)
            {
                Log.customer.error(7000, ClassName + "::error closing preparedInsertSQLStatement: " + getStackMessg(e));
                Log.customer.error(7000, "::run() exception15 in processing mesg = " + e.getMessage());
                Log.customer.error(7000, "::run() exception15 in processing cause = " + e.getCause());
                Log.customer.error(7000, "::run() exception15 = " + getStackMessg(e));
            }

        }
    }

    private void UpdateFinishMsgDW2ETLLogs(String sClientId, int etlNum, String docName, String sMessage, int sRecordsExtracted, String extractFlag) throws Exception
    {
        int liUpdateCount = -1;
        PreparedStatement preparedInsertSQLStatement = null;

        try
        {
            if (initConnection())
            {

                String lsUpdateSQL = "UPDATE DW2_ETL_LOGS SET DOC_END_TIME = ? , RECS_EXTRACTED_FROM_ARIBA = ?, EXTRACT_MESSAGE = ?, EXTRACT_FLAG = ? WHERE CLIENT_ID = ? AND DW_ETLNUM=? AND EXTRACTED_DOC_NAME = ?";
                preparedInsertSQLStatement = moConnection.prepareStatement(lsUpdateSQL);
                preparedInsertSQLStatement.setTimestamp(1, new java.sql.Timestamp(Calendar.getInstance().getTimeInMillis()));
                if (StringUtil.equalsIgnoreCase(docName, ETL_LOGS_ALL_DOC_NAME))
                {
                    // leave it blank for all records
                    preparedInsertSQLStatement.setNull(2, Types.NULL);
                }
                else
                {
                    preparedInsertSQLStatement.setInt(2, sRecordsExtracted);
                }

                preparedInsertSQLStatement.setString(3, sMessage);
                preparedInsertSQLStatement.setString(4, extractFlag);
                preparedInsertSQLStatement.setString(5, sClientId);
                preparedInsertSQLStatement.setInt(6, etlNum);
                preparedInsertSQLStatement.setString(7, docName);

                liUpdateCount = preparedInsertSQLStatement.executeUpdate();

                if (liUpdateCount > 0)
                {
                    // log messages
                    Log.customer.debug(ClassName + "::success writing update etl logs in DB");
                }
                else
                {
                    // log messages
                    Log.customer.error(ClassName + "::failed writing to DW2 LOG message DB - update failed");
                }
            }
            else
            {
                Log.customer.error(ClassName + "::failed writing to DW2 LOG message DB - connection not established");
            }
        } // JB - catch above in main run method and write a message to monitor!
        finally
        {
            try
            {
                if (preparedInsertSQLStatement != null)
                {
                    preparedInsertSQLStatement.close();
                }
            }
            catch (SQLException e)
            {
                Log.customer.error(7000, ClassName + "::error closing preparedInsertSQLStatement: " + getStackMessg(e));
                Log.customer.error(7000, "::run() exception16 in processing mesg = " + e.getMessage());
                Log.customer.error(7000, "::run() exception16 in processing cause = " + e.getCause());
                Log.customer.error(7000, "::run() exception16 = " + getStackMessg(e));
            }

        }
    }

    // writes to DW2_ETL_LOGS_HIST and removes entries from DW2_ETL_LOGS for # liDWETLNum
    private void WriteToHistAndClearETLLogs(int etlNum) throws Exception
    {
        int liUpdateCount = -1;
        PreparedStatement preparedSQLStatement = null;

        try
        {
            if (initConnection())
            {

                String lsSQLStatemnt = "INSERT INTO DW2_ETL_LOGS_ERRORS SELECT * FROM DW2_ETL_LOGS where DW_ETLNUM=?";

                Log.customer.debug(ClassName + "::success writing in DB");
                preparedSQLStatement = moConnection.prepareStatement(lsSQLStatemnt);
                preparedSQLStatement.setInt(1, etlNum);
                liUpdateCount = preparedSQLStatement.executeUpdate();

                if (liUpdateCount > 0)
                {
                    // log messages
                    Log.customer.debug(ClassName + "::archived logs for old ETL # " + (new Integer(etlNum)).toString());
                }
                else
                {
                    // log messages
                    Log.customer.debug(ClassName + "::DID NOT archive logs for old ETL # OR maybe this is new ETL #" + (new Integer(etlNum)).toString());
                }

                lsSQLStatemnt = "DELETE FROM DW2_ETL_LOGS where DW_ETLNUM=?";
                preparedSQLStatement = moConnection.prepareStatement(lsSQLStatemnt);
                preparedSQLStatement.setInt(1, etlNum);
                preparedSQLStatement.executeUpdate();

                // log messages
                Log.customer.debug(ClassName + "::deleted from DW2_ETL_LOGS for old ETL # OR maybe this is new ETL #" + (new Integer(etlNum)).toString());

            }
            else
            {
                Log.customer.error(ClassName + "::failed to update/delete DW2_ETL_LOGS_HIST - connection not established");
            }
        }
        finally
        {
            try
            {
                if (preparedSQLStatement != null)
                {
                    preparedSQLStatement.close();
                }
            }
            catch (SQLException e)
            {
                Log.customer.error(7000, ClassName + "::error closing preparedInsertSQLStatement: " + getStackMessg(e));
                Log.customer.error(7000, "::run() exception17 in processing mesg = " + e.getMessage());
                Log.customer.error(7000, "::run() exception17 in processing cause = " + e.getCause());
                Log.customer.error(7000, "::run() exception17 = " + getStackMessg(e));
            }
        }
    }

    // remove entries from DW2_EXTRACTED_DOCS for # liDWETLNum
    private void RemoveExtractedDOCS(int etlNum) throws Exception
    {
        int liUpdateCount = -1;
        PreparedStatement preparedInsertSQLStatement = null;

        try
        {
            if (initConnection())
            {

                String lsSQLInsert = "DELETE FROM DW2_EXTRACTED_DOCS where DW_ETLNUM=?";

                Log.customer.debug(ClassName + "::deleting  DW2_EXTRACTED_DOCS in DB");
                preparedInsertSQLStatement = moConnection.prepareStatement(lsSQLInsert);
                preparedInsertSQLStatement.setInt(1, etlNum);
                liUpdateCount = preparedInsertSQLStatement.executeUpdate();

                if (liUpdateCount > 0)
                {
                    // log messages
                    Log.customer.debug(ClassName + "::deleted from DW2_EXTRACTED_DOCS for old ETL # " + (new Integer(etlNum)).toString());
                }
                else
                {
                    // log messages
                    Log.customer.debug(ClassName + "::DID NOT delete from DW2_EXTRACTED_DOCS for old ETL # OR maybe this is new ETL #" + (new Integer(etlNum)).toString());
                }
            }
            else
            {
                Log.customer.error(ClassName + "::failed to update DW2_EXTRACTED_DOCS - connection not established");
            }
        }
        finally
        {
            try
            {
                if (preparedInsertSQLStatement != null)
                {
                    preparedInsertSQLStatement.close();
                }
            }
            catch (SQLException e)
            {
                Log.customer.error(7000, ClassName + "::error closing preparedInsertSQLStatement: " + getStackMessg(e));
                Log.customer.error(7000, "::run() exception18 in processing mesg = " + e.getMessage());
                Log.customer.error(7000, "::run() exception18 in processing cause = " + e.getCause());
                Log.customer.error(7000, "::run() exception18 = " + getStackMessg(e));
            }
        }

    }

    private void WriteToDATECTRLHIST() throws Exception
    {
        int liUpdateCount = -1;
        PreparedStatement preparedInsertSQLStatement = null;

        try
        {
            if (initConnection())
            {

                Calendar cl = Calendar.getInstance();
                cl.setTimeInMillis(extractStartTime.getTime());
                String sExtractStartDate = "'" + sdf_ET2.format(cl.getTime()) + "'" + "," + "'" + "yyyy-MM-dd hh24:mi:ss" + "'";
                String sExtractEndDate = "'" + sdf_ET2.format(Calendar.getInstance().getTime()) + "'" + "," + "'" + "yyyy-MM-dd hh24:mi:ss" + "'";

                String lsSQLInsert = "INSERT INTO DW2_DATE_CTRL_HIST (CLIENT_ID,DW_TYPE,DW_ETLNUM,FROM_DATE, TO_DATE,ADHOC_FROM_DATE, ADHOC_TO_DATE,RUN_TYPE, EXTRACT_START_TIME, EXTRACT_END_TIME) SELECT CLIENT_ID,DW_TYPE,DW_ETLNUM,FROM_DATE, TO_DATE,ADHOC_FROM_DATE, ADHOC_TO_DATE,RUN_TYPE, to_date(" + sExtractStartDate + "),to_date(" + sExtractEndDate + ") FROM DW2_DATE_CTRL WHERE DW_TYPE = ?";
                Log.customer.debug(ClassName + "::success writing in DB " + lsSQLInsert);
                preparedInsertSQLStatement = moConnection.prepareStatement(lsSQLInsert);
                preparedInsertSQLStatement.setString(1, DW_TYPE_ARIBA);
                liUpdateCount = preparedInsertSQLStatement.executeUpdate();

                if (liUpdateCount > 0)
                {
                    // log messages
                    Log.customer.debug(ClassName + "::row copied to DW2_DATE_CTRL_HIST");
                }
                else
                {
                    // log messages
                    Log.customer.debug(ClassName + "::row NOT copied to DW2_DATE_CTRL_HIST");
                }
            }
            else
            {
                Log.customer.error(ClassName + "::failed to update DW2_DATE_CTRL_HIST");
            }
        }
        finally
        {
            try
            {
                if (preparedInsertSQLStatement != null)
                {
                    preparedInsertSQLStatement.close();
                }
            }
            catch (SQLException e)
            {
                Log.customer.error(7000, ClassName + "::error closing preparedInsertSQLStatement: " + getStackMessg(e));
                Log.customer.error(7000, "::run() exception19 in processing mesg = " + e.getMessage());
                Log.customer.error(7000, "::run() exception19 in processing cause = " + e.getCause());
                Log.customer.error(7000, "::run() exception19 = " + getStackMessg(e));
            }
        }
    }

    // update timestamp
    /*
     * String lsUpdateSQL = "UPDATE DW2_ETL_LOGS SET DOC_START_TIME = ? WHERE CLIENT_ID=? AND DW_ETLNUM=? AND EXTRACTED_DOC_NAME = ?"; preStatement = moConnection.prepareStatement(lsSQL); preStatement.setTimestamp(1, new java.sql.Timestamp(Calendar.getInstance().getTimeInMillis())); preStatement.setString(2, lsCLIENT_ID); preStatement.setInt(3, liDWETLNum); preStatement.setString(4, ETL_LOGS_ALL_DOC_NAME);
     *
     * iSqlRetVal = preStatement.executeUpdate();
     *
     * if (iSqlRetVal == 0) // no row was updated. so return without any changes - one of the where conditions failed { Log.customer.error(8888, ClassName + "::could not write to DW2_ETL_LOGS"); Log.customer.error(8888, ClassName + "::run() " + MONITORING_MESSAGE); return; }
     */

    private String getStackMessg(Exception ex)
    {
        StackTraceElement[] arr = ex.getStackTrace();
        String stackMessg = "";

        ex.printStackTrace();

        if (ex.getMessage() != null)
        {
            stackMessg = stackMessg + ex.getMessage() + "\n";
        }
        if (ex.getCause() != null)
        {
            stackMessg = "cause:" + stackMessg + ex.getCause() + "\n";
        }
        for (int i = 0; i < arr.length; i++)
        {
            stackMessg = stackMessg + (arr[i].toString()) + "\n";
        }
        return stackMessg;
    }

    private boolean initConnection()
    {
        Log.customer.debug(ClassName + "::initConnection() start");
        try
        {
            if (moConnection == null || moConnection.isClosed())
            {
                Log.customer.warning(7000, ClassName + "::initConnection() is null/closed so get one");
                try
                {

                    String lsDriver = msDBDriver;
                    String lsUrl = msDBURL;
                    String lsUser = msDBUser;
                    String lsPassword = msDBPassword;

                    Log.customer.warning(7000, ClassName + "Driver = " + lsDriver + ", URL = " + lsUrl + ", User = " + lsUser + " password = " + lsPassword);
                    moConnection = (Connection) DriverManager.getConnection(lsUrl, lsUser, lsPassword);

                    Log.customer.warning(7000, ClassName + "::initConnection() connected. now setAutoCommit to false.");
                    moConnection.setAutoCommit(false);

                }
                catch (SQLException loExp)
                {
                    Log.customer.error(7000, ClassName + "::initConnection() SQL Exception in connection = " + loExp.getMessage());
                    Log.customer.error(7000, "::run() exception20 in processing mesg = " + loExp.getMessage());
                    Log.customer.error(7000, "::run() exception20 in processing cause = " + loExp.getCause());
                    Log.customer.error(7000, "::run() exception20 = " + getStackMessg(loExp));
                    return false;
                }
                Log.customer.warning(7000, ClassName + "::initConnection() new connection created");
                return true;
            }
            else
            {
                Log.customer.debug(ClassName + "::initConnection() using existing connection");
                return true;
            }
        }
        catch (Exception loEx)
        {
            Log.customer.error(7000, ClassName + "::initConnection() exception = " + loEx.getMessage());
            Log.customer.error(7000, "::run() exception21 in processing mesg = " + loEx.getMessage());
            Log.customer.error(7000, "::run() exception21 in processing cause = " + loEx.getCause());
            Log.customer.error(7000, "::run() exception21 = " + getStackMessg(loEx));
            return false;
        }
    }

    // Log errors to DW2_ETL_LOGS_ERRORS
    private void WriteErrorLogs(String sClientID, int iETLNum, String sDocName, String sErrorMessage) throws Exception
    {

        int liUpdateCount = -1;
        PreparedStatement preparedSQLStatement = null;

        try
        {
            if (initConnection())
            {

                Log.customer.debug(ClassName + "::writing errors to DW2_ETL_LOGS_ERRORS");
                String lsSQLStatemnt = "INSERT INTO DW2_ETL_LOGS_ERRORS (CLIENT_ID, DW_ETLNUM, EXTRACTED_DOC_NAME, EXTRACT_MESSAGE) VALUES (?,?,?,?)";
                preparedSQLStatement = moConnection.prepareStatement(lsSQLStatemnt);
                preparedSQLStatement.setString(1, sClientID);
                preparedSQLStatement.setInt(2, iETLNum);
                preparedSQLStatement.setString(3, sDocName);
                preparedSQLStatement.setString(4, sErrorMessage);

                liUpdateCount = preparedSQLStatement.executeUpdate();

                if (liUpdateCount > 0)
                {
                    // log messages
                    Log.customer.debug(ClassName + ":: logged errors for ETL # " + (new Integer(iETLNum)).toString());
                }
                else
                {
                    // log messages
                    Log.customer.debug(ClassName + "::DID NOT log erros for ETL # " + (new Integer(iETLNum)).toString());
                }
            }
            else
            {
                Log.customer.error(ClassName + "::failed to insert to DW2_ETL_LOGS_ERRORS - connection not established");
            }
        }
        finally
        {
            try
            {
                if (preparedSQLStatement != null)
                {
                    preparedSQLStatement.close();
                }
            }
            catch (SQLException e)
            {
                Log.customer.error(7000, ClassName + "::error closing preparedInsertSQLStatement: " + getStackMessg(e));
                Log.customer.error(7000, "::WriteErrorLogs() exception mesg = " + e.getMessage());
                Log.customer.error(7000, "::WriteErrorLogs() exception cause = " + e.getCause());
                Log.customer.error(7000, "::WriteErrorLogs() exception= " + getStackMessg(e));
            }
        }
    }

}
