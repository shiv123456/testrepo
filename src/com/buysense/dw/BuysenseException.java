/**
 * @author  Andrius Karpavicius
 * @version 1.0
 * Indicates Buysense applciation exceptions
 **/


package com.buysense.dw;

public class BuysenseException extends Exception
{
    
    // Exception constructor
    public BuysenseException()
    {
        super();
    }
    
    // Exception constructor
    public BuysenseException(String s)
    {
        super(s);
    }
    
    
    // Exception constructor
    public BuysenseException(Exception e)
    {
        super(e.toString());
        e.printStackTrace();
    }
}
