package com.cgi.evamq.consumer;


import java.util.ArrayList;
import java.util.List;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;

public class EvaConsumeMessage extends Thread {
	
	private List<EvaConsumeCallBack> callbacks = new ArrayList<EvaConsumeCallBack>();
	private List<String> queues = new ArrayList<String>();
	private String loEndPoint = "";
	
	
	public void run() {
		
		if (loEndPoint == "") {
			return;
		}
		CamelContext context = new DefaultCamelContext(); // probably expensive to create context

        // connect to embedded ActiveMQ JMS broker
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(loEndPoint);
        // move the URL out of here as it will be env specific
        context.addComponent("jms", JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
   	
		ConsumerTemplate template = context.createConsumerTemplate();
		try {
			context.start();
		
		System.out.println("Waiting for messages... ");
			
		String body;
		String queueName = null;
		
		while(true) {
		
		  int position = 0;
          for (EvaConsumeCallBack timerCallBack : callbacks) {        	  
                       
            queueName = queues.get(position);
            
            body = template.receiveBody("jms:"+queueName, 3000, String.class);
            
        	if (body != null) {
        		
        		System.out.println("Message: " + body);
                try {
					timerCallBack.updateMessage(body);
				} catch (JMSException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                
        	}
        	
        	position++;
          }
                                       
		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setEndpoint(String endpoint) {
		
		loEndPoint = endpoint;
		
	}
	
	public void registerCallBack(EvaConsumeCallBack timerCallBack, String queue) {
        callbacks.add(timerCallBack);
        queues.add(queue);
    }
	
	
}
