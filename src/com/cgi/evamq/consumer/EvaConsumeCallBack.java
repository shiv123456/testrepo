package com.cgi.evamq.consumer;

import javax.jms.JMSException;


public interface EvaConsumeCallBack
{
    public void updateMessage(String message) throws JMSException, Exception;
    public String getQueueName() throws JMSException, Exception;
    public String getEndPoint() throws JMSException, Exception; 
    
}

