package com.cgi.evamq.common;

public class EvaQueueMessage
{

    public final static String APPLICATION_ARIBA = "ARIBA";
    public final static String APPLICATION_PORTAL = "PORTAL";
    public final static String APPLICATION_ADV = "ADV";
    public final static String APPLICATION_VSS = "VSS";
    public final static String APPLICATION_EXTARIBA = "EXTARIBA";
    public final static String APPLICATION_QQ = "QQ";    
    
    private String MsgCreator; // Application that creates the message and pushes it to
                               // camel/activemq broker example:ARIBA/PORTAL/ADV/QQ
    private String ObjID; // ID of the object for which message was created - example:POXYZ /
                          // VENDORXYZ/ EFORMIDXYZ
    private String ObjOrigin; // Indicates the application from where the document/object initially
                              // originated from. For example: Ariba reqs can be created as part of
                              // import process from QQ, external order process or from ADV
                              // example: QQ/ExternalOrders/ADV
    private String ObjOriginID; // ID id the originating object Example: QQ027150_AWD596
    
    private String Event; // This is either state or status for a particular document. example:
                          // Ordered (for POs)/ New (vendor updates)

    private String DocSourceID; // If it�s POStatus then the originating document is a requisition.
                                // example:REQXYZ
    private String VendorID; // example:VENDORXYZ
    private String DocUserID; // example: USERID
	private String EvaOtherPayLoad; // payload that applications can send


    // public String EvaOtherPayLoad; //Queue specific payload. Some queues might need more
    // information to process other than the fields listed above. This will be a custom class that
    // producer application and consumer application can mutually agree upon

    public String getMsgCreator()
    {
        return MsgCreator;
    }

    public void setMsgCreator(String msgCreator)
    {
        MsgCreator = msgCreator;
    }

    public String getObjID()
    {
        return ObjID;
    }

    public void setObjID(String objID)
    {
        ObjID = objID;
    }

    public String getObjOrigin()
    {
        return ObjOrigin;
    }

    public void setObjOrigin(String objOrigin)
    {
        ObjOrigin = objOrigin;
    }

    public String getEvent()
    {
        return Event;
    }

    public void setEvent(String event)
    {
        Event = event;
    }

    public String getDocSourceID()
    {
        return DocSourceID;
    }

    public void setDocSourceID(String docSourceID)
    {
        DocSourceID = docSourceID;
    }

    public String getVendorID()
    {
        return VendorID;
    }

    public void setVendorID(String vendorID)
    {
        VendorID = vendorID;
    }

    public String getDocUserID()
    {
        return DocUserID;
    }

    public void setDocUserID(String docUserID)
    {
        DocUserID = docUserID;
    }

    public String getObjOriginID()
    {
        return ObjOriginID;
    }

    public void setObjOriginID(String objOriginID)
    {
        ObjOriginID = objOriginID;
    }

	public String getEvaOtherPayLoad()
    {
        return EvaOtherPayLoad;
    }

    public void setEvaOtherPayLoad(String evaOtherPayLoad)
    {
        EvaOtherPayLoad = evaOtherPayLoad;
    }

}
