package com.cgi.evamq.producer;

import javax.jms.JMSException;

import com.cgi.evamq.common.EvaQueueMessage;

public interface EvaSendMessage
{
    public void initializeCamel() throws JMSException, Exception;
    public void forceInitializeCamel() throws JMSException, Exception;
    public void cleanupCamel() throws JMSException, Exception;
    public void publishMessageToQueue(EvaQueueMessage EvaMsg) throws JMSException, Exception;        
}
