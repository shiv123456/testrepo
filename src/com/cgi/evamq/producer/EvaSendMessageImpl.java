package com.cgi.evamq.producer;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;



import com.cgi.evamq.common.EvaQueueMessage;
import com.google.gson.Gson;

public abstract class EvaSendMessageImpl implements EvaSendMessage
{
    private static CamelContext context;
    private static ProducerTemplate prodtemplate;

    public void initializeCamel() throws JMSException, Exception
    {
        if (context != null)
        {
            return;
        }
        // create CamelContext
        context = new DefaultCamelContext(); // probably expensive to create context

        // connect to embedded ActiveMQ JMS broker
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(getEndpoint());
        // move the URL out of here as it will be env specific
        context.addComponent("jms", JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
        context.start();

        // create producer template
        prodtemplate = context.createProducerTemplate();
    }

    public void cleanupCamel() throws JMSException, Exception
    {
        if (context != null)
        {
            // stop the CamelContext
            context.stop();
        }
    }

    // This methos could be useful if message queue connection gets reset
    public void forceInitializeCamel() throws JMSException, Exception
    {
        cleanupCamel();
        context = null;
        initializeCamel();
    }

    public void publishMessageToQueue(EvaQueueMessage EvaMsg) throws JMSException, Exception
    {
        Gson gson = new Gson();
        String debugMsg = "EvaSendMessageImpl:PO Status being sent to QUEUE:" + gson.toJson(EvaMsg);
        printDebugMsg(debugMsg);
        prodtemplate.sendBody("jms:" + getQueueName(), gson.toJson(EvaMsg));
    }

    // app specific logging method. This can be overridden by Portal or other applications
    protected abstract void printDebugMsg(String msg);
    
    //app specific end point retrieval method
    protected abstract String getEndpoint() throws Exception;
    
    // app specific 
    protected abstract String getQueueName() throws Exception;
}
