package config.java.ams.custom;

import ariba.approvable.core.Approvable;
import ariba.base.core.BaseObject;
import ariba.htmlui.approvableui.wizards.ARBApprovableContext;
import ariba.util.core.Date;
import ariba.util.core.StringUtil;
import ariba.util.log.Log;

public class BuysenseARBReceivingContext extends ARBApprovableContext {
	
	public String getReviewScreenDetails() {
        Log.customer.debug("Inside BuysenseARBReceivingContext");
        String sHint = "@ariba.procureui.hint/ReceivingOneStepHint";
        Approvable oReceipt = getApprovable();
        if (((BaseObject) oReceipt).instanceOf("ariba.receiving.core.Receipt"))
        {
            Date oDate = (Date) oReceipt.getDottedFieldValue("BuysenseEditDate");
            if (oDate != null)
            {
                sHint = "<font face=Arial size=2 color=#FF0000><b> </b></font>";
            }
        }
        return sHint;
	}
	@Override
	public String getTitleStepName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTitlePageName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTitleHint() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTitleHowTo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTitleScreenDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getJustifyStepName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getJustifyPageName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getJustifyHint() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getJustifyHowTo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getJustifyScreenDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getJustifyStepHelp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getApprovalFlowStepName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getApprovalFlowPageName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getApprovalFlowHint() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getApprovalFlowHowTo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getApprovalFlowScreenDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getApprovalFlowStepHelp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getReviewStepName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getReviewPageName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getReviewHint() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getReviewHowTo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getReviewStepHelp() {
		// TODO Auto-generated method stub
		return null;
	}


}
